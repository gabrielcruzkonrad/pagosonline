/**
 * Pagos_v3Holder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zonapagos.www.ws_verificar_pagos.holders.test;

public final class Pagos_v3Holder implements javax.xml.rpc.holders.Holder {
    public com.zonapagos.www.ws_verificar_pagos.test.Pagos_v3 value;

    public Pagos_v3Holder() {
    }

    public Pagos_v3Holder(com.zonapagos.www.ws_verificar_pagos.test.Pagos_v3 value) {
        this.value = value;
    }

}
