/**
 * Respuesta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zonapagos.www.ws_verificar_pagos.test;

public class Respuesta  implements java.io.Serializable {
    private int codigo;

    private java.lang.String descripcion;

    private com.zonapagos.www.ws_verificar_pagos.test.RespuestaDs_verificar_pago ds_verificar_pago;

    public Respuesta() {
    }

    public Respuesta(
           int codigo,
           java.lang.String descripcion,
           com.zonapagos.www.ws_verificar_pagos.test.RespuestaDs_verificar_pago ds_verificar_pago) {
           this.codigo = codigo;
           this.descripcion = descripcion;
           this.ds_verificar_pago = ds_verificar_pago;
    }


    /**
     * Gets the codigo value for this Respuesta.
     * 
     * @return codigo
     */
    public int getCodigo() {
        return codigo;
    }


    /**
     * Sets the codigo value for this Respuesta.
     * 
     * @param codigo
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }


    /**
     * Gets the descripcion value for this Respuesta.
     * 
     * @return descripcion
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }


    /**
     * Sets the descripcion value for this Respuesta.
     * 
     * @param descripcion
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }


    /**
     * Gets the ds_verificar_pago value for this Respuesta.
     * 
     * @return ds_verificar_pago
     */
    public com.zonapagos.www.ws_verificar_pagos.test.RespuestaDs_verificar_pago getDs_verificar_pago() {
        return ds_verificar_pago;
    }


    /**
     * Sets the ds_verificar_pago value for this Respuesta.
     * 
     * @param ds_verificar_pago
     */
    public void setDs_verificar_pago(com.zonapagos.www.ws_verificar_pagos.test.RespuestaDs_verificar_pago ds_verificar_pago) {
        this.ds_verificar_pago = ds_verificar_pago;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Respuesta)) return false;
        Respuesta other = (Respuesta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.codigo == other.getCodigo() &&
            ((this.descripcion==null && other.getDescripcion()==null) || 
             (this.descripcion!=null &&
              this.descripcion.equals(other.getDescripcion()))) &&
            ((this.ds_verificar_pago==null && other.getDs_verificar_pago()==null) || 
             (this.ds_verificar_pago!=null &&
              this.ds_verificar_pago.equals(other.getDs_verificar_pago())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getCodigo();
        if (getDescripcion() != null) {
            _hashCode += getDescripcion().hashCode();
        }
        if (getDs_verificar_pago() != null) {
            _hashCode += getDs_verificar_pago().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Respuesta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "Respuesta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "codigo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descripcion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "descripcion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ds_verificar_pago");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "ds_verificar_pago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", ">Respuesta>ds_verificar_pago"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
