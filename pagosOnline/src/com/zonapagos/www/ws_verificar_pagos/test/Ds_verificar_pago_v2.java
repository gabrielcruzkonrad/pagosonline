/**
 * Ds_verificar_pago_v2.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zonapagos.www.ws_verificar_pagos.test;

public class Ds_verificar_pago_v2  implements java.io.Serializable {
    private java.lang.String id_pago;

    private int id_tienda;

    private java.lang.String id_clave;

    public Ds_verificar_pago_v2() {
    }

    public Ds_verificar_pago_v2(
           java.lang.String id_pago,
           int id_tienda,
           java.lang.String id_clave) {
           this.id_pago = id_pago;
           this.id_tienda = id_tienda;
           this.id_clave = id_clave;
    }


    /**
     * Gets the id_pago value for this Ds_verificar_pago_v2.
     * 
     * @return id_pago
     */
    public java.lang.String getId_pago() {
        return id_pago;
    }


    /**
     * Sets the id_pago value for this Ds_verificar_pago_v2.
     * 
     * @param id_pago
     */
    public void setId_pago(java.lang.String id_pago) {
        this.id_pago = id_pago;
    }


    /**
     * Gets the id_tienda value for this Ds_verificar_pago_v2.
     * 
     * @return id_tienda
     */
    public int getId_tienda() {
        return id_tienda;
    }


    /**
     * Sets the id_tienda value for this Ds_verificar_pago_v2.
     * 
     * @param id_tienda
     */
    public void setId_tienda(int id_tienda) {
        this.id_tienda = id_tienda;
    }


    /**
     * Gets the id_clave value for this Ds_verificar_pago_v2.
     * 
     * @return id_clave
     */
    public java.lang.String getId_clave() {
        return id_clave;
    }


    /**
     * Sets the id_clave value for this Ds_verificar_pago_v2.
     * 
     * @param id_clave
     */
    public void setId_clave(java.lang.String id_clave) {
        this.id_clave = id_clave;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Ds_verificar_pago_v2)) return false;
        Ds_verificar_pago_v2 other = (Ds_verificar_pago_v2) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id_pago==null && other.getId_pago()==null) || 
             (this.id_pago!=null &&
              this.id_pago.equals(other.getId_pago()))) &&
            this.id_tienda == other.getId_tienda() &&
            ((this.id_clave==null && other.getId_clave()==null) || 
             (this.id_clave!=null &&
              this.id_clave.equals(other.getId_clave())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId_pago() != null) {
            _hashCode += getId_pago().hashCode();
        }
        _hashCode += getId_tienda();
        if (getId_clave() != null) {
            _hashCode += getId_clave().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Ds_verificar_pago_v2.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", ">ds_verificar_pago_v2"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_pago");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "id_pago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_tienda");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "id_tienda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_clave");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "id_clave"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
