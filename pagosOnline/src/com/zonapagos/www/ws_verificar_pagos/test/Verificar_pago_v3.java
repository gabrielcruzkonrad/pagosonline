/**
 * Verificar_pago_v3.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zonapagos.www.ws_verificar_pagos.test;

public class Verificar_pago_v3  implements java.io.Serializable {
    private java.lang.String str_id_pago;

    private int int_id_tienda;

    private java.lang.String str_id_clave;

    private com.zonapagos.www.ws_verificar_pagos.test.Pagos_v3[] res_pagos_v3;

    private int int_error;

    private java.lang.String str_error;

    public Verificar_pago_v3() {
    }

    public Verificar_pago_v3(
           java.lang.String str_id_pago,
           int int_id_tienda,
           java.lang.String str_id_clave,
           com.zonapagos.www.ws_verificar_pagos.test.Pagos_v3[] res_pagos_v3,
           int int_error,
           java.lang.String str_error) {
           this.str_id_pago = str_id_pago;
           this.int_id_tienda = int_id_tienda;
           this.str_id_clave = str_id_clave;
           this.res_pagos_v3 = res_pagos_v3;
           this.int_error = int_error;
           this.str_error = str_error;
    }


    /**
     * Gets the str_id_pago value for this Verificar_pago_v3.
     * 
     * @return str_id_pago
     */
    public java.lang.String getStr_id_pago() {
        return str_id_pago;
    }


    /**
     * Sets the str_id_pago value for this Verificar_pago_v3.
     * 
     * @param str_id_pago
     */
    public void setStr_id_pago(java.lang.String str_id_pago) {
        this.str_id_pago = str_id_pago;
    }


    /**
     * Gets the int_id_tienda value for this Verificar_pago_v3.
     * 
     * @return int_id_tienda
     */
    public int getInt_id_tienda() {
        return int_id_tienda;
    }


    /**
     * Sets the int_id_tienda value for this Verificar_pago_v3.
     * 
     * @param int_id_tienda
     */
    public void setInt_id_tienda(int int_id_tienda) {
        this.int_id_tienda = int_id_tienda;
    }


    /**
     * Gets the str_id_clave value for this Verificar_pago_v3.
     * 
     * @return str_id_clave
     */
    public java.lang.String getStr_id_clave() {
        return str_id_clave;
    }


    /**
     * Sets the str_id_clave value for this Verificar_pago_v3.
     * 
     * @param str_id_clave
     */
    public void setStr_id_clave(java.lang.String str_id_clave) {
        this.str_id_clave = str_id_clave;
    }


    /**
     * Gets the res_pagos_v3 value for this Verificar_pago_v3.
     * 
     * @return res_pagos_v3
     */
    public com.zonapagos.www.ws_verificar_pagos.test.Pagos_v3[] getRes_pagos_v3() {
        return res_pagos_v3;
    }


    /**
     * Sets the res_pagos_v3 value for this Verificar_pago_v3.
     * 
     * @param res_pagos_v3
     */
    public void setRes_pagos_v3(com.zonapagos.www.ws_verificar_pagos.test.Pagos_v3[] res_pagos_v3) {
        this.res_pagos_v3 = res_pagos_v3;
    }


    /**
     * Gets the int_error value for this Verificar_pago_v3.
     * 
     * @return int_error
     */
    public int getInt_error() {
        return int_error;
    }


    /**
     * Sets the int_error value for this Verificar_pago_v3.
     * 
     * @param int_error
     */
    public void setInt_error(int int_error) {
        this.int_error = int_error;
    }


    /**
     * Gets the str_error value for this Verificar_pago_v3.
     * 
     * @return str_error
     */
    public java.lang.String getStr_error() {
        return str_error;
    }


    /**
     * Sets the str_error value for this Verificar_pago_v3.
     * 
     * @param str_error
     */
    public void setStr_error(java.lang.String str_error) {
        this.str_error = str_error;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Verificar_pago_v3)) return false;
        Verificar_pago_v3 other = (Verificar_pago_v3) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.str_id_pago==null && other.getStr_id_pago()==null) || 
             (this.str_id_pago!=null &&
              this.str_id_pago.equals(other.getStr_id_pago()))) &&
            this.int_id_tienda == other.getInt_id_tienda() &&
            ((this.str_id_clave==null && other.getStr_id_clave()==null) || 
             (this.str_id_clave!=null &&
              this.str_id_clave.equals(other.getStr_id_clave()))) &&
            ((this.res_pagos_v3==null && other.getRes_pagos_v3()==null) || 
             (this.res_pagos_v3!=null &&
              java.util.Arrays.equals(this.res_pagos_v3, other.getRes_pagos_v3()))) &&
            this.int_error == other.getInt_error() &&
            ((this.str_error==null && other.getStr_error()==null) || 
             (this.str_error!=null &&
              this.str_error.equals(other.getStr_error())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStr_id_pago() != null) {
            _hashCode += getStr_id_pago().hashCode();
        }
        _hashCode += getInt_id_tienda();
        if (getStr_id_clave() != null) {
            _hashCode += getStr_id_clave().hashCode();
        }
        if (getRes_pagos_v3() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRes_pagos_v3());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRes_pagos_v3(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getInt_error();
        if (getStr_error() != null) {
            _hashCode += getStr_error().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Verificar_pago_v3.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", ">verificar_pago_v3"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("str_id_pago");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "str_id_pago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("int_id_tienda");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "int_id_tienda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("str_id_clave");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "str_id_clave"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("res_pagos_v3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "res_pagos_v3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "pagos_v3"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "pagos_v3"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("int_error");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "int_error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("str_error");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "str_error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
