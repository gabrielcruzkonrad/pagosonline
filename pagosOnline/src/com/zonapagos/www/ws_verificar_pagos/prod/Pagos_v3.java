/**
 * Pagos_v3.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zonapagos.www.ws_verificar_pagos.prod;

public class Pagos_v3  implements java.io.Serializable {
    private java.lang.String str_id_pago;

    private int int_estado_pago;

    private int int_id_forma_pago;

    private double dbl_valor_pagado;

    private java.lang.String str_ticketID;

    private java.lang.String str_id_clave;

    private java.lang.String str_id_cliente;

    private java.lang.String str_franquicia;

    private int int_cod_aprobacion;

    private int int_codigo_servico;

    private int int_codigo_banco;

    private java.lang.String str_nombre_banco;

    private java.lang.String str_codigo_transaccion;

    private int int_ciclo_transaccion;

    private java.lang.String str_campo1;

    private java.lang.String str_campo2;

    private java.lang.String str_campo3;

    private java.util.Calendar dat_fecha;

    public Pagos_v3() {
    }

    public Pagos_v3(
           java.lang.String str_id_pago,
           int int_estado_pago,
           int int_id_forma_pago,
           double dbl_valor_pagado,
           java.lang.String str_ticketID,
           java.lang.String str_id_clave,
           java.lang.String str_id_cliente,
           java.lang.String str_franquicia,
           int int_cod_aprobacion,
           int int_codigo_servico,
           int int_codigo_banco,
           java.lang.String str_nombre_banco,
           java.lang.String str_codigo_transaccion,
           int int_ciclo_transaccion,
           java.lang.String str_campo1,
           java.lang.String str_campo2,
           java.lang.String str_campo3,
           java.util.Calendar dat_fecha) {
           this.str_id_pago = str_id_pago;
           this.int_estado_pago = int_estado_pago;
           this.int_id_forma_pago = int_id_forma_pago;
           this.dbl_valor_pagado = dbl_valor_pagado;
           this.str_ticketID = str_ticketID;
           this.str_id_clave = str_id_clave;
           this.str_id_cliente = str_id_cliente;
           this.str_franquicia = str_franquicia;
           this.int_cod_aprobacion = int_cod_aprobacion;
           this.int_codigo_servico = int_codigo_servico;
           this.int_codigo_banco = int_codigo_banco;
           this.str_nombre_banco = str_nombre_banco;
           this.str_codigo_transaccion = str_codigo_transaccion;
           this.int_ciclo_transaccion = int_ciclo_transaccion;
           this.str_campo1 = str_campo1;
           this.str_campo2 = str_campo2;
           this.str_campo3 = str_campo3;
           this.dat_fecha = dat_fecha;
    }


    /**
     * Gets the str_id_pago value for this Pagos_v3.
     * 
     * @return str_id_pago
     */
    public java.lang.String getStr_id_pago() {
        return str_id_pago;
    }


    /**
     * Sets the str_id_pago value for this Pagos_v3.
     * 
     * @param str_id_pago
     */
    public void setStr_id_pago(java.lang.String str_id_pago) {
        this.str_id_pago = str_id_pago;
    }


    /**
     * Gets the int_estado_pago value for this Pagos_v3.
     * 
     * @return int_estado_pago
     */
    public int getInt_estado_pago() {
        return int_estado_pago;
    }


    /**
     * Sets the int_estado_pago value for this Pagos_v3.
     * 
     * @param int_estado_pago
     */
    public void setInt_estado_pago(int int_estado_pago) {
        this.int_estado_pago = int_estado_pago;
    }


    /**
     * Gets the int_id_forma_pago value for this Pagos_v3.
     * 
     * @return int_id_forma_pago
     */
    public int getInt_id_forma_pago() {
        return int_id_forma_pago;
    }


    /**
     * Sets the int_id_forma_pago value for this Pagos_v3.
     * 
     * @param int_id_forma_pago
     */
    public void setInt_id_forma_pago(int int_id_forma_pago) {
        this.int_id_forma_pago = int_id_forma_pago;
    }


    /**
     * Gets the dbl_valor_pagado value for this Pagos_v3.
     * 
     * @return dbl_valor_pagado
     */
    public double getDbl_valor_pagado() {
        return dbl_valor_pagado;
    }


    /**
     * Sets the dbl_valor_pagado value for this Pagos_v3.
     * 
     * @param dbl_valor_pagado
     */
    public void setDbl_valor_pagado(double dbl_valor_pagado) {
        this.dbl_valor_pagado = dbl_valor_pagado;
    }


    /**
     * Gets the str_ticketID value for this Pagos_v3.
     * 
     * @return str_ticketID
     */
    public java.lang.String getStr_ticketID() {
        return str_ticketID;
    }


    /**
     * Sets the str_ticketID value for this Pagos_v3.
     * 
     * @param str_ticketID
     */
    public void setStr_ticketID(java.lang.String str_ticketID) {
        this.str_ticketID = str_ticketID;
    }


    /**
     * Gets the str_id_clave value for this Pagos_v3.
     * 
     * @return str_id_clave
     */
    public java.lang.String getStr_id_clave() {
        return str_id_clave;
    }


    /**
     * Sets the str_id_clave value for this Pagos_v3.
     * 
     * @param str_id_clave
     */
    public void setStr_id_clave(java.lang.String str_id_clave) {
        this.str_id_clave = str_id_clave;
    }


    /**
     * Gets the str_id_cliente value for this Pagos_v3.
     * 
     * @return str_id_cliente
     */
    public java.lang.String getStr_id_cliente() {
        return str_id_cliente;
    }


    /**
     * Sets the str_id_cliente value for this Pagos_v3.
     * 
     * @param str_id_cliente
     */
    public void setStr_id_cliente(java.lang.String str_id_cliente) {
        this.str_id_cliente = str_id_cliente;
    }


    /**
     * Gets the str_franquicia value for this Pagos_v3.
     * 
     * @return str_franquicia
     */
    public java.lang.String getStr_franquicia() {
        return str_franquicia;
    }


    /**
     * Sets the str_franquicia value for this Pagos_v3.
     * 
     * @param str_franquicia
     */
    public void setStr_franquicia(java.lang.String str_franquicia) {
        this.str_franquicia = str_franquicia;
    }


    /**
     * Gets the int_cod_aprobacion value for this Pagos_v3.
     * 
     * @return int_cod_aprobacion
     */
    public int getInt_cod_aprobacion() {
        return int_cod_aprobacion;
    }


    /**
     * Sets the int_cod_aprobacion value for this Pagos_v3.
     * 
     * @param int_cod_aprobacion
     */
    public void setInt_cod_aprobacion(int int_cod_aprobacion) {
        this.int_cod_aprobacion = int_cod_aprobacion;
    }


    /**
     * Gets the int_codigo_servico value for this Pagos_v3.
     * 
     * @return int_codigo_servico
     */
    public int getInt_codigo_servico() {
        return int_codigo_servico;
    }


    /**
     * Sets the int_codigo_servico value for this Pagos_v3.
     * 
     * @param int_codigo_servico
     */
    public void setInt_codigo_servico(int int_codigo_servico) {
        this.int_codigo_servico = int_codigo_servico;
    }


    /**
     * Gets the int_codigo_banco value for this Pagos_v3.
     * 
     * @return int_codigo_banco
     */
    public int getInt_codigo_banco() {
        return int_codigo_banco;
    }


    /**
     * Sets the int_codigo_banco value for this Pagos_v3.
     * 
     * @param int_codigo_banco
     */
    public void setInt_codigo_banco(int int_codigo_banco) {
        this.int_codigo_banco = int_codigo_banco;
    }


    /**
     * Gets the str_nombre_banco value for this Pagos_v3.
     * 
     * @return str_nombre_banco
     */
    public java.lang.String getStr_nombre_banco() {
        return str_nombre_banco;
    }


    /**
     * Sets the str_nombre_banco value for this Pagos_v3.
     * 
     * @param str_nombre_banco
     */
    public void setStr_nombre_banco(java.lang.String str_nombre_banco) {
        this.str_nombre_banco = str_nombre_banco;
    }


    /**
     * Gets the str_codigo_transaccion value for this Pagos_v3.
     * 
     * @return str_codigo_transaccion
     */
    public java.lang.String getStr_codigo_transaccion() {
        return str_codigo_transaccion;
    }


    /**
     * Sets the str_codigo_transaccion value for this Pagos_v3.
     * 
     * @param str_codigo_transaccion
     */
    public void setStr_codigo_transaccion(java.lang.String str_codigo_transaccion) {
        this.str_codigo_transaccion = str_codigo_transaccion;
    }


    /**
     * Gets the int_ciclo_transaccion value for this Pagos_v3.
     * 
     * @return int_ciclo_transaccion
     */
    public int getInt_ciclo_transaccion() {
        return int_ciclo_transaccion;
    }


    /**
     * Sets the int_ciclo_transaccion value for this Pagos_v3.
     * 
     * @param int_ciclo_transaccion
     */
    public void setInt_ciclo_transaccion(int int_ciclo_transaccion) {
        this.int_ciclo_transaccion = int_ciclo_transaccion;
    }


    /**
     * Gets the str_campo1 value for this Pagos_v3.
     * 
     * @return str_campo1
     */
    public java.lang.String getStr_campo1() {
        return str_campo1;
    }


    /**
     * Sets the str_campo1 value for this Pagos_v3.
     * 
     * @param str_campo1
     */
    public void setStr_campo1(java.lang.String str_campo1) {
        this.str_campo1 = str_campo1;
    }


    /**
     * Gets the str_campo2 value for this Pagos_v3.
     * 
     * @return str_campo2
     */
    public java.lang.String getStr_campo2() {
        return str_campo2;
    }


    /**
     * Sets the str_campo2 value for this Pagos_v3.
     * 
     * @param str_campo2
     */
    public void setStr_campo2(java.lang.String str_campo2) {
        this.str_campo2 = str_campo2;
    }


    /**
     * Gets the str_campo3 value for this Pagos_v3.
     * 
     * @return str_campo3
     */
    public java.lang.String getStr_campo3() {
        return str_campo3;
    }


    /**
     * Sets the str_campo3 value for this Pagos_v3.
     * 
     * @param str_campo3
     */
    public void setStr_campo3(java.lang.String str_campo3) {
        this.str_campo3 = str_campo3;
    }


    /**
     * Gets the dat_fecha value for this Pagos_v3.
     * 
     * @return dat_fecha
     */
    public java.util.Calendar getDat_fecha() {
        return dat_fecha;
    }


    /**
     * Sets the dat_fecha value for this Pagos_v3.
     * 
     * @param dat_fecha
     */
    public void setDat_fecha(java.util.Calendar dat_fecha) {
        this.dat_fecha = dat_fecha;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Pagos_v3)) return false;
        Pagos_v3 other = (Pagos_v3) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.str_id_pago==null && other.getStr_id_pago()==null) || 
             (this.str_id_pago!=null &&
              this.str_id_pago.equals(other.getStr_id_pago()))) &&
            this.int_estado_pago == other.getInt_estado_pago() &&
            this.int_id_forma_pago == other.getInt_id_forma_pago() &&
            this.dbl_valor_pagado == other.getDbl_valor_pagado() &&
            ((this.str_ticketID==null && other.getStr_ticketID()==null) || 
             (this.str_ticketID!=null &&
              this.str_ticketID.equals(other.getStr_ticketID()))) &&
            ((this.str_id_clave==null && other.getStr_id_clave()==null) || 
             (this.str_id_clave!=null &&
              this.str_id_clave.equals(other.getStr_id_clave()))) &&
            ((this.str_id_cliente==null && other.getStr_id_cliente()==null) || 
             (this.str_id_cliente!=null &&
              this.str_id_cliente.equals(other.getStr_id_cliente()))) &&
            ((this.str_franquicia==null && other.getStr_franquicia()==null) || 
             (this.str_franquicia!=null &&
              this.str_franquicia.equals(other.getStr_franquicia()))) &&
            this.int_cod_aprobacion == other.getInt_cod_aprobacion() &&
            this.int_codigo_servico == other.getInt_codigo_servico() &&
            this.int_codigo_banco == other.getInt_codigo_banco() &&
            ((this.str_nombre_banco==null && other.getStr_nombre_banco()==null) || 
             (this.str_nombre_banco!=null &&
              this.str_nombre_banco.equals(other.getStr_nombre_banco()))) &&
            ((this.str_codigo_transaccion==null && other.getStr_codigo_transaccion()==null) || 
             (this.str_codigo_transaccion!=null &&
              this.str_codigo_transaccion.equals(other.getStr_codigo_transaccion()))) &&
            this.int_ciclo_transaccion == other.getInt_ciclo_transaccion() &&
            ((this.str_campo1==null && other.getStr_campo1()==null) || 
             (this.str_campo1!=null &&
              this.str_campo1.equals(other.getStr_campo1()))) &&
            ((this.str_campo2==null && other.getStr_campo2()==null) || 
             (this.str_campo2!=null &&
              this.str_campo2.equals(other.getStr_campo2()))) &&
            ((this.str_campo3==null && other.getStr_campo3()==null) || 
             (this.str_campo3!=null &&
              this.str_campo3.equals(other.getStr_campo3()))) &&
            ((this.dat_fecha==null && other.getDat_fecha()==null) || 
             (this.dat_fecha!=null &&
              this.dat_fecha.equals(other.getDat_fecha())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStr_id_pago() != null) {
            _hashCode += getStr_id_pago().hashCode();
        }
        _hashCode += getInt_estado_pago();
        _hashCode += getInt_id_forma_pago();
        _hashCode += new Double(getDbl_valor_pagado()).hashCode();
        if (getStr_ticketID() != null) {
            _hashCode += getStr_ticketID().hashCode();
        }
        if (getStr_id_clave() != null) {
            _hashCode += getStr_id_clave().hashCode();
        }
        if (getStr_id_cliente() != null) {
            _hashCode += getStr_id_cliente().hashCode();
        }
        if (getStr_franquicia() != null) {
            _hashCode += getStr_franquicia().hashCode();
        }
        _hashCode += getInt_cod_aprobacion();
        _hashCode += getInt_codigo_servico();
        _hashCode += getInt_codigo_banco();
        if (getStr_nombre_banco() != null) {
            _hashCode += getStr_nombre_banco().hashCode();
        }
        if (getStr_codigo_transaccion() != null) {
            _hashCode += getStr_codigo_transaccion().hashCode();
        }
        _hashCode += getInt_ciclo_transaccion();
        if (getStr_campo1() != null) {
            _hashCode += getStr_campo1().hashCode();
        }
        if (getStr_campo2() != null) {
            _hashCode += getStr_campo2().hashCode();
        }
        if (getStr_campo3() != null) {
            _hashCode += getStr_campo3().hashCode();
        }
        if (getDat_fecha() != null) {
            _hashCode += getDat_fecha().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Pagos_v3.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "pagos_v3"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("str_id_pago");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "str_id_pago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("int_estado_pago");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "int_estado_pago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("int_id_forma_pago");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "int_id_forma_pago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dbl_valor_pagado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "dbl_valor_pagado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("str_ticketID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "str_ticketID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("str_id_clave");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "str_id_clave"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("str_id_cliente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "str_id_cliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("str_franquicia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "str_franquicia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("int_cod_aprobacion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "int_cod_aprobacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("int_codigo_servico");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "int_codigo_servico"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("int_codigo_banco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "int_codigo_banco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("str_nombre_banco");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "str_nombre_banco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("str_codigo_transaccion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "str_codigo_transaccion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("int_ciclo_transaccion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "int_ciclo_transaccion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("str_campo1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "str_campo1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("str_campo2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "str_campo2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("str_campo3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "str_campo3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dat_fecha");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "dat_fecha"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
