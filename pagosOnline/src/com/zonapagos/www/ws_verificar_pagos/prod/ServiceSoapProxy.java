package com.zonapagos.www.ws_verificar_pagos.prod;

public class ServiceSoapProxy implements com.zonapagos.www.ws_verificar_pagos.prod.ServiceSoap {
  private String _endpoint = null;
  private com.zonapagos.www.ws_verificar_pagos.prod.ServiceSoap serviceSoap = null;
  
  public ServiceSoapProxy() {
    _initServiceSoapProxy();
  }
  
  public ServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initServiceSoapProxy();
  }
  
  private void _initServiceSoapProxy() {
    try {
      serviceSoap = (new com.zonapagos.www.ws_verificar_pagos.prod.ServiceLocator()).getServiceSoap();
      if (serviceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)serviceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)serviceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (serviceSoap != null)
      ((javax.xml.rpc.Stub)serviceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.zonapagos.www.ws_verificar_pagos.prod.ServiceSoap getServiceSoap() {
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap;
  }
  
  public com.zonapagos.www.ws_verificar_pagos.prod.Respuesta ds_verificar_pago(java.lang.String id_pago, int id_tienda, java.lang.String id_clave) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.ds_verificar_pago(id_pago, id_tienda, id_clave);
  }
  
  public com.zonapagos.www.ws_verificar_pagos.prod.Respuesta ds_verificar_pago_v2(java.lang.String id_pago, int id_tienda, java.lang.String id_clave) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.ds_verificar_pago_v2(id_pago, id_tienda, id_clave);
  }
  
  public int verificar_pago_v3(java.lang.String str_id_pago, int int_id_tienda, java.lang.String str_id_clave, com.zonapagos.www.ws_verificar_pagos.holders.prod.ArrayOfPagos_v3Holder res_pagos_v3, javax.xml.rpc.holders.IntHolder int_error, javax.xml.rpc.holders.StringHolder str_error) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.verificar_pago_v3(str_id_pago, int_id_tienda, str_id_clave, res_pagos_v3, int_error, str_error);
  }
  
  
}