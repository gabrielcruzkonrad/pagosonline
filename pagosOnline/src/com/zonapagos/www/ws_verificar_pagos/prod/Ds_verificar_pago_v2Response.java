/**
 * Ds_verificar_pago_v2Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zonapagos.www.ws_verificar_pagos.prod;

public class Ds_verificar_pago_v2Response  implements java.io.Serializable {
    private com.zonapagos.www.ws_verificar_pagos.prod.Respuesta ds_verificar_pago_v2Result;

    public Ds_verificar_pago_v2Response() {
    }

    public Ds_verificar_pago_v2Response(
           com.zonapagos.www.ws_verificar_pagos.prod.Respuesta ds_verificar_pago_v2Result) {
           this.ds_verificar_pago_v2Result = ds_verificar_pago_v2Result;
    }


    /**
     * Gets the ds_verificar_pago_v2Result value for this Ds_verificar_pago_v2Response.
     * 
     * @return ds_verificar_pago_v2Result
     */
    public com.zonapagos.www.ws_verificar_pagos.prod.Respuesta getDs_verificar_pago_v2Result() {
        return ds_verificar_pago_v2Result;
    }


    /**
     * Sets the ds_verificar_pago_v2Result value for this Ds_verificar_pago_v2Response.
     * 
     * @param ds_verificar_pago_v2Result
     */
    public void setDs_verificar_pago_v2Result(com.zonapagos.www.ws_verificar_pagos.prod.Respuesta ds_verificar_pago_v2Result) {
        this.ds_verificar_pago_v2Result = ds_verificar_pago_v2Result;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Ds_verificar_pago_v2Response)) return false;
        Ds_verificar_pago_v2Response other = (Ds_verificar_pago_v2Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ds_verificar_pago_v2Result==null && other.getDs_verificar_pago_v2Result()==null) || 
             (this.ds_verificar_pago_v2Result!=null &&
              this.ds_verificar_pago_v2Result.equals(other.getDs_verificar_pago_v2Result())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDs_verificar_pago_v2Result() != null) {
            _hashCode += getDs_verificar_pago_v2Result().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Ds_verificar_pago_v2Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", ">ds_verificar_pago_v2Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ds_verificar_pago_v2Result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "ds_verificar_pago_v2Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.zonapagos.com/ws_verificar_pagos", "Respuesta"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
