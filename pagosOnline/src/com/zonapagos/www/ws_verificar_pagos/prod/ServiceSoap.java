/**
 * ServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zonapagos.www.ws_verificar_pagos.prod;

public interface ServiceSoap extends java.rmi.Remote {
    public com.zonapagos.www.ws_verificar_pagos.prod.Respuesta ds_verificar_pago(java.lang.String id_pago, int id_tienda, java.lang.String id_clave) throws java.rmi.RemoteException;
    public com.zonapagos.www.ws_verificar_pagos.prod.Respuesta ds_verificar_pago_v2(java.lang.String id_pago, int id_tienda, java.lang.String id_clave) throws java.rmi.RemoteException;
    public int verificar_pago_v3(java.lang.String str_id_pago, int int_id_tienda, java.lang.String str_id_clave, com.zonapagos.www.ws_verificar_pagos.holders.prod.ArrayOfPagos_v3Holder res_pagos_v3, javax.xml.rpc.holders.IntHolder int_error, javax.xml.rpc.holders.StringHolder str_error) throws java.rmi.RemoteException;
}
