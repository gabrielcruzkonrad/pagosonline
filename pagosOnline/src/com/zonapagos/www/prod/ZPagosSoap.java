/**
 * ZPagosSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zonapagos.www.prod;

public interface ZPagosSoap extends java.rmi.Remote {
    public java.lang.String inicio_pagoV2(int id_tienda, java.lang.String clave, double total_con_iva, double valor_iva, java.lang.String id_pago, java.lang.String descripcion_pago, java.lang.String email, java.lang.String id_cliente, java.lang.String tipo_id, java.lang.String nombre_cliente, java.lang.String apellido_cliente, java.lang.String telefono_cliente, java.lang.String info_opcional1, java.lang.String info_opcional2, java.lang.String info_opcional3, java.lang.String codigo_servicio_principal, java.lang.String[] lista_codigos_servicio_multicredito, java.lang.String[] lista_nit_codigos_servicio_multicredito, double[] lista_valores_con_iva, double[] lista_valores_iva, int total_codigos_servicio) throws java.rmi.RemoteException;
    public java.lang.String inicio_pagoV3(int id_tienda, java.lang.String clave, double total_con_iva, double valor_iva, java.lang.String id_pago, java.lang.String descripcion_pago, java.lang.String email, java.lang.String id_cliente, java.lang.String tipo_id, java.lang.String nombre_cliente, java.lang.String apellido_cliente, java.lang.String telefono_cliente, java.lang.String info_opcional1, java.lang.String info_opcional2, java.lang.String info_opcional3, java.lang.String codigo_servicio_principal, java.lang.String[] lista_codigos_servicio_multicredito, java.lang.String[] lista_nit_codigos_servicio_multicredito, double[] lista_valores_con_iva, double[] lista_valores_iva, int total_codigos_servicio, java.lang.String archivo_adjunto_nombre_original, java.lang.String archivo_adjunto_nombre_guardado, java.lang.String[] lista_info_opcional) throws java.rmi.RemoteException;
    public java.lang.String inicio_pagoV4(int id_tienda, java.lang.String clave, double total_con_iva, double valor_iva, java.lang.String id_pago, java.lang.String descripcion_pago, java.lang.String email, java.lang.String id_cliente, java.lang.String tipo_id, java.lang.String nombre_cliente, java.lang.String apellido_cliente, java.lang.String telefono_cliente, java.lang.String info_opcional1, java.lang.String info_opcional2, java.lang.String info_opcional3, java.lang.String codigo_servicio_principal, java.lang.String[] lista_codigos_servicio_multicredito, java.lang.String[] lista_nit_codigos_servicio_multicredito, double[] lista_valores_con_iva, double[] lista_valores_iva, int total_codigos_servicio, java.lang.String lista_info_opcional_codigo, java.lang.String lista_info_opcional_valor, com.zonapagos.www.holders.prod.ArrayOfStringHolder lista_respuesta) throws java.rmi.RemoteException;
}
