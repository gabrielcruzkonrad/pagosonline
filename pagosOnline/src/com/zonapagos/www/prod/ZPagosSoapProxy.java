package com.zonapagos.www.prod;

public class ZPagosSoapProxy implements com.zonapagos.www.prod.ZPagosSoap {
  private String _endpoint = null;
  private com.zonapagos.www.prod.ZPagosSoap zPagosSoap = null;
  
  public ZPagosSoapProxy() {
    _initZPagosSoapProxy();
  }
  
  public ZPagosSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initZPagosSoapProxy();
  }
  
  private void _initZPagosSoapProxy() {
    try {
      zPagosSoap = (new com.zonapagos.www.prod.ZPagosLocator()).getZPagosSoap();
      if (zPagosSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)zPagosSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)zPagosSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (zPagosSoap != null)
      ((javax.xml.rpc.Stub)zPagosSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.zonapagos.www.prod.ZPagosSoap getZPagosSoap() {
    if (zPagosSoap == null)
      _initZPagosSoapProxy();
    return zPagosSoap;
  }
  
  public java.lang.String inicio_pagoV2(int id_tienda, java.lang.String clave, double total_con_iva, double valor_iva, java.lang.String id_pago, java.lang.String descripcion_pago, java.lang.String email, java.lang.String id_cliente, java.lang.String tipo_id, java.lang.String nombre_cliente, java.lang.String apellido_cliente, java.lang.String telefono_cliente, java.lang.String info_opcional1, java.lang.String info_opcional2, java.lang.String info_opcional3, java.lang.String codigo_servicio_principal, java.lang.String[] lista_codigos_servicio_multicredito, java.lang.String[] lista_nit_codigos_servicio_multicredito, double[] lista_valores_con_iva, double[] lista_valores_iva, int total_codigos_servicio) throws java.rmi.RemoteException{
    if (zPagosSoap == null)
      _initZPagosSoapProxy();
    return zPagosSoap.inicio_pagoV2(id_tienda, clave, total_con_iva, valor_iva, id_pago, descripcion_pago, email, id_cliente, tipo_id, nombre_cliente, apellido_cliente, telefono_cliente, info_opcional1, info_opcional2, info_opcional3, codigo_servicio_principal, lista_codigos_servicio_multicredito, lista_nit_codigos_servicio_multicredito, lista_valores_con_iva, lista_valores_iva, total_codigos_servicio);
  }
  
  public java.lang.String inicio_pagoV3(int id_tienda, java.lang.String clave, double total_con_iva, double valor_iva, java.lang.String id_pago, java.lang.String descripcion_pago, java.lang.String email, java.lang.String id_cliente, java.lang.String tipo_id, java.lang.String nombre_cliente, java.lang.String apellido_cliente, java.lang.String telefono_cliente, java.lang.String info_opcional1, java.lang.String info_opcional2, java.lang.String info_opcional3, java.lang.String codigo_servicio_principal, java.lang.String[] lista_codigos_servicio_multicredito, java.lang.String[] lista_nit_codigos_servicio_multicredito, double[] lista_valores_con_iva, double[] lista_valores_iva, int total_codigos_servicio, java.lang.String archivo_adjunto_nombre_original, java.lang.String archivo_adjunto_nombre_guardado, java.lang.String[] lista_info_opcional) throws java.rmi.RemoteException{
    if (zPagosSoap == null)
      _initZPagosSoapProxy();
    return zPagosSoap.inicio_pagoV3(id_tienda, clave, total_con_iva, valor_iva, id_pago, descripcion_pago, email, id_cliente, tipo_id, nombre_cliente, apellido_cliente, telefono_cliente, info_opcional1, info_opcional2, info_opcional3, codigo_servicio_principal, lista_codigos_servicio_multicredito, lista_nit_codigos_servicio_multicredito, lista_valores_con_iva, lista_valores_iva, total_codigos_servicio, archivo_adjunto_nombre_original, archivo_adjunto_nombre_guardado, lista_info_opcional);
  }
  
  public java.lang.String inicio_pagoV4(int id_tienda, java.lang.String clave, double total_con_iva, double valor_iva, java.lang.String id_pago, java.lang.String descripcion_pago, java.lang.String email, java.lang.String id_cliente, java.lang.String tipo_id, java.lang.String nombre_cliente, java.lang.String apellido_cliente, java.lang.String telefono_cliente, java.lang.String info_opcional1, java.lang.String info_opcional2, java.lang.String info_opcional3, java.lang.String codigo_servicio_principal, java.lang.String[] lista_codigos_servicio_multicredito, java.lang.String[] lista_nit_codigos_servicio_multicredito, double[] lista_valores_con_iva, double[] lista_valores_iva, int total_codigos_servicio, java.lang.String lista_info_opcional_codigo, java.lang.String lista_info_opcional_valor, com.zonapagos.www.holders.prod.ArrayOfStringHolder lista_respuesta) throws java.rmi.RemoteException{
    if (zPagosSoap == null)
      _initZPagosSoapProxy();
    return zPagosSoap.inicio_pagoV4(id_tienda, clave, total_con_iva, valor_iva, id_pago, descripcion_pago, email, id_cliente, tipo_id, nombre_cliente, apellido_cliente, telefono_cliente, info_opcional1, info_opcional2, info_opcional3, codigo_servicio_principal, lista_codigos_servicio_multicredito, lista_nit_codigos_servicio_multicredito, lista_valores_con_iva, lista_valores_iva, total_codigos_servicio, lista_info_opcional_codigo, lista_info_opcional_valor, lista_respuesta);
  }
  
  
}