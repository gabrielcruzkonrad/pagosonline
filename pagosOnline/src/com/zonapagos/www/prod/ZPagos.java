/**
 * ZPagos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zonapagos.www.prod;

public interface ZPagos extends javax.xml.rpc.Service {
    public java.lang.String getZPagosSoapAddress();

    public com.zonapagos.www.prod.ZPagosSoap getZPagosSoap() throws javax.xml.rpc.ServiceException;

    public com.zonapagos.www.prod.ZPagosSoap getZPagosSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
