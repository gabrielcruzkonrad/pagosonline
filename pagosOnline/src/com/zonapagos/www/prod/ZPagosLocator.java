/**
 * ZPagosLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zonapagos.www.prod;

public class ZPagosLocator extends org.apache.axis.client.Service implements com.zonapagos.www.prod.ZPagos {

    public ZPagosLocator() {
    }


    public ZPagosLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ZPagosLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ZPagosSoap
    private java.lang.String ZPagosSoap_address = "https://www.zonapagos.com/ws_inicio_pagov2/Zpagos.asmx";

    public java.lang.String getZPagosSoapAddress() {
        return ZPagosSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ZPagosSoapWSDDServiceName = "ZPagosSoap";

    public java.lang.String getZPagosSoapWSDDServiceName() {
        return ZPagosSoapWSDDServiceName;
    }

    public void setZPagosSoapWSDDServiceName(java.lang.String name) {
        ZPagosSoapWSDDServiceName = name;
    }

    public com.zonapagos.www.prod.ZPagosSoap getZPagosSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ZPagosSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getZPagosSoap(endpoint);
    }

    public com.zonapagos.www.prod.ZPagosSoap getZPagosSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.zonapagos.www.prod.ZPagosSoapStub _stub = new com.zonapagos.www.prod.ZPagosSoapStub(portAddress, this);
            _stub.setPortName(getZPagosSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setZPagosSoapEndpointAddress(java.lang.String address) {
        ZPagosSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.zonapagos.www.prod.ZPagosSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.zonapagos.www.prod.ZPagosSoapStub _stub = new com.zonapagos.www.prod.ZPagosSoapStub(new java.net.URL(ZPagosSoap_address), this);
                _stub.setPortName(getZPagosSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ZPagosSoap".equals(inputPortName)) {
            return getZPagosSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.zonapagos.com", "ZPagos");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.zonapagos.com", "ZPagosSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ZPagosSoap".equals(portName)) {
            setZPagosSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
