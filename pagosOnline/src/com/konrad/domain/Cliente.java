package com.konrad.domain;

public class Cliente {
	private Long cliente;
	private String nombreNegocio;
	private String direccion;
	private String telefono;
	private Persona persona;
	private String direccionElectronica;
	private String fax;
	private String direccionEntrega;
	private String servidorCorreo;
	private EstadoCliente estadoCliente;
	private String egresado;
	private String estudianteActivo;
	private CentroCosto centroCosto;
	private ProgramaAcademico programa;
	private PeriodoFacturacion periodo;
	private Long codigoAnterior;
	private Canal canal;
	
	public Cliente() {
		super();	
	}
	
	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEgresado() {
		return egresado;
	}

	public void setEgresado(String egresado) {
		this.egresado = egresado;
	}

	public String getDireccionEntrega() {
		return direccionEntrega;
	}

	public void setDireccionEntrega(String direccionEntrega) {
		this.direccionEntrega = direccionEntrega;
	}

	public String getServidorCorreo() {
		return servidorCorreo;
	}

	public void setServidorCorreo(String servidorCorreo) {
		this.servidorCorreo = servidorCorreo;
	}

	public Long getCliente() {
		return cliente;
	}
	public void setCliente(Long cliente) {
		this.cliente = cliente;
	}
	public String getNombreNegocio() {
		return nombreNegocio;
	}
	public void setNombreNegocio(String nombreNegocio) {
		this.nombreNegocio = nombreNegocio;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getDireccionElectronica() {
		return direccionElectronica;
	}
	public void setDireccionElectronica(String direccionElectronica) {
		this.direccionElectronica = direccionElectronica;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public EstadoCliente getEstadoCliente() {
		return estadoCliente;
	}

	public void setEstadoCliente(EstadoCliente estadoCliente) {
		this.estadoCliente = estadoCliente;
	}

	public String getEstudianteActivo() {
		return estudianteActivo;
	}

	public void setEstudianteActivo(String estudianteActivo) {
		this.estudianteActivo = estudianteActivo;
	}

	public CentroCosto getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(CentroCosto centroCosto) {
		this.centroCosto = centroCosto;
	}

	public ProgramaAcademico getPrograma() {
		return programa;
	}

	public void setPrograma(ProgramaAcademico programa) {
		this.programa = programa;
	}

	public PeriodoFacturacion getPeriodo() {
		return periodo;
	}

	public void setPeriodo(PeriodoFacturacion periodo) {
		this.periodo = periodo;
	}

	public Long getCodigoAnterior() {
		return codigoAnterior;
	}

	public void setCodigoAnterior(Long codigoAnterior) {
		this.codigoAnterior = codigoAnterior;
	}

	public Canal getCanal() {
		return canal;
	}

	public void setCanal(Canal canal) {
		this.canal = canal;
	}
	
	
}
