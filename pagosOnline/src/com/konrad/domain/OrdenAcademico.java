package com.konrad.domain;

import java.util.Date;

public class OrdenAcademico {

	private Long idOrden;
	private Estudiante estudiante;
	private PeriodoAcademico periodoAcademico;
	private Date fechaLimite;
	private Date fechaExtraordinaria;
	private Date fechaExtemporanea;
	private Double valorTotal;
	private Long idOrdenPago;
	private Long idFormularioPago;
	private Date fechaHizoPago;
	
	public OrdenAcademico(Long idOrden, Estudiante estudiante,
			PeriodoAcademico periodoAcademico, Date fechaLimite,
			Date fechaExtraordinaria, Date fechaExtemporanea,
			Double valorTotal, Long idOrdenPago, Long idFormularioPago,
			Date fechaHizoPago) {
		super();
		this.idOrden = idOrden;
		this.estudiante = estudiante;
		this.periodoAcademico = periodoAcademico;
		this.fechaLimite = fechaLimite;
		this.fechaExtraordinaria = fechaExtraordinaria;
		this.fechaExtemporanea = fechaExtemporanea;
		this.valorTotal = valorTotal;
		this.idOrdenPago = idOrdenPago;
		this.idFormularioPago = idFormularioPago;
		this.fechaHizoPago = fechaHizoPago;
	}

	public OrdenAcademico() {
		// TODO Auto-generated constructor stub
	}

	public Long getIdOrden() {
		return idOrden;
	}

	public void setIdOrden(Long idOrden) {
		this.idOrden = idOrden;
	}

	public Estudiante getEstudiante() {
		return estudiante;
	}

	public void setEstudiante(Estudiante estudiante) {
		this.estudiante = estudiante;
	}

	public PeriodoAcademico getPeriodoAcademico() {
		return periodoAcademico;
	}

	public void setPeriodoAcademico(PeriodoAcademico periodoAcademico) {
		this.periodoAcademico = periodoAcademico;
	}

	public Date getFechaLimite() {
		return fechaLimite;
	}

	public void setFechaLimite(Date fechaLimite) {
		this.fechaLimite = fechaLimite;
	}

	public Date getFechaExtraordinaria() {
		return fechaExtraordinaria;
	}

	public void setFechaExtraordinaria(Date fechaExtraordinaria) {
		this.fechaExtraordinaria = fechaExtraordinaria;
	}

	public Date getFechaExtemporanea() {
		return fechaExtemporanea;
	}

	public void setFechaExtemporanea(Date fechaExtemporanea) {
		this.fechaExtemporanea = fechaExtemporanea;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Long getIdOrdenPago() {
		return idOrdenPago;
	}

	public void setIdOrdenPago(Long idOrdenPago) {
		this.idOrdenPago = idOrdenPago;
	}

	public Long getIdFormularioPago() {
		return idFormularioPago;
	}

	public void setIdFormularioPago(Long idFormularioPago) {
		this.idFormularioPago = idFormularioPago;
	}

	public Date getFechaHizoPago() {
		return fechaHizoPago;
	}

	public void setFechaHizoPago(Date fechaHizoPago) {
		this.fechaHizoPago = fechaHizoPago;
	}
	
	

}
