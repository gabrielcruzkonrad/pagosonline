package com.konrad.domain;

public class ProgramaAcademico {

	
	private String idPrograma; 
	private String nombrePrograma;
	private String codigoPrograma;
	private TipoPrograma tipoPrograma;
	private Facultad facultad;
	private CentroCosto centroCosto;
	private String periodo;
	private String descuento;
	private String porcentaje;
	private Integer cupoPrograma;
	
	
	public ProgramaAcademico() {
		// TODO Auto-generated constructor stub
	}

	public ProgramaAcademico(String idPrograma, String nombrePrograma,
			TipoPrograma tipoPrograma, Facultad facultad, CentroCosto centroCosto, String periodo) {
		super();
		this.idPrograma = idPrograma;
		this.nombrePrograma = nombrePrograma;
		this.tipoPrograma = tipoPrograma;
		this.facultad = facultad;
		this.centroCosto = centroCosto;
		this.periodo = periodo;
	}

	public Facultad getFacultad() {
		return facultad;
	}

	public void setFacultad(Facultad facultad) {
		this.facultad = facultad;
	}

	public CentroCosto getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(CentroCosto centroCosto) {
		this.centroCosto = centroCosto;
	}

	public String getIdPrograma() {
		return idPrograma;
	}

	public void setIdPrograma(String idPrograma) {
		this.idPrograma = idPrograma;
	}

	public String getNombrePrograma() {
		return nombrePrograma;
	}

	public void setNombrePrograma(String nombrePrograma) {
		this.nombrePrograma = nombrePrograma;
	}

	public TipoPrograma getTipoPrograma() {
		return tipoPrograma;
	}

	public void setTipoPrograma(TipoPrograma tipoPrograma) {
		this.tipoPrograma = tipoPrograma;
	}

	public String getCodigoPrograma() {
		return codigoPrograma;
	}

	public void setCodigoPrograma(String codigoPrograma) {
		this.codigoPrograma = codigoPrograma;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getDescuento() {
		return descuento;
	}

	public void setDescuento(String descuento) {
		this.descuento = descuento;
	}

	public String getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(String porcentaje) {
		this.porcentaje = porcentaje;
	}

	public Integer getCupoPrograma() {
		return cupoPrograma;
	}

	public void setCupoPrograma(Integer cupoPrograma) {
		this.cupoPrograma = cupoPrograma;
	}

}
