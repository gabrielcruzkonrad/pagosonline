package com.konrad.domain;

public class CausaNota {
	
	private Integer causaNota;
	private String nombreCausaNota;
	
	public CausaNota() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public CausaNota(int causaNota, String nombreCausaNota) {
		super();
		this.causaNota = causaNota;
		this.nombreCausaNota = nombreCausaNota;
	}

	public int getCausaNota() {
		return causaNota;
	}
	public void setCausaNota(int causaNota) {
		this.causaNota = causaNota;
	}
	public String getNombreCausaNota() {
		return nombreCausaNota;
	}
	public void setNombreCausaNota(String nombreCausaNota) {
		this.nombreCausaNota = nombreCausaNota;
	}
	
	
}
