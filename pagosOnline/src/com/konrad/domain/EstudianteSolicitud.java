package com.konrad.domain;

import java.util.Date;

public class EstudianteSolicitud {

	private Long idEstudianteSolicitud;
	private Estudiante estudiante;
	private PeriodoAcademico periodoAcademico;
	private String observaciones;
	private Integer estado;
	private Date fechaEstado;
	private ProgramaAcademico programaAcademico;
	private String tipoContacto;
	private String usuario;
	private Integer vinculo;
	
	public EstudianteSolicitud() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	public EstudianteSolicitud(Long idEstudianteSolicitud,
			Estudiante estudiante, PeriodoAcademico periodoAcademico,
			String observaciones, Integer estado, Date fechaEstado,
			ProgramaAcademico programaAcademico, String tipoContacto,
			String usuario, Integer vinculo) {
		super();
		this.idEstudianteSolicitud = idEstudianteSolicitud;
		this.estudiante = estudiante;
		this.periodoAcademico = periodoAcademico;
		this.observaciones = observaciones;
		this.estado = estado;
		this.fechaEstado = fechaEstado;
		this.programaAcademico = programaAcademico;
		this.tipoContacto = tipoContacto;
		this.usuario = usuario;
		this.vinculo = vinculo;
	}

	public Long getIdEstudianteSolicitud() {
		return idEstudianteSolicitud;
	}

	public void setIdEstudianteSolicitud(Long idEstudianteSolicitud) {
		this.idEstudianteSolicitud = idEstudianteSolicitud;
	}

	public Estudiante getEstudiante() {
		return estudiante;
	}

	public void setEstudiante(Estudiante estudiante) {
		this.estudiante = estudiante;
	}

	public PeriodoAcademico getPeriodoAcademico() {
		return periodoAcademico;
	}

	public void setPeriodoAcademico(PeriodoAcademico periodoAcademico) {
		this.periodoAcademico = periodoAcademico;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Date getFechaEstado() {
		return fechaEstado;
	}

	public void setFechaEstado(Date fechaEstado) {
		this.fechaEstado = fechaEstado;
	}

	public ProgramaAcademico getProgramaAcademico() {
		return programaAcademico;
	}

	public void setProgramaAcademico(ProgramaAcademico programaAcademico) {
		this.programaAcademico = programaAcademico;
	}

	public String getTipoContacto() {
		return tipoContacto;
	}

	public void setTipoContacto(String tipoContacto) {
		this.tipoContacto = tipoContacto;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Integer getVinculo() {
		return vinculo;
	}

	public void setVinculo(Integer vinculo) {
		this.vinculo = vinculo;
	}
	
	

}
