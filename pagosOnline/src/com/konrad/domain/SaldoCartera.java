package com.konrad.domain;

import java.util.Date;

public class SaldoCartera {
	
	private String documento;
	private String organizacion;
	private Long numeroCredito;
	private Date fechaDocumento;
	private Date fechaVencimiento;
	private Double valorDocumento;
	private Double valorAfectado;
	private Date fechaCancelacion;
	private Cliente cliente;
	private Integer identificador; 
	private CausaNota causaNota;
	private ConceptoNota conceptoNota;
	private PeriodoFacturacion periodoFacturacion;
	private String concepto;
	private String banderaDiaHabil;
	private String estadoVencimiento;
	private String descripcion;
	private Integer prioridad;
	private Integer diasVencimiento;
	
	
	
	public Integer getIdentificador() {
		return identificador;
	}

	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
	}
	
	public PeriodoFacturacion getPeriodoFacturacion() {
		return periodoFacturacion;
	}

	public void setPeriodoFacturacion(PeriodoFacturacion periodoFacturacion) {
		this.periodoFacturacion = periodoFacturacion;
	}

	public CausaNota getCausaNota() {
		return causaNota;
	}

	public void setCausaNota(CausaNota causaNota) {
		this.causaNota = causaNota;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getOrganizacion() {
		return organizacion;
	}

	public void setOrganizacion(String organizacion) {
		this.organizacion = organizacion;
	}

	public Long getNumeroCredito() {
		return numeroCredito;
	}

	public void setNumeroCredito(Long numeroCredito) {
		this.numeroCredito = numeroCredito;
	}

	public Date getFechaDocumento() {
		return fechaDocumento;
	}

	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public Double getValorDocumento() {
		return valorDocumento;
	}

	public void setValorDocumento(Double valorDocumento) {
		this.valorDocumento = valorDocumento;
	}

	public Double getValorAfectado() {
		return valorAfectado;
	}

	public void setValorAfectado(Double valorAfectado) {
		this.valorAfectado = valorAfectado;
	}

	public Date getFechaCancelacion() {
		return fechaCancelacion;
	}

	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getBanderaDiaHabil() {
		return banderaDiaHabil;
	}

	public void setBanderaDiaHabil(String banderaDiaHabil) {
		this.banderaDiaHabil = banderaDiaHabil;
	}

	public String getEstadoVencimiento() {
		return estadoVencimiento;
	}

	public void setEstadoVencimiento(String estadoVencimiento) {
		this.estadoVencimiento = estadoVencimiento;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(Integer prioridad) {
		this.prioridad = prioridad;
	}


	public ConceptoNota getConceptoNota() {
		return conceptoNota;
	}

	public void setConceptoNota(ConceptoNota conceptoNota) {
		this.conceptoNota = conceptoNota;
	}

	public Integer getDiasVencimiento() {
		return diasVencimiento;
	}

	public void setDiasVencimiento(Integer diasVencimiento) {
		this.diasVencimiento = diasVencimiento;
	}
	
}
