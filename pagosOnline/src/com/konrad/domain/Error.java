package com.konrad.domain;

public class Error {

	private String tipo;
	private String causa;
	private String descripcion;
	private String accion;
	private String errortecnico;
	
	
	
	public String getErrortecnico() {
		return errortecnico;
	}
	public void setErrortecnico(String errortecnico) {
		this.errortecnico = errortecnico;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCausa() {
		return causa;
	}
	public void setCausa(String causa) {
		this.causa = causa;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}	
	
}
