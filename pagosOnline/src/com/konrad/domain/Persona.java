package com.konrad.domain;

import java.util.Date;

public class Persona {
	
	private String nombreRazonSocial;
	private String primerApellido;
	private String segundoApellido;
	private String identificacion;
	private TipoIdentificacion tipoIdentificacion;
	private Long secuenciaPersona;
	private String direccionElectronica;
	private String telefonoResidencia;
	private String direccionResidencia;
	private String apartadoAereo;
	private String paginaWeb;
	private String envioCorrespondencia;
	private Date fechaNacimiento;
	private String nombres;
	private Poblacion poblacionResidencia;
	private String sexo;
	private Cliente cliente;
	private String empresa;
	private String cargo;
	private String gradoEscolaridad;
	private String ocupacion;
	private Integer aceptarCondiciones;
	private Integer aceptarPersonales;
	private String perteneceColegioColombianoPsicologos;
	private String aceptarInscripcionPsicoWeb;
	//cesar
	private String universidadProcedencia;
	private String aceptaCena;
	private String tipoParticipante;
	private String modalidadAsistencia;

	public Persona() {
		super();
	}
		
	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getDireccionResidencia() {
		return direccionResidencia;
	}

	public void setDireccionResidencia(String direccionResidencia) {
		this.direccionResidencia = direccionResidencia;
	}


	public String getApartadoAereo() {
		return apartadoAereo;
	}


	public void setApartadoAereo(String apartadoAereo) {
		this.apartadoAereo = apartadoAereo;
	}


	public String getPaginaWeb() {
		return paginaWeb;
	}


	public void setPaginaWeb(String paginaWeb) {
		this.paginaWeb = paginaWeb;
	}


	public String getEnvioCorrespondencia() {
		return envioCorrespondencia;
	}


	public void setEnvioCorrespondencia(String envioCorrespondencia) {
		this.envioCorrespondencia = envioCorrespondencia;
	}
	
	public String getNombreRazonSocial() {
		return nombreRazonSocial;
	}


	public void setNombreRazonSocial(String nombreRazonSocial) {
		this.nombreRazonSocial = nombreRazonSocial;
	}


	public String getPrimerApellido() {
		return primerApellido;
	}


	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}


	public String getSegundoApellido() {
		return segundoApellido;
	}


	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}


	public String getIdentificacion() {
		return identificacion;
	}


	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}


	public Long getSecuenciaPersona() {
		return secuenciaPersona;
	}


	public void setSecuenciaPersona(Long secuenciaPersona) {
		this.secuenciaPersona = secuenciaPersona;
	}


	public String getDireccionElectronica() {
		return direccionElectronica;
	}


	public void setDireccionElectronica(String direccionElectronica) {
		this.direccionElectronica = direccionElectronica;
	}


	public String getNombres() {
		return nombres;
	}


	public void setNombres(String nombres) {
		this.nombres = nombres;
	}


	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}


	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}


	public String getTelefonoResidencia() {
		return telefonoResidencia;
	}


	public void setTelefonoResidencia(String telefonoResidencia) {
		this.telefonoResidencia = telefonoResidencia;
	}


	public TipoIdentificacion getTipoIdentificacion() {
		return tipoIdentificacion;
	}


	public void setTipoIdentificacion(TipoIdentificacion tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}


	public Cliente getCliente() {
		return cliente;
	}


	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Poblacion getPoblacionResidencia() {
		return poblacionResidencia;
	}

	public void setPoblacionResidencia(Poblacion poblacionResidencia) {
		this.poblacionResidencia = poblacionResidencia;
	}

	public String getGradoEscolaridad() {
		return gradoEscolaridad;
	}

	public void setGradoEscolaridad(String gradoEscolaridad) {
		this.gradoEscolaridad = gradoEscolaridad;
	}

	public String getOcupacion() {
		return ocupacion;
	}

	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}

	public Integer getAceptarCondiciones() {
		return aceptarCondiciones;
	}

	public void setAceptarCondiciones(Integer aceptarCondiciones) {
		this.aceptarCondiciones = aceptarCondiciones;
	}

	public Integer getAceptarPersonales() {
		return aceptarPersonales;
	}

	public void setAceptarPersonales(Integer aceptarPersonales) {
		this.aceptarPersonales = aceptarPersonales;
	}

	public String getPerteneceColegioColombianoPsicologos() {
		return perteneceColegioColombianoPsicologos;
	}

	public void setPerteneceColegioColombianoPsicologos(
			String perteneceColegioColombianoPsicologos) {
		this.perteneceColegioColombianoPsicologos = perteneceColegioColombianoPsicologos;
	}

	public String getAceptarInscripcionPsicoWeb() {
	    return aceptarInscripcionPsicoWeb;
	}

	public void setAceptarInscripcionPsicoWeb(
		String aceptarInscripcionPsicoWeb) {
	    this.aceptarInscripcionPsicoWeb = aceptarInscripcionPsicoWeb;
	}

	public String getUniversidadProcedencia() {
		return universidadProcedencia;
	}

	public void setUniversidadProcedencia(String universidadProcedencia) {
		this.universidadProcedencia = universidadProcedencia;
	}

	public String getAceptaCena() {
		return aceptaCena;
	}

	public void setAceptaCena(String aceptaCena) {
		this.aceptaCena = aceptaCena;
	}

	public String getTipoParticipante() {
		return tipoParticipante;
	}

	public void setTipoParticipante(String tipoParticipante) {
		this.tipoParticipante = tipoParticipante;
	}

	public String getModalidadAsistencia() {
		return modalidadAsistencia;
	}

	public void setModalidadAsistencia(String modalidadAsistencia) {
		this.modalidadAsistencia = modalidadAsistencia;
	}

}
