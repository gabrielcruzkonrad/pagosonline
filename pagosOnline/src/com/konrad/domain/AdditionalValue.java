package com.konrad.domain;

public class AdditionalValue {

	private Double value;
	private String currency;
	
	public AdditionalValue() {
		// TODO Auto-generated constructor stub
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

}
