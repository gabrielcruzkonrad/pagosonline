package com.konrad.domain;

public class Fondo {

	private Long fondo;
	private String nombreFondo;
	private String nombreExterno;
	private String estadoFondo;
	private Fondo fondoPredecesor;
	
	public Long getFondo() {
		return fondo;
	}
	public void setFondo(Long fondo) {
		this.fondo = fondo;
	}
	public String getNombreFondo() {
		return nombreFondo;
	}
	public void setNombreFondo(String nombreFondo) {
		this.nombreFondo = nombreFondo;
	}
	public String getNombreExterno() {
		return nombreExterno;
	}
	public void setNombreExterno(String nombreExterno) {
		this.nombreExterno = nombreExterno;
	}
	public Fondo getFondoPredecesor() {
		return fondoPredecesor;
	}
	public void setFondoPredecesor(Fondo fondoPredecesor) {
		this.fondoPredecesor = fondoPredecesor;
	}
	public String getEstadoFondo() {
		return estadoFondo;
	}
	public void setEstadoFondo(String estadoFondo) {
		this.estadoFondo = estadoFondo;
	}
	
	
}
