package com.konrad.domain;

public class TipoPrograma {

	private String idTipoPrograma;
	private String nombreTipoPrograma;
	
	
	
	public TipoPrograma(String idTipoPrograma, String nombreTipoPrograma) {
		super();
		this.idTipoPrograma = idTipoPrograma;
		this.nombreTipoPrograma = nombreTipoPrograma;
	}

	public TipoPrograma() {
		// TODO Auto-generated constructor stub
		super();
	}

	public String getIdTipoPrograma() {
		return idTipoPrograma;
	}

	public void setIdTipoPrograma(String idTipoPrograma) {
		this.idTipoPrograma = idTipoPrograma;
	}

	public String getNombreTipoPrograma() {
		return nombreTipoPrograma;
	}

	public void setNombreTipoPrograma(String nombreTipoPrograma) {
		this.nombreTipoPrograma = nombreTipoPrograma;
	}

}
