package com.konrad.domain;

public class Poblacion {


	private Departamento departamento;
	private Long poblacion;
	private String nombrePoblacion;
	private String aplicaIca;
	private String gentilicio;
	
	public Poblacion() {
		// TODO Auto-generated constructor stub
	}
	
	public Poblacion(Departamento departamento, Long poblacion,
			String nombrePoblacion, String aplicaIca, String gentilicio) {
		super();
		this.departamento = departamento;
		this.poblacion = poblacion;
		this.nombrePoblacion = nombrePoblacion;
		this.aplicaIca = aplicaIca;
		this.gentilicio = gentilicio;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public Long getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(Long poblacion) {
		this.poblacion = poblacion;
	}

	public String getNombrePoblacion() {
		return nombrePoblacion;
	}

	public void setNombrePoblacion(String nombrePoblacion) {
		this.nombrePoblacion = nombrePoblacion;
	}

	public String getAplicaIca() {
		return aplicaIca;
	}

	public void setAplicaIca(String aplicaIca) {
		this.aplicaIca = aplicaIca;
	}

	public String getGentilicio() {
		return gentilicio;
	}

	public void setGentilicio(String gentilicio) {
		this.gentilicio = gentilicio;
	}



}
