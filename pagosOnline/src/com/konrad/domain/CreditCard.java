package com.konrad.domain;

public class CreditCard {

	private String maskedNumber;
	private String name;
	private String issuerBank;
	
	
	public CreditCard() {
		// TODO Auto-generated constructor stub
	}


	public String getMaskedNumber() {
		return maskedNumber;
	}


	public void setMaskedNumber(String maskedNumber) {
		this.maskedNumber = maskedNumber;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getIssuerBank() {
		return issuerBank;
	}


	public void setIssuerBank(String issuerBank) {
		this.issuerBank = issuerBank;
	}

}
