package com.konrad.domain;

public class PingResponse {

	private String code;
	private String error;
	private String transactionResponse;
	
	public PingResponse() {
		// TODO Auto-generated constructor stub
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getTransactionResponse() {
		return transactionResponse;
	}

	public void setTransactionResponse(String transactionResponse) {
		this.transactionResponse = transactionResponse;
	}

}
