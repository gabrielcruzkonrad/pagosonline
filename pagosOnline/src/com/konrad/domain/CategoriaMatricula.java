package com.konrad.domain;

public class CategoriaMatricula {

	private Long idCategoria;
	private ProgramaAcademico programaAcademico;
	private Double valor;
	private String nombre;
	
	public CategoriaMatricula(Long idCategoria,
			ProgramaAcademico programaAcademico, Double valor, String nombre) {
		super();
		this.idCategoria = idCategoria;
		this.programaAcademico = programaAcademico;
		this.valor = valor;
		this.nombre = nombre;
	}
	
	public CategoriaMatricula() {
		// TODO Auto-generated constructor stub
		super();
	}

	public Long getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(Long idCategoria) {
		this.idCategoria = idCategoria;
	}

	public ProgramaAcademico getProgramaAcademico() {
		return programaAcademico;
	}

	public void setProgramaAcademico(ProgramaAcademico programaAcademico) {
		this.programaAcademico = programaAcademico;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



}
