package com.konrad.domain;

import java.util.Date;



public class ReciboConsignacion {
  private Long reciboConsignacion;
  private Long numeroFila;
  private String observaciones;
  private Cliente cliente;
  private Persona persona;
  private Double valorTotal;
  private Double valorDetalle;
  private Double valorVencido;
  private Date fecha;
  private Date fechaPago;
  private VencimientoPeriodo periodo;
  private String generaComision;
  private TasaRepresentativaMercado tasaRepresentativa;
  private TasaRepresentativaMercado tasaRepresentativaVencido;
  private CuentaReciboConsignacion cuentaReciboConsignacion;
  private Orden orden;
  private Long identificador; 
  private String banderaFecha;
  private String banderaDiaHabil;
  private SaldoCartera saldoCartera;
  private String estado;
  
public ReciboConsignacion() {
	super();
}

public Long getReciboConsignacion() {
	return reciboConsignacion;
}

public void setReciboConsignacion(Long reciboConsignacion) {
	this.reciboConsignacion = reciboConsignacion;
}

public Cliente getCliente() {
	return cliente;
}

public void setCliente(Cliente cliente) {
	this.cliente = cliente;
}

public VencimientoPeriodo getPeriodo() {
	return periodo;
}
public void setPeriodo(VencimientoPeriodo periodo) {
	this.periodo = periodo;
}

public Double getValorTotal() {
	return valorTotal;
}

public void setValorTotal(Double valorTotal) {
	this.valorTotal = valorTotal;
}

public Double getValorDetalle() {
	return valorDetalle;
}

public void setValorDetalle(Double valorDetalle) {
	this.valorDetalle = valorDetalle;
}

public String getObservaciones() {
	return observaciones;
}

public void setObservaciones(String observaciones) {
	this.observaciones = observaciones;
}

public Long getNumeroFila() {
	return numeroFila;
}

public void setNumeroFila(Long numeroFila) {
	this.numeroFila = numeroFila;
}

public TasaRepresentativaMercado getTasaRepresentativa() {
	return tasaRepresentativa;
}

public void setTasaRepresentativa(TasaRepresentativaMercado tasaRepresentativa) {
	this.tasaRepresentativa = tasaRepresentativa;
}

public CuentaReciboConsignacion getCuentaReciboConsignacion() {
	return cuentaReciboConsignacion;
}

public void setCuentaReciboConsignacion(CuentaReciboConsignacion cuentaReciboConsignacion) {
	this.cuentaReciboConsignacion = cuentaReciboConsignacion;
}

public String getGeneraComision() {
	return generaComision;
}

public void setGeneraComision(String generaComision) {
	this.generaComision = generaComision;
}

public Date getFecha() {
	return fecha;
}

public void setFecha(Date fecha) {
	this.fecha = fecha;
}

public Long getIdentificador() {
	return identificador;
}

public void setIdentificador(Long identificador) {
	this.identificador = identificador;
}

public TasaRepresentativaMercado getTasaRepresentativaVencido() {
	return tasaRepresentativaVencido;
}

public void setTasaRepresentativaVencido(TasaRepresentativaMercado tasaRepresentativaVencido) {
	this.tasaRepresentativaVencido = tasaRepresentativaVencido;
}

public Double getValorVencido() {
	return valorVencido;
}

public void setValorVencido(Double valorVencido) {
	this.valorVencido = valorVencido;
}

public Orden getOrden() {
	return orden;
}

public void setOrden(Orden orden) {
	this.orden = orden;
}

public String getBanderaFecha() {
	return banderaFecha;
}

public void setBanderaFecha(String banderaFecha) {
	this.banderaFecha = banderaFecha;
}

public String getBanderaDiaHabil() {
	return banderaDiaHabil;
}

public void setBanderaDiaHabil(String banderaDiaHabil) {
	this.banderaDiaHabil = banderaDiaHabil;
}

public Date getFechaPago() {
	return fechaPago;
}

public void setFechaPago(Date fechaPago) {
	this.fechaPago = fechaPago;
}

public SaldoCartera getSaldoCartera() {
	return saldoCartera;
}

public void setSaldoCartera(SaldoCartera saldoCartera) {
	this.saldoCartera = saldoCartera;
}

public Persona getPersona() {
	return persona;
}

public void setPersona(Persona persona) {
	this.persona = persona;
}

public String getEstado() {
	return estado;
}

public void setEstado(String estado) {
	this.estado = estado;
}
  	
}
