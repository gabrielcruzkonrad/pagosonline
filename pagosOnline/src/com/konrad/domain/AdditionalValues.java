package com.konrad.domain;

public class AdditionalValues {

	private AdditionalValue PM_TAX_ADMINISTRATIVE_FEE;
	private AdditionalValue TX_ADDITIONAL_VALUE;
	private AdditionalValue PM_TAX_ADMINISTRATIVE_FEE_RETURN_BASE;
	private AdditionalValue TX_VALUE;
	private AdditionalValue PM_TAX_RETURN_BASE;
	private AdditionalValue PM_ADMINISTRATIVE_FEE;
	private AdditionalValue PM_VALUE;
	private AdditionalValue TX_ADMINISTRATIVE_FEE;
	private AdditionalValue TX_NETWORK_VALUE;
	private AdditionalValue PM_NETWORK_VALUE;
	private AdditionalValue PM_ADDITIONAL_VALUE;
	private AdditionalValue TX_TAX_RETURN_BASE;
	private AdditionalValue PM_TAX;
	private AdditionalValue TX_TAX;
	private AdditionalValue TX_TAX_ADMINISTRATIVE_FEE;
	private AdditionalValue TX_TAX_ADMINISTRATIVE_FEE_RETURN_BASE;
	
	
	
	
	public AdditionalValues() {
		// TODO Auto-generated constructor stub
	}




	public AdditionalValue getPM_TAX_ADMINISTRATIVE_FEE() {
		return PM_TAX_ADMINISTRATIVE_FEE;
	}




	public void setPM_TAX_ADMINISTRATIVE_FEE(
			AdditionalValue pM_TAX_ADMINISTRATIVE_FEE) {
		PM_TAX_ADMINISTRATIVE_FEE = pM_TAX_ADMINISTRATIVE_FEE;
	}




	public AdditionalValue getTX_ADDITIONAL_VALUE() {
		return TX_ADDITIONAL_VALUE;
	}




	public void setTX_ADDITIONAL_VALUE(AdditionalValue tX_ADDITIONAL_VALUE) {
		TX_ADDITIONAL_VALUE = tX_ADDITIONAL_VALUE;
	}




	public AdditionalValue getPM_TAX_ADMINISTRATIVE_FEE_RETURN_BASE() {
		return PM_TAX_ADMINISTRATIVE_FEE_RETURN_BASE;
	}




	public void setPM_TAX_ADMINISTRATIVE_FEE_RETURN_BASE(
			AdditionalValue pM_TAX_ADMINISTRATIVE_FEE_RETURN_BASE) {
		PM_TAX_ADMINISTRATIVE_FEE_RETURN_BASE = pM_TAX_ADMINISTRATIVE_FEE_RETURN_BASE;
	}




	public AdditionalValue getTX_VALUE() {
		return TX_VALUE;
	}




	public void setTX_VALUE(AdditionalValue tX_VALUE) {
		TX_VALUE = tX_VALUE;
	}




	public AdditionalValue getPM_TAX_RETURN_BASE() {
		return PM_TAX_RETURN_BASE;
	}




	public void setPM_TAX_RETURN_BASE(AdditionalValue pM_TAX_RETURN_BASE) {
		PM_TAX_RETURN_BASE = pM_TAX_RETURN_BASE;
	}




	public AdditionalValue getPM_ADMINISTRATIVE_FEE() {
		return PM_ADMINISTRATIVE_FEE;
	}




	public void setPM_ADMINISTRATIVE_FEE(AdditionalValue pM_ADMINISTRATIVE_FEE) {
		PM_ADMINISTRATIVE_FEE = pM_ADMINISTRATIVE_FEE;
	}




	public AdditionalValue getPM_VALUE() {
		return PM_VALUE;
	}




	public void setPM_VALUE(AdditionalValue pM_VALUE) {
		PM_VALUE = pM_VALUE;
	}




	public AdditionalValue getTX_ADMINISTRATIVE_FEE() {
		return TX_ADMINISTRATIVE_FEE;
	}




	public void setTX_ADMINISTRATIVE_FEE(AdditionalValue tX_ADMINISTRATIVE_FEE) {
		TX_ADMINISTRATIVE_FEE = tX_ADMINISTRATIVE_FEE;
	}




	public AdditionalValue getTX_NETWORK_VALUE() {
		return TX_NETWORK_VALUE;
	}




	public void setTX_NETWORK_VALUE(AdditionalValue tX_NETWORK_VALUE) {
		TX_NETWORK_VALUE = tX_NETWORK_VALUE;
	}




	public AdditionalValue getPM_NETWORK_VALUE() {
		return PM_NETWORK_VALUE;
	}




	public void setPM_NETWORK_VALUE(AdditionalValue pM_NETWORK_VALUE) {
		PM_NETWORK_VALUE = pM_NETWORK_VALUE;
	}




	public AdditionalValue getPM_ADDITIONAL_VALUE() {
		return PM_ADDITIONAL_VALUE;
	}




	public void setPM_ADDITIONAL_VALUE(AdditionalValue pM_ADDITIONAL_VALUE) {
		PM_ADDITIONAL_VALUE = pM_ADDITIONAL_VALUE;
	}




	public AdditionalValue getTX_TAX_RETURN_BASE() {
		return TX_TAX_RETURN_BASE;
	}




	public void setTX_TAX_RETURN_BASE(AdditionalValue tX_TAX_RETURN_BASE) {
		TX_TAX_RETURN_BASE = tX_TAX_RETURN_BASE;
	}




	public AdditionalValue getPM_TAX() {
		return PM_TAX;
	}




	public void setPM_TAX(AdditionalValue pM_TAX) {
		PM_TAX = pM_TAX;
	}




	public AdditionalValue getTX_TAX() {
		return TX_TAX;
	}




	public void setTX_TAX(AdditionalValue tX_TAX) {
		TX_TAX = tX_TAX;
	}




	public AdditionalValue getTX_TAX_ADMINISTRATIVE_FEE() {
		return TX_TAX_ADMINISTRATIVE_FEE;
	}




	public void setTX_TAX_ADMINISTRATIVE_FEE(
			AdditionalValue tX_TAX_ADMINISTRATIVE_FEE) {
		TX_TAX_ADMINISTRATIVE_FEE = tX_TAX_ADMINISTRATIVE_FEE;
	}




	public AdditionalValue getTX_TAX_ADMINISTRATIVE_FEE_RETURN_BASE() {
		return TX_TAX_ADMINISTRATIVE_FEE_RETURN_BASE;
	}




	public void setTX_TAX_ADMINISTRATIVE_FEE_RETURN_BASE(
			AdditionalValue tX_TAX_ADMINISTRATIVE_FEE_RETURN_BASE) {
		TX_TAX_ADMINISTRATIVE_FEE_RETURN_BASE = tX_TAX_ADMINISTRATIVE_FEE_RETURN_BASE;
	}

}
