package com.konrad.domain;


public class Usuario {
	
	private String usuario;
	private String clave;
	private String nombres;
	private String apellidos;
	private String estado;
	private String numeroidentificacion;
	private String administrador;
	private String incluido;
	private String descripcion;
	
	public Usuario(){	}
	public Usuario(String usuario,String clave,String nombre)
	{
		this.usuario=usuario;
		this.clave=clave;
	}
   		
	
	public String getAdministrador() {
		return administrador;
	}
	public void setAdministrador(String administrador) {
		this.administrador = administrador;
	}
	
	public void setUsuario(String usuario)
	{
		this.usuario=usuario;
		
	}
	public String getUsuario()
	{
		return usuario;
		
	}
	public void setClave(String clave)
	{
		this.clave=clave;
		
	}
	public String getClave()
	{
		return clave;
		
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getNumeroidentificacion() {
		return numeroidentificacion;
	}
	public void setNumeroidentificacion(String numeroidentificacion) {
		this.numeroidentificacion = numeroidentificacion;
	}
	public void setIncluido(String incluido) {
		this.incluido = incluido;
	}
	public String getIncluido() {
		return incluido;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
