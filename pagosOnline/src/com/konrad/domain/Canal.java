package com.konrad.domain;

import java.io.Serializable;

public class Canal implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6091053413874215754L;
	private Long canal;     
	private String nombreCanal; 
	private Double facturaMinima;
	private Integer prioridad;
	
	public Long getCanal() {
		return canal;
	}
	public void setCanal(Long canal) {
		this.canal = canal;
	}
	public String getNombreCanal() {
		return nombreCanal;
	}
	public void setNombreCanal(String nombreCanal) {
		this.nombreCanal = nombreCanal;
	}
	public Double getFacturaMinima() {
		return facturaMinima;
	}
	public void setFacturaMinima(Double facturaMinima) {
		this.facturaMinima = facturaMinima;
	}
	public Integer getPrioridad() {
		return prioridad;
	}
	public void setPrioridad(Integer prioridad) {
		this.prioridad = prioridad;
	} 
	

}
