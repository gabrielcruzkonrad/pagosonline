package com.konrad.domain;

public class ProgramaReporte {

	private String reporte;
	private CentroCosto centroCosto;
	
	public ProgramaReporte() {
		super();
	}
	
	public String getReporte() {
		return reporte;
	}

	public void setReporte(String reporte) {
		this.reporte = reporte;
	}

	public CentroCosto getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(CentroCosto centroCosto) {
		this.centroCosto = centroCosto;
	}

}
