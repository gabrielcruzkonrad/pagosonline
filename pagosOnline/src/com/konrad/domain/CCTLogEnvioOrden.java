package com.konrad.domain;

import java.io.Serializable;
import java.util.Date;

public class CCTLogEnvioOrden implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8736583763969970080L;
	private Integer seqLogEnvioOrden;
	private Orden orden;
	private Date fechaEnvio;

	public CCTLogEnvioOrden() {
		// TODO Auto-generated constructor stub
	}

	public Integer getSeqLogEnvioOrden() {
		return seqLogEnvioOrden;
	}

	public void setSeqLogEnvioOrden(Integer seqLogEnvioOrden) {
		this.seqLogEnvioOrden = seqLogEnvioOrden;
	}

	public Orden getOrden() {
		return orden;
	}

	public void setOrden(Orden orden) {
		this.orden = orden;
	}

	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

}
