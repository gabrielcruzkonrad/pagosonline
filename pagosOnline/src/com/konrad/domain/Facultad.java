package com.konrad.domain;

import java.util.Date;

public class Facultad {

	private Long idFacultad;
	private String nombreFacultad;
	private String nombreDecano;
	private String nombreViceDecano;
	private String nombreSecundario;
	private Date fechaFormacion;
	private Date fechaFinalizacion;
	private String codigoFacultad;
	private Date fechaAprobacion;
	private String aprobacionInterna;
	private String aprobacionExterna;
	
	public Facultad() {
		// TODO Auto-generated constructor stub
	}



	public Facultad(Long idFacultad, String nombreFacultad,
			String nombreDecano, String nombreViceDecano,
			String nombreSecundario, Date fechaFormacion,
			Date fechaFinalizacion, String codigoFacultad,
			Date fechaAprobacion, String aprobacionInterna,
			String aprobacionExterna) {
		super();
		this.idFacultad = idFacultad;
		this.nombreFacultad = nombreFacultad;
		this.nombreDecano = nombreDecano;
		this.nombreViceDecano = nombreViceDecano;
		this.nombreSecundario = nombreSecundario;
		this.fechaFormacion = fechaFormacion;
		this.fechaFinalizacion = fechaFinalizacion;
		this.codigoFacultad = codigoFacultad;
		this.fechaAprobacion = fechaAprobacion;
		this.aprobacionInterna = aprobacionInterna;
		this.aprobacionExterna = aprobacionExterna;
	}



	public Long getIdFacultad() {
		return idFacultad;
	}

	public void setIdFacultad(Long idFacultad) {
		this.idFacultad = idFacultad;
	}

	public String getNombreFacultad() {
		return nombreFacultad;
	}

	public void setNombreFacultad(String nombreFacultad) {
		this.nombreFacultad = nombreFacultad;
	}

	public String getNombreDecano() {
		return nombreDecano;
	}

	public void setNombreDecano(String nombreDecano) {
		this.nombreDecano = nombreDecano;
	}

	public String getNombreViceDecano() {
		return nombreViceDecano;
	}

	public void setNombreViceDecano(String nombreViceDecano) {
		this.nombreViceDecano = nombreViceDecano;
	}

	public String getCodigoFacultad() {
		return codigoFacultad;
	}

	public void setCodigoFacultad(String codigoFacultad) {
		this.codigoFacultad = codigoFacultad;
	}

	public String getNombreSecundario() {
		return nombreSecundario;
	}

	public void setNombreSecundario(String nombreSecundario) {
		this.nombreSecundario = nombreSecundario;
	}

	public Date getFechaFormacion() {
		return fechaFormacion;
	}

	public void setFechaFormacion(Date fechaFormacion) {
		this.fechaFormacion = fechaFormacion;
	}

	public Date getFechaFinalizacion() {
		return fechaFinalizacion;
	}

	public void setFechaFinalizacion(Date fechaFinalizacion) {
		this.fechaFinalizacion = fechaFinalizacion;
	}

	public Date getFechaAprobacion() {
		return fechaAprobacion;
	}

	public void setFechaAprobacion(Date fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}

	public String getAprobacionInterna() {
		return aprobacionInterna;
	}

	public void setAprobacionInterna(String aprobacionInterna) {
		this.aprobacionInterna = aprobacionInterna;
	}

	public String getAprobacionExterna() {
		return aprobacionExterna;
	}

	public void setAprobacionExterna(String aprobacionExterna) {
		this.aprobacionExterna = aprobacionExterna;
	}

}
