package com.konrad.domain;

public class Asignatura {

  private String codigo;
  private String nombre;
  private String grupo;
  private String notauno;
  private String notados;
  private String nota;
  private String notaoficial;
  private String estado;
  private String intensidad;
  private String creditos;
	
	public Asignatura() {
		// TODO Auto-generated constructor stub
	}



	    public String getCodigo() {
	        return codigo;
	    }

	    public void setCodigo(String codigo) {
	        this.codigo = codigo;
	    }

	    public String getNombre() {
	        return nombre;
	    }

	    public void setNombre(String nombre) {
	        this.nombre = nombre;
	    }

	    public String getGrupo() {
	        return grupo;
	    }

	    public void setGrupo(String grupo) {
	        this.grupo = grupo;
	    }
	    
	    public String getNota() {
	        return nota;
	    }

	    public void setNota(String nota) {
	        this.nota = nota;
	    }

	    public String getNotaoficial() {
	        return notaoficial;
	    }

	    public void setNotaoficial(String notaoficial) {
	        this.notaoficial = notaoficial;
	    }

	    public String getEstado() {
	        return estado;
	    }

	    public void setEstado(String estado) {
	        this.estado = estado;
	    }

	    public String getIntensidad() {
	        return intensidad;
	    }

	    public void setIntensidad(String intensidad) {
	        this.intensidad = intensidad;
	    }

	    public String getNotauno() {
	        return notauno;
	    }

	    public void setNotauno(String notauno) {
	        this.notauno = notauno;
	    }

	    public String getNotados() {
	        return notados;
	    }

	    public void setNotados(String notados) {
	        this.notados = notados;
	    }
	    
	    public String getCreditos(){
	    return creditos;
	    }
	    
	    public void setCreditos(String creditos){
	    this.creditos = creditos;
	    }
	    
	}

