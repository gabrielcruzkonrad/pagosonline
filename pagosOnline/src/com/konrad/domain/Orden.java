package com.konrad.domain;

import java.util.Date;

public class Orden {

	private Long orden;
	private String documento;
	private String organizacion;
	private PeriodoFacturacion periodo;
	private CentroCosto centroCosto;
	private Cliente cliente;
	private String descripcion;
	private String mensaje;
	private Fondo fondo;
	private FuenteFuncion fuenteFuncion;
	private String estado;
	private Date fechaOrden;
	private Date fechaLiquidacion;
	private Date fechaVencimiento;
	private String indicadorCPC;
	
	public Orden() {
		super();
	}
	
	public Long getOrden() {
		return orden;
	}
	public void setOrden(Long orden) {
		this.orden = orden;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public String getOrganizacion() {
		return organizacion;
	}
	public void setOrganizacion(String organizacion) {
		this.organizacion = organizacion;
	}
	public PeriodoFacturacion getPeriodo() {
		return periodo;
	}
	public void setPeriodo(PeriodoFacturacion periodo) {
		this.periodo = periodo;
	}

	public CentroCosto getCentroCosto() {
		return centroCosto;
	}

	public void setCentroCosto(CentroCosto centroCosto) {
		this.centroCosto = centroCosto;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Fondo getFondo() {
		return fondo;
	}

	public void setFondo(Fondo fondo) {
		this.fondo = fondo;
	}

	public FuenteFuncion getFuenteFuncion() {
		return fuenteFuncion;
	}

	public void setFuenteFuncion(FuenteFuncion fuenteFuncion) {
		this.fuenteFuncion = fuenteFuncion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaOrden() {
		return fechaOrden;
	}

	public void setFechaOrden(Date fechaOrden) {
		this.fechaOrden = fechaOrden;
	}

	public Date getFechaLiquidacion() {
		return fechaLiquidacion;
	}

	public void setFechaLiquidacion(Date fechaLiquidacion) {
		this.fechaLiquidacion = fechaLiquidacion;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public String getIndicadorCPC() {
		return indicadorCPC;
	}

	public void setIndicadorCPC(String indicadorCPC) {
		this.indicadorCPC = indicadorCPC;
	}
	
}
