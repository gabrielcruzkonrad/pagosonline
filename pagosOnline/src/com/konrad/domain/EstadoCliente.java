package com.konrad.domain;

public class EstadoCliente {

	private Long estadoCliente;			
	private String nombreEstado;			
	private String bloqueado;			
	private String defecto;
	
	public EstadoCliente() {
		super();
	}
	public Long getEstadoCliente() {
		return estadoCliente;
	}
	public void setEstadoCliente(Long estadoCliente) {
		this.estadoCliente = estadoCliente;
	}
	public String getNombreEstado() {
		return nombreEstado;
	}
	public void setNombreEstado(String nombreEstado) {
		this.nombreEstado = nombreEstado;
	}
	public String getBloqueado() {
		return bloqueado;
	}
	public void setBloqueado(String bloqueado) {
		this.bloqueado = bloqueado;
	}
	public String getDefecto() {
		return defecto;
	}
	public void setDefecto(String defecto) {
		this.defecto = defecto;
	} 			
	
}
