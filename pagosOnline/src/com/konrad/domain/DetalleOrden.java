package com.konrad.domain;


public class DetalleOrden {

    private Orden orden;  
    private Double cantidad;
    private Double precio;
    private Double descuento; 
    private Double iva;
    private Double subBruto;
    private Double subDescuento;
    private Double subIva;
    private Double subTotal;
    private String liquidado;
    private String descripcion;
    private Long referencia; 
    private String nombreReferencia;
    private String linea; 
    private String estado;  
    private CentroCosto centroCosto;                
    private Fondo fondo;
    private CCTAtributoProducto atributoProducto;
    
	public Orden getOrden() {
		return orden;
	}
	public void setOrden(Orden orden) {
		this.orden = orden;
	}
	public Double getCantidad() {
		return cantidad;
	}
	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}
	public Double getPrecio() {
		return precio;
	}
	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	public Double getDescuento() {
		return descuento;
	}
	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}
	public Double getIva() {
		return iva;
	}
	public void setIva(Double iva) {
		this.iva = iva;
	}
	public Double getSubBruto() {
		return subBruto;
	}
	public void setSubBruto(Double subBruto) {
		this.subBruto = subBruto;
	}
	public Double getSubDescuento() {
		return subDescuento;
	}
	public void setSubDescuento(Double subDescuento) {
		this.subDescuento = subDescuento;
	}
	public Double getSubIva() {
		return subIva;
	}
	public void setSubIva(Double subIva) {
		this.subIva = subIva;
	}
	public Double getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}
	public String getLiquidado() {
		return liquidado;
	}
	public void setLiquidado(String liquidado) {
		this.liquidado = liquidado;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Long getReferencia() {
		return referencia;
	}
	public void setReferencia(Long referencia) {
		this.referencia = referencia;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public CentroCosto getCentroCosto() {
		return centroCosto;
	}
	public void setCentroCosto(CentroCosto centroCosto) {
		this.centroCosto = centroCosto;
	}
	public Fondo getFondo() {
		return fondo;
	}
	public void setFondo(Fondo fondo) {
		this.fondo = fondo;
	}
	public String getNombreReferencia() {
		return nombreReferencia;
	}
	public void setNombreReferencia(String nombreReferencia) {
		this.nombreReferencia = nombreReferencia;
	}
	public CCTAtributoProducto getAtributoProducto() {
		return atributoProducto;
	}
	public void setAtributoProducto(CCTAtributoProducto atributoProducto) {
		this.atributoProducto = atributoProducto;
	}
}
