package com.konrad.domain;

public class Secuencias {

	private String nombre;
	private Long siguienteSecuencia;
	private Long incremento;
	
	public Secuencias() {
		// TODO Auto-generated constructor stub
	}
	
	public Secuencias(String nombre, Long siguienteSecuencia, Long incremento) {
		super();
		this.nombre = nombre;
		this.siguienteSecuencia = siguienteSecuencia;
		this.incremento = incremento;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getSiguienteSecuencia() {
		return siguienteSecuencia;
	}

	public void setSiguienteSecuencia(Long siguienteSecuencia) {
		this.siguienteSecuencia = siguienteSecuencia;
	}

	public Long getIncremento() {
		return incremento;
	}

	public void setIncremento(Long incremento) {
		this.incremento = incremento;
	}

	

}
