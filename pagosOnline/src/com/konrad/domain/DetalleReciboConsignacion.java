package com.konrad.domain;

public class DetalleReciboConsignacion {

	private ReciboConsignacion reciboConsignacion;
	private SaldoCartera saldoCartera;
	private Double valor;
	private Integer secuencia;
	private String observaciones;
	private Orden orden;
	private ConceptoNota conceptoNota;
	private CausaNota causaNota;
	
	
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Integer getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(Integer secuencia) {
		this.secuencia = secuencia;
	}

	public Orden getOrden() {
		return orden;
	}

	public void setOrden(Orden orden) {
		this.orden = orden;
	}

	public ConceptoNota getConceptoNota() {
		return conceptoNota;
	}

	public void setConceptoNota(ConceptoNota conceptoNota) {
		this.conceptoNota = conceptoNota;
	}

	public CausaNota getCausaNota() {
		return causaNota;
	}

	public void setCausaNota(CausaNota causaNota) {
		this.causaNota = causaNota;
	}

	public DetalleReciboConsignacion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReciboConsignacion getReciboConsignacion() {
		return reciboConsignacion;
	}

	public void setReciboConsignacion(ReciboConsignacion reciboConsignacion) {
		this.reciboConsignacion = reciboConsignacion;
	}

	public SaldoCartera getSaldoCartera() {
		return saldoCartera;
	}

	public void setSaldoCartera(SaldoCartera saldoCartera) {
		this.saldoCartera = saldoCartera;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	
}
