package com.konrad.domain;

import java.util.Date;

public class PagosOnline {
	private ReciboConsignacion reciboConsignacion;
	private String referenciaVenta;
	private Date fechaTransaccion;
	private Date fechaPago;
	private String hora;
	private String estadoTransaccion;
	private String bancoPse;
	private Double valor;
	private Moneda moneda;
	private String numeroTransaccion;
	private String medioPago;
	private String tipoMedioPago;
	private String referenciaExtra1;
	private String referenciaExtra2;
	private String cus;
	private String procesado;
	private String mensaje;
	private String organizacion;
	private String numero;
	private Date fechaProcesado;
	private Long secuencia;
	private String descripcionTransaccion;
	
	public PagosOnline() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ReciboConsignacion getReciboConsignacion() {
		return reciboConsignacion;
	}
	public void setReciboConsignacion(ReciboConsignacion reciboConsignacion) {
		this.reciboConsignacion = reciboConsignacion;
	}
	public String getReferenciaVenta() {
		return referenciaVenta;
	}
	public void setReferenciaVenta(String referenciaVenta) {
		this.referenciaVenta = referenciaVenta;
	}
	public Date getFechaTransaccion() {
		return fechaTransaccion;
	}
	public void setFechaTransaccion(Date fechaTransaccion) {
		this.fechaTransaccion = fechaTransaccion;
	}
	public Date getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getEstadoTransaccion() {
		return estadoTransaccion;
	}
	public void setEstadoTransaccion(String estadoTransaccion) {
		this.estadoTransaccion = estadoTransaccion;
	}
	public String getBancoPse() {
		return bancoPse;
	}
	public void setBancoPse(String bancoPse) {
		this.bancoPse = bancoPse;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public Moneda getMoneda() {
		return moneda;
	}
	public void setMoneda(Moneda moneda) {
		this.moneda = moneda;
	}
	public String getNumeroTransaccion() {
		return numeroTransaccion;
	}
	public void setNumeroTransaccion(String numeroTransaccion) {
		this.numeroTransaccion = numeroTransaccion;
	}
	public String getMedioPago() {
		return medioPago;
	}
	public void setMedioPago(String medioPago) {
		this.medioPago = medioPago;
	}
	public String getTipoMedioPago() {
		return tipoMedioPago;
	}
	public void setTipoMedioPago(String tipoMedioPago) {
		this.tipoMedioPago = tipoMedioPago;
	}
	public String getReferenciaExtra1() {
		return referenciaExtra1;
	}
	public void setReferenciaExtra1(String referenciaExtra1) {
		this.referenciaExtra1 = referenciaExtra1;
	}
	public String getReferenciaExtra2() {
		return referenciaExtra2;
	}
	public void setReferenciaExtra2(String referenciaExtra2) {
		this.referenciaExtra2 = referenciaExtra2;
	}
	public String getCus() {
		return cus;
	}
	public void setCus(String cus) {
		this.cus = cus;
	}
	public String getProcesado() {
		return procesado;
	}
	public void setProcesado(String procesado) {
		this.procesado = procesado;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getOrganizacion() {
		return organizacion;
	}
	public void setOrganizacion(String organizacion) {
		this.organizacion = organizacion;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Date getFechaProcesado() {
		return fechaProcesado;
	}
	public void setFechaProcesado(Date fechaProcesado) {
		this.fechaProcesado = fechaProcesado;
	}
	public Long getSecuencia() {
		return secuencia;
	}
	public void setSecuencia(Long secuencia) {
		this.secuencia = secuencia;
	}
	public String getDescripcionTransaccion() {
		return descripcionTransaccion;
	}
	public void setDescripcionTransaccion(String descripcionTransaccion) {
		this.descripcionTransaccion = descripcionTransaccion;
	}
	
	
	
	
}
