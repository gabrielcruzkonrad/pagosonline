package com.konrad.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class CCTAtributoProducto implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1536793872738095412L;
	private Long secAtributoProducto;
	private Long referencia;
	private byte[] fotoProducto;
	private String tipoDescuento;
	private BigDecimal descuento;
	private String tipoImpuesto;
	private BigDecimal impuesto;
	
	
	public CCTAtributoProducto() {
		// TODO Auto-generated constructor stub
	}


	public Long getSecAtributoProducto() {
		return secAtributoProducto;
	}


	public void setSecAtributoProducto(Long secAtributoProducto) {
		this.secAtributoProducto = secAtributoProducto;
	}


	public Long getReferencia() {
		return referencia;
	}


	public void setReferencia(Long referencia) {
		this.referencia = referencia;
	}


	public byte[] getFotoProducto() {
		return fotoProducto;
	}


	public void setFotoProducto(byte[] fotoProducto) {
		this.fotoProducto = fotoProducto;
	}


	public String getTipoDescuento() {
		return tipoDescuento;
	}


	public void setTipoDescuento(String tipoDescuento) {
		this.tipoDescuento = tipoDescuento;
	}


	public BigDecimal getDescuento() {
		return descuento;
	}


	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}


	public String getTipoImpuesto() {
		return tipoImpuesto;
	}


	public void setTipoImpuesto(String tipoImpuesto) {
		this.tipoImpuesto = tipoImpuesto;
	}


	public BigDecimal getImpuesto() {
		return impuesto;
	}


	public void setImpuesto(BigDecimal impuesto) {
		this.impuesto = impuesto;
	}

}
