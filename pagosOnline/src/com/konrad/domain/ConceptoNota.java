package com.konrad.domain;

public class ConceptoNota {

	private Integer conceptoNota;
	private String nombreConcepto;
	private String aplica;
	
	public ConceptoNota() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ConceptoNota(Integer conceptoNota, String nombreConcepto,
			String aplica) {
		super();
		this.conceptoNota = conceptoNota;
		this.nombreConcepto = nombreConcepto;
		this.aplica = aplica;
	}

	public Integer getConceptoNota() {
		return conceptoNota;
	}

	public void setConceptoNota(Integer conceptoNota) {
		this.conceptoNota = conceptoNota;
	}

	public String getNombreConcepto() {
		return nombreConcepto;
	}

	public void setNombreConcepto(String nombreConcepto) {
		this.nombreConcepto = nombreConcepto;
	}

	public String getAplica() {
		return aplica;
	}

	public void setAplica(String aplica) {
		this.aplica = aplica;
	}
	
	
	
}
