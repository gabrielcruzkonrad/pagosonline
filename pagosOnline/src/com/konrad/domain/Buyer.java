package com.konrad.domain;

public class Buyer {

   
    private String merchantBuyerId;
    private String  fullName;
    private String emailAddress;
    private String contactPhone;

	
	public Buyer() {
		// TODO Auto-generated constructor stub
	}


	public String getMerchantBuyerId() {
		return merchantBuyerId;
	}


	public void setMerchantBuyerId(String merchantBuyerId) {
		this.merchantBuyerId = merchantBuyerId;
	}


	public String getFullName() {
		return fullName;
	}


	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


	public String getEmailAddress() {
		return emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public String getContactPhone() {
		return contactPhone;
	}


	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

}
