package com.konrad.domain;

import java.util.Date;

public class VencimientoPeriodo {
	private Long grupo;		
	private Date fechaVencimiento;			
	private PeriodoFacturacion periodo;			
	private Integer porcentaje;		
	private String descripcionGrupo;		
	private String descripcionFecha;
	
	public VencimientoPeriodo() {
		super();
	}
	
	
	public Long getGrupo() {
		return grupo;
	}
	public void setGrupo(Long grupo) {
		this.grupo = grupo;
	}
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public PeriodoFacturacion getPeriodo() {
		return periodo;
	}
	public void setPeriodo(PeriodoFacturacion periodo) {
		this.periodo = periodo;
	}
	public Integer getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(Integer porcentaje) {
		this.porcentaje = porcentaje;
	}
	public String getDescripcionGrupo() {
		return descripcionGrupo;
	}
	public void setDescripcionGrupo(String descripcionGrupo) {
		this.descripcionGrupo = descripcionGrupo;
	}
	public String getDescripcionFecha() {
		return descripcionFecha;
	}
	public void setDescripcionFecha(String descripcionFecha) {
		this.descripcionFecha = descripcionFecha;
	}	

}
