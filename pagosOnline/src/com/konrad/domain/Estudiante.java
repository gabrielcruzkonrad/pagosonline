package com.konrad.domain;

import java.util.Date;

public class Estudiante {

	private String documentoEstudiante;
	private Long numeroCarne;
	private Long idEstudiante;
	private PeriodoAcademico periodoIngreso;
	private String claveNumero;
	private String email;
	private String nombreEstudiante;
	private String apellidosEstudiante;
	private Date fechaInscripcion;
	private ProgramaAcademico programaAcademico;
	private String sexo;
	private TipoIdentificacion tipoDocumento;
	private String direccionCorrespondencia;
	private String telefonoCorrespondencia;
	private String celularEstudiante;
	private String emailOtro;
	private Integer pazSalvo;
	private Integer estado;
	private Date fechaNacimiento;
	private Integer semestre;
	private Integer semestreNivel;
	private Integer cargaMaxima;
	private Integer horasMaxima;
	private CategoriaMatricula categoriaMatricula;
	private ConceptoAcademico conceptoAcademico;
	private TipoPrograma tipoPrograma;
	private PlanEstudios planEstudios;
	private Long numeroCarneAnterior;
	
	public Estudiante() {
		// TODO Auto-generated constructor stub
		super();
	}

	public Estudiante(String documentoEstudiante, Long numeroCarne,
			Long idEstudiante, PeriodoAcademico periodoIngreso,
			String claveNumero, String email, String nombreEstudiante,
			String apellidosEstudiante, Date fechaInscripcion,
			ProgramaAcademico programaAcademico, String sexo,
			TipoIdentificacion tipoDocumento, String direccionCorrespondencia,
			String telefonoCorrespondencia, String celularEstudiante,
			String emailOtro, Integer pazSalvo, Integer estado,
			Date fechaNacimiento, Integer semestre, Integer semestreNivel,
			Integer cargaMaxima, Integer horasMaxima,
			CategoriaMatricula categoriaMatricula,
			ConceptoAcademico conceptoAcademico, TipoPrograma tipoPrograma,
			PlanEstudios planEstudios) {
		super();
		this.documentoEstudiante = documentoEstudiante;
		this.numeroCarne = numeroCarne;
		this.idEstudiante = idEstudiante;
		this.periodoIngreso = periodoIngreso;
		this.claveNumero = claveNumero;
		this.email = email;
		this.nombreEstudiante = nombreEstudiante;
		this.apellidosEstudiante = apellidosEstudiante;
		this.fechaInscripcion = fechaInscripcion;
		this.programaAcademico = programaAcademico;
		this.sexo = sexo;
		this.tipoDocumento = tipoDocumento;
		this.direccionCorrespondencia = direccionCorrespondencia;
		this.telefonoCorrespondencia = telefonoCorrespondencia;
		this.celularEstudiante = celularEstudiante;
		this.emailOtro = emailOtro;
		this.pazSalvo = pazSalvo;
		this.estado = estado;
		this.fechaNacimiento = fechaNacimiento;
		this.semestre = semestre;
		this.semestreNivel = semestreNivel;
		this.cargaMaxima = cargaMaxima;
		this.horasMaxima = horasMaxima;
		this.categoriaMatricula = categoriaMatricula;
		this.conceptoAcademico = conceptoAcademico;
		this.tipoPrograma = tipoPrograma;
		this.planEstudios = planEstudios;
	}

	public String getDocumentoEstudiante() {
		return documentoEstudiante;
	}

	public void setDocumentoEstudiante(String documentoEstudiante) {
		this.documentoEstudiante = documentoEstudiante;
	}

	public Long getNumeroCarne() {
		return numeroCarne;
	}

	public void setNumeroCarne(Long numeroCarne) {
		this.numeroCarne = numeroCarne;
	}

	public Long getIdEstudiante() {
		return idEstudiante;
	}

	public void setIdEstudiante(Long idEstudiante) {
		this.idEstudiante = idEstudiante;
	}

	public PeriodoAcademico getPeriodoIngreso() {
		return periodoIngreso;
	}

	public void setPeriodoIngreso(PeriodoAcademico periodoIngreso) {
		this.periodoIngreso = periodoIngreso;
	}

	public String getClaveNumero() {
		return claveNumero;
	}

	public void setClaveNumero(String claveNumero) {
		this.claveNumero = claveNumero;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombreEstudiante() {
		return nombreEstudiante;
	}

	public void setNombreEstudiante(String nombreEstudiante) {
		this.nombreEstudiante = nombreEstudiante;
	}

	public String getApellidosEstudiante() {
		return apellidosEstudiante;
	}

	public void setApellidosEstudiante(String apellidosEstudiante) {
		this.apellidosEstudiante = apellidosEstudiante;
	}

	public Date getFechaInscripcion() {
		return fechaInscripcion;
	}

	public void setFechaInscripcion(Date fechaInscripcion) {
		this.fechaInscripcion = fechaInscripcion;
	}

	public ProgramaAcademico getProgramaAcademico() {
		return programaAcademico;
	}

	public void setProgramaAcademico(ProgramaAcademico programaAcademico) {
		this.programaAcademico = programaAcademico;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public TipoIdentificacion getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoIdentificacion tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getDireccionCorrespondencia() {
		return direccionCorrespondencia;
	}

	public void setDireccionCorrespondencia(String direccionCorrespondencia) {
		this.direccionCorrespondencia = direccionCorrespondencia;
	}

	public String getTelefonoCorrespondencia() {
		return telefonoCorrespondencia;
	}

	public void setTelefonoCorrespondencia(String telefonoCorrespondencia) {
		this.telefonoCorrespondencia = telefonoCorrespondencia;
	}

	public String getCelularEstudiante() {
		return celularEstudiante;
	}

	public void setCelularEstudiante(String celularEstudiante) {
		this.celularEstudiante = celularEstudiante;
	}

	public String getEmailOtro() {
		return emailOtro;
	}

	public void setEmailOtro(String emailOtro) {
		this.emailOtro = emailOtro;
	}

	public Integer getPazSalvo() {
		return pazSalvo;
	}

	public void setPazSalvo(Integer pazSalvo) {
		this.pazSalvo = pazSalvo;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public Integer getSemestre() {
		return semestre;
	}

	public void setSemestre(Integer semestre) {
		this.semestre = semestre;
	}

	public Integer getCargaMaxima() {
		return cargaMaxima;
	}

	public void setCargaMaxima(Integer cargaMaxima) {
		this.cargaMaxima = cargaMaxima;
	}

	public Integer getHorasMaxima() {
		return horasMaxima;
	}

	public void setHorasMaxima(Integer horasMaxima) {
		this.horasMaxima = horasMaxima;
	}

	public CategoriaMatricula getCategoriaMatricula() {
		return categoriaMatricula;
	}

	public void setCategoriaMatricula(CategoriaMatricula categoriaMatricula) {
		this.categoriaMatricula = categoriaMatricula;
	}

	public TipoPrograma getTipoPrograma() {
		return tipoPrograma;
	}

	public void setTipoPrograma(TipoPrograma tipoPrograma) {
		this.tipoPrograma = tipoPrograma;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Integer getSemestreNivel() {
		return semestreNivel;
	}

	public void setSemestreNivel(Integer semestreNivel) {
		this.semestreNivel = semestreNivel;
	}

	public PlanEstudios getPlanEstudios() {
		return planEstudios;
	}

	public void setPlanEstudios(PlanEstudios planEstudios) {
		this.planEstudios = planEstudios;
	}

	public ConceptoAcademico getConceptoAcademico() {
		return conceptoAcademico;
	}

	public void setConceptoAcademico(ConceptoAcademico conceptoAcademico) {
		this.conceptoAcademico = conceptoAcademico;
	}

	public Long getNumeroCarneAnterior() {
		return numeroCarneAnterior;
	}

	public void setNumeroCarneAnterior(Long numeroCarneAnterior) {
		this.numeroCarneAnterior = numeroCarneAnterior;
	}

}
