package com.konrad.domain;

import java.util.Date;

public class TransaccionZonaPagos {
    private Long secTransaccionZonaPagos;
    private Long idPago;
    private Long estadoPago;
    private Long idFormaPago;
    private Double valorPagado;
    private Long ticketId;
    private String idClave;
    private String idCliente;
    private String franquicia;
    private Long codigoAprobacion;
    private Long codigoServicio;
    private Long codigoBanco;
    private String nombreBanco;
    private Long codigoTransaccion;
    private Long cicloTransaccion;
    private String campo1;
    private String campo2;
    private String campo3;
    private Long idComercio;
    private Date datFecha;
    private Long idPredecesor;
    
    
    
	public TransaccionZonaPagos() {
		super();
	}
	
	public Long getSecTransaccionZonaPagos() {
		return secTransaccionZonaPagos;
	}
	public void setSecTransaccionZonaPagos(Long secTransaccionZonaPagos) {
		this.secTransaccionZonaPagos = secTransaccionZonaPagos;
	}
	public Long getIdPago() {
		return idPago;
	}
	public void setIdPago(Long idPago) {
		this.idPago = idPago;
	}
	public Long getEstadoPago() {
		return estadoPago;
	}
	public void setEstadoPago(Long estadoPago) {
		this.estadoPago = estadoPago;
	}
	public Long getIdFormaPago() {
		return idFormaPago;
	}
	public void setIdFormaPago(Long idFormaPago) {
		this.idFormaPago = idFormaPago;
	}
	public Double getValorPagado() {
		return valorPagado;
	}
	public void setValorPagado(Double valorPagado) {
		this.valorPagado = valorPagado;
	}
	public Long getTicketId() {
		return ticketId;
	}
	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}
	public String getIdClave() {
		return idClave;
	}
	public void setIdClave(String idClave) {
		this.idClave = idClave;
	}
	public String getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	public String getFranquicia() {
		return franquicia;
	}
	public void setFranquicia(String franquicia) {
		this.franquicia = franquicia;
	}
	public Long getCodigoServicio() {
		return codigoServicio;
	}
	public void setCodigoServicio(Long codigoServicio) {
		this.codigoServicio = codigoServicio;
	}
	public Long getCodigoBanco() {
		return codigoBanco;
	}
	public void setCodigoBanco(Long codigoBanco) {
		this.codigoBanco = codigoBanco;
	}
	public String getNombreBanco() {
		return nombreBanco;
	}
	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}
	public Long getCodigoTransaccion() {
		return codigoTransaccion;
	}
	public void setCodigoTransaccion(Long codigoTransaccion) {
		this.codigoTransaccion = codigoTransaccion;
	}
	public Long getCicloTransaccion() {
		return cicloTransaccion;
	}
	public void setCicloTransaccion(Long cicloTransaccion) {
		this.cicloTransaccion = cicloTransaccion;
	}
	public String getCampo1() {
		return campo1;
	}
	public void setCampo1(String campo1) {
		this.campo1 = campo1;
	}
	public String getCampo2() {
		return campo2;
	}
	public void setCampo2(String campo2) {
		this.campo2 = campo2;
	}
	public String getCampo3() {
		return campo3;
	}
	public void setCampo3(String campo3) {
		this.campo3 = campo3;
	}
	public Long getIdComercio() {
		return idComercio;
	}
	public void setIdComercio(Long idComercio) {
		this.idComercio = idComercio;
	}
	public Date getDatFecha() {
		return datFecha;
	}
	public void setDatFecha(Date datFecha) {
		this.datFecha = datFecha;
	}

	public Long getCodigoAprobacion() {
		return codigoAprobacion;
	}

	public void setCodigoAprobacion(Long codigoAprobacion) {
		this.codigoAprobacion = codigoAprobacion;
	}

	public Long getIdPredecesor() {
		return idPredecesor;
	}

	public void setIdPredecesor(Long idPredecesor) {
		this.idPredecesor = idPredecesor;
	}
	
	
	
}
