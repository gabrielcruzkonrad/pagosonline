package com.konrad.domain;

import java.util.Date;

public class Patrocinado {
	
	private Long secPatrocinado;
	private Persona persona;
	private Patrocinador patrocinador;
    private Date fechaCreacion;
    private PeriodoFacturacion periodo;
    private String estado;

	public Patrocinado() {
		// TODO Auto-generated constructor stub

	}

	public Long getSecPatrocinado() {
		return secPatrocinado;
	}

	public void setSecPatrocinado(Long secPatrocinado) {
		this.secPatrocinado = secPatrocinado;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Patrocinador getPatrocinador() {
		return patrocinador;
	}

	public void setPatrocinador(Patrocinador patrocinador) {
		this.patrocinador = patrocinador;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public PeriodoFacturacion getPeriodo() {
		return periodo;
	}

	public void setPeriodo(PeriodoFacturacion periodo) {
		this.periodo = periodo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
}
