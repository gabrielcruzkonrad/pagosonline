package com.konrad.domain;

public class TipoIdentificacion {

	private String tipoIdentificacion;			
	private String nombreTipoIdentificacion;			
	private Long formatoDian;
	
	public TipoIdentificacion() {
		super();
	
	}

	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public String getNombreTipoIdentificacion() {
		return nombreTipoIdentificacion;
	}

	public void setNombreTipoIdentificacion(String nombreTipoIdentificacion) {
		this.nombreTipoIdentificacion = nombreTipoIdentificacion;
	}

	public Long getFormatoDian() {
		return formatoDian;
	}

	public void setFormatoDian(Long formatoDian) {
		this.formatoDian = formatoDian;
	}			
	
	
}
