package com.konrad.domain;

public class ConceptoAcademico {

	private Long idConcepto;
	private String nombreConcepto;
	private Double valor;
	private CategoriaMatricula categoriaMatricula;
	private String usa;
	
	public ConceptoAcademico() {
		super();
	}
		
	public ConceptoAcademico(Long idConcepto, String nombreConcepto,
			Double valor, CategoriaMatricula categoriaMatricula, String usa) {
		super();
		this.idConcepto = idConcepto;
		this.nombreConcepto = nombreConcepto;
		this.valor = valor;
		this.categoriaMatricula = categoriaMatricula;
		this.usa = usa;
	}

	public Long getIdConcepto() {
		return idConcepto;
	}

	public void setIdConcepto(Long idConcepto) {
		this.idConcepto = idConcepto;
	}

	public String getNombreConcepto() {
		return nombreConcepto;
	}

	public void setNombreConcepto(String nombreConcepto) {
		this.nombreConcepto = nombreConcepto;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public CategoriaMatricula getCategoriaMatricula() {
		return categoriaMatricula;
	}

	public void setCategoriaMatricula(CategoriaMatricula categoriaMatricula) {
		this.categoriaMatricula = categoriaMatricula;
	}

	public String getUsa() {
		return usa;
	}

	public void setUsa(String usa) {
		this.usa = usa;
	}



}
