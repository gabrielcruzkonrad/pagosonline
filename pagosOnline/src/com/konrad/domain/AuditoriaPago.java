package com.konrad.domain;

import java.util.Date;
import com.konrad.domain.Moneda;

public class AuditoriaPago {
	
	private Long secAuditoriaPago;
	private String xId;
	private String codTienda;
	private String codLineaNegocio;
	private String codAlumno;
	private String nombreAlumno;
	private Date fechaOperacion; 
	private String tipoAccion;
	private String cuota;
	private String tipoDocumento;
	private String numDocumento;
	private Moneda moneda;
	private String saldo;
	private String mora;
	private String importeLocal;
	private String importeExtr;
	private String numPedido;
	private String numTarjeta;
	private String anyoExp;
	private String mesExp;
	private String tarjetaHabiente;
	private String emailConstancia;
	private String glosa;
	private Date fechaCreacion;
	private String usuarioCreacion;
	private Date fechaModificacion;
	private String usuarioModificacion;
	private ReciboConsignacion reciboConsignacion;
	private String eticket;
	
	
	
	public AuditoriaPago() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Long getSecAuditoriaPago() {
		return secAuditoriaPago;
	}
	public void setSecAuditoriaPago(Long secAuditoriaPago) {
		this.secAuditoriaPago = secAuditoriaPago;
	}
	public String getxId() {
		return xId;
	}
	public void setxId(String xId) {
		this.xId = xId;
	}
	public String getCodTienda() {
		return codTienda;
	}
	public void setCodTienda(String codTienda) {
		this.codTienda = codTienda;
	}
	public String getCodLineaNegocio() {
		return codLineaNegocio;
	}
	public void setCodLineaNegocio(String codLineaNegocio) {
		this.codLineaNegocio = codLineaNegocio;
	}
	public String getCodAlumno() {
		return codAlumno;
	}
	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}
	public String getNombreAlumno() {
		return nombreAlumno;
	}
	public void setNombreAlumno(String nombreAlumno) {
		this.nombreAlumno = nombreAlumno;
	}
	public Date getFechaOperacion() {
		return fechaOperacion;
	}
	public void setFechaOperacion(Date fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	public String getTipoAccion() {
		return tipoAccion;
	}
	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}
	public String getCuota() {
		return cuota;
	}
	public void setCuota(String cuota) {
		this.cuota = cuota;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNumDocumento() {
		return numDocumento;
	}
	public void setNumDocumento(String numDocumento) {
		this.numDocumento = numDocumento;
	}
	public Moneda getMoneda() {
		return moneda;
	}
	public void setMoneda(Moneda moneda) {
		this.moneda = moneda;
	}
	public String getSaldo() {
		return saldo;
	}
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}
	public String getMora() {
		return mora;
	}
	public void setMora(String mora) {
		this.mora = mora;
	}
	public String getImporteLocal() {
		return importeLocal;
	}
	public void setImporteLocal(String importeLocal) {
		this.importeLocal = importeLocal;
	}
	public String getImporteExtr() {
		return importeExtr;
	}
	public void setImporteExtr(String importeExtr) {
		this.importeExtr = importeExtr;
	}
	public String getNumPedido() {
		return numPedido;
	}
	public void setNumPedido(String numPedido) {
		this.numPedido = numPedido;
	}
	public String getNumTarjeta() {
		return numTarjeta;
	}
	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}
	public String getAnyoExp() {
		return anyoExp;
	}
	public void setAnyoExp(String anyoExp) {
		this.anyoExp = anyoExp;
	}
	public String getMesExp() {
		return mesExp;
	}
	public void setMesExp(String mesExp) {
		this.mesExp = mesExp;
	}
	public String getTarjetaHabiente() {
		return tarjetaHabiente;
	}
	public void setTarjetaHabiente(String tarjetaHabiente) {
		this.tarjetaHabiente = tarjetaHabiente;
	}
	public String getEmailConstancia() {
		return emailConstancia;
	}
	public void setEmailConstancia(String emailConstancia) {
		this.emailConstancia = emailConstancia;
	}
	public String getGlosa() {
		return glosa;
	}
	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}
	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public ReciboConsignacion getReciboConsignacion() {
		return reciboConsignacion;
	}

	public void setReciboConsignacion(ReciboConsignacion reciboConsignacion) {
		this.reciboConsignacion = reciboConsignacion;
	}

	public String getEticket() {
		return eticket;
	}

	public void setEticket(String eticket) {
		this.eticket = eticket;
	}
	
	
}
