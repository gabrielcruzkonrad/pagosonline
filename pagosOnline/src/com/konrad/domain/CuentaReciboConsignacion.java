package com.konrad.domain;

public class CuentaReciboConsignacion {
	private String tipoEntidad;			
	private String usoCuenta; 			
	private String numeroCuenta;			
	private String oficinaEntidad;			
	private String entidad;
	
	
	
	public CuentaReciboConsignacion() {
		super();
	}
	public String getTipoEntidad() {
		return tipoEntidad;
	}
	public void setTipoEntidad(String tipoEntidad) {
		this.tipoEntidad = tipoEntidad;
	}
	public String getUsoCuenta() {
		return usoCuenta;
	}
	public void setUsoCuenta(String usoCuenta) {
		this.usoCuenta = usoCuenta;
	}
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	public String getOficinaEntidad() {
		return oficinaEntidad;
	}
	public void setOficinaEntidad(String oficinaEntidad) {
		this.oficinaEntidad = oficinaEntidad;
	}
	public String getEntidad() {
		return entidad;
	}
	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}
	
}
