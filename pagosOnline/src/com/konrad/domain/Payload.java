package com.konrad.domain;

import java.util.List;

public class Payload {
	
	  private String id;
	  private String accountId;
	  private String status;
	  private String referenceCode;
	  private String description;
	  private String airlineCode;
	  private String language;
	  private String notifyUrl;
	  private ShippingAddress shippingAddress;
	  private Buyer buyer;
	  private String antifraudMerchantId;
	  private List<Transaction> transactions;
	  private AdditionalValues additionalValues;
	  
	public Payload() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public ShippingAddress getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(ShippingAddress shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public Buyer getBuyer() {
		return buyer;
	}

	public void setBuyer(Buyer buyer) {
		this.buyer = buyer;
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

	public AdditionalValues getAdditionalValues() {
		return additionalValues;
	}

	public void setAdditionalValues(AdditionalValues additionalValues) {
		this.additionalValues = additionalValues;
	}


	public String getAntifraudMerchantId() {
		return antifraudMerchantId;
	}

	public void setAntifraudMerchantId(String antifraudMerchantId) {
		this.antifraudMerchantId = antifraudMerchantId;
	}

}
