package com.konrad.domain;

import java.util.List;

public class PayloadList {

	private  List<Payload> payloadDetails;
	
	public PayloadList() {
		// TODO Auto-generated constructor stub
	}

	public List<Payload> getPayloadDetails() {
		return payloadDetails;
	}

	public void setPayloadDetails(List<Payload> payloadDetails) {
		this.payloadDetails = payloadDetails;
	}

}
