package com.konrad.domain;

import java.util.Date;

public class PeriodoFacturacion {

	private String periodo;			
	private String nombrePeriodo;			
	private String estado;			
	private String tipoPeriodo;			
	private String cierrePagina; 						
	private Date inicioDiferido;			
	private Integer mesesDiferido;
	
	
	
	public PeriodoFacturacion() {
		super();
	}
	
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getNombrePeriodo() {
		return nombrePeriodo;
	}
	public void setNombrePeriodo(String nombrePeriodo) {
		this.nombrePeriodo = nombrePeriodo;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getTipoPeriodo() {
		return tipoPeriodo;
	}
	public void setTipoPeriodo(String tipoPeriodo) {
		this.tipoPeriodo = tipoPeriodo;
	}
	public String getCierrePagina() {
		return cierrePagina;
	}
	public void setCierrePagina(String cierrePagina) {
		this.cierrePagina = cierrePagina;
	}
	public Date getInicioDiferido() {
		return inicioDiferido;
	}
	public void setInicioDiferido(Date inicioDiferido) {
		this.inicioDiferido = inicioDiferido;
	}
	public Integer getMesesDiferido() {
		return mesesDiferido;
	}
	public void setMesesDiferido(Integer mesesDiferido) {
		this.mesesDiferido = mesesDiferido;
	}			
	
	
	
}
