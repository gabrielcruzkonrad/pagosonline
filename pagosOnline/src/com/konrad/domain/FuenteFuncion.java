package com.konrad.domain;

public class FuenteFuncion {

	private Long fuenteFuncion;
	private String nombreFuenteFuncion;
	private String nombreExterno;
	private String tipoFuenteFuncion;
	private FuenteFuncion fuenteFuncionPredecesor;
	
	public Long getFuenteFuncion() {
		return fuenteFuncion;
	}
	public void setFuenteFuncion(Long fuenteFuncion) {
		this.fuenteFuncion = fuenteFuncion;
	}
	public String getNombreFuenteFuncion() {
		return nombreFuenteFuncion;
	}
	public void setNombreFuenteFuncion(String nombreFuenteFuncion) {
		this.nombreFuenteFuncion = nombreFuenteFuncion;
	}
	public FuenteFuncion getFuenteFuncionPredecesor() {
		return fuenteFuncionPredecesor;
	}
	public void setFuenteFuncionPredecesor(FuenteFuncion fuenteFuncionPredecesor) {
		this.fuenteFuncionPredecesor = fuenteFuncionPredecesor;
	}
	public String getNombreExterno() {
		return nombreExterno;
	}
	public void setNombreExterno(String nombreExterno) {
		this.nombreExterno = nombreExterno;
	}
	public String getTipoFuenteFuncion() {
		return tipoFuenteFuncion;
	}
	public void setTipoFuenteFuncion(String tipoFuenteFuncion) {
		this.tipoFuenteFuncion = tipoFuenteFuncion;
	}
	
	
	
}
