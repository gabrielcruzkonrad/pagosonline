package com.konrad.domain;

public class PeriodoAcademico {

	private String periodoAcademico;
	private String descripcionPeriodo;
	private Integer secuencia;
	private Integer activo;
		
	
	
	public PeriodoAcademico(String periodoAcademico, String descripcionPeriodo,
			Integer secuencia, Integer activo) {
		super();
		this.periodoAcademico = periodoAcademico;
		this.descripcionPeriodo = descripcionPeriodo;
		this.secuencia = secuencia;
		this.activo = activo;
	}

	public PeriodoAcademico() {
		// TODO Auto-generated constructor stub
	}
	
	public String getPeriodoAcademico() {
		return periodoAcademico;
	}


	public void setPeriodoAcademico(String periodoAcademico) {
		this.periodoAcademico = periodoAcademico;
	}


	public String getDescripcionPeriodo() {
		return descripcionPeriodo;
	}


	public void setDescripcionPeriodo(String descripcionPeriodo) {
		this.descripcionPeriodo = descripcionPeriodo;
	}


	public Integer getSecuencia() {
		return secuencia;
	}


	public void setSecuencia(Integer secuencia) {
		this.secuencia = secuencia;
	}


	public Integer getActivo() {
		return activo;
	}


	public void setActivo(Integer activo) {
		this.activo = activo;
	}

}
