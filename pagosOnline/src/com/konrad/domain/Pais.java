package com.konrad.domain;

public class Pais {

	private Long pais;
	private String nombrePais;
	private String origen; 
	private String gentilicio;
	
	public Pais() {
		// TODO Auto-generated constructor stub
	}
	
	public Pais(Long pais, String nombrePais, String origen, String gentilicio) {
		super();
		this.pais = pais;
		this.nombrePais = nombrePais;
		this.origen = origen;
		this.gentilicio = gentilicio;
	}

	public Long getPais() {
		return pais;
	}


	public void setPais(Long pais) {
		this.pais = pais;
	}


	public String getNombrePais() {
		return nombrePais;
	}


	public void setNombrePais(String nombrePais) {
		this.nombrePais = nombrePais;
	}


	public String getOrigen() {
		return origen;
	}


	public void setOrigen(String origen) {
		this.origen = origen;
	}


	public String getGentilicio() {
		return gentilicio;
	}


	public void setGentilicio(String gentilicio) {
		this.gentilicio = gentilicio;
	}

}
