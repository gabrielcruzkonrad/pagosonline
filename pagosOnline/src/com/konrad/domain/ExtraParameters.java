package com.konrad.domain;

public class ExtraParameters {

	
   private String RESPONSE_URL;
   private String INSTALLMENTS_NUMBER;
   private String CHECKOUT_VERSION;
	
	public ExtraParameters() {
		// TODO Auto-generated constructor stub
	}

	public String getRESPONSE_URL() {
		return RESPONSE_URL;
	}

	public void setRESPONSE_URL(String rESPONSE_URL) {
		RESPONSE_URL = rESPONSE_URL;
	}

	public String getINSTALLMENTS_NUMBER() {
		return INSTALLMENTS_NUMBER;
	}

	public void setINSTALLMENTS_NUMBER(String iNSTALLMENTS_NUMBER) {
		INSTALLMENTS_NUMBER = iNSTALLMENTS_NUMBER;
	}

	public String getCHECKOUT_VERSION() {
		return CHECKOUT_VERSION;
	}

	public void setCHECKOUT_VERSION(String cHECKOUT_VERSION) {
		CHECKOUT_VERSION = cHECKOUT_VERSION;
	}

}
