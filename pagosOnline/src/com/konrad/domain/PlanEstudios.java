package com.konrad.domain;

import java.util.Date;

public class PlanEstudios {

	private Long idPlan;
	private ProgramaAcademico programaAcademico;
	private String codigoPlan;
	private Date fechaInicial;
	private Date fechaFinal;
	private Date fechaExpiracion;
	private String descripcionPlan;
	private String activo;
	private Integer horasMaximo;
	private String emailPlan;
	private String emailPlanAlterno;
	
	
	public PlanEstudios(Long idPlan, ProgramaAcademico programaAcademico,
			String codigoPlan, Date fechaInicial, Date fechaFinal,
			Date fechaExpiracion, String descripcionPlan, String activo,
			Integer horasMaximo, String emailPlan, String emailPlanAlterno) {
		super();
		this.idPlan = idPlan;
		this.programaAcademico = programaAcademico;
		this.codigoPlan = codigoPlan;
		this.fechaInicial = fechaInicial;
		this.fechaFinal = fechaFinal;
		this.fechaExpiracion = fechaExpiracion;
		this.descripcionPlan = descripcionPlan;
		this.activo = activo;
		this.horasMaximo = horasMaximo;
		this.emailPlan = emailPlan;
		this.emailPlanAlterno = emailPlanAlterno;
	}

	public PlanEstudios() {
		// TODO Auto-generated constructor stub
	}

	public Long getIdPlan() {
		return idPlan;
	}

	public void setIdPlan(Long idPlan) {
		this.idPlan = idPlan;
	}

	public ProgramaAcademico getProgramaAcademico() {
		return programaAcademico;
	}

	public void setProgramaAcademico(ProgramaAcademico programaAcademico) {
		this.programaAcademico = programaAcademico;
	}

	public String getCodigoPlan() {
		return codigoPlan;
	}

	public void setCodigoPlan(String codigoPlan) {
		this.codigoPlan = codigoPlan;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public Date getFechaExpiracion() {
		return fechaExpiracion;
	}

	public void setFechaExpiracion(Date fechaExpiracion) {
		this.fechaExpiracion = fechaExpiracion;
	}

	public String getDescripcionPlan() {
		return descripcionPlan;
	}

	public void setDescripcionPlan(String descripcionPlan) {
		this.descripcionPlan = descripcionPlan;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public Integer getHorasMaximo() {
		return horasMaximo;
	}

	public void setHorasMaximo(Integer horasMaximo) {
		this.horasMaximo = horasMaximo;
	}

	public String getEmailPlan() {
		return emailPlan;
	}

	public void setEmailPlan(String emailPlan) {
		this.emailPlan = emailPlan;
	}

	public String getEmailPlanAlterno() {
		return emailPlanAlterno;
	}

	public void setEmailPlanAlterno(String emailPlanAlterno) {
		this.emailPlanAlterno = emailPlanAlterno;
	}
	
}
