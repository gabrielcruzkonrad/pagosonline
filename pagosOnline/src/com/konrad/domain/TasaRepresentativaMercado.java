package com.konrad.domain;

import java.util.Date;

import com.konrad.util.Moneda;

public class TasaRepresentativaMercado {

	private Moneda moneda;
	private Date fecha;
	private Double valor;
	private Boolean festivo;
	
	
	public TasaRepresentativaMercado() {
		super();
	}
	public Moneda getMoneda() {
		return moneda;
	}
	public void setMoneda(Moneda moneda) {
		this.moneda = moneda;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Boolean getFestivo() {
		return festivo;
	}
	public void setFestivo(Boolean festivo) {
		this.festivo = festivo;
	}
	
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	
}
