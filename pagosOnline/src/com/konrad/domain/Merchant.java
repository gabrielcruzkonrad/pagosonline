package com.konrad.domain;

public class Merchant {

	private String apiLogin;
	private String apiKey;
	
	public Merchant() {
		// TODO Auto-generated constructor stub
	}
	
	public String getApiLogin() {
		return apiLogin;
	}

	public void setApiLogin(String apiLogin) {
		this.apiLogin = apiLogin;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}



}
