package com.konrad.domain;

import java.util.Date;

public class Patrocinador {

	private Long secPatrocinador;
    private TipoIdentificacion tipoIdentificacion;
    private String identificacion;
    private String nombreRazonSocial;
    private String primerApellido; 
    private String segundoApellido;
    private Date fechaCreacion;
    private Date fechaConstitucion;
    private String archivoRut;
    private Persona persona;
    private String direccionElectronica;
    private String telefonoResidencia;
    private String contactoPatrocinador;
    private String estado;
   
	public Patrocinador() {
		// TODO Auto-generated constructor stub
	}
    
    public Long getSecPatrocinador() {
		return secPatrocinador;
	}

	public void setSecPatrocinador(Long secPatrocinador) {
		this.secPatrocinador = secPatrocinador;
	}

	public TipoIdentificacion getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(TipoIdentificacion tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getNombreRazonSocial() {
		return nombreRazonSocial;
	}

	public void setNombreRazonSocial(String nombreRazonSocial) {
		this.nombreRazonSocial = nombreRazonSocial;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaConstitucion() {
		return fechaConstitucion;
	}

	public void setFechaConstitucion(Date fechaConstitucion) {
		this.fechaConstitucion = fechaConstitucion;
	}

	public String getArchivoRut() {
		return archivoRut;
	}

	public void setArchivoRut(String archivoRut) {
		this.archivoRut = archivoRut;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public String getDireccionElectronica() {
		return direccionElectronica;
	}

	public void setDireccionElectronica(String direccionElectronica) {
		this.direccionElectronica = direccionElectronica;
	}

	public String getTelefonoResidencia() {
		return telefonoResidencia;
	}

	public void setTelefonoResidencia(String telefonoResidencia) {
		this.telefonoResidencia = telefonoResidencia;
	}

	public String getContactoPatrocinador() {
		return contactoPatrocinador;
	}

	public void setContactoPatrocinador(String contactoPatrocinador) {
		this.contactoPatrocinador = contactoPatrocinador;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
}
