package com.konrad.domain;

import org.zkoss.zul.Image;

public class Mensaje {

	private Image imagen;
	private String mensaje;
		
	public Mensaje() {
		super();
	}


	public Mensaje(Image imagen, String mensaje) {
		super();
		this.imagen = imagen;
		this.mensaje = mensaje;
	}
	
	
	public Image getImagen() {
		return imagen;
	}
	public void setImagen(Image imagen) {
		this.imagen = imagen;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
