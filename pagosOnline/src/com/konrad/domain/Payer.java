package com.konrad.domain;

public class Payer {

	private String merchantPayerId;
    private String fullName;
    private ShippingAddress billingAddress;
    private String emailAddress;
    private String contactPhone;
    private String dniNumber;
	
	public Payer() {
		// TODO Auto-generated constructor stub
	}

	public String getMerchantPayerId() {
		return merchantPayerId;
	}

	public void setMerchantPayerId(String merchantPayerId) {
		this.merchantPayerId = merchantPayerId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public ShippingAddress getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(ShippingAddress billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getDniNumber() {
		return dniNumber;
	}

	public void setDniNumber(String dniNumber) {
		this.dniNumber = dniNumber;
	}

}
