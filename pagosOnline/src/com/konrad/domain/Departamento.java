package com.konrad.domain;

public class Departamento {

	private Pais pais;
	private Long departamento;
	private String nombreDepartamento;
	private String gentilicio;
	
	public Departamento() {
		// TODO Auto-generated constructor stub
	}
	
	public Departamento(Pais pais, Long departamento,
			String nombreDepartamento, String gentilicio) {
		super();
		this.pais = pais;
		this.departamento = departamento;
		this.nombreDepartamento = nombreDepartamento;
		this.gentilicio = gentilicio;
	}

	public Pais getPais() {
		return pais;
	}


	public void setPais(Pais pais) {
		this.pais = pais;
	}


	public Long getDepartamento() {
		return departamento;
	}


	public void setDepartamento(Long departamento) {
		this.departamento = departamento;
	}


	public String getNombreDepartamento() {
		return nombreDepartamento;
	}


	public void setNombreDepartamento(String nombreDepartamento) {
		this.nombreDepartamento = nombreDepartamento;
	}


	public String getGentilicio() {
		return gentilicio;
	}


	public void setGentilicio(String gentilicio) {
		this.gentilicio = gentilicio;
	}




}
