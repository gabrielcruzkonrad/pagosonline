package com.konrad.domain;

import java.util.List;

public class Result {

	private List<Payload> payload;
	
	public Result() {
		// TODO Auto-generated constructor stub
	}
	
	public List<Payload> getPayload() {
		return payload;
	}
	
	public void setPayload(List<Payload> payload) {
		this.payload = payload;
	}

}
