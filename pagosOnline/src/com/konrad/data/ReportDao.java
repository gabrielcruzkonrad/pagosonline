package com.konrad.data;

import java.sql.Connection;
import java.util.Collection;

import org.apache.ibatis.session.SqlSession;



public class ReportDao extends DaoStandard {

    private SqlSession session = sqlSessionFactory.openSession(true);

    public Collection<Object> obtenerListado(String sqlName) throws Exception {
	try {
	    return session.selectList(sqlName);
	} finally {
	    session.close();
	}
    }

    public Collection<Object> obtenerListado(String sqlName, Object object)
	    throws Exception {
	try {
	    return session.selectList(sqlName, object);
	} finally {
	    session.close();
	}
    }

    @Override
    public Object obtenerRegistro(String sqlName) throws Exception {
	try {
	    return session.selectOne(sqlName);
	} finally {
	    session.close();
	}
    }

    @Override
    public Object obtenerRegistro(String sqlName, Object object)
	    throws Exception {
	try {
	    return session.selectOne(sqlName, object);
	} finally {
	    session.close();
	}
    }

    @Override
    public Object ejecutarProcedimiento(String sqlName) throws Exception {
	try {
	    return session.update(sqlName);
	} finally {
	    session.close();
	}
    }

    @Override
    public Object ejecutarProcedimiento(String sqlName, Object object)
	    throws Exception {
	try {
	    return session.update(sqlName, object);
	} finally {
	    session.close();
	}
    }

    public Object validarSQL(String sql) throws Exception {
	try {
	    Connection con = session.getConnection();
	    return con.prepareStatement(sql);
	} finally {
	    session.close();
	}
    }

    public Connection retornarConexion(){
    	return this.session.getConnection();
    }

}
