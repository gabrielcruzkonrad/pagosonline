package com.konrad.data;

import java.sql.Connection;
import java.util.List;

public class ReportMgr extends ManagerStandard {

    public List<Object> obtenerListado(String qryName, Object objeto)
	    throws Exception {
	ReportDao reportDao = new ReportDao();
	return (List<Object>) reportDao
		.obtenerListado(qryName, objeto);
    }

    public List<Object> obtenerListado(String qryName) throws Exception {
    	ReportDao reportDao = new ReportDao();
	return (List<Object>) reportDao.obtenerListado(qryName);
    }

    public Object obtenerRegistro(String qryName, Object objeto)
	    throws Exception {
    	ReportDao reportDao = new ReportDao();
	return reportDao.obtenerRegistro(qryName, objeto);
    }

    public Object obtenerRegistro(String qryName) throws Exception {
    	ReportDao reportDao = new ReportDao();
	return reportDao.obtenerRegistro(qryName);
    }

    public Object ejecutarProcedimiento(String qryName, Object objeto)
	    throws Exception {
    	ReportDao reportDao = new ReportDao();
	return reportDao.ejecutarProcedimiento(qryName, objeto);
    }

    public Object ejecutarProcedimiento(String qryName) throws Exception {
    	ReportDao reportDao = new ReportDao();
	return reportDao.ejecutarProcedimiento(qryName);
    }

    public Object validarSQL(String sql) throws Exception {
    	ReportDao reportDao = new ReportDao();
	return reportDao.validarSQL(sql);
    }

    public Connection retornarConexion(){
    	ReportDao reportDao = new ReportDao();
    	return reportDao.retornarConexion();
    }
}
