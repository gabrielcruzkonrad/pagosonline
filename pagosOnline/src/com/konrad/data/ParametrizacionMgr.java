package com.konrad.data;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;

public class ParametrizacionMgr extends ManagerStandard {

    public List<Object> obtenerListado(String qryName, Object objeto)
	    throws Exception {
	ParametrizacionDao parametrizacionDao = new ParametrizacionDao();
	return (List<Object>) parametrizacionDao
		.obtenerListado(qryName, objeto);
    }

    public List<Object> obtenerListado(String qryName) throws Exception {
	ParametrizacionDao parametrizacionDao = new ParametrizacionDao();
	return (List<Object>) parametrizacionDao.obtenerListado(qryName);
    }

    public Object obtenerRegistro(String qryName, Object objeto)
	    throws Exception {
	ParametrizacionDao parametrizacionDao = new ParametrizacionDao();
	return parametrizacionDao.obtenerRegistro(qryName, objeto);
    }

    public Object obtenerRegistro(String qryName) throws Exception {
	ParametrizacionDao parametrizacionDao = new ParametrizacionDao();
	return parametrizacionDao.obtenerRegistro(qryName);
    }

    public Object ejecutarProcedimiento(String qryName, Object objeto)
	    throws Exception {
	ParametrizacionDao parametrizacionDao = new ParametrizacionDao();
	return parametrizacionDao.ejecutarProcedimiento(qryName, objeto);
    }

    public Object insertarRegistro(String qryName, Object objeto)
	    throws Exception {
	ParametrizacionDao parametrizacionDao = new ParametrizacionDao();
	return parametrizacionDao.insertarRegistro(qryName, objeto);
    }

    public Object actualizarRegistro(String qryName, Object objeto)
	    throws Exception {
	ParametrizacionDao parametrizacionDao = new ParametrizacionDao();
	return parametrizacionDao.actualizarRegistro(qryName, objeto);
    }

    public Object borrarRegistro(String qryName, Object objeto)
	    throws Exception {
	ParametrizacionDao parametrizacionDao = new ParametrizacionDao();
	return parametrizacionDao.borrarRegistro(qryName, objeto);
    }

    public Object ejecutarProcedimiento(String qryName) throws Exception {
	ParametrizacionDao parametrizacionDao = new ParametrizacionDao();
	return parametrizacionDao.ejecutarProcedimiento(qryName);
    }

    public Object validarSQL(String sql) throws Exception {
	ParametrizacionDao parametrizacionDao = new ParametrizacionDao();
	return parametrizacionDao.validarSQL(sql);
    }

    
    @Override
    public Collection<Object> obtenerListado(String sqlName, Object objeto,
            int omitir, int maximo) throws Exception {
	ParametrizacionDao parametrizacionDao = new ParametrizacionDao();
	return (List<Object>) parametrizacionDao.obtenerListado(sqlName,objeto,
		omitir, maximo);
    }
    
   
    public Connection obtenerConexion() throws Exception{
    	ParametrizacionDao parametrizacionDao = new ParametrizacionDao();
    	return parametrizacionDao.obtenerConexion();
    }
  
}
