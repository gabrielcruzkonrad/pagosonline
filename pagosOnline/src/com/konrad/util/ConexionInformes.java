package com.konrad.util;



import java.sql.*;
import java.util.ResourceBundle;


public class ConexionInformes {
	
	private static final Connection conexion;
	
	static {
        try {

        	String arcvhivoconfiguracion ="fukl.siii.properties.siii-conexion";
			ResourceBundle rb=ResourceBundle.getBundle(arcvhivoconfiguracion);
            String driver = rb.getString("driver");
            String url = rb.getString("url");
            String usuario = rb.getString("username");
            String clave = rb.getString("password");
            Class.forName(driver);
            conexion = DriverManager.getConnection(url, usuario, clave);

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error inicializando conexion: " + 
                                       e);
        }
    }
    public static Connection getConexion() {
        return conexion;
    }


}
