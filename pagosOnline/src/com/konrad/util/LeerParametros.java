package com.konrad.util;

import java.util.ResourceBundle;

public class LeerParametros {
	private static final String rutadocumentos;
	
	static {
		try {
		String arcvhivoconfiguracion ="fukl.siii.properties.documentos";
		ResourceBundle rb=ResourceBundle.getBundle(arcvhivoconfiguracion);
		rutadocumentos=rb.getString("RUTA");
		} catch (Exception e) {
		e.printStackTrace();
		throw new RuntimeException ("Error initializing archivo  de propiedades de archivos: " + e);
		}
		}
	
	public static String getRutaDocumentos () {
		return rutadocumentos;
		}

}
