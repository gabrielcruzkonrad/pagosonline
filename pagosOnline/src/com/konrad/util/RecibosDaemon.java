package com.konrad.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.core.MediaType;
import javax.xml.rpc.holders.IntHolder;
import javax.xml.rpc.holders.StringHolder;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.konrad.domain.AuditoriaPago;
import com.konrad.domain.Details;
import com.konrad.domain.Merchant;
import com.konrad.domain.Moneda;
import com.konrad.domain.PagosOnline;
import com.konrad.domain.Payload;
import com.konrad.domain.PingRequest;
import com.konrad.domain.ReferenceResponse;
import com.konrad.domain.TransaccionZonaPagos;
import com.konrad.domain.Transaction;
import com.konrad.domain.TransactionResponse;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.helper.AppContext;
import com.konrad.helper.ReciboConsignacionHelper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.zonapagos.www.ws_verificar_pagos.holders.prod.ArrayOfPagos_v3Holder;
import com.zonapagos.www.ws_verificar_pagos.prod.ServiceSoapProxy;

public class RecibosDaemon extends Thread {
	private static final RecibosDaemon recibosDaemon= new RecibosDaemon();
	private TransaccionZonaPagos transaccionZonaPagos;
	protected static Logger log = Logger.getLogger(RecibosDaemon.class);
	
	private RecibosDaemon() {
		super();
	}


	public static RecibosDaemon getInstance(){
			return recibosDaemon;
	}
	
	
	@Override
	public void run() {
		try {
			while(true){
			Thread.sleep((60000)*60);
			this.setPriority(Thread.MAX_PRIORITY);
			this.verificarEstadoPagos("Verificaci�n a efectuarse en este instante");
//			this.verificarEstadoPagos();
//			this.verificarEstadoPagosEstudio();
//			this.verificarEstadoPagosSinIniciar();
			this.verificarTransaccionesPagosPendientes();
		//	this.printPagoPorReferencia("677237014");
		//	this.printPagoPorReferencia("1502291858147");
			}
			
		} catch (InterruptedException e) {
			log.info("Error por interrupci�n en demonio --> "+e.getMessage());
			e.printStackTrace();
			
		} catch (Exception e) {
			log.info("Error por excepci�n  en demonio --> "+e.getMessage());
			e.printStackTrace();
		}
		
	}

	public void verificarEstadoPagos(String mensaje){
		log.info("Este es el mensaje a verificar para pruebas de Demonio: "+mensaje);
	}
	
	
	@SuppressWarnings("unchecked")
	public void verificarEstadoPagosSinIniciar(){
		try{
		TransaccionZonaPagos transaccion = new TransaccionZonaPagos();
		transaccion.setEstadoPago(new Long(IConstantes.ESTADO_PENDIENTE_INICIAR));
		this.setTransaccionZonaPagos(transaccion);
		List<TransaccionZonaPagos> listaTransacciones = new ArrayList <TransaccionZonaPagos>();
		listaTransacciones = ((List<TransaccionZonaPagos>)ParametrizacionFac.getFacade().obtenerListado("selectTransaccionZonaPagosFecha",this.getTransaccionZonaPagos()));
		for(TransaccionZonaPagos objeto: listaTransacciones){
			log.info("Empezar a procesar pagos pendientes Inicio");
			if(objeto!= null){
				if(objeto.getIdPago()>0L){
					if(ReciboConsignacionHelper.getHelper().getServicioPruebasProduccion()){
						ServiceSoapProxy servicioVerificacion = new ServiceSoapProxy();
						ArrayOfPagos_v3Holder res_pagos_v3 = new ArrayOfPagos_v3Holder();
						IntHolder int_error = new IntHolder();
						int int_id_tienda= new Integer(objeto.getIdComercio().toString());
						StringHolder str_error = new StringHolder();
						String str_id_pago = new String(objeto.getIdPago().toString());
						String str_id_clave = new String(objeto.getIdComercio().toString().equals(IConstantes.ID_TIENDA)?
															IConstantes.CLAVE_SERVICIO:
														objeto.getIdComercio().toString().equals(IConstantes.ID_TIENDA_BANCOLOMBIA)?
															IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
														objeto.getIdComercio().toString().equals(IConstantes.ID_TIENDA_DOLARES)?
																IConstantes.CLAVE_SERVICIO_DOLARES:
																IConstantes.CLAVE_SERVICIO_EURO	
																);
						Integer estadoArrayPago;
						int estadoPago = servicioVerificacion.verificar_pago_v3(str_id_pago, int_id_tienda, str_id_clave, res_pagos_v3, int_error, str_error); 
						log.info("encuentra pago: "+estadoPago);
						if (estadoPago>0){ //Significa que encontr� un pago
							estadoArrayPago = (res_pagos_v3.value[0]!=null?
												(((Integer)res_pagos_v3.value[0].getInt_estado_pago())!=null?
												res_pagos_v3.value[0].getInt_estado_pago()
												:0)
												:0);
						if (res_pagos_v3.value[0]!=null){
							
							objeto.setIdFormaPago((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()!=null?
									((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()).longValue():new Long(0L));
							objeto.setValorPagado((Double)res_pagos_v3.value[0].getDbl_valor_pagado()!=null?
									((Double)res_pagos_v3.value[0].getDbl_valor_pagado()):new Double(0.00));
							objeto.setTicketId(res_pagos_v3.value[0].getStr_ticketID()!= null?
									!res_pagos_v3.value[0].getStr_ticketID().equals("")?
									new Long(res_pagos_v3.value[0].getStr_ticketID()):new Long(0L):new Long(0L));
							objeto.setIdClave(res_pagos_v3.value[0].getStr_id_clave());
							objeto.setIdCliente(res_pagos_v3.value[0].getStr_id_cliente());
							objeto.setFranquicia(res_pagos_v3.value[0].getStr_franquicia()!=null?!res_pagos_v3.value[0].getStr_franquicia().equals("")?
									res_pagos_v3.value[0].getStr_franquicia():new String("0"):new String("0"));
							objeto.setCodigoAprobacion((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()!=null?
									((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()).longValue():new Long(0L));
							objeto.setCodigoServicio((Integer)res_pagos_v3.value[0].getInt_codigo_servico()!=null?
									((Integer)res_pagos_v3.value[0].getInt_codigo_servico()).longValue():new Long(0L));
							objeto.setCodigoBanco((Integer)res_pagos_v3.value[0].getInt_codigo_banco()!=null?
									((Integer)res_pagos_v3.value[0].getInt_codigo_banco()).longValue():new Long(0L));
							objeto.setNombreBanco(res_pagos_v3.value[0].getStr_nombre_banco()!=null?!res_pagos_v3.value[0].getStr_nombre_banco().equals("")?
									res_pagos_v3.value[0].getStr_nombre_banco():new String("0"):new String("0"));
							objeto.setCodigoTransaccion(res_pagos_v3.value[0].getStr_codigo_transaccion()!=null?
									!res_pagos_v3.value[0].getStr_codigo_transaccion().equals("")?
									new Long(res_pagos_v3.value[0].getStr_codigo_transaccion()):new Long(0L):new Long(0L));
							
							objeto.setCicloTransaccion((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()!=null?
									((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()).longValue():new Long(0L));
							objeto.setCampo1(res_pagos_v3.value[0].getStr_campo1());
							objeto.setCampo2(res_pagos_v3.value[0].getStr_campo2());
							objeto.setCampo3(res_pagos_v3.value[0].getStr_campo3());
							objeto.setDatFecha(objeto.getDatFecha()!=null?objeto.getDatFecha():new Date());
							}
							log.info("estado array: "+estadoArrayPago);
							if (estadoArrayPago.longValue() != objeto.getEstadoPago()) {
								objeto.setEstadoPago(estadoArrayPago.longValue());
							}
							if(!estadoArrayPago.toString().equals(IConstantes.ESTADO_PENDIENTE_INICIAR) && int_error.value == 0){
								try{
								ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
								}catch(Exception e){
									e.printStackTrace();
								}
							}	
						}else{
							if(int_error.value==1){
								objeto.setEstadoPago(0L);
								try{
								ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}
					} else{
						com.zonapagos.www.ws_verificar_pagos.test.ServiceSoapProxy servicioVerificacion = new com.zonapagos.www.ws_verificar_pagos.test.ServiceSoapProxy();
						com.zonapagos.www.ws_verificar_pagos.holders.test.ArrayOfPagos_v3Holder res_pagos_v3 = new com.zonapagos.www.ws_verificar_pagos.holders.test.ArrayOfPagos_v3Holder();
						IntHolder int_error = new IntHolder();
						int int_id_tienda= new Integer(objeto.getIdComercio().toString());
						StringHolder str_error = new StringHolder();
						String str_id_pago = new String(objeto.getIdPago().toString());
						String str_id_clave = new String(IConstantes.CLAVE_SERVICIO_PRUEBAS);
						Integer estadoArrayPago;
						int estadoPago = servicioVerificacion.verificar_pago_v3(str_id_pago, int_id_tienda, str_id_clave, res_pagos_v3, int_error, str_error); 
						log.info("encuentra pago: "+estadoPago);
						if (estadoPago>0){ //Significa que encontr� un pago
							estadoArrayPago = (res_pagos_v3.value[0]!=null?
												(((Integer)res_pagos_v3.value[0].getInt_estado_pago())!=null?
												res_pagos_v3.value[0].getInt_estado_pago()
												:0)
												:0);
						
							if (res_pagos_v3.value[0]!=null){
								objeto.setIdFormaPago((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()!=null?
										((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()).longValue():new Long(0L));
								objeto.setValorPagado((Double)res_pagos_v3.value[0].getDbl_valor_pagado()!=null?
										((Double)res_pagos_v3.value[0].getDbl_valor_pagado()):new Double(0.00));
								objeto.setTicketId(res_pagos_v3.value[0].getStr_ticketID()!= null?
										!res_pagos_v3.value[0].getStr_ticketID().equals("")?
										new Long(res_pagos_v3.value[0].getStr_ticketID()):new Long(0L):new Long(0L));
								objeto.setIdClave(res_pagos_v3.value[0].getStr_id_clave());
								objeto.setIdCliente(res_pagos_v3.value[0].getStr_id_cliente());
								objeto.setFranquicia(res_pagos_v3.value[0].getStr_franquicia()!=null?!res_pagos_v3.value[0].getStr_franquicia().equals("")?
										res_pagos_v3.value[0].getStr_franquicia():new String("0"):new String("0"));
								objeto.setCodigoAprobacion((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()!=null?
										((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()).longValue():new Long(0L));
								objeto.setCodigoServicio((Integer)res_pagos_v3.value[0].getInt_codigo_servico()!=null?
										((Integer)res_pagos_v3.value[0].getInt_codigo_servico()).longValue():new Long(0L));
								objeto.setCodigoBanco((Integer)res_pagos_v3.value[0].getInt_codigo_banco()!=null?
										((Integer)res_pagos_v3.value[0].getInt_codigo_banco()).longValue():new Long(0L));
								objeto.setNombreBanco(res_pagos_v3.value[0].getStr_nombre_banco()!=null?!res_pagos_v3.value[0].getStr_nombre_banco().equals("")?
										res_pagos_v3.value[0].getStr_nombre_banco():new String("0"):new String("0"));
								objeto.setCodigoTransaccion(res_pagos_v3.value[0].getStr_codigo_transaccion()!=null?
										!res_pagos_v3.value[0].getStr_codigo_transaccion().equals("")?
										new Long(res_pagos_v3.value[0].getStr_codigo_transaccion()):new Long(0L):new Long(0L));
								
								objeto.setCicloTransaccion((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()!=null?
										((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()).longValue():new Long(0L));
								objeto.setCampo1(res_pagos_v3.value[0].getStr_campo1());
								objeto.setCampo2(res_pagos_v3.value[0].getStr_campo2());
								objeto.setCampo3(res_pagos_v3.value[0].getStr_campo3());
								objeto.setDatFecha(objeto.getDatFecha()!=null?objeto.getDatFecha():new Date());
								}
						
							log.info("estado array: "+estadoArrayPago);
							if (estadoArrayPago.longValue() != objeto.getEstadoPago()) {
								objeto.setEstadoPago(estadoArrayPago.longValue());
							}
								if(!estadoArrayPago.toString().equals(IConstantes.ESTADO_PENDIENTE_INICIAR) && int_error.value == 0){
									try{
									ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
									} catch(Exception e){
										e.printStackTrace();
									}
								}
						} else{
							if(int_error.value==1){
								objeto.setEstadoPago(0L);
								try{
								ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}
						
					}
				}
			}
		}
		
	} catch (InterruptedException e) {
		log.info("Error por interrupci�n en verificarPagosSinIniciar--> "+e.getMessage());
		e.printStackTrace();
	} catch (Exception e) {
		log.info("Error por excepci�n en verificarPagosSinIniciar--> "+e.getMessage());
		e.printStackTrace();
	}

		
}
	
	
	@SuppressWarnings("unchecked")
	public List<AuditoriaPago> getListAuditoriaPagoPendiente(){
		List<AuditoriaPago> listaAuditoriaPago = new ArrayList<AuditoriaPago>();
		try{
			
			listaAuditoriaPago = (List<AuditoriaPago>)ParametrizacionFac.getFacade().obtenerListado("selectListAuditoriaPagoPendiente");
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}
		
		return listaAuditoriaPago;
		
		}
	
	public void verificarTransaccionesPagosPendientes(){
		List<AuditoriaPago> listaAuditoriaPago = new ArrayList<AuditoriaPago>();
		try{
			
			listaAuditoriaPago = this.getListAuditoriaPagoPendiente();
			if(listaAuditoriaPago !=null){
				if(listaAuditoriaPago.size()>0){
			    	for(AuditoriaPago auditoria : listaAuditoriaPago){
			    		log.info("Aud -> Rec. "+auditoria.getReciboConsignacion().getReciboConsignacion());
			    		this.verificarPagoPorReferencia(auditoria);
			    	}
				}
			}

		}catch(Exception e){
			e.printStackTrace();
			log.error(e.getMessage());
		}
	}
	
	
	public void actualizarAuditoriaPagoCondicional(AuditoriaPago auditoria, 
			TransactionResponse transactionResponse ,
			List<Transaction> transacciones){
		try{
		String estadoConfirmado =null;
		String estadoRechazado =null;
		String tipoMedioPago ="";
		String medioPago ="";
		String bancoPse ="";
		String numeroTarjetaCredito = "";
		Boolean pagoExitoso = false;
		TransactionResponse transactionResponseLocal;
		
		
		if (transacciones!= null){
			if(transacciones.size()>0){
				for(Transaction transaccion : transacciones ){
					if(transaccion.getTransactionResponse()!= null) {
						transactionResponseLocal = transaccion.getTransactionResponse();
						
						if(transactionResponseLocal.getState().equalsIgnoreCase(IConstantes.APPROVED_TRANSACTION)){
							estadoConfirmado = IConstantes.ESTADO_CONFIRMADO_DEFAULT;
							pagoExitoso = true;
						} else if(transactionResponseLocal.getState().equalsIgnoreCase(IConstantes.DECLINED_TRANSACTION)){
							estadoRechazado = IConstantes.ESTADO_RECHAZADO_DEFAULT;
						} else if(transactionResponseLocal.getState().equalsIgnoreCase(IConstantes.PENDING_TRANSACTION)){
							estadoRechazado = IConstantes.ESTADO_PENDIENTE_DEFAULT;
						} else if(transactionResponseLocal.getState().equalsIgnoreCase(IConstantes.EXPIRED_TRANSACTION)){
							estadoRechazado = IConstantes.ESTADO_EXPIRADO_DEFAULT;
						}else if(transactionResponseLocal.getState().equalsIgnoreCase(IConstantes.ERROR_TRANSACTION)){
							estadoRechazado = IConstantes.ESTADO_ERROR_DEFAULT;
						}
					}
					if(transaccion.getPaymentMethod()!=null){
						
						if(!transaccion.getPaymentMethod().toUpperCase().contains(IConstantes.PSE_PREFIX)){
							numeroTarjetaCredito = transaccion.getCreditCard().getMaskedNumber();
						}
						
						if(pagoExitoso) {
							if(transaccion.getPaymentMethod().toUpperCase().contains(IConstantes.PSE_PREFIX)){
								tipoMedioPago = IConstantes.TIPO_MEDIO_PAGO_PSE_PAYU;
								medioPago = IConstantes.MEDIO_PAGO_PSE_PAYU;
								bancoPse= IConstantes.BANCO_PSE_DEFAULT_PAYU;
							} else if(transaccion.getPaymentMethod().toUpperCase().contains(IConstantes.VISA_PREFIX)){
								tipoMedioPago = IConstantes.TIPO_MEDIO_PAGO_TC_PAYU;
								medioPago = IConstantes.MEDIO_PAGO_TC_VISA_PAYU;
								bancoPse= "0";
							} else if(transaccion.getPaymentMethod().toUpperCase().contains(IConstantes.AMERICAN_EXPRESS_PREFIX)
								|| transaccion.getPaymentMethod().toUpperCase().contains(IConstantes.AMERICAN_EXPRESS_PREFIX2)){
								tipoMedioPago = IConstantes.TIPO_MEDIO_PAGO_TC_PAYU;
								medioPago = IConstantes.MEDIO_PAGO_TC_AMERICAN_EXPRESS_PAYU;
								bancoPse= "0";
							} else if(transaccion.getPaymentMethod().toUpperCase().contains(IConstantes.MASTER_CARD_PREFIX)){
								tipoMedioPago = IConstantes.TIPO_MEDIO_PAGO_TC_PAYU;
								medioPago = IConstantes.MEDIO_PAGO_TC_MASTER_CARD_PAYU;
								bancoPse= "0";
							} else if(transaccion.getPaymentMethod().toUpperCase().contains(IConstantes.DINERS_PREFIX)){
								tipoMedioPago = IConstantes.TIPO_MEDIO_PAGO_TC_PAYU;
								medioPago = IConstantes.MEDIO_PAGO_TC_DINERS_PAYU;
								bancoPse= "0";
							} 
							
							break;
						} 
					}
				}
			}
		}
		
		if(estadoConfirmado !=null){
			if(estadoConfirmado.equalsIgnoreCase(IConstantes.ESTADO_CONFIRMADO_DEFAULT)){
				
				auditoria.setTipoAccion(String.valueOf(estadoConfirmado));
				auditoria.setCodLineaNegocio("U");
				auditoria.setNumTarjeta(numeroTarjetaCredito);
				
				ParametrizacionFac.getFacade().actualizarRegistro("updateAuditoriaPago", auditoria);
				
				PagosOnline pagoOnlineAux = new PagosOnline();
				PagosOnline pagoOnlineParametro = new PagosOnline();
				boolean miExiste = false;
				pagoOnlineParametro.setReciboConsignacion(auditoria.getReciboConsignacion());
				try{
				pagoOnlineAux = (PagosOnline)ParametrizacionFac.getFacade().obtenerRegistro("selectPagosOnline", pagoOnlineParametro);
				} catch(Exception e){
					e.printStackTrace();
					log.error(e.getMessage());
					pagoOnlineAux = null;
				}
				
				if(pagoOnlineAux!=null){
					if(pagoOnlineAux.getReciboConsignacion()!=null){
						if(pagoOnlineAux.getReciboConsignacion().getReciboConsignacion()!=null){
							if(pagoOnlineAux.getReciboConsignacion().getReciboConsignacion()>0L){
								miExiste = true;
							}
						}
					}
				}
				
				if(!miExiste){
					PagosOnline registroPago = new PagosOnline();
					registroPago.setReferenciaVenta(auditoria.getxId());
					registroPago.setReciboConsignacion(auditoria.getReciboConsignacion());
					registroPago.setFechaTransaccion(auditoria.getFechaCreacion());
					registroPago.setFechaPago(new SimpleDateFormat(IConstantes.FORMATO_FECHA_RESPONSE_VISA).
						parse((new SimpleDateFormat(IConstantes.FORMATO_FECHA_RESPONSE_VISA)).format(auditoria.getFechaCreacion())) );
					registroPago.setHora(
							new SimpleDateFormat("HH:mm").format(new Date()));
					registroPago.setEstadoTransaccion(String.valueOf(estadoConfirmado));
					
					registroPago.setMedioPago(medioPago);
					registroPago.setTipoMedioPago(tipoMedioPago);
					registroPago.setBancoPse(bancoPse);
					registroPago.setValor(Double.valueOf(auditoria.getSaldo()));
				Moneda moneda = new Moneda();
				if(auditoria.getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)){
					if(auditoria.getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS_ICEBERG)){
						moneda = auditoria.getMoneda();
					} else{
						moneda.setCodigo(IConstantes.PESOS_COLOMBIANOS_ICEBERG);
					}
				}else {
					moneda = auditoria.getMoneda();
				}
				
				registroPago.setMoneda(moneda);
				registroPago.setCus(auditoria.getNumPedido());
				registroPago.setProcesado(IConstantes.GENERAL_NO);
				registroPago.setOrganizacion(IConstantes.ORGANIZACION_DEFECTO);
				
				ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarPagoOnlineCondicional", registroPago);
				
				if(registroPago!= null){
					if(registroPago.getSecuencia()!=null){
						if(registroPago.getSecuencia()> 0L){
								ParametrizacionFac.getFacade().ejecutarProcedimiento("procesarPagoOnline", registroPago);
							}
						}
					}
				} 
			}
		}else if(estadoRechazado!=null){
			auditoria.setTipoAccion(String.valueOf(estadoRechazado));
			auditoria.setCodLineaNegocio("U");
			auditoria.setNumTarjeta(numeroTarjetaCredito);
			
			ParametrizacionFac.getFacade().actualizarRegistro("updateAuditoriaPago", auditoria);
		}
		
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}
	}
	
	public void verificarPagoPorReferencia(AuditoriaPago auditoria){
		try{
		Map<String,String> mapaParametros = new HashMap<String,String>();
		mapaParametros.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);	
		String referenceCode =auditoria.getNumPedido();
		Client client = Client.create();
    	
    	mapaParametros.put("parametro", IConstantes.URL_REST_PAYULATAM);
    	String urlRestApi = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
    	
    	WebResource webResource = client
		   .resource(urlRestApi);
		
		
    	mapaParametros.remove("parametro");
    	mapaParametros.put("parametro", IConstantes.API_KEY_PAYULATAM);
    	String apiKey = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
    	
    	mapaParametros.remove("parametro");
    	mapaParametros.put("parametro", IConstantes.API_LOGIN_PAYULATAM);
    	String apiLogin = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
		
		PingRequest request = new PingRequest();
    	Details details = new Details();
    	Merchant merchant = new Merchant();
    	merchant.setApiKey(apiKey);
    	merchant.setApiLogin(apiLogin);
    	details.setReferenceCode(referenceCode);
    	request.setDetails(details);
    	request.setMerchant(merchant);
    	request.setCommand(IConstantes.COMMAND_API_REFERENCE_CODE_PAYU);
    	request.setTest("false");
    	request.setLanguage("en");
    	
    	GsonBuilder builder = new GsonBuilder(); 
        builder.setPrettyPrinting(); 
        Gson gson = builder.create();
        
        String input = gson.toJson(request);
    	ClientResponse response = webResource.type("application/json").accept(MediaType.APPLICATION_JSON)
				   .post(ClientResponse.class, input);
    	
    	
		String output = response.getEntity(String.class);
		log.info("response [verificarReferencia]: "+output);
		
		ReferenceResponse respuestaReferencia = new ReferenceResponse();
		respuestaReferencia = gson.fromJson(output, ReferenceResponse.class); 
		boolean miValido = false;
		TransactionResponse transactionResponse =null;
		List<Transaction> listaTransacciones =null;
		if(respuestaReferencia.getCode()!=null){
			if(respuestaReferencia.getResult()!=null){
				
				List<Payload> payloadList = respuestaReferencia.getResult().getPayload();
				if(payloadList!=null){
					if(payloadList.size()>0){
						for(Payload payload: payloadList){
							listaTransacciones = payload.getTransactions();
							if(listaTransacciones!=null){
								if(listaTransacciones.size()>0){
									for(Transaction transaccion : listaTransacciones){
										transactionResponse = transaccion.getTransactionResponse();
										if(transactionResponse!=null){
											miValido = true;
										}
									}
								}
							}
						}
					
						if(miValido){
							this.actualizarAuditoriaPagoCondicional(auditoria, transactionResponse, listaTransacciones);
						}
					}
				} else {
					log.info("No hay Result - payload nulo");
//					auditoria.setTipoAccion(IConstantes.ESTADO_RECHAZADO_DEFAULT);
//					auditoria.setCodLineaNegocio("U");
//					auditoria.setNumTarjeta("");
//					ParametrizacionFac.getFacade().actualizarRegistro("updateAuditoriaPago", auditoria);
				}
			} else {
				log.info("No hay C�digo");
			}
		}
		

		}catch(Exception e){
			e.printStackTrace();
			log.info("Error: "+e.getMessage()+" Detalle: "+e.getStackTrace());
		}
		
	}
	
	
	public void printPagoPorReferencia(String referencia){
		try{
		String referenceCode =referencia;
		Client client = Client.create();
    	Properties appProperties = AppContext.getInstance().getAppProperties();
    	String urlRestApi = appProperties.getProperty("ap.urlRestApi");	
		
    	WebResource webResource = client
		   .resource(urlRestApi);
		
		Map<String,String> mapaParametros = new HashMap<String,String>();
		mapaParametros.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
    
    	mapaParametros.put("parametro", IConstantes.API_KEY_PAYULATAM);
    	String apiKey = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
    	
    	mapaParametros.remove("parametro");
    	mapaParametros.put("parametro", IConstantes.API_LOGIN_PAYULATAM);
    	String apiLogin = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
		
		PingRequest request = new PingRequest();
    	Details details = new Details();
    	Merchant merchant = new Merchant();
    	merchant.setApiKey(apiKey);
    	merchant.setApiLogin(apiLogin);
    	details.setReferenceCode(referenceCode);
    	request.setDetails(details);
    	request.setMerchant(merchant);
    	request.setCommand(IConstantes.COMMAND_API_REFERENCE_CODE_PAYU);
    	request.setTest("false");
    	request.setLanguage("en");
    	
    	GsonBuilder builder = new GsonBuilder(); 
        builder.setPrettyPrinting(); 
        Gson gson = builder.create();
        
        String input = gson.toJson(request);
    	ClientResponse response = webResource.type("application/json").accept(MediaType.APPLICATION_JSON)
				   .post(ClientResponse.class, input);
    	
    	
		String output = response.getEntity(String.class);
		
		ReferenceResponse respuestaReferencia = new ReferenceResponse();
		respuestaReferencia = gson.fromJson(output, ReferenceResponse.class); 
		
		output = gson.toJson(respuestaReferencia);
		System.out.println("------- Impresi�n de referencia obtenida --------");
		System.out.println(output);
		System.out.println("------- Fin --------");
		} catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}
		
	}
	
    public Properties getProperties(String pathProperties){
    	Properties properties;
	    try {     
	    	FileInputStream file = new FileInputStream(pathProperties);
	        properties = new Properties();
	        properties.load(file);
	        file.close();
	        
	        return properties;
	    } catch (FileNotFoundException e) {
	    	e.printStackTrace();
	    	log.error(e);
	    	return null;
	    } catch (IOException e) {
	    	e.printStackTrace();
	    	log.error(e);
	    	return null;
	    }
    } 
	
	public void verificarPagoPorReferencia(String referencia){
		try{
		String referenceCode =referencia;
		Client client = Client.create();
    	Properties appProperties = AppContext.getInstance().getAppProperties();
    	if (appProperties.containsKey("ap.urlRestApi")) {
    		if(appProperties.getProperty("ap.urlRestApi").equals(null) || appProperties.getProperty("ap.urlRestApi").equals("")) {
    			appProperties = 
    	    			this.getProperties(AppContext.getInstance().getContextPath() + 
    	    					File.separator +"WEB-INF" + 
    	    					File.separator + "classes" + 
    	    					File.separator + "Aplicacion.properties"
    	    					);	
    		}
    	} else {
    		
    		appProperties = 
    			this.getProperties(AppContext.getInstance().getContextPath() + 
    					File.separator +"WEB-INF" + 
    					File.separator + "classes" + 
    					File.separator + "Aplicacion.properties"
    					);
    	}
    	String urlRestApi = appProperties.getProperty("ap.urlRestApi");	
		
    	WebResource webResource = client
		   .resource(urlRestApi);
		
		Map<String,String> mapaParametros = new HashMap<String,String>();
		mapaParametros.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
    
    	mapaParametros.put("parametro", IConstantes.API_KEY_PAYULATAM);
    	String apiKey = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
    	
    	mapaParametros.remove("parametro");
    	mapaParametros.put("parametro", IConstantes.API_LOGIN_PAYULATAM);
    	String apiLogin = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
		
		PingRequest request = new PingRequest();
    	Details details = new Details();
    	Merchant merchant = new Merchant();
    	merchant.setApiKey(apiKey);
    	merchant.setApiLogin(apiLogin);
    	details.setReferenceCode(referenceCode);
    	request.setDetails(details);
    	request.setMerchant(merchant);
    	request.setCommand(IConstantes.COMMAND_API_REFERENCE_CODE_PAYU);
    	request.setTest("false");
    	request.setLanguage("en");
    	
    	GsonBuilder builder = new GsonBuilder(); 
        builder.setPrettyPrinting(); 
        Gson gson = builder.create();
        
        String input = gson.toJson(request);
    	ClientResponse response = webResource.type("application/json").accept(MediaType.APPLICATION_JSON)
				   .post(ClientResponse.class, input);
    	
    	
		String output = response.getEntity(String.class);
		
		ReferenceResponse respuestaReferencia = new ReferenceResponse();
		respuestaReferencia = gson.fromJson(output, ReferenceResponse.class); 

		if(respuestaReferencia.getCode()!=null){
			List<Payload> payloadList = respuestaReferencia.getResult().getPayload();
			if(payloadList!=null){
				if(payloadList.size()>0){
					for(Payload payload: payloadList){
						List<Transaction> listaTransacciones = payload.getTransactions();
						if(listaTransacciones!=null){
							if(listaTransacciones.size()>0){
								for(Transaction transaccion : listaTransacciones){
									TransactionResponse transactionResponse = transaccion.getTransactionResponse();
									if(transactionResponse!=null){
										
									}
								}
							}
						}
					}
				}
			}
		}
		
	

		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}
		
	}
	
	
	@SuppressWarnings("unchecked")
	public void verificarEstadoPagos(){
		try{
		TransaccionZonaPagos transaccion = new TransaccionZonaPagos();
		transaccion.setEstadoPago(new Long(IConstantes.ESTADO_PENDIENTE_FINALIZAR));
		this.setTransaccionZonaPagos(transaccion);
		List<TransaccionZonaPagos> listaTransacciones = new ArrayList <TransaccionZonaPagos>();
		listaTransacciones = ((List<TransaccionZonaPagos>)ParametrizacionFac.getFacade().obtenerListado("selectTransaccionZonaPagosFecha",this.getTransaccionZonaPagos()));
		log.info("Listado de pagos pendientes por finalizar: "+listaTransacciones.size());
		for(TransaccionZonaPagos objeto: listaTransacciones){
			log.info("Empezar a procesar pagos pendientes Finalizar");
			if(objeto!= null){
				if(objeto.getIdPago()>0L){
					if(ReciboConsignacionHelper.getHelper().getServicioPruebasProduccion()){
						ServiceSoapProxy servicioVerificacion = new ServiceSoapProxy();
						ArrayOfPagos_v3Holder res_pagos_v3 = new ArrayOfPagos_v3Holder();
						IntHolder int_error = new IntHolder();
						int int_id_tienda= new Integer(objeto.getIdComercio().toString());
						StringHolder str_error = new StringHolder();
						String str_id_pago = new String(objeto.getIdPago().toString());
						String str_id_clave = new String(objeto.getIdComercio().toString().equals(IConstantes.ID_TIENDA)?
								IConstantes.CLAVE_SERVICIO:
							objeto.getIdComercio().toString().equals(IConstantes.ID_TIENDA_BANCOLOMBIA)?
								IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
							objeto.getIdComercio().toString().equals(IConstantes.ID_TIENDA_DOLARES)?
									IConstantes.CLAVE_SERVICIO_DOLARES:
									IConstantes.CLAVE_SERVICIO_EURO);
						Integer estadoArrayPago;
						int estadoPago = servicioVerificacion.verificar_pago_v3(str_id_pago, int_id_tienda, str_id_clave, res_pagos_v3, int_error, str_error); 
						log.info("encuentra pago: "+estadoPago);
						if (estadoPago>0){ //Significa que encontr� un pago
							estadoArrayPago = (res_pagos_v3.value[0]!=null?
												(((Integer)res_pagos_v3.value[0].getInt_estado_pago())!=null?
												res_pagos_v3.value[0].getInt_estado_pago()
												:0)
												:0);
						
							if (res_pagos_v3.value[0]!=null){
								objeto.setIdFormaPago((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()!=null?
										((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()).longValue():new Long(0L));
								objeto.setValorPagado((Double)res_pagos_v3.value[0].getDbl_valor_pagado()!=null?
										((Double)res_pagos_v3.value[0].getDbl_valor_pagado()):new Double(0.00));
								objeto.setTicketId(res_pagos_v3.value[0].getStr_ticketID()!= null?
										!res_pagos_v3.value[0].getStr_ticketID().equals("")?
										new Long(res_pagos_v3.value[0].getStr_ticketID()):new Long(0L):new Long(0L));
								objeto.setIdClave(res_pagos_v3.value[0].getStr_id_clave());
								objeto.setIdCliente(res_pagos_v3.value[0].getStr_id_cliente());
								objeto.setFranquicia(res_pagos_v3.value[0].getStr_franquicia()!=null?!res_pagos_v3.value[0].getStr_franquicia().equals("")?
										res_pagos_v3.value[0].getStr_franquicia():new String("0"):new String("0"));
								objeto.setCodigoAprobacion((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()!=null?
										((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()).longValue():new Long(0L));
								objeto.setCodigoServicio((Integer)res_pagos_v3.value[0].getInt_codigo_servico()!=null?
										((Integer)res_pagos_v3.value[0].getInt_codigo_servico()).longValue():new Long(0L));
								objeto.setCodigoBanco((Integer)res_pagos_v3.value[0].getInt_codigo_banco()!=null?
										((Integer)res_pagos_v3.value[0].getInt_codigo_banco()).longValue():new Long(0L));
								objeto.setNombreBanco(res_pagos_v3.value[0].getStr_nombre_banco()!=null?!res_pagos_v3.value[0].getStr_nombre_banco().equals("")?
										res_pagos_v3.value[0].getStr_nombre_banco():new String("0"):new String("0"));
								objeto.setCodigoTransaccion(res_pagos_v3.value[0].getStr_codigo_transaccion()!=null?
										!res_pagos_v3.value[0].getStr_codigo_transaccion().equals("")?
										new Long(res_pagos_v3.value[0].getStr_codigo_transaccion()):new Long(0L):new Long(0L));
								
								objeto.setCicloTransaccion((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()!=null?
										((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()).longValue():new Long(0L));
								objeto.setCampo1(res_pagos_v3.value[0].getStr_campo1());
								objeto.setCampo2(res_pagos_v3.value[0].getStr_campo2());
								objeto.setCampo3(res_pagos_v3.value[0].getStr_campo3());
								objeto.setDatFecha(objeto.getDatFecha()!=null?objeto.getDatFecha():new Date());
								}
						
							log.info("estado array: "+estadoArrayPago);
							
							if (estadoArrayPago.longValue() != objeto.getEstadoPago()) {
								objeto.setEstadoPago(estadoArrayPago.longValue());
							}
								if(!estadoArrayPago.toString().equals(IConstantes.ESTADO_PENDIENTE_INICIAR) && int_error.value == 0){
									try{
									ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
									} catch(Exception e){
										e.printStackTrace();
									}
								}
						} else{
							if(int_error.value==1){
							objeto.setEstadoPago(0L);
							try{
							ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
							} catch(Exception e){
								e.printStackTrace();
							}
							}
						}
							
					} else{
						com.zonapagos.www.ws_verificar_pagos.test.ServiceSoapProxy servicioVerificacion = new com.zonapagos.www.ws_verificar_pagos.test.ServiceSoapProxy();
						com.zonapagos.www.ws_verificar_pagos.holders.test.ArrayOfPagos_v3Holder res_pagos_v3 = new com.zonapagos.www.ws_verificar_pagos.holders.test.ArrayOfPagos_v3Holder();
						IntHolder int_error = new IntHolder();
						int int_id_tienda= new Integer(objeto.getIdComercio().toString());
						StringHolder str_error = new StringHolder();
						String str_id_pago = new String(objeto.getIdPago().toString());
						String str_id_clave = new String(IConstantes.CLAVE_SERVICIO_PRUEBAS);
						Integer estadoArrayPago;
						int estadoPago = servicioVerificacion.verificar_pago_v3(str_id_pago, int_id_tienda, str_id_clave, res_pagos_v3, int_error, str_error); 
						log.info("encuentra pago: "+estadoPago);
						if (estadoPago>0){ //Significa que encontr� un pago
							estadoArrayPago = (res_pagos_v3.value[0]!=null?
												(((Integer)res_pagos_v3.value[0].getInt_estado_pago())!=null?
												res_pagos_v3.value[0].getInt_estado_pago()
												:0)
												:0);
						
							if (res_pagos_v3.value[0]!=null){
								objeto.setIdFormaPago((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()!=null?
										((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()).longValue():new Long(0L));
								objeto.setValorPagado((Double)res_pagos_v3.value[0].getDbl_valor_pagado()!=null?
										((Double)res_pagos_v3.value[0].getDbl_valor_pagado()):new Double(0.00));
								objeto.setTicketId(res_pagos_v3.value[0].getStr_ticketID()!= null?
										!res_pagos_v3.value[0].getStr_ticketID().equals("")?
										new Long(res_pagos_v3.value[0].getStr_ticketID()):new Long(0L):new Long(0L));
								objeto.setIdClave(res_pagos_v3.value[0].getStr_id_clave());
								objeto.setIdCliente(res_pagos_v3.value[0].getStr_id_cliente());
								objeto.setFranquicia(res_pagos_v3.value[0].getStr_franquicia()!=null?!res_pagos_v3.value[0].getStr_franquicia().equals("")?
										res_pagos_v3.value[0].getStr_franquicia():new String("0"):new String("0"));
								objeto.setCodigoAprobacion((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()!=null?
										((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()).longValue():new Long(0L));
								objeto.setCodigoServicio((Integer)res_pagos_v3.value[0].getInt_codigo_servico()!=null?
										((Integer)res_pagos_v3.value[0].getInt_codigo_servico()).longValue():new Long(0L));
								objeto.setCodigoBanco((Integer)res_pagos_v3.value[0].getInt_codigo_banco()!=null?
										((Integer)res_pagos_v3.value[0].getInt_codigo_banco()).longValue():new Long(0L));
								objeto.setNombreBanco(res_pagos_v3.value[0].getStr_nombre_banco()!=null?!res_pagos_v3.value[0].getStr_nombre_banco().equals("")?
										res_pagos_v3.value[0].getStr_nombre_banco():new String("0"):new String("0"));
								objeto.setCodigoTransaccion(res_pagos_v3.value[0].getStr_codigo_transaccion()!=null?
										!res_pagos_v3.value[0].getStr_codigo_transaccion().equals("")?
										new Long(res_pagos_v3.value[0].getStr_codigo_transaccion()):new Long(0L):new Long(0L));
								
								objeto.setCicloTransaccion((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()!=null?
										((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()).longValue():new Long(0L));
								objeto.setCampo1(res_pagos_v3.value[0].getStr_campo1());
								objeto.setCampo2(res_pagos_v3.value[0].getStr_campo2());
								objeto.setCampo3(res_pagos_v3.value[0].getStr_campo3());
								objeto.setDatFecha(objeto.getDatFecha()!=null?objeto.getDatFecha():new Date());
								}
						
							log.info("estado array: "+estadoArrayPago);
							if (estadoArrayPago.longValue() != objeto.getEstadoPago()) {
								objeto.setEstadoPago(estadoArrayPago.longValue());
							}
							if(!estadoArrayPago.toString().equals(IConstantes.ESTADO_PENDIENTE_INICIAR) && int_error.value == 0){
								try{
								ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}else{
							if(int_error.value==1){
							objeto.setEstadoPago(0L);
							try{
							ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
							} catch(Exception e){
								e.printStackTrace();
							}
							}
						}
						
					}
				}
			}
		}
		
	} catch (InterruptedException e) {
		log.info("Error por interrupci�n en verificarPagosSinFinalizar--> "+e.getMessage());
		e.printStackTrace();
	} catch (Exception e) {
		log.info("Error por excepci�n en verificarPagosSinFinalizar--> "+e.getMessage());
		e.printStackTrace();
	}
	
}
	

	@SuppressWarnings("unchecked")
	public void verificarEstadoPagosEstudio(){
		try{
		TransaccionZonaPagos transaccion = new TransaccionZonaPagos();
		transaccion.setEstadoPago(new Long(IConstantes.ESTADO_PENDIENTE_ESTUDIO));
		this.setTransaccionZonaPagos(transaccion);
		List<TransaccionZonaPagos> listaTransacciones = new ArrayList <TransaccionZonaPagos>();
		listaTransacciones = ((List<TransaccionZonaPagos>)ParametrizacionFac.getFacade().obtenerListado("selectTransaccionZonaPagosFecha",this.getTransaccionZonaPagos()));
		for(TransaccionZonaPagos objeto: listaTransacciones){
			log.info("Empezar a procesar pagos pendientes estudio");
			if(objeto!= null){
				if(objeto.getIdPago()>0L){
					if(ReciboConsignacionHelper.getHelper().getServicioPruebasProduccion()){
						ServiceSoapProxy servicioVerificacion = new ServiceSoapProxy();
						ArrayOfPagos_v3Holder res_pagos_v3 = new ArrayOfPagos_v3Holder();
						IntHolder int_error = new IntHolder();
						int int_id_tienda= new Integer(objeto.getIdComercio().toString());
						StringHolder str_error = new StringHolder();
						String str_id_pago = new String(objeto.getIdPago().toString());
						String str_id_clave = new String(objeto.getIdComercio().toString().equals(IConstantes.ID_TIENDA)?
								IConstantes.CLAVE_SERVICIO:
							objeto.getIdComercio().toString().equals(IConstantes.ID_TIENDA_BANCOLOMBIA)?
								IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
							objeto.getIdComercio().toString().equals(IConstantes.ID_TIENDA_DOLARES)?
									IConstantes.CLAVE_SERVICIO_DOLARES:
									IConstantes.CLAVE_SERVICIO_EURO);
						Integer estadoArrayPago;
						int estadoPago = servicioVerificacion.verificar_pago_v3(str_id_pago, int_id_tienda, str_id_clave, res_pagos_v3, int_error, str_error); 
						log.info("encuentra pago: "+estadoPago);
						if (estadoPago>0){ //Significa que encontr� un pago
							estadoArrayPago = (res_pagos_v3.value[0]!=null?
												(((Integer)res_pagos_v3.value[0].getInt_estado_pago())!=null?
												res_pagos_v3.value[0].getInt_estado_pago()
												:0)
												:0);
						
							if (res_pagos_v3.value[0]!=null){
								objeto.setIdFormaPago((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()!=null?
										((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()).longValue():new Long(0L));
								objeto.setValorPagado((Double)res_pagos_v3.value[0].getDbl_valor_pagado()!=null?
										((Double)res_pagos_v3.value[0].getDbl_valor_pagado()):new Double(0.00));
								objeto.setTicketId(res_pagos_v3.value[0].getStr_ticketID()!= null?
										!res_pagos_v3.value[0].getStr_ticketID().equals("")?
										new Long(res_pagos_v3.value[0].getStr_ticketID()):new Long(0L):new Long(0L));
								objeto.setIdClave(res_pagos_v3.value[0].getStr_id_clave());
								objeto.setIdCliente(res_pagos_v3.value[0].getStr_id_cliente());
								objeto.setFranquicia(res_pagos_v3.value[0].getStr_franquicia()!=null?!res_pagos_v3.value[0].getStr_franquicia().equals("")?
										res_pagos_v3.value[0].getStr_franquicia():new String("0"):new String("0"));
								objeto.setCodigoAprobacion((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()!=null?
										((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()).longValue():new Long(0L));
								objeto.setCodigoServicio((Integer)res_pagos_v3.value[0].getInt_codigo_servico()!=null?
										((Integer)res_pagos_v3.value[0].getInt_codigo_servico()).longValue():new Long(0L));
								objeto.setCodigoBanco((Integer)res_pagos_v3.value[0].getInt_codigo_banco()!=null?
										((Integer)res_pagos_v3.value[0].getInt_codigo_banco()).longValue():new Long(0L));
								objeto.setNombreBanco(res_pagos_v3.value[0].getStr_nombre_banco()!=null?!res_pagos_v3.value[0].getStr_nombre_banco().equals("")?
										res_pagos_v3.value[0].getStr_nombre_banco():new String("0"):new String("0"));
								objeto.setCodigoTransaccion(res_pagos_v3.value[0].getStr_codigo_transaccion()!=null?
										!res_pagos_v3.value[0].getStr_codigo_transaccion().equals("")?
										new Long(res_pagos_v3.value[0].getStr_codigo_transaccion()):new Long(0L):new Long(0L));
								
								objeto.setCicloTransaccion((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()!=null?
										((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()).longValue():new Long(0L));
								objeto.setCampo1(res_pagos_v3.value[0].getStr_campo1());
								objeto.setCampo2(res_pagos_v3.value[0].getStr_campo2());
								objeto.setCampo3(res_pagos_v3.value[0].getStr_campo3());
								objeto.setDatFecha(objeto.getDatFecha()!=null?objeto.getDatFecha():new Date());
								}
						
							log.info("estado array: "+estadoArrayPago);
							
							if (estadoArrayPago.longValue() != objeto.getEstadoPago()) {
								objeto.setEstadoPago(estadoArrayPago.longValue());
							}
								if(!estadoArrayPago.toString().equals(IConstantes.ESTADO_PENDIENTE_INICIAR) && int_error.value == 0){
									try{
									ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
									} catch(Exception e){
										e.printStackTrace();
									}
								}
						} else{
							if(int_error.value==1){
							objeto.setEstadoPago(0L);
							try{
							ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
							} catch(Exception e){
								e.printStackTrace();
							}
							}
						}
							
					} else{
						com.zonapagos.www.ws_verificar_pagos.test.ServiceSoapProxy servicioVerificacion = new com.zonapagos.www.ws_verificar_pagos.test.ServiceSoapProxy();
						com.zonapagos.www.ws_verificar_pagos.holders.test.ArrayOfPagos_v3Holder res_pagos_v3 = new com.zonapagos.www.ws_verificar_pagos.holders.test.ArrayOfPagos_v3Holder();
						IntHolder int_error = new IntHolder();
						int int_id_tienda= new Integer(objeto.getIdComercio().toString());
						StringHolder str_error = new StringHolder();
						String str_id_pago = new String(objeto.getIdPago().toString());
						String str_id_clave = new String(IConstantes.CLAVE_SERVICIO_PRUEBAS);
						Integer estadoArrayPago;
						int estadoPago = servicioVerificacion.verificar_pago_v3(str_id_pago, int_id_tienda, str_id_clave, res_pagos_v3, int_error, str_error); 
						log.info("encuentra pago: "+estadoPago);
						if (estadoPago>0){ //Significa que encontr� un pago
							estadoArrayPago = (res_pagos_v3.value[0]!=null?
												(((Integer)res_pagos_v3.value[0].getInt_estado_pago())!=null?
												res_pagos_v3.value[0].getInt_estado_pago()
												:0)
												:0);
						
							if (res_pagos_v3.value[0]!=null){
								objeto.setIdFormaPago((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()!=null?
										((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()).longValue():new Long(0L));
								objeto.setValorPagado((Double)res_pagos_v3.value[0].getDbl_valor_pagado()!=null?
										((Double)res_pagos_v3.value[0].getDbl_valor_pagado()):new Double(0.00));
								objeto.setTicketId(res_pagos_v3.value[0].getStr_ticketID()!= null?
										!res_pagos_v3.value[0].getStr_ticketID().equals("")?
										new Long(res_pagos_v3.value[0].getStr_ticketID()):new Long(0L):new Long(0L));
								objeto.setIdClave(res_pagos_v3.value[0].getStr_id_clave());
								objeto.setIdCliente(res_pagos_v3.value[0].getStr_id_cliente());
								objeto.setFranquicia(res_pagos_v3.value[0].getStr_franquicia()!=null?!res_pagos_v3.value[0].getStr_franquicia().equals("")?
										res_pagos_v3.value[0].getStr_franquicia():new String("0"):new String("0"));
								objeto.setCodigoAprobacion((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()!=null?
										((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()).longValue():new Long(0L));
								objeto.setCodigoServicio((Integer)res_pagos_v3.value[0].getInt_codigo_servico()!=null?
										((Integer)res_pagos_v3.value[0].getInt_codigo_servico()).longValue():new Long(0L));
								objeto.setCodigoBanco((Integer)res_pagos_v3.value[0].getInt_codigo_banco()!=null?
										((Integer)res_pagos_v3.value[0].getInt_codigo_banco()).longValue():new Long(0L));
								objeto.setNombreBanco(res_pagos_v3.value[0].getStr_nombre_banco()!=null?!res_pagos_v3.value[0].getStr_nombre_banco().equals("")?
										res_pagos_v3.value[0].getStr_nombre_banco():new String("0"):new String("0"));
								objeto.setCodigoTransaccion(res_pagos_v3.value[0].getStr_codigo_transaccion()!=null?
										!res_pagos_v3.value[0].getStr_codigo_transaccion().equals("")?
										new Long(res_pagos_v3.value[0].getStr_codigo_transaccion()):new Long(0L):new Long(0L));
								
								objeto.setCicloTransaccion((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()!=null?
										((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()).longValue():new Long(0L));
								objeto.setCampo1(res_pagos_v3.value[0].getStr_campo1());
								objeto.setCampo2(res_pagos_v3.value[0].getStr_campo2());
								objeto.setCampo3(res_pagos_v3.value[0].getStr_campo3());
								objeto.setDatFecha(objeto.getDatFecha()!=null?objeto.getDatFecha():new Date());
								}
						
							log.info("estado array: "+estadoArrayPago);
							if (estadoArrayPago.longValue() != objeto.getEstadoPago()) {
								objeto.setEstadoPago(estadoArrayPago.longValue());
							}
							if(!estadoArrayPago.toString().equals(IConstantes.ESTADO_PENDIENTE_INICIAR) && int_error.value == 0){
								try{
								ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}else{
							if(int_error.value==1){
							objeto.setEstadoPago(0L);
							try{
							ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
							} catch(Exception e){
								e.printStackTrace();
							}
							}
						}
						
					}
				}
			}
		}
		
	} catch (InterruptedException e) {
		log.info("Error por interrupci�n en verificarPagosSinFinalizarEstudio--> "+e.getMessage());
		e.printStackTrace();
	} catch (Exception e) {
		log.info("Error por excepci�n en verificarPagosSinFinalizarEstudio--> "+e.getMessage());
		e.printStackTrace();
	}
	
}
	
	
	
	
	public TransaccionZonaPagos getTransaccionZonaPagos() {
		return transaccionZonaPagos;
	}


	public void setTransaccionZonaPagos(TransaccionZonaPagos transaccionZonaPagos) {
		this.transaccionZonaPagos = transaccionZonaPagos;
	}

}
