package com.konrad.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.konrad.helper.ReciboConsignacionHelper;

public class ConversionDaemon extends Thread{
	private static final ConversionDaemon conversionDaemon = new ConversionDaemon();
	protected static Logger log = Logger.getLogger(ConversionDaemon.class);
	
	public static ConversionDaemon getInstance(){
		return conversionDaemon;
	}
	
	public void run(){
		try {
			while(true){
			Thread.sleep((60000)*7);
			this.setPriority(Thread.MIN_PRIORITY);
			
			Map<String, Object> mapaResultDolar = new HashMap<String,Object>();
 			Map<String, Object> mapaResultEuro = new HashMap<String,Object>();
 			
 			mapaResultDolar = ReciboConsignacionHelper.getHelper().setConversionHistorica(IConstantes.FORMATO_FECHA, 
 					IConstantes.DOLARES_AMERICANOS,IConstantes.PESOS_COLOMBIANOS_ICEBERG, new Date());
 			// Significa que se hace en moneda extranjera se verifica que haya subido la del d�a EUROS -DOLARES
 			mapaResultEuro = ReciboConsignacionHelper.getHelper().setConversionHistorica(IConstantes.FORMATO_FECHA, 
 					IConstantes.EUROS,IConstantes.DOLARES_AMERICANOS, new Date());
 			log.info("Factor Demonio Conversi�n D�lar: "+mapaResultDolar.get("FACTOR"));
 			log.info("Factor Demonio Conversi�n Euro: "+mapaResultEuro.get("FACTOR"));
 			Thread.yield();
 			
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
