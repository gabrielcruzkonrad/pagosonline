package com.konrad.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.util.ThemeProvider;

/*
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation; either version 2 of the
License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
USA */

public class MultiThemeProvider implements ThemeProvider {
	private String themeName, fileList;
	
	public String getThemeName() {
			return themeName;
	}
	
	public void setThemeName(String themeName) {
			this.themeName = themeName;
	}
	public MultiThemeProvider() {
		try {
			InputStream is = getClass().getResourceAsStream("/zkthemer.properties");
			if (is == null)
			throw new RuntimeException("Cannot find zkthemer.properties");
			Properties prop = new Properties();
			prop.load(is);
			themeName = (String) prop.get("theme");
			fileList = (String) prop.get("fileList");
			if (themeName == null) {
				throw new RuntimeException("zkthemer.properties found, but missing 'theme' entry");
			}
			is.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Collection getThemeURIs(Execution exec, List uris) {
		List newUris = new ArrayList(uris);
		for (Object object : newUris) {
			String uri = (String) object;
		if (uri.startsWith("~./")) {
				uri = "~./" + themeName + "/" + uri.substring(3);
			}
		uris.add(uri);
		}
		return uris;
	}
	
	public String beforeWCS(Execution exec, String uri) {
		return uri;
	}
public String beforeWidgetCSS(Execution exec, String uri) {
	String fileName = uri.substring(uri.lastIndexOf("/") + 1);
	if (!(fileList.indexOf(fileName) < 0))
		uri = (new StringBuilder("~./")).append(themeName).append("/").append(uri.substring(3)).toString();
		return uri;
	}

	public int getWCSCacheControl(Execution exec, String uri) {
		return -1;// safe to cache
	}
}