package com.konrad.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.io.IOUtils;

public class SendEmail {

	private class SMTPAuthenticator extends Authenticator {

		private String dEmail;
		private String dPassword;
 
		public SMTPAuthenticator(String email, String password){
			dEmail = email;
			dPassword = password;
		}

		public PasswordAuthentication getPasswordAuthentication(){
			return new PasswordAuthentication(dEmail, dPassword);
		}
	}

/**
* Funci�n que permite enviar un correo electr�nico con archivos adjunto.
* El contenido del correo puede estar en formato HTML.
* @param emisor Correo de qui&eacute;n emite el correo
* @param asunto Asunto del e-mail
* @param receptores Correos de los receptores del e-mail
* @param mensaje Mensaje del e-mail
* @param adjuntos Ruta de archivos adjuntos en el e-mail
* @return TRUE si el mail fue enviado con �xito, FALSE en caso contrario
*/

public boolean enviaGoogleApp(String emisor, 
							  String asunto, 
							  List<String> receptores, 
							  String mensaje, 
							  List<String> adjuntos,
							  String email,
							  String contrasena){

	boolean envioExitoso = true;
	String emailGetCursos = email;
	String password = contrasena;

	Properties props = new Properties();
	props.put("mail.smtp.user", emisor);
	props.put("mail.smtp.host", "smtp.gmail.com");
	props.put("mail.smtp.port", "465");
	props.put("mail.smtp.starttls.enable", "true");
	props.put("mail.smtp.auth", "true");
	props.put("mail.smtp.socketFactory.port", "465");
	props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	props.put("mail.smtp.socketFactory.fallback", "false");

	try{

		Authenticator auth = new SMTPAuthenticator(emailGetCursos, password);
		Session session = Session.getInstance(props, auth);

		MimeMessage message = new MimeMessage(session);
		InternetAddress[] dest = new InternetAddress[receptores.size()];
		for (int i=0; i<dest.length; i++){
			dest[i] = new InternetAddress( receptores.get(i) );
		}

		//Se define qui�n es el emisor del e-mail
		message.setFrom(new InternetAddress(emisor));
		InternetAddress[] replyTo = new InternetAddress[1];
		replyTo[0] = new InternetAddress( emisor );
		message.setReplyTo( replyTo );
		//Se definen el o los destinatarios
		message.addRecipients(Message.RecipientType.TO, dest);
		//message.addRecipients(Message.RecipientType.CC, dest);
		//message.addRecipients(Message.RecipientType.BCC, dest);
		//Se defina el asunto del e-mail
		message.setSubject(asunto);

		//Se seteo el mensaje del e-mail
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(message,"text/html");
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);

		//Se adjuntan los archivos al correo

		if( adjuntos!=null && adjuntos.size()>0 ){
			for( String rutaAdjunto : adjuntos ){
				messageBodyPart = new MimeBodyPart();
				File f = new File(rutaAdjunto);

				if( f.exists() ){

					DataSource source = new FileDataSource( rutaAdjunto );
					messageBodyPart.setDataHandler( new DataHandler(source) );
					messageBodyPart.setFileName( f.getName() );
					multipart.addBodyPart(messageBodyPart);
				}
			}
		}

		//Se junta el mensaje y los archivos adjuntos
		message.setContent(multipart);
		//Se env�a el e-mail
		Transport.send( message );
	}

	catch (Exception e) {
		envioExitoso = false;
	}
	finally{
		//Se eliminan del servidor los archivos adjuntos
		if( adjuntos!=null && adjuntos.size()>0 ){
			for( String rutaAdjunto : adjuntos ){
				try{

					File arch = new File(rutaAdjunto);
					arch.delete();

				}
				catch (Exception e) {}

			}
		}
	}

	return envioExitoso;
}

/**
* Funci�n que permite enviar un correo electr&oacute;nico con archivos adjunto.
* El contenido del correo puede estar en formato HTML.
* @param emisor Correo de qui&eacute;n emite el correo
* @param asunto Asunto del e-mail
* @param receptores Correos de los receptores del e-mail
* @param mensaje Mensaje del e-mail
* @param adjuntos Ruta de archivos adjuntos en el e-mail
* @return TRUE si el mail fue enviado con &eacute;xito, FALSE en caso contrario
*/
public boolean envia(String servidorCorreo, String puertoCorreo, String emisor, String asunto, 
		List<String> receptores, List<String> receptoresCopia, String mensaje, List<String> adjuntos, String contrasena,
		String tls, String authentication){

	boolean envioExitoso = true;

	try{
		Properties props = System.getProperties();
		//Se define el servidor de correos
		props.put("mail.smtp.host", servidorCorreo);
		props.put("mail.smtp.port", puertoCorreo);
		props.put("mail.smtp.user", emisor);
		props.put("mail.smtp.starttls.enable", tls);
		props.put("mail.smtp.auth", authentication);
		props.put("mail.smtp.ssl.protocols", "TLSv1.2");
        props.put("mail.smtp.ssl.trust", servidorCorreo);
		//props.put("mail.smtp.socketFactory.port", puertoCorreo);
		//props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		//props.put("mail.smtp.socketFactory.fallback", "false");
		
		String password = contrasena;
		Authenticator auth = new SMTPAuthenticator(emisor, password);
		

		//Se obtiene sesi�n desde el servidor de correos
		Session session = Session.getInstance(props, auth);
		session.setDebug(true);
		MimeMessage message = new MimeMessage(session);
		InternetAddress[] dest = new InternetAddress[receptores.size()];
		InternetAddress[] destCopy = new InternetAddress[receptoresCopia.size()];

		for (int i=0;i<dest.length;i++){

			dest[i] = new InternetAddress( receptores.get(i) );

		}
		
		for (int i=0;i<destCopy.length;i++){

			destCopy[i] = new InternetAddress( receptoresCopia.get(i) );

		}
		
		//Se define qui�n es el emisor del e-mail
		message.setFrom(new InternetAddress(emisor));
		InternetAddress[] replyTo = new InternetAddress[1];
		replyTo[0] = new InternetAddress( emisor );
		message.setReplyTo( replyTo );
		
		//Se definen el o los destinatarios
		message.addRecipients(Message.RecipientType.TO, dest);
		message.addRecipients(Message.RecipientType.CC, destCopy);
		//message.addRecipients(Message.RecipientType.BCC, dest); 
		//Se defina el asunto del e-mail
		message.setSubject(asunto);
		//message.setContent(mensaje, "text/plain");

		//Se seteo el mensaje del e-mail
		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(mensaje,"text/plain");
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);
		//Se adjuntan los archivos al correo
		if( adjuntos!=null && adjuntos.size()>0 ){
			System.out.println("si hay adjuntos...");
			
			for( String rutaAdjunto : adjuntos ){
				messageBodyPart = new MimeBodyPart();
				File f = new File(rutaAdjunto);
				
				if( f.exists() ){
					System.out.println("si encontr� archivo...");
					DataSource source = new FileDataSource( rutaAdjunto );
					messageBodyPart.setDataHandler( new DataHandler(source) );
					messageBodyPart.setFileName( f.getName() );
					multipart.addBodyPart(messageBodyPart);
				}
				else{
					System.out.println("No encontr� archivo...");
				}	
			}
		}
		//Se junta el mensaje y los archivos adjuntos
		message.setContent(multipart);
		//Se env&iacute;a el e-mail
		Transport.send( message );

	}

	catch (Exception e) {
		envioExitoso = false;
		e.printStackTrace();
	}
	finally{
		//Se eliminan del servidor los archivos adjuntos
		if( adjuntos!=null && adjuntos.size()>0 ){
			for( String rutaAdjunto : adjuntos ){
				try{
					File arch = new File(rutaAdjunto);
					arch.delete();
				}

				catch (Exception e) { 
					e.printStackTrace();
				}
			}
		}
	}

	return envioExitoso;
}

public boolean envia(String servidorCorreo, String puertoCorreo, String emisor, String asunto, 
		List<String> receptores, List<String> receptoresCopia, String mensaje, List<String> adjuntos, String contrasena){

	boolean envioExitoso = true;

	try{
		Properties props = System.getProperties();
		//Se define el servidor de correos
		props.put("mail.smtp.host", servidorCorreo);
		props.put("mail.smtp.port", puertoCorreo);
		props.put("mail.smtp.user", emisor);
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.ssl.protocols", "TLSv1.2");
        props.put("mail.smtp.ssl.trust", servidorCorreo);
		//props.put("mail.smtp.socketFactory.port", puertoCorreo);
		//props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		//props.put("mail.smtp.socketFactory.fallback", "false");
		
		String password = contrasena;
		Authenticator auth = new SMTPAuthenticator(emisor, password);
		

		//Se obtiene sesi�n desde el servidor de correos
		Session session = Session.getInstance(props, auth);
		session.setDebug(true);
		MimeMessage message = new MimeMessage(session);
		InternetAddress[] dest = new InternetAddress[receptores.size()];
		InternetAddress[] destCopy = new InternetAddress[receptoresCopia.size()];

		for (int i=0;i<dest.length;i++){

			dest[i] = new InternetAddress( receptores.get(i) );

		}
		
		for (int i=0;i<destCopy.length;i++){

			destCopy[i] = new InternetAddress( receptoresCopia.get(i) );

		}
		
		//Se define qui�n es el emisor del e-mail
		message.setFrom(new InternetAddress(emisor));
		InternetAddress[] replyTo = new InternetAddress[1];
		replyTo[0] = new InternetAddress( emisor );
		message.setReplyTo( replyTo );
		
		//Se definen el o los destinatarios
		message.addRecipients(Message.RecipientType.TO, dest);
		message.addRecipients(Message.RecipientType.CC, destCopy);
		//message.addRecipients(Message.RecipientType.BCC, dest); 
		//Se defina el asunto del e-mail
		message.setSubject(asunto);
		//message.setContent(mensaje, "text/plain");

		//Se seteo el mensaje del e-mail
		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(mensaje,"text/plain");
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);
		//Se adjuntan los archivos al correo
		if( adjuntos!=null && adjuntos.size()>0 ){
			System.out.println("si hay adjuntos...");
			
			for( String rutaAdjunto : adjuntos ){
				messageBodyPart = new MimeBodyPart();
				File f = new File(rutaAdjunto);
				
				if( f.exists() ){
					System.out.println("si encontr� archivo...");
					DataSource source = new FileDataSource( rutaAdjunto );
					messageBodyPart.setDataHandler( new DataHandler(source) );
					messageBodyPart.setFileName( f.getName() );
					multipart.addBodyPart(messageBodyPart);
				}
				else{
					System.out.println("No encontr� archivo...");
				}	
			}
		}
		//Se junta el mensaje y los archivos adjuntos
		message.setContent(multipart);
		//Se env&iacute;a el e-mail
		Transport.send( message );

	}

	catch (Exception e) {
		envioExitoso = false;
		e.printStackTrace();
	}
	finally{
		//Se eliminan del servidor los archivos adjuntos
		if( adjuntos!=null && adjuntos.size()>0 ){
			for( String rutaAdjunto : adjuntos ){
				try{
					File arch = new File(rutaAdjunto);
					arch.delete();
				}

				catch (Exception e) { 
					e.printStackTrace();
				}
			}
		}
	}

	return envioExitoso;
}

public File getFileDesdeInputStream(InputStream entrada, String nombre){
	File archivo = new File(nombre);
	try{
	 FileOutputStream salida = new FileOutputStream(archivo);
	 IOUtils.copy(entrada, salida);

	} catch(Exception e){
			e.printStackTrace();
	}
	return archivo;
}

}
