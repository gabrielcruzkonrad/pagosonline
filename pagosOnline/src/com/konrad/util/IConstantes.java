package com.konrad.util;

public interface IConstantes {

	public static final Integer MONTO_GENERACION_RECIBO =10;
	public static final String HORA_BLOQUEO_PAGO ="23:30";
	
	// Constantes para indicar las operaciones sobre un registro
	final String INSERTAR = "I";
	final String EDITAR = "U";
	final String SELECCIONAR = "S";

	//Tipos de Mensaje
	public static final String ERROR ="E";
	public static final String WARNING ="W";
	public static final String INFORMATION ="I";
	public static final String CONFIRM ="C";
	public static final String ERROR_VENCIDO= "V";
	public static final String ACTIVO = "A";

	//imagenes para los tipos de mensajes
	public static final String ERROR_IMAGE ="imagenes/error-icon.png";
	public static final String WARNING_IMAGE ="imagenes/Alarm-Warning-icon.png";
	public static final String INFORMATION_IMAGE ="imagenes/Info-icon.png";
	public static final String CONFIRMATION_IMAGE ="imagenes/ok-icon.png";
	public static final String SEARCH_IMAGE ="imagenes/search-icon.png";
	public static final String ERROR_VENCIDO_IMAGE= "imagenes/warning-vencido.png";
	
	
	
	//Mensajes personalizados para pagos en línea
	public static final String ERROR_PAGO_PENDIENTE ="Lo sentimos; existen pagos pendientes por finalizar, espere unos minutos para volver a consultar";
	public static final String ERROR_GENERACION_RECIBO ="Lo sentimos; ha ocurrido un error inesperado en la generación de su recibo.";
	public static final String ERROR_PAGO_INICIO ="Lo sentimos; el pago no ha iniciado, motivo: ";
	public static final String ERROR_PAGO_SIN_VALOR ="Lo sentimos; el pago no ha iniciado, No existen valores para pago ";
	public static final String CONFIRMACION_INICIO_PAGO ="El pago fue iniciado exitosamente. Espere un momento...";
	public static final String ADVERTENCIA_MONEDA_EXTRANJERA ="Conversión monetaria informativa, será aplicado el valor en Pesos Colombianos (COP)";
	public static final String INFORMACION_PAGO_VENCIMIENTO ="Es recomendable realizar este pago ya que es el más próximo a vencerse.";
	public static final String INFORMACION_PAGO_VENCIDO ="Una vez finalice su pago vencido; espere aproximadamente una Hora para que se refleje en el sistema Financiero antes de realizar otro.";
	public static final String INFORMACION_PAGO ="Una vez finalice su pago; espere aproximadamente una (1) Hora para que se refleje en el sistema Financiero antes de realizar otro.";
	public static final String INFORMACION_PAGO_VENCIDO_BANCO="En caso de que sea realizado el pago en banco con recibo impreso de código de barras este se reflejará al siguiente día.";
	public static final String INFORMACION_GENERACION_RECIBO ="Una vez presione el botón \" Generar recibo y pagar !!\" se liquidará de nuevo el monto de su recibo a la fecha de pago; el consecutivo de pago cambiará.";
	public static final String INFORMACION_PAGO_VENCIMIENTO_ORDEN ="El pago de esta orden NO garantiza la aprobación del semestre en curso ni la calidad de estudiante.";
	public static final String INFORMACION_PAGO_ABONO ="Espere aproximadamente una (1) Hora para que su abono se refleje en el sistema Financiero antes de realizar otro.";
	public static final String INFORMACION_PAGO_ABONO_BANCO ="Al abonar en banco con recibo impreso de código de barras este pago se reflejará al siguiente día.";
	public static final String ADVERTENCIA_PUBLICACION ="Si el pago corresponde a una Publicación entonces la comisión es Cero (0)";
	public static final String INFORMACION_AUTENTICACION_ACADEMICO="Para continuar el proceso de impresión por favor identifiquese como estudiante.";
	public static final String INFORMACION_ACTUALIZACION_DATOS ="Con el fin de brindar un mejor servicio lo invitamos a actualizar sus datos personales.";
	public static final String INFORMACION_ACTUALIZACION_IDENTIDAD="Para actualización de documento de identidad y/o nombres se requiere presentación de fotocopia del documento de identidad en la oficina de Registro Académico.";
	public static final String INFORMACION_CAMPOS_OBLIGATORIOS ="Tenga en cuenta que los campos marcados (*) son campos obligatorios.";
	public static final String MENSAJE_OPCION_NO_SELECCIONADA = "No se ha seleccionado opción alguna";
	public static final String MENSAJE_SALDO_CARTERA = "Su estado en cartera impide continuar esta operación";
	public static final String MENSAJE_ESTADO_BIBLIOTECA = "Su estado en biblioteca impide continuar esta operación";
	public static final String MENSAJE_CONTRASENA_INVALIDA= "La contraseña es inválida o no ha sido digitada";
	public static final String MENSAJE_INFORMACION_INVALIDA="La información digitada es inválida";
	public static final String INFORMACION_ABONO = "El pago de abonos no implica automáticamente la aplicación de beneficios o descuentos por pronto pago; para obtenerlos, es necesario solicitarlos directamente a la dirección administrativa y financiera. Por favor, env�e su solicitud a la dirección de correo electrónico atencion.financiero@konradlorenz.edu.co";
	public static final String INFORMACION_HORARIO_BANCARIO = "Recuerde que si imprime su recibo deberá pagar en horario bancario.";
	public static final String INFORMACION_FECHA_GENERACION = "Por favor registre la fecha de pago de su recibo.";
	public static final String MENSAJE_PERMITIR_POPUPS ="";
	public static final String MENSAJE_PERMITIR_POPUPS_INSCRIPCION ="";
	public static final String MENSAJE_PATROCINADOR_REGISTRADO ="Se ha registrado la inscripción y el patrocinador.";
	public static final String MENSAJE_COLEGIO_COLOMBIANO_PSICOLOGIA="Ha manifestado pertenecer al colegio colombiano de psicólogos, por favor espere verificación para aplicaci�n de descuento";
	public static final String MENSAJE_CARTERA ="IMPORTANTE: Tenga en cuenta que su pago será abonado a la obligación mas vencida";
	public static final String MENSAJE_CARTERA_LISTA = "Seleccione obligaciones en orden de vencimiento...";
	public static final String MENSAJE_CARTERA_CHEQUES = "Su cartera presenta cheques devueltos vaya a la opción correspondiente...";
	public static final String MENSAJE_CARTERA_SELECCION = "Aquí puede seleccionar varias obligaciones consecutivamente...";
	public static final String MENSAJE_ABONO_VALOR_INVALIDO = "Valor inválido para registrar un abono...";
	public static final String MENSAJE_ABONO_PERIODO_INVALIDO = "Valor inválido de periodo para registrar un abono...";
	public static final String MENSAJE_NO_CARTERA_ORDEN = "No presenta ordenes de derechos académicos por pagar o cartera pendiente...";
	public static final String MENSAJE_CARTERA_CREDITO = "Su estado de cuenta presenta Saldo... recuerde que si no está al día no se generará orden de matrícula.";
	public static final String MENSAJE_CARTERA_MULTA = "Su estado de cuenta presenta Multa de biblioteca... recuerde que si no está al día no se generará orden de matrícula.";
	public static final String ASUNTO_CORREO_PAGO_PROVISION = "Notificación de pago de cartera provisionada/Castigada";
	public static final String MENSAJE_RECORDATORIO_PERIODO ="No olvide seleccionar el periodo de abono ...";
	public static final String MENSAJE_RECIBO_PAGADO_EDU_CONT = "Este usuario ya registra pago en el programa solicitado";
	public static final String MENSAJE_CONSULTA_MULTAS_PAGO ="El pago de este tipo de conceptos puede realizarlos por las opciones de pago total o abonos, tenga en cuenta "+
															"que sus abonos serán aplicados primero a intereses y capital de sus créditos vigentes, luego órdenes de "+
															"derechos académicos y  finalmente a multas.";
	public static final String ASUNTO_NUEVO_PATROCINADOR = "Notificación nueva empresa";
	public static final String MENSAJE_NUEVO_PATROCINADOR = "A continuación los datos de una nueva empresa para facturación :";
	public static final String MENSAJE_FINAL_PATROCINADOR = "Agradecemos su gestión para la creación del tercero.";
	public static final String MENSAJE_BLOQUEO_HORA_PAGO = "Apreciado estudiante, informamos que en este horario ({hora}) no es posible iniciar transacciones de pago, por favor intente a partir de las 00:01 hrs.";
	public static final String AUTENTICADO = "AUTENTICADO";
	
	// Colores para Mensajes
	public static final String ERROR_COLOR= "#FF9999";
	public static final String WARNING_COLOR= "#FFFFCC";
	public static final String INFORMATION_COLOR= "#99CCFF";
	public static final String CONFIRMATION_COLOR= "#CCFFCC";
	public static final String ERROR_VENCIDO_COLOR="#FFCCCC";
	public static final String STYLE_TEXTO_APLICACION ="font-size:11px;font-family:arial,verdana;";
	public static final String STYLE_TEXTO_SECUNDARIO_APLICACION ="font-size:8px;font-family:arial,verdana;font-style:italic;";
	
	/* Constantes para definir contsantes generales */
	public static final String PERMISO_CONCEDIDO = "S";
	public static final String PERMISO_NO_CONCEDIDO = "N";
	
	//Estados de las transacciones en Zona Pagos
	public static final String ESTADO_FINALIZADO ="1";
	public static final String ESTADO_PENDIENTE_FINALIZAR ="999";
	public static final String ESTADO_PENDIENTE_INICIAR ="888";
	public static final String ESTADO_PENDIENTE_ESTUDIO ="4001";
	public static final String ESTADO_RECHAZADO ="0";
	
	//Monedas usadas para Pagos en l�nea
	public static final String PESOS_COLOMBIANOS ="COP";
	public static final String PESOS_COLOMBIANOS_ICEBERG = "PES";
	public static final String DOLARES_AMERICANOS ="USD";
	public static final String EUROS ="EUR";
	// tama�o de letra para los mensajes
	public static final String TAMANO_LETRA_DEFAULT="11px";
	//imagenes monedas usadas
	public static final String PESOS_COLOMBIANOS_IMAGE ="imagenes/Colombia-icon.png";
	public static final String DOLARES_AMERICANOS_IMAGE ="imagenes/United-States-icon.png";
	public static final String EUROS_IMAGE ="imagenes/Europe-icon.png";
	
	//Tasas y comisiones para poner en Sesi�n
	public static final String TRM_DOLAR ="TRM_DOLAR";
	public static final String TRM_EURO ="TRM_EURO";
	public static final String COMISION_EURO ="COMISION_EURO";
	public static final String COMISION_DOLAR ="COMISION_DOLAR";
	public static final String PARAMETRO_DOLAR ="NEGOCIACION_DOLAR";
	public static final String PARAMETRO_EURO ="NEGOCIACION_EURO";
	
	//Conexi�n a BD academico
	public static final String SERVIDOR_ACADEMICO ="172.17.1.3";
	public static final String USUARIO_ACADEMICO="irsadmin";
	public static final String CONTRASENA_ACADEMICO="icebergrs";
	public static final String DATABASE_ACADEMICO ="escolaris_fukl";
//	public static final String SERVIDOR_ACADEMICO ="192.168.100.67";
//	public static final String USUARIO_ACADEMICO="registros";
//	public static final String CONTRASENA_ACADEMICO="popeye";
//	public static final String DATABASE_ACADEMICO ="escolaris_fukl";
	
	// Constantes acad�mico
	public static final String NOMBRE_SECUENCIA_ESTUDIANTE="ESTUDIANTE";
	public static final String NOMBRE_SECUENCIA_ORDENES ="ORDENES";
	public static final String NOMBRE_SECUENCIA_EST_SOLIC = "ESTADSOL";
	public static final Integer ESTADO_EDUCACION_CONTINUADA = -24;
	public static final Integer PAZ_SALVO_EDUCACION_CONTINUADA = -1;
	public static final Integer ORDEN_PAGO_DEFECTO_ACADEMICO = 1;
	public static final String LUGAR_PAGO_ORDENES_DEFECTO = "2";
	public static final String OBSERVACIONES_SOLICITUD_DEFECTO="Registro creado en proceso automático de inscripción educación Continuada";
	public static final Integer ESTADO_INSCRITO_EDUCACION_CONTINUADA = -150;
	public static final Integer ESTADO_DOCUMENTOS_EDUCACION_CONTINUADA = -147;
	public static final Integer ESTADO_ADMITIDO_EDUCACION_CONTINUADA = -140;
	
	
	// valores por defecto Notificacion 
	public static Integer DIAS_PROXIMO_VENCIMIENTO_NOTIFICACION = 4; 
	public static String CONCEPTO_CAPITAL_NOTIFICACION = "DC";
	public static String CONCEPTO_CAPITAL_REC_PROV = "RR";
	public static String CONCEPTO_CAPITAL_REC_CAST = "RD";
	public static String CONCEPTO_CAPITAL_REC_CAST_INT_PROV = "RB";
	public static String CONCEPTO_CHEQUE_DEVUELTO_NOTIFICACION = "DH";
	public static String CONCEPTO_SANCION_CHEQUE_NOTIFICACION = "SCH";
	public static String CONCEPTO_OTRO_NOTIFICACION = "OT";
	
	// mensajes notificaciones
	public static final String ASUNTO_NOTIFICACION = "Notificaci&oacute;n obligaci&oacute;n financiera Konrad Lorenz";
	public static final String DIRECCION_PLATAFORMA_FINANCIERA = "https://serviciosweb.konradlorenz.edu.co/pagosOnline";
	public static final String MENSAJE_SALUDO_NOTIFICACION ="<p style=\"font-family:arial,verdana;\" >Apreciado(a) estudiante;</p>";
	public static final String MENSAJE_PROXIMO_VENCIMIENTO_NOTIFICACION =
			"<p style=\"font-family:arial,verdana;\" >"+
			"Con un cordial saludo, le recordamos que la obligaci&oacute;n con la Fundaci&oacute;n Universitaria Konrad Lorenz se encuentra "+
			"pr&oacute;xima a vencer, para evitar mayores intereses lo invitamos a realizar su pago en l&iacute;nea a trav&eacute;s de nuestra plataforma de pagos: " + 
			"<a style=\"font-family:arial,verdana; text-decoration:none;\" href=\""+IConstantes.DIRECCION_PLATAFORMA_FINANCIERA+"\">Ir a la plataforma de servicios financieros</a> " + 
			"o imprimir el recibo para realizar el pago en oficinas de Bancos Occidente y Bancolombia.</p>";
	public static final String MENSAJE_VENCIDO_RECIBO_NOTIFICACION = 
			"<p style=\"font-family:arial,verdana;\" >"+
			"Con un cordial saludo, le recordamos que la obligaci&oacute;n con la Fundaci&oacute;n Universitaria Konrad Lorenz se encuentra "+
			"en mora, para evitar mayores intereses lo invitamos a realizar su pago en l&iacute;nea a trav&eacute;s de nuestra plataforma de pagos: "+
			"<a style=\"font-family:arial,verdana; text-decoration:none;\" href=\""+IConstantes.DIRECCION_PLATAFORMA_FINANCIERA+"\""+">Ir a la plataforma de servicios financieros</a>"+
			"o imprimir el recibo para realizar el pago en oficinas de Bancos Occidente y Bancolombia.</p>";
	
	
	public static final String MENSAJE_PAGO_NOTIFICACION = "<p style=\"font-family:arial,verdana;\">Si ya realiz&oacute; el pago, favor hacer caso omiso a este correo.</p> ";
	public static final String MENSAJE_FINALIZACION_NOTIFICACION = "<p style=\"font-family:arial,verdana;\">Cualquier inquietud puede comunicarse al correo: "+ 
	"<a style=\"font-family:arial,verdana; text-decoration:none;\" href=\"mailto:atencion.financiero@konradlorenz.edu.co\">atencion.financiero@konradlorenz.edu.co</a>, a las l&iacute;neas telef&oacute;nicas 3472311 ext. 174 y 178. </p>";
	
	public static final String PIE_PAGINA_NOTIFICACION ="<div style=\" border-radius:5px; background:#f2f2f2; padding:5px 3px 5px 0; \">" +
			"<span><img src=\"http://www.konradlorenz.edu.co/templates/openweb-j17/images/logo.png\"></span> " +
			"<span><p style=\"font-family:arial,verdana; font-size:0.8em;\">Direcci&oacute;n Financiera</span>"+
			"</div>";
	
	/* Constantes para la invocaci�n del servicios Web (Zona Pagos)*/
	
	// id Comercio o tienda
	final String ID_TIENDA ="316";
	final String ID_TIENDA_BANCOLOMBIA ="310";
	//final String ID_TIENDA_BANCOLOMBIA ="316";
	final String ID_TIENDA_EURO ="322";
	final String ID_TIENDA_DOLARES ="321";
	final String ID_TIENDA_PRUEBAS ="312";
	
	//codigo servicio
	final String CODIGO_SERVICIO ="1830";
	final String CODIGO_SERVICIO_BANCOLOMBIA ="0306";
//	final String CODIGO_SERVICIO_BANCOLOMBIA ="1830";
	final String CODIGO_SERVICIO_DOLARES ="0000";
	final String CODIGO_SERVICIO_EURO ="0000";
	final String CODIGO_SERVICIO_PRUEBAS="2701";
	
	//clave servicio
	final String CLAVE_SERVICIO ="Konrad3214";
	final String CLAVE_SERVICIO_BANCOLOMBIA ="Konrad3214";
	final String CLAVE_SERVICIO_DOLARES ="Konrad3214";
	final String CLAVE_SERVICIO_EURO ="Konrad3214";
	final String CLAVE_SERVICIO_PRUEBAS="123";
	
	//ruta servicio
	final String RUTA_SERVICIO_PRUEBAS="https://www.zonapagos.com/t_tiendavirtualfukl/pago.asp?estado_pago=iniciar_pago&identificador=";
	final String RUTA_SERVICIO ="https://www.zonapagos.com/t_fukl2/pago.asp?estado_pago=iniciar_pago&identificador=";
	final String RUTA_SERVICIO_BANCOLOMBIA ="https://www.zonapagos.com/t_fukl/pago.asp?estado_pago=iniciar_pago&identificador=";
	//final String RUTA_SERVICIO_BANCOLOMBIA ="https://www.zonapagos.com/t_fukl2/pago.asp?estado_pago=iniciar_pago&identificador=";
	final String RUTA_SERVICIO_DOLARES ="https://www.zonapagos.com/t_fuklus/pago.asp?estado_pago=iniciar_pago&identificador=";
	final String RUTA_SERVICIO_EURO ="https://www.zonapagos.com/t_fukleur/pago.asp?estado_pago=iniciar_pago&identificador=";
	final String RUTA_CONVERSION_DOLAR ="http://obiee.banrep.gov.co/analytics/saw.dll?Download&Format=CSV&Extension=.csv&BypassCache=true&Path=/shared/Consulta%20Series%20Estadisticas%20desde%20Excel/1.%20Tasa%20de%20Cambio%20Peso%20Colombiano/1.1%20TRM%20-%20Disponible%20desde%20el%2027%20de%20noviembre%20de%201991/IQY%20Serie%20historica&NQUser=publico&NQPassword=publico&SyncOperation=1";
	//final String RUTA_CONVERSION_DOLAR ="https://www.superfinanciera.gov.co/descargas?com=institucional&name=pubFile1005930&downloadname=historia.xls";
	//final String RUTA_CONVERSION_EURO = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
	final String RUTA_CONVERSION_EURO = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml";
	
	// entidades
	final String TIPO_ENTIDAD_BANCOLOMBIA = "FIN";
	final String TIPO_ENTIDAD_OCCIDENTE = "FIN";
	final String ENTIDAD_BANCOLOMBIA = "7";
	final String ENTIDAD_OCCIDENTE = "23";

	// ORGANIZACION
	final String ORGANIZACION_DEFECTO = "1";

	/* constantes para definir la m�scara de los numeros */
	public static final String FORMATO_NUMERO = "###,###,###,###,###";

	/* constantes para definir el formato de las fechas */
	public static final String FORMATO_FECHA = "dd/MM/yyyy";

	public static final String FORMATO_FECHA_COMPLETO = "yyyy/MM/dd HH:mm:ss";

	public static final String FORMATO_FECHA_MES = "MMM dd 'de' yyyy";

	/* constantes para definir el tama�o de la paginaci�n de los listados largos */
	public static final int TAMANO_PAGINACION = 15;
	public static final int TAMANO_PAGINACION_BANDBOX = 5;
	
	/* constante para definir el archivo propiedades de la aplicaci�n */
	public static final String PROPIEDADES_APLICACION = "aplicacion.properties";
	public static final String RUTA_REPORTES = "reportes/fuentes/";
	public static final String RUTA_REPORTES_EJECUTABLES = "reportes/ejecutables/";
	public static final String RUTA_IMAGENES = "imagenes/";

	/* constante para definir el archivo propiedades del LDAP */
	public static final String PROPIEDADES_LDAP = "ldap.properties";


	/*
	 * constantes para definir los identificadores y los �conos que informan del
	 * estado del �ltimo proceso realizado
	 */
	public static final String ESTADO_IMAGEN_OK = "/imagenes/estado_ok.gif";
	public static final String ESTADO_IMAGEN_ERROR = "/imagenes/estado_error.gif";
	// --
	public static final int ESTADO_INSERCION_OK = 1;
	public static final int ESTADO_INSERCION_ERROR = 2;

	public static final int ESTADO_EDICION_OK = 3;
	public static final int ESTADO_EDICION_ERROR = 4;

	public static final int ESTADO_BORRAR_OK = 5;
	public static final int ESTADO_BORRAR_ERROR = 6;

	public static final int ESTADO_DEFAULT_OK = 7;
	public static final int ESTADO_DEFAULT_ERROR = 8;
	
	public static final int ESTADO_DEFAULT_ADVERTENCIA = 9;

	/* constantes para definir si un usuario esta o no autenticado */
	public static final Integer USUARIO_AUTENTICADO = new Integer(1);
	public static final Integer USUARIO_NO_AUTENTICADO = new Integer(0);

	/* parámetros guardados en la session */
	public static final String USUARIO_SESSION = "usuario";
	public static final String USUARIO_SESSION2 = "usuario2";
	public static final String USUARIO_UNIDAD = "unidad";
	public static final String INFORMACION_SESION = "INFO_SESION";

	// standard report parameter names
	public static final String REP_USER_ID = "ICEBERGRS_USER_ID";
	public static final String REP_USER_NAME = "ICEBERGRS_USER_NAME";
	public static final String REP_IMAGE_DIR = "ICEBERGRS_IMAGE_DIR";
	public static final String REP_REPORT_DIR = "ICEBERGRS_REPORT_DIR";
	public static final String REP_EXPORT_TYPE_PARAM = "ICEBERGRS_EXPORT_TYPE";


	public static final String MENSAJE_INSCRIPCION_PSICO_WEB =
	"Con ocasión al #{TITULO_ENCUENTRO_KONRAD} de la Fundaci�n Universitaria Konrad Lorenz, Psicoweb y LPC Lider por competencias,"+
	" plataformas para evaluaci�n y selecci�n de personal, te invitan a hacerte acreedor de una membres�a GRATUITA  de las plataformas PSICOWEB y LPC"+
	" para que evalúes tus candidatos. (Aplican Condiciones y Restricciones). \n"+
	"Informes: #{CORREO_INFORMES_PSICOWEB} ";
	
	public static final String URL_POLITICAS_TRATAMIENTO_PERSONALES = "https://www.konradlorenz.edu.co/politicas-para-el-tratamientos-de-datos-personales/";
	

	// -nombre standard de un reporte
	public static final String REP_NOMBRE_DEFAULT = "reporte";
	public static final String REP_ORDEN = "ORDEN_WEB2";
	public static final String REP_ORDEN_TRES_CUERPOS = "ORDEN_WEB";
	public static final String REP_CUOTA_CREDITO = "CUOTA_CREDITO";

	// -- Se definen las constantes S = SI y N = NO para utiliazar en
	// verificaciones
	public static final String GENERAL_SI = "S"; // -podría ser S = SI y N = NO
	public static final String GENERAL_NO = "N";

	// --los reportes en jasper tienen predefinidos una serie de parámetros
	// --los cuales son de uso interno de la librería estos no deberían verse
	// --tales parámetros tienen prefijo REPORT_% y ICEBERGRS_%
	public static final String REP_OCULTAR_PARAMETROS = GENERAL_SI; // -podrá
	// ser S =
	// SI y N =
	// NO
	public static final String REP_OCULTAR_PARAMETROS_PREFIJO1 = "REPORT_";
	public static final String REP_OCULTAR_PARAMETROS_PREFIJO2 = "ICEBERGRS_";
	public static final String REP_OCULTAR_PARAMETROS_PREFIJO3 = "LOCAL";
	public static final String REP_OCULTAR_PARAMETROS_PREFIJO4 = "IS";
	public static final String REP_OCULTAR_PARAMETROS_PREFIJO5 = "JASPER_";
	public static final String REP_OCULTAR_PARAMETROS_PREFIJO6 = "NOMBRE_PLANTILLA";

	// mensajes generales default
	public static final String MENSAJE_DEFAULT_OK = "OK";
	public static final String MENSAJE_DEFAULT_ERROR = "Error";
	public static final String MENSAJE_DEFAULT_ADVERTENCIA = "Advertencia";


	// mensajes generales validación del formulario
	public static final String MENSAJE_FORMULARIO_VACIO = "Verifique el formulario de entrada de datos, y llene aquellos que lo requieran";

	public static final String PROVEEDOR_HIBERNATE = "com.casewaresa.framework.provider.HibernateProvider";

	// mensajes para restriccion llave unica en archivo adjunto
	public static final String MENSAJE_LLAVE_UNICA_CONTEXTO = "El Archivo que intenta adjuntar ya está asociado al Contexto seleccionado.";
	public static final String MENSAJE_LLAVE_UNICA_FORMA = " El Archivo que intenta adjuntar ya está asociado a la Forma seleccionada.";

	// Constantes para la creación de TABs en el menú.
	public static final String IDTAB_CABECERA = "cabeza";
	public static final String IDTAB_CONTENIDO = "cuerpo";
	public static final String SEPERADOR_RENGLON_SI = "S";
	public static final String SEPERADOR_RENGLON_NO = "N";
	public static final String ACCION_RENGLON_EDICION = "E";
	public static final String ACCION_RENGLON_NUEVO = "N";
	public static final String CADENA_SEPARADOR = "<-------------->";
	public static final String SUBIR_ORDEN = "S";
	public static final String BAJAR_ORDEN = "B";
	public static final String SUBIR_NIVEL = "S";
	public static final String BAJAR_NIVEL = "B";
	public static final String[] DOCUMENTOS_EXCEPTUADOS = {"ECAE","FVA","FTES","CV"};


	public static final String ID_SALIR = "idMNZTitemSalir";
	public static final String ID_CERRAR_TODOS = "idMNZTitemCerrarTodos";
	public static final boolean AUTENTICACION_LDAP = true;
	public static final String PANTALLA_LOGGEO = "/login.zul";
	
	public static final String MENSAJE_CANCELAR_FORMULARIO = "Está seguro que desea cancelar la operación?";
	
	// constantes opciones nuevo menu
	public static final String ID_ANCLA_DERECHOS_ACADEMICOS ="idDerechosAcademicosNew";
	public static final String ID_ANCLA_CREDITOS ="idCreditosNew";
	public static final String ID_ANCLA_RECIBOS_SALDOS ="idRecibosPorSaldosNew";
	public static final String ID_ANCLA_OTROS_PAGOS ="idOtrosPagosNew";
	public static final String ID_ANCLA_CARTERA ="idCarteraNew";
	public static final String ID_ANCLA_CHEQUES_DEVUELTOS ="idChequesDevueltosNew";
	public static final String ID_ANCLA_MULTAS_BIBLIOTECA ="idMultasBibliotecaNew";
	public static final String ID_ANCLA_ABONOS ="idAbonosIcebergNew";
	public static final String ID_ANCLA_PAGO_TOTAL ="idPagoTotalIcebergNew";
	public static final String ID_ANCLA_SALIR ="idMenuSalirPagosNew";
	public static final String RUTA_ABONOS ="pages/abonosIceberg.zul";
	public static final String RUTA_PAGO_TOTAL ="pages/pagoTotalRecibo.zul";
	public static final String RUTA_NOTAS_PERIODO_ACTUAL ="pages/notasPeriodoActual.zul";
	public static final String RUTA_NOTAS_HISTORICO ="pages/notasHistorico.zul";
	public static final String RUTA_INFORME_ESTADO ="pages/informeEstado.zul";
	public static final String RUTA_AUTENTICACION_ACADEMICO ="pages/autenticacionEscolaris.zul";
	public static final String RUTA_ACTUALIZACION_ACADEMICO ="pages/actualizacionEscolaris.zul";
	public static final String RUTA_DERECHOS_ACADEMICOS ="pages/derechosAcademicos.zul";

	public static final String FORMATO_FECHA_RESPONSE_VISA = "dd/MM/yyyy hh:mm aa";
	public static final String VALOR_VISA_DEFAULT = "VISA";
	public static final String FORMATO_NUMERO_WS = "########0.00"; 
	public static final String MONEDA_VISA_DEFAULT = "PEN";
	public static final String MONEDA_PAGOS_ONLINE_DEFAULT = "COP";
	public static final String ESTADO_CONFIRMADO_DEFAULT = "4";
	public static final String ESTADO_RECHAZADO_DEFAULT = "6";
	public static final String ESTADO_PENDIENTE_DEFAULT = "7";
	public static final String ESTADO_ERROR_DEFAULT = "104";
	public static final String ESTADO_EXPIRADO_DEFAULT = "5";
	public static final String APPROVED_TRANSACTION = "APPROVED";
	public static final String EXPIRED_TRANSACTION = "EXPIRED";
	public static final String DECLINED_TRANSACTION = "DECLINED";
	public static final String ERROR_TRANSACTION = "ERROR";
	public static final String PENDING_TRANSACTION = "PENDING";
	public static final String VISA_PREFIX = "VISA";
	public static final String DINERS_PREFIX = "DINERS";
	public static final String AMERICAN_EXPRESS_PREFIX = "AMERICAN";
	public static final String AMERICAN_EXPRESS_PREFIX2 = "AMEX";
	public static final String MASTER_CARD_PREFIX = "MASTER";
	public static final String PSE_PREFIX = "PSE";
	public static final String TIPO_MEDIO_PAGO_PSE_PAYU ="4";
	public static final String MEDIO_PAGO_PSE_PAYU ="25";
	public static final String BANCO_PSE_DEFAULT_PAYU ="PSE";
	public static final String TIPO_MEDIO_PAGO_TC_PAYU ="2";
	public static final String MEDIO_PAGO_TC_VISA_PAYU ="10";
	public static final String MEDIO_PAGO_TC_MASTER_CARD_PAYU ="11";
	public static final String MEDIO_PAGO_TC_AMERICAN_EXPRESS_PAYU ="12";
	public static final String MEDIO_PAGO_TC_DINERS_PAYU ="22";
	
	
	
	public static final String URL_WEBCHECKOUT_PAYULATAM ="URL_WEBCHECKOUT_PAYULATAM";
	public static final String ACCOUNT_ID_PAYULATAM ="ACCOUNT_ID_PAYULATAM";
	public static final String ACCOUNT_ID_PAYULATAM_EVENTOS ="ACCOUNT_ID_PAYULATAM_EVE";
	public static final String ACCOUNT_ID_PAYULATAM_PUBLICACIONES ="ACCOUNT_ID_PAYULATAM_PUB";
	public static final String API_KEY_PAYULATAM ="API_KEY_PAYULATAM";
	public static final String URL_REST_PAYULATAM ="URL_REST_PAYULATAM";
	public static final String PUBLIC_KEY_PAYULATAM ="PUBLIC_KEY_PAYULATAM";
	public static final String EXIGE_AUTENTICACION_ORDEN ="EXIGE_AUTENTICACION_ORDEN";
	public static final String API_LOGIN_PAYULATAM ="API_LOGIN_PAYULATAM";
	public static final String MERCHANT_ID_PAYULATAM ="MERCHANT_ID_PAYULATAM";
	public static final String COMMAND_API_REFERENCE_CODE_PAYU="ORDER_DETAIL_BY_REFERENCE_CODE";
	public static final String ABREVIATURA_COMPONENTE_CXC ="CC";
	public static final String PROGRAMAS_VALIDA_MODALIDAD = "PROGRAMAS_VALIDA_MODALIDAD";
	public static final String DESCUENTO_EGRESADO_EDUCACION_CONTINUA="DSCTO_EDU_CONT_EGRESADO";
	public static final String DESCUENTO_ESTUDIANTE_EDUCACION_CONTINUA="DSCTO_EDU_CONT_ESTUDIANTE";
	public static final String DESCUENTO_FUNCIONARIO_EDUCACION_CONTINUA="DSCTO_EDU_CONT_FUNCIONARIO";
	public static final String PORCENTAJE_DESCUENTO_EGRESADO_EDUCACION_CONTINUA="PORC_EDU_CONT_EGRESADO";
	public static final String PORCENTAJE_DESCUENTO_ESTUDIANTE_EDUCACION_CONTINUA="PORC_EDU_CONT_ESTUDIANTE";
	public static final String PORCENTAJE_DESCUENTO_FUNCIONARIO_EDUCACION_CONTINUA="PORC_EDU_CONT_FUNCIONARIO";
	public static final String INCREMENTO_GRUPO_ORDEN_EDUCACION_CONTINUADA="INCREMENTO_GRUPO_EDU_CONT";
	public static final String FUENTE_FUNCION_DESCUENTO_EDUCACION_CONTINUADA="FUENTE_FUNCION_DESC_EDU_CONT";
	
	public static final String PAGES_PATH = "/pages/formulario/";

	// TIPOS DE RECIBO DE CONSIGNACION PARA DIRECCIONAR ACCOUNT ID PAYU LATAM
	public static final String TIPO_RECIBO_MATRICULAS ="MATRICULAS";
	public static final String TIPO_RECIBO_PUBLICACIONES ="PUBLICACIONES";
	public static final String TIPO_RECIBO_EVENTOS ="EVENTOS";
	public static final String[] DOCUMENTOS_MATRICULAS_PAYU={"OM","OMP","FTES","ECAE","DG","7MAT","CV","SUP","DP","CL"};
	public static final String[] DOCUMENTOS_EVENTOS_PAYU={"FVA"};
	public static final String[] DOCUMENTOS_PUBLICACIONES_PAYU={"FVP"};
	
	public static final String TIPO_INVOCACION_DERECHOS_ACADEMICOS = "DA";
	public static final String TIPO_INVOCACION_CREDITOS_VIGENTES = "CR";
	public static final String TIPO_INVOCACION_CREDITOS_VENCIDOS = "CV";
	public static final String TIPO_INVOCACION_ABONOS = "AB";
	public static final String TIPO_INVOCACION_PAGO_TOTAL = "PT";
	public static final String TIPO_INVOCACION_CARTERA = "CA";
	public static final String TIPO_INVOCACION_OTROS_PAGOS= "OP";
	public static final String TIPO_INVOCACION_CHEQUES_DEVUELTOS= "CD";
	public static final String TIPO_INVOCACION_RECIBOS_SALDOS= "RS";
	public static final String TIPO_INVOCACION_HISTORICO_NOTAS="HN";
	public static final String RUTA_I18N_ES="/WEB-INF/i3-label_es_CO.properties";
	public static final String RUTA_I18N_EN="/WEB-INF/i3-label_en_CNTY.properties";
	public static final String POBLACION_DEFECTO = "1";
	public static final String DEPARTAMENTO_DEFECTO = "11";
	public static final String PAIS_DEFECTO = "57";
	public static final String DIRECCION_DEFECTO ="CRA 9BIS # 68-43";
	
	
	// Orden
	public static final String ESTADO_CONFIRMADO_ORDEN ="V";
	
	//Detalle orden
	public static final String LIQUIDADO = "N";
	public static final String ESTADO = "I";
	
	// carrito
	public static final String VER_CARRITO = "CART";
	public static final String VER_HISTORICO_CARRITO = "HIST";
}



