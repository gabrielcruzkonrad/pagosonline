package com.konrad.util;
import java.util.List;

public class EmailThread implements Runnable {

	private String servidorCorreo; 
	private String puertoCorreo;
	private String emisor; 
	private String asunto; 
	private List<String> receptores; 
	private List<String> receptoresCopia; 
	private String mensaje; 
	private List<String> adjuntos; 
	private String contrasena;
		
	public String getServidorCorreo() {
		return servidorCorreo;
	}

	public void setServidorCorreo(String servidorCorreo) {
		this.servidorCorreo = servidorCorreo;
	}

	public String getPuertoCorreo() {
		return puertoCorreo;
	}

	public void setPuertoCorreo(String puertoCorreo) {
		this.puertoCorreo = puertoCorreo;
	}

	public String getEmisor() {
		return emisor;
	}

	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public List<String> getReceptores() {
		return receptores;
	}

	public void setReceptores(List<String> receptores) {
		this.receptores = receptores;
	}

	public List<String> getReceptoresCopia() {
		return receptoresCopia;
	}

	public void setReceptoresCopia(List<String> receptoresCopia) {
		this.receptoresCopia = receptoresCopia;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public List<String> getAdjuntos() {
		return adjuntos;
	}

	public void setAdjuntos(List<String> adjuntos) {
		this.adjuntos = adjuntos;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	@Override
	public void run() {
		this.run(	
				this.getServidorCorreo(),
				this.getPuertoCorreo(),
				this.getEmisor(),
				this.getAsunto(),
				this.getReceptores(),
				this.getReceptoresCopia(),
				this.getMensaje(),
				this.getAdjuntos(),
				this.getContrasena()
				);
	}
	
	public void run(String servidorCorreo, String puertoCorreo, String emisor, String asunto, 
			List<String> receptores, List<String> receptoresCopia, String mensaje, List<String> adjuntos, String contrasena){
		
		SendEmail emailSender = new SendEmail();
		emailSender.envia(servidorCorreo, puertoCorreo, emisor, asunto, receptores, receptoresCopia, mensaje, adjuntos, contrasena);
		
	}

}
