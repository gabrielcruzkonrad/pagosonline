package com.konrad.util;

import java.util.ResourceBundle;

public class RutaInformes {
	
private static final String rutainformes;
	
	static {
		try {
		String archivoconfiguracion ="fukl.siii.properties.documentos";
		ResourceBundle rb=ResourceBundle.getBundle(archivoconfiguracion);
		rutainformes=rb.getString("RUTA_INFORME");
		} catch (Exception e) {
		e.printStackTrace();
		throw new RuntimeException ("Error initializing archivo  de propiedades de archivos: " + e);
		}
		}
	
	public static String getRutaInformes () {
		return rutainformes;
		}

}
