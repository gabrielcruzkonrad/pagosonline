package com.konrad.util;

import org.zkoss.zk.ui.Executions;

import com.konrad.action.ErrorAction;
import org.apache.log4j.Logger;

public class GestionaError {

	protected static Logger log = Logger.getLogger(GestionaError.class);
	
	public void mostrarError(String mensaje,String tipoerror)
	{
		try{
		ErrorAction winError = (ErrorAction)Executions.createComponents("/pages/winerror.zul", null, null); 
		winError.doModal( mensaje,tipoerror);
		}
		catch (Exception ex){
			log.info(ex.getMessage());
			}
	}
	
}
