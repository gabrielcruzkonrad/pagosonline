/**
 * 
 */
package com.konrad.util;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;

import com.konrad.domain.Usuario;
import com.konrad.facade.ParametrizacionFac;
//import com.konrad.facade.TreeFacadePorDemanda;


@SuppressWarnings("unchecked")
public class TreeIceberg {

	/**
	 * 
	 */
	private Tree tree;
	private Treeitem itemSelected;
	
	private List<CellListaSeleccion> listadoObjetos;
	private Long secItemSelected = null;
	private List<Integer> path = null;
	private MyItemTree myItemTree;
	private Long ordenHermanos;
	private String md5;
	private String textoBuscar = null;
	private Integer posicionObjeto = 0;
	private int posicionSG = 0;
	private Integer arbolBusquedaObjeto;
	private Label idZLblEtiquetaPredecesor;
	private Label idZLblNombrePredecesor;
	private String etiqueta;
	private Caption idZCaptionTreeIceberg;
	private String ids;
	private String consultaObtenerArbol;
	private String consultaPath;
	private String consultaObtener;
	private String consultaobtenerListado;
	private String consultaintercambiarHermanos;
	private String consultaActualizarNivel;
    private Map<String, Object> parameter;
    private Class<?> treeItemRender = TreeItemRenderer.class;
    protected static Logger log = Logger.getLogger(TreeIceberg.class);
    
    
    
	public void afterCompose() {
		this.onAgregarEventos();
		this.loadTree();
	}

	public Tree getTree() {
		return tree;
	}

	public void setTree(Tree tree) {
		this.tree = tree;
	}

	public Treeitem getItemSelected() {
		return itemSelected;
	}

	public void setItemSelected(Treeitem itemSelected) {
		this.itemSelected = itemSelected;
	}

	public Long getSecItemSelected() {
		return secItemSelected;
	}

	public void setSecItemSelected(Long secItemSelected) {
		this.secItemSelected = secItemSelected;
	}



	public List<Integer> getPath() {
		return path;
	}

	public void setPath(List<Integer> path) {
		this.path = path;
	}

	public MyItemTree getMyItemTree() {
		return myItemTree;
	}

	public void setMyItemTree(MyItemTree myItemTree) {
		this.myItemTree = myItemTree;
	}

	public Long getOrdenHermanos() {
		return ordenHermanos;
	}

	public void setOrdenHermanos(Long ordenHermanos) {
		this.ordenHermanos = ordenHermanos;
	}

	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}

	public String getTextoBuscar() {
		return textoBuscar;
	}

	public void setTextoBuscar(String textoBuscar) {
		this.textoBuscar = textoBuscar;
	}

	public Integer getPosicionObjeto() {
		return posicionObjeto;
	}

	public void setPosicionObjeto(Integer posicionObjeto) {
		this.posicionObjeto = posicionObjeto;
	}

	public int getPosicionSG() {
		return posicionSG;
	}

	public void setPosicionSG(int posicionSG) {
		this.posicionSG = posicionSG;
	}

	public Integer getArbolBusquedaObjeto() {
		return arbolBusquedaObjeto;
	}

	public void setArbolBusquedaObjeto(Integer arbolBusquedaObjeto) {
		this.arbolBusquedaObjeto = arbolBusquedaObjeto;
	}

	public Label getIdZLblEtiquetaPredecesor() {
		return idZLblEtiquetaPredecesor;
	}

	public void setIdZLblEtiquetaPredecesor(Label idZLblEtiquetaPredecesor) {
		this.idZLblEtiquetaPredecesor = idZLblEtiquetaPredecesor;
	}

	public Label getIdZLblNombrePredecesor() {
		return idZLblNombrePredecesor;
	}

	public void setIdZLblNombrePredecesor(Label idZLblNombrePredecesor) {
		this.idZLblNombrePredecesor = idZLblNombrePredecesor;
	}

	public Caption getIdZCaptionTreeIceberg() {
		return idZCaptionTreeIceberg;
	}

	public void setIdZCaptionTreeIceberg(Caption idZCaptionTreeIceberg) {
		this.idZCaptionTreeIceberg = idZCaptionTreeIceberg;
	}

	public Map<String, Object> getParameter() {
		return parameter;
	}

	public Class<?> getTreeItemRender() {
		return treeItemRender;
	}

	public void setParameter(Map<String, Object> parameter) {
		this.parameter = parameter;
	}

	public void setTreeItemRender(Class<?> treeItemRender) {
		this.treeItemRender = treeItemRender;
	}


	public String getIds() {
		return ids;
	}


	public void setIds(String ids) {
		this.ids = ids;
	}

	public Label getLblEtiquetaPredecesor() {
		return idZLblEtiquetaPredecesor;
	}

	public void setLblEtiquetaPredecesor(Label idZLblEtiquetaPredecesor) {
		this.idZLblEtiquetaPredecesor = idZLblEtiquetaPredecesor;
	}

	public Label getLblNombrePredecesor() {
		return idZLblNombrePredecesor;
	}

	public void setLblNombrePredecesor(Label idZLblNombrePredecesor) {
		this.idZLblNombrePredecesor = idZLblNombrePredecesor;
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	
	public String getConsultaObtenerArbol() {
		return consultaObtenerArbol;
	}

	public void setConsultaObtenerArbol(String consultaObtenerArbol) {
		this.consultaObtenerArbol = consultaObtenerArbol;
	}

	public String getConsultaActualizarNivel() {
		return consultaActualizarNivel;
	}

	public void setConsultaActualizarNivel(String consultaActualizarNivel) {
		this.consultaActualizarNivel = consultaActualizarNivel;
	}

	public String getConsultaPath() {
		return consultaPath;
	}

	public void setConsultaPath(String consultaPath) {
		this.consultaPath = consultaPath;
	}

	public String getConsultaObtener() {
		return consultaObtener;
	}

	public void setConsultaObtener(String consultaObtener) {
		this.consultaObtener = consultaObtener;
	}

	public String getConsultaobtenerListado() {
		return consultaobtenerListado;
	}

	public void setConsultaobtenerListado(String consultaobtenerListado) {
		this.consultaobtenerListado = consultaobtenerListado;
	}

	public String getConsultaintercambiarHermanos() {
		return consultaintercambiarHermanos;
	}

	public void setConsultaintercambiarHermanos(
			String consultaintercambiarHermanos) {
		this.consultaintercambiarHermanos = consultaintercambiarHermanos;
	}

	
	@SuppressWarnings("rawtypes")
	public void onAgregarEventos() {
		log.info("Ejecutando metodo [ agregarEventos ]");
		tree.addEventListener(Events.ON_SELECT, new EventListener() {

			public void onEvent(Event arg0) throws Exception {
				onSeleccionarMaestro(tree.getSelectedItem());

			}
		});


	}
	

	public void loadTree() {
		log.info("Ejecutando metodo [ loadTree ]");
		// limpia historico de mensajes

		List<MyItemTree> lista = null;

		try {
			// consultamos el arbol de tipo dato
			Session zkSession = Sessions.getCurrent(); 
	        HttpSession nativeHttpSession = (HttpSession) zkSession.getNativeSession();
			
			Usuario infusuariotmp  = new Usuario();
	        infusuariotmp=(Usuario)nativeHttpSession.getAttribute("usuario");
			
			    lista = (List<MyItemTree>) ParametrizacionFac.getFacade().obtenerListado("",infusuariotmp.getUsuario());
			// enviamos el arbol y la lista a la
			// interfaz web para que sea dibujado
//			    (Treechildren tree, List<MyItemTree> listaDatos, 
//						String etiqueta, String consulta, String ids, boolean treeItemRenderMovimiento)
//			String etiqueta = new String("GE");
//			String ids = new String("GEZ");
//			String consulta = new String("Consulta");
//			Boolean movimiento = true;
			
			    
//			TreeFacadePorDemanda.getFacade().buildTreePorDemanda(tree, lista,
//					etiqueta,consulta,ids,movimiento);

			// Si el arbol de las unidades tiene
			// datos
			if (lista.size() > 1) {
				log.info("listado Original: "+lista.size());	
				List<Treeitem> listaItem = new LinkedList<Treeitem>(
						tree.getItems());
				log.info("listado posterior: "+listaItem.size());
				this.onSeleccionarMaestro(listaItem.get(0));
				// Enviamos el primer treeItem
				// para que lo seleccione.
				tree.setSelectedItem(listaItem.get(0));
			} else {
					if(lista.size()==1){
				this.onSeleccionarMaestro((Treeitem) ((Treechildren) tree
						.getChildren().get(0)).getFirstChild());

				tree.setSelectedItem((Treeitem) ((Treechildren) tree
						.getChildren().get(0)).getFirstChild());
					} else
					{
						Integer resultado = Messagebox.show("Esta cuenta de usuario: "
								+infusuariotmp.getUsuario()+" no tiene asociado un ROL para Pagos Online en Iceberg ","Error",
					        	   Messagebox.OK, Messagebox.ERROR);
						log.info("resultado: "+resultado);
					}
			}

			// this.getGbxTamanoMensaje().setHeight("auto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onSeleccionarMaestro(Treeitem treeItem) {

		log.info("Ejecutando el metodo [ onSeleccionarMaestro ]...");
		// limpiamos el formulario

		String nombrePredecesor = null;
		MyItemTree objeto = null;

		try {
			// Asignamos los valores del vector de
			// el item seleccionado de Fondo al
			// objeto de tipo MyItemTree.java
			this.itemSelected = treeItem;

			Session zkSession = Sessions.getCurrent(); 
	        HttpSession nativeHttpSession = (HttpSession) zkSession.getNativeSession();
			
			Usuario infusuariotmp  = new Usuario();
	        infusuariotmp=(Usuario)nativeHttpSession.getAttribute("usuario");
			
			this.secItemSelected = new Long(
					((Treecell) ((Treerow) this.itemSelected.getFirstChild())
							.getChildren().get(4)).getLabel());
		

			log.info("Primera Celda TreeItem "+
					((Treecell) ((Treerow) this.itemSelected.getFirstChild())
							.getChildren().get(0)).getLabel());

			log.info("Segunda Celda TreeItem "+
					((Treecell) ((Treerow) this.itemSelected.getFirstChild())
							.getChildren().get(1)).getLabel());
			
			log.info("tercera Celda TreeItem "+
					((Treecell) ((Treerow) this.itemSelected.getFirstChild())
							.getChildren().get(2)).getLabel());
			
			log.info("cuarta Celda TreeItem "+
					((Treecell) ((Treerow) this.itemSelected.getFirstChild())
							.getChildren().get(3)).getLabel());
			
			log.info("quinta Celda TreeItem "+
					((Treecell) ((Treerow) this.itemSelected.getFirstChild())
							.getChildren().get(4)).getLabel());
			
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("NODO_BUSCADO", this.secItemSelected);
			parametros.put("ESTADO", null);
			parametros.put("USUARIO", infusuariotmp.getUsuario());
			parametros.put("isNumeroDescendientes", "IS_NOT_NULL");

			this.path = (List<Integer>) ParametrizacionFac.getFacade().obtenerListado("",parametros);
			this.path.add(0, 0);

			//this.myItemTree = (MyItemTree) TreeFacadePorDemanda.getFacade()
				//	.getDataItemSelected(this.tree.getSelectedItem());

			log.info("Valor   : " + this.myItemTree.getValor());
			log.info("Etiqueta: " + this.myItemTree.getEtiqueta());
			log.info("Padre   : " + this.myItemTree.getPadre());
			log.info("Orden   : " + this.myItemTree.getOrdenHermanos());
			log.info("Nivel   : " + this.myItemTree.getNivel());

			if (this.myItemTree.getNivel() == 0) {
				idZLblNombrePredecesor.setValue(null);
				idZLblEtiquetaPredecesor.setVisible(false);

			} else {
					log.info("id de Nodo: "+this.myItemTree.getId());
				Long id = this.myItemTree.getId() != null ? new Long(
						this.myItemTree.getId()) : null;
				log.info("secuencia: " + id);

				
				Map<String, Object> parametrosMenu = new HashMap<String, Object>();
				parametrosMenu.put("USUARIOSIII", infusuariotmp.getUsuario());
				parametrosMenu.put("MENU", id.toString());
				
				
				objeto = (MyItemTree) ParametrizacionFac.getFacade().obtenerRegistro("",parametrosMenu);


				if (objeto.getPadre() != null) {
					nombrePredecesor = objeto.getPadre();
					log.info("Nombre Predecesor: " + nombrePredecesor);
				}

	
				this.ordenHermanos = new Long(objeto.getOrdenHermanos());

			}

		} catch (Exception e) {
			e.printStackTrace();
			
		}
	}

	
	
	public void setItemTree(Tree tree, String data, Integer campo,
			Integer arbolBusqueda, Long secuencia) {
		log.info("Ejecutando mÃ©todo [setItemTree]...");

		try {

			if (secuencia == null) {

				if (data != null)
					if (data.trim().equals(""))
						loadTree();

				// PatrÃ³n de bÃºsqueda
				String exp = null;

				if (data != null)
					exp = data.toUpperCase();
				else
					data = null;

				if (this.textoBuscar == null
						|| (this.textoBuscar != null && !this.textoBuscar
								.equals(data))
						|| !arbolBusqueda.equals(this.arbolBusquedaObjeto)) {
					HashMap<String, Object> parameters = new HashMap<String, Object>();

					if (campo == 0) {
						parameters.put("codigo", exp.isEmpty() ? null : "%"
								+ exp + "%");
						parameters.put("nombre", null);
					} else {
						parameters.put("codigo", null);
						parameters.put("nombre", exp.isEmpty() ? null : "%"
								+ exp + "%");
					}

					if (arbolBusqueda.equals(0)) {
						this.listadoObjetos = (List<CellListaSeleccion>) ParametrizacionFac.getFacade().obtenerListado("",parameters);
						log.info("this.listadoObjetos.size(): "
								+ this.listadoObjetos.size());
					}

					this.posicionObjeto = 0;
					this.posicionSG = 0;
					this.textoBuscar = data;
					this.arbolBusquedaObjeto = arbolBusqueda;
				}

				Object[] listaItems = this.listadoObjetos.toArray();
				int contadorBusqueda = arbolBusqueda.equals(0) ? this.posicionObjeto
						: this.posicionSG;
				log.info("contadorBusqueda :" + contadorBusqueda);

				if (contadorBusqueda < listaItems.length) {
					CellListaSeleccion obj = (CellListaSeleccion) listaItems[contadorBusqueda];

					if (arbolBusqueda.equals(0)) {
						this.posicionObjeto++;
						this.secItemSelected = obj.getSecuencia();
						Map<String, Object> parametros = new HashMap<String, Object>();
						parametros.put("NODO_BUSCADO", this.secItemSelected);
						parametros.put("ESTADO", null);
						parametros.put("isNumeroDescendientes", "NOT_NULL");

						this.path = (List<Integer>) ParametrizacionFac.getFacade().obtenerListado("",parametros);
						this.path.add(0, 0);

//						TreeFacadePorDemanda.getFacade().printPath(this.path);
//						this.onSeleccionarMaestro(TreeFacadePorDemanda.getFacade()
//								.getTreeItemByPath(this.tree, this.path));

					}

				}

				log.info("this.posicionUnidades: " + this.posicionObjeto);
				log.info("this.posicionSG: " + this.posicionSG);
				log.info("listaItems.length: " + listaItems.length);

				if (this.posicionObjeto == listaItems.length) {
					log.info("entro aquii.....");
					this.posicionObjeto = 0;
				}

			} else {

				this.secItemSelected = secuencia;
				Map<String, Object> parametros = new HashMap<String, Object>();
				parametros.put("NODO_BUSCADO", this.secItemSelected);
				parametros.put("ESTADO", null);
				parametros.put("isNumeroDescendientes", "NOT_NULL");

				this.path = (List<Integer>)  ParametrizacionFac.getFacade().obtenerListado("",parametros);
				this.path.add(0, 0);

//				this.onSeleccionarMaestro(TreeFacadePorDemanda.getFacade()
//						.getTreeItemByPath(this.tree, this.path));

			} 

		} catch (Exception e) {
			e.printStackTrace();
			
		}
	}

}
