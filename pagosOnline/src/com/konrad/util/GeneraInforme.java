package com.konrad.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Map;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;

import org.zkoss.util.media.AMedia;


public class GeneraInforme {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public AMedia generar(String tipoReporte, String informe, Map params,
			Connection con) throws IOException {
		AMedia amedia = null;
		InputStream is = null;
		File fl = null;
		JRExporter exporter;
		try {
			fl = new File(RutaInformes.getRutaInformes() + informe);

			if (!fl.exists())
				throw new RuntimeException("resource for " + informe
						+ " not found.");

			is = new FileInputStream(fl);
			JasperPrint jasperPrint = JasperFillManager.fillReport(is, params,
					ConexionInformes.getConexion());
			ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

			// genera el tipo de informe seleccionado
			if (tipoReporte.equalsIgnoreCase("csv")) {
				exporter = new JRCsvExporter();
				exporter.setParameter(JRExporterParameter.JASPER_PRINT,
						jasperPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
						arrayOutputStream);
				exporter.exportReport();
				arrayOutputStream.close();
				amedia = new AMedia("detalleindicador.csv", "csv", "text/csv",
						arrayOutputStream.toByteArray());
			} else if (tipoReporte.equalsIgnoreCase("pdf")) {
				exporter = new JRPdfExporter();
				exporter.setParameter(JRExporterParameter.JASPER_PRINT,
						jasperPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
						arrayOutputStream);
				exporter.exportReport();

				arrayOutputStream.close();
				amedia = new AMedia("report.pdf", "pdf", "application/pdf",
						arrayOutputStream.toByteArray());
			} else if (tipoReporte.equalsIgnoreCase("html")) {

				exporter = new JRHtmlExporter();
				exporter.setParameter(JRExporterParameter.JASPER_PRINT,
						jasperPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
						arrayOutputStream);
				exporter.exportReport();
				arrayOutputStream.close();
				amedia = new AMedia("report.html", "html", "text/html",
						arrayOutputStream.toByteArray());

			} else if (tipoReporte.equalsIgnoreCase("rtf")) {
				exporter = new JRRtfExporter();
				exporter.setParameter(JRExporterParameter.JASPER_PRINT,
						jasperPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
						arrayOutputStream);
				exporter.exportReport();
				arrayOutputStream.close();
				amedia = new AMedia("report.rtf", "rtf", "application/rtf",
						arrayOutputStream.toByteArray());
			}

		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			if (is != null) {
				is.close();
			}
		}
		return amedia;

	}

}
