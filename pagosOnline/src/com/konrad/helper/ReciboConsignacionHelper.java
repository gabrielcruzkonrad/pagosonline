package com.konrad.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.zkoss.zhtml.Form;
import org.zkoss.zhtml.Input;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Auxhead;
import org.zkoss.zul.Auxheader;
import org.zkoss.zul.Image;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Window;

import com.konrad.domain.Asignatura;
import com.konrad.domain.AuditoriaPago;
import com.konrad.domain.CategoriaMatricula;
import com.konrad.domain.CentroCosto;
import com.konrad.domain.Cliente;
import com.konrad.domain.ConceptoAcademico;
import com.konrad.domain.DetalleReciboConsignacion;
import com.konrad.domain.Estudiante;
import com.konrad.domain.EstudianteSolicitud;
import com.konrad.domain.Facultad;
import com.konrad.domain.Moneda;
import com.konrad.domain.Orden;
import com.konrad.domain.OrdenAcademico;
import com.konrad.domain.PeriodoAcademico;
import com.konrad.domain.PeriodoFacturacion;
import com.konrad.domain.Persona;
import com.konrad.domain.PlanEstudios;
import com.konrad.domain.Poblacion;
import com.konrad.domain.ProgramaAcademico;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.SaldoCartera;
import com.konrad.domain.Secuencias;
import com.konrad.domain.TasaRepresentativaMercado;
import com.konrad.domain.TipoIdentificacion;
import com.konrad.domain.TipoPrograma;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.util.IConstantes;
import com.konrad.util.ItemListaSeleccion;
import com.konrad.util.LlaveNatural;

import NET.webserviceX.www.Currency;
import NET.webserviceX.www.CurrencyConvertorSoapProxy;


public class ReciboConsignacionHelper {

	private static final ReciboConsignacionHelper reciboHelper = new ReciboConsignacionHelper();
	protected static Logger log = Logger.getLogger(ReciboConsignacionHelper.class);
	private Boolean servicioPruebasProduccion;


	private ReciboConsignacionHelper() {
		super();
	}
	
	public static ReciboConsignacionHelper getHelper(){
		return reciboHelper;
	}
	
	public Double getConversion(Currency monedaDe, Currency monedaA ){
		Double monedaConvertida = new Double(0.00);
		try{
            CurrencyConvertorSoapProxy conversorMoneda = new CurrencyConvertorSoapProxy();
            monedaConvertida = conversorMoneda.conversionRate(monedaDe, monedaA);
   
          
            } catch (Exception e){
           	 e.printStackTrace();
           	 
            }

		return monedaConvertida;

	}
	
	public Long getRefVenta(){
		Long refVenta = java.util.Calendar.getInstance().getTimeInMillis();
		return refVenta;	
	}
	
	public String getPersonalEmail(String identificacion){
		String direccionElectronica = "";
		try{
			direccionElectronica = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectCorreoPersonalUsuarioSinu", identificacion);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return direccionElectronica;
	}
	
	
	public List<LlaveNatural> returnListParametrosPayU(ReciboConsignacion recibo){
		List<LlaveNatural> listaLlaves = new ArrayList<LlaveNatural>();
		try{
			if(recibo.getIdentificador()==null){
	    		recibo.setIdentificador(this.getRefVenta());
	    	}
	    	
	    	String direccionElectronica = recibo.getCliente().getPersona().getDireccionElectronica();
	    	
	    	
	    	System.out.println("asunto de la cuestion de enviar post");
	    	
	    	Map<String,String> mapaParametros = new HashMap<String,String>();
	    	
	    	mapaParametros.put("parametro", IConstantes.URL_WEBCHECKOUT_PAYULATAM);
	    	mapaParametros.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
	    	
	    	String url = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
	    	
	    	mapaParametros.remove("parametro");
	    	mapaParametros.put("parametro", IConstantes.MERCHANT_ID_PAYULATAM);
	    	String merchantId = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
	    	
	    	mapaParametros.remove("parametro");
	    	mapaParametros.put("parametro", IConstantes.API_KEY_PAYULATAM);
	    	String apiKey = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
	    	
	    	mapaParametros.remove("parametro");
	    	mapaParametros.put("parametro", IConstantes.API_LOGIN_PAYULATAM);
	    	String apiLogin = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
	    	
	    	mapaParametros.remove("parametro");
	    	mapaParametros.put("parametro", IConstantes.ACCOUNT_ID_PAYULATAM);
	    	String accountId = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
	 		
	    	if (url!=null && !url.equals("")){
	    		if((merchantId!=null && !merchantId.equals("")) 
	    				&& (apiKey!=null && !apiKey.equals(""))
	    				&& (apiLogin!=null && !apiLogin.equals(""))
	    				&& (accountId!=null && !accountId.equals(""))
	    				){
	    					
							Properties appProperties = AppContext.getInstance().getAppProperties();
							String urlConfirmacion = appProperties.getProperty("ap.urlConfirmacion");
							String pruebas = appProperties.getProperty("ap.indicadorPruebasPagosElectronicos");
							String target = appProperties.getProperty("ap.targetPagosElectronicos");
	    			
	    					// agregamos url de inicio
	    					LlaveNatural llave = new LlaveNatural();
	    					llave.setLlave("url");
	    					llave.setValor(url);
	    					listaLlaves.add(llave);
	    					
	    					// agregamos target
	    					llave = new LlaveNatural();
	    					llave.setLlave("target");
	    					llave.setValor(target);
	    					listaLlaves.add(llave);
	    					
	    					
	    					//agregamos merchantId 
	    					llave = new LlaveNatural();
	    					llave.setLlave("merchantId");
	    					llave.setValor(merchantId);
	    					listaLlaves.add(llave);
	    					
	    					
	    					// agregamos referenceCode
	    					llave = new LlaveNatural();
	    					llave.setLlave("referenceCode");
	    					llave.setValor(String.valueOf(recibo.getIdentificador()));
	    					listaLlaves.add(llave);
	    					
	    					//agregamos description
	    					llave = new LlaveNatural();
	    					llave.setLlave("description");
	    					llave.setValor(recibo.getObservaciones().length()>=255?recibo.getObservaciones().substring(0,254):recibo.getObservaciones());
	    					listaLlaves.add(llave);
	    					
	    					//agregamos amount
	    					llave = new LlaveNatural();
	    					llave.setLlave("amount");
	    					llave.setValor(new BigDecimal(recibo.getValorDetalle()).setScale(0,RoundingMode.CEILING).toString());
	    					listaLlaves.add(llave);
	    					
	    					// agregamos tax
	    					llave = new LlaveNatural();
	    					llave.setLlave("tax");
	    					llave.setValor(new BigDecimal(0).setScale(0,RoundingMode.CEILING).toString());
	    					listaLlaves.add(llave);
	    					
	    					// agregamos taxReturnBase
	    					llave = new LlaveNatural();
	    					llave.setLlave("taxReturnBase");
	    					llave.setValor(new BigDecimal(0).setScale(0,RoundingMode.CEILING).toString());
	    					listaLlaves.add(llave);
	    					
	    					// calcular firma
	    					String firmaString= apiKey+"~"+merchantId+"~"+String.valueOf(recibo.getIdentificador())+
	    							"~"+new BigDecimal(recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
	    									recibo.getValorDetalle().doubleValue():
	    										recibo.getTasaRepresentativa().getValor().doubleValue()).setScale(0,RoundingMode.CEILING).toString()+
	    							"~"+recibo.getTasaRepresentativa().getMoneda().getCodigo();
	    					
	    					MessageDigest algoritmo = MessageDigest.getInstance("MD5");
	    					algoritmo.reset();
	    					algoritmo.update(firmaString.getBytes());
	    					byte[] digest = algoritmo.digest();
	    					
	    					StringBuffer hashedpasswd = new StringBuffer();
	    						String hx;
	    						for (int i=0;i<digest.length;i++){
	    							hx = Integer.toHexString(0xFF & digest[i]);
	    							if(hx.length() == 1){hx = "0" + hx;}
	    							hashedpasswd.append(hx);
	    					}
	    					
	    						String signature = hashedpasswd.toString();
	    						System.out.println("firma a enviar: "+signature);
	    						System.out.println("identificador: "+recibo.getIdentificador());
	    						
	    						
	    					// agregamos signature
		    				llave = new LlaveNatural();
		    				llave.setLlave("signature");
		    				llave.setValor(signature);
		    				listaLlaves.add(llave);	
		    				
		    				//agregamos accountId
		    				llave = new LlaveNatural();
		    				llave.setLlave("accountId");
		    				llave.setValor(accountId);
		    				listaLlaves.add(llave);
		    				
		    				//agregamos currency
		    				llave = new LlaveNatural();
		    				llave.setLlave("currency");
		    				llave.setValor(IConstantes.MONEDA_PAGOS_ONLINE_DEFAULT);
		    				listaLlaves.add(llave);
		    				
		    				//agregamos buyerFullName
		    				llave = new LlaveNatural();
		    				llave.setLlave("buyerFullName");
		    				llave.setValor((recibo.getCliente().getPersona().getNombreRazonSocial()
    								+" "+recibo.getCliente().getPersona().getPrimerApellido()
    								+" "+recibo.getCliente().getPersona().getSegundoApellido()).trim());
		    				listaLlaves.add(llave);
		    				
		    				//agregamos buyerEmail
		    				llave = new LlaveNatural();
		    				llave.setLlave("buyerEmail");
		    				llave.setValor(direccionElectronica);
		    				listaLlaves.add(llave);

		    				//agregamos shippingAddress
		    				llave = new LlaveNatural();
		    				llave.setLlave("shippingAddress");
		    				llave.setValor(recibo.getCliente().getPersona()!=null?
    								recibo.getCliente().getPersona().getDireccionResidencia()!=null?
    										recibo.getCliente().getPersona().getDireccionResidencia()
    										:"No registra":"No registra");
		    				listaLlaves.add(llave);
		    				
		    				//agregamos shippingCity
		    				llave = new LlaveNatural();
		    				llave.setLlave("shippingCity");
		    				llave.setValor(appProperties.getProperty("ap.ciudadTransaccion"));
		    				listaLlaves.add(llave);
		    				
		    				//agregamos shippingCountry
		    				llave = new LlaveNatural();
		    				llave.setLlave("shippingCountry");
		    				llave.setValor(appProperties.getProperty("ap.codigoISOPais"));
		    				listaLlaves.add(llave);
		    				
		    				//agregamos telephone
		    				llave = new LlaveNatural();
		    				llave.setLlave("telephone");
		    				llave.setValor(recibo.getCliente().getPersona()!=null?
    								recibo.getCliente().getPersona().getTelefonoResidencia()!=null?
    										recibo.getCliente().getPersona().getTelefonoResidencia()
    										:"No registra":"No registra");
		    				listaLlaves.add(llave);
		    				
		    				//agregamos confirmationUrl
		    				llave = new LlaveNatural();
		    				llave.setLlave("confirmationUrl");
		    				llave.setValor(urlConfirmacion);
		    				listaLlaves.add(llave);
		    				
		    				//agregamos  test
		    				llave = new LlaveNatural();
		    				llave.setLlave("test");
		    				llave.setValor(pruebas);
		    				listaLlaves.add(llave);
	    		}
	    	}
	    	
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}
		
		
		return listaLlaves;
		
	}
	
	
	@SuppressWarnings("unchecked")
	public String onValidarTipoRecibo(ReciboConsignacion recibo){
		String tipoRecibo = null;
		try{
			
			List<DetalleReciboConsignacion> listaDetallesRecibo = 
					(List<DetalleReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("selectListDetalleReciboConsignacionImpreso", recibo);
			
			if(listaDetallesRecibo!= null){
				if(listaDetallesRecibo.size()>0){
					for(DetalleReciboConsignacion detalleRecibo : listaDetallesRecibo){
						
						SaldoCartera saldoCartera = detalleRecibo.getSaldoCartera();
						Orden orden = detalleRecibo.getOrden();
						
						for(String documento : IConstantes.DOCUMENTOS_MATRICULAS_PAYU){
							
							if(saldoCartera!=null){
								if(saldoCartera.getDocumento()!=null){
									if(saldoCartera.getDocumento().equalsIgnoreCase(documento)){
										tipoRecibo = IConstantes.TIPO_RECIBO_MATRICULAS;
									}
								}
							}
							
							if(orden!=null){
								if(orden.getDocumento()!=null){
									if(orden.getDocumento().equalsIgnoreCase(documento)){
										tipoRecibo = IConstantes.TIPO_RECIBO_MATRICULAS;
									}
								}
							}
						}
					}
					
					if (tipoRecibo == null){
					for(DetalleReciboConsignacion detalleRecibo : listaDetallesRecibo){
						
						SaldoCartera saldoCartera = detalleRecibo.getSaldoCartera();
						Orden orden = detalleRecibo.getOrden();
						
						for(String documento : IConstantes.DOCUMENTOS_PUBLICACIONES_PAYU){
							
							if(saldoCartera!=null){
								if(saldoCartera.getDocumento()!=null){
									if(saldoCartera.getDocumento().equalsIgnoreCase(documento)){
										tipoRecibo = IConstantes.TIPO_RECIBO_PUBLICACIONES;
									}
								}
							}
							
							if(orden!=null){
								if(orden.getDocumento()!=null){
									if(orden.getDocumento().equalsIgnoreCase(documento)){
										tipoRecibo = IConstantes.TIPO_RECIBO_PUBLICACIONES;
									}
								}
							}
						}
					}
				}
					
					if (tipoRecibo == null){
						for(DetalleReciboConsignacion detalleRecibo : listaDetallesRecibo){
							
							SaldoCartera saldoCartera = detalleRecibo.getSaldoCartera();
							Orden orden = detalleRecibo.getOrden();
							
							for(String documento : IConstantes.DOCUMENTOS_EVENTOS_PAYU){
								
								if(saldoCartera!=null){
									if(saldoCartera.getDocumento()!=null){
										if(saldoCartera.getDocumento().equalsIgnoreCase(documento)){
											tipoRecibo = IConstantes.TIPO_RECIBO_EVENTOS;
										}
									}
								}

								if(orden!=null){
									if(orden.getDocumento()!=null){
										if(orden.getDocumento().equalsIgnoreCase(documento)){
											tipoRecibo = IConstantes.TIPO_RECIBO_EVENTOS;
										}
									}
								}
							}
						}
					}
					
					if(tipoRecibo == null){
						tipoRecibo = IConstantes.TIPO_RECIBO_MATRICULAS;
					}
					
				} // FIN SI LA LISTA TIENE ITEMS
			} // FIN SI LA LISTA NO ES NULA
			
		}catch(Exception e){
			tipoRecibo = IConstantes.TIPO_RECIBO_MATRICULAS;
			e.printStackTrace();
			log.info(e.getMessage());
		}

		System.out.println("Tipo de recibo --> "+tipoRecibo);
		return tipoRecibo;
	}
	

	public void registrarAuditoriaPayU(ReciboConsignacion recibo, String tipo){
		try{
		
			Map<String,String> mapaParametros = new HashMap<String,String>();
			
			// se definen variables para el control de la moneda
			BigDecimal valor = new BigDecimal(0); 
			String codigoMoneda = new String(IConstantes.PESOS_COLOMBIANOS);
	    	
	    	mapaParametros.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
	    	mapaParametros.put("parametro", IConstantes.MERCHANT_ID_PAYULATAM);
	    	String merchantId = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
	    	
	    	mapaParametros.remove("parametro");
	    	mapaParametros.put("parametro", IConstantes.API_KEY_PAYULATAM);
	    	String apiKey = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
	    	
			// se determina el valor y c�digo de divisa que debe enviarse seg�n el tipo invocado 
			if(tipo.equalsIgnoreCase(IConstantes.TIPO_INVOCACION_DERECHOS_ACADEMICOS)){
				System.out.println("Invocacion derechos academicos");
				com.konrad.util.Moneda monedaDefault = new com.konrad.util.Moneda();
				monedaDefault.setCodigo(IConstantes.PESOS_COLOMBIANOS);
				TasaRepresentativaMercado tasaRepresentativaDefault = new TasaRepresentativaMercado();
				tasaRepresentativaDefault.setMoneda(monedaDefault);
				if(recibo.getTasaRepresentativa()== null || 
						(recibo.getTasaRepresentativa()!= null 
						&& recibo.getTasaRepresentativa().getMoneda()== null) 
						) {
					recibo.setTasaRepresentativa(tasaRepresentativaDefault);
				}
				valor = new BigDecimal( 
						recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
								recibo.getValorDetalle().doubleValue():
									recibo.getTasaRepresentativa().getValor().doubleValue()).setScale(0,RoundingMode.CEILING);
				codigoMoneda = recibo.getTasaRepresentativa().getMoneda().getCodigo();
				System.out.println("Invocacion derechos academicos -> Cod Moneda -> "+codigoMoneda);
				
			}else if (tipo.equalsIgnoreCase(IConstantes.TIPO_INVOCACION_CREDITOS_VIGENTES)){
				
				System.out.println("Invocacion creditos vigentes");
				valor = new BigDecimal(recibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
						recibo.getValorVencido().doubleValue():
						recibo.getTasaRepresentativaVencido().getValor().doubleValue()).setScale(0,RoundingMode.CEILING);
				codigoMoneda = recibo.getTasaRepresentativaVencido().getMoneda().getCodigo();
				
				System.out.println("Invocacion creditos vigentes -> Cod Moneda -> "+codigoMoneda);
				
			}else if (tipo.equalsIgnoreCase(IConstantes.TIPO_INVOCACION_CREDITOS_VENCIDOS)){
				
				System.out.println("Invocacion creditos vencidos ");
				
				valor = new BigDecimal(recibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
						recibo.getValorVencido().doubleValue():
						recibo.getTasaRepresentativaVencido().getValor().doubleValue()).setScale(0,RoundingMode.CEILING);
				codigoMoneda= recibo.getTasaRepresentativaVencido().getMoneda().getCodigo();
				
				System.out.println("Invocacion creditos vencidos -> Cod Moneda -> "+codigoMoneda);
				
			}else if (tipo.equalsIgnoreCase(IConstantes.TIPO_INVOCACION_ABONOS)){
				
				System.out.println("Invocacion abonos");
				valor = new BigDecimal(recibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
						recibo.getValorVencido().doubleValue():
						recibo.getTasaRepresentativaVencido().getValor().doubleValue()).setScale(0,RoundingMode.CEILING);
				
				codigoMoneda=recibo.getTasaRepresentativa().getMoneda().getCodigo();
				
				System.out.println("Invocacion abonos -> Cod Moneda -> "+codigoMoneda);
				
			}else if (tipo.equalsIgnoreCase(IConstantes.TIPO_INVOCACION_PAGO_TOTAL)){
				
				System.out.println("Invocacion pago total");
				valor = new BigDecimal(recibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
						recibo.getValorVencido().doubleValue():
						recibo.getTasaRepresentativaVencido().getValor().doubleValue()).setScale(0,RoundingMode.CEILING);
				codigoMoneda = recibo.getTasaRepresentativa().getMoneda().getCodigo();
				
				System.out.println("Invocacion pago total -> Cod Moneda -> "+codigoMoneda);
				
			}else if (tipo.equalsIgnoreCase(IConstantes.TIPO_INVOCACION_OTROS_PAGOS)){
				
				System.out.println("Invocacion otros pagos");
				
				valor = new BigDecimal(recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
						recibo.getValorDetalle().doubleValue():
						recibo.getTasaRepresentativa().getValor().doubleValue()).setScale(0,RoundingMode.CEILING);
				codigoMoneda = recibo.getTasaRepresentativa().getMoneda().getCodigo();
				
				System.out.println("Invocacion otros pagos -> Cod Moneda -> "+codigoMoneda);
				
			}else if (tipo.equalsIgnoreCase(IConstantes.TIPO_INVOCACION_RECIBOS_SALDOS)){
				
				System.out.println("Invocacion recibos por saldos");
				valor = new BigDecimal(recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
						recibo.getValorDetalle().doubleValue():
						recibo.getTasaRepresentativa().getValor().doubleValue()).setScale(0,RoundingMode.CEILING);
				codigoMoneda = recibo.getTasaRepresentativa().getMoneda().getCodigo();
				System.out.println("Invocacion recibos por saldos -> Cod Moneda -> "+codigoMoneda);
				
			}else if (tipo.equalsIgnoreCase(IConstantes.TIPO_INVOCACION_CARTERA)){
				System.out.println("Invocacion cartera ");
				valor = new BigDecimal(recibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
						recibo.getValorDetalle().doubleValue():
						recibo.getValorVencido().doubleValue()).setScale(0,RoundingMode.CEILING);
				codigoMoneda= recibo.getTasaRepresentativaVencido().getMoneda().getCodigo();
				System.out.println("Invocacion cartera -> CodigoMoneda --> "+codigoMoneda);
				
			}else if (tipo.equalsIgnoreCase(IConstantes.TIPO_INVOCACION_CHEQUES_DEVUELTOS)){
				
				System.out.println("Invocacion cheques devueltos ");
				valor = new BigDecimal(recibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
							recibo.getValorVencido().doubleValue():
								recibo.getTasaRepresentativaVencido().getValor().doubleValue()).setScale(0,RoundingMode.CEILING);
				codigoMoneda= recibo.getTasaRepresentativaVencido().getMoneda().getCodigo();
				System.out.println("Invocacion cheques devueltos -> codigoMoneda -->"+codigoMoneda);
				
			} else{
				
				System.out.println("Invocacion no determinada");
				valor = new BigDecimal(
						recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
								recibo.getValorDetalle().doubleValue():
									recibo.getTasaRepresentativa().getValor().doubleValue()).setScale(0,RoundingMode.CEILING);
				codigoMoneda = recibo.getTasaRepresentativa().getMoneda().getCodigo();
				
				System.out.println("Invocacion no determinada  ->  codigoMoneda --> "+codigoMoneda);
			}
	    	
	    	
	    if (apiKey !=null){	
	    	AuditoriaPago auditoria = new AuditoriaPago();
		
	    	BigDecimal monto = valor;
		
	    	Locale locale = new Locale("en", "US");
	    	NumberFormat numberFormat = NumberFormat.getNumberInstance(locale);
	    	DecimalFormat formato = (DecimalFormat)numberFormat;
	    	formato.applyPattern(IConstantes.FORMATO_NUMERO_WS);
		
	    	auditoria.setCodTienda(merchantId);
	    	auditoria.setCodAlumno(recibo.getCliente().getCliente().toString());
	    	auditoria.setNombreAlumno(
				(recibo.getCliente().getPersona().getNombreRazonSocial())+(recibo.getCliente().getPersona().getPrimerApellido()!=null?
						" "+recibo.getCliente().getPersona().getPrimerApellido():"")+(recibo.getCliente().getPersona().getSegundoApellido()!=null?" "+
								recibo.getCliente().getPersona().getSegundoApellido():"")
	    			);	
	    	Moneda moneda = new Moneda();
	    	moneda.setCodigo(codigoMoneda);
		
	    	auditoria.setFechaCreacion(new Date());
	    	auditoria.setFechaOperacion(new Date());
	    	auditoria.setReciboConsignacion(recibo);
	    	auditoria.setNumPedido(recibo.getIdentificador().toString());
	    	auditoria.setxId(recibo.getReciboConsignacion().toString());
	    	auditoria.setMoneda(moneda);
	    	auditoria.setMora(new String("N"));
	    	auditoria.setSaldo(formato.format(monto.doubleValue()));
	    	auditoria.setImporteLocal(formato.format(monto.doubleValue()));
	    	auditoria.setImporteExtr(formato.format(0d));
	    	auditoria.setEticket(recibo.getReciboConsignacion().toString());
	    	auditoria.setNumDocumento(recibo.getReciboConsignacion().toString());
	    	auditoria.setCuota((recibo.getNumeroFila()!=null?recibo.getNumeroFila().toString():"1"));
		
	    	String firma_cadena= apiKey+"~"+merchantId+"~"+String.valueOf(recibo.getIdentificador())+
					"~"+monto.setScale(0,RoundingMode.CEILING).toString()+
					"~"+codigoMoneda;
		
	    	MessageDigest alg = MessageDigest.getInstance("MD5");
	    	alg.reset();
	    	alg.update(firma_cadena.getBytes());
	    	
	    	byte[] digest = alg.digest();
	    	StringBuffer hashedpasswd = new StringBuffer();
	    	String hx;
		
	    	for (int i=0;i<digest.length;i++){
	    		hx = Integer.toHexString(0xFF & digest[i]);
	    		if(hx.length() == 1){
	    			hx = "0" + hx;
	    		}
					hashedpasswd.append(hx);
	    	}
		
	    	String firmacreada = hashedpasswd.toString();
	    	auditoria.setGlosa(firmacreada);
		
			ParametrizacionFac.getFacade().ejecutarProcedimiento("insertAuditoriaPago", auditoria);
			
	    }
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void sendPostPayU(ReciboConsignacion recibo, String tipo){
		try{
		
    		recibo.setIdentificador(
    				Long.valueOf(
    							String.valueOf(recibo.getReciboConsignacion())+
    							String.valueOf(this.getRefVenta()).substring(0,
    									String.valueOf(this.getRefVenta()).length()<=
    										(15-String.valueOf(recibo.getReciboConsignacion()).length())?
    												String.valueOf(this.getRefVenta()).length():
    													(15-String.valueOf(recibo.getReciboConsignacion()).length())
    													)
    							
    							)
    						);
    	
    	
    	
    	String direccionElectronica = recibo.getCliente().getPersona().getDireccionElectronica();
    	String accountId = new String("");
    	
    	System.out.println("asunto de la cuestion de enviar post");
    	
    	Map<String,String> mapaParametros = new HashMap<String,String>();
    	
    	mapaParametros.put("parametro", IConstantes.URL_WEBCHECKOUT_PAYULATAM);
    	mapaParametros.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
    	
    	String url = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
    	
    	mapaParametros.remove("parametro");
    	mapaParametros.put("parametro", IConstantes.MERCHANT_ID_PAYULATAM);
    	String merchantId = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
    	
    	mapaParametros.remove("parametro");
    	mapaParametros.put("parametro", IConstantes.API_KEY_PAYULATAM);
    	String apiKey = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
    	
    	mapaParametros.remove("parametro");
    	mapaParametros.put("parametro", IConstantes.API_LOGIN_PAYULATAM);
    	String apiLogin = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
    	
    	
    	mapaParametros.remove("parametro");
    	mapaParametros.put("parametro", IConstantes.ACCOUNT_ID_PAYULATAM);
    	String accountIdMatriculas = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
 		
    	mapaParametros.remove("parametro");
    	mapaParametros.put("parametro", IConstantes.ACCOUNT_ID_PAYULATAM_PUBLICACIONES);
    	String accountIdPublicaciones = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
    	
    	mapaParametros.remove("parametro");
    	mapaParametros.put("parametro", IConstantes.ACCOUNT_ID_PAYULATAM_EVENTOS);
    	String accountIdEventos = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
    	
    	String tipoRecibo = this.onValidarTipoRecibo(recibo);
    	
    	if (tipoRecibo.equalsIgnoreCase(IConstantes.TIPO_RECIBO_EVENTOS)){
    			if(accountIdEventos!= null){
    				accountId = accountIdEventos;
    			}		
    	} else if(tipoRecibo.equalsIgnoreCase(IConstantes.TIPO_RECIBO_PUBLICACIONES)){
    				if(accountIdPublicaciones!= null){
        				accountId = accountIdPublicaciones;
        			}		
    	} else if(tipoRecibo.equalsIgnoreCase(IConstantes.TIPO_RECIBO_MATRICULAS)){ 
    			if(accountIdMatriculas!= null){
        				accountId = accountIdMatriculas;
    			}
    	}
    	
    	if(accountId==null || accountId.equals("")){
    		accountId = accountIdMatriculas;
    	}
    	
    	
    	System.out.println("Account id --> "+accountId);
    	
    	if (url!=null && !url.equals("")){
    		if((merchantId!=null && !merchantId.equals("")) 
    				&& (apiKey!=null && !apiKey.equals(""))
    				&& (apiLogin!=null && !apiLogin.equals(""))
    				&& (accountId!=null && !accountId.equals(""))
    				){
    			
    					Properties appProperties = AppContext.getInstance().getAppProperties();
    					String urlConfirmacion = appProperties.getProperty("ap.urlConfirmacion");
    					String urlRespuesta = appProperties.getProperty("ap.urlRespuesta");
    					String pruebas = appProperties.getProperty("ap.indicadorPruebasPagosElectronicos");
    					String monedaExtranjera = appProperties.getProperty("ap.enviarTransaccionMonedaExtranjera");
    					Form form = new Form();
    					form.setDynamicProperty("action", url);        
    					form.setDynamicProperty("method", "post");
    					form.setDynamicProperty("target", "_self");
    					// se definen variables para el control de la moneda
    					BigDecimal valor = new BigDecimal(0); 
    					String codigoMoneda = new String(IConstantes.PESOS_COLOMBIANOS);
    					String serviceCode =new String(IConstantes.CODIGO_SERVICIO);
    					String periodo = new String("");
    					
    					if(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA)){
    						serviceCode = IConstantes.CODIGO_SERVICIO_BANCOLOMBIA;
    					}	
    					
    					if(recibo.getPeriodo()!=null){
    						if(recibo.getPeriodo().getPeriodo()!=null){
    							if(recibo.getPeriodo().getPeriodo().getPeriodo()!=null){
    								periodo = recibo.getPeriodo().getPeriodo().getPeriodo();
    							}
    						}
    					}
        	
    					if(monedaExtranjera==null){
    						monedaExtranjera = "0";
    					}
    					
    					Window win = (Window) Executions.createComponents(IConstantes.PAGES_PATH
    							+ "formularioPagosOnlinePost.zul", null, null);
        
    					form.setParent(win);
    					form.setPage(win.getPage());
        	
    					Input inpMerchantId = new Input();
    					inpMerchantId.setDynamicProperty("type", "hidden");
    					inpMerchantId.setDynamicProperty("name", "merchantId");
    					inpMerchantId.setValue(merchantId);
    					inpMerchantId.setParent(form);
    					
    					Input inpReferenceCode = new Input();
    					inpReferenceCode.setDynamicProperty("type", "hidden");
    					inpReferenceCode.setDynamicProperty("name", "referenceCode");
    					inpReferenceCode.setValue(String.valueOf(recibo.getIdentificador()));
    					inpReferenceCode.setParent(form);
    					
    					Input inpServiceCode = new Input();
    					inpServiceCode.setDynamicProperty("type", "hidden");
    					inpServiceCode.setDynamicProperty("name", "pseServiceCode");
    					inpServiceCode.setValue(serviceCode);
    					inpServiceCode.setParent(form);
    					
    					Input inpPayerDocument = new Input();
    					inpPayerDocument.setDynamicProperty("type", "hidden");
    					inpPayerDocument.setDynamicProperty("name", "payerDocument");
    					inpPayerDocument.setValue(recibo.getCliente().getPersona().getIdentificacion());
    					inpPayerDocument.setParent(form);
    					
    					Input inpExtra1 = new Input();
    					inpExtra1.setDynamicProperty("type", "hidden");
    					inpExtra1.setDynamicProperty("name", "extra1");
    					inpExtra1.setValue(String.valueOf(recibo.getCliente().getCliente()));
    					inpExtra1.setParent(form);
    					
    					Input inpExtra2 = new Input();
    					inpExtra2.setDynamicProperty("type", "hidden");
    					inpExtra2.setDynamicProperty("name", "extra2");
    					inpExtra2.setValue(String.valueOf(recibo.getReciboConsignacion()));
    					inpExtra2.setParent(form);
    					
    					Input inpExtra3 = new Input();
    					inpExtra3.setDynamicProperty("type", "hidden");
    					inpExtra3.setDynamicProperty("name", "extra3");
    					inpExtra3.setValue(periodo);
    					inpExtra3.setParent(form);
    					
    					Input inpDescription = new Input();
    					inpDescription.setDynamicProperty("type", "hidden");
    					inpDescription.setDynamicProperty("name", "description");
    					inpDescription.setValue(recibo.getObservaciones().length()>=255?recibo.getObservaciones().substring(0,254):recibo.getObservaciones());
    					inpDescription.setParent(form);
    					
    					// se determina el valor y c�digo de divisa que debe enviarse seg�n el tipo invocado 
    					if(tipo.equalsIgnoreCase(IConstantes.TIPO_INVOCACION_DERECHOS_ACADEMICOS)){
    						
    						if(monedaExtranjera.equalsIgnoreCase("1")){

    							valor = new BigDecimal(
    								recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
    										recibo.getValorDetalle().doubleValue():
    											recibo.getTasaRepresentativa().getValor().doubleValue()).setScale(0,RoundingMode.CEILING);
    								codigoMoneda = recibo.getTasaRepresentativa().getMoneda().getCodigo();
    						} else{
    							valor = new BigDecimal(recibo.getValorDetalle().doubleValue()).setScale(0,RoundingMode.CEILING);
    							codigoMoneda = IConstantes.PESOS_COLOMBIANOS;		
    							
    						} 
    					}else if (tipo.equalsIgnoreCase(IConstantes.TIPO_INVOCACION_CREDITOS_VIGENTES)){
    						
    						if(monedaExtranjera.equalsIgnoreCase("1")){
    						
    							valor = new BigDecimal(recibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
									recibo.getValorVencido().doubleValue():
									recibo.getTasaRepresentativaVencido().getValor().doubleValue()).setScale(0,RoundingMode.CEILING);
    							codigoMoneda = recibo.getTasaRepresentativaVencido().getMoneda().getCodigo();
    						} else{
    							valor = new BigDecimal(recibo.getValorVencido().doubleValue()).setScale(0,RoundingMode.CEILING);
    							codigoMoneda = IConstantes.PESOS_COLOMBIANOS;	
    						}
    						
    					}else if (tipo.equalsIgnoreCase(IConstantes.TIPO_INVOCACION_CREDITOS_VENCIDOS)){
    						if(monedaExtranjera.equalsIgnoreCase("1")){
    							valor = new BigDecimal(recibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
									recibo.getValorVencido().doubleValue():
									recibo.getTasaRepresentativaVencido().getValor().doubleValue()).setScale(0,RoundingMode.CEILING);
    							codigoMoneda= recibo.getTasaRepresentativaVencido().getMoneda().getCodigo();
    						} else {
    							valor = new BigDecimal(recibo.getValorVencido().doubleValue()).setScale(0,RoundingMode.CEILING);
    							codigoMoneda = IConstantes.PESOS_COLOMBIANOS;	
    						}
    						
    					}else if (tipo.equalsIgnoreCase(IConstantes.TIPO_INVOCACION_ABONOS)){
    						if(monedaExtranjera.equalsIgnoreCase("1")){
    							valor = new BigDecimal(recibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
									recibo.getValorVencido().doubleValue():
									recibo.getTasaRepresentativaVencido().getValor().doubleValue()).setScale(0,RoundingMode.CEILING);
    						
    							codigoMoneda=recibo.getTasaRepresentativa().getMoneda().getCodigo();
    						} else{
    							valor = new BigDecimal(recibo.getValorVencido().doubleValue()).setScale(0,RoundingMode.CEILING);
    							codigoMoneda = IConstantes.PESOS_COLOMBIANOS;
    						}
    						
    					}else if (tipo.equalsIgnoreCase(IConstantes.TIPO_INVOCACION_PAGO_TOTAL)){
    						if(monedaExtranjera.equalsIgnoreCase("1")){
    							valor = new BigDecimal(recibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
									recibo.getValorVencido().doubleValue():
									recibo.getTasaRepresentativaVencido().getValor().doubleValue()).setScale(0,RoundingMode.CEILING);
    							codigoMoneda = recibo.getTasaRepresentativa().getMoneda().getCodigo();
    						} else{
    							valor = new BigDecimal(recibo.getValorVencido().doubleValue()).setScale(0,RoundingMode.CEILING);
    							codigoMoneda = IConstantes.PESOS_COLOMBIANOS;
    						}
    						
    						
    					}else if (tipo.equalsIgnoreCase(IConstantes.TIPO_INVOCACION_OTROS_PAGOS)){
    						if(monedaExtranjera.equalsIgnoreCase("1")){
    							valor = new BigDecimal(recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
									recibo.getValorDetalle().doubleValue():
									recibo.getTasaRepresentativa().getValor().doubleValue()).setScale(0,RoundingMode.CEILING);
    							codigoMoneda = recibo.getTasaRepresentativa().getMoneda().getCodigo();
    						} else{
    							valor = new BigDecimal(recibo.getValorDetalle().doubleValue()).setScale(0,RoundingMode.CEILING);
    							codigoMoneda = IConstantes.PESOS_COLOMBIANOS;
    						}
    						
    						
    					}else if (tipo.equalsIgnoreCase(IConstantes.TIPO_INVOCACION_RECIBOS_SALDOS)){
    						if(monedaExtranjera.equalsIgnoreCase("1")){
    							valor = new BigDecimal(recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
									recibo.getValorDetalle().doubleValue():
									recibo.getTasaRepresentativa().getValor().doubleValue()).setScale(0,RoundingMode.CEILING);
    							codigoMoneda = recibo.getTasaRepresentativa().getMoneda().getCodigo();
    						} else{
    							valor = new BigDecimal(recibo.getValorDetalle().doubleValue()).setScale(0,RoundingMode.CEILING);
    							codigoMoneda = IConstantes.PESOS_COLOMBIANOS;
    						}	
    						
    					}else if (tipo.equalsIgnoreCase(IConstantes.TIPO_INVOCACION_CARTERA)){
    						if(monedaExtranjera.equalsIgnoreCase("1")){
    							valor = new BigDecimal(recibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
									recibo.getValorDetalle().doubleValue():
									recibo.getValorVencido().doubleValue()).setScale(0,RoundingMode.CEILING);
    							codigoMoneda= recibo.getTasaRepresentativaVencido().getMoneda().getCodigo();
    						} else{
    							valor = new BigDecimal(recibo.getValorDetalle().doubleValue()).setScale(0,RoundingMode.CEILING);
    							codigoMoneda = IConstantes.PESOS_COLOMBIANOS;
    						}
    						
    					}else if (tipo.equalsIgnoreCase(IConstantes.TIPO_INVOCACION_CHEQUES_DEVUELTOS)){
    						if(monedaExtranjera.equalsIgnoreCase("1")){
    							valor = new BigDecimal(recibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
										recibo.getValorVencido().doubleValue():
											recibo.getTasaRepresentativaVencido().getValor().doubleValue()).setScale(0,RoundingMode.CEILING);
    							codigoMoneda= recibo.getTasaRepresentativaVencido().getMoneda().getCodigo();
    						} else {
    							valor = new BigDecimal(recibo.getValorVencido().doubleValue()).setScale(0,RoundingMode.CEILING);
    							codigoMoneda = IConstantes.PESOS_COLOMBIANOS;
    						}
    						
    					} else{
    						if(monedaExtranjera.equalsIgnoreCase("1")){
    							valor = new BigDecimal(
    								recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
    										recibo.getValorDetalle().doubleValue():
    											recibo.getTasaRepresentativa().getValor().doubleValue()).setScale(0,RoundingMode.CEILING);
    							codigoMoneda = recibo.getTasaRepresentativa().getMoneda().getCodigo();
    						}else{
    							valor = new BigDecimal(recibo.getValorDetalle().doubleValue()).setScale(0,RoundingMode.CEILING);
    							codigoMoneda = IConstantes.PESOS_COLOMBIANOS;
    						}	
    					}
    					
    					
    					
    					Input inpAmount = new Input();
    					inpAmount.setDynamicProperty("type", "hidden");
    					inpAmount.setDynamicProperty("name", "amount");
    					inpAmount.setValue(valor.setScale(0,RoundingMode.CEILING).toString());
    					inpAmount.setParent(form);
    					
    					Input inpTax = new Input();
    					inpTax.setDynamicProperty("type", "hidden");
    					inpTax.setDynamicProperty("name", "tax");
    					inpTax.setValue(new BigDecimal(0).setScale(0,RoundingMode.CEILING).toString());
    					inpTax.setParent(form);
    					
    					Input inpTaxReturnBase = new Input();
    					inpTaxReturnBase.setDynamicProperty("type", "hidden");
    					inpTaxReturnBase.setDynamicProperty("name", "taxReturnBase");
    					inpTaxReturnBase.setValue(new BigDecimal(0).setScale(0,RoundingMode.CEILING).toString());
    					inpTaxReturnBase.setParent(form);
    					
    					System.out.println("pasa montos");
    					
    					String firmaString= apiKey+"~"+merchantId+"~"+String.valueOf(recibo.getIdentificador())+
    							"~"+valor.toString()+
    							"~"+codigoMoneda;
    					
    					MessageDigest algoritmo = MessageDigest.getInstance("MD5");
    					algoritmo.reset();
    					algoritmo.update(firmaString.getBytes());
    					byte[] digest = algoritmo.digest();
    					
    					StringBuffer hashedpasswd = new StringBuffer();
    						String hx;
    						for (int i=0;i<digest.length;i++){
    							hx = Integer.toHexString(0xFF & digest[i]);
    							if(hx.length() == 1){hx = "0" + hx;}
    							hashedpasswd.append(hx);
    					}
    					
    						String signature = hashedpasswd.toString();
    						System.out.println("firma a enviar: "+signature);
    						System.out.println("identificador: "+recibo.getIdentificador());
    						
    						Input InpSignature = new Input();
    						InpSignature.setDynamicProperty("type", "hidden");
    						InpSignature.setDynamicProperty("name", "signature");
    						InpSignature.setValue(signature);
    						InpSignature.setParent(form);	
    					
    						Input inpAccountId = new Input();
    						inpAccountId.setDynamicProperty("type", "hidden");
    						inpAccountId.setDynamicProperty("name", "accountId");
    						inpAccountId.setValue(accountId);
    						inpAccountId.setParent(form);	
    						
    						Input inpCurrency = new Input();
    						inpCurrency.setDynamicProperty("type", "hidden");
    						inpCurrency.setDynamicProperty("name", "currency");
    						inpCurrency.setValue(codigoMoneda);
    						inpCurrency.setParent(form);	
    						
    						Input inpBuyerFullName = new Input();
    						inpBuyerFullName.setDynamicProperty("type", "hidden");
    						inpBuyerFullName.setDynamicProperty("name", "buyerFullName");
    						inpBuyerFullName.setValue((recibo.getCliente().getPersona().getNombreRazonSocial()
    								+" "+recibo.getCliente().getPersona().getPrimerApellido()
    								+" "+recibo.getCliente().getPersona().getSegundoApellido()).trim());
    						inpBuyerFullName.setParent(form);	
    						
    						Input inpBuyerEmail = new Input();
    						inpBuyerEmail.setDynamicProperty("type", "hidden");
    						inpBuyerEmail.setDynamicProperty("name", "buyerEmail");
    						inpBuyerEmail.setValue(direccionElectronica);
    						inpBuyerEmail.setParent(form);
    						
    						if(recibo.getCliente().getPersona().getDireccionResidencia()!=null){
    							if(!recibo.getCliente().getPersona().getDireccionResidencia().equalsIgnoreCase("")){
    								Input inpShippingAddress = new Input(); 
    								inpShippingAddress.setDynamicProperty("type", "hidden");
    								inpShippingAddress.setDynamicProperty("name", "shippingAddress");
    								inpShippingAddress.setValue(recibo.getCliente().getPersona()!=null?
    								recibo.getCliente().getPersona().getDireccionResidencia()!=null?
    										recibo.getCliente().getPersona().getDireccionResidencia()
    										:"No registra":"No registra");
    								inpShippingAddress.setParent(form);
    							}	
    						}
    						
    						Input inpShippingCity = new Input();
    						inpShippingCity.setDynamicProperty("type", "hidden");
    						inpShippingCity.setDynamicProperty("name", "shippingCity");
    						inpShippingCity.setValue(appProperties.getProperty("ap.ciudadTransaccion"));
    						inpShippingCity.setParent(form);
    						
    						Input inpShippingCountry = new Input();
    						inpShippingCountry.setDynamicProperty("type", "hidden");
    						inpShippingCountry.setDynamicProperty("name", "shippingCountry");
    						inpShippingCountry.setValue(appProperties.getProperty("ap.codigoISOPais"));
    						inpShippingCountry.setParent(form);
    						
    						if(recibo.getCliente().getPersona().getTelefonoResidencia()!=null){
    							if(!recibo.getCliente().getPersona().getTelefonoResidencia().equalsIgnoreCase("")){
    							Input inpTelephone = new Input();
    							inpTelephone.setDynamicProperty("type", "hidden");
    							inpTelephone.setDynamicProperty("name", "telephone");
    							inpTelephone.setValue(recibo.getCliente().getPersona()!=null?
    								recibo.getCliente().getPersona().getTelefonoResidencia()!=null?
    										recibo.getCliente().getPersona().getTelefonoResidencia()
    										:"No registra":"No registra");
    							inpTelephone.setParent(form);
    							}
    						}
    						
    						Input inpConfirmationUrl = new Input();
    						inpConfirmationUrl.setDynamicProperty("type", "hidden");
    						inpConfirmationUrl.setDynamicProperty("name", "confirmationUrl");
    						inpConfirmationUrl.setValue(urlConfirmacion);
    						inpConfirmationUrl.setParent(form);
    						
    						Input inpResponsenUrl = new Input();
    						inpResponsenUrl.setDynamicProperty("type", "hidden");
    						inpResponsenUrl.setDynamicProperty("name", "responseUrl");
    						inpResponsenUrl.setValue(urlRespuesta);
    						inpResponsenUrl.setParent(form);
    						
    						Input inpTest = new Input();
    						inpTest.setDynamicProperty("type", "hidden");
    						inpTest.setDynamicProperty("name", "test");
    						inpTest.setValue(pruebas);
    						inpTest.setParent(form);
    						
    						System.out.println("haciendo el submit form");
    						
    						Clients.submitForm(form);
    		}
    	}
    	
    
    
		}catch(Exception e){
			e.printStackTrace();
		}

	}
	
	
	@SuppressWarnings("rawtypes")
	public void setListBasicGeneric (Listbox lista, List listado, String molde, String labelLista ){
		Listhead encabezadoLista = new Listhead();
		Auxhead cabezaAuxiliar = new Auxhead();
		Auxheader itemCabAuxiliar = new Auxheader();
		itemCabAuxiliar.setLabel(labelLista);
		cabezaAuxiliar.appendChild(itemCabAuxiliar);
		Listheader itemEncLista = new Listheader();
		itemEncLista.setLabel("C�digo");
		encabezadoLista.appendChild(itemEncLista);
		lista.appendChild(cabezaAuxiliar);
		lista.appendChild(encabezadoLista);
		Listitem itemLista;
		Listcell celdaItem;
		if (listado!= null){
			if (!listado.isEmpty()){
				if(listado.size()>0){
					for(Object object : listado){
						celdaItem = new Listcell();
						itemLista = new Listitem();
						celdaItem.setValue(((ItemListaSeleccion)object).getValor());
						celdaItem.setLabel(((ItemListaSeleccion)object).getEtiqueta());
						itemLista.appendChild(celdaItem);
						lista.appendChild(itemLista);
					}
				}
			}
		}
		
		lista.setMold(molde);
		
		if(listado.size()>0){
		lista.setSelectedIndex(0);
		}
		
	}
	
	
	@SuppressWarnings("rawtypes")
	public void setListBasic (Listbox lista, List listado, String molde, String labelLista ){
		Listhead encabezadoLista = new Listhead();
		Auxhead cabezaAuxiliar = new Auxhead();
		Auxheader itemCabAuxiliar = new Auxheader();
		itemCabAuxiliar.setLabel(labelLista);
		cabezaAuxiliar.appendChild(itemCabAuxiliar);
		Listheader itemEncLista = new Listheader();
		itemEncLista.setLabel("C�digo");
		encabezadoLista.appendChild(itemEncLista);
		lista.appendChild(cabezaAuxiliar);
		lista.appendChild(encabezadoLista);
		Listitem itemLista;
		Listcell celdaItem;
		if (listado!= null){
			if (!listado.isEmpty()){
				if(listado.size()>0){
					for(Object object : listado){
						celdaItem = new Listcell();
						itemLista = new Listitem();
						celdaItem.setValue(((ItemListaSeleccion)object).getValor());
						celdaItem.setLabel(((ItemListaSeleccion)object).getEtiqueta());
						if (((ItemListaSeleccion)object).getValor().equals(IConstantes.DOLARES_AMERICANOS)){
							celdaItem.setImage(IConstantes.DOLARES_AMERICANOS_IMAGE);
						} else if (((ItemListaSeleccion)object).getValor().equals(IConstantes.EUROS)){
							celdaItem.setImage(IConstantes.EUROS_IMAGE);
						} else{
							celdaItem.setImage(IConstantes.PESOS_COLOMBIANOS_IMAGE);
						}
							
						
						itemLista.appendChild(celdaItem);
						lista.appendChild(itemLista);
					}
				}
			}
		}
		
		lista.setMold(molde);
		
		if(listado.size()>0){
		lista.setSelectedIndex(0);
		}
		
	}
	
	public List<ItemListaSeleccion> fillListCurrency(){
		List<ItemListaSeleccion> listaMonedasBasica = new ArrayList<ItemListaSeleccion>();
		
		ItemListaSeleccion item = new ItemListaSeleccion();
		item.setValor(IConstantes.PESOS_COLOMBIANOS);
		item.setEtiqueta("Pesos Colombianos");
		listaMonedasBasica.add(item);
		
		
		item = new ItemListaSeleccion();
		item.setValor(IConstantes.DOLARES_AMERICANOS);
		item.setEtiqueta("D�lares EEUU");
		listaMonedasBasica.add(item);
		
		item = new ItemListaSeleccion();
		item.setValor(IConstantes.EUROS);
		item.setEtiqueta("Euros");
		listaMonedasBasica.add(item);
		
		return listaMonedasBasica;
		
	}
	
	public void onInicializarListaCurrency(Listbox lista ){
		List<ItemListaSeleccion> listaCurrency= this.fillListCurrency();
		this.setListBasic(lista, listaCurrency, "select", "Monedas B�sicas");
		Properties appProperties = AppContext.getInstance().getAppProperties();
		String transaccionMonedaExtranjera =appProperties.getProperty("ap.enviarTransaccionMonedaExtranjera");
		
		if (transaccionMonedaExtranjera==null){
			transaccionMonedaExtranjera="0";
		}
		
		if(!transaccionMonedaExtranjera.equalsIgnoreCase("1")){
			lista.setDisabled(true);
		}

	}
	

	public void onRetirarElementosListaCurrency(Listbox lista, Double trmEuro, Double trmDolar){
		if (lista.getItemCount()>1){
			
			if(trmDolar ==null){
				lista.removeItemAt(1);
			}
			else{
				if(trmDolar <= 0d){
					lista.removeItemAt(1);
				}
			}
		
			if(lista.getItemCount()>2){	
			    if(trmEuro ==null){
				lista.removeItemAt(2);
			    }else{
				if(trmEuro <= 0d){
				    lista.removeItemAt(2);
				}
			    }
			}
		}
	}
	
	public Map<String,Object> setConversionHistorica(String formatoFecha, String monedaOrigen, String monedaDestino, Date fecha ){
		Map<String,Object> mapaParametros = new HashMap <String,Object>();
	
		try {
			String fechaBuscada =new SimpleDateFormat(formatoFecha).format(fecha);
			System.out.println("fecha Convertida: "+fechaBuscada);
			Double factor =  new Double(0.00);
			if (monedaOrigen.equals(IConstantes.DOLARES_AMERICANOS)){
				factor = getConversion(IConstantes.RUTA_CONVERSION_DOLAR,"yyyy-MM-dd" , fecha);
			} else{
				factor = getConversion(IConstantes.RUTA_CONVERSION_EURO, "yyyy-MM-dd",IConstantes.EUROS, IConstantes.DOLARES_AMERICANOS, fecha);
			}
			if(factor==null){
				factor=0.00;
			}
			
			mapaParametros.put("MONEDA_ORIGEN",monedaOrigen);
			mapaParametros.put("MONEDA_DESTINO",monedaDestino);
			mapaParametros.put("FECHA",fechaBuscada);
			mapaParametros.put("FACTOR", factor.doubleValue());
			System.out.println("Factor antes de enviar: "+mapaParametros.get("FACTOR"));
			
			ParametrizacionFac.getFacade().ejecutarProcedimiento("setCurrencyFecha", mapaParametros);
			
			System.out.println("Moneda Origen: "+mapaParametros.get("MONEDA_ORIGEN"));
			System.out.println("Moneda Destino: "+mapaParametros.get("MONEDA_DESTINO"));
			System.out.println("Fecha: "+mapaParametros.get("FECHA"));
			System.out.println("Factor: "+mapaParametros.get("FACTOR"));
			
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mapaParametros;
		
	}
	
	public Double getConversion(String urlArchivo, String formatoFecha, String monedaDe, String monedaA, Date fecha){
		Double trm = new Double(0L);
		TasaRepresentativaMercado tasaBuscada = new TasaRepresentativaMercado();
		TasaRepresentativaMercado tasaBuscar = new TasaRepresentativaMercado();
		tasaBuscar.setValor(0d);
		try {
			tasaBuscada.setFecha(new SimpleDateFormat(formatoFecha).parse(new SimpleDateFormat(formatoFecha).format(fecha)));
			URL url = new URL(urlArchivo);
			File archivoTrm = new File("historiaEuro.xml");
			org.apache.commons.io.FileUtils.copyURLToFile(url,archivoTrm);
			// esto para cuando el archivo es hist�rico
			Calendar calendarDate = Calendar.getInstance();  
		    calendarDate.setTime(tasaBuscada.getFecha()); 
			
		    if((new SimpleDateFormat("dd").format(tasaBuscada.getFecha()).equals("01") && 
		    		new SimpleDateFormat("MM").format(tasaBuscada.getFecha()).equals("05")) ||
		    		(new SimpleDateFormat("dd").format(tasaBuscada.getFecha()).equals("01") && 
				    		new SimpleDateFormat("MM").format(tasaBuscada.getFecha()).equals("01")) ||
				    		(new SimpleDateFormat("dd").format(tasaBuscada.getFecha()).equals("25") && 
						    		new SimpleDateFormat("MM").format(tasaBuscada.getFecha()).equals("12"))
		    		){
		    	
		    	 calendarDate.add(Calendar.MINUTE, (60 * 24 * -1));  
		    	 tasaBuscada.setFecha(calendarDate.getTime());
		    	 
		    } else if ((new SimpleDateFormat("dd").format(tasaBuscada.getFecha()).equals("26") && 
		    		new SimpleDateFormat("MM").format(tasaBuscada.getFecha()).equals("12"))){
		    		
		    		calendarDate.add(Calendar.MINUTE, (60 * 24 * -2));  
		    		tasaBuscada.setFecha(calendarDate.getTime());
		    	
		    }
		    
			
			if(new SimpleDateFormat("EEEE" , new Locale("en", "US")).format(tasaBuscada.getFecha()).equalsIgnoreCase("SATURDAY")){
			       calendarDate.add(Calendar.MINUTE, (60 * 24 * -1));  
			       tasaBuscada.setFecha(calendarDate.getTime());
				
			} else 
				if(new SimpleDateFormat("EEEE",new Locale("en", "US")).format(tasaBuscada.getFecha()).equalsIgnoreCase("SUNDAY")) {
					calendarDate.add(Calendar.MINUTE, (60 * 24 * -2));  
				       tasaBuscada.setFecha(calendarDate.getTime());
				
			}
			
			if(archivoTrm.exists()){
				
				 SAXBuilder builder = new SAXBuilder();
				 Document document = (Document) builder.build( archivoTrm );
				 
				 Element rootNode = document.getRootElement();
				 List<Element> list = rootNode.getChildren();
				 Iterator<Element> iterador = list.iterator(); 
				 while(iterador.hasNext()){
					 Element cubo = iterador.next();
					 List<Element> listaConversion = cubo.getChildren();
					 Iterator<Element> iteradorNodos = listaConversion.iterator();
					 
					 while(iteradorNodos.hasNext()){
						 Element cuboConversionFecha = iteradorNodos.next();
						
						 if(cuboConversionFecha.getName().equalsIgnoreCase("Cube")){
							 if(new SimpleDateFormat(formatoFecha).format(tasaBuscada.getFecha()).equals(cuboConversionFecha.getAttributeValue("time"))){
								 log.info(cuboConversionFecha.getAttributeValue("time"));
								 
								 List<Element> listaCurrency = cuboConversionFecha.getChildren();
								 Iterator<Element> iteratorCurrency = listaCurrency.iterator();
								 
								 while(iteratorCurrency.hasNext()){
									 Element tasaNode = iteratorCurrency.next();
									 if(tasaNode.getAttributeValue("currency").equals(monedaA)){
										 tasaBuscar.setFecha(tasaBuscada.getFecha());
										 tasaBuscar.setValor(new Double(tasaNode.getAttributeValue("rate")));	
										 trm = tasaBuscar.getValor();
										 log.info(tasaBuscar.getValor());
									 }
								 } 
							 }
						 }
					 }
				 }
			} else{
				log.info("No existe informaci�n del archivo");
			}
			
			// Si definitivamente no encuentra informaci�n en el archivo (es viernes santo o lunes de pascua)
			
			
			if (trm <= 0d && tasaBuscar.getValor() <= 0d){
				
				Calendar calendarAux = Calendar.getInstance();
				calendarAux.setTime(tasaBuscada.getFecha());
				calendarAux.add(Calendar.MINUTE, (60 * 24 * -1));
				tasaBuscada.setFecha(calendarAux.getTime());
				
				 this.getConversion(urlArchivo, formatoFecha, monedaDe, monedaA, tasaBuscada.getFecha());
				
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return trm;
	}
	
	
	
	public Double getConversionCSV(String urlArchivo, String formatoFecha, Date fecha){
		Double trm = new Double(0L);
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		File archivoTcrm;
		
	 
		try {
	 
			URL url = new URL(urlArchivo);
			log.info("url "+urlArchivo);
			archivoTcrm = new File("historia.csv");
			org.apache.commons.io.FileUtils.copyURLToFile(url,archivoTcrm);	
			String charset = "UTF-16";
			String cadenaTrm = "";
			if(archivoTcrm.exists()){
			br = new BufferedReader(new InputStreamReader (new FileInputStream(archivoTcrm), charset));
			int contadorFilas = 0;
			System.out.println(" se ha encontrado el archivo");
			while ((line = br.readLine()) != null) {
			if(contadorFilas>1){
				// use comma as separator
				
				String[] splitter = line.split(cvsSplitBy);
				
				if(splitter.length>1){
					if(new SimpleDateFormat(formatoFecha).format(new SimpleDateFormat(formatoFecha).parse(splitter[1].replace("\"", "")))
							.equals(new SimpleDateFormat(formatoFecha).format(fecha))){
						System.out.println("columna valor:  "+splitter[2].trim());
						if(line.contains("\"")){
							cadenaTrm = line.substring(line.indexOf("\""), line.indexOf("\"", line.indexOf("\"")+1));
							cadenaTrm = cadenaTrm.replace("\"","").replace(" ", "").replace(",",".").trim();
							System.out.println("columna valor 2:  "+cadenaTrm.trim());
							trm = new Double(cadenaTrm);
						} 
						else{
							trm = new Double(splitter[2].trim());
						}
					}
				}	

				}
				contadorFilas++;
			}
	 
			}else{
				System.out.println("No se ha encontrado el archivo");
			}
	 
		} catch(Exception e){
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	 
		System.out.println("trm hallada en CSV: "+trm);
		return trm;
		
	}
	
	
public Double getConversionXLS(String urlArchivo, String formatoFecha, Date fecha){
	Double trm =0d;
	
	try {
		
		//Workbook workbook = null;
		TasaRepresentativaMercado tasaBuscada = new TasaRepresentativaMercado();
		TasaRepresentativaMercado tasaBuscar = new TasaRepresentativaMercado();
		tasaBuscada.setFecha(new SimpleDateFormat(formatoFecha).parse(new SimpleDateFormat(formatoFecha).format(fecha)));
		// Create a new trust manager that trust all certificates
//		TrustManager[] trustAllCerts = new TrustManager[]{
//		    new X509TrustManager() {
//		        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//		            return null;
//		        }
//		        public void checkClientTrusted(
//		            java.security.cert.X509Certificate[] certs, String authType) {
//		        }
//		        public void checkServerTrusted(
//		            java.security.cert.X509Certificate[] certs, String authType) {
//		        }
//		    }
//		};

		// Activate the new trust manager
//		    SSLContext sc = SSLContext.getInstance("SSL");
//		    sc.init(null, trustAllCerts, new java.security.SecureRandom());
//		    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		
		URL url = new URL(urlArchivo);
		log.info("url "+urlArchivo);
		File archivoTcrm = new File("historia.xls");
		org.apache.commons.io.FileUtils.copyURLToFile(url,archivoTcrm);
	
		if(archivoTcrm.exists()){
			
			log.info("El archivo ha sido encontrado "+archivoTcrm.getName());
			// para JXL
//			workbook = Workbook.getWorkbook(archivoTcrm);
//			Sheet sheet = workbook.getSheet(0);
//			Integer  numeroFilas =sheet.getRows();
//			Integer numeroColumnas = sheet.getColumns();
			//para POI
			HSSFWorkbook workbookPOI = null;
			FileInputStream input = new FileInputStream(archivoTcrm);
			workbookPOI = new HSSFWorkbook(input);
			HSSFSheet sheetPOI = workbookPOI.getSheetAt(0);
			Iterator<Row> iteradorFilas = sheetPOI.iterator();
			while(iteradorFilas.hasNext()){
				Row fila = iteradorFilas.next();
				
				Iterator<Cell> iteradorCeldas = fila.cellIterator();
				while(iteradorCeldas.hasNext()){
					Cell celdaPOI = iteradorCeldas.next();
					
					if (HSSFDateUtil.isCellDateFormatted(celdaPOI)){
						tasaBuscar.setFecha(
								new SimpleDateFormat(formatoFecha).parse(
										new SimpleDateFormat(formatoFecha).format(celdaPOI.getDateCellValue())));
					}
					
					
					if(tasaBuscar!= null){
						if(tasaBuscar.getFecha()!= null){
								if (new SimpleDateFormat(formatoFecha).format(tasaBuscar.getFecha()).equals(new SimpleDateFormat(formatoFecha).format(tasaBuscada.getFecha()))){
										log.info("encuentra tasa:"+fila.getCell(2).getStringCellValue());
										String miTasa = new String();
										miTasa = fila.getCell(2).getStringCellValue().replace(".","");
										miTasa = miTasa.replace(",", ".");
										BigDecimal numero = new BigDecimal(miTasa);
										log.info(miTasa);
										tasaBuscada.setValor(new Double(numero.doubleValue()));
							}
						}
					}
				} // Fin iterador Celdas
			} // fin iterador Filas
			
//			for(int i=0; i<numeroFilas; i++ ){
//			for(int j=0; j<numeroColumnas; j++){
//				jxl.Cell celda = sheet.getCell(j,i);
//				if (celda.getType().equals(CellType.DATE)){
//					tasaBuscar.setFecha(new SimpleDateFormat(formatoFecha).parse(celda.getContents()));
//					
//				}
//			}
//			if(tasaBuscar!= null){
//				if(tasaBuscar.getFecha()!= null){
//					
//						if (new SimpleDateFormat(formatoFecha).format(tasaBuscar.getFecha()).equals(new SimpleDateFormat(formatoFecha).format(tasaBuscada.getFecha()))){
//								log.info("encuentra tasa: "+sheet.getCell(2,i).getContents());
//								String miTasa = new String();
//								miTasa = sheet.getCell(2,i).getContents().replace(".","");
//								miTasa = miTasa.replace(",", ".");
//								BigDecimal numero = new BigDecimal(miTasa);
//								log.info(miTasa);
//								tasaBuscada.setValor(new Double(numero.doubleValue()));
//					}
//				}
//			}
//
//		}
			log.info("informacion de tasa encontrada: "+tasaBuscada.getValor());
			
			trm = tasaBuscada.getValor();
			//workbook.close();
			//input.close();
		}// Fin existe archivo
		else{
			log.info("No existe informaci�n del archivo");
		}	
		
	}

	 catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}  catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
	
	return trm;
	
}
	
	public Double getConversion(String urlArchivo, String formatoFecha, Date fecha){

			Double trm = 0d;
			trm = getConversionCSV(urlArchivo,formatoFecha,fecha);	
		
		return trm;
	}

	public Boolean getServicioPruebasProduccion() {
		return servicioPruebasProduccion;
	}

	public void setServicioPruebasProduccion(Boolean servicioPruebasProduccion) {
		this.servicioPruebasProduccion = servicioPruebasProduccion;
	}

	public Connection getConnectionSqlServer(String servidor, String usuario, String contrasena, String database){
		Connection connection = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String url = "jdbc:sqlserver://"+servidor+";databaseName="+database+
					";user="+usuario+";password="+contrasena+";";
				connection= DriverManager.getConnection(url);
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		 catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
		return connection;
	}
	
	public void onSetImageCurrency(Listbox lista, Image imagen){
		Listitem item = lista.getSelectedItem();
		
		if(((Listcell)item.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)){
			imagen.setSrc(IConstantes.PESOS_COLOMBIANOS_IMAGE);
			imagen.setTooltiptext(IConstantes.PESOS_COLOMBIANOS);
		}else if(((Listcell)item.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)){
			imagen.setSrc(IConstantes.DOLARES_AMERICANOS_IMAGE);
			imagen.setTooltiptext(IConstantes.DOLARES_AMERICANOS);
		}else {
			imagen.setSrc(IConstantes.EUROS_IMAGE);
			imagen.setTooltiptext(IConstantes.EUROS);
		}
		
	}

	public void setDatosClienteAcademico(Map<String, Object> parametros){
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
	     String consulta="update  registro.estudiante set dir_corr=?, tel_corr=?, cel_est=?, eotro=?, teloficina=? ";
	      consulta+=" where nro_carn=?";
	      
	      
	     try{
	    	 if(conn != null){
	    		 statement = conn.prepareStatement(consulta);
	    		 statement.setString(1, parametros.get("DIRECCION_RESIDENCIA").toString());
	    		 statement.setString(2, parametros.get("TELEFONO_RESIDENCIA").toString());
	    		 statement.setString(3, parametros.get("CELULAR").toString());
	    		 statement.setString(4, parametros.get("CORREO_ALTERNO").toString());
	    		 statement.setString(5, parametros.get("TELEFONO_OFICINA").toString());
	    		 statement.setString(6, parametros.get("CLIENTE").toString());
	    		 statement.execute();
	    	 } 
	    	 
	     }catch(Exception e){
	    	 e.printStackTrace();
	    	 
	     } finally{
	        	if(conn!= null){
	        		try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
	        	}
	        	if(statement!= null){
	        		try {
						statement.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
	        	}
	    	 
	     }
	      
		
	}
	
	
	public int getEstadoAcademico(Map<String, Object> parametros){
		int miEstadoAcademico =0;
    	ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		try{
        String v_sql ="select count(*) registros ";
        v_sql+="from registro.estudiante est ";        
        v_sql+="where est.nro_carn=? ";
        v_sql+="and  est.estado > 0 ";
        v_sql+="and  est.pyz > 0 ";
        		
		statement = conn.prepareStatement(v_sql);
		statement.setString(1,parametros.get("CLIENTE").toString());
		rs = statement.executeQuery();
			if(conn != null){
				if(rs!= null){
					while(rs.next()){
						miEstadoAcademico = rs.getShort("registros");
		        		}
        		}
			}
		} catch(Exception e){
			e.printStackTrace();
		}finally{
        	if(rs!= null){
        		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(conn!= null){
        		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(statement!= null){
        		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	
        }
		return miEstadoAcademico;
	}

	
	@SuppressWarnings("resource")
	public Long getSecuenciaObjeto(String objeto){
		Long secuencia = 0L;
		ResultSet rs = null;
		PreparedStatement statement = null;
		Secuencias secuencias = new Secuencias();
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		try{
			
			String consulta = "SELECT sig_sec as siguienteSecuencia, incr as incremento, nombre as nombre FROM registro.secuencias WHERE UPPER(nombre) =?";
			statement = conn.prepareStatement(consulta);
			statement.setString(1,objeto);
			rs = statement.executeQuery();
			if(conn != null){
				if(rs!= null){
					while(rs.next()){
						secuencias.setNombre(rs.getString("nombre"));
						secuencias.setSiguienteSecuencia(rs.getLong("siguienteSecuencia"));
						secuencias.setIncremento(rs.getLong("incremento"));
						secuencia = secuencias.getSiguienteSecuencia();
		        		}
        		}
				
				if (secuencias!= null){
					if(secuencias.getNombre()!=null && !secuencias.getNombre().equalsIgnoreCase("")){
						consulta = "UPDATE registro.secuencias SET sig_sec = sig_sec+incr WHERE nombre = ?";
						statement = conn.prepareStatement(consulta);
						statement.setString(1,objeto);
						statement.execute();
					}
				}
			}

		}catch(Exception e){
			log.info(e.getMessage());
			e.printStackTrace();
		}finally{
        	if(rs!= null){
        		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(conn!= null){
        		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(statement!= null){
        		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	
        }
		
		return secuencia; 
	}
	
	
	public ConceptoAcademico getConceptoAcademico(String idCategoriaMatricula){
		ConceptoAcademico conceptoAcademico = new ConceptoAcademico();
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		try{
			String consulta ="SELECT conc.id_conc as id_concepto, conc.nom_conc as nombre_concepto, conc.valor as valor_concepto, "+
							" conc.catgmat as id_categoria, conc.usa as usa from registro.conceptos conc where conc.catgmat = ? and conc.usa = 'S'";
			statement = conn.prepareStatement(consulta);
			statement.setString(1,idCategoriaMatricula);
			rs = statement.executeQuery();
			if(conn != null){
				if(rs!= null){
					while(rs.next()){
						CategoriaMatricula categoriaMatricula = new CategoriaMatricula();
						categoriaMatricula.setIdCategoria(rs.getLong("id_categoria"));
						conceptoAcademico.setIdConcepto(rs.getLong("id_concepto"));
						conceptoAcademico.setNombreConcepto(rs.getString("nombre_concepto"));
						conceptoAcademico.setValor(rs.getDouble("valor_concepto"));
						conceptoAcademico.setCategoriaMatricula(categoriaMatricula);
						conceptoAcademico.setUsa(rs.getString("usa"));
						
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}finally{
        	if(rs!= null){
        		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(conn!= null){
        		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(statement!= null){
        		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	
        }
		return conceptoAcademico;
	}
	
	public PlanEstudios getPlanEstudiosAcademico(String idPrograma){
		PlanEstudios planEstudios = new PlanEstudios();
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		try{
			String consulta ="select plane.id_plan as idPlan, plane.id_prog as idPrograma, plane.cod_plan as codigoPlan,"+ 
					" plane.des_plan as descripcionPlan, plane.act as activo from registro.plan_estud plane where plane.id_prog = ?";
			statement = conn.prepareStatement(consulta);
			statement.setString(1,idPrograma);
			rs = statement.executeQuery();
			if(conn != null){
				if(rs!= null){
					while(rs.next()){
						ProgramaAcademico programa = new ProgramaAcademico();
						programa.setIdPrograma(rs.getString("idPrograma"));
						planEstudios.setIdPlan(rs.getLong("idPlan"));
						planEstudios.setCodigoPlan(rs.getString("codigoPlan"));
						planEstudios.setDescripcionPlan(rs.getString("descripcionPlan"));
						planEstudios.setActivo(rs.getString("activo"));
						planEstudios.setProgramaAcademico(programa);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}finally{
        	if(rs!= null){
        		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(conn!= null){
        		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(statement!= null){
        		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	
        }
		
		return planEstudios;
	}
	
	
	
	public List<PlanEstudios> getListPlanEstudiosAcademico(String idPrograma){
		List<PlanEstudios> listaPlanEstudios = new ArrayList<PlanEstudios>();
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		try{
			String consulta ="select plane.id_plan as idPlan, plane.id_prog as idPrograma, plane.cod_plan as codigoPlan,"+ 
					" plane.des_plan as descripcionPlan, plane.act as activo  from registro.plan_estud plane where plane.id_prog = ?";
			statement = conn.prepareStatement(consulta);
			statement.setString(1,idPrograma);
			rs = statement.executeQuery();
			if(conn != null){
				if(rs!= null){
					while(rs.next()){
						PlanEstudios plan  = new PlanEstudios();
						ProgramaAcademico programa = new ProgramaAcademico();
						programa.setIdPrograma(rs.getString("idPrograma"));
						plan.setIdPlan(rs.getLong("idPlan"));
						plan.setCodigoPlan(rs.getString("codigoPlan"));
						plan.setDescripcionPlan(rs.getString("descripcionPlan"));
						plan.setActivo(rs.getString("activo"));
						plan.setProgramaAcademico(programa);
						listaPlanEstudios.add(plan);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}finally{
        	if(rs!= null){
        		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(conn!= null){
        		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(statement!= null){
        		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	
        }
		
		return listaPlanEstudios;
	}
	
	
	public void actualizarEstudianteEducacionContinuadaNroCarn(Estudiante estudiante){
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		String consulta="update registro.estudiante set"+
				" nro_carn = ?, email =?, eotro = ?, cel_est=?, dir_corr=?, tel_corr=?, per_ing =?, catmat=?, estado=?" +
				" where nro_carn = ?";
		try{
			statement = conn.prepareStatement(consulta);
			statement.setString(1, String.valueOf(estudiante.getNumeroCarne()));
			statement.setString(2, estudiante.getEmail());
			statement.setString(3, estudiante.getEmailOtro());
			statement.setString(4, estudiante.getCelularEstudiante());
			statement.setString(5, estudiante.getDireccionCorrespondencia());
			statement.setString(6, estudiante.getTelefonoCorrespondencia());
			statement.setString(7, estudiante.getPeriodoIngreso().getPeriodoAcademico());
			statement.setString(8, estudiante.getCategoriaMatricula().getIdCategoria().toString());
			statement.setString(9, estudiante.getEstado().toString());
			statement.setString(10, estudiante.getNumeroCarne().toString());
			statement.execute();
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}finally{
        	if(rs!= null){
        		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(conn!= null){
        		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(statement!= null){
        		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        }
	}
	
	public void actualizarEstudianteEducacionContinuadaId(Estudiante estudiante){
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		String consulta="update registro.estudiante set"+
				" nro_carn=?, email=?, eotro=?, cel_est=?, dir_corr=?, tel_corr=?, per_ing=?, catmat=?, estado=? " +
				" where id_est = ?";
		log.info(consulta);
		
		try{
			statement = conn.prepareStatement(consulta);
			statement.setString(1, String.valueOf(estudiante.getNumeroCarne()));
			statement.setString(2, estudiante.getEmail());
			statement.setString(3, estudiante.getEmailOtro());
			statement.setString(4, estudiante.getCelularEstudiante());
			statement.setString(5, estudiante.getDireccionCorrespondencia());
			statement.setString(6, estudiante.getTelefonoCorrespondencia());
			statement.setString(7, estudiante.getPeriodoIngreso().getPeriodoAcademico());
			statement.setString(8, estudiante.getCategoriaMatricula().getIdCategoria().toString());
			statement.setString(9, estudiante.getEstado().toString());
			statement.setLong(10, estudiante.getIdEstudiante());
			statement.execute();
			
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}finally{
        	if(rs!= null){
        		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(conn!= null){
        		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(statement!= null){
        		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        }
	}
	
	public void crearEstudianteAcademico(Estudiante estudiante){
		log.info("[CrearEstudianteAcademico]");
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		try{
			String consulta="insert into registro.estudiante("+
					"id_est, nro_carn, docest, nom_est, ape_est, email, eotro, cel_est, sexo, tipdoc, dir_corr, tel_corr, fec_nac, idprg, " +
					"per_ing, id_plan, catmat, estado, pyz, clanum, semnv, tpprg, fec_insc"+
					") values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			statement = conn.prepareStatement(consulta);
			statement.setLong(1, estudiante.getIdEstudiante());
			statement.setLong(2, estudiante.getNumeroCarne());
			statement.setString(3, estudiante.getDocumentoEstudiante());
			statement.setString(4, estudiante.getNombreEstudiante());
			statement.setString(5, estudiante.getApellidosEstudiante());
			statement.setString(6, estudiante.getEmail());
			statement.setString(7, estudiante.getEmailOtro());
			statement.setString(8, estudiante.getCelularEstudiante());
			statement.setString(9, estudiante.getSexo());
			statement.setString(10, estudiante.getTipoDocumento().getTipoIdentificacion());
			statement.setString(11, estudiante.getDireccionCorrespondencia());
			statement.setString(12, estudiante.getTelefonoCorrespondencia());
			statement.setDate(13, new java.sql.Date(estudiante.getFechaNacimiento().getTime()));
			statement.setLong(14, Long.valueOf(estudiante.getProgramaAcademico().getIdPrograma()));
			statement.setString(15, estudiante.getPeriodoIngreso().getPeriodoAcademico());
			statement.setLong(16, estudiante.getPlanEstudios().getIdPlan());
			statement.setLong(17, estudiante.getCategoriaMatricula().getIdCategoria());
			statement.setInt(18, estudiante.getEstado());
			statement.setInt(19, estudiante.getPazSalvo());
			statement.setString(20, estudiante.getClaveNumero());
			statement.setInt(21, estudiante.getSemestreNivel());
			statement.setString(22, estudiante.getProgramaAcademico().getTipoPrograma().getIdTipoPrograma());
			statement.setDate(23, new java.sql.Date(new Date().getTime()));
			statement.execute();
			
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}finally{
        	if(rs!= null){
        		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(conn!= null){
        		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(statement!= null){
        		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	
        }
	}
	
	
	public void crearOrdenAcademico(OrdenAcademico orden){
		log.info("[CrearOrdenAcademico]");
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		try{
			String consulta="insert into registro.ordenes("+
					"id_ord, id_est, num_ord, per_acad, fec_lim, fextr, fexte, val_tot, lug_pago, val_pag, id_ord_pago  " +
					") values (?,?,?,?,?,?,?,?,?,?,?)";
			statement = conn.prepareStatement(consulta);
			statement.setLong(1, orden.getIdOrden());
			statement.setLong(2, orden.getEstudiante().getIdEstudiante());
			statement.setString(3, orden.getIdOrden().toString());
			statement.setString(4, orden.getPeriodoAcademico().getPeriodoAcademico());
			statement.setDate(5, new java.sql.Date(new Date().getTime()));
			statement.setDate(6, new java.sql.Date(new Date().getTime()));
			statement.setDate(7, new java.sql.Date(new Date().getTime()));
			statement.setDouble(8, orden.getEstudiante().getConceptoAcademico().getValor());
			statement.setString(9, IConstantes.LUGAR_PAGO_ORDENES_DEFECTO);
			statement.setDouble(10, orden.getEstudiante().getConceptoAcademico().getValor());
			statement.setInt(11, IConstantes.ORDEN_PAGO_DEFECTO_ACADEMICO);
			statement.execute();
			
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}finally{
        	if(rs!= null){
        		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(conn!= null){
        		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(statement!= null){
        		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	
        }
	}
	
	public void crearEstadoSolicitudAcademico(EstudianteSolicitud solicitud){
		log.info("[CrearEstadoSolicitudAcademico]");
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		try{
			String consulta="insert into registro.estsolic("+
					"id_estad, id_est, estado, fecestad, periodo, idprg, nom_usu, observ " +
					") values (?,?,?,?,?,?,?,?)";
			statement = conn.prepareStatement(consulta);
			statement.setLong(1, solicitud.getIdEstudianteSolicitud());
			statement.setLong(2, solicitud.getEstudiante().getIdEstudiante());
			statement.setInt(3,solicitud.getEstado());
			statement.setDate(4, new java.sql.Date(new Date().getTime()));
			statement.setString(5, solicitud.getPeriodoAcademico().getPeriodoAcademico());
			statement.setString(6, solicitud.getProgramaAcademico().getIdPrograma());
			statement.setString(7, solicitud.getUsuario());
			statement.setString(8, IConstantes.OBSERVACIONES_SOLICITUD_DEFECTO+" Periodo: "+solicitud.getPeriodoAcademico().getPeriodoAcademico()+
					" Usuario: "+solicitud.getUsuario()+" Fecha: "+new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"));
			statement.execute();
			
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}finally{
        	if(rs!= null){
        		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(conn!= null){
        		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(statement!= null){
        		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	
        }
	}
	
	
	public ProgramaAcademico getDatosProgramaAcademico(String idPrograma){
		log.info("[getDatosProgramaAcademico]");
		ProgramaAcademico programa = new ProgramaAcademico();
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		try{
			String consulta ="Select prog.id_prog as idPrograma, prog.nom_prog as nombrePrograma, "+
					 		" prog.tip_prog as tipoPrograma, "+
							" fac.id_fac as idFacultad, fac.cod_fac as codigoFacultad, fac.nom_fac as nombreFacultad, " +
							" prog.centro_costo as centroCosto, prog.periodo as periodo, "+
							" prog.Descuento as descuento, prog.Porcentaje as porcentaje, "+ 
							" prog.cup_prog as cupoPrograma "+
							" from registro.programas prog " +
							" inner join registro.facultad fac " +
							" on fac.id_fac = prog.id_fac "+
							" where prog.id_prog = ?";
			statement = conn.prepareStatement(consulta);
			statement.setString(1,idPrograma);
			rs = statement.executeQuery();
			if(conn != null){
				if(rs!= null){
					while(rs.next()){
						CentroCosto centroCosto = new CentroCosto();
						Facultad facultad = new Facultad();
						TipoPrograma tipoPrograma = new TipoPrograma();
						facultad.setIdFacultad(new Long(rs.getString("idFacultad")));
						facultad.setNombreFacultad(rs.getString("nombreFacultad"));
						facultad.setCodigoFacultad(rs.getString("codigoFacultad"));
						centroCosto.setCentroCosto(rs.getString("centroCosto"));
						programa.setIdPrograma(rs.getString("idPrograma"));
						programa.setNombrePrograma(rs.getString("nombrePrograma"));
						programa.setPeriodo(rs.getString("periodo"));
						programa.setDescuento(rs.getString("descuento"));
						programa.setPorcentaje(rs.getString("porcentaje"));
						programa.setCupoPrograma(rs.getInt("cupoPrograma"));
						tipoPrograma.setIdTipoPrograma(rs.getString("tipoPrograma"));
						programa.setTipoPrograma(tipoPrograma);
						programa.setCentroCosto(centroCosto);
						programa.setFacultad(facultad);
		        	}
        		}
			}
			
		}catch(Exception e){
			log.info(e.getMessage());
			e.printStackTrace();
		}finally{
        	if(rs!= null){
        		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(conn!= null){
        		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(statement!= null){
        		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	
        }
		
		return programa;
	}
	
	
	public String getEstadoAcademicoEstudiante(Map <String, Object> mapaRequest){
		String estado = "INACTIVO";
		
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
			IConstantes.SERVIDOR_ACADEMICO, 
			IConstantes.USUARIO_ACADEMICO, 
			IConstantes.CONTRASENA_ACADEMICO, 
			IConstantes.DATABASE_ACADEMICO);
		try{
			String v_sql;
	        v_sql ="select count(*) registros ";
	        v_sql+="from registro.estudiante est ";        
	        v_sql+="where est.nro_carn=? ";
	        v_sql+="and  est.estado > 0 ";
	        v_sql+="and  est.pyz > 0 ";
	        
	        statement = conn.prepareStatement(v_sql);
        	statement.setString(1,mapaRequest.get("CLIENTE").toString());
        	rs = statement.executeQuery();
       	 	if(conn != null){
            	if(rs!= null){
            		while(rs.next()){
            			if(!rs.getString("registros").equalsIgnoreCase("0")){
            				estado = "ACTIVO";
            			}
            		}
            	}
            }
			
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}{
        	if(rs!= null){
        		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(conn!= null){
        		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(statement!= null){
        		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	
        }
		
		return estado;
		
	}
	
	public List<Asignatura> getAsignaturasAcademico(Map<String, Object> mapaRequest){
		List<Asignatura> listaAsignaturas = new ArrayList<Asignatura>();
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
			IConstantes.SERVIDOR_ACADEMICO, 
			IConstantes.USUARIO_ACADEMICO, 
			IConstantes.CONTRASENA_ACADEMICO, 
			IConstantes.DATABASE_ACADEMICO);
		try{
			
			String v_sql;
			v_sql ="select asig.cod_asig,upper(asig.nom_asig) asignatura,grp.num_grupo grupo, ";
			v_sql+="isnull(convert(varchar,rgs.nota1),'No Disponible') notauno, ";
			v_sql+="isnull(convert(varchar,rgs.nota2),'No Disponible') notados, ";
			v_sql+="isnull(convert(varchar,rgs.nota),'No Disponible') nota, ";
			v_sql+="isnull(convert(varchar,rgs.ofi),'No Disponible') notaoficial, ";
			v_sql+="cla.descl as estado,convert(int,asig.hor_sem) intensidad, convert(int,asig.creditos) creditos ";
			v_sql+="from registro.estudiante est,  ";
			v_sql+="registro.registro rgs, ";
			v_sql+="registro.periodos per, ";
			v_sql+="registro.grupo grp, ";      
			v_sql+="registro.asignatura asig, ";
			v_sql+="registro.defclaves cla  ";
			v_sql+="where est.nro_carn=?  ";
			v_sql+="and rgs.id_est= est.id_est  ";
			v_sql+="and  per.act!='0'  ";
			v_sql+="and per.per_acad=rgs.per_acad  ";
			v_sql+="and  rgs.id_grupo=grp.id_grupo  ";
			v_sql+="and asig.id_asig=grp.id_asig ";
			v_sql+="and cla.idcl=rgs.clave ";
			v_sql+="order by  asignatura ";
			
			statement = conn.prepareStatement(v_sql);
        	statement.setString(1,mapaRequest.get("CLIENTE").toString());
        	rs = statement.executeQuery();
       	 	if(conn != null){
            	if(rs!= null){
            		while(rs.next()){
            			
            		}
            	}
            }
		} catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}finally{
        	if(rs!= null){
        		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(conn!= null){
        		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(statement!= null){
        		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	
        }
		return listaAsignaturas;
	}
	
	
	public Map<String, Object> getDatosAcademicosEstudiante(Map<String, Object> mapaRequest){
		
		Map <String, Object> mapaResult = new HashMap<String,Object>();
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
			IConstantes.SERVIDOR_ACADEMICO, 
			IConstantes.USUARIO_ACADEMICO, 
			IConstantes.CONTRASENA_ACADEMICO, 
			IConstantes.DATABASE_ACADEMICO);
		try{
			
			String v_sql;
			v_sql ="select est.ape_est+' '+est.nom_est as nombres, ";
			v_sql+=" est.nro_carn codigo, ";
			v_sql+="  est.semnv as semestre, ";
			v_sql+=" upper(lisest.dest) as  estadoestudiante, "; 
			v_sql+=" upper(prg.nom_prog) programa, ";
			v_sql+=" est.carmax as cargamaxima,"; 
			v_sql+=" (case when  est.tipdoc='T' then 'Tarjeta de Identidad'";
			v_sql+="      when  est.tipdoc='C' then 'Cedula'";
			v_sql+="      when  est.tipdoc='P' then 'Pasaporte'";
			v_sql+="     else               est.tipdoc            end) as tipo,";
			v_sql+=" est.docest as identificacion,";
			v_sql+=" SUBSTRING (est.dir_corr,1,80) as direccion,";
			v_sql+=" SUBSTRING (est.tel_corr,1,80) as telefono,";
			v_sql+=" SUBSTRING (est.eotro,1,80) as email,";
			v_sql+=" est.cel_est as celular ";
			v_sql+=" from registro.estudiante est, "; 
			v_sql+=" registro.programas prg, ";
			v_sql+=" registro.lisestados lisest ";
			v_sql+=" where est.nro_carn=?";
			v_sql+=" and  prg.id_prog=est.idprg";
			v_sql+=" and lisest.nest=est.estado";
			
			statement = conn.prepareStatement(v_sql);
        	statement.setString(1,mapaRequest.get("CLIENTE").toString());
        	rs = statement.executeQuery();
       	 	if(conn != null){
            	if(rs!= null){
            		while(rs.next()){
            			
            			mapaResult.put("NOMBRES", rs.getString("nombres"));
            			mapaResult.put("CLIENTE", rs.getString("codigo"));
            			mapaResult.put("SEMESTRE", rs.getString("semestre"));
            			mapaResult.put("ESTADO_ACADEMICO", rs.getString("estadoEstudiante"));
            			mapaResult.put("PROGRAMA", rs.getString("programa"));
            			mapaResult.put("CARGA_MAXIMA", rs.getString("cargamaxima"));
            			mapaResult.put("TIPO_IDENTIFICACION", rs.getString("tipo"));
            			mapaResult.put("IDENTIFICACION", rs.getString("identificacion"));
            			mapaResult.put("DIRECCION", rs.getString("direccion"));
            			mapaResult.put("TELEFONO", rs.getString("telefono"));
            			mapaResult.put("EMAIL", rs.getString("email"));
            			mapaResult.put("CELULAR", rs.getString("celular"));

            		}
            	}
            }
			
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}finally{
        	if(rs!= null){
        		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(conn!= null){
        		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(statement!= null){
        		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	
        }
		
	    return mapaResult;
	    
	}
    
	
	
	public List<PeriodoFacturacion> getListadoPeriodosAcademicos(Map<String, Object> parametros){
		List<PeriodoFacturacion> listaPeriodos = new ArrayList<PeriodoFacturacion>();
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
			IConstantes.SERVIDOR_ACADEMICO, 
			IConstantes.USUARIO_ACADEMICO, 
			IConstantes.CONTRASENA_ACADEMICO, 
			IConstantes.DATABASE_ACADEMICO);
		try{
			String v_sql ="select   distinct bdef.per_acad as periodo ";
			v_sql+="from registro.estudiante estpos,registro.biblia_post bdef ";        
			v_sql+="where estpos.nro_carn='?'";
			v_sql+="and  bdef.clave not in ('I','C') ";
			v_sql+="and bdef.id_est=estpos.id_est ";
			v_sql+="union all ";
			v_sql+="select   distinct bdef.per_acad as periodo ";
			v_sql+="from registro.estudiante est,registro.biblia_def bdef ";        
			v_sql+="where est.nro_carn='?'";
			v_sql+="and  bdef.clave not in ('I','C') ";
			v_sql+="and bdef.id_est=est.id_est ";
			v_sql+="order by 1";        
			
			statement = conn.prepareStatement(v_sql);
        	statement.setString(1,parametros.get("CLIENTE").toString());
        	statement.setString(2,parametros.get("CLIENTE").toString());
        	rs = statement.executeQuery();
        	 if(conn != null){
             	if(rs!= null){
             		while(rs.next()){
             			PeriodoFacturacion periodo = new PeriodoFacturacion();
             			periodo.setPeriodo(rs.getString("periodo"));
             			listaPeriodos.add(periodo);
             		}
             	}
             }
        	
			
		} catch(Exception e){
			e.printStackTrace();
		}finally{
        	if(rs!= null){
        		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(conn!= null){
        		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(statement!= null){
        		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	
        }
		
		return listaPeriodos;
	}
	
	
	
	public String evaluarEstudianteAcademicoAspirante(String identificacion){
		String valor = "N";
		String consulta = "Select "+
						"'S' as estudiante "+
						"from registro.estudiante " +
						"where docest = ? " +
						"and estado > 0 and pyz > 0 "+
						"and tpprg in ('P','E')";
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		
		try{
			statement = conn.prepareStatement(consulta);
			statement.setString(1,identificacion);
			rs = statement.executeQuery();
			if(conn != null){
		        if(rs!= null){
		        	while(rs.next()){
						valor =rs.getString("estudiante");
						
		        		}
		        	}
		        }
			
		}catch(Exception e){
			log.info(e.getMessage());
			e.printStackTrace();
		}finally{
	    	if(rs!= null){
	    		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	if(conn!= null){
	    		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	if(statement!= null){
	    		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	
	    }
		return valor;
		
	}
	
	
	public Estudiante getEstudianteBasic(String idEstudiante, String numeroCarne, String identificacion, String idPrograma ){
		Estudiante estudiante = new Estudiante();
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		String consulta = "SELECT "+
							"est.id_est as idEstudiante, "+
							"est.nom_est as nombreEstudiante, "+
							"est.ape_est as apellidosEstudiante, "+
							"est.docest as documentoEstudiante, "+
							"est.tipdoc as tipoIdentificacion, "+
							"est.nro_carn as numeroCarne, "+
							"est.email as email, "+ 
							"est.eotro as emailOtro, "+
							"est.sexo as sexo, "+
							"est.dir_corr as direccionCorrespondencia, "+
							"est.tel_corr as telefonoCorrespondencia, "+
							"est.cel_est as celularEstudiante, "+
							"est.per_ing as periodoIngreso, "+
							"est.idprg as idPrograma, "+
							"est.tpprg as tipoPrograma, "+
							"est.clanum as claveNumero, "+
							"est.pyz as pazSalvo, "+
							"est.estado as estadoEstudiante, "+
							"est.catmat as categoriaMatricula "+
							"FROM registro.estudiante est "+
							"WHERE (est.id_est  = ? OR est.nro_carn= ? OR est.docest = ?) " +
							"AND est.idprg = ?";
							
							
		try{
			if(conn!=null){
				statement = conn.prepareStatement(consulta);
				statement.setString(1, idEstudiante);
				statement.setString(2, numeroCarne);
				statement.setString(3, identificacion);
				statement.setString(4, idPrograma);
				rs = statement.executeQuery();

				if(rs!=null){
					while(rs.next()){
						
						estudiante.setIdEstudiante(rs.getLong("idEstudiante"));
						estudiante.setNombreEstudiante(rs.getString("nombreEstudiante"));
						estudiante.setApellidosEstudiante(rs.getString("apellidosEstudiante"));
						estudiante.setDocumentoEstudiante(rs.getString("documentoEstudiante"));
						TipoIdentificacion tipoIdentificacion = new TipoIdentificacion();
						tipoIdentificacion.setTipoIdentificacion(rs.getString("tipoIdentificacion"));
						estudiante.setTipoDocumento(tipoIdentificacion);
						estudiante.setEmail(rs.getString("email"));
						estudiante.setEmailOtro(rs.getString("emailOtro"));
						estudiante.setSexo(rs.getString("sexo"));
						estudiante.setDireccionCorrespondencia(rs.getString("direccionCorrespondencia"));
						estudiante.setTelefonoCorrespondencia(rs.getString("telefonoCorrespondencia"));
						estudiante.setCelularEstudiante(rs.getString("celularEstudiante"));
						PeriodoAcademico periodoAcademico = new PeriodoAcademico();
						periodoAcademico.setPeriodoAcademico(rs.getString("periodoIngreso"));
						estudiante.setPeriodoIngreso(periodoAcademico);
						ProgramaAcademico programa = new ProgramaAcademico();
						programa.setIdPrograma(rs.getString("idPrograma"));
						TipoPrograma tipoPrograma = new TipoPrograma();
						tipoPrograma.setIdTipoPrograma(rs.getString("tipoPrograma"));
						programa.setTipoPrograma(tipoPrograma);
						estudiante.setProgramaAcademico(programa);
						estudiante.setClaveNumero(rs.getString("claveNumero"));
						estudiante.setPazSalvo(rs.getInt("pazSalvo"));
						estudiante.setEstado(rs.getInt("estadoEstudiante"));
						CategoriaMatricula categoriaMatricula = new CategoriaMatricula();
						categoriaMatricula.setIdCategoria(rs.getLong("categoriaMatricula"));
						estudiante.setCategoriaMatricula(categoriaMatricula);
						
						
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}finally{
	    	if(rs!= null){
	    		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	if(conn!= null){
	    		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	if(statement!= null){
	    		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	
	    }
		return estudiante;
	}
	
	public Persona getEstudianteAcademicoAspirante(String identificacion){
		
		Cliente cliente = new Cliente();
		Persona persona = new Persona();
		TipoIdentificacion tipId = new TipoIdentificacion();
		String consulta = "select " +
				" est.id_est secuencia_persona,"+ 
				" CASE  WHEN est.tipdoc = 'T' THEN 'T.I' " +
				" WHEN est.tipdoc = 'C' THEN 'C.C' "+ 
				" WHEN est.tipdoc NOT IN ('T','C') THEN 'CEX' "+ 
				" ELSE 'CEX' END tipo_identificacion , "+
				" CASE  WHEN est.tipdoc = 'T' THEN 'TARJETA DE IDENTIDAD' " +
				" WHEN est.tipdoc = 'C' THEN 'CEDULA DE CIUDADANIA' "+ 
				" WHEN est.tipdoc NOT IN ('T','C') THEN 'CEDULA DE EXTRANJERIA' "+ 
				" ELSE 'CEDULA DE EXTRANJERIA' END nombre_identificacion, "+
				" est.docest identificacion, "+
				" est.nom_est nombre, "+
				" est.ape_est apellido, "+
				" '' segundo_apellido,"+
				" est.nro_carn cliente,"+
				" cod_fac+cod_prog programa, "+
				" '1' organizacion, "+
				" 'N' egresado, "+
				" 'S' estudiante_activo"+
				" from  registro.estudiante est, "+ 
				" registro.programas prog, "+
				" registro.facultad fac"+
				" where "+ 
				" est.idprg = prog.id_prog "+
				" and prog.id_fac = fac.id_fac "+
				" and ((est.nro_carn = ?) OR (est.docest = ? ) ) "+
				" and est.tpprg in ('E','P') " +
				" and est.estado > 0 and est.pyz > 0";
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		
		try{
			
			
			if(conn!=null){
				statement = conn.prepareStatement(consulta);
				statement.setString(1, identificacion);
				statement.setString(2, identificacion);
				rs = statement.executeQuery();

				if(rs!=null){
					while(rs.next()){
						
						persona.setSecuenciaPersona(rs.getLong("secuencia_persona"));
						tipId = new TipoIdentificacion();
						cliente = new Cliente();
						cliente.setEgresado(rs.getString("egresado"));
						cliente.setEstudianteActivo(rs.getString("estudiante_activo"));
						cliente.setCliente(rs.getLong("cliente"));
						persona.setCliente(cliente);
						persona.setNombreRazonSocial(rs.getString("nombre"));
						persona.setPrimerApellido(rs.getString("apellido").split(" ")[0]);
						persona.setSegundoApellido(rs.getString("apellido").split(" ").length>=2?rs.getString("apellido").split(" ")[1]:"");
						persona.setIdentificacion(rs.getString("identificacion"));
						tipId.setTipoIdentificacion(rs.getString("tipo_identificacion"));
						tipId.setNombreTipoIdentificacion(rs.getString("nombre_identificacion"));
						persona.setTipoIdentificacion(tipId);
        			
						log.info("Nombre recuperado desde Escolaris: "+persona.getNombreRazonSocial());
						log.info("Apellidos recuperado desde Escolaris: "+persona.getPrimerApellido()+" "+persona.getSegundoApellido());
						
					}
				}
			}
			
		}catch(Exception e){
			log.info(e.getMessage());
			e.printStackTrace();
		}finally{
	    	if(rs!= null){
	    		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	if(conn!= null){
	    		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	if(statement!= null){
	    		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	
	    }
		
		return persona;
	} 
	
	
	public Persona getEgresadoAcademicoAspirante(String identificacion){
		
		Cliente cliente = new Cliente();
		Persona persona = new Persona();
		TipoIdentificacion tipId = new TipoIdentificacion();
		String consulta = "select registro.estudiante.id_est secuencia_persona,"+ 
				" CASE  WHEN tipdoc = 'T' THEN 'T.I' " +
				" WHEN tipdoc = 'C' THEN 'C.C' "+ 
				" WHEN tipdoc NOT IN ('T','C') THEN 'CEX' "+ 
				" ELSE 'CEX' END tipo_identificacion , "+
				" CASE  WHEN tipdoc = 'T' THEN 'TARJETA DE IDENTIDAD' " +
				" WHEN tipdoc = 'C' THEN 'CEDULA DE CIUDADANIA' "+ 
				" WHEN tipdoc NOT IN ('T','C') THEN 'CEDULA DE EXTRANJERIA' "+ 
				" ELSE 'CEDULA DE EXTRANJERIA' END nombre_identificacion, "+
				" docest identificacion, "+
				" nom_est nombre, "+
				" ape_est apellido, "+
				" '' segundo_apellido,"+
				" nro_carn cliente,"+
				" cod_fac+cod_prog programa, "+
				" '1' organizacion, "+
				" 'S' egresado, "+
				" 'N' estudiante_activo,"+
				" registro.estudiante.tpprg tipo_programa"+
				" from  registro.estudiante, "+ 
				" registro.infgrado, "+
				" registro.programas, "+
				" registro.facultad "+
				" where registro.estudiante.id_est = registro.infgrado.id_est "+
				" and registro.infgrado.idprg = registro.programas.id_prog "+
				" and registro.programas.id_fac = registro.facultad.id_fac "+
				" and ((registro.estudiante.nro_carn = ?) OR (registro.estudiante.docest = ? ) )"+
				" order by registro.estudiante.tpprg desc";
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		
		try{
			
			
			if(conn!=null){
				statement = conn.prepareStatement(consulta);
				statement.setString(1, identificacion);
				statement.setString(2, identificacion);
				rs = statement.executeQuery();

				if(rs!=null){
					while(rs.next()){
						
						persona.setSecuenciaPersona(rs.getLong("secuencia_persona"));
						tipId = new TipoIdentificacion();
						cliente = new Cliente();
						cliente.setEgresado(rs.getString("egresado"));
						cliente.setEstudianteActivo(rs.getString("estudiante_activo"));
						cliente.setCliente(rs.getLong("cliente"));
						persona.setCliente(cliente);
						persona.setNombreRazonSocial(rs.getString("nombre"));
						persona.setPrimerApellido(rs.getString("apellido").split(" ")[0]);
						persona.setSegundoApellido(rs.getString("apellido").split(" ").length>=2?rs.getString("apellido").split(" ")[1]:"");
						persona.setIdentificacion(rs.getString("identificacion"));
						tipId.setTipoIdentificacion(rs.getString("tipo_identificacion"));
						tipId.setNombreTipoIdentificacion(rs.getString("nombre_identificacion"));
						persona.setTipoIdentificacion(tipId);
        			
						log.info("Nombre recuperado desde Escolaris: "+persona.getNombreRazonSocial());
						log.info("Apellidos recuperado desde Escolaris: "+persona.getPrimerApellido()+" "+persona.getSegundoApellido());
						
					}
				}
			}
			
		}catch(Exception e){
			log.info(e.getMessage());
			e.printStackTrace();
		}finally{
	    	if(rs!= null){
	    		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	if(conn!= null){
	    		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	if(statement!= null){
	    		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	
	    }
		
		return persona;
	} 
	
	
	public String evaluarEgresadoAcademicoAspirante(String identificacion){
		String valor = "N";
		String consulta = "select "+ 
				" 'S' egresado "+
				" from  registro.estudiante, "+ 
				" registro.infgrado, "+
				" registro.programas, "+
				" registro.facultad "+
				" where registro.estudiante.id_est = registro.infgrado.id_est "+
				" and registro.infgrado.idprg = registro.programas.id_prog "+
				" and registro.programas.id_fac = registro.facultad.id_fac "+
				" and (registro.estudiante.docest = ? )"; 
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		
		try{
			
			statement = conn.prepareStatement(consulta);
			statement.setString(1,identificacion);
			rs = statement.executeQuery();
			if(conn != null){
		        if(rs!= null){
		        	while(rs.next()){
						valor =rs.getString("egresado");
						
		        		}
		        	}
		        }
			
		}catch(Exception e){
			log.info(e.getMessage());
			e.printStackTrace();
		}finally{
	    	if(rs!= null){
	    		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	if(conn!= null){
	    		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	if(statement!= null){
	    		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	    	}
	    	
	    }
		
		return valor;		
	}
	
	public CategoriaMatricula getCategoriaMatricula(String idPrograma){
		CategoriaMatricula categoriaMatricula = new CategoriaMatricula();
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		String consulta = "SELECT cat.idcat as idCategoria, cat.cdprg as codPrograma, cat.valor as valor, cat.nombre as nombre" +
				" FROM registro.categmat cat where cat.cdprg = ? "+
				" and exists (select 'x' from registro.conceptos conc where conc.catgmat = cat.idcat and conc.usa = 'S' ) "+ 
				" order by cat.valor";
		try{
			statement = conn.prepareStatement(consulta);
			statement.setString(1,idPrograma);
			rs = statement.executeQuery();
			if(conn != null){
		        if(rs!= null){
		        	while(rs.next()){
		        		ProgramaAcademico programaAcademico = new ProgramaAcademico();
		        		programaAcademico.setCodigoPrograma(rs.getString("codPrograma"));
		        		categoriaMatricula.setIdCategoria(rs.getLong("idCategoria"));
		        		categoriaMatricula.setNombre(rs.getString("nombre"));
		        		categoriaMatricula.setValor(rs.getDouble("valor"));
		        		categoriaMatricula.setProgramaAcademico(programaAcademico);
		        	}
		        }
		    }
			
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}
		
		return categoriaMatricula;
	}
	
@SuppressWarnings("unchecked")
public List<TipoIdentificacion> getListadoTipoIdentificacion(){
	List<TipoIdentificacion> listaTipoId = new ArrayList<TipoIdentificacion>();
	try{
		
		listaTipoId = (List<TipoIdentificacion>)ParametrizacionFac.getFacade().obtenerListado("selectTipoIdentificacionFinanciero");
		
	}catch(Exception e){
		log.info(e.getMessage());
		e.printStackTrace();
	}
	
	return listaTipoId;
	
}	


@SuppressWarnings("unchecked")
public List<Poblacion> getListadoPoblacion(String valor){
	List<Poblacion> listaPoblacion = new ArrayList<Poblacion>();
	try{
		
		listaPoblacion = (List<Poblacion>)ParametrizacionFac.getFacade().obtenerListado("selectListUbicacionGeograficaFiltro", valor);
		
	}catch(Exception e){
		log.info(e.getMessage());
		e.printStackTrace();
	}
	
	return listaPoblacion;
	
}	



public List<Facultad> getFacultad(String idPrograma){
	List<Facultad> facultades = new ArrayList<Facultad>();
	ResultSet rs = null;
	PreparedStatement statement = null;
	Connection conn = this.getConnectionSqlServer(
			IConstantes.SERVIDOR_ACADEMICO, 
			IConstantes.USUARIO_ACADEMICO, 
			IConstantes.CONTRASENA_ACADEMICO, 
			IConstantes.DATABASE_ACADEMICO);
	try{
		String consulta ="select facu.id_fac as idFacultad, facu.nom_fac as nombreFacultad, facu.cod_fac as codigoFacultad from registro.facultad facu ";
		consulta+="where exists (select 'x' from registro.programas WHERE id_fac=facu.id_fac and id_prog= ? ) ";
		statement = conn.prepareStatement(consulta);
    	statement.setString(1,idPrograma);
    	rs = statement.executeQuery();
    	
		if(conn != null){
        	if(rs!= null){
        		while(rs.next()){
        			Facultad facultad  = new Facultad();
        			facultad.setIdFacultad(Long.valueOf(rs.getString("idFacultad")));
        			facultad.setNombreFacultad(rs.getString("nombreFacultad")); 
        			facultad.setCodigoFacultad(rs.getString("codigoFacultad"));
        			facultades.add(facultad);
        		}
        	}
		}
    	
		
	}catch(Exception e){
		
	}finally{
    	if(rs!= null){
    		try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
    	if(conn!= null){
    		try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
    	if(statement!= null){
    		try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
	}
	
	return facultades;
}
	
public List<ProgramaAcademico> getListadoProgramasAcademicos(){
	
	List<ProgramaAcademico> listaProgramas = new ArrayList<ProgramaAcademico>();
	ResultSet rs = null;
	PreparedStatement statement = null;
	Connection conn = this.getConnectionSqlServer(
			IConstantes.SERVIDOR_ACADEMICO, 
			IConstantes.USUARIO_ACADEMICO, 
			IConstantes.CONTRASENA_ACADEMICO, 
			IConstantes.DATABASE_ACADEMICO);
	try{
		
		String consulta =
				" Select DISTINCT prog.id_prog as idPrograma, prog.nom_prog as nombrePrograma, prog.tip_prog as tipoPrograma, "+
				" fac.id_fac as idFacultad, fac.nom_fac as nombreFacultad, prog.centro_costo as centroCosto, prog.cod_prog as codPrograma, "+
				" prog.periodo as periodo, prog.Descuento as descuento, prog.Porcentaje as porcentaje, prog.cup_prog as cupo  "+		
				" from registro.programas prog " +
				" inner join registro.facultad fac " +
				" on fac.id_fac = prog.id_fac "+
				" where prog.tip_prog = 'D' and prog.activo='S' order by prog.nom_prog";
				
		statement = conn.prepareStatement(consulta);
		rs = statement.executeQuery();
		if(conn != null){
        	if(rs!= null){
        		while(rs.next()){
        			ProgramaAcademico programa  = new ProgramaAcademico();
        			TipoPrograma tipoPrograma = new TipoPrograma();
        			Facultad facultad = new Facultad();
        			CentroCosto centroCosto = new CentroCosto();
        			programa.setIdPrograma(rs.getString("idPrograma"));
        			programa.setNombrePrograma(rs.getString("nombrePrograma"));
        			programa.setCodigoPrograma(rs.getString("codPrograma"));
        			programa.setPeriodo(rs.getString("periodo"));
        			programa.setDescuento(rs.getString("descuento"));
        			programa.setPorcentaje(rs.getString("porcentaje"));
        			programa.setCupoPrograma(rs.getInt("cupo"));
        			tipoPrograma.setIdTipoPrograma(rs.getString("tipoPrograma"));
        			facultad.setIdFacultad(rs.getLong("idFacultad"));
        			facultad.setNombreFacultad(rs.getString("nombreFacultad"));
        			centroCosto.setCentroCosto(rs.getString("centroCosto"));
        			programa.setTipoPrograma(tipoPrograma);
        			programa.setFacultad(facultad);
        			programa.setCentroCosto(centroCosto);
        			listaProgramas.add(programa);
        		}
        	}
		}
	}catch(Exception e){
		log.info(e.getMessage());
		e.printStackTrace();
		
	}finally{
    	if(rs!= null){
    		try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
    	if(conn!= null){
    		try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
    	if(statement!= null){
    		try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
    	
    }
	
	return listaProgramas;
	
}

	public List<PeriodoFacturacion> getPeriodoEducacionContinuada(){
		List<PeriodoFacturacion> periodos = new ArrayList<PeriodoFacturacion>();
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		
        try {
        	
        	String consulta = "select per_ed_continuada as periodoContinuada from registro.periodo_admisiones";
        	statement = conn.prepareStatement(consulta);
        	rs = statement.executeQuery();
        	 
        	  if(conn != null){
              	if(rs!= null){
              		while(rs.next()){
              		PeriodoFacturacion periodo = new PeriodoFacturacion();	
      				 periodo.setPeriodo(rs.getString("periodoContinuada"));
      				 periodos.add(periodo);
              		}
              	}
              }
        	
        }catch(Exception e){
        	e.printStackTrace();
        	log.info(e.getMessage());
        } finally{
           	if(rs!= null){
        		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(conn!= null){
        		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(statement!= null){
        		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        }
        
		return periodos;
	}
	
	public Cliente getAutenticacionAcademico(Map<String, Object> parametros ){
		Cliente clienteAux = new Cliente();
		ResultSet rs = null;
		PreparedStatement statement = null;
		Connection conn = this.getConnectionSqlServer(
				IConstantes.SERVIDOR_ACADEMICO, 
				IConstantes.USUARIO_ACADEMICO, 
				IConstantes.CONTRASENA_ACADEMICO, 
				IConstantes.DATABASE_ACADEMICO);
		
        try {
        	String consulta = "SELECT nro_carn as USUARIO,nom_est +' '+ape_est as NOMBRES,nro_carn as CODIGO  FROM registro.estudiante ";
        	consulta+="WHERE nro_carn = ? AND clanum = ? ";
        	statement = conn.prepareStatement(consulta);
        	statement.setString(1,parametros.get("CLIENTE").toString());
        	statement.setString(2,parametros.get("CONTRASENA").toString());
        	rs = statement.executeQuery();
        	
        if(conn != null){
        	if(rs!= null){
        		while(rs.next()){
				clienteAux.setCliente(new Long(rs.getString("CODIGO")));
				clienteAux.setNombreNegocio(rs.getString("NOMBRES"));
        		}
        	}
        }
        }catch(Exception e){
        	e.printStackTrace();
        }finally{
        	if(rs!= null){
        		try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(conn!= null){
        		try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(statement!= null){
        		try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	
        }
		return clienteAux;
	}
	
	
	public Boolean validatePaymentDate() {
		String horaActual = new SimpleDateFormat("HH:mm").format(new Date());
		String[] horaMinutoActual = horaActual.split(":");
		
		String horaParametro = IConstantes.HORA_BLOQUEO_PAGO;
		String[] horaMinutoParametro = horaParametro.split(":");
		
		if(	Integer.valueOf(horaMinutoActual[0])>=Integer.valueOf(horaMinutoParametro[0])
				&& Integer.valueOf(horaMinutoActual[1])>= Integer.valueOf(horaMinutoParametro[1]))
				{
					System.out.println("Es hora aplicable a bloqueo..."+horaMinutoActual[0]+" : "+horaMinutoActual[1]);
					return false;
				}
		
		return true;
	}
	
}
