package com.konrad.helper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;


public class PropertiesReader {

	public static final PropertiesReader propertiesReader = new PropertiesReader();
	private static Logger log = Logger.getLogger(PropertiesReader.class);

	private PropertiesReader() {
		super();
	}

	public static PropertiesReader getInstance() {
		return propertiesReader;
	}

	public Object readProperties(String ruta, String archivo, String atributo) {
		Object objeto = null;
		try {

			/** Creamos un Objeto de tipo Properties */
			Properties propiedades = new Properties();
			/** Cargamos el archivo desde la ruta especificada */
			if (ruta == null || ruta.equals("")) {
				log.info("ENTRO MAL");
				propiedades.load(new FileInputStream(archivo + ".properties"));
				
			} else {
				log.info(ruta + archivo
						+ ".properties");
				propiedades.load(new FileInputStream(ruta + archivo
						+ ".properties"));
				
			}

			/** Obtenemos el parámetro definido en el archivo */
			objeto = propiedades.getProperty(atributo);

		} catch (FileNotFoundException e) {
			System.out.println("Error, El archivo no existe");
		} catch (IOException e) {
			System.out.println("Error, No se puede leer el archivo");
		}
		return objeto;
	}

}
