package com.konrad.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.rpc.holders.IntHolder;
import javax.xml.rpc.holders.StringHolder;

import org.apache.log4j.Logger;

import NET.webserviceX.www.Currency;

import com.konrad.domain.TransaccionZonaPagos;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.util.IConstantes;
import com.zonapagos.www.ws_verificar_pagos.holders.prod.ArrayOfPagos_v3Holder;
import com.zonapagos.www.ws_verificar_pagos.prod.ServiceSoapProxy;


public class Tester {
	protected static Logger log = Logger.getLogger(Tester.class);
	/**
	 * @param args
	 */
	private TransaccionZonaPagos transaccionZonaPagos;
	
	@SuppressWarnings("unchecked")
	public void verificarEstadoPagosSinIniciar(){
		try{
		TransaccionZonaPagos transaccion = new TransaccionZonaPagos();
		transaccion.setEstadoPago(new Long(IConstantes.ESTADO_PENDIENTE_INICIAR));
		this.setTransaccionZonaPagos(transaccion);
		List<TransaccionZonaPagos> listaTransacciones = new ArrayList <TransaccionZonaPagos>();
		listaTransacciones = ((List<TransaccionZonaPagos>)ParametrizacionFac.getFacade().obtenerListado("selectTransaccionZonaPagosFecha",this.getTransaccionZonaPagos()));
		for(TransaccionZonaPagos objeto: listaTransacciones){
			log.info("Empezar a procesar pagos pendientes Inicio");
			if(objeto!= null){
				if(objeto.getIdPago()>0L){
					if(ReciboConsignacionHelper.getHelper().getServicioPruebasProduccion()){
						ServiceSoapProxy servicioVerificacion = new ServiceSoapProxy();
						ArrayOfPagos_v3Holder res_pagos_v3 = new ArrayOfPagos_v3Holder();
						IntHolder int_error = new IntHolder();
						int int_id_tienda= new Integer(objeto.getIdComercio().toString());
						StringHolder str_error = new StringHolder();
						String str_id_pago = new String(objeto.getIdPago().toString());
						String str_id_clave = new String(objeto.getIdComercio().toString().equals(IConstantes.ID_TIENDA)?
															IConstantes.CLAVE_SERVICIO:
														objeto.getIdComercio().toString().equals(IConstantes.ID_TIENDA_BANCOLOMBIA)?
															IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
														objeto.getIdComercio().toString().equals(IConstantes.ID_TIENDA_DOLARES)?
																IConstantes.CLAVE_SERVICIO_DOLARES:
																IConstantes.CLAVE_SERVICIO_EURO	
																);
						Integer estadoArrayPago;
						int estadoPago = servicioVerificacion.verificar_pago_v3(str_id_pago, int_id_tienda, str_id_clave, res_pagos_v3, int_error, str_error); 
						log.info("encuentra pago: "+estadoPago);
						log.info("id_pago: "+objeto.getIdPago());
						if (estadoPago>0){ //Significa que encontr� un pago
							estadoArrayPago = (res_pagos_v3.value[0]!=null?
												(((Integer)res_pagos_v3.value[0].getInt_estado_pago())!=null?
												res_pagos_v3.value[0].getInt_estado_pago()
												:0)
												:0);
						if (res_pagos_v3.value[0]!=null){
							
							objeto.setIdFormaPago((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()!=null?
									((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()).longValue():new Long(0L));
							objeto.setValorPagado((Double)res_pagos_v3.value[0].getDbl_valor_pagado()!=null?
									((Double)res_pagos_v3.value[0].getDbl_valor_pagado()):new Double(0.00));
							objeto.setTicketId(res_pagos_v3.value[0].getStr_ticketID()!= null?
									!res_pagos_v3.value[0].getStr_ticketID().equals("")?
									new Long(res_pagos_v3.value[0].getStr_ticketID()):new Long(0L):new Long(0L));
							objeto.setIdClave(res_pagos_v3.value[0].getStr_id_clave());
							objeto.setIdCliente(res_pagos_v3.value[0].getStr_id_cliente());
							objeto.setFranquicia(res_pagos_v3.value[0].getStr_franquicia()!=null?!res_pagos_v3.value[0].getStr_franquicia().equals("")?
									res_pagos_v3.value[0].getStr_franquicia():new String("0"):new String("0"));
							objeto.setCodigoAprobacion((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()!=null?
									((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()).longValue():new Long(0L));
							objeto.setCodigoServicio((Integer)res_pagos_v3.value[0].getInt_codigo_servico()!=null?
									((Integer)res_pagos_v3.value[0].getInt_codigo_servico()).longValue():new Long(0L));
							objeto.setCodigoBanco((Integer)res_pagos_v3.value[0].getInt_codigo_banco()!=null?
									((Integer)res_pagos_v3.value[0].getInt_codigo_banco()).longValue():new Long(0L));
							objeto.setNombreBanco(res_pagos_v3.value[0].getStr_nombre_banco()!=null?!res_pagos_v3.value[0].getStr_nombre_banco().equals("")?
									res_pagos_v3.value[0].getStr_nombre_banco():new String("0"):new String("0"));
							objeto.setCodigoTransaccion(res_pagos_v3.value[0].getStr_codigo_transaccion()!=null?
									!res_pagos_v3.value[0].getStr_codigo_transaccion().equals("")?
									new Long(res_pagos_v3.value[0].getStr_codigo_transaccion()):new Long(0L):new Long(0L));
							
							objeto.setCicloTransaccion((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()!=null?
									((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()).longValue():new Long(0L));
							objeto.setCampo1(res_pagos_v3.value[0].getStr_campo1());
							objeto.setCampo2(res_pagos_v3.value[0].getStr_campo2());
							objeto.setCampo3(res_pagos_v3.value[0].getStr_campo3());
							objeto.setDatFecha(objeto.getDatFecha()!=null?objeto.getDatFecha():new Date());
							}
							log.info("estado array: "+estadoArrayPago);
							if (estadoArrayPago.longValue() != objeto.getEstadoPago()) {
								objeto.setEstadoPago(estadoArrayPago.longValue());
							}
							if(!estadoArrayPago.toString().equals(IConstantes.ESTADO_PENDIENTE_INICIAR)){
								ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
							}	
						}else{
							objeto.setEstadoPago(0L);
							ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
						}
					} else{
						com.zonapagos.www.ws_verificar_pagos.test.ServiceSoapProxy servicioVerificacion = new com.zonapagos.www.ws_verificar_pagos.test.ServiceSoapProxy();
						com.zonapagos.www.ws_verificar_pagos.holders.test.ArrayOfPagos_v3Holder res_pagos_v3 = new com.zonapagos.www.ws_verificar_pagos.holders.test.ArrayOfPagos_v3Holder();
						IntHolder int_error = new IntHolder();
						int int_id_tienda= new Integer(objeto.getIdComercio().toString());
						StringHolder str_error = new StringHolder();
						String str_id_pago = new String(objeto.getIdPago().toString());
						String str_id_clave = new String(objeto.getIdComercio().toString().equals(IConstantes.ID_TIENDA)?
								IConstantes.CLAVE_SERVICIO:
							objeto.getIdComercio().toString().equals(IConstantes.ID_TIENDA_BANCOLOMBIA)?
								IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
							objeto.getIdComercio().toString().equals(IConstantes.ID_TIENDA_DOLARES)?
									IConstantes.CLAVE_SERVICIO_DOLARES:
									IConstantes.CLAVE_SERVICIO_EURO);
						Integer estadoArrayPago;
						int estadoPago = servicioVerificacion.verificar_pago_v3(str_id_pago, int_id_tienda, str_id_clave, res_pagos_v3, int_error, str_error); 
						log.info("encuentra pago: "+estadoPago);
						if (estadoPago>0){ //Significa que encontr� un pago
							estadoArrayPago = (res_pagos_v3.value[0]!=null?
												(((Integer)res_pagos_v3.value[0].getInt_estado_pago())!=null?
												res_pagos_v3.value[0].getInt_estado_pago()
												:0)
												:0);
						
							if (res_pagos_v3.value[0]!=null){
								objeto.setIdFormaPago((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()!=null?
										((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()).longValue():new Long(0L));
								objeto.setValorPagado((Double)res_pagos_v3.value[0].getDbl_valor_pagado()!=null?
										((Double)res_pagos_v3.value[0].getDbl_valor_pagado()):new Double(0.00));
								objeto.setTicketId(res_pagos_v3.value[0].getStr_ticketID()!= null?
										!res_pagos_v3.value[0].getStr_ticketID().equals("")?
										new Long(res_pagos_v3.value[0].getStr_ticketID()):new Long(0L):new Long(0L));
								objeto.setIdClave(res_pagos_v3.value[0].getStr_id_clave());
								objeto.setIdCliente(res_pagos_v3.value[0].getStr_id_cliente());
								objeto.setFranquicia(res_pagos_v3.value[0].getStr_franquicia()!=null?!res_pagos_v3.value[0].getStr_franquicia().equals("")?
										res_pagos_v3.value[0].getStr_franquicia():new String("0"):new String("0"));
								objeto.setCodigoAprobacion((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()!=null?
										((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()).longValue():new Long(0L));
								objeto.setCodigoServicio((Integer)res_pagos_v3.value[0].getInt_codigo_servico()!=null?
										((Integer)res_pagos_v3.value[0].getInt_codigo_servico()).longValue():new Long(0L));
								objeto.setCodigoBanco((Integer)res_pagos_v3.value[0].getInt_codigo_banco()!=null?
										((Integer)res_pagos_v3.value[0].getInt_codigo_banco()).longValue():new Long(0L));
								objeto.setNombreBanco(res_pagos_v3.value[0].getStr_nombre_banco()!=null?!res_pagos_v3.value[0].getStr_nombre_banco().equals("")?
										res_pagos_v3.value[0].getStr_nombre_banco():new String("0"):new String("0"));
								objeto.setCodigoTransaccion(res_pagos_v3.value[0].getStr_codigo_transaccion()!=null?
										!res_pagos_v3.value[0].getStr_codigo_transaccion().equals("")?
										new Long(res_pagos_v3.value[0].getStr_codigo_transaccion()):new Long(0L):new Long(0L));
								
								objeto.setCicloTransaccion((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()!=null?
										((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()).longValue():new Long(0L));
								objeto.setCampo1(res_pagos_v3.value[0].getStr_campo1());
								objeto.setCampo2(res_pagos_v3.value[0].getStr_campo2());
								objeto.setCampo3(res_pagos_v3.value[0].getStr_campo3());
								objeto.setDatFecha(objeto.getDatFecha()!=null?objeto.getDatFecha():new Date());
								}
						
							log.info("estado array: "+estadoArrayPago);
							if (estadoArrayPago.longValue() != objeto.getEstadoPago()) {
								objeto.setEstadoPago(estadoArrayPago.longValue());
							}
								if(!estadoArrayPago.toString().equals(IConstantes.ESTADO_PENDIENTE_INICIAR)){
									ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
								}
						} else{
							objeto.setEstadoPago(0L);
							ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
						}
						
					}
				}
			}
		}
		
	} catch (InterruptedException e) {

		e.printStackTrace();
	} catch (Exception e) {
		e.printStackTrace();
	}

		
}
	
	
	public void pruebasConsumoServicio(){
		try{
		TransaccionZonaPagos objeto = new TransaccionZonaPagos();
		objeto.setIdPago(new Long("119632"));
		objeto.setIdClave("Konrad3214");
		objeto.setIdComercio(new Long("310"));
		com.zonapagos.www.ws_verificar_pagos.prod.ServiceSoapProxy servicioVerificacion = new com.zonapagos.www.ws_verificar_pagos.prod.ServiceSoapProxy();
		com.zonapagos.www.ws_verificar_pagos.holders.prod.ArrayOfPagos_v3Holder res_pagos_v3 = new com.zonapagos.www.ws_verificar_pagos.holders.prod.ArrayOfPagos_v3Holder();
		IntHolder int_error = new IntHolder();
		int int_id_tienda= new Integer(objeto.getIdComercio().toString());
		StringHolder str_error = new StringHolder();
		String str_id_pago = new String(objeto.getIdPago().toString());
		String str_id_clave = new String(objeto.getIdClave());
		Integer estadoArrayPago;
		int estadoPago = servicioVerificacion.verificar_pago_v3(str_id_pago, int_id_tienda, str_id_clave, res_pagos_v3, int_error, str_error); 
		log.info("encuentra pago: "+estadoPago);
		if (estadoPago>0){ //Significa que encontr� un pago
			estadoArrayPago = (res_pagos_v3.value[0]!=null?
								(((Integer)res_pagos_v3.value[0].getInt_estado_pago())!=null?
								res_pagos_v3.value[0].getInt_estado_pago()
								:0)
								:0);
			log.info("estado array pagos: "+estadoArrayPago);
			
			objeto.setIdFormaPago((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()!=null?
					((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()).longValue():new Long(0L));
			objeto.setValorPagado((Double)res_pagos_v3.value[0].getDbl_valor_pagado()!=null?
					((Double)res_pagos_v3.value[0].getDbl_valor_pagado()):new Double(0.00));
			objeto.setTicketId(res_pagos_v3.value[0].getStr_ticketID()!= null?
					!res_pagos_v3.value[0].getStr_ticketID().equals("")?
					new Long(res_pagos_v3.value[0].getStr_ticketID()):new Long(0L):new Long(0L));
			objeto.setIdClave(res_pagos_v3.value[0].getStr_id_clave());
			objeto.setIdCliente(res_pagos_v3.value[0].getStr_id_cliente());
			objeto.setFranquicia(res_pagos_v3.value[0].getStr_franquicia());
			objeto.setCodigoAprobacion((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()!=null?
					((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()).longValue():new Long(0L));
			objeto.setCodigoServicio((Integer)res_pagos_v3.value[0].getInt_codigo_servico()!=null?
					((Integer)res_pagos_v3.value[0].getInt_codigo_servico()).longValue():new Long(0L));
			objeto.setCodigoBanco((Integer)res_pagos_v3.value[0].getInt_codigo_banco()!=null?
					((Integer)res_pagos_v3.value[0].getInt_codigo_banco()).longValue():new Long(0L));
			objeto.setNombreBanco(res_pagos_v3.value[0].getStr_nombre_banco());
			objeto.setCodigoTransaccion(res_pagos_v3.value[0].getStr_codigo_transaccion()!=null?
					!res_pagos_v3.value[0].getStr_codigo_transaccion().equals("")?
					new Long(res_pagos_v3.value[0].getStr_codigo_transaccion()):new Long(0L):new Long(0L));
			
			objeto.setCicloTransaccion((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()!=null?
					((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()).longValue():new Long(0L));
			objeto.setCampo1(res_pagos_v3.value[0].getStr_campo1());
			objeto.setCampo2(res_pagos_v3.value[0].getStr_campo2());
			objeto.setCampo3(res_pagos_v3.value[0].getStr_campo3());
			objeto.setDatFecha(objeto.getDatFecha()!=null?objeto.getDatFecha():new Date());
		
			log.info("estado array: "+estadoArrayPago);
			log.info("banco: "+res_pagos_v3.value[0].getStr_nombre_banco());
			log.info("cliente: "+res_pagos_v3.value[0].getStr_id_cliente());
			log.info("fran: "+res_pagos_v3.value[0].getStr_franquicia());
			log.info("Clave: "+res_pagos_v3.value[0].getStr_id_clave());
			log.info("cod banco: "+res_pagos_v3.value[0].getInt_codigo_banco());
			
		} else{
			objeto.setEstadoPago(0L);
			
			}
		
		
	}catch(Exception e){
		e.printStackTrace();
	}
	
}	
	
	public void pruebasConsumoServicioTasa(){
		Double trm =ReciboConsignacionHelper.getHelper().getConversion(Currency.EUR, Currency.USD);
		log.info("Tasa EUR-USD: "+trm.doubleValue());
		
	}
	
	public void pruebasLecturaXml(){
		Double trm = 0d;
		try {
			trm = ReciboConsignacionHelper.getHelper().getConversion(IConstantes.RUTA_CONVERSION_EURO, "yyyy-MM-dd",IConstantes.EUROS, IConstantes.DOLARES_AMERICANOS,
					new SimpleDateFormat("dd/MM/yyyy").parse("28/08/2013"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("Tasa EUR-USD: "+trm.doubleValue());
	}
	
	public Map<String, Object> pruebasSetCurrencyFecha(String formato, String monedaOrigen, String monedaDestino, Date fecha){
		Map<String, Object> mapaResultado = new HashMap <String,Object>();
		mapaResultado = ReciboConsignacionHelper.getHelper().setConversionHistorica(formato, monedaOrigen, monedaDestino, fecha);
		return mapaResultado;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
//			Double miTasa = 1833.76;
//			BigDecimal cifraDecimal =new BigDecimal(miTasa);
//			log.info("Cifra Decimal: "+cifraDecimal.setScale(0,RoundingMode.CEILING));
//			
//			miTasa = 1833.13;
//			cifraDecimal = new BigDecimal(miTasa);
//			log.info("Cifra Decimal 2: "+cifraDecimal.setScale(0,RoundingMode.CEILING));
			
			Tester tester = new Tester();
//			Date fecha = new Date();
//			Map<String, Object> mapaPruebas = new HashMap <String,Object>();
//			mapaPruebas = tester.pruebasSetCurrencyFecha(IConstantes.FORMATO_FECHA,IConstantes.DOLARES_AMERICANOS,IConstantes.PESOS_COLOMBIANOS_ICEBERG,fecha);
//			System.out.println("Moneda Origen Pruebas: "+mapaPruebas.get("MONEDA_ORIGEN"));
//			System.out.println("Moneda Destino Pruebas: "+mapaPruebas.get("MONEDA_DESTINO"));
//			System.out.println("Fecha Pruebas: "+mapaPruebas.get("FECHA"));
//			System.out.println("Factor Pruebas: "+mapaPruebas.get("FACTOR"));
			//tester.pruebasConsumoServicio();
//			tester.pruebasLecturaXml();	
//			tester.pruebasConsumoServicioTasa();
			tester.verificarEstadoPagosSinIniciar();
	}


	public TransaccionZonaPagos getTransaccionZonaPagos() {
		return transaccionZonaPagos;
	}


	public void setTransaccionZonaPagos(TransaccionZonaPagos transaccionZonaPagos) {
		this.transaccionZonaPagos = transaccionZonaPagos;
	}

}
