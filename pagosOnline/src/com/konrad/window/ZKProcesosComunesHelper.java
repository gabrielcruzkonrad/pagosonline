package com.konrad.window;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.zkforge.ckez.CKeditor;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Spinner;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.impl.InputElement;

public class ZKProcesosComunesHelper{
	
	/** desc esta es la variable [ log ] de la clase [ ZKComponentsHelper.java ]*/
	protected static Logger log = Logger.getLogger(ZKProcesosComunesHelper.class);
	
	/** desc esta es la variable [ componentesHelper ] de la clase [ ZKComponentsHelper.java ]*/	
	private ZKComponentesHelper componentesHelper = new ZKComponentesHelper();
	
	/**
	 * @Class ZKComponentsHelper 
	 * @name  resetAllChildren
	 * @param componentePadre
	 * @param filtro
	 * @desc Este método se encarga de resetear los componentes tipo ingreso de datos
	 * util en la reinicialización de campos en los formularios de ZK
	 */
	@SuppressWarnings("rawtypes")
	public void limpiarCampos (AbstractComponent componentePadre, Class filtro)
	{	  
	 List listaComponentes = null;
	 
	 //--obtenemos los componentes correspondientes al filtro
	 listaComponentes = componentesHelper.getComponentes (componentePadre, filtro);
	
	 for (Iterator iteradorComponentes = listaComponentes.iterator(); iteradorComponentes.hasNext();){
		AbstractComponent componente = (AbstractComponent) iteradorComponentes.next();
		
		if (componente instanceof Datebox) {
			Datebox componenteOriginal = (Datebox) componente;
			componenteOriginal.setRawValue(null);
		}
		else if (componente instanceof Decimalbox) {
			Decimalbox componenteOriginal = (Decimalbox) componente;
			componenteOriginal.setValue(new BigDecimal(0.0));
		}
		else if (componente instanceof Doublebox) {
			Doublebox componenteOriginal = (Doublebox) componente;
			componenteOriginal.setValue(new Double(0.0));			
		}
		else if (componente instanceof Intbox) {
			Intbox componenteOriginal = (Intbox) componente;
			componenteOriginal.setValue(new Integer(0));			
		}	
		else if (componente instanceof Textbox) {
			Textbox componenteOriginal = (Textbox) componente;
			componenteOriginal.setRawValue("");
		}
		else if (componente instanceof Checkbox) {
			Checkbox componenteOriginal = (Checkbox) componente;
			componenteOriginal.setChecked(false); 
		}		
		else if (componente instanceof Spinner) {
			Spinner componenteOriginal = (Spinner) componente;
			componenteOriginal.setRawValue(new Integer(0));
		}		
		else if (componente instanceof CKeditor) {
			CKeditor componenteOriginal = (CKeditor) componente;
			componenteOriginal.setValue("");
		}	 
		
	 }//fin for
	}
	
	/**
	 * @Class ZKComponentsHelper 
	 * @name  resetAllChildren
	 * @param componentePadre
	 * @param filtro
	 * @desc Este método se encarga de resetear los componentes tipo ingreso de datos
	 * util en la reinicialización de campos en los formualrios de ZK
	 */
	@SuppressWarnings("rawtypes")
	public boolean validarCampos (AbstractComponent componentePadre, Class filtro)
	{
	 List listaComponentes = null;
	 
	 //--obtenemos los componentes correspondientes al filtro
	 listaComponentes = componentesHelper.getComponentes (componentePadre, filtro);
	 
	 for (Iterator iteradorComponentes = listaComponentes.iterator(); iteradorComponentes.hasNext();){
			AbstractComponent componente = (AbstractComponent) iteradorComponentes.next();
			
			/* si es un componente de tipo entrada de datos entonces se valida
			 * para otra clase de componentes es necesario implementar sus respectivas 
			 * validaciones*/
			
			if (componente instanceof InputElement) {
				InputElement componenteValidar = (InputElement) componente;
				
				//se verifica su validez...
				if (!componenteValidar.isValid()) {
					return false; //--> hubo un error en el formulario
				}	
			}	
	 }//fin for
	 return true;
	}
}

