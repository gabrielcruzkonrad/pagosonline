package com.konrad.window;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.A;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Tabpanels;
import org.zkoss.zul.Tabs;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;

import com.konrad.action.AbonosIcebergAction;
import com.konrad.action.AutenticacionEscolarisAction;
import com.konrad.action.LoginAction;
import com.konrad.action.PagoTotalReciboAction;
import com.konrad.domain.Cliente;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.Usuario;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.util.IConstantes;
import com.konrad.util.TreeIceberg;

public class AdministradorVentanasAction extends ActionStandardBorder implements AfterCompose {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** desc esta es la variable [ log ] de la clase [ AdministradorVentanasAction.java ]*/	
	protected static Logger log = Logger.getLogger(AdministradorVentanasAction.class);	
    private TreeIceberg treeIceberg;
    private ReciboConsignacion reciboConsignacion;
	/* (non-Javadoc)
	 * @see org.zkoss.zk.ui.ext.AfterCompose#afterCompose()
	 */
	@SuppressWarnings("rawtypes")
	public void afterCompose() {
		try{
		Label labelBase = (Label)this.getFellow("lblUsuarioBaseMenu");
		Label labelBaseCodigo = (Label)this.getFellow("lblUsuarioBaseMenuCodigo");
		labelBase.setValue(labelBase.getValue()+
		((Usuario)((HashMap)this.getDesktop().getSession().getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.USUARIO_SESSION)).getNombres()
		);
		
		labelBaseCodigo.setValue("Código: "+
		((Usuario)((HashMap)this.getDesktop().getSession().getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.USUARIO_SESSION)).getUsuario());
		
		ReciboConsignacion recibo = new ReciboConsignacion();
		Cliente cliente = new Cliente();
		cliente.setCliente(new Long(((Usuario)this.getDesktop().getSession().getAttribute(IConstantes.USUARIO_SESSION)).getUsuario()));
		recibo.setCliente(cliente);
		this.setReciboConsignacion(recibo);
		List listaOrdenes = (List)ParametrizacionFac.getFacade().obtenerListado("selectRecibosConsignacionOrdenes", recibo);
		List listaCreditos = (List)ParametrizacionFac.getFacade().obtenerListado("selectRecibosConsignacionCreditosAll", recibo);
		List listaOtros = (List)ParametrizacionFac.getFacade().obtenerListado("selectRecibosConsignacionOtrosPagos", recibo);
		List listaChequesDevueltos = (List)ParametrizacionFac.getFacade().obtenerListado("selectChequesDevueltos", recibo);
		List listaRecibosPorSaldos = (List)ParametrizacionFac.getFacade().obtenerListado("selectRecibosConsignacionRecibosPorSaldos", recibo);
		List listaCartera = (List)ParametrizacionFac.getFacade().obtenerListado("selectCarteraCliente", recibo);
		List listaMultas = (List)ParametrizacionFac.getFacade().obtenerListado("selectListadoMultas", recibo);
		Double multaBiblioteca = (Double)ParametrizacionFac.getFacade().obtenerRegistro("selectSaldoMultasBiblioteca", recibo);
		if(multaBiblioteca == null){
			multaBiblioteca = 0d;
		}
		
		if(listaOrdenes!=null){
		if(listaOrdenes.size()>0){
			this.cargarVentana(IConstantes.ID_ANCLA_DERECHOS_ACADEMICOS);
			}	
		}
		
		if(listaRecibosPorSaldos!=null){
			if(listaRecibosPorSaldos.size()>0){
				this.cargarVentana(IConstantes.ID_ANCLA_RECIBOS_SALDOS);	
				}	
			}
		
		if(listaOtros!= null){
			if(listaOtros.size()>0){
				this.cargarVentana(IConstantes.ID_ANCLA_OTROS_PAGOS);
			}
		}
		
		if(listaMultas!=null){
			if(listaMultas.size()>0){
				this.cargarVentana(IConstantes.ID_ANCLA_MULTAS_BIBLIOTECA);
			}
		}
	
		if(listaChequesDevueltos!=null){
			if(listaChequesDevueltos.size()>0){
				this.cargarVentana(IConstantes.ID_ANCLA_CHEQUES_DEVUELTOS);
				}	
			}
		
		if(listaCartera!=null){
			if(listaCartera.size()>0){
				this.cargarVentana(IConstantes.ID_ANCLA_CARTERA);
				}	
			}
		
		if(listaCreditos!=null){
			if(listaCreditos.size()>0){
				this.cargarVentana(IConstantes.ID_ANCLA_CREDITOS);
			}
		}
	
		if((listaCartera==null && listaChequesDevueltos == null && listaOrdenes==null && listaOtros==null && multaBiblioteca<=0d)||
				(listaCartera.size()<=0 && listaChequesDevueltos.size()<=0 && listaOrdenes.size()<=0 && listaOtros.size()<=0 && multaBiblioteca<=0d)){
			Clients.showNotification(IConstantes.MENSAJE_NO_CARTERA_ORDEN, Clients.NOTIFICATION_TYPE_WARNING, this, "top_left", 6000, true);
		}
		
		if((listaCartera!=null  && listaOrdenes==null && multaBiblioteca <=0d)||
				(listaCartera.size()>0 && listaOrdenes.size()<=0 && multaBiblioteca <=0d)){
			Clients.showNotification(IConstantes.MENSAJE_CARTERA_CREDITO, Clients.NOTIFICATION_TYPE_WARNING, this, "top_left", 6000, true);
		}
		
		if((listaCartera==null  && listaOrdenes==null && multaBiblioteca >0d)||
				(listaCartera.size()<=0 && listaOrdenes.size()<=0 && multaBiblioteca >0d)){
			Clients.showNotification(IConstantes.MENSAJE_CARTERA_MULTA, Clients.NOTIFICATION_TYPE_WARNING, this, "top_left", 6000, true);
		}
		
		if(listaChequesDevueltos.size()>0){
			Clients.showNotification(IConstantes.MENSAJE_CARTERA_CHEQUES, Clients.NOTIFICATION_TYPE_WARNING, this, "top_left", 6000, true);
		}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		//treeIceberg.loadTree();
		// TODO Auto-generated method stub
	}
	
	
	
	
	/**
	 * @type   M�todo de la clase AdministradorVentanasAction
	 * @name   cargarVentana
	 * @return void
	 * @desc   Este metodo se encarga de administrar las peticiones para crear nuevas ventanas
	 */
	
	@SuppressWarnings("rawtypes")
	public void cargarVentanaCondicional(String id){
		try{
		Tabbox tabboxVentanas = (Tabbox) this.getFellow("idZMENTbxVentanas");
		A a = (A)this.getFellow(id);
		String mZul = null;
		String mTitulo = null;
		String mID = null;
	
		if(a!=null){
			if(a.getId()!=null){
				Map<String, Object> mapaAtributos = a.getAttributes();
				if(mapaAtributos.size()>0){
					if(mapaAtributos.containsKey("ruta")){
				
						mZul = mapaAtributos.get("ruta").toString();	
						mTitulo = a.getLabel();
						mID = a.getId();
						
						if (mZul!= null && mZul.length()>1){
							// Si no es abonos o pago total que son ventanas
							if(!mZul.equals(IConstantes.RUTA_ABONOS) && !mZul.equals(IConstantes.RUTA_PAGO_TOTAL) ){
								
								if(mZul.equals(IConstantes.RUTA_NOTAS_PERIODO_ACTUAL) 
											|| mZul.equals(IConstantes.RUTA_NOTAS_HISTORICO) 
											|| mZul.equals(IConstantes.RUTA_INFORME_ESTADO)){
									
									if(((String)this.getDesktop().getSession().getAttribute(IConstantes.AUTENTICADO)).equalsIgnoreCase("N")){
										
										AutenticacionEscolarisAction autenticacionAction = 
												(AutenticacionEscolarisAction)Executions.createComponents(IConstantes.RUTA_AUTENTICACION_ACADEMICO, null, null);
										ReciboConsignacion recibo = new ReciboConsignacion();
										Cliente cliente = new Cliente();
										cliente.setCliente(
												Long.valueOf(((Usuario)((HashMap)this.getDesktop().getSession().getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.USUARIO_SESSION)).getUsuario()));
										
										recibo.setCliente(cliente);
										
										autenticacionAction.doModal(recibo, mZul, mID, this);
									} else if (((String)this.getDesktop().getSession().getAttribute(IConstantes.AUTENTICADO)).equalsIgnoreCase("S")){
										
										//2. configuramos el componente <tabbox> si es necesario
										if (tabboxVentanas.getTabs() == null) {
											log.info("INICIALIZAR TABBOX: "+mTitulo);
											inicializarTabbox();
										}
										//3. Revisar que la ventana ya exista en el listado de tabs
							
										Integer indice = this.getTab(mID);
										log.info("Indice Obtenido: "+indice);
							
										if (indice >= 0) {
											//-- seleccionamos el tab correspondiente y salimos del metodo
											tabboxVentanas.setSelectedIndex(indice);
											return;
										}
							
										//4. Prepara un nuevo Tab que contenga la ventana a cargar
							
										this.inicializarTab(mTitulo, mID);
							
										//5. cargar el contenido.
										log.info("id Area trabajo [buscar]: "+mID+IConstantes.IDTAB_CONTENIDO);
										Tabpanel areaTrabajo = (Tabpanel) this.getFellow(mID
											+ IConstantes.IDTAB_CONTENIDO);
										log.info("abriendo el formulario...");
										//se selecciona el tab que se acaba de crear
										Executions.createComponents(mZul, areaTrabajo, null);
										indice = this.getTab(mID);
										tabboxVentanas.setSelectedIndex(indice);
										
									}
									
								} else{
									//2. configuramos el componente <tabbox> si es necesario
									if (tabboxVentanas.getTabs() == null) {
										log.info("INICIALIZAR TABBOX: "+mTitulo);
										inicializarTabbox();
									}
									//3. Revisar que la ventana ya exista en el listado de tabs
						
									Integer indice = this.getTab(mID);
									log.info("Indice Obtenido: "+indice);
						
									if (indice >= 0) {
										//-- seleccionamos el tab correspondiente y salimos del metodo
										tabboxVentanas.setSelectedIndex(indice);
										return;
									}
						
									//4. Prepara un nuevo Tab que contenga la ventana a cargar
						
									this.inicializarTab(mTitulo, mID);
						
									//5. cargar el contenido.
									log.info("id Area trabajo [buscar]: "+mID+IConstantes.IDTAB_CONTENIDO);
									Tabpanel areaTrabajo = (Tabpanel) this.getFellow(mID
										+ IConstantes.IDTAB_CONTENIDO);
									log.info("abriendo el formulario...");
									//se selecciona el tab que se acaba de crear
									Executions.createComponents(mZul, areaTrabajo, null);
									indice = this.getTab(mID);
									tabboxVentanas.setSelectedIndex(indice);
								}
								
								}else{
									if(mZul.equals(IConstantes.RUTA_ABONOS)){
										AbonosIcebergAction abonosAction = (AbonosIcebergAction)Executions.createComponents(mZul, null, null);
										abonosAction.doModal();
									} else{
										PagoTotalReciboAction pagoTotalAction = (PagoTotalReciboAction)Executions.createComponents(mZul, null, null);
										pagoTotalAction.doModal(this.getReciboConsignacion());
									}
								}
							}
						}
					}
				}
			}	
			
		}catch(Exception e){
			log.info(e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * @type   M�todo de la clase AdministradorVentanasAction
	 * @name   cargarVentana
	 * @return void
	 * @desc   Este metodo se encarga de administrar las peticiones para crear nuevas ventanas
	 */
	
	public void cargarVentana(String id){
		try{
		Tabbox tabboxVentanas = (Tabbox) this.getFellow("idZMENTbxVentanas");
		A a = (A)this.getFellow(id);
		String mZul = null;
		String mTitulo = null;
		String mID = null;
	
		if(a!=null){
			if(a.getId()!=null){
				Map<String, Object> mapaAtributos = a.getAttributes();
				if(mapaAtributos.size()>0){
					if(mapaAtributos.containsKey("ruta")){
				
						mZul = mapaAtributos.get("ruta").toString();	
						mTitulo = a.getLabel();
						mID = a.getId();
						
						if (mZul!= null && mZul.length()>1){
							if(!mZul.equals("pages/abonosIceberg.zul") && !mZul.equals("pages/pagoTotalRecibo.zul") ){
								//2. configuramos el componente <tabbox> si es necesario
								if (tabboxVentanas.getTabs() == null) {
									log.info("INICIALIZAR TABBOX: "+mTitulo);
									inicializarTabbox();
								}
								//3. Revisar que la ventana ya exista en el listado de tabs
						
								Integer indice = this.getTab(mID);
								log.info("Indice Obtenido: "+indice);
						
								if (indice >= 0) {
									//-- seleccionamos el tab correspondiente y salimos del metodo
									tabboxVentanas.setSelectedIndex(indice);
									return;
								}
						
								//4. Prepara un nuevo Tab que contenga la ventana a cargar
						
								this.inicializarTab(mTitulo, mID);
						
								//5. cargar el contenido.
								log.info("id Area trabajo [buscar]: "+mID+IConstantes.IDTAB_CONTENIDO);
								Tabpanel areaTrabajo = (Tabpanel) this.getFellow(mID
										+ IConstantes.IDTAB_CONTENIDO);
								log.info("abriendo el formulario...");
								//se selecciona el tab que se acaba de crear
								Executions.createComponents(mZul, areaTrabajo, null);
								indice = this.getTab(mID);
								tabboxVentanas.setSelectedIndex(indice);
								
							}else{
								if(mZul.equals("pages/abonosIceberg.zul")){
								AbonosIcebergAction abonosAction = (AbonosIcebergAction)Executions.createComponents(mZul, null, null);
								abonosAction.doModal();
								} else{
									PagoTotalReciboAction pagoTotalAction = (PagoTotalReciboAction)Executions.createComponents(mZul, null, null);
									pagoTotalAction.doModal(this.getReciboConsignacion());
								}
								
							}
						}
					}
					
				}
					 
			}
					
		}
		}catch(Exception e){
			log.info(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void cargarVentana(){
		log.info("Ejecutando el m�todo [ cargarVentana ]... ");
		
		try {
			Tree menuOpciones = (Tree) this.getFellow("idZMENTreeMenuOpciones");
			//Tree menuOpciones = (Tree) this.getFellow("idZMENTreeMenuOpcionesDinamico");
			Tabbox tabboxVentanas = (Tabbox) this.getFellow("idZMENTbxVentanas");
			
			String mZul = null;
			String mTitulo = null;
			String mID = null;
			//1. Obtener la informaci�n suficiente para cargar la ventana
			
			
			
			//mTitulo = 		((Treecell) ((Treerow) menuOpciones.getSelectedItem().getFirstChild())
						//	.getChildren().get(0)).getLabel();

			//mZul =
				//	((Treecell) ((Treerow) menuOpciones.getSelectedItem().getFirstChild())
					//		.getChildren().get(1)).getLabel();
			
			mZul = menuOpciones.getSelectedItem().getValue().toString();
			mTitulo = ((Treecell) menuOpciones.getSelectedItem().getTreerow()
					.getFirstChild()).getLabel().toString();
			mID = menuOpciones.getSelectedItem().getId().toString();
			//mID = ((Treecell) ((Treerow) menuOpciones.getSelectedItem().getFirstChild())
					//.getChildren().get(4)).getLabel();
			
			log.info("opcion de menu  : " + mZul);
			log.info("opcion de menu [Longitud]  : " + mZul.length());
			log.info("Id Item Selected: " + mID);
			log.info("Titulo ventana: "+mTitulo);
			
			if (mZul!= null && mZul.length()>1){
				if(!mZul.equals("pages/abonosIceberg.zul") && !mZul.equals("pages/pagoTotalRecibo.zul") ){
					//2. configuramos el componente <tabbox> si es necesario
					if (tabboxVentanas.getTabs() == null) {
						log.info("INICIALIZAR TABBOX: "+mTitulo);
						inicializarTabbox();
					}
					//3. Revisar que la ventana ya exista en el listado de tabs
			
					Integer indice = this.getTab(mID);
					log.info("Indice Obtenido: "+indice);
			
					if (indice >= 0) {
						//-- seleccionamos el tab correspondiente y salimos del metodo
						tabboxVentanas.setSelectedIndex(indice);
						menuOpciones.clearSelection();
						return;
					}
			
					//4. Prepara un nuevo Tab que contenga la ventana a cargar
			
					this.inicializarTab(mTitulo, mID);
			
					//5. cargar el contenido.
					log.info("id Area trabajo [buscar]: "+mID+IConstantes.IDTAB_CONTENIDO);
					Tabpanel areaTrabajo = (Tabpanel) this.getFellow(mID
							+ IConstantes.IDTAB_CONTENIDO);
					log.info("abriendo el formulario...");
					//se selecciona el tab que se acaba de crear
					Executions.createComponents(mZul, areaTrabajo, null);
					indice = this.getTab(mID);
					tabboxVentanas.setSelectedIndex(indice);
					
				}else{
					if(mZul.equals("pages/abonosIceberg.zul")){
					AbonosIcebergAction abonosAction = (AbonosIcebergAction)Executions.createComponents(mZul, null, null);
					abonosAction.doModal();
					} else{
						PagoTotalReciboAction pagoTotalAction = (PagoTotalReciboAction)Executions.createComponents(mZul, null, null);
						pagoTotalAction.doModal(this.getReciboConsignacion());
					}
					
				}
				//6. ejecutamos cualquier l�gica adicinal antes de terminar este m�todo
				menuOpciones.clearSelection();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onPrintAttribute(String id){
		log.info("onPrintAttribute[String id]");
		A a = new A();
		a = (A)this.getFellow(id);
		Map<String, Object> mapaAtributos = a.getAttributes();
		
		if(mapaAtributos.size()>0){
			log.info("tama�o Mapa: "+mapaAtributos.size());
			if(mapaAtributos.containsKey("ruta")){
				log.info(mapaAtributos.get("ruta").toString());
			}
		}
		
		
		
	}

	public void onSalir(){
		log.info("saliendo de aplicacion... ");
		Integer resultado = -1;

		try {
		  
		    resultado = Messagebox.show(
			    "¿Está seguro de Salir?",
			    "Salir de Pagos en Línea",
			    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);

		    if (resultado == Messagebox.YES) {
			// destruimos la session
		    	
			this.getDesktop().getSession()
				.removeAttribute(IConstantes.INFORMACION_SESION);
			this.getDesktop().setAttribute(IConstantes.INFORMACION_SESION, null);
			//this.getDesktop().getSession().invalidate();
			this.detach();
			
			LoginAction loginAction  = (LoginAction)Executions.createComponents("index.zul",
				this.getParent(), null);
			loginAction.afterCompose();
		    }
		} catch (Exception e) {
		    log.error(e.getMessage(), e);
		    e.printStackTrace();
		} 
	    }
		
	
	/**
	 * @type   Método de la clase AdministradorVentanasAction
	 * @name   inicializarTabbox
	 * @return void
	 * @desc   Este método se encarga de inicializar el tabbox de las ventanas
	 */
	public void inicializarTabbox(){
		Tabbox tabboxVentanas = (Tabbox) this.getFellow("idZMENTbxVentanas");
		
		//--- creamos su componente <tabs>
		Tabs grupoCabecerasTabs = new Tabs();
		grupoCabecerasTabs.setId("idZMENTabsHeaderVentanas");
		tabboxVentanas.appendChild( grupoCabecerasTabs );
		
		//--- creamos su componente <tabs>
		Tabpanels grupoTabPanels = new Tabpanels();
		grupoTabPanels.setId("idZMENTabpanelsBodyVentanas");
		tabboxVentanas.appendChild( grupoTabPanels );		
	}
	
	/**
	 * @type   M�todo de la clase AdministradorVentanasAction
	 * @name   inicializarTab
	 * @return void
	 * @desc   Este m�todo se encarga de preparar un panel vac�o para su uso. 
	 */
	public void inicializarTab ( String pTabTitulo, String pID ){
		
		//---creamos un elemento <tab>
		Tabs grupoCabecerasTabs = (Tabs) this.getFellow("idZMENTabsHeaderVentanas");
		Tab tab = new Tab();
		tab.setLabel( pTabTitulo );
		tab.setId(pID + IConstantes.IDTAB_CABECERA);
		tab.setClosable(true);
		grupoCabecerasTabs.appendChild( tab );
		
		//-- creamos un elemento <tabpanel>
		Tabpanels grupoTabPanels = (Tabpanels) this.getFellow("idZMENTabpanelsBodyVentanas");
		Tabpanel tabpanel = new Tabpanel();
		
		tabpanel.setId(pID + IConstantes.IDTAB_CONTENIDO);
		log.info("TabPanel Generado: "+pID + IConstantes.IDTAB_CONTENIDO);
		grupoTabPanels.appendChild( tabpanel );
	}
	
	/**
	 * @type   M�todo de la clase AdministradorVentanasAction
	 * @name   getTab
	 * @return Integer
	 * @param  pID
	 * @desc   Este m�todo se encarga de obtener un tab 
	 */
	
	public Integer getTab( String pID ){
		Tabs grupoCabecerasTabs = (Tabs) this.getFellow("idZMENTabsHeaderVentanas");
		Integer i = 0; //-mantenemos un indice del tab, los tab existentes son indexados desde el 0 (cero)
		
		if ( grupoCabecerasTabs.getChildren().size() <= 0){
			//-- simplemente no hay que evaluar
			return -2;
		}
		else{
			
			List<Component> listadoTabsVentanas  = grupoCabecerasTabs.getChildren();
			for (Object componente : listadoTabsVentanas) {
				Tab tab = (Tab) componente;				
				if (tab.getId().equals(pID+IConstantes.IDTAB_CABECERA)){
					//-- el tab ya existe!!!!
					return i; 
				}
				i++;
			}
			//--el tab no fue encontrado
			return -1;
		}
	}

	public void setTreeIceberg(TreeIceberg treeIceberg) {
		this.treeIceberg = treeIceberg;
	}

	public TreeIceberg getTreeIceberg() {
		return treeIceberg;
	}

	public ReciboConsignacion getReciboConsignacion() {
		return reciboConsignacion;
	}

	public void setReciboConsignacion(ReciboConsignacion reciboConsignacion) {
		this.reciboConsignacion = reciboConsignacion;
	}
	
	
}

