package com.konrad.window;

import java.util.List;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Tabpanels;
import org.zkoss.zul.Tabs;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;

import com.konrad.util.IConstantes;




public class AdministraVentanas extends ActionStandardBorder implements AfterCompose {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** desc esta es la variable [ log ] de la clase [ AdministradorVentanasAction.java ]*/	
	protected static Logger log = Logger.getLogger("");	

	/* (non-Javadoc)
	 * @see org.zkoss.zk.ui.ext.AfterCompose#afterCompose()
	 */
	public void afterCompose() {
		// TODO Auto-generated method stub
	}
	
	/**
	 * @type   Método de la clase AdministradorVentanasAction
	 * @name   cargarVentana
	 * @return void
	 * @desc   Este metodo se encarga de administrar las peticiones para crear nuevas ventanas
	 */
	public void cargarVentana(){
		log.info("Ejecutando el m�todo [ cargarVentana ]... ");
		
		try {
			Tree menuOpciones = (Tree) this.getFellow("idZMENTreeMenuOpciones");
			Tabbox tabboxVentanas = (Tabbox) this.getFellow("idZMENTbxVentanas");
			String mZul = null;
			String mTitulo = null;
			String mID = null;
			//1. Obtener la información suficiente para cargar la ventana
			mZul = menuOpciones.getSelectedItem().getValue().toString();
			mTitulo = ((Treecell) menuOpciones.getSelectedItem().getTreerow()
					.getFirstChild()).getLabel().toString();
			mID = menuOpciones.getSelectedItem().getId();
			log.info("opcion de menu  : " + mZul);
			log.info("Id Item Selected: " + mID);
			
			//2. configuramos el componente <tabbox> si es necesario
			if (tabboxVentanas.getTabs() == null) {
				inicializarTabbox();
			}
			//3. Revisar que la ventana ya exista en el listado de tabs
			Integer indice = this.getTab(mID);
			if (indice >= 0) {
				//-- seleccionamos el tab correspondiente y salimos del m�todo
				tabboxVentanas.setSelectedIndex(indice);
				menuOpciones.clearSelection();
				return;
			}
			//4. Prepara un nuevo Tab que contenga la ventana a cargar
			this.inicializarTab(mTitulo, mID);
			
			//5. cargar el contenido.
			Tabpanel areaTrabajo = (Tabpanel) this.getFellow(mID
					+ IConstantes.IDTAB_CONTENIDO);
			Executions.createComponents(mZul, areaTrabajo, null);
			
			//6. ejecutamos cualquier lógica adicinal antes de terminar este método
			menuOpciones.clearSelection();
			
			//se selecciona el tab que se acaba de crear
			indice = this.getTab(mID);
			tabboxVentanas.setSelectedIndex(indice);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * @type   Método de la clase AdministradorVentanasAction
	 * @name   inicializarTabbox
	 * @return void
	 * @desc   Este método se encarga de inicializar el tabbox de las ventanas
	 */
	public void inicializarTabbox(){
		Tabbox tabboxVentanas = (Tabbox) this.getFellow("idZMENTbxVentanas");
		
		//--- creamos su componente <tabs>
		Tabs grupoCabecerasTabs = new Tabs();
		grupoCabecerasTabs.setId("idZMENTabsHeaderVentanas");
		tabboxVentanas.appendChild( grupoCabecerasTabs );
		
		//--- creamos su componente <tabs>
		Tabpanels grupoTabPanels = new Tabpanels();
		grupoTabPanels.setId("idZMENTabpanelsBodyVentanas");
		tabboxVentanas.appendChild( grupoTabPanels );		
	}
	
	/**
	 * @type   M�todo de la clase AdministradorVentanasAction
	 * @name   inicializarTab
	 * @return void
	 * @desc   Este m�todo se encarga de preparar un panel vac�o para su uso. 
	 */
	public void inicializarTab ( String pTabTitulo, String pID ){
		
		//---creamos un elemento <tab>
		Tabs grupoCabecerasTabs = (Tabs) this.getFellow("idZMENTabsHeaderVentanas");
		Tab tab = new Tab();
		tab.setLabel( pTabTitulo );
		tab.setId(pID + IConstantes.IDTAB_CABECERA);
		tab.setClosable(true);
		grupoCabecerasTabs.appendChild( tab );
		
		//-- creamos un elemento <tabpanel>
		Tabpanels grupoTabPanels = (Tabpanels) this.getFellow("idZMENTabpanelsBodyVentanas");
		Tabpanel tabpanel = new Tabpanel();
		tabpanel.setId(pID + IConstantes.IDTAB_CONTENIDO);
		grupoTabPanels.appendChild( tabpanel );
	}
	
	/**
	 * @type   M�todo de la clase AdministradorVentanasAction
	 * @name   getTab
	 * @return Integer
	 * @param  pID
	 * @desc   Este m�todo se encarga de obtener un tab 
	 */

	public Integer getTab( String pID ){
		Tabs grupoCabecerasTabs = (Tabs) this.getFellow("idZMENTabsHeaderVentanas");
		Integer i = 0; //-mantenemos un indice del tab, los tab existentes son indexados desde el 0 (cero)
		
		if ( grupoCabecerasTabs.getChildren().size() <= 0){
			//-- simplemente no hay que evaluar
			return -2;
		}
		else{
			
			List<Component> listadoTabsVentanas  = grupoCabecerasTabs.getChildren();
			for (Object componente : listadoTabsVentanas) {
				Tab tab = (Tab) componente;				
				if (tab.getId().equals(pID+IConstantes.IDTAB_CABECERA)){
					//-- el tab ya existe!!!!
					return i; 
				}
				i++;
			}
			//--el tab no fue encontrado
			return -1;
		}
	}
	
	
}
