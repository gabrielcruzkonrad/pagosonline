package com.konrad.window;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.AbstractComponent;


public class ZKComponentesHelper 
{
	/** desc esta es la variable [ log ] de la clase [ ZKComponentsHelper.java ]*/
	protected static Logger log = Logger.getLogger(ZKComponentesHelper.class);
	
	/**
	 * @Class ZKComponentsHelper 
	 * @name  getComponents
	 * @param componentePadre
	 * @param filtro
	 * @param listaComponentesFiltrada
	 * @return List
	 * @desc Este m�todo se encarga de recorrer el arbol de componentes a partir de un componente padre
	 * y retorna los componentes hijos que cumplan con el filtro, el acceso directo a este m�todo esta restringido 
	 * solo a llamados internos de la clase.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List getComponentes (AbstractComponent componentePadre, Class filtro, List listaComponentesFiltrada)
	{
	 List listaComponentesHijos = (List) componentePadre.getChildren();
	 AbstractComponent componenteHijo =  null;
	 
	 //-- validamos que la lista no est� vac�a
	 if (listaComponentesFiltrada == null)
	     listaComponentesFiltrada = new ArrayList();
	
     for (int i = 0; i < listaComponentesHijos.size(); i++) 
     {       
	   componenteHijo = (AbstractComponent) listaComponentesHijos.get(i);
	   
	   //--si es un componente del tipo del filtro...
	   if (filtro.isInstance(componenteHijo) && filtro!=null){
	     //log.info("hijo " + i + " -> " + componenteHijo.getId() + " clase: " + componenteHijo.getClass());
	     listaComponentesFiltrada.add(componenteHijo);
	   }
	   else if (filtro == null)//si no hay filtros entonces le mandamos todos...
	   {
		 //log.info("hijo " + i + " -> " + componenteHijo.getId() + " clase: " + componenteHijo.getClass());
		 listaComponentesFiltrada.add(componenteHijo);
	   }
	   
	   //--si el componente es un contenedor de m�s objetos entonces.. los invocamos tambien a ellos
	   if (componenteHijo.getChildren().size()!= 0)
	   {		   
		 //CUIDADO!!! recursividad a la vista..
		   getComponentes (componenteHijo,filtro,listaComponentesFiltrada);
	   }	   		
	 }//fin for
     return listaComponentesFiltrada;
	}
	
	/**
	 * @Class ZKComponentsHelper 
	 * @name  getComponents
	 * @param componentePadre
	 * @param filtro
	 * @return List
	 * @desc Este m�todo se encarga de recorrer el arbol de componentes a partir de un componente padre
	 * y retorna los componentes hijos que cumplan con el filtro.
	 */
	@SuppressWarnings("rawtypes")
	public List getComponentes (AbstractComponent componentePadre, Class filtro)
	{
	 List listaComponentes = null;	

	 //--obtenemos los componentes correspondientes al filtro
	 listaComponentes = getComponentes (componentePadre, filtro, listaComponentes);
		
	 return listaComponentes;
	}	
}
