package com.konrad.window;

import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zul.Row;

/**
 * @author Fabio Bar�n
 * @date Oct 28, 2008
 * @name IAssemblerStandard.java
 * @desc Esta clase provee la plantilla b�sica para el ensamble de 
 *       objetos 
 */
public interface IAssemblerStandard {

	/**
	 * @type   M�todo de la clase IAssemblerStandard
	 * @name   crearRowDesdeDto
	 * @return Row
	 * @param objetoFuente
	 * @desc   Este m�todo se encarga de crear un objeto Row a partir de un DTO 
	 */
	//public Row crearRowDesdeDto(Object objetoFuente);
	
	public Row crearRowDesdeDto(Object objetoFuente, AbstractComponent componente);
	/**
	 * @type   M�todo de la clase IAssemblerStandard
	 * @name   crearDtoDesdeRow
	 * @return Object
	 * @param componente
	 * @desc   Este m�todo se encarga de crear un DTO a partor de un componente de tipo AbstractComponente
	 */
	public Object crearDtoDesdeRow(AbstractComponent componente);
}
