package com.konrad.window;

import java.util.List;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;
import com.konrad.util.IConstantes;

/**
 * @author Caseware
 * @date 23/01/2007
 * @name ActionStandard.java
 * @desc Define el comportamiento que debe tener todos los Actions del proyecto adem�s
 *       provee las funcionalidades de:
 *       - validarFormulario:  valida los controles contenidos dentro de un control con ID=formulario
 *       - limpiarFormulario: limpia los controles contenidos dentro de un control con ID=formulario
 *       - setMensajeHistorico: Coloca un mensaje en la zona de mensajes hist�riocos de las ventanas
 *       - limpiarHistoricoMensajes: limpia el contenido de la zona de mensajes hist�riocos de las ventanas
 *       - persistirObjeto: ayuda a maneter persistido un objeto... hibernate
 *       - confirmarTransaccion: ayuda a manetner los datos sincronizados por medio de un flush (commit)
 *       - onConfirmacionPopup: muestra una pantalla de confirmaci�n de borrado 
 *       - lanzarExcepcion: env�a una excepci�n ocurrida a una ventana modal.
 */
@SuppressWarnings({  "serial" })
public abstract class ActionStandard extends Window 
{
	
	/** desc esta es la variable [ log ] de la clase [ ActionStandar.java ]*/	
	protected static Logger log = Logger.getLogger(ActionStandard.class);
	

	/**
	 * @Class ActionStandar 
	 * @name  validarFormulario
	 * @return boolean
	 * @desc Este m�todo se encarga de validar el formualario, este metodo a diferencia del 
	 * anterior recibe el id del componente a validar
	 */
	public boolean validarFormulario (String idFormulario) {
	
		AbstractComponent contenedorFormulario = null;
		ZKProcesosComunesHelper zkHelper = null;		
		
		//--- obtenemos el contenedor del formulario
		contenedorFormulario =(AbstractComponent) this.getFellow(idFormulario);
		
		//instanciamos la clase que ayudar� en procesos comunes en los formualrio como la validaci�n 
		zkHelper = new ZKProcesosComunesHelper();
			
		//-validamos el formualrio
		return zkHelper.validarCampos(contenedorFormulario, InputElement.class);		
	}



	/**
	 * @Class ActionStandar 
	 * @name  limpiarFormulario
	 * @return boolean
	 * @param  componente
	 * @desc Este método se encarga de reiniciar los campos del formulario que llega como par�metro
	 */
	public void limpiarFormulario( AbstractComponent componente )
	{
		AbstractComponent contenedorFormulario = null;
		ZKProcesosComunesHelper zkHelper = null;		
		
		//--- obtenemos el contenedor del formulario seg�n el par�metro
		contenedorFormulario = componente;
		
		//instanciamos la clase que ayudar� en procesos comunes en los formualrio como la validaci�n 
		zkHelper = new ZKProcesosComunesHelper();
			
		//-validamos el formulario
		zkHelper.limpiarCampos (contenedorFormulario, AbstractComponent.class);
	}	
	
	/**
	 * @Class ActionStandar 
	 * @name  limpiarFormulario
	 * @return boolean
	 * @param id 
	 * @desc Este m�todo se encarga de reiniciar los campos del formulario que se identifique 
	 *       con el par�metro id
	 */
	public void limpiarFormulario( String idFormulario )
	{
		AbstractComponent contenedorFormulario = null;
		ZKProcesosComunesHelper zkHelper = null;		
		
		//--- obtenemos el contenedor del formulario seg�n el id de par�metro
		contenedorFormulario =(AbstractComponent) this.getFellow( idFormulario );
		
		//instanciamos la clase que ayudar� en procesos comunes en los formualrio como la validaci�n 
		zkHelper = new ZKProcesosComunesHelper();
			
		//-validamos el formulario
		zkHelper.limpiarCampos (contenedorFormulario, AbstractComponent.class);
	}

	/**
	 * @Class ActionStandar 
	 * @name  setMensajeHistorico
	 * @return void
	 * @desc Este m�todo se encarga de actualizar el mensaje de informaci�n
	 */
	public void setMensajeHistorico(String idGridHistorico,String idGroupHistorico,int estado, String labeli18n,String llaveRegistro,String mensajeExt)
	{
	 Grid  contenedorMensajes = null;
	 Rows contenido = null;
	 Row fila = null;
	 Image imagen = null;
	 String mensaje = "";
	   
	 String llave;
	 
	
	 if(llaveRegistro==null){
		 llave="";
	 }else{
	 llave=" [ " + llaveRegistro +" ]";
	 }
	 
	 if(mensajeExt==null){
		 mensajeExt="";
	 }
	 
	 //--- obtenemos la grilla del historico de mensajes
	 
	 contenedorMensajes =  (Grid) this.getFellow(idGridHistorico);
	 limpiarHistoricoMensajes(contenedorMensajes,idGridHistorico,idGroupHistorico);
	 
	 //hacemos visible el group que contiene la grilla
	 Groupbox groupinformacion =(Groupbox) this.getFellow(idGroupHistorico); 
	 groupinformacion.setVisible(true);

	 contenido = contenedorMensajes.getRows();
	 
	 //preparamos el contenedor del menasje	 	 
	 fila = new Row();
	 fila.setParent(contenido);
	 fila.setStyle("background: #FFFFF0 ");
     try 
     {
    	 if(labeli18n!=null){	
    		 //se agrega la llave que tiene el contenido del mensaje
    		// mensaje = IdiomaFacade.getLabel(labeli18n);
    	 }

		 switch (estado)
		 {
		 case IConstantes.ESTADO_INSERCION_OK:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_OK);
			   
			   break;
			  }
		 case IConstantes.ESTADO_INSERCION_ERROR:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			   llave="";
			   break;
			  }	 
		 case IConstantes.ESTADO_EDICION_OK:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_OK);
			   break;
			  }
		 case IConstantes.ESTADO_EDICION_ERROR:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			   break;
			  }	 
		 case IConstantes.ESTADO_BORRAR_OK:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_OK);
			   break;
			  }	 
		 case IConstantes.ESTADO_BORRAR_ERROR:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			   break;
			  }	 
		 case IConstantes.ESTADO_DEFAULT_OK:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_OK);
			   //mensaje = IdiomaFacade.getLabel("global:mensajeEstadoDefaultOK.default.prompt");
			   llave="";
			   break;
			  }	 
		 case IConstantes.ESTADO_DEFAULT_ERROR:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			   //mensaje = IdiomaFacade.getLabel("global:mensajeEstadoDefaultERROR.default.prompt");
			   //llave=""; //TODO: ver que no se necesite
			   break;
			  }		 
		 default:{
			 imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			 //mensaje = IdiomaFacade.getLabel("Estado no definido");
		  }		 
		 }//fin switch
		
        } 
        catch (Exception e) {
        	//--error del negocio...
        	//this.lanzarExcepcion(new Excepcion(IExcepcion.EXCEPTION_GLOBAL,""));//TODO internacionalizar
		}		 
	 //---ponemos el mensaje
	 fila.appendChild(imagen);	 
	 fila.appendChild(new Label(mensaje + llave+" "+mensajeExt));
	}	
	
	
	/**
	 * @Class ActionStandar 
	 * @name  setMensajeHistorico
	 * @return void
	 * @desc Este m�todo se encarga de actualizar el mensaje de informaci�n
	 */
	@SuppressWarnings({ "null", "rawtypes" })
	public void setMensajeHistorico(String idGridHistorico,String idGroupHistorico,int estado, String labeli18n,List llavesNaturales,String mensajeExt,String formai18n )
	{
	 Grid  contenedorMensajes = null;
	 //LlaveNatural llaveNatural = null;
	 Rows contenido = null;
	 Row fila = null;
	 Image imagen = null;
	 String mensaje = "";
	 String llave = "";
	 
	 
	 if(llavesNaturales != null){
		 llave = " [ ";		 
		 for(int i=0;i<llavesNaturales.size();i++){
			 //llaveNatural  = (LlaveNatural)llavesNaturales.get(i);	
			 if(formai18n != null || !formai18n.equals("")){				 
				// resultado = llaveNatural.getLlave().split("\\.")[0]+"."+formai18n+"."+llaveNatural.getLlave().split("\\.")[2];
			 }
			 else{
				 //resultado = llaveNatural.getLlave();
			 }			 
			 //llave = llave +IdiomaFacade.getLabel(resultado)+ " "+ llaveNatural.getValor();			 
			 
			 if(llavesNaturales.size()-1 > i){			
				 llave = llave +", ";
			 }			 
		 }		 
		 llave = llave+" ] ";
	}
	 
	 if(mensajeExt==null){
		 mensajeExt="";
	 }
	 
	 //--- obtenemos la grilla del historico de mensajes
	 
	 contenedorMensajes =  (Grid) this.getFellow(idGridHistorico);
	 limpiarHistoricoMensajes(contenedorMensajes,idGridHistorico,idGroupHistorico);
	 
	 //hacemos visible el group que contiene la grilla
	 Groupbox groupinformacion =(Groupbox) this.getFellow(idGroupHistorico);
	 groupinformacion.setVisible(true);

	 contenido = contenedorMensajes.getRows();
	 
	 //preparamos el contenedor del menasje	 	 
	 fila = new Row();
	 fila.setParent(contenido);
	 fila.setStyle("background: #FFFFF0 ");
	 //fila.setStyle("background: blue ");
     try 
     {
    	 if(labeli18n!=null){	
    		 //se agrega la llave que tiene el contenido del mensaje
    		 //mensaje = IdiomaFacade.getLabel(labeli18n);
    	 }

		 switch (estado)
		 {
		 case IConstantes.ESTADO_INSERCION_OK:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_OK);
			   
			   break;
			  }
		 case IConstantes.ESTADO_INSERCION_ERROR:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			   llave="";
			   break;
			  }	 
		 case IConstantes.ESTADO_EDICION_OK:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_OK);
			   break;
			  }
		 case IConstantes.ESTADO_EDICION_ERROR:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			   break;
			  }	 
		 case IConstantes.ESTADO_BORRAR_OK:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_OK);
			   break;
			  }	 
		 case IConstantes.ESTADO_BORRAR_ERROR:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			   break;
			  }	 
		 case IConstantes.ESTADO_DEFAULT_OK:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_OK);
			   //mensaje = IdiomaFacade.getLabel("global:mensajeEstadoDefaultOK.default.prompt");
			   llave="";
			   break;
			  }	 
		 case IConstantes.ESTADO_DEFAULT_ERROR:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			   //mensaje = IdiomaFacade.getLabel("global:mensajeEstadoDefaultERROR.default.prompt");
			   llave="";		   
			   break;
			  }		 
		 default:{
			 imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			 //mensaje = IdiomaFacade.getLabel("Estado no definido");
		  }		 
		 }//fin switch
		
        } 
        catch (Exception e) {
        	//--error del negocio...
        	//this.lanzarExcepcion(new Excepcion(IExcepcion.EXCEPCION_GENERAL,e));//TODO internacionalizar
        	e.printStackTrace();
		}		 
	 //---ponemos el mensaje
	 fila.appendChild(imagen);	 
	 fila.appendChild(new Label(mensaje + llave+" "+mensajeExt));
	}	
	
	
	/**
	 * @Class ActionStandar 
	 * @name  limpiarHistoricoMensajes
	 * @return void
	 * @desc Este m�todo se encarga de limpiar la grilla que contiene el historico de mensajes
	 * el parametro es utilizado en el metodo setMensajehistorico para que limpie la grilla 
	 * antes de escribir en ella.
	 */
	public void limpiarHistoricoMensajes(Grid grilla,String idGridHistorico,String idGroupHistorico){
		
		Groupbox groupinformacion = null;
		
		if(grilla==null){
			grilla =  (Grid) this.getFellow(idGridHistorico);
			//ocultamos el groupbox que contiene la grilla
			groupinformacion =(Groupbox) this.getFellow(idGroupHistorico); 
			groupinformacion.setVisible(false);

		}
		grilla.getRows().getChildren().clear();
		 
	}
	
	/**
	 * @type   M�todo de la clase ActionStandar
	 * @name   onConfirmacionPopup
	 * @return int
	 * @desc   muestra un popup de confirmaci�n para confirmar el borrado
	 */
	public int onConfirmacionPopup (String pantalla) {
		int resultado = 0;

		try {

			/*resultado = Messagebox.show(IdiomaFacade.getLabel("global:confirmacionborrar."+pantalla+".prompt"),
					        IdiomaFacade.getLabel("global:confirmacionborrar."+pantalla+".caption"),
					        Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION);*/
			
			resultado = Messagebox.show("Esta seguro de borrar "+pantalla+"?","Esta seguro que desea borrar el registro?",
	        Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION);

		}
		catch (Exception e) {
			//this.lanzarExcepcion(new Excepcion(IExcepcion.EXCEPCION_POR_INTERRUPCION,"",e));//TODO internacionalizar	
		}
		return resultado;
	}
	
	
	}

