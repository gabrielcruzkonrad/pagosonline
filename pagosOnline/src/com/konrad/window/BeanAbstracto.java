/**
 * BeanAbstracto.java
 */
package com.konrad.window;

import java.io.Serializable;
import java.util.List;

/**
 * @author Fabio Bar�n 
 * @date 23/01/2007
 * @name BeanAbstracto.java
 * @desc Define el comportamiento de todos los DTO del proyecto.
 */
@SuppressWarnings({  "serial" })
public abstract class BeanAbstracto implements Serializable{
	private String MD5 = null;
	
	/**
	 * @type   Constructor de la clase BeanAbstracto
	 * @name   BeanAbstracto 
	 * @return void
	 * @desc   Crea una nueva instancia de BeanAbstracto
	 */
	public BeanAbstracto()
	{
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * los objetos deber�an sobreeescribir los m�todos hashcode y equlas de la api de java
	 * para que hibernate funcione bien, si es necesario, por ejemplo en el manejo de los
	 * PersistenSet de hibernate, ( si se implementa el m�todo equals deber�a tambien implementarse
	 * el hashcode para que el modelo quede consistente)
	 * 
	 * Para implementar bien �stos m�todos se deben seguir las siguientes leyes:
	 *  
	 * Indicates whether some other object is "equal to" this one.
	 * 
	 * The equals method implements an equivalence relation: It is reflexive: for any reference 
	 * value x, x.equals(x) should return true.
	 * 
	 * It is symmetric: for any reference values x and y, x.equals(y) should return true if and 
	 * only if y.equals(x) returns true.
	 * 
	 * It is transitive: for any reference values x, y, and z, if x.equals(y) returns true and 
	 * y.equals(z) returns true, then x.equals(z) should return true.
	 * 
	 * It is consistent: for any reference values x and y, multiple invocations of x.equals(y) 
	 * consistently return true or consistently return false, provided no information used in 
	 * equals comparisons on the object is modified.
	 *
	 * For any non-null reference value x, x.equals(null) should return false.
	 * 
	 * Por ultimo tratar de implementar equals sobre los m�todos que son llaves en el modelo
	 * tipo PK's, UK's de tal manera que cada objeto se �nico
	 * 
	 * m�s informaci�n:
	 *      - HIBERNATE in Action.pdf p�gina 148
	 *      - http://www.geocities.com/technofundo/tech/java/equalhash.html
	 *      - 
	 * */
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	
	/**
	 * @type   Método de la clase BeanAbstracto
	 * @name   getMD5
	 * @return void
	 * @param MD5
	 * @desc   obtiene el valor de la propiedad mD5
	 */
	public String getMD5() {
		return MD5;
	}

	/**
	 * @type   Método de la clase BeanAbstracto
	 * @name   setMD5
	 * @return String
	 * @param MD5
	 * @desc   Actualiza el valor de la propiedad mD5
	 */
	public void setMD5(String md5) {
		MD5 = md5;
	}

	@SuppressWarnings("rawtypes")
	public abstract List getLlaveNatural();
	
}
