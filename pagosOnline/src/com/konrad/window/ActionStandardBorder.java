package com.konrad.window;

import java.util.List;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.impl.InputElement;
import com.konrad.util.IConstantes;

/*import com.casewaresa.framework.action.impl.ExcepcionPopupAction;
import com.casewaresa.framework.contract.IConstantes;
import com.casewaresa.framework.contract.IExcepcion;
import com.casewaresa.framework.dto.impl.LlaveNatural;
import com.casewaresa.framework.excepciones.impl.Excepcion;
import com.casewaresa.framework.helper.ZKProcesosComunesHelper;
import com.casewaresa.iceberg_co.action.ActionStandardBorder;*/

public abstract class ActionStandardBorder extends Borderlayout 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** desc esta es la variable [ log ] de la clase [ ActionStandar.java ]*/	
	protected static Logger log = Logger.getLogger(ActionStandardBorder.class);
	
	
	
	/**
	 * @Class ActionStandar 
	 * @name  validarFormulario
	 * @return boolean
	 * @desc Este m�todo se encarga de validar el formualario, este metodo 
	 * recibe el id del componente a validar
	 */
	public boolean validarFormulario (String idComponente) {
	
		AbstractComponent contenedorFormulario = null;
		ZKProcesosComunesHelper zkHelper = null;		
		
		//--- obtenemos el contenedor del formulario
		contenedorFormulario =(AbstractComponent) this.getFellow(idComponente);
		
		//instanciamos la clase que ayudar� en procesos comunes en los formualrio como la validaci�n 
		zkHelper = new ZKProcesosComunesHelper();
			
		//-validamos el formualrio
		return zkHelper.validarCampos(contenedorFormulario, InputElement.class);		
	}


	/**
	 * @Class ActionStandar 
	 * @name  limpiarFormulario
	 * @return boolean
	 * @desc Este m�todo se encarga de reiniciar los campos del formualrio
	 */
	public void limpiarFormulario(String idFormulario)
	{
		AbstractComponent contenedorFormulario = null;
		ZKProcesosComunesHelper zkHelper = null;		
		
		//--- obtenemos el contenedor del formulario
		contenedorFormulario =(AbstractComponent) this.getFellow(idFormulario);
		
		//instanciamos la clase que ayudar� en procesos comunes en los formualrio como la validaci�n 
		zkHelper = new ZKProcesosComunesHelper();
			
		//-validamos el formulario
		zkHelper.limpiarCampos (contenedorFormulario, AbstractComponent.class);
	}
	

	/**
	 * @Class ActionStandar 
	 * @name  limpiarFormulario
	 * @return boolean
	 * @param  componente
	 * @desc Este método se encarga de reiniciar los campos del formulario que llega como par�metro
	 */
	public void limpiarFormulario( AbstractComponent componente )
	{
		AbstractComponent contenedorFormulario = null;
		ZKProcesosComunesHelper zkHelper = null;		
		
		//--- obtenemos el contenedor del formulario seg�n el par�metro
		contenedorFormulario = componente;
		
		//instanciamos la clase que ayudar� en procesos comunes en los formualrio como la validaci�n 
		zkHelper = new ZKProcesosComunesHelper();
			
		//-validamos el formulario
		zkHelper.limpiarCampos (contenedorFormulario, AbstractComponent.class);
	}	
	
	/**
	 * @Class ActionStandar 
	 * @name  setMensajeHistorico
	 * @return void
	 * @desc Este metodo se encarga de actualizar el mensaje de informacion
	 */
	public void setMensajeHistorico(String idGridHistorico,String idGroupHistorico,int estado, String label,String llaveRegistro,String mensajeExt)
	{
	 Grid  contenedorMensajes = null;
	 Rows contenido = null;
	 Row fila = null;
	 Image imagen = null;
	   
	 String llave;
	 
	
	 if(llaveRegistro==null){
		 llave="";
	 }else{
	 llave=" [ " + llaveRegistro +" ]";
	 }
	 
	 if(mensajeExt==null){
		 mensajeExt="";
	 }
	 
	 //--- obtenemos la grilla del historico de mensajes
	 
	 contenedorMensajes =  (Grid) this.getFellow(idGridHistorico);
	 limpiarHistoricoMensajes(contenedorMensajes,idGridHistorico,idGroupHistorico);
	 
	 //hacemos visible el group que contiene la grilla
	 Groupbox groupinformacion =(Groupbox) this.getFellow(idGroupHistorico); 
	 groupinformacion.setVisible(true);

	 contenido = contenedorMensajes.getRows();
	 
	 //preparamos el contenedor del menasje	 	 
	 fila = new Row();
	 fila.setParent(contenido);
	 fila.setStyle("background: #FFFFF0 ");
     try 
     {
		 switch (estado)
		 {
		 case IConstantes.ESTADO_INSERCION_OK:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_OK);
			   
			   break;
			  }
		 case IConstantes.ESTADO_INSERCION_ERROR:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			   llave="";
			   break;
			  }	 
		 case IConstantes.ESTADO_EDICION_OK:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_OK);
			   break;
			  }
		 case IConstantes.ESTADO_EDICION_ERROR:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			   break;
			  }	 
		 case IConstantes.ESTADO_BORRAR_OK:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_OK);
			   break;
			  }	 
		 case IConstantes.ESTADO_BORRAR_ERROR:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			   break;
			  }	 
		 case IConstantes.ESTADO_DEFAULT_OK:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_OK);
			   label = IConstantes.MENSAJE_DEFAULT_OK;
			   llave="";
			   break;
			  }	 
		 case IConstantes.ESTADO_DEFAULT_ERROR:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			   label = IConstantes.MENSAJE_DEFAULT_ERROR;
			   //llave=""; //TODO: ver que no se necesite
			   break;
			  }		 
		 default:{
			 imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			 label = "Estado no definido";
		  }		 
		 }//fin switch
		
        } 
        catch (Exception e) {
        	//--error del negocio...
        	//this.lanzarExcepcion(new Excepcion(IExcepcion.EXCEPTION_GLOBAL,""));//TODO internacionalizar
		}		 
	 //---ponemos el mensaje
	 fila.appendChild(imagen);	 
	 fila.appendChild(new Label(label + llave+" "+mensajeExt));
	}	
	
	
	/**
	 * @Class ActionStandar 
	 * @name  setMensajeHistorico
	 * @return void
	 * @desc Este metodo se encarga de actualizar el mensaje de informacion
	 */
	@SuppressWarnings("rawtypes")
	public void setMensajeHistorico(String idGridHistorico,String idGroupHistorico,int estado, String label,List llavesNaturales,String mensajeExt )
	{
	 Grid  contenedorMensajes = null;
	// LlaveNatural llaveNatural = null;
	 Rows contenido = null;
	 Row fila = null;
	 Image imagen = null;
	 String llave = "";
	 
	 if(llavesNaturales != null){
		 llave = " [ ";		 
		 for(int i=0;i<llavesNaturales.size();i++){
			// llaveNatural  = (LlaveNatural)llavesNaturales.get(i);	
			 			 
			// llave = llave +llaveNatural.getLlave()+ " "+ llaveNatural.getValor();			 
			 
			 if(llavesNaturales.size()-1 > i){			
				 llave = llave +", ";
			 }			 
		 }		 
		 llave = llave+" ] ";
	}
	 
	 if(mensajeExt==null){
		 mensajeExt="";
	 }
	 
	 //--- obtenemos la grilla del historico de mensajes
	 
	 contenedorMensajes =  (Grid) this.getFellow(idGridHistorico);
	 limpiarHistoricoMensajes(contenedorMensajes,idGridHistorico,idGroupHistorico);
	 
	 //hacemos visible el group que contiene la grilla
	 Groupbox groupinformacion =(Groupbox) this.getFellow(idGroupHistorico);
	 groupinformacion.setVisible(true);

	 contenido = contenedorMensajes.getRows();
	 
	 //preparamos el contenedor del menasje	 	 
	 fila = new Row();
	 fila.setParent(contenido);
	 fila.setStyle("background: #FFFFF0 ");
	 //fila.setStyle("background: blue ");
     try 
     {

		 switch (estado)
		 {
		 case IConstantes.ESTADO_INSERCION_OK:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_OK);
			   
			   break;
			  }
		 case IConstantes.ESTADO_INSERCION_ERROR:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			   llave="";
			   break;
			  }	 
		 case IConstantes.ESTADO_EDICION_OK:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_OK);
			   break;
			  }
		 case IConstantes.ESTADO_EDICION_ERROR:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			   break;
			  }	 
		 case IConstantes.ESTADO_BORRAR_OK:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_OK);
			   break;
			  }	 
		 case IConstantes.ESTADO_BORRAR_ERROR:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			   break;
			  }	 
		 case IConstantes.ESTADO_DEFAULT_OK:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_OK);
			   label = IConstantes.MENSAJE_DEFAULT_OK;
			   llave="";
			   break;
			  }	 
		 case IConstantes.ESTADO_DEFAULT_ERROR:{
			   imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			   label = IConstantes.MENSAJE_DEFAULT_ERROR;
			   llave="";		   
			   break;
			  }		 
		 default:{
			 imagen = new Image(IConstantes.ESTADO_IMAGEN_ERROR);
			 label = "Estado no definido";
		  }		 
		 }//fin switch
		
        } 
        catch (Exception e) {
        	//--error del negocio...
        	//this.lanzarExcepcion(new Excepcion(IExcepcion.EXCEPCION_GENERAL,e));
        	e.printStackTrace();
		}		 
	 //---ponemos el mensaje
	 fila.appendChild(imagen);	 
	 fila.appendChild(new Label(label + llave+" "+mensajeExt));
	}	
	
	
	/**
	 * @Class ActionStandar 
	 * @name  limpiarHistoricoMensajes
	 * @return void
	 * @desc Este m�todo se encarga de limpiar la grilla que contiene el historico de mensajes
	 * el parametro es utilizado en el metodo setMensajehistorico para que limpie la grilla 
	 * antes de escribir en ella.
	 */
	public void limpiarHistoricoMensajes(Grid grilla,String idGridHistorico,String idGroupHistorico){
		
		Groupbox groupinformacion = null;
		
		if(grilla==null){
			grilla =  (Grid) this.getFellow(idGridHistorico);
			//ocultamos el groupbox que contiene la grilla
			groupinformacion =(Groupbox) this.getFellow(idGroupHistorico); 
			groupinformacion.setVisible(false);

		}
		grilla.getRows().getChildren().clear();
		 
	}
	
	/**
	 * @type   M�todo de la clase ActionStandar
	 * @name   onConfirmacionPopup
	 * @return int
	 * @desc   muestra un popup de confirmaci�n para confirmar el borrado
	 */
	public int onConfirmacionPopup (String pantalla ,String llave) {
		int resultado = 0;

		try {

			resultado = Messagebox.show("Esta seguro de borrar "+pantalla+" "+llave+"?","Esta seguro que desea borrar el registro?",
	        Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION);

		}
		catch (Exception e) {
			//this.lanzarExcepcion(new Excepcion(IExcepcion.EXCEPCION_POR_INTERRUPCION,"",e));//TODO internacionalizar	
		}
		return resultado;
	}
	
	/**
	 * @type   M�todo de la clase ActionStandard
	 * @name   mostrarExcepcion
	 * @return void
	 * @desc   permite lanzar un popup modal con el error ocurrido
	 */
/*	public void lanzarExcepcion (Excepcion pException){
		log.info("ejecutando [ mostrarExcepcion ]... ");

		ExcepcionPopupAction win = null;

		try {
			win = (ExcepcionPopupAction) Executions.createComponents(IExcepcion.PANTALLA_EXCEPCIONPOPUP, null, null);		
		
			//---mandamos la excepcion 
			win.setPException ( pException );
			
			//--mostramos la pantalla
			win.doModal();		
		} 
		catch (InterruptedException e) {
			this.lanzarExcepcion(new Excepcion(IExcepcion.EXCEPCION_POR_INTERRUPCION,"",e));//TODO internacionalizar
		}
	}*/
}
