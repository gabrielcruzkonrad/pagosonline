/**
 * ItemListaSeleccion.java
 */
package com.konrad.window;

import java.util.List;



//import com.casewaresa.framework.dto.BeanAbstracto;

/**
 * @author CaseWare Ingenieria
 * @date 27/04/2009
 * @name ItemTree.java
 * @desc --
 */

@SuppressWarnings({"serial"})
public class MyItemTree extends BeanAbstracto{
	private String padre 	= null;
	private String valor 	= null;
	private String etiqueta = null;
	private Integer nivel 	= null;
	private String separador =null;

	/**
	 * @type   Constructor de la clase ItemTree
	 * @name   ItemTree
	 * @param padre
	 * @param valor
	 * @param etiqueta
	 * @desc   --
	 */	
	
	public MyItemTree() {
	
	}
	
	public MyItemTree(String padre, String valor, String etiqueta, Integer nivel) {
		this.padre 		= padre;
		this.valor 		= valor;
		this.etiqueta 	= etiqueta;
		this.nivel		= nivel;
	}
	
	public MyItemTree(String padre, String valor, String etiqueta, Integer nivel,String separador) {
		this.padre 		= padre;
		this.valor 		= valor;
		this.etiqueta 	= etiqueta;
		this.nivel		= nivel;
		this.separador  = separador;
	}

	/**
	 * @type   Método de la clase ItemTree
	 * @name   getPadre
	 * @return void
	 * @param padre
	 * @desc   obtiene el valor de la propiedad padre
	 */
	public String getPadre() {
		return padre;
	}

	/**
	 * @type   Método de la clase ItemTree
	 * @name   setPadre
	 * @return String
	 * @param padre
	 * @desc   Actualiza el valor de la propiedad padre
	 */
	public void setPadre(String padre) {
		this.padre = padre;
	}

	/**
	 * @type   Método de la clase ItemTree
	 * @name   getValor
	 * @return void
	 * @param valor
	 * @desc   obtiene el valor de la propiedad valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * @type   Método de la clase ItemTree
	 * @name   setValor
	 * @return String
	 * @param valor
	 * @desc   Actualiza el valor de la propiedad valor
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	/**
	 * @type   Método de la clase ItemTree
	 * @name   getEtiqueta
	 * @return void
	 * @param etiqueta
	 * @desc   obtiene el valor de la propiedad etiqueta
	 */
	public String getEtiqueta() {
		return etiqueta;
	}

	/**
	 * @type   Método de la clase ItemTree
	 * @name   setEtiqueta
	 * @return String
	 * @param etiqueta
	 * @desc   Actualiza el valor de la propiedad etiqueta
	 */
	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	/**
	 * @type   Método de la clase MyItemTree
	 * @name   getNivel
	 * @return void
	 * @param nivel
	 * @desc   obtiene el valor de la propiedad nivel
	 */
	public Integer getNivel() {
		return nivel;
	}

	/**
	 * @type   Método de la clase MyItemTree
	 * @name   setNivel
	 * @return Integer
	 * @param nivel
	 * @desc   Actualiza el valor de la propiedad nivel
	 */
	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getLlaveNatural() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.getValor() + " - " +this.getEtiqueta();
	}

	/**
	 * @type   M�todo de la clase MyItemTree
	 * @name   getSeparador
	 * @return void
	 * @param separador
	 * @desc   obtiene el valor de la propiedad separador
	 */
	public String getSeparador() {
		return separador;
	}

	/**
	 * @type   M�todo de la clase MyItemTree
	 * @name   setSeparador
	 * @return String
	 * @param separador
	 * @desc   Actualiza el valor de la propiedad separador
	 */
	public void setSeparador(String separador) {
		this.separador = separador;
	}

}
