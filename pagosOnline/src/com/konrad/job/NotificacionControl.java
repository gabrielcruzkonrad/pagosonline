package com.konrad.job;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class NotificacionControl {
  private final ScheduledExecutorService scheduler =
    Executors.newScheduledThreadPool(1);

  public NotificacionControl(){
	  super();
  }
  
  public void runNotification() {
    final Runnable notificador = new Runnable() {
      public void run() { 
    	  NotificacionCartera notificacionCartera = new NotificacionCartera();
    	  notificacionCartera.runNotification();
      }
    };
     scheduler.scheduleAtFixedRate(notificador, 7, 1, TimeUnit.DAYS);
    /*scheduler.schedule(
    		new Runnable() {
    			public void run() { 
    					notificadorHandle.cancel(true); 
    					}
    		}, 60 * 60, TimeUnit.SECONDS);*/
  }
}