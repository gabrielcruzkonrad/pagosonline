package com.konrad.job;

import static org.quartz.DateBuilder.evenMinuteDate;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

public class VerificacionPagosCarteraJobScheduler {

	protected static Logger log = Logger.getLogger(VerificacionPagosCarteraJobScheduler.class);
	
	public VerificacionPagosCarteraJobScheduler() {
		// TODO Auto-generated constructor stub
	}
	
    // First we must get a reference to a scheduler
	public void run(){
		try{
		SchedulerFactory sf = new StdSchedulerFactory();
		Scheduler sched = sf.getScheduler();
		
		Date runTime = evenMinuteDate(new Date());
		
	    log.info("------- Scheduling Job  -------------------");

	    // define the job and tie it to our HelloJob class
	    JobDetail job = newJob(VerificacionPagosCarteraJob.class).withIdentity("job1", "group1").build();

	    // Trigger the job to run on the next round minute
	    Trigger trigger = newTrigger().withIdentity("trigger1", "group1")
	    		.withSchedule(CronScheduleBuilder.cronSchedule("0 0 * * * ? *"))
	    		.startAt(runTime)
	    		.build();

	    // Tell quartz to schedule the job using our trigger
	    sched.scheduleJob(job, trigger);
	    log.info(job.getKey() + " will run at: " + runTime);

	    // Start up the scheduler (nothing can actually run until the
	    // scheduler has been started)
	    sched.start();
		
		
		}catch(Exception e){
			e.printStackTrace();
			log.error(e.getMessage());
		}
	}

}
