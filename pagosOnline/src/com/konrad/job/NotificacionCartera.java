package com.konrad.job;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.konrad.domain.Estudiante;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.helper.ReciboConsignacionHelper;
import com.konrad.util.IConstantes;
import com.konrad.util.SendEmail;

public class NotificacionCartera {

	protected static Logger log = Logger.getLogger(NotificacionCartera.class);

	public NotificacionCartera() {
		// TODO Auto-generated constructor stub
	}

	
	public void calcularVencimientosCliente(ReciboConsignacion recibo){
		try{
			ParametrizacionFac.getFacade().ejecutarProcedimiento("calcularVencimientosCliente",recibo);
			
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}
	}
	
	public BigDecimal getInteresCorriente(ReciboConsignacion recibo){
		BigDecimal miCantidad = new BigDecimal(0d);
		Double miRetorno = 0d;
		try{
		 miRetorno= (Double)ParametrizacionFac.getFacade().obtenerRegistro("selectInteresesCorrientesCliente", recibo);
		 if(miRetorno!=null){
			 if(miRetorno > 0d){
				 miCantidad = new BigDecimal(miRetorno);
			 } else{
				 miCantidad = new BigDecimal(0d);
			 }
		 } else{
			 miCantidad = new BigDecimal(0d);
		 }
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
			miCantidad = new BigDecimal(0d);
		}
		return miCantidad;
	}
	
	public BigDecimal getInteresMora(ReciboConsignacion recibo){
		BigDecimal miCantidad = new BigDecimal(0d);
		Double miRetorno = 0d;
		try{
		miRetorno = (Double)ParametrizacionFac.getFacade().obtenerRegistro("selectInteresesMoraCliente", recibo);
		 if(miRetorno!=null){
			 if(miRetorno > 0d){
				 miCantidad = new BigDecimal(miRetorno);
			 } else{
				 miCantidad = new BigDecimal(0d);
			 }
		 } else{
			 miCantidad = new BigDecimal(0d);
		 }
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
			miCantidad = new BigDecimal(0d);
		}
		return miCantidad;
	}
	
	@SuppressWarnings("unchecked")
	public BigDecimal getSancionChequeDevuelto(ReciboConsignacion recibo){
		BigDecimal miCantidad = new BigDecimal(0d);
		Double miValor = 0d; 
		try{
			
			List<ReciboConsignacion> listaSancion = 
					(List<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("selectSancionChequeDevueltoElegible",recibo);
			
			if(listaSancion!=null){
				if(listaSancion.size()>0){
					for(ReciboConsignacion item: listaSancion){
						miValor = item.getValorDetalle();
						if(miValor!=null){
							miCantidad=miCantidad.add(new BigDecimal(miValor));
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
			miCantidad = new BigDecimal(0d);
		}
		return miCantidad;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<ReciboConsignacion> getCapital(ReciboConsignacion recibo){
		List<ReciboConsignacion> listaRecibos = new ArrayList<ReciboConsignacion>();
		try{
			listaRecibos = (List<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("selectCarteraClienteElegibles", recibo);
			
		} catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}
		
		return listaRecibos;
	}
	
	@SuppressWarnings("unchecked")
	public List<ReciboConsignacion> getCapitalElegible(Map<String,String> parametros){
		
		List<ReciboConsignacion> listaRecibos = new ArrayList<ReciboConsignacion>();
		try{
			
			listaRecibos = (List<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("selectClientesElegibles", parametros);
		} catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}
		
		return listaRecibos;
	}
	
	
	
	

	public void sendCorreoNotificacion(ReciboConsignacion recibo, String asunto, String mensaje){
		try{
		SendEmail enviarCorreo = new SendEmail();
		Map<String, String> mapaCorreo = new HashMap<String,String>();
		ParametrizacionFac.getFacade().obtenerRegistro("seleccionarDatosServidorCorreo", mapaCorreo);
		Estudiante estudiante= ReciboConsignacionHelper.getHelper().getEstudianteBasic("", recibo.getCliente().getCliente().toString(),"", "");
		String servidorCorreo = mapaCorreo.get("SERVIDOR_SMTP");
		String puertoCorreo = mapaCorreo.get("PUERTO_SMTP");
		String emisor = mapaCorreo.get("USUARIO_REMITENTE_SMTP");
		String contrasena = mapaCorreo.get("CLAVE_AUTENTICACION_SMTP");
		String authentication = mapaCorreo.get("REQUIERE_AUTENTICACION_SMTP").equalsIgnoreCase("1")?"true":"false";
		String tls = mapaCorreo.get("REQUIERE_TLS_SMTP").equalsIgnoreCase("1")?"true":"false";
		List<String> receptores = new ArrayList<String>();
		List<String> receptoresCopia = new ArrayList<String>();
		List<String> adjuntos = new ArrayList<String>();
		
		if(estudiante!= null){
			if(estudiante.getEmail() !=null){
				if(!estudiante.getEmail().equalsIgnoreCase("")){
					receptores.add(estudiante.getEmail());
				}
			}
			
			if(estudiante.getEmailOtro()!=null){
				if(!estudiante.getEmailOtro().equalsIgnoreCase("")){
					receptoresCopia.add(estudiante.getEmailOtro());
				}
			}
			
		}else{
			if(recibo.getCliente().getPersona().getDireccionElectronica()!= null){
				if(!recibo.getCliente().getPersona().getDireccionElectronica().equalsIgnoreCase("")){
					receptores.add(recibo.getCliente().getPersona().getDireccionElectronica());
				}	
			}	
			
			if(recibo.getCliente().getPersona().getPaginaWeb()!= null){
				if(!recibo.getCliente().getPersona().getPaginaWeb().equalsIgnoreCase("")){
					receptoresCopia.add(recibo.getCliente().getPersona().getPaginaWeb());
				}
			}	
		}
		
		if(receptores.size()>0){
			log.info("Correo al cliente: "+recibo.getCliente().getCliente());
			for(String cuenta: receptores){
				log.info("cuenta a enviar: "+cuenta);
			}
			enviarCorreo.envia(servidorCorreo, puertoCorreo, emisor, asunto, receptores, receptoresCopia, mensaje, adjuntos, contrasena, tls, authentication);
		}
		
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}
	}
	
	
	public void runNotification() {
		try{
			String diasNotificacion = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectDiasProximoVencimiento");
			NumberFormat formatoDecimal = new DecimalFormat("$###,###,###.00");
			Integer diasProximoVencimiento = new Integer("0");
			String asunto = IConstantes.ASUNTO_NOTIFICACION;
			String enviarProximosVencimientos = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectIndicadorProximoVencimiento");
			
			if(enviarProximosVencimientos==null) {
				enviarProximosVencimientos = "N";
			}
			
			try{
				 diasProximoVencimiento = Integer.valueOf(diasNotificacion);	
			}catch(Exception e){
				diasProximoVencimiento = 0;
			}
			
			if(diasProximoVencimiento== null){
				diasProximoVencimiento = IConstantes.DIAS_PROXIMO_VENCIMIENTO_NOTIFICACION;
			} else if (diasProximoVencimiento == 0){
				diasProximoVencimiento = IConstantes.DIAS_PROXIMO_VENCIMIENTO_NOTIFICACION;
			}
			
			
			Map<String,String> parametros = new HashMap<String,String>();
			List<ReciboConsignacion> listaRecibosElegibles  = this.getCapitalElegible(parametros);
			if(listaRecibosElegibles!=null){
				if(listaRecibosElegibles.size()>0){
				
					for(ReciboConsignacion recibo : listaRecibosElegibles){
						recibo.setIdentificador(Long.valueOf(recibo.getCliente().getCliente().toString()));
						this.calcularVencimientosCliente(recibo);
						List<ReciboConsignacion> listaRecibosCliente = this.getCapital(recibo);
						if(listaRecibosCliente!= null){
							if(listaRecibosCliente.size()>0){
								for(ReciboConsignacion cuota :listaRecibosCliente){
									
									BigDecimal valorIntereses = this.getInteresCorriente(cuota);
									BigDecimal valorMora = this.getInteresMora(cuota);
									BigDecimal sancionCheque = this.getSancionChequeDevuelto(cuota);
									
									if(cuota.getSaldoCartera().getDiasVencimiento() <=0){
										if(Math.abs(cuota.getSaldoCartera().getDiasVencimiento()) <= diasProximoVencimiento){
											if(enviarProximosVencimientos.equalsIgnoreCase("S")) {	
											String mensaje =IConstantes.MENSAJE_SALUDO_NOTIFICACION;
											mensaje = mensaje+"<p style=\"font-weight:bold; font-family:arial,verdana; \">"+recibo.getCliente().getCliente()+" - "+
													recibo.getCliente().getPersona().getNombreRazonSocial()+
													(recibo.getCliente().getPersona().getPrimerApellido()!=null?" "+recibo.getCliente().getPersona().getPrimerApellido():"")+
													(recibo.getCliente().getPersona().getSegundoApellido()!=null?" "+recibo.getCliente().getPersona().getSegundoApellido():"")+
													"</p>";
											mensaje = mensaje + IConstantes.MENSAJE_PROXIMO_VENCIMIENTO_NOTIFICACION;
											mensaje = mensaje+"<p style=\"font-weight:bold; font-family:arial,verdana;\">Le enviamos el detalle de su obligaci&oacute;n:</p>";
											mensaje =mensaje+"<ul><li><p style=\"font-family:arial,verdana;\">No. Obligaci&oacute;n: "+cuota.getReciboConsignacion()+"</p></li>";
											mensaje = mensaje +"<li><p style=\"font-family:arial,verdana;\">Fecha de Vencimiento: "+new SimpleDateFormat("dd/MM/yyyy").format(cuota.getPeriodo().getFechaVencimiento())+"</p></li>";
											mensaje = mensaje + "<li><p style=\"font-family:arial,verdana;\">Intereses corrientes: "+formatoDecimal.format(valorIntereses.doubleValue())+"</p></li>";
											mensaje = mensaje + "<li><p style=\"font-family:arial,verdana;\">Intereses de mora: "+formatoDecimal.format(valorMora.doubleValue())+"</p></li>";
											if(cuota.getSaldoCartera().getConcepto().equalsIgnoreCase(IConstantes.CONCEPTO_CAPITAL_NOTIFICACION)
													|| cuota.getSaldoCartera().getConcepto().equalsIgnoreCase(IConstantes.CONCEPTO_OTRO_NOTIFICACION)
													||cuota.getSaldoCartera().getConcepto().equalsIgnoreCase(IConstantes.CONCEPTO_CAPITAL_REC_CAST)
													||cuota.getSaldoCartera().getConcepto().equalsIgnoreCase(IConstantes.CONCEPTO_CAPITAL_REC_CAST_INT_PROV)
													||cuota.getSaldoCartera().getConcepto().equalsIgnoreCase(IConstantes.CONCEPTO_CAPITAL_REC_PROV)
													){
												
												mensaje = mensaje + "<li><p style=\"font-family:arial,verdana;\">Capital: "+formatoDecimal.format(cuota.getValorDetalle())+"</p></li>";	
											}
											if(cuota.getSaldoCartera().getConcepto().equalsIgnoreCase(IConstantes.CONCEPTO_CHEQUE_DEVUELTO_NOTIFICACION)){
												mensaje = mensaje + "<li><p style=\"font-family:arial,verdana;\">Cheque devuelto: "+formatoDecimal.format(cuota.getValorDetalle())+"</p></li>";
												mensaje = mensaje + "<li><p style=\"font-family:arial,verdana;\">Sanci&oacute;n cheque devuelto: "+formatoDecimal.format(sancionCheque)+"</p></li></ul>";
											}
											
											
											mensaje = mensaje + IConstantes.MENSAJE_PAGO_NOTIFICACION;
											mensaje = mensaje + IConstantes.MENSAJE_FINALIZACION_NOTIFICACION;
											mensaje = mensaje + IConstantes.PIE_PAGINA_NOTIFICACION;
											
											log.info(mensaje);
											System.out.println(mensaje);
											this.sendCorreoNotificacion(cuota, asunto, mensaje);
											}
										}
									} else{

										String mensaje =IConstantes.MENSAJE_SALUDO_NOTIFICACION;
										mensaje = mensaje+"<p style=\"font-weight:bold; font-family:arial,verdana; \">"+recibo.getCliente().getCliente()+" - "+
												recibo.getCliente().getPersona().getNombreRazonSocial()+
												(recibo.getCliente().getPersona().getPrimerApellido()!=null?" "+recibo.getCliente().getPersona().getPrimerApellido():"")+
												(recibo.getCliente().getPersona().getSegundoApellido()!=null?" "+recibo.getCliente().getPersona().getSegundoApellido():"")+
												"</p>";
										mensaje = mensaje+IConstantes.MENSAJE_VENCIDO_RECIBO_NOTIFICACION;
										mensaje = mensaje+"<p style=\"font-weight:bold; font-family:arial,verdana;\">Le enviamos el detalle de su obligaci&oacute;n:</p>";
										mensaje =mensaje+"<ul><li><p style=\"font-family:arial,verdana;\">No. Obligaci&oacute;n: "+cuota.getReciboConsignacion()+"</p></li>";
										mensaje = mensaje +"<li><p style=\"font-family:arial,verdana;\">Fecha de Vencimiento: "+new SimpleDateFormat("dd/MM/yyyy").format(cuota.getPeriodo().getFechaVencimiento())+"</p></li>";
										mensaje = mensaje+"<li><p style=\"font-family:arial,verdana;\">Intereses Corrientes: "+formatoDecimal.format(valorIntereses.doubleValue())+"</p></li>";
										mensaje = mensaje+"<li><p style=\"font-family:arial,verdana;\">Intereses de Mora: "+formatoDecimal.format(valorMora.doubleValue())+"</p></li>";
										
										
										if(cuota.getSaldoCartera().getConcepto().equalsIgnoreCase(IConstantes.CONCEPTO_CAPITAL_NOTIFICACION)
												|| cuota.getSaldoCartera().getConcepto().equalsIgnoreCase(IConstantes.CONCEPTO_OTRO_NOTIFICACION)){
											mensaje = mensaje + "<li><p style=\"font-family:arial,verdana;\">Capital: "+formatoDecimal.format(cuota.getValorDetalle())+"</p></li>";	
										}
										if(cuota.getSaldoCartera().getConcepto().equalsIgnoreCase(IConstantes.CONCEPTO_CHEQUE_DEVUELTO_NOTIFICACION)){
											mensaje = mensaje + "<li><p style=\"font-family:arial,verdana;\">Cheque devuelto: "+formatoDecimal.format(cuota.getValorDetalle())+"</p></li>";
											mensaje = mensaje + "<li><p style=\"font-family:arial,verdana;\">Sanci&oacute;n cheque devuelto: "+formatoDecimal.format(sancionCheque)+"</p></li></ul>";
										}
										
										
										mensaje = mensaje + IConstantes.MENSAJE_PAGO_NOTIFICACION;
										mensaje = mensaje + IConstantes.MENSAJE_FINALIZACION_NOTIFICACION;
										mensaje = mensaje + IConstantes.PIE_PAGINA_NOTIFICACION;
										log.info(mensaje);
										System.out.println(mensaje);
										
										this.sendCorreoNotificacion(cuota, asunto, mensaje);
									}
								}
							}
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
		}
	}
	
}
