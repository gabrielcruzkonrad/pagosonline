package com.konrad.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class VerificacionPagosCarteraJob implements Job{

	public VerificacionPagosCarteraJob() {
		// TODO Auto-generated constructor stub
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		VerificacionPagosCartera verificacionPagosCartera = VerificacionPagosCartera.getInstance();
		verificacionPagosCartera.verificarTransaccionesPagosPendientes();
	}
	
}
