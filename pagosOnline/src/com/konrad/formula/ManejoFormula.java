package com.konrad.formula;

import java.util.ArrayList;


public class ManejoFormula {
	
	public ArrayList<String> getVaribles(String formula) {
		ArrayList<String> arregloVariables= new ArrayList<String>();
		String variable = "";
		int evalucionCaracter = 0;
		
		for (int x = 0; x < formula.length(); x++) {

			evalucionCaracter = tipoCaracter(formula.charAt(x));
			
			switch (evalucionCaracter) {
			case 1   : {
						variable += formula.charAt(x);
						break;
					   }
			case 100 : {
						variable = "";
						break;
					   }
			case 101:  {
				         arregloVariables.add(variable);
					   }
			}
		}
		return arregloVariables;
	}

	private static int tipoCaracter(char x) {
		int i = -1;
		switch (x) {
		case '(':
			i = 0;
			break;
		case ')':
			i = 0;
			break;
		case '[':
			i = 0;
			break;
		case ']':
			i = 0;
			break;
		case '+':
			i = 0;
			break;
		case '-':
			i = 0;
			break;
		case '*':
			i = 0;
			break;
		case '/':
			i = 0;
			break;
		case '^':
			i = 0;
			break;
		case '�':
			i = 0;
			break;
		case '!':
			i = 0;
			break;
		case '@':
			i = 0;
			break;
		case ',':
			i = 0;
			break;
		case '{':
			i = 100;
			break;
		case '}':
			i = 101;
			break;
		default:
			i = 1;
		}
		return i;
	}

}
