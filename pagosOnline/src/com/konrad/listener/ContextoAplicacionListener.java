package com.konrad.listener;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.konrad.dao.ConfiguradorIbatis;
import com.konrad.helper.AppContext;
import com.konrad.helper.ReciboConsignacionHelper;
import com.konrad.job.VerificacionPagosCarteraJobScheduler;

/**
 * Application Lifecycle Listener implementation class
 * ContextoAplicacionListener
 * 
 */
public class ContextoAplicacionListener implements ServletContextListener {

    /**
     * Propiedad rutaContexto de la clase [ ContextoAplicacionListener.java ]
     * 
     * @desc: guarda ela ruta física del contexto de la aplicación
     */
	protected static Logger log = Logger.getLogger(ContextoAplicacionListener.class);
    private String rutaContexto = "";
	private String contextPath;
	private Properties appProperties;

    /**
     * Default constructor.
     */
    public ContextoAplicacionListener() {
	// TODO Auto-generated constructor stub
    }

    /**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0) {
	try {
	    // TODO Auto-generated method stub

	    // --cargamos la configuración de Ibatis
		log.info("Empieza arranque de aplicación");
	    ConfiguradorIbatis.getInstance().configurar("desarrollo");

	    // hallamos la ruta del contexto de la aplicación
	    this.rutaContexto = arg0.getServletContext().getRealPath("/");

	    // guardamos esta ruta para usarla en otros módulos
	    ContextoAplicacion pContextoAplicacion = ContextoAplicacion
		    .getInstance();
	    pContextoAplicacion.setRutaContexto(this.rutaContexto);
	    
	    
	    this.contextPath = arg0.getServletContext().getRealPath(File.separator);
		this.appProperties = this.getProperties(this.contextPath + File.separator +"WEB-INF" + File.separator + "classes" + File.separator + "Aplicacion.properties");
		
		AppContext appContext = AppContext.getInstance(); 
		appContext.setContextPath(contextPath);
		appContext.setAppProperties(appProperties);

	    	// arrancamos el demonio de verificación de los pagos
	    	/*if(!RecibosDaemon.getInstance().isAlive()){
	    		RecibosDaemon.getInstance().setDaemon(true);
	    		RecibosDaemon.getInstance().setPriority(Thread.MAX_PRIORITY);
	    		RecibosDaemon.getInstance().start();
	    	}*/
	    	
	    	// arrancamos demonio de recuperación de tasas de cambio
	    	
	    	/*if(!ConversionDaemon.getInstance().isAlive()){
	    		ConversionDaemon.getInstance().setDaemon(true);
	    		ConversionDaemon.getInstance().setPriority(Thread.MIN_PRIORITY);
	    		ConversionDaemon.getInstance().start();
	    	}*/
	    	
	    	//Se fija la variable para saber que servicios invocar (Pruebas/Producción) si es falso es pruebas sino es producción
	    	ReciboConsignacionHelper.getHelper().setServicioPruebasProduccion(true);
	    	String ejecutarJobVerificacion = this.getAppProperties().getProperty("ap.ejecucionJobVerificacionPagos");
	    	
	    	if(ejecutarJobVerificacion.equalsIgnoreCase("S")) {
	    		VerificacionPagosCarteraJobScheduler verificacionPagosSch = new VerificacionPagosCarteraJobScheduler();
	    		verificacionPagosSch.run();
	    	}
	    	

	} catch (Exception e) {

	    log.error(e);
	}

    }
    
    public Properties getProperties(String pathProperties){
    	Properties properties;
	    try {     
	    	FileInputStream file = new FileInputStream(pathProperties);
	        properties = new Properties();
	        properties.load(file);
	        file.close();
	        
	        return properties;
	    } catch (FileNotFoundException e) {
	    	e.printStackTrace();
	    	log.error(e);
	    	return null;
	    } catch (IOException e) {
	    	e.printStackTrace();
	    	log.error(e);
	    	return null;
	    }
    } 

    public String getContextPath() {
		return contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	public Properties getAppProperties() {
		return appProperties;
	}

	public void setAppProperties(Properties appProperties) {
		this.appProperties = appProperties;
	}

	public void contextDestroyed(ServletContextEvent arg0) {
	// TODO Auto-generated method stub

    }
}
