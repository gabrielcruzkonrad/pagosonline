package com.konrad.assembler;

import java.text.DecimalFormat;
import java.util.regex.Pattern;

import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zul.A;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Vbox;

import com.konrad.action.ShoppingCartAction;
import com.konrad.domain.DetalleOrden;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.helper.ReciboConsignacionHelper;
import com.konrad.util.IConstantes;
import com.konrad.window.IAssemblerStandard;

public class ShoppingCartAssembler implements IAssemblerStandard {

	@Override
	public Row crearRowDesdeDto(Object objetoFuente,
			AbstractComponent componente) {
		
		return null;
	}

	@Override
	public Object crearDtoDesdeRow(AbstractComponent componente) {
		
		return null;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Listitem crearListItemOrdenDesdeDto(Object objetoFuente, AbstractComponent componente){
		
		Listitem item = new Listitem();
		try {
			final ReciboConsignacion recibo = (ReciboConsignacion)objetoFuente;
			final ShoppingCartAction ventana = (ShoppingCartAction) componente;
			item.setAttribute("RECIBO_CONSIGNACION", recibo);
			
			Label label = new Label();
			
			// orden (documento - numero)
			label.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
			Listcell celda = new Listcell();
			label.setValue(recibo.getOrden().getDocumento()+"-"+recibo.getOrden().getOrden());
			celda.appendChild(label);
			item.appendChild(celda);
			
			label = new Label();
			
			// descripción
			label.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
			celda = new Listcell();
			label.setValue(recibo.getObservaciones());
			celda.appendChild(label);
			item.appendChild(celda);
			
			label = new Label();
			
			// valor total
			label.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
			celda = new Listcell();
			label.setValue(new DecimalFormat("'$'###,###,###.##").format(recibo.getValorDetalle()));
			celda.appendChild(label);
			item.appendChild(celda);
			
			// imprimir
			A anclaVisual = new A();
			anclaVisual.setIconSclass("z-icon-print");
			anclaVisual.addEventListener(Events.ON_CLICK, new EventListener() {
			      public void onEvent(Event arg0) throws Exception {
			       //Metodo imprimir orden pedido
			    	  ventana.onImprimirRecibo(recibo);
			    	 
			      }
			    });
			if(recibo.getNumeroFila()>1 
					|| recibo.getPeriodo().getFechaVencimiento().compareTo(ventana.getFechaSistema())<0) {
				anclaVisual.setDisabled(true);
			}
			celda =new Listcell();
			celda.appendChild(anclaVisual);
			item.appendChild(celda);
			
			// pagar
			anclaVisual = new A();
			anclaVisual.setIconSclass("z-icon-credit-card");

			anclaVisual.addEventListener(Events.ON_CLICK, new EventListener() {
			      public void onEvent(Event arg0) throws Exception {
			       //Metod pagar orden de pedido
			    	  ventana.onPagarEnLineaPayUW(recibo);			    	 
			      }
			    });
			
			if(recibo.getNumeroFila()>1 
					|| recibo.getPeriodo().getFechaVencimiento().compareTo(ventana.getFechaSistema())<0
					|| !ReciboConsignacionHelper.getHelper().validatePaymentDate()) {
				anclaVisual.setDisabled(true);
			}
			celda =new Listcell();
			celda.appendChild(anclaVisual);
			item.appendChild(celda);
			
			// anular
			anclaVisual = new A();
			anclaVisual.setIconSclass("z-icon-trash");
			anclaVisual.addEventListener(Events.ON_CLICK, new EventListener() {
			      public void onEvent(Event arg0) throws Exception {
			       //Medodo anular orden de pedido
			    	  ventana.onAnularOrden(recibo);
			    	 
			      }
			    });
			if(recibo.getNumeroFila()>1 
					|| recibo.getPeriodo().getFechaVencimiento().compareTo(ventana.getFechaSistema())<0) {
				anclaVisual.setDisabled(true);
			}
			celda =new Listcell();
			celda.appendChild(anclaVisual);
			item.appendChild(celda);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return item;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Listitem crearListItemDesdeDto(Object objetoFuente, AbstractComponent componente){
		Listitem item = new Listitem();
		try{
		final DetalleOrden detalleOrden = (DetalleOrden)objetoFuente;
		final Integer hashCode = Executions.getCurrent().getSession().hashCode(); 
		final ShoppingCartAction ventana = (ShoppingCartAction) componente;
		Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
		
		item.setAttribute("DETALLE_ORDEN", detalleOrden);
		item.setId("shoppingCartListItem"+hashCode+detalleOrden.getLinea());
		Label label = new Label();
		
		// código de referencia
		label.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
		Listcell celda = new Listcell();
		label.setValue(detalleOrden.getReferencia().toString());
		celda.appendChild(label);
		item.appendChild(celda);
		
		// descripción de producto
		label = new Label();
		label.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
		label.setValue(detalleOrden.getNombreReferencia());
		Label label2 = new Label();
		label2.setStyle(IConstantes.STYLE_TEXTO_SECUNDARIO_APLICACION);
		label2.setValue(detalleOrden.getDescripcion()!=null?detalleOrden.getDescripcion():"");
		Vbox vbox = new Vbox();
		vbox.appendChild(label2);
		celda = new Listcell();
		celda.appendChild(label);
		celda.appendChild(vbox);
		item.appendChild(celda);
		
		//valor unitario (precio)
		label = new Label();
		label.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
		label.setValue(new DecimalFormat("'$'###,###,###.##").format(detalleOrden.getPrecio()));
		celda =new Listcell();
		celda.appendChild(label);
		item.appendChild(celda);
		
		//valor iva
		label = new Label();
		label.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
		label.setValue(new DecimalFormat("'$'###,###,###.##").format(detalleOrden.getIva()));
		celda =new Listcell();
		celda.appendChild(label);
		item.appendChild(celda);
		
		//valor descuento
		label = new Label();
		label.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
		label.setValue(new DecimalFormat("'$'###,###,###.##").format(detalleOrden.getDescuento()));
		celda =new Listcell();
		celda.appendChild(label);
		item.appendChild(celda);
		
		// cantidad 
		Intbox intboxCantidad = new Intbox();
		intboxCantidad.setValue(1);
		intboxCantidad.setId("shoppingCartIntbox"+hashCode+detalleOrden.getLinea());
		intboxCantidad.setConstraint("no empty; no negative; no zero");
		intboxCantidad.setDisabled(true);
		intboxCantidad.setClass("form-control");
		intboxCantidad.setMaxlength(2);
		intboxCantidad.setStyle("min-width:43px; max-width:45px;");
		intboxCantidad.addEventListener(Events.ON_CHANGE, new EventListener() {
		      public void onEvent(Event arg0) throws Exception {
		       //Medodo ver mas
		    	  if(((InputEvent)arg0).getValue()!=null 
			    			 && pattern.matcher(((InputEvent)arg0).getValue()).matches() ) { 
		    		  ventana.onSelectListItem();
		    	  } else {
		    		  intboxCantidad.setValue(1);
		    	  }
		      }
		    });
		intboxCantidad.addEventListener(Events.ON_CHANGING, new EventListener() {
		      public void onEvent(Event arg0) throws Exception {
		       //Medodo ver mas 
		    	 if(((InputEvent)arg0).getValue()!=null 
		    			 && pattern.matcher(((InputEvent)arg0).getValue()).matches() ) { 
		    		 intboxCantidad.setValue(Integer.valueOf(((InputEvent)arg0).getValue()));
		    	 }else {
		    		 intboxCantidad.setValue(1);
		    	 }
		    	 ventana.onSelectListItem();
		      }
		    });
		celda = new Listcell();
		celda.appendChild(intboxCantidad);
		item.appendChild(celda);
		
		
		//subtotal
		Double subTotal = (detalleOrden.getPrecio()*detalleOrden.getCantidad());
		Double subIva = (subTotal*(detalleOrden.getIva()/100));
		Double subDescuento = (subTotal*(detalleOrden.getDescuento()/100));
		subTotal = subTotal + subIva - subDescuento;
		label = new Label();
		label.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
		label.setValue(new DecimalFormat("'$'###,###,###.##").format(subTotal));
		
		celda =new Listcell();
		celda.appendChild(label);
		item.appendChild(celda);
		
		// imagen
		//Image image = new Image();
		A anclaVisual = new A();
		anclaVisual.setIconSclass("z-icon-eye");
		//image.setSrc("/imagenes/book_48.png");
		anclaVisual.addEventListener(Events.ON_CLICK, new EventListener() {
		      public void onEvent(Event arg0) throws Exception {
		       //Medodo visualizar producto
		    	  if(detalleOrden.getAtributoProducto() != null &&
		    			  detalleOrden.getAtributoProducto().getFotoProducto()!=null) {
		    		  ventana.onImprimirProducto(detalleOrden);
		    	  }else {
		    		  Messagebox.show("Previsualización no disponible");  
		    	  }
		      }
		    });
		celda =new Listcell();
		celda.appendChild(anclaVisual);
		item.appendChild(celda);
		
		
		
		} catch(Exception e){
			e.printStackTrace();
		}
		
		
		
		return item;
	}

}
