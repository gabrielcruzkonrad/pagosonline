package com.konrad.assembler;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.A;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;

import com.konrad.action.CustomerListAction;
import com.konrad.domain.Usuario;
import com.konrad.util.IConstantes;
import com.konrad.window.IAssemblerStandard;

public class UserListAssembler implements IAssemblerStandard { 

	protected static Logger log = Logger.getLogger(UserListAssembler.class);
	
public Object crearDtoDesdeRow(AbstractComponent componente) {
	// TODO Auto-generated method stub
	return null;
}

@SuppressWarnings({ "rawtypes", "unchecked" })
public Row crearRowDesdeDto(Object objetoFuente, AbstractComponent componente) {
	Row fila=new Row();

	final CustomerListAction ventana =(CustomerListAction)componente; 
	final Usuario usuario = (Usuario)objetoFuente;


    Label labelAux = new Label();
	labelAux.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
	labelAux.setValue(usuario.getUsuario().toString());
	fila.appendChild(labelAux);
	labelAux = new Label();
	labelAux.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
	labelAux.setValue(usuario.getNombres());
	fila.appendChild(labelAux);
	labelAux = new Label();
	labelAux.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
	labelAux.setValue(usuario.getDescripcion());
	fila.appendChild(labelAux);


	A anclaVisual = new A();
	anclaVisual.setIconSclass("z-icon-check");
	anclaVisual.addEventListener(Events.ON_CLICK, new EventListener() {
	      public void onEvent(Event arg0) throws Exception {
	       //Metodo imprimir orden pedido
	    	 ventana.seleccionarUsuario(usuario);
	      }
	    });
		
	Hbox celda =new Hbox();
	celda.appendChild(anclaVisual);
	fila.appendChild(celda);

	return fila;

}	

}

