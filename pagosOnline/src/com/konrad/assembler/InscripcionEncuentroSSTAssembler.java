package com.konrad.assembler;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zul.Label;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Row;

import com.konrad.action.RecibirInscripcionSSTAction;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.TasaRepresentativaMercado;
import com.konrad.util.IConstantes;
import com.konrad.util.Moneda;
import com.konrad.window.IAssemblerStandard;

public class InscripcionEncuentroSSTAssembler implements IAssemblerStandard { 

	protected static Logger log = Logger.getLogger(InscripcionEncuentroAssembler.class);
	
public Object crearDtoDesdeRow(AbstractComponent componente) {
	// TODO Auto-generated method stub
	return null;
}


public Row crearRowDesdeDto(Object objetoFuente, AbstractComponent componente) {
	Row fila=new Row();
	final ReciboConsignacion reciboConsignacion = (ReciboConsignacion)objetoFuente;
	final Long reciboConsignacionId = reciboConsignacion.getReciboConsignacion(); 
	final RecibirInscripcionSSTAction ventana =(RecibirInscripcionSSTAction)componente; 
	
	Radiogroup radioGroup = (Radiogroup)ventana.getFellow("idRadioGroupInscripcionEncuentroSST");
	Radio radio = new Radio();
	log.info("porcentaje "+reciboConsignacion.getPeriodo().getPorcentaje());
	radio.setId("idRadioButtonReciboEncuentroSST"+reciboConsignacionId.toString()+reciboConsignacion.getNumeroFila());
	
	TasaRepresentativaMercado tasaIncluir = new TasaRepresentativaMercado();
	Moneda monedaIncluir= new Moneda();
	BigDecimal cantidadRedondear;
	
		monedaIncluir.setCodigo(IConstantes.PESOS_COLOMBIANOS);
		tasaIncluir.setMoneda(monedaIncluir);
		cantidadRedondear =new BigDecimal(reciboConsignacion.getValorDetalle().doubleValue());
		tasaIncluir.setValor(cantidadRedondear.setScale(0,RoundingMode.CEILING).doubleValue());
		tasaIncluir.setFecha(new Date());
		reciboConsignacion.setTasaRepresentativa(tasaIncluir);

	radio.setAttribute("RECIBO_CONSIGNACION", reciboConsignacion);
	if (reciboConsignacion.getNumeroFila().intValue()>1){
		radio.setDisabled(true);
	}else{
		radio.setDisabled(false);
	}
	radio.setRadiogroup(radioGroup);
	SimpleDateFormat df = new SimpleDateFormat(IConstantes.FORMATO_FECHA);
    DecimalFormat decimalFormat = new DecimalFormat("'$'###,###.##");
    fila.appendChild(radio);
    radio.setParent(fila);
    Label labelAux = new Label();
	labelAux.setValue(reciboConsignacion.getReciboConsignacion().toString());
	fila.appendChild(labelAux);
	
	
	labelAux = new Label();
	labelAux.setValue(reciboConsignacion.getObservaciones());
	fila.appendChild(labelAux);
	
	labelAux = new Label();
	labelAux.setValue(df.format(reciboConsignacion.getPeriodo().getFechaVencimiento()));
	fila.appendChild(labelAux);
	
	
	Double monedaConvertir = reciboConsignacion.getValorDetalle().doubleValue();
	
	
			labelAux = new Label();
			labelAux.setValue(decimalFormat.format(new BigDecimal(monedaConvertir).setScale(0,RoundingMode.CEILING)));
			fila.appendChild(labelAux);

	return fila;

	}	

}

