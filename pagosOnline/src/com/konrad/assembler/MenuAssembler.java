/**
 * MenuAssembler.java
 */
package com.konrad.assembler;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;

import com.konrad.util.IConstantes;


public class MenuAssembler{
	
	protected static Logger log = Logger.getLogger(MenuAssembler.class);	
	
	
	/**
	 * @type   Metodo de la clase IPMenuAssembler
	 * @name   construirOpcionesMenu
	 * @return void
	 * @param pComponente
	 * @param rol
	 * @throws Exception
	 * @desc   Construye las opciones de menu
	 */
	
	public void construirOpcionesMenu(Component pComponente,String rol)throws Exception
	{
		log.info("Ejecutando.. construirOpcionesMenu ");
		Tree menu = null;
		
		//linea no usada porque el menu esta estatico
		//opcionesMenu = (List)ParametrizacionFac.getFacade().obtenerListado("getOpcionesMenuRol",rol );
		menu = (Tree)pComponente.getFellow("idZMENTreeMenuOpciones");
		//linea no usada porque el menu esta estatico
		//treeFacade.getFacade().buildTreeOpcionesMenu(menu, opcionesMenu);
		
		//Se agrega la opcion de cerrar todos
		menu.getTreechildren().appendChild(treeItem( IConstantes.ID_CERRAR_TODOS,"Cerrar pestañas"));
		//Se agrega la opcion de salir
	    menu.getTreechildren().appendChild(treeItem( IConstantes.ID_SALIR,"Salir"));
	}
	
	/**
	 * @type   Metodo de la clase IPMenuAssembler
	 * @name   treeitem
	 * @return Treeitem
	 * @param id
	 * @param texto
	 * @desc   Construye una opcion de menu
	 */

	private Treeitem treeItem(String id, String texto){
		log.info("Ejecutando.. treeItem("+id+", "+texto+") ");
		Treeitem opcion = new Treeitem();
		Treerow filaMenu = new Treerow();
		Treecell celdaMenu = new Treecell();

	    opcion.setId(id);
	    opcion.setValue(id);
	    celdaMenu.setLabel(texto);
	    celdaMenu.setStyle("font-weight:bold;");
	    
	    filaMenu.appendChild(celdaMenu);
	    opcion.appendChild(filaMenu);
		return opcion;
	}
}