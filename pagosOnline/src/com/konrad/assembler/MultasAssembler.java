package com.konrad.assembler;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Row;

import com.konrad.domain.ReciboConsignacion;
import com.konrad.util.IConstantes;
import com.konrad.window.IAssemblerStandard;

public class MultasAssembler implements IAssemblerStandard {

	@Override
	public Row crearRowDesdeDto(Object objetoFuente,
			AbstractComponent componente) {
		
		return null;
	}

	@Override
	public Object crearDtoDesdeRow(AbstractComponent componente) {
		
		return null;
	}
	
	public Listitem crearListItemDesdeDto(Object objetoFuente, AbstractComponent componente){
		Listitem item = new Listitem();
		try{
		final ReciboConsignacion reciboConsignacion = (ReciboConsignacion)objetoFuente;
		
		final Long reciboConsignacionId = reciboConsignacion.getReciboConsignacion(); 
		

		
		item.setAttribute("RECIBO_CONSIGNACION", reciboConsignacion);
		item.setId(reciboConsignacion.getSaldoCartera().getDocumento()+reciboConsignacionId);
		Label label = new Label();
		
		// c�digo de documento
		label.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
		Listcell celda = new Listcell();
		label.setValue(reciboConsignacion.getSaldoCartera().getDocumento());
		celda.appendChild(label);
		item.appendChild(celda);
		
		// numero de cr�dito
		label = new Label();
		label.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
		label.setValue(reciboConsignacion.getSaldoCartera().getNumeroCredito().toString());
		celda = new Listcell();
		celda.appendChild(label);
		item.appendChild(celda);
		//observaciones
		label = new Label();
		label.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
		label.setValue(reciboConsignacion.getObservaciones());
		celda =new Listcell();
		celda.appendChild(label);
		item.appendChild(celda);
		
		// descripcion
		label = new Label();
		label.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
		label.setValue(reciboConsignacion.getSaldoCartera().getDescripcion());
		celda =new Listcell();
		celda.appendChild(label);
		item.appendChild(celda);
		
		// fecha Vencimiento
		label = new Label();
		label.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
		label.setValue(new SimpleDateFormat(IConstantes.FORMATO_FECHA).format(reciboConsignacion.getPeriodo().getFechaVencimiento()));
		celda =new Listcell();
		celda.appendChild(label);
		item.appendChild(celda);
		
		//Periodo 
		label = new Label();
		label.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
		label.setValue(reciboConsignacion.getPeriodo().getPeriodo().getPeriodo());
		celda = new Listcell();
		celda.appendChild(label);
		item.appendChild(celda);
		
		// valor doc. -- cambiar en caso de encontrar otra moneda
		label = new Label();
		label.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
		label.setValue(new DecimalFormat("'$'###,###,###.##").format(reciboConsignacion.getValorTotal()));
		celda = new Listcell();
		celda.appendChild(label);
		item.appendChild(celda);
		
		// valor saldo -- cambiar en caso de encontrar otra moneda
		label = new Label();
		label.setStyle(IConstantes.STYLE_TEXTO_APLICACION);
		celda = new Listcell();
		label = new Label();
		label.setStyle( IConstantes.STYLE_TEXTO_APLICACION+"font-weight:bold;");
		label.setValue(new DecimalFormat("'$'###,###,###.##").format(reciboConsignacion.getValorDetalle()));
		celda.appendChild(label);
		item.appendChild(celda);
		
		} catch(Exception e){
			e.printStackTrace();
		}
		
		
		
		return item;
	}

}
