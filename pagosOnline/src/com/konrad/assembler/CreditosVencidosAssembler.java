package com.konrad.assembler;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Row;

import com.konrad.action.CreditosVencidosAction;
import com.konrad.action.UsuarioAction;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.TasaRepresentativaMercado;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.util.IConstantes;
import com.konrad.util.Moneda;
import com.konrad.window.IAssemblerStandard;

public class CreditosVencidosAssembler implements IAssemblerStandard { 

	protected static Logger log = Logger.getLogger(UsuarioAction.class);
	
public Object crearDtoDesdeRow(AbstractComponent componente) {
	// TODO Auto-generated method stub
	return null;
}
@SuppressWarnings("rawtypes")
public Row crearRowDesdeDto(Object objetoFuente, AbstractComponent componente) {
	
	Row fila=new Row();
	try{
	final ReciboConsignacion reciboConsignacion = (ReciboConsignacion)objetoFuente;
	
	final Long reciboConsignacionId = reciboConsignacion.getReciboConsignacion(); 
	
	final CreditosVencidosAction ventana =(CreditosVencidosAction)componente; 
	reciboConsignacion.setIdentificador(Long.valueOf(ventana.getDesktop().getSession().hashCode()));
	Double saldoActual = (Double)ParametrizacionFac.getFacade().obtenerRegistro("selectValorReciboVencido", reciboConsignacion);
	Radiogroup radioGroup = (Radiogroup)ventana.getFellow("idRadioGroupCreditosVencidos");
	Radio radio = new Radio();
	log.info("porcentaje: "+reciboConsignacion.getPeriodo().getPorcentaje());
	radio.setId("idRadioButtonReciboVencidos"+reciboConsignacionId.toString()+reciboConsignacion.getNumeroFila());
	
	TasaRepresentativaMercado tasaIncluir = new TasaRepresentativaMercado();
	TasaRepresentativaMercado tasaIncluirVencido = new TasaRepresentativaMercado();
	Moneda monedaIncluir= new Moneda();
	
	Listbox listaMonedaIncluir= (Listbox)ventana.getFellow("idLbxCreditosVencidos");
	Listitem itemSeleccionadoIncluir = listaMonedaIncluir.getSelectedItem();
	BigDecimal cantidadRedondear;
	BigDecimal cantidadRedondearVencido;
	
	if(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)){
		monedaIncluir.setCodigo(IConstantes.PESOS_COLOMBIANOS);
		
		tasaIncluir.setMoneda(monedaIncluir);
		tasaIncluirVencido.setMoneda(monedaIncluir);
		cantidadRedondearVencido = new BigDecimal(saldoActual);
		cantidadRedondear =new BigDecimal(reciboConsignacion.getValorDetalle().doubleValue());
		tasaIncluirVencido.setValor(cantidadRedondearVencido.setScale(0,RoundingMode.CEILING).doubleValue());
		tasaIncluir.setValor(cantidadRedondear.setScale(0,RoundingMode.CEILING).doubleValue());
		
		tasaIncluir.setFecha(new Date());
		tasaIncluirVencido.setFecha(new Date());
		reciboConsignacion.setTasaRepresentativa(tasaIncluir);
		reciboConsignacion.setTasaRepresentativaVencido(tasaIncluirVencido);
		reciboConsignacion.setValorVencido(tasaIncluirVencido.getValor());
	
	}else{ //Entonces se hace el c�lculo con los USD
		if(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)){
			
			monedaIncluir.setCodigo(IConstantes.DOLARES_AMERICANOS);
			tasaIncluir.setMoneda(monedaIncluir);
			tasaIncluirVencido.setMoneda(monedaIncluir);
			//Se encuentra la comisi�n para restar a la TRM
			BigDecimal comisionPesos = new BigDecimal((String)((HashMap)ventana.getDesktop().getSession().
					getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.COMISION_DOLAR)); 
			//Si es en d�lares se realiza el c�lculo asi (valor total del recibo / (trm-comision))
			//Para la parte de recibos vencidos se hace la conversi�n con el recibo calculado a la fecha
			cantidadRedondearVencido = new BigDecimal(saldoActual 
					/ (((TasaRepresentativaMercado)((HashMap)ventana.getDesktop().getSession().
					getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor()-comisionPesos.doubleValue()));
			
			//Valor original del recibo Vencido
			cantidadRedondear = new BigDecimal(reciboConsignacion.getValorDetalle().doubleValue() 
					/ (((TasaRepresentativaMercado)((HashMap)ventana.getDesktop().getSession().
					getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor()-comisionPesos.doubleValue()));
			
			//Se incluye la tasa redondeada
			tasaIncluir.setValor(cantidadRedondear.setScale(0,RoundingMode.CEILING).doubleValue());
			tasaIncluirVencido.setValor(cantidadRedondearVencido.setScale(0,RoundingMode.CEILING).doubleValue());
			tasaIncluir.setFecha(new Date());
			tasaIncluirVencido.setFecha(new Date());
			reciboConsignacion.setTasaRepresentativa(tasaIncluir);
			reciboConsignacion.setTasaRepresentativaVencido(tasaIncluirVencido);
			
			// Se requiere enviar el valor en pesos por lo tanto se hace valor en USD * trm
			
			BigDecimal cantidadConvertida = new BigDecimal(cantidadRedondear.doubleValue());
			cantidadConvertida = cantidadConvertida.multiply(new BigDecimal(((TasaRepresentativaMercado)((HashMap)ventana.getDesktop().getSession().
					getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor()));
			
			BigDecimal cantidadConvertidaVencido = new BigDecimal(cantidadRedondearVencido.doubleValue());
			cantidadConvertidaVencido = cantidadConvertidaVencido.multiply(new BigDecimal(((TasaRepresentativaMercado)((HashMap)ventana.getDesktop().getSession().
					getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor()));
			
			reciboConsignacion.setValorDetalle(cantidadConvertida.doubleValue());
			reciboConsignacion.setValorVencido(cantidadConvertidaVencido.doubleValue());
			log.info("valor recibo USD - COP: "+reciboConsignacion.getValorDetalle());
			log.info("valor recibo Vencido USD - COP: "+reciboConsignacion.getValorVencido());
		
		
		} else{//Entonces se hace el c�lculo con los Euros
			
			monedaIncluir.setCodigo(IConstantes.EUROS);
			tasaIncluir.setMoneda(monedaIncluir);
			tasaIncluirVencido.setMoneda(monedaIncluir);
			
			// se encuentra la comision para restar a la TRM 
			BigDecimal comisionPesos = new BigDecimal((String)((HashMap)ventana.getDesktop().getSession().
					getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.COMISION_EURO));
			
			// Se debe calcular una TRM entre USD y EUR 
			BigDecimal retorno = new BigDecimal(((TasaRepresentativaMercado)((HashMap)ventana.getDesktop().getSession().
					getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_EURO)).getValor()/
					((TasaRepresentativaMercado)((HashMap)ventana.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor());
			//luego se realiza el c�lculo del monto (valorRecibo / (retorno * (trm -comision)))
			cantidadRedondear = 
					new BigDecimal(
							(
								reciboConsignacion.getValorDetalle().doubleValue() / 
									(retorno.doubleValue()* (
												((TasaRepresentativaMercado)((HashMap)ventana.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor() - comisionPesos.doubleValue()
							)
						)  
					)
				);
			
			
			cantidadRedondearVencido = 
					new BigDecimal(
							(
								saldoActual / 
									(retorno.doubleValue()* (
												((TasaRepresentativaMercado)((HashMap)ventana.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor() - comisionPesos.doubleValue()
							)
						)  
					)
				);
			
			// Se incluye la tasa redondeada
			tasaIncluir.setValor(cantidadRedondear.setScale(0,RoundingMode.CEILING).doubleValue());
			tasaIncluirVencido.setValor(cantidadRedondearVencido.setScale(0,RoundingMode.CEILING).doubleValue());
			tasaIncluir.setFecha(new Date());
			tasaIncluirVencido.setFecha(new Date());
			reciboConsignacion.setTasaRepresentativa(tasaIncluir);
			reciboConsignacion.setTasaRepresentativaVencido(tasaIncluirVencido);
			
			// Se requiere enviar el valor en pesos por lo tanto se hace valor en EUR * trm
						BigDecimal cantidadConvertida = new BigDecimal(cantidadRedondear.doubleValue());
						cantidadConvertida = cantidadConvertida.multiply(new BigDecimal(((TasaRepresentativaMercado)((HashMap)ventana.getDesktop().getSession().
								getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_EURO)).getValor()));
						
						BigDecimal cantidadConvertidaVencido = new BigDecimal(cantidadRedondearVencido.doubleValue());
						cantidadConvertidaVencido = cantidadConvertidaVencido.multiply(new BigDecimal(((TasaRepresentativaMercado)((HashMap)ventana.getDesktop().getSession().
								getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_EURO)).getValor()));
						
						reciboConsignacion.setValorDetalle(cantidadConvertida.doubleValue());
						reciboConsignacion.setValorVencido(cantidadConvertidaVencido.doubleValue());
						log.info("valor recibo EUR - COP: "+reciboConsignacion.getValorDetalle());
						log.info("valor recibo Vencido EUR - COP: "+reciboConsignacion.getValorVencido());
	}
}
	
	radio.setAttribute("RECIBO_CONSIGNACION", reciboConsignacion);
	if (reciboConsignacion.getNumeroFila().intValue()>1){
		radio.setDisabled(true);
	}else{
		radio.setDisabled(false);
		if(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)){
			fila.setStyle("background-color: "+IConstantes.ERROR_VENCIDO_COLOR+";");
		}else {
			fila.setStyle("background-color: "+IConstantes.WARNING_COLOR+";");
		}
	}
	radio.setRadiogroup(radioGroup);
    DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
    DecimalFormat decimalFormat = new DecimalFormat("'$'###,###.##");
    Image imagenMoneda = new Image();
    fila.appendChild(radio);
    radio.setParent(fila);
	fila.appendChild(new Label(reciboConsignacion.getReciboConsignacion().toString()));
	fila.appendChild(new Label(reciboConsignacion.getPeriodo().getPeriodo().getPeriodo()));
	fila.appendChild(new Label(reciboConsignacion.getObservaciones()));
	fila.appendChild(new Label(df.format(reciboConsignacion.getPeriodo().getFechaVencimiento())));
	
	Listbox listaMoneda= (Listbox)ventana.getFellow("idLbxCreditosVencidos"); 
	Listitem itemSeleccionado = listaMoneda.getSelectedItem();
	TasaRepresentativaMercado tasaSeleccionada = new TasaRepresentativaMercado();
	Double monedaConvertir = reciboConsignacion.getValorDetalle().doubleValue();
	Double monedaConvertirVencido = reciboConsignacion.getValorVencido().doubleValue();
	
	if(((Listcell)itemSeleccionado.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)){
			fila.appendChild(new Label(decimalFormat.format(new BigDecimal(monedaConvertir).setScale(0,RoundingMode.CEILING))));
			fila.appendChild(new Label(decimalFormat.format(new BigDecimal(monedaConvertirVencido).setScale(0,RoundingMode.CEILING))));
			
			imagenMoneda = new Image();
			imagenMoneda.setSrc(IConstantes.PESOS_COLOMBIANOS_IMAGE);
			imagenMoneda.setTooltiptext(IConstantes.PESOS_COLOMBIANOS);
			fila.appendChild(imagenMoneda);
			
		
	}else{ 
		if(((Listcell)itemSeleccionado.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)){
			
			tasaSeleccionada = (TasaRepresentativaMercado)((HashMap)ventana.getDesktop().getSession(). 
						getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR);
			
			fila.appendChild(new Label(decimalFormat.format(new BigDecimal(monedaConvertir/tasaSeleccionada.getValor()).setScale(0,RoundingMode.CEILING))));
			fila.appendChild(new Label(decimalFormat.format(new BigDecimal(monedaConvertirVencido/tasaSeleccionada.getValor()).setScale(0,RoundingMode.CEILING))));
			imagenMoneda = new Image();
			imagenMoneda.setSrc(IConstantes.DOLARES_AMERICANOS_IMAGE);
			imagenMoneda.setTooltiptext(IConstantes.DOLARES_AMERICANOS);
			fila.appendChild(imagenMoneda);
			
			
		} else{
			
			tasaSeleccionada =  (TasaRepresentativaMercado)((HashMap)ventana.getDesktop().getSession().
					getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_EURO);
			
			fila.appendChild(new Label(decimalFormat.format(new BigDecimal(monedaConvertir/tasaSeleccionada.getValor()).setScale(0,RoundingMode.CEILING))));
			fila.appendChild(new Label(decimalFormat.format(new BigDecimal(monedaConvertirVencido/tasaSeleccionada.getValor()).setScale(0,RoundingMode.CEILING))));
			
			imagenMoneda = new Image();
			imagenMoneda.setSrc(IConstantes.EUROS_IMAGE);
			imagenMoneda.setTooltiptext(IConstantes.EUROS);
			fila.appendChild(imagenMoneda);
		}
	}
	
	}catch(Exception e){
		e.printStackTrace();
	}
	
	return fila;

}	

}

