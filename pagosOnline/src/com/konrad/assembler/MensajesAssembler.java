package com.konrad.assembler;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;

import com.konrad.action.UsuarioAction;
import com.konrad.domain.Mensaje;
import com.konrad.util.IConstantes;
import com.konrad.window.IAssemblerStandard;

public class MensajesAssembler implements IAssemblerStandard { 

	protected static Logger log = Logger.getLogger(UsuarioAction.class);
	
public Object crearDtoDesdeRow(AbstractComponent componente) {
	// TODO Auto-generated method stub
	return null;
}

public Row crearRowDesdeDto( Mensaje mensaje, String tipoMensaje) {
	Row fila=new Row();
	
	Image imagen = new Image();
	Label labelMensaje = new Label();
	
	labelMensaje.setStyle("font-weight:bold;"+IConstantes.STYLE_TEXTO_APLICACION);

	
			if(tipoMensaje.equals(IConstantes.ERROR)){
				imagen.setSrc(IConstantes.ERROR_IMAGE);
				
				fila.appendChild(imagen);
				
				labelMensaje.setValue(mensaje.getMensaje());
				fila.appendChild(labelMensaje);
				fila.setStyle("background-color: "+IConstantes.ERROR_COLOR+";");
				
			} else if (tipoMensaje.equals(IConstantes.WARNING)){
				imagen.setSrc(IConstantes.WARNING_IMAGE);
				fila.appendChild(imagen);
				labelMensaje.setValue(mensaje.getMensaje());
				fila.appendChild(labelMensaje);
				fila.setStyle("background-color: "+IConstantes.WARNING_COLOR+";");
				fila.setAlign("center");
			}else if(tipoMensaje.equals(IConstantes.CONFIRM)){
				imagen.setSrc(IConstantes.CONFIRMATION_IMAGE);
				fila.appendChild(imagen);
				labelMensaje.setValue(mensaje.getMensaje());
				fila.appendChild(labelMensaje);
				fila.setStyle("background-color: "+IConstantes.CONFIRMATION_COLOR+";");
				fila.setAlign("center");
			} else if(tipoMensaje.equals(IConstantes.ERROR_VENCIDO)){
				imagen.setSrc(IConstantes.ERROR_VENCIDO_IMAGE);
				fila.appendChild(imagen);
				labelMensaje.setValue(mensaje.getMensaje());
				fila.appendChild(labelMensaje);
				fila.setStyle("background-color: "+IConstantes.ERROR_VENCIDO_COLOR+";");
				fila.setAlign("center");
			} else {
				imagen.setSrc(IConstantes.INFORMATION_IMAGE);
				fila.appendChild(imagen);
				labelMensaje.setValue(mensaje.getMensaje());
				fila.appendChild(labelMensaje);
				fila.setStyle("background-color: "+IConstantes.INFORMATION_COLOR+";");
				fila.setAlign("center");
			}

	return fila;

}
@Override
public Row crearRowDesdeDto(Object objetoFuente, AbstractComponent componente) {
	// TODO Auto-generated method stub
	return null;
}	

}

