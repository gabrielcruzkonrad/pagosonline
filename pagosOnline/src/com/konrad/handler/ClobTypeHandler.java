package com.konrad.handler;

/**
 * @author gabriel.cruz
 *
 */
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;
import org.apache.tomcat.dbcp.dbcp2.PoolableConnection;

public class ClobTypeHandler implements TypeHandler {
  public void setParameter(PreparedStatement ps, int i, Object parameter, JdbcType jdbcType)
      throws SQLException {
    String inputParam = (String) parameter;
    if ((null == inputParam) || inputParam.isEmpty()) {
      /* empty string is equal to NULL under Oracle */
      ps.setNull(i, Types.CLOB);
    } else {
      Connection conn = ps.getConnection();
      if (conn instanceof PoolableConnection) {
        conn = ((PoolableConnection) conn).getDelegate();
      }
      Clob newClob = conn.createClob();
      newClob.truncate(0);
      newClob.setString(1, inputParam);
      ps.setClob(i, newClob);
    }
  }

  public Object getResult(ResultSet rs, String columnName) throws SQLException {
    String value = "";
    Clob clob = rs.getClob(columnName);
    if (clob != null) {
      int size = (int) clob.length();
      value = clob.getSubString(1, size);
    }
    return value;
  }

  public Object getResult(CallableStatement cs, int columnIndex) throws SQLException {
    String value = "";
    Clob clob = cs.getClob(columnIndex);
    if (clob != null) {
      int size = (int) clob.length();
      value = clob.getSubString(1, size);
    }
    return value;
  }

  public Object getResult(ResultSet rs, int columnIndex) throws SQLException {
    String value = "";
    Clob clob = rs.getClob(columnIndex);
    if (clob != null) {
      int size = (int) clob.length();
      value = clob.getSubString(1, size);
    }
    return value;
  }
}
