package com.konrad.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.konrad.util.LlaveNatural;

/**
 * Servlet implementation class PagoOnlineServlet
 */
@WebServlet("/PagoOnlineServlet")
public class PagoOnlineServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PagoOnlineServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("estamos por ac� ...");
		response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession(false);
        List<LlaveNatural> listaLlaves = new ArrayList<LlaveNatural>(); 
        listaLlaves =(ArrayList<LlaveNatural>)session.getAttribute("LISTA_PARAMETROS");
        String rutaInicio =""; 
        String target = "";
        
        if(listaLlaves!=null){
        	if(listaLlaves.size()>0){
        		for(LlaveNatural llave : listaLlaves){
        			if(llave.getLlave().equalsIgnoreCase("url")){
        				rutaInicio = llave.getValor();
        			}
        			
        			if(llave.getLlave().equalsIgnoreCase("target")){
        				target = llave.getValor();
        			}
        		}
        	}
        }
        out.println("<html>");
        out.println("<body onLoad='enviar()'>");
        out.println("<script language='JavaScript'>");
        out.println("function enviar(){");
        out.println("document.formulario.submit();}");
        out.println("</script>");
        out.println("<form name='formulario' action='"+rutaInicio+"' method='post' target='"+target+"'>");
        if(listaLlaves!=null){
        	if(listaLlaves.size()>0){
        		for(LlaveNatural llave : listaLlaves){
        			System.out.println("for parametros: "+llave.getLlave()+" valor: "+llave.getValor());
        			if(!(llave.getLlave().equalsIgnoreCase("url") || llave.getLlave().equalsIgnoreCase("target"))){
        				out.println("<input type='hidden' name='"+llave.getLlave()+"' value='"+llave.getValor()+"'>");
        			}
        		}
        	}
        }
        
        
        out.println("</form>");
        out.println("</body>");
        out.println("</html>");

        out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession(false);
        List<LlaveNatural> listaLlaves = new ArrayList<LlaveNatural>(); 
        listaLlaves =(ArrayList<LlaveNatural>)session.getAttribute("LISTA_PARAMETROS");
        String rutaInicio = (String)session.getAttribute("RUTA_INICIO");
        String target = (String)session.getAttribute("TARGET");
        
        out.println("<html>");
        out.println("<body >");
        out.println("<script language='JavaScript'>");
        out.println("function enviar(){");
        out.println("document.formulario.submit();}");
        out.println("</script>");
        out.println("<form name='formulario' action='"+rutaInicio+"' method='post' target='"+target+"'>");
        if(listaLlaves!=null){
        	if(listaLlaves.size()>0){
        		for(LlaveNatural llave : listaLlaves){
        			out.println("<input type='text' name='"+llave.getLlave()+"' value='"+llave.getValor()+"'/>");
        		}
        	}
        }
        
        
        out.println("</form>");
        out.println("</body>");
        out.println("</html>");


	}

}
