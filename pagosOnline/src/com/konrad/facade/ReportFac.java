package com.konrad.facade;

import java.sql.Connection;

import org.apache.log4j.Logger;

import com.konrad.data.ReportMgr;

public class ReportFac {

    /** desc: Esta clase es singlenton */
    private static final ReportFac reportFac = new ReportFac();

    protected static Logger log = Logger.getLogger(ReportFac.class);

    /**
     * @type Constructor de la clase ParametrizacionFac
     * @name ParametrizacionFac
     * @return void
     * @desc Crea una instancia de ParametrizacionFac
     */
    private ReportFac() {
	super();
    }

    /**
     * @type M�todo de la clase ParametrizacionFac
     * @name ParametrizacionFac
     * @return LogginFacade
     * @desc permite obtener la instancia del objeto ParametrizacionFac
     *       (singlenton)
     */
    public static ReportFac getFacade() {
	return reportFac;
    }

    /****************************************************************************************/
    /** METODOS DE LA FACHADA */
    /****************************************************************************************/

    public Object obtenerListado(String qryName, Object objeto)
	    throws Exception {
	ReportMgr reportMgr = new ReportMgr();
	return reportMgr.obtenerListado(qryName, objeto);
    }

    public Object obtenerListado(String qryName) throws Exception {
    	ReportMgr reportMgr = new ReportMgr();
	return reportMgr.obtenerListado(qryName);
    }

    public Object obtenerRegistro(String qryName, Object objeto)
	    throws Exception {
    	ReportMgr reportMgr = new ReportMgr();
	return reportMgr.obtenerRegistro(qryName, objeto);
    }

    public Object obtenerRegistro(String qryName) throws Exception {
    	ReportMgr reportMgr = new ReportMgr();
	return reportMgr.obtenerRegistro(qryName);
    }

    public Object ejecutarProcedimiento(String qryName, Object objeto)
	    throws Exception {
    	ReportMgr reportMgr = new ReportMgr();
	return reportMgr.ejecutarProcedimiento(qryName, objeto);
    }

    public Object ejecutarProcedimiento(String qryName) throws Exception {
    	ReportMgr reportMgr = new ReportMgr();
	return reportMgr.ejecutarProcedimiento(qryName);
    }

    public Object validarSQL(String sql) throws Exception {
    	ReportMgr reportMgr = new ReportMgr();
	return reportMgr.validarSQL(sql);
    }
    
    public Connection retornarConexion(){
    	ReportMgr reportMgr = new ReportMgr();
    	return reportMgr.retornarConexion();
    }
}