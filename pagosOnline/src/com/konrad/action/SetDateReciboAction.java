package com.konrad.action;


import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import com.konrad.assembler.MensajesAssembler;
import com.konrad.domain.Mensaje;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.Usuario;
import com.konrad.domain.VencimientoPeriodo;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.util.IConstantes;
import com.konrad.window.ActionStandard;

public class SetDateReciboAction extends ActionStandard {
	
	private static final long serialVersionUID = -1840592650435657086L;

	protected static Logger log = Logger.getLogger(SetDateReciboAction.class);
	private ReciboConsignacion reciboConsignacion;




	@SuppressWarnings("rawtypes")
	public void doModal(ReciboConsignacion reciboConsignacion){
		try{
		Mensaje mensaje = new Mensaje();
		String tipoMensaje = IConstantes.INFORMATION;
		mensaje.setMensaje(IConstantes.INFORMACION_FECHA_GENERACION);
		this.onSetMensaje(mensaje, tipoMensaje);
		
		mensaje = new Mensaje();
		mensaje.setMensaje(IConstantes.INFORMACION_HORARIO_BANCARIO);
		this.onSetMensaje(mensaje, tipoMensaje);
		
		ReciboConsignacion reciboAuxFecha = new ReciboConsignacion();
		VencimientoPeriodo periodoAux = new VencimientoPeriodo();
		periodoAux.setFechaVencimiento(new Date());
		reciboAuxFecha.setPeriodo(periodoAux);
		
		Date fechaGeneracion = (Date)ParametrizacionFac.getFacade().obtenerRegistro("getSiguienteDiaHabil", reciboAuxFecha); 
		((Datebox)this.getFellow("idDbxDateSetDateRecibo")).setValue(fechaGeneracion);
		
		this.setReciboConsignacion(reciboConsignacion);
		Label labelUsuario = (Label)this.getFellow("idLblUsuarioSetDateRecibo");
		labelUsuario.setValue(
			((Usuario)((HashMap)this.getDesktop().getSession().getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.USUARIO_SESSION)).getUsuario()
			+"-"+
			((Usuario)((HashMap)this.getDesktop().getSession().getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.USUARIO_SESSION)).getNombres()		
				);
		
		super.doModal();
		} catch(Exception e){
			e.printStackTrace();
			Messagebox.show(
				    e.getMessage(),
				    "Error de Pagos en L�nea",
				    Messagebox.YES, Messagebox.ERROR);
			e.printStackTrace();
			
		}
	}
	
	public void doOverlapped(){
		super.doOverlapped();
	}
	
	

	public void onSalir(){
		this.detach();
	}
	
	public void onSetMensaje(Mensaje mensaje, String tipoMensaje ){
		((Grid)this.getFellow("idGrdSetDateReciboMensajes")).setVisible(true);
		MensajesAssembler mensajeAs = new MensajesAssembler();
		Rows rowsMensaje = (Rows)this.getFellow("idRowsSetDateReciboMensajes");
		Row filaMensaje = mensajeAs.crearRowDesdeDto(mensaje, tipoMensaje);
		rowsMensaje.appendChild(filaMensaje);
}
	
	
	
	@SuppressWarnings("unchecked")
	public void onImprimirRecibo(){

		ReciboConsignacion recibo = this.getReciboConsignacion();
		Integer  resultado = Messagebox.show(
			    "�Confirma la generaci�n de este recibo de pago? (Esto puede tomar varios segundos) "+IConstantes.MENSAJE_PERMITIR_POPUPS,
			    "Confirmar Generaci�n de Recibo de Pago",
			    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
		if(resultado == Messagebox.YES){
			// Se fijan variables para el nuevo recibo
			ReciboConsignacion nuevoRecibo = new ReciboConsignacion();
			nuevoRecibo.setCliente(recibo.getCliente());
			nuevoRecibo.setReciboConsignacion(recibo.getReciboConsignacion());
			nuevoRecibo.setPeriodo(recibo.getPeriodo());
			Datebox dateValue = (Datebox)this.getFellow("idDbxDateSetDateRecibo");
			nuevoRecibo.getPeriodo().setFechaVencimiento(dateValue.getValue());
			nuevoRecibo.setCuentaReciboConsignacion(recibo.getCuentaReciboConsignacion());
			nuevoRecibo.setFecha(recibo.getFecha());
			nuevoRecibo.setGeneraComision(recibo.getGeneraComision());
			nuevoRecibo.setNumeroFila(recibo.getNumeroFila());
			nuevoRecibo.setObservaciones(recibo.getObservaciones());
			nuevoRecibo.setTasaRepresentativa(recibo.getTasaRepresentativa());
			nuevoRecibo.setTasaRepresentativaVencido(recibo.getTasaRepresentativaVencido());
			nuevoRecibo.setValorDetalle(recibo.getValorDetalle());
			nuevoRecibo.setValorTotal(recibo.getValorTotal());
			nuevoRecibo.setValorVencido(recibo.getValorVencido());
			nuevoRecibo.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
			nuevoRecibo.setBanderaFecha(IConstantes.GENERAL_NO);
			nuevoRecibo.setBanderaDiaHabil(IConstantes.GENERAL_SI);
			
			// Calculamos vencimientos a la fecha y luego procedemos con la creaci�n del nuevo recibo
			try {
				Date fechaGeneracion = (Date)ParametrizacionFac.getFacade().obtenerRegistro("getSiguienteDiaHabil", nuevoRecibo);
				nuevoRecibo.getPeriodo().setFechaVencimiento(fechaGeneracion);
				ParametrizacionFac.getFacade().ejecutarProcedimiento("calcularVencimientosCliente",nuevoRecibo);
				
				List <ReciboConsignacion> recibosBusqueda = 
						(List<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("buscarReciboCancelacionSaldos", nuevoRecibo);
				
				if(recibosBusqueda!=null){
					if(recibosBusqueda.size()>0){
						nuevoRecibo.setReciboConsignacion(recibosBusqueda.get(0).getReciboConsignacion());
					}else{
						ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarReciboReliquidado", nuevoRecibo);	
					}
				}else{
					ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarReciboReliquidado", nuevoRecibo);	
				}
				
				// impresi�n del recibo de consignaci�n
				if(recibo.getReciboConsignacion()!= null && nuevoRecibo.getReciboConsignacion()!=null){
					if(recibo.getReciboConsignacion().intValue() != nuevoRecibo.getReciboConsignacion().intValue()){
						PrintReportAction reportAction = (PrintReportAction)Executions.createComponents("pages/printReport.zul", null, null);
						reportAction.doModal(IConstantes.REP_CUOTA_CREDITO, nuevoRecibo);
						this.detach();
					}else{
						Messagebox.show(
							    IConstantes.ERROR_GENERACION_RECIBO,
							    "Error de Pagos en L�nea",
							    Messagebox.YES, Messagebox.ERROR);
					  Rows rowsMensaje = (Rows)this.getFellow("idRowsSetDateReciboMensajes"); 
					  rowsMensaje.getChildren().clear();
					  Mensaje mensaje = new Mensaje();
					  mensaje.setMensaje(IConstantes.ERROR_GENERACION_RECIBO);
					  String tipoMensaje = IConstantes.ERROR;
					  this.onSetMensaje(mensaje, tipoMensaje);
					}
				}else{
					Messagebox.show(
						    IConstantes.ERROR_GENERACION_RECIBO,
						    "Error de Pagos en L�nea",
						    Messagebox.YES, Messagebox.ERROR);
				  Rows rowsMensaje = (Rows)this.getFellow("idRowsSetDateReciboMensajes"); 
				  rowsMensaje.getChildren().clear();
				  Mensaje mensaje = new Mensaje();
				  mensaje.setMensaje(IConstantes.ERROR_GENERACION_RECIBO);
				  String tipoMensaje = IConstantes.ERROR;
				  this.onSetMensaje(mensaje, tipoMensaje);
				}
			} catch (Exception e) {
				Messagebox.show(
					    e.getMessage(),
					    "Error de Pagos en L�nea",
					    Messagebox.YES, Messagebox.ERROR);
				e.printStackTrace();
			}
			
			
		}
		
	}
	


	public ReciboConsignacion getReciboConsignacion() {
		return reciboConsignacion;
	}

	public void setReciboConsignacion(ReciboConsignacion reciboConsignacion) {
		this.reciboConsignacion = reciboConsignacion;
	}	
	
}