package com.konrad.action;

import org.apache.log4j.Logger;
import org.zkoss.zul.Label;

import com.konrad.domain.Error;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.window.ActionStandard;


public class ErrorAction extends ActionStandard
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 534276749837282219L;
	protected static Logger log = Logger.getLogger(ErrorAction.class);
	
	public void doModal(String errortecnico,String tipo) throws Exception{
		
		   Error error =new Error();
		   Error errorresultado =new Error();
		   error.setTipo(tipo);
		   error.setErrortecnico(errortecnico);
	
		   errorresultado=(Error)ParametrizacionFac.getFacade().obtenerRegistro("",error);
		   
		   Label lbError = (Label) this.getFellowIfAny("idMSError");
		   lbError.setValue((errorresultado.getDescripcion()==null)?"":errorresultado.getDescripcion());
		   
		   Label lbCausa = (Label) this.getFellowIfAny("idMSErrorCausa");
		   lbCausa.setValue((errorresultado.getCausa()==null)?"":errorresultado.getCausa());
		   
		   Label lbAccion = (Label) this.getFellowIfAny("idMSErrorAccion");
		   lbAccion.setValue((errorresultado.getAccion()==null)?"":errorresultado.getAccion());
		   
		   Label lbErrortecnico = (Label) this.getFellowIfAny("idMSErrorTecnico");
		   lbErrortecnico.setValue((errortecnico==null)?"":errortecnico);
		   
		   super.doModal();
		   
		}
	
}