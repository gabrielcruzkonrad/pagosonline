package com.konrad.action;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.zkforge.bwcaptcha.Captcha;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;

import com.konrad.assembler.UserListAssembler;
import com.konrad.domain.Cliente;
import com.konrad.domain.Persona;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.TasaRepresentativaMercado;
import com.konrad.domain.Usuario;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.helper.ReciboConsignacionHelper;
import com.konrad.util.IConstantes;
import com.konrad.util.Moneda;
import com.konrad.window.ActionStandard;


public class CustomerListAction extends ActionStandard {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6814047463504815160L;
	protected static Logger log = Logger.getLogger(CustomerListAction.class);
	private Persona persona;
	private Cliente cliente;
	private List<Usuario> listaUsuarios;
	
	
	public void doModal(List<Usuario> listaUsuarios){
		UserListAssembler ass = new UserListAssembler();
		Rows rowsCustomer = (Rows)this.getFellow("idRowsCustomerList");
		Grid gridCustomer = (Grid)this.getFellow("idGridCustomerList");
		listaUsuarios.stream().forEach(e->{
			Row fila = ass.crearRowDesdeDto(e, this);
			rowsCustomer.appendChild(fila);
			
		}); 
		gridCustomer.setMold("paging");
		gridCustomer.setPageSize(7);
		gridCustomer.applyProperties();
		gridCustomer.invalidate();	
		
		super.doModal();
	}
	
	public void seleccionarUsuario(Usuario usuario) {
		
		Usuario infusuario = new Usuario();

		log.info("Inicia validacion Usuario [CustomerList]");

		Usuario vusuario=new Usuario();
		vusuario.setUsuario(usuario.getUsuario());
		try {
			  infusuario=(Usuario)ParametrizacionFac.getFacade().obtenerRegistro("validarUsuario",vusuario);
			  
	        } catch (Exception e) {
			e.printStackTrace();
		 	throw new RuntimeException ("No ha sido posible culminar el proceso de autenticaci�n debido a un tiempo de espera prolongado, por favor intente de nuevo.");
	        }
		
	 	  
		 if (infusuario!= null){
			 if(infusuario.getUsuario()!=null){

				Session zkSession = Sessions.getCurrent(); 
				HttpSession nativeHttpSession = (HttpSession) zkSession.getNativeSession();
				nativeHttpSession.setAttribute(IConstantes.USUARIO_SESSION, infusuario);
				Map<String, Object> infoSesion = new HashMap<String, Object>();
				infoSesion.put(IConstantes.USUARIO_SESSION, infusuario);
            
             	ReciboConsignacion reciboParametro= new ReciboConsignacion();
             	Cliente cliente = new Cliente();
             	cliente.setCliente(new Long(infusuario.getUsuario()));
             	reciboParametro.setCliente(cliente);
            		
             	// Significa que se hace en moneda extranjera se verifica que haya subido la del d�a DOLARES - PESOS
             	Map<String, Object> mapaResultDolar = new HashMap<String,Object>();
             	Map<String, Object> mapaResultEuro = new HashMap<String,Object>();
             	mapaResultDolar.put("FECHA", new SimpleDateFormat(IConstantes.FORMATO_FECHA).format(new Date()));
             	mapaResultDolar.put("MONEDA_ORIGEN", IConstantes.DOLARES_AMERICANOS);
             	mapaResultDolar.put("MONEDA_DESTINO", IConstantes.PESOS_COLOMBIANOS_ICEBERG);
             	mapaResultEuro.put("FECHA",new SimpleDateFormat(IConstantes.FORMATO_FECHA).format(new Date()));
             	mapaResultEuro.put("MONEDA_ORIGEN", IConstantes.EUROS);
             	mapaResultEuro.put("MONEDA_DESTINO", IConstantes.DOLARES_AMERICANOS);
             			
             			
             			try {
								mapaResultDolar.put("FACTOR", (BigDecimal)ParametrizacionFac.getFacade().obtenerRegistro("getCurrencyFecha", mapaResultDolar));
								mapaResultEuro.put("FACTOR", (BigDecimal)ParametrizacionFac.getFacade().obtenerRegistro("getCurrencyFecha", mapaResultEuro));
						} catch (Exception e) {
								e.printStackTrace();
						}
             			
             				
             			if(mapaResultDolar.get("FACTOR") == null){
             			mapaResultDolar = (Map<String,Object>)ReciboConsignacionHelper.getHelper().setConversionHistorica(IConstantes.FORMATO_FECHA, 
             					IConstantes.DOLARES_AMERICANOS,IConstantes.PESOS_COLOMBIANOS_ICEBERG, new Date());
             			}
             			if(mapaResultEuro.get("FACTOR")== null){
             			mapaResultEuro = (Map<String,Object>)ReciboConsignacionHelper.getHelper().setConversionHistorica(IConstantes.FORMATO_FECHA, 
             					IConstantes.EUROS,IConstantes.DOLARES_AMERICANOS, new Date());
             			}
     
             			log.info("Factor Dolar login: "+mapaResultDolar.get("FACTOR"));
             			log.info("Factor Euro login: "+mapaResultEuro.get("FACTOR"));
             			
             			if(mapaResultDolar.get("FACTOR")==null){
             				mapaResultDolar.put("FACTOR", new BigDecimal(0.00));
             			}
             			
             			if(mapaResultEuro.get("FACTOR")==null){
             				mapaResultEuro.put("FACTOR", new BigDecimal(0.00));
             			}
             				//Obtener tasas representativas del Mercado
             				Double trm = ((BigDecimal)mapaResultDolar.get("FACTOR")).doubleValue();

             				TasaRepresentativaMercado  tasaRepMercadoDolar = new TasaRepresentativaMercado();
             				tasaRepMercadoDolar.setValor(trm);
             				tasaRepMercadoDolar.setFecha(new Date());
             				Moneda monedaDolar = new Moneda();
             				monedaDolar.setCodigo(IConstantes.DOLARES_AMERICANOS);
             				tasaRepMercadoDolar.setMoneda(monedaDolar);
             				Map<String, String> mapaParametro = new HashMap<String, String>();
             				mapaParametro.put("COMISION_RECUPERAR",IConstantes.PARAMETRO_DOLAR);
             				String comisionEuro = new String("0");
             				String comisionDolar = new String("0");
             		
             				try {
             			
             					comisionDolar = (String)ParametrizacionFac.getFacade().obtenerRegistro("seleccionarComisionNegociada", mapaParametro );
             					mapaParametro.clear();
             					mapaParametro.put("COMISION_RECUPERAR",IConstantes.PARAMETRO_EURO );
             					comisionEuro = (String)ParametrizacionFac.getFacade().obtenerRegistro("seleccionarComisionNegociada", mapaParametro );
             				} catch (Exception e) {
						
             					e.printStackTrace();
             				}
             				log.info("comisi�n D�lar: "+comisionDolar);
             				infoSesion.put(IConstantes.COMISION_DOLAR,comisionDolar);
             				log.info("comisi�n Euro: "+comisionEuro);
             				infoSesion.put(IConstantes.COMISION_EURO,comisionEuro);
             				
             				infoSesion.put(IConstantes.TRM_DOLAR, tasaRepMercadoDolar); 		
             
             				//Double trmEuroDolar =ReciboConsignacionHelper.getHelper().getConversion(Currency.EUR, Currency.USD);
             				Double trmEuroDolar =((BigDecimal)mapaResultEuro.get("FACTOR")).doubleValue();
             		
             				log.info("Euro - DOLAR TRM: "+trmEuroDolar);
             		
             				TasaRepresentativaMercado tasaRepMercadoEuro = new TasaRepresentativaMercado();
             				log.info("Tasa EUR - COP"+(trmEuroDolar*trm));
             				tasaRepMercadoEuro.setValor(((Double)(trmEuroDolar*trm)));
             				tasaRepMercadoEuro.setFecha(new Date());
             				Moneda monedaEuro = new Moneda();
             				monedaEuro.setCodigo(IConstantes.EUROS);
             				tasaRepMercadoEuro.setMoneda(monedaEuro);		
             		
             				infoSesion.put(IConstantes.TRM_EURO, tasaRepMercadoEuro);
             		
             				this.getDesktop().getSession().setAttribute(IConstantes.INFORMACION_SESION,infoSesion);
             				this.getDesktop().getSession().setAttribute(IConstantes.AUTENTICADO,"N");

             			 	try{
             	             	Cliente clienteConsulta = (Cliente)ParametrizacionFac.getFacade().obtenerRegistro("selectClienteBasico", cliente);
             	             	if (clienteConsulta != null){
             	             		if(clienteConsulta.getCliente()!=null){
             	             			if(clienteConsulta.getPersona().getDireccionElectronica()==null || 
             	             					clienteConsulta.getPersona().getTelefonoResidencia()==null){
             	             				
             	             				System.out.println("Encuentra dato vac�o");
             	             				
             	             				
             	             				UpdateCorreoTelefonoAction updateCorreoTel = (UpdateCorreoTelefonoAction)Executions.createComponents("pages/updateCorreoTelefono.zul", null, null);
             	             				updateCorreoTel.doModal(clienteConsulta);
             	             				this.detach();
             	             				
             	             			}else{
             	             				System.out.println("Datos llenos: "+clienteConsulta.getPersona().getDireccionElectronica()+" "
             	             			+clienteConsulta.getPersona().getTelefonoResidencia());
             	             				this.detach();
             	             			}
             	             		}else {
             	             			Messagebox.show("Error validando su identidad","Error", Messagebox.OK, Messagebox.ERROR);
             	             		}
             	             		
             	             	}
             	             	} catch(Exception e){
             	             		e.printStackTrace();
             	             	}
             				
			 }
		 }
	}
	
	public void doOverlapped(){
		super.doOverlapped();
	}


	
	public CustomerListAction() {
		// TODO Auto-generated constructor stub
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Usuario> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<Usuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

}
