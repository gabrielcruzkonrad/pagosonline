package com.konrad.action;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.ext.AfterCompose;

import com.konrad.domain.Cliente;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.TransaccionZonaPagos;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.window.ActionStandardBorder;

public class TestAction extends ActionStandardBorder implements AfterCompose {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1564575261216360001L;

	protected static Logger log = Logger.getLogger(TestAction.class);
		
	public void onTestEnvioCorreo(){
		TransaccionZonaPagos transaccion = new TransaccionZonaPagos();
		transaccion.setIdCliente("902132283");
		transaccion.setIdPago(123400L);
		transaccion.setEstadoPago(1L);
		if(transaccion.getEstadoPago().intValue()== 1){
			try{
				Map<String,String> mapaServidor = new HashMap<String,String>();
				ParametrizacionFac.getFacade().ejecutarProcedimiento("seleccionarDatosServidorCorreo", mapaServidor);
				
				if(mapaServidor.get("SERVIDOR_SMTP")!=null &&
						mapaServidor.get("PUERTO_SMTP")!=null &&
						mapaServidor.get("REQUIERE_TLS_SMTP")!=null &&
						mapaServidor.get("REQUIERE_AUTENTICACION_SMTP")!=null &&
						mapaServidor.get("USUARIO_AUTENTICACION_SMTP")!=null &&
						mapaServidor.get("CLAVE_AUTENTICACION_SMTP")!=null &&
						mapaServidor.get("USUARIO_REMITENTE_SMTP")!=null ){
						
						ReciboConsignacion reciboConsignacionVerificacion = new ReciboConsignacion();
						Cliente clienteVerificacion = new Cliente();
						clienteVerificacion.setCliente(new Long(transaccion.getIdCliente()));
						reciboConsignacionVerificacion.setCliente(clienteVerificacion);
						String confirmacionEnvio = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectRastroProvisionCastigo", reciboConsignacionVerificacion);
						if(confirmacionEnvio==null){
							confirmacionEnvio = "N";
						}
						if(confirmacionEnvio.equals("S")){
							
//								clienteVerificacion = (Cliente)ParametrizacionFac.getFacade().obtenerRegistro("selectClienteBasico", clienteVerificacion);
//								
//								String servidorCorreo = mapaServidor.get("SERVIDOR_SMTP");
//								String puertoCorreo = mapaServidor.get("PUERTO_SMTP");
//								String emisor = mapaServidor.get("USUARIO_REMITENTE_SMTP");
//								String asunto = IConstantes.ASUNTO_CORREO_PAGO_PROVISION;
//								String contrasena = mapaServidor.get("CLAVE_AUTENTICACION_SMTP");
//								String mensaje = "El estudiante con código: "+clienteVerificacion.getCliente()+" y nombres: "+
//										clienteVerificacion.getPersona().getNombreRazonSocial()+" "+clienteVerificacion.getPersona().getPrimerApellido()+" "+clienteVerificacion.getPersona().getSegundoApellido()+
//										" ha realizado un pago con el recibo de consignación "+transaccion.getIdPago()+" y presenta saldos por cartera Provisionada/Castigada."+
//										"\n Por favor gestione el proceso de recuperación de cartera."+
//										"\n Gracias;";
//								List<String> receptores = new ArrayList<String>();
//								List<String> receptoresCopia = new ArrayList<String>();
//								List<String> adjuntos = new ArrayList<String>();
//								String correoReporteProvision = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectCorreoReportePagoProvision");
//								receptores.add(correoReporteProvision);/								SendEmail enviarCorreo = new SendEmail();
//								enviarCorreo.envia(servidorCorreo, puertoCorreo, emisor, asunto, receptores, receptoresCopia, mensaje, adjuntos, contrasena);
						}
				} else{
					log.info("No se encontraron datos del servidor");
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			}
	}
	

		public void afterCompose() {		
		this.invalidate();
		this.applyProperties();
		this.onTestEnvioCorreo();
		}
		

		public void onSalir(){
			this.detach();
		}

}