package com.konrad.action;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;

import com.konrad.assembler.MensajesAssembler;
import com.konrad.assembler.RecibosPorSaldosAssembler;
import com.konrad.domain.Cliente;
import com.konrad.domain.Mensaje;
import com.konrad.domain.PeriodoFacturacion;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.TasaRepresentativaMercado;
import com.konrad.domain.TransaccionZonaPagos;
import com.konrad.domain.Usuario;
import com.konrad.domain.VencimientoPeriodo;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.helper.ReciboConsignacionHelper;
import com.konrad.util.IConstantes;
import com.konrad.window.ActionStandardBorder;
import com.zonapagos.www.prod.ZPagosLocator;
import com.zonapagos.www.prod.ZPagosSoapProxy;

public class DetalleDocumentosAction extends ActionStandardBorder implements AfterCompose {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1564575261216360001L;
	private ZPagosLocator zonaPagosLocator;
	private ZPagosSoapProxy zonaPagosProxy;

	protected static Logger log = Logger.getLogger(DetalleDocumentosAction.class);
		
		
		@SuppressWarnings("rawtypes")
		public void afterCompose() {		
			try { 
				
				ReciboConsignacionHelper.getHelper().onInicializarListaCurrency((Listbox)this.getFellow("idLbxRecibosPorSaldos"));
				ReciboConsignacionHelper.getHelper().onRetirarElementosListaCurrency((Listbox)this.getFellow("idLbxRecibosPorSaldos"),
						((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
								getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_EURO)).getValor(),
								((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
										getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor());
				this.onSetImageCurrency();
				this.buscarMaestro();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public void onSelectMoneda(){
			log.info("Ejecutando el metodo [ onSelectMoneda ]... ");
			this.onSetImageCurrency();
			this.buscarMaestro();
			
		}
		
		@SuppressWarnings({ "unchecked"})
		public void buscarMaestro(){
			
			log.info("Ejecutando el método [ buscarMaestro ]... ");
		
			List<ReciboConsignacion> listaDatos=null;
			
			this.getFellow("idGbxFormConsultaRecibosPorSaldos").setVisible(true);
			try {
				
				Cliente cliente = new Cliente();
				cliente.setCliente(new Long(((Usuario)this.getDesktop().getSession().getAttribute(IConstantes.USUARIO_SESSION)).getUsuario()));
				ReciboConsignacion reciboConsignacion = new ReciboConsignacion();
				reciboConsignacion.setCliente(cliente);
				 
				listaDatos=(List<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("selectRecibosConsignacionRecibosPorSaldos",reciboConsignacion); 
				Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupRecibosPorSaldos"); 
				radioGroup.detach();
				radioGroup = new Radiogroup();
				radioGroup.setId("idRadioGroupRecibosPorSaldos");
				((Groupbox)this.getFellow("idGbxFormConsultaRecibosPorSaldos")).appendChild(radioGroup);
				Listbox listaMonedaIncluir= (Listbox)this.getFellow("idLbxRecibosPorSaldos");
		   		Listitem itemSeleccionadoIncluir = listaMonedaIncluir.getSelectedItem();
			
					radioGroup.applyProperties();
					radioGroup.invalidate();
				
				Rows filas = (Rows)this.getFellow("idRowsMaestrosQryRecibosPorSaldos");
				filas.getChildren().clear();       
				log.info("termino de ejecutar la consulta ");
				RecibosPorSaldosAssembler derechosAs = new RecibosPorSaldosAssembler();
				if (listaDatos!=null){
				if (listaDatos.size()>0){
					for (Object object : listaDatos) {
						Row fila = derechosAs.crearRowDesdeDto(object, this);
						filas.appendChild(fila);
						}
					((Button)this.getFellow("idBtnRecibosPorSaldosPagar")).setDisabled(false);
					((Button)this.getFellow("idBtnRecibosPorSaldosImprimirRecibo")).setDisabled(false);
					((Button)this.getFellow("idBtnRecibosPorSaldosRealizarAbono")).setDisabled(false);
					((Button)this.getFellow("idBtnRecibosPorSaldosRefresh")).setDisabled(false);
					((Button)this.getFellow("idBtnRecibosPorSaldosPagoTotal")).setDisabled(false);
					
					radioGroup.setSelectedIndex(0);
					Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesRecibosPorSaldos");
					rowsMensaje.getChildren().clear();
					Mensaje mensaje = new Mensaje();
			       	String tipoMensaje = new String();
			       	if(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)){
			        	tipoMensaje = IConstantes.INFORMATION;
						mensaje.setMensaje(IConstantes.INFORMACION_PAGO_VENCIMIENTO);
						this.onSetMensaje(mensaje, tipoMensaje);
						
			        }else if(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)) {
			        	tipoMensaje = IConstantes.WARNING;
						mensaje.setMensaje(IConstantes.ADVERTENCIA_MONEDA_EXTRANJERA);
						this.onSetMensaje(mensaje, tipoMensaje);
						mensaje.setMensaje(IConstantes.ADVERTENCIA_PUBLICACION);
						this.onSetMensaje(mensaje, tipoMensaje);			
						
					}else{
						tipoMensaje = IConstantes.WARNING;
						mensaje.setMensaje(IConstantes.ADVERTENCIA_MONEDA_EXTRANJERA);
						this.onSetMensaje(mensaje, tipoMensaje);
						mensaje.setMensaje(IConstantes.ADVERTENCIA_PUBLICACION);
						this.onSetMensaje(mensaje, tipoMensaje);
						
					}
			       
			      
					}else{
						((Button)this.getFellow("idBtnRecibosPorSaldosPagar")).setDisabled(true);
						((Button)this.getFellow("idBtnRecibosPorSaldosImprimirRecibo")).setDisabled(true);
						((Button)this.getFellow("idBtnRecibosPorSaldosRealizarAbono")).setDisabled(true);
						((Button)this.getFellow("idBtnRecibosPorSaldosRefresh")).setDisabled(true);
						((Button)this.getFellow("idBtnRecibosPorSaldosPagoTotal")).setDisabled(true);
					}
				}else{
					((Button)this.getFellow("idBtnRecibosPorSaldosPagar")).setDisabled(true);
					((Button)this.getFellow("idBtnRecibosPorSaldosImprimirRecibo")).setDisabled(true);
					((Button)this.getFellow("idBtnRecibosPorSaldosRealizarAbono")).setDisabled(true);
					((Button)this.getFellow("idBtnRecibosPorSaldosRefresh")).setDisabled(true);
					((Button)this.getFellow("idBtnRecibosPorSaldosPagoTotal")).setDisabled(true);
				}

	            Grid tabla = (Grid) this.getFellow("idGridMaestrosQryRecibosPorSaldos");
				// se configura la tabla....
				tabla.setMold("paging");
				tabla.setPageSize(IConstantes.TAMANO_PAGINACION);
				tabla.applyProperties();
				tabla.invalidate();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		@SuppressWarnings({ "unchecked" })
		public void buscarMaestro(VencimientoPeriodo vencimiento){
			
			log.info("Ejecutando el método [ buscarMaestro ]... ");
		
			List<ReciboConsignacion> listaDatos=null;
			
			this.getFellow("idGbxFormConsultaRecibosPorSaldos").setVisible(true);
			try {
				
				Cliente cliente = new Cliente();
				cliente.setCliente(new Long(((Usuario)this.getDesktop().getSession().getAttribute(IConstantes.USUARIO_SESSION)).getUsuario()));
				ReciboConsignacion reciboConsignacion = new ReciboConsignacion();
				reciboConsignacion.setPeriodo(vencimiento);
				reciboConsignacion.setCliente(cliente);
				 
				listaDatos=(List<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("selectRecibosConsignacionRecibosPorSaldos",reciboConsignacion); 
				Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupRecibosPorSaldos"); 
				radioGroup.detach();
				radioGroup = new Radiogroup();
				radioGroup.setId("idRadioGroupRecibosPorSaldos");
				((Groupbox)this.getFellow("idGbxFormConsultaRecibosPorSaldos")).appendChild(radioGroup);
				Listbox listaMonedaIncluir= (Listbox)this.getFellow("idLbxRecibosPorSaldos");
		   		Listitem itemSeleccionadoIncluir = listaMonedaIncluir.getSelectedItem();
			
					radioGroup.applyProperties();
					radioGroup.invalidate();
				
				Rows filas = (Rows)this.getFellow("idRowsMaestrosQryRecibosPorSaldos");
				filas.getChildren().clear();       
				log.info("termino de ejecutar la consulta ");
				RecibosPorSaldosAssembler derechosAs = new RecibosPorSaldosAssembler();
				if (listaDatos!=null){
				if (listaDatos.size()>0){
					for (Object object : listaDatos) {
						Row fila = derechosAs.crearRowDesdeDto(object, this);
						filas.appendChild(fila);
						}
					((Button)this.getFellow("idBtnRecibosPorSaldosPagar")).setDisabled(false);
					((Button)this.getFellow("idBtnRecibosPorSaldosImprimirRecibo")).setDisabled(false);
					((Button)this.getFellow("idBtnRecibosPorSaldosRealizarAbono")).setDisabled(false);
					((Button)this.getFellow("idBtnRecibosPorSaldosRefresh")).setDisabled(false);
					((Button)this.getFellow("idBtnRecibosPorSaldosPagoTotal")).setDisabled(false);
					
					radioGroup.setSelectedIndex(0);
					Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesRecibosPorSaldos");
					rowsMensaje.getChildren().clear();
					Mensaje mensaje = new Mensaje();
			       	String tipoMensaje = new String();
			        if(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)){
			        	tipoMensaje = IConstantes.INFORMATION;
						mensaje.setMensaje(IConstantes.INFORMACION_PAGO_VENCIMIENTO);
						this.onSetMensaje(mensaje, tipoMensaje);
						
			        }else if(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)) {
			        	tipoMensaje = IConstantes.WARNING;
						mensaje.setMensaje(IConstantes.ADVERTENCIA_MONEDA_EXTRANJERA);
						this.onSetMensaje(mensaje, tipoMensaje);
						mensaje.setMensaje(IConstantes.ADVERTENCIA_PUBLICACION);
						this.onSetMensaje(mensaje, tipoMensaje);			
						
					}else{
						tipoMensaje = IConstantes.WARNING;
						mensaje.setMensaje(IConstantes.ADVERTENCIA_MONEDA_EXTRANJERA);
						this.onSetMensaje(mensaje, tipoMensaje);
						mensaje.setMensaje(IConstantes.ADVERTENCIA_PUBLICACION);
						this.onSetMensaje(mensaje, tipoMensaje);
						
					}
					
					}else{
						((Button)this.getFellow("idBtnRecibosPorSaldosPagar")).setDisabled(true);
						((Button)this.getFellow("idBtnRecibosPorSaldosImprimirRecibo")).setDisabled(true);
						((Button)this.getFellow("idBtnRecibosPorSaldosRealizarAbono")).setDisabled(true);
						((Button)this.getFellow("idBtnRecibosPorSaldosRefresh")).setDisabled(true);
						((Button)this.getFellow("idBtnRecibosPorSaldosPagoTotal")).setDisabled(true);
					}
				}else{
					((Button)this.getFellow("idBtnRecibosPorSaldosPagar")).setDisabled(true);
					((Button)this.getFellow("idBtnRecibosPorSaldosImprimirRecibo")).setDisabled(true);
					((Button)this.getFellow("idBtnRecibosPorSaldosRealizarAbono")).setDisabled(true);
					((Button)this.getFellow("idBtnRecibosPorSaldosRefresh")).setDisabled(true);
					((Button)this.getFellow("idBtnRecibosPorSaldosPagoTotal")).setDisabled(true);
				}

	            Grid tabla = (Grid) this.getFellow("idGridMaestrosQryRecibosPorSaldos");
				// se configura la tabla....
				tabla.setMold("paging");
				tabla.setPageSize(IConstantes.TAMANO_PAGINACION);
				tabla.applyProperties();
				tabla.invalidate();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	
		
		public void onSetMensaje(Mensaje mensaje, String tipoMensaje ){
			((Grid)this.getFellow("gridMensajesRecibosPorSaldos")).setVisible(true);
		       MensajesAssembler mensajeAs = new MensajesAssembler();
		       Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesRecibosPorSaldos");
		       Row filaMensaje = mensajeAs.crearRowDesdeDto(mensaje, tipoMensaje);
		       rowsMensaje.appendChild(filaMensaje);
		}	
		
		public void onImprimirIndice(){
			Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupRecibosPorSaldos");
			log.info("indice seleccionado: "+radioGroup.getSelectedIndex());
			log.info("N�mero de �tems: "+radioGroup.getItemCount());
		}
		
		@SuppressWarnings({ "unchecked", "rawtypes" })
		public void onPagarEnLinea(){
			Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupRecibosPorSaldos");
				Integer  resultado = Messagebox.show(
				    "�Confirma este pago? "+IConstantes.MENSAJE_PERMITIR_POPUPS,
				    "Confirmar Inicio Pago",
				    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
				if(resultado==Messagebox.YES){
			  
			  
					String[] listServiciosMulti = null;
					String[] listNitMulti = null;
					double[] listValIvaMulti = null;
					double[] listIvaMulti = null;
					String wsResult;
			
					Radio radio = radioGroup.getSelectedItem();
					ReciboConsignacion recibo = (ReciboConsignacion)radio.getAttribute("RECIBO_CONSIGNACION");
					log.info("Recibo "+recibo.getReciboConsignacion());
					log.info("valor "+recibo.getValorTotal());
					log.info("valor detalle "+recibo.getValorDetalle());
					this.setZonaPagosLocator(new ZPagosLocator());
					this.setZonaPagosProxy(new ZPagosSoapProxy());
					com.zonapagos.www.test.ZPagosSoapProxy zonaPagosProxyPruebas = new com.zonapagos.www.test.ZPagosSoapProxy();
			
					Map<String, String> mapaParametros = new HashMap<String, String>();
					mapaParametros.put("ESTADO_PENDIENTE_INICIAR",IConstantes.ESTADO_PENDIENTE_INICIAR);
					mapaParametros.put("ESTADO_PENDIENTE_FINALIZAR",IConstantes.ESTADO_PENDIENTE_FINALIZAR);
					mapaParametros.put("ID_CLIENTE",recibo.getCliente().getCliente().toString());
					List<TransaccionZonaPagos> listaDatos = new ArrayList<TransaccionZonaPagos>();
					listaDatos = null;
		
			  try {
				listaDatos = (List<TransaccionZonaPagos>)ParametrizacionFac.getFacade().obtenerListado("selectTransaccionZonaPagosPendientes", mapaParametros);
			  } catch (Exception e) {
			
				e.printStackTrace();
			  }
			  
			  if(listaDatos.size()<=0){
				  if (recibo!= null){
					  try {
						  if(ReciboConsignacionHelper.getHelper().getServicioPruebasProduccion()){
							  log.info("Invocamos el servicio de producci�n");
							  wsResult = this.getZonaPagosProxy().inicio_pagoV2(
									  	recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
										(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
		            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
										Integer.parseInt(IConstantes.ID_TIENDA_BANCOLOMBIA):
										Integer.parseInt(IConstantes.ID_TIENDA):
											recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
													Integer.parseInt(IConstantes.ID_TIENDA_DOLARES):
														Integer.parseInt(IConstantes.ID_TIENDA_EURO), 
										recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
												(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
								            	&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
												IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
												IConstantes.CLAVE_SERVICIO:
													recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
															IConstantes.CLAVE_SERVICIO_DOLARES:
															IConstantes.CLAVE_SERVICIO_EURO,  
															recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
																	recibo.getValorDetalle().doubleValue():
																	recibo.getTasaRepresentativa().getValor().doubleValue(), 
										new Double(0).doubleValue(), 
										recibo.getReciboConsignacion().toString(), 
										recibo.getObservaciones().substring(0, recibo.getObservaciones().length()>=70?69:recibo.getObservaciones().length()), 
										recibo.getCliente().getPersona().getDireccionElectronica(), 
										recibo.getCliente().getCliente().toString(), 
										recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("C.C")?
										new String("1"):
										recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NIT")?
										new String("3"):
										recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("T.I")?
										new String("5"):
										recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("CEX")?	
										new String("2"):
										recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NEX")?			
										new String("10"):
										recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("RC")?		
										new String("9"):
										recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("PS")?			
										new String("6"):
										new String("11"), 
										recibo.getCliente().getPersona().getNombreRazonSocial().substring(
												0,	recibo.getCliente().getPersona().getNombreRazonSocial().length()>=50?49:
													recibo.getCliente().getPersona().getNombreRazonSocial().length()), 
											(recibo.getCliente().getPersona().getPrimerApellido()+" "+
													recibo.getCliente().getPersona().getSegundoApellido()).substring(
															0, (recibo.getCliente().getPersona().getPrimerApellido()+" "+
																	recibo.getCliente().getPersona().getSegundoApellido())
																	.length()>=50?49:
																		(recibo.getCliente().getPersona().getPrimerApellido()+" "+
																				recibo.getCliente().getPersona().getSegundoApellido())
																				.length()
																	), 
																	recibo.getCliente().getPersona().getTelefonoResidencia().substring(0, 
																			recibo.getCliente().getPersona().getTelefonoResidencia().length()>=50?49:	
																				recibo.getCliente().getPersona().getTelefonoResidencia().length()),
										new String(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
												getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor().toString()), 
										new String(recibo.getTasaRepresentativa().getValor().toString()+" "+recibo.getTasaRepresentativa().getMoneda().getCodigo()), 
										new String(recibo.getPeriodo()!=null?
	            								recibo.getPeriodo().getPeriodo()!=null?
	            										recibo.getPeriodo().getPeriodo().getPeriodo():"":""), 
										recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
										(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
		            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
										IConstantes.CODIGO_SERVICIO_BANCOLOMBIA:
										IConstantes.CODIGO_SERVICIO:
											recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
													IConstantes.CODIGO_SERVICIO_DOLARES:
													IConstantes.CODIGO_SERVICIO_EURO,  
										listServiciosMulti,
										listNitMulti,
										listValIvaMulti,
										listIvaMulti, 
										Integer.parseInt(new String("0")));
						  }else{
							  log.info("Invocamos el servicio de pruebas");
							  wsResult = zonaPagosProxyPruebas.inicio_pagoV2(
									  	recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
										(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
		            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
										Integer.parseInt(IConstantes.ID_TIENDA_BANCOLOMBIA):
										Integer.parseInt(IConstantes.ID_TIENDA):
											recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
													Integer.parseInt(IConstantes.ID_TIENDA_DOLARES):
														Integer.parseInt(IConstantes.ID_TIENDA_EURO)	
												, 
												recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
														(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
						            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
														IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
														IConstantes.CLAVE_SERVICIO:
															recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
																	IConstantes.CLAVE_SERVICIO_DOLARES:
																	IConstantes.CLAVE_SERVICIO_EURO, 
																	recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
																			recibo.getValorDetalle().doubleValue():
																			recibo.getTasaRepresentativa().getValor().doubleValue(), 
										new Double(0).doubleValue(), 
										recibo.getReciboConsignacion().toString(), 
										recibo.getObservaciones().substring(0, recibo.getObservaciones().length()>=70?69:recibo.getObservaciones().length()), 
										recibo.getCliente().getPersona().getDireccionElectronica(), 
										recibo.getCliente().getCliente().toString(), 
										recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("C.C")?
										new String("1"):
										recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NIT")?
										new String("3"):
										recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("T.I")?
										new String("5"):
										recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("CEX")?	
										new String("2"):
										recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NEX")?			
										new String("10"):
										recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("RC")?		
										new String("9"):
										recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("PS")?			
										new String("6"):
										new String("11"), 
										recibo.getCliente().getPersona().getNombreRazonSocial().substring(
												0,	recibo.getCliente().getPersona().getNombreRazonSocial().length()>=50?49:
													recibo.getCliente().getPersona().getNombreRazonSocial().length()), 
											(recibo.getCliente().getPersona().getPrimerApellido()+" "+
													recibo.getCliente().getPersona().getSegundoApellido()).substring(
															0, (recibo.getCliente().getPersona().getPrimerApellido()+" "+
																	recibo.getCliente().getPersona().getSegundoApellido())
																	.length()>=50?49:
																		(recibo.getCliente().getPersona().getPrimerApellido()+" "+
																				recibo.getCliente().getPersona().getSegundoApellido())
																				.length()
																	), 
																	recibo.getCliente().getPersona().getTelefonoResidencia().substring(0, 
																			recibo.getCliente().getPersona().getTelefonoResidencia().length()>=50?49:	
																				recibo.getCliente().getPersona().getTelefonoResidencia().length()),
										new String(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
												getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor().toString()), 
										new String(recibo.getTasaRepresentativa().getValor().toString()+" "+recibo.getTasaRepresentativa().getMoneda().getCodigo()), 
										new String(recibo.getPeriodo()!=null?
	            								recibo.getPeriodo().getPeriodo()!=null?
	            										recibo.getPeriodo().getPeriodo().getPeriodo():"":""), 
										recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
										(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
		            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
										IConstantes.CODIGO_SERVICIO_BANCOLOMBIA:
										IConstantes.CODIGO_SERVICIO:
											recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
													IConstantes.CODIGO_SERVICIO_DOLARES:
													IConstantes.CODIGO_SERVICIO_EURO, 
										listServiciosMulti,
										listNitMulti,
										listValIvaMulti,
										listIvaMulti, 
										Integer.parseInt(new String("0")));
					}
						  log.info(wsResult);
				
						  if (!wsResult.isEmpty()){
							  if(!wsResult.startsWith("-1")){
								  if(Long.parseLong(wsResult)>0){
							
									  TransaccionZonaPagos transaccion = new TransaccionZonaPagos();
									  transaccion.setSecTransaccionZonaPagos(null);
									  transaccion.setIdPago(recibo.getReciboConsignacion());
									  transaccion.setEstadoPago(new Long(IConstantes.ESTADO_PENDIENTE_INICIAR));
									  transaccion.setIdFormaPago(null);
									  transaccion.setValorPagado(recibo.getValorDetalle());
									  transaccion.setTicketId(null);
									  transaccion.setIdClave(recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
											  (recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
							            	&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
											IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
											IConstantes.CLAVE_SERVICIO:
												recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
														IConstantes.CLAVE_SERVICIO_DOLARES:
														IConstantes.CLAVE_SERVICIO_EURO
														);
									  transaccion.setIdCliente(recibo.getCliente().getCliente().toString());
									  transaccion.setFranquicia(null);
									  transaccion.setCodigoServicio(null);
									  transaccion.setCodigoBanco(null);
									  transaccion.setNombreBanco(null);
									  transaccion.setCodigoTransaccion(null);
									  transaccion.setCicloTransaccion(null);
									  transaccion.setCampo1(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
											  getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor().toString());
									  transaccion.setCampo2(recibo.getTasaRepresentativa().getValor().toString()+" "+recibo.getTasaRepresentativa().getMoneda().getCodigo());
									  transaccion.setCampo3(recibo.getPeriodo()!=null?
	            								recibo.getPeriodo().getPeriodo()!=null?
	            										recibo.getPeriodo().getPeriodo().getPeriodo():"":"");
									  transaccion.setIdComercio(new Long(recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
											  (recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
			            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
											Integer.parseInt(IConstantes.ID_TIENDA_BANCOLOMBIA):
											Integer.parseInt(IConstantes.ID_TIENDA):
												recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
														Integer.parseInt(IConstantes.ID_TIENDA_DOLARES):
															Integer.parseInt(IConstantes.ID_TIENDA_EURO)));
									  transaccion.setDatFecha(new Date());
		            		
									  ParametrizacionFac.getFacade().guardarRegistro("insertTransaccionZonaPagos", transaccion);
									  log.info("registro guardado: "+transaccion.getSecTransaccionZonaPagos());
									  log.info("corriendo el redirect");
		            		
									 // RedirectAction winRedirectPagos = (RedirectAction)Executions.createComponents("pages/redirectPago.zul", null,null);
		            		if(recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)){
		            			
		            			//Execution execution = Executions.getCurrent();
		            			if(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
		            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA)){
		            				Executions.sendRedirect(IConstantes.RUTA_SERVICIO_BANCOLOMBIA+wsResult);
		            			//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO_BANCOLOMBIA+wsResult);
		            			} else if (recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_OCCIDENTE) 
		            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_OCCIDENTE)){
		            				//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO+wsResult);
		            				Executions.sendRedirect(IConstantes.RUTA_SERVICIO+wsResult);	
		            			} else{
		            				//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO+wsResult);
		            				Executions.sendRedirect(IConstantes.RUTA_SERVICIO+wsResult);
		            			}
		            			//execution.setVoided(true);
		            			
		            			
		            		}else if (recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)){
		            			//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO_DOLARES+wsResult);
		            			//Execution execution = Executions.getCurrent();
		            			Executions.sendRedirect(IConstantes.RUTA_SERVICIO_DOLARES+wsResult);
		            			//execution.setVoided(true);
		            		} else {
		            			//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO_EURO+wsResult);
		            			//Execution execution = Executions.getCurrent();
		            			Executions.sendRedirect(IConstantes.RUTA_SERVICIO_EURO+wsResult);
		            			//execution.setVoided(true);
		            		}
		            		
		          		  Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesOtrosPagos"); 
		       			  rowsMensaje.getChildren().clear();
		       			  Mensaje mensaje = new Mensaje();
		       			  
		       			  mensaje.setMensaje(IConstantes.CONFIRMACION_INICIO_PAGO);
		       			  String tipoMensaje = IConstantes.CONFIRM;
		       			  this.onSetMensaje(mensaje, tipoMensaje);
							
						}else{ //else wsresult >0
							Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesRecibosPorSaldos"); 
			       			  rowsMensaje.getChildren().clear();
			       			  Mensaje mensaje = new Mensaje();
			       			  
			       			  mensaje.setMensaje(IConstantes.ERROR_PAGO_INICIO+wsResult);
			       			  String tipoMensaje = IConstantes.ERROR;
			       			  this.onSetMensaje(mensaje, tipoMensaje);
							
						}
					}else{ // wsresult empieza -1
						Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesRecibosPorSaldos"); 
		       			  rowsMensaje.getChildren().clear();
		       			  Mensaje mensaje = new Mensaje();
		       			  
		       			  mensaje.setMensaje(IConstantes.ERROR_PAGO_INICIO+wsResult);
		       			  String tipoMensaje = IConstantes.ERROR;
		       			  this.onSetMensaje(mensaje, tipoMensaje);
						
					}
					
					
				} else{// ws result nulo
					  Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesRecibosPorSaldos"); 
	       			  rowsMensaje.getChildren().clear();
	       			  Mensaje mensaje = new Mensaje();
	       			  
	       			  mensaje.setMensaje(IConstantes.ERROR_PAGO_INICIO+wsResult);
	       			  String tipoMensaje = IConstantes.ERROR;
	       			  this.onSetMensaje(mensaje, tipoMensaje);
					
				}	

			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
					}
				}
				  
				  
			} else if(listaDatos.size()>0){
				  Messagebox.show(
						    IConstantes.ERROR_PAGO_PENDIENTE,
						    "Error de Pagos en L�nea",
						    Messagebox.YES, Messagebox.ERROR);
				  Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesRecibosPorSaldos"); 
				  rowsMensaje.getChildren().clear();
				  Mensaje mensaje = new Mensaje();
				  mensaje.setMensaje(IConstantes.ERROR_PAGO_PENDIENTE);
				  String tipoMensaje = IConstantes.ERROR;
				  this.onSetMensaje(mensaje, tipoMensaje);
			}
		} 
	}
		

		public void onBuscarParametro()
		{
			PeriodoFacturacion periodoFacturacion = new PeriodoFacturacion();
			Textbox textoParametro = (Textbox)this.getFellow("parPeriodoRecibosPorSaldos");
			periodoFacturacion.setPeriodo(textoParametro.getValue());
			VencimientoPeriodo vencimientoPeriodo = new VencimientoPeriodo();
			vencimientoPeriodo.setPeriodo(periodoFacturacion);
			
			this.buscarMaestro(vencimientoPeriodo);
		}

		public ZPagosLocator getZonaPagosLocator() {
			return zonaPagosLocator;
		}

		public void setZonaPagosLocator(ZPagosLocator zonaPagosLocator) {
			this.zonaPagosLocator = zonaPagosLocator;
		}

		public ZPagosSoapProxy getZonaPagosProxy() {
			return zonaPagosProxy;
		}

		public void setZonaPagosProxy(ZPagosSoapProxy zonaPagosProxy) {
			this.zonaPagosProxy = zonaPagosProxy;
		}

		public void onSetImageCurrency(){
			Listbox lista = (Listbox)this.getFellow("idLbxRecibosPorSaldos"); 
			Image imagen = (Image)this.getFellow("idImgMonedaRecibosPorSaldos");
			ReciboConsignacionHelper.getHelper().onSetImageCurrency(lista, imagen);
		}
		
		public void onImprimirRecibo(){
			Radiogroup radioGrupo = (Radiogroup)this.getFellow("idRadioGroupRecibosPorSaldos");
			if(radioGrupo.getItemCount()>0){
				Radio radio = radioGrupo.getSelectedItem();
				ReciboConsignacion recibo = (ReciboConsignacion)radio.getAttribute("RECIBO_CONSIGNACION");
				PrintReportAction printReportAction = (PrintReportAction)Executions.createComponents("pages/printReport.zul", null, null);
				printReportAction.doModal(IConstantes.REP_CUOTA_CREDITO, recibo);
			}else{
				Messagebox.show(
					    IConstantes.MENSAJE_OPCION_NO_SELECCIONADA,
					    "Error de Pagos en L�nea",
					    Messagebox.YES, Messagebox.ERROR);
			  Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesRecibosPorSaldos"); 
			  rowsMensaje.getChildren().clear();
			  Mensaje mensaje = new Mensaje();
			  mensaje.setMensaje(IConstantes.MENSAJE_OPCION_NO_SELECCIONADA);
			  String tipoMensaje = IConstantes.ERROR;
			  this.onSetMensaje(mensaje, tipoMensaje);
			}
		}
		
		public void onSalir(){
			this.detach();
		}

}