package com.konrad.action;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.zkoss.util.media.AMedia;
import org.zkoss.zul.Iframe;

import com.konrad.domain.DetalleOrden;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.facade.ReportFac;
import com.konrad.listener.ContextoAplicacion;
import com.konrad.util.IConstantes;
import com.konrad.window.ActionStandard;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

public class PrintReportAction extends ActionStandard {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1564575261216360001L;


	protected static Logger log = Logger.getLogger(PrintReportAction.class);
	protected static final Logger lLogger = Logger.getLogger("net.sf.jasperreports");
		
	public void doModal(String nombreReporte, ReciboConsignacion recibo){
		 
		this.generarReporte(nombreReporte, recibo, "1");
		super.doModal();
	}
	
	public void imprimirProducto(DetalleOrden detalleOrden) {
		
		 log.info("poner producto en Amedia");
         final AMedia amedia = 
             new AMedia(detalleOrden.getReferencia()+".pdf", "pdf", "application/pdf", 
            		 detalleOrden.getAtributoProducto().getFotoProducto());
              
         //Se fija el contenido del iframe
         log.info("poner reporte iframe");
         ((Iframe)this.getFellow("idIFrameReporte")).setWidth("100%");
         ((Iframe)this.getFellow("idIFrameReporte")).setHeight("100%");
         ((Iframe)this.getFellow("idIFrameReporte")).setContent(amedia);
         super.doModal();
	}
	
	public void generarReporte(String nombreReporte, ReciboConsignacion recibo, String bandera){
		 JasperPrint jp = null;
		  ByteArrayOutputStream out = null;
		  lLogger.setLevel(Level.OFF);
		  
	        try {
	            //generate report pdf stream
	        	log.info("Ruta: "+ContextoAplicacion.getInstance().getRutaContexto()+
		                IConstantes.RUTA_REPORTES);
	        	
	        	// fijar parámetros del reporte	
	            final Map<String, Object> params = new HashMap<String, Object>();
	            if(nombreReporte.startsWith("ORDEN")){
	            	params.put("P_ORGANIZACION", recibo.getOrden().getOrganizacion());
	            	params.put("P_ORDEN", recibo.getOrden().getOrden());
	            	params.put("P_DOCUMENTO", recibo.getOrden().getDocumento());
	            
	            }else if (nombreReporte.startsWith("CUOTA")){
	            	
	            	params.put("P_INICIAL", recibo.getReciboConsignacion());
		            params.put("P_FINAL", recibo.getReciboConsignacion());
		            
	            } else{
	            	params.put("P_INICIAL", recibo.getReciboConsignacion());
		            params.put("P_FINAL", recibo.getReciboConsignacion());
	            }
	            //fijar parámetros para subreportes
	            params.put("ICEBERGRS_REPORT_DIR",new File(ContextoAplicacion.getInstance().getRutaContexto()+
		                IConstantes.RUTA_REPORTES_EJECUTABLES));
	            params.put("ICEBERGRS_IMAGE_DIR", new File(ContextoAplicacion.getInstance().getRutaContexto()+IConstantes.RUTA_IMAGENES));
	 
	        
	            log.info("nombre Reporte (1): "+nombreReporte);
	            // compilamos el jrxml con un jasper
	            log.info("cargar reporte");
	            JasperReport report = (JasperReport)JRLoader.loadObject(new File(ContextoAplicacion.getInstance().getRutaContexto()+
			                IConstantes.RUTA_REPORTES_EJECUTABLES+nombreReporte+".jasper"));
	            
//	             JasperReport report =  JasperCompileManager.compileReport(ContextoAplicacion.getInstance().getRutaContexto()+
//			                IConstantes.RUTA_REPORTES+nombreReporte+".jrxml");
	            log.info("Recuperar Conexion");
	            Connection conexionReporte =  ReportFac.getFacade().retornarConexion();
	            log.info("llenar reporte");	
	            // ejecución del reporte con parámetros 
	    		jp = JasperFillManager.fillReport(report, params, conexionReporte);
	    		// si no hay stream entonces abrirlo
	    		if(out==null){
	    			out = new ByteArrayOutputStream();
	    		}
	    		// se prepara la exportación del reporte
	    		JRPdfExporter exporter = new JRPdfExporter();
	    		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
	            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
	            // Se exporta el reporte
	            log.info("exportar reporte");	
	            exporter.exportReport();
	            // Se pone el reporte en el Amedia
	            log.info("poner reporte Amedia");
	            final AMedia amedia = 
	                new AMedia(nombreReporte+".pdf", "pdf", "application/pdf", out.toByteArray());
	                 
	            //sSe fija el contenido del iframe
	            log.info("poner reporte iframe");
	            ((Iframe)this.getFellow("idIFrameReporte")).setWidth("100%");
	            ((Iframe)this.getFellow("idIFrameReporte")).setHeight("100%");
	            ((Iframe)this.getFellow("idIFrameReporte")).setContent(amedia);
	            
	            out.close();
	        } catch (Exception e) {
	            throw new RuntimeException(e);
	        }
	}
	
	public void generarReporte(String nombreReporte, ReciboConsignacion recibo){
		
		 InputStream is = null;
	        try {
	        	log.info("nombre Reporte (2): "+nombreReporte);
	        	
	            //generate report pdf stream
	            is = new FileInputStream( ContextoAplicacion.getInstance().getRutaContexto()+
		                IConstantes.RUTA_REPORTES_EJECUTABLES+nombreReporte+".jasper");
	                 
	            final Map<String, Object> params = new HashMap<String, Object>();
	            if(nombreReporte.startsWith("ORDEN")){
	            	params.put("P_ORGANIZACION", recibo.getOrden().getOrganizacion());
	            	params.put("P_ORDEN", recibo.getOrden().getOrden());
	            	params.put("P_DOCUMENTO", recibo.getOrden().getDocumento());
	            
	            }else if (nombreReporte.startsWith("CUOTA")){
	            	
	            	params.put("P_INICIAL", recibo.getReciboConsignacion());
		            params.put("P_FINAL", recibo.getReciboConsignacion());
		            
	            } else{
	            	params.put("P_INICIAL", recibo.getReciboConsignacion());
		            params.put("P_FINAL", recibo.getReciboConsignacion());
	            }
	            //fijar parámetros para subreportes
	            params.put("ICEBERGRS_REPORT_DIR",new File(ContextoAplicacion.getInstance().getRutaContexto()+
		                IConstantes.RUTA_REPORTES_EJECUTABLES));
	            params.put("ICEBERGRS_IMAGE_DIR", new File(ContextoAplicacion.getInstance().getRutaContexto()+IConstantes.RUTA_IMAGENES));
	 
	            
	            final byte[] buf = 
	                JasperRunManager.runReportToPdf(is, params, ReportFac.getFacade().retornarConexion());
	                 
	            //prepare the AMedia for iframe
	            final InputStream mediais = new ByteArrayInputStream(buf);
	            final AMedia amedia = 
	                new AMedia(nombreReporte+".pdf", "pdf", "application/pdf", mediais);
	                 
	            //set iframe content
	            ((Iframe)this.getFellow("idIFrameReporte")).setWidth("100%");
	            ((Iframe)this.getFellow("idIFrameReporte")).setHeight("100%");
	            ((Iframe)this.getFellow("idIFrameReporte")).setContent(amedia);
	        } catch (Exception ex) {
	            throw new RuntimeException(ex);
	        } finally {
	            if (is != null) {
	                try {
						is.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            }
	        }
		
	}
	
	public void doOverlapped(){
		super.doOverlapped();
	}

}