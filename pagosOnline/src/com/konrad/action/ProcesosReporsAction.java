package com.konrad.action;


import org.apache.log4j.Logger;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Messagebox;

import com.konrad.window.ActionStandardBorder;

public class ProcesosReporsAction  extends ActionStandardBorder implements AfterCompose {

		/**
	 * 
	 */
	private static final long serialVersionUID = -8031927641947898657L;
		protected static Logger log = Logger.getLogger(ProcesosReporsAction.class);

		
		public void afterCompose() {}
		public void generarInforme(){
		try
		{
			int resultado;
			resultado = Messagebox.show("La aplicacion de formulas solo es valida para indicadores de tipo valor.","Advertencia",
		        	   Messagebox.OK, Messagebox.INFORMATION);
			
		log.info("resultado: "+resultado);
		}
		catch (Exception e) {
			log.info("Adicionando varible. "+e.getMessage());	
		}	
			
		}

}
