package com.konrad.action;


import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;

import com.konrad.assembler.MensajesAssembler;
import com.konrad.domain.CentroCosto;
import com.konrad.domain.Cliente;
import com.konrad.domain.Mensaje;
import com.konrad.domain.ProgramaReporte;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.Usuario;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.helper.ReciboConsignacionHelper;
import com.konrad.util.IConstantes;
import com.konrad.window.ActionStandard;
import com.konrad.window.AdministradorVentanasAction;

public class AutenticacionEscolarisAction extends ActionStandard {
	
	private static final long serialVersionUID = -1840592650435657086L;

	protected static Logger log = Logger.getLogger(AutenticacionEscolarisAction.class);
	private ReciboConsignacion reciboConsignacion;
	private String ruta;
	private AdministradorVentanasAction adminW;
	private String idZul;
	private String autenticado;
		
	@SuppressWarnings("rawtypes")
	public void doModal( ReciboConsignacion reciboConsignacion, String ruta , String idZul, AdministradorVentanasAction adminW){
		
		Mensaje mensaje = new Mensaje();
		String tipoMensaje = IConstantes.INFORMATION;
		mensaje.setMensaje(IConstantes.INFORMACION_AUTENTICACION_ACADEMICO);
		this.onSetMensaje(mensaje, tipoMensaje);
		this.setReciboConsignacion(reciboConsignacion);
		this.setRuta(ruta);
		this.setAdminW(adminW);
		this.setIdZul(idZul);
		this.setAutenticado(
				this.getDesktop().getSession().getAttribute(IConstantes.AUTENTICADO)!=null?
				((String)this.getDesktop().getSession().getAttribute(IConstantes.AUTENTICADO)):
					"N");
	
		
		Label labelUsuario = (Label)this.getFellow("idLblAutenticacionUsuarioAcademico");
		labelUsuario.setValue(
			((Usuario)((HashMap)this.getDesktop().getSession().getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.USUARIO_SESSION)).getUsuario()
			+"-"+
			((Usuario)((HashMap)this.getDesktop().getSession().getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.USUARIO_SESSION)).getNombres()		
				);
		
		super.doModal();
	}
	
	
	@SuppressWarnings("rawtypes")
	public void doModal( ReciboConsignacion reciboConsignacion,  String ruta ){
		
		Mensaje mensaje = new Mensaje();
		String tipoMensaje = IConstantes.INFORMATION;
		mensaje.setMensaje(IConstantes.INFORMACION_AUTENTICACION_ACADEMICO);
		this.onSetMensaje(mensaje, tipoMensaje);
		this.setReciboConsignacion(reciboConsignacion);
		this.setRuta(ruta);
		this.setAutenticado(
				this.getDesktop().getSession().getAttribute(IConstantes.AUTENTICADO)!=null?
				((String)this.getDesktop().getSession().getAttribute(IConstantes.AUTENTICADO)):
					"N");
		
		Label labelUsuario = (Label)this.getFellow("idLblAutenticacionUsuarioAcademico");
		labelUsuario.setValue(
			((Usuario)((HashMap)this.getDesktop().getSession().getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.USUARIO_SESSION)).getUsuario()
			+"-"+
			((Usuario)((HashMap)this.getDesktop().getSession().getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.USUARIO_SESSION)).getNombres()		
				);
		
		super.doModal();
		this.onValidateAutenticacionAcademico();
	}
	
	
	public void onValidateAutenticacionAcademico(){
		if ((this.getAutenticado()).equalsIgnoreCase("S")){
			Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesAutenticacionRegistro");
			rowsMensaje.getChildren().clear();
			rowsMensaje.invalidate();
			Mensaje mensaje = new Mensaje();
			String tipoMensaje = IConstantes.INFORMATION;
			mensaje.setMensaje(IConstantes.INFORMACION_ACTUALIZACION_DATOS);
			this.onSetMensaje(mensaje, tipoMensaje);
			mensaje.setMensaje(IConstantes.INFORMACION_ACTUALIZACION_IDENTIDAD);
			this.onSetMensaje(mensaje, tipoMensaje);
			mensaje.setMensaje(IConstantes.INFORMACION_CAMPOS_OBLIGATORIOS);
			this.onSetMensaje(mensaje, tipoMensaje);
			((Groupbox)this.getFellow("idGbxAutenticacionUsuario")).setVisible(false);
			System.out.println("por ac� si se entr� .....");
			Label labelNombreUsuario = (Label)this.getFellow("idLblNombreUsuario");
			Label labelIdentificacionUsuario =(Label)this.getFellow("idLblIdentificacionUsuario");
			Label labelTipoIdentificacionUsuario = (Label)this.getFellow("idLblTipoIdentificacionUsuario");
			labelNombreUsuario.setValue(this.getReciboConsignacion().getCliente().getPersona().getNombreRazonSocial()+" "+
				this.getReciboConsignacion().getCliente().getPersona().getPrimerApellido()+" "+
				this.getReciboConsignacion().getCliente().getPersona().getSegundoApellido());
			labelIdentificacionUsuario.setValue(this.getReciboConsignacion().getCliente().getPersona().getIdentificacion());
			labelTipoIdentificacionUsuario.setValue(
					this.getReciboConsignacion().getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion()+" "+
							this.getReciboConsignacion().getCliente().getPersona().getTipoIdentificacion().getNombreTipoIdentificacion());
		
			Textbox textoCorreo  = (Textbox)this.getFellow("idTbxCorreoElectronicoUsuario");
			Textbox textoCelular = (Textbox)this.getFellow("idTbxCelularUsuario");
			Textbox textoTelefonoOficina =(Textbox)this.getFellow("idTbxTelefonoOficinaUsuario");
			Textbox textoTelefonoResidencia =(Textbox)this.getFellow("idTbxTelefonoResidenciaUsuario");
			Textbox textoDireccionResidencia =(Textbox)this.getFellow("idTbxDireccionResidenciaUsuario");
			
			System.out.println("sigui� por ac� luego de la cuesti�n de los textbox .....");
			Cliente clienteBasic = new Cliente();
			clienteBasic.setCliente(this.getReciboConsignacion().getCliente().getCliente());
			try {
				clienteBasic = (Cliente)ParametrizacionFac.getFacade().obtenerRegistro("selectClienteBasico", clienteBasic);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			textoCorreo.setValue(clienteBasic.getPersona().getPaginaWeb()!=null?
					clienteBasic.getPersona().getPaginaWeb():"usuario@dominio.com");
			textoCelular.setValue(clienteBasic.getPersona().getApartadoAereo()!=null?
					clienteBasic.getPersona().getApartadoAereo():"0");
			textoTelefonoOficina.setValue(clienteBasic.getPersona().getEnvioCorrespondencia());
			textoTelefonoResidencia.setValue(clienteBasic.getPersona().getTelefonoResidencia()!=null?
					clienteBasic.getPersona().getTelefonoResidencia():"0");
			textoDireccionResidencia.setValue(clienteBasic.getPersona().getDireccionResidencia()!=null?
					clienteBasic.getPersona().getDireccionResidencia():"0");
			System.out.println("sigui� por ac� .....");
		
			((Groupbox)this.getFellow("idGbxActualizacionDatosUsuario")).setVisible(true);
			this.setHeight("650px");
		}
		
	}

	public void doOverlapped(){
		super.doOverlapped();
	}

	public void onAutenticarUsuario(){
		Textbox textoContrasena = (Textbox)this.getFellow("idPasswordUsuarioAcademico");
		if(textoContrasena.getValue()!=null){
			if(!textoContrasena.getValue().equals("")){
				Map<String, Object> parametrosAutenticacion = new HashMap<String, Object>();
				parametrosAutenticacion.put("CLIENTE", this.getReciboConsignacion().getCliente().getCliente());
				parametrosAutenticacion.put("CONTRASENA", textoContrasena.getValue());
				Cliente cliente = ReciboConsignacionHelper.getHelper().getAutenticacionAcademico(parametrosAutenticacion);
				if(cliente != null){
					if(cliente.getNombreNegocio()!=null ){
						if(!cliente.getNombreNegocio().equals("")){
							this.getDesktop().getSession().setAttribute(IConstantes.AUTENTICADO, "S");
							if(this.getRuta().equals(IConstantes.RUTA_NOTAS_PERIODO_ACTUAL) ||
									this.getRuta().equals(IConstantes.RUTA_NOTAS_HISTORICO) ||
									this.getRuta().equals(IConstantes.RUTA_INFORME_ESTADO)		
									){
									this.getAdminW().cargarVentanaCondicional(this.getIdZul());
									this.onSalir();
									
								
							}else{
								Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesAutenticacionRegistro");
								rowsMensaje.getChildren().clear();
								rowsMensaje.invalidate();
								Mensaje mensaje = new Mensaje();
								String tipoMensaje = IConstantes.INFORMATION;
								mensaje.setMensaje(IConstantes.INFORMACION_ACTUALIZACION_DATOS);
								this.onSetMensaje(mensaje, tipoMensaje);
								mensaje.setMensaje(IConstantes.INFORMACION_ACTUALIZACION_IDENTIDAD);
								this.onSetMensaje(mensaje, tipoMensaje);
								mensaje.setMensaje(IConstantes.INFORMACION_CAMPOS_OBLIGATORIOS);
								this.onSetMensaje(mensaje, tipoMensaje);
								((Groupbox)this.getFellow("idGbxAutenticacionUsuario")).setVisible(false);
								System.out.println("por ac� si se entr� .....");
								Label labelNombreUsuario = (Label)this.getFellow("idLblNombreUsuario");
								Label labelIdentificacionUsuario =(Label)this.getFellow("idLblIdentificacionUsuario");
								Label labelTipoIdentificacionUsuario = (Label)this.getFellow("idLblTipoIdentificacionUsuario");
								labelNombreUsuario.setValue(this.getReciboConsignacion().getCliente().getPersona().getNombreRazonSocial()+" "+
									this.getReciboConsignacion().getCliente().getPersona().getPrimerApellido()+" "+
									this.getReciboConsignacion().getCliente().getPersona().getSegundoApellido());
								labelIdentificacionUsuario.setValue(this.getReciboConsignacion().getCliente().getPersona().getIdentificacion());
								labelTipoIdentificacionUsuario.setValue(
										this.getReciboConsignacion().getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion()+" "+
												this.getReciboConsignacion().getCliente().getPersona().getTipoIdentificacion().getNombreTipoIdentificacion());
							
								Textbox textoCorreo  = (Textbox)this.getFellow("idTbxCorreoElectronicoUsuario");
								Textbox textoCelular = (Textbox)this.getFellow("idTbxCelularUsuario");
								Textbox textoTelefonoOficina =(Textbox)this.getFellow("idTbxTelefonoOficinaUsuario");
								Textbox textoTelefonoResidencia =(Textbox)this.getFellow("idTbxTelefonoResidenciaUsuario");
								Textbox textoDireccionResidencia =(Textbox)this.getFellow("idTbxDireccionResidenciaUsuario");
								
								System.out.println("sigui� por ac� luego de la cuesti�n de los textbox .....");
								Cliente clienteBasic = new Cliente();
								clienteBasic.setCliente(this.getReciboConsignacion().getCliente().getCliente());
								try {
									clienteBasic = (Cliente)ParametrizacionFac.getFacade().obtenerRegistro("selectClienteBasico", clienteBasic);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								textoCorreo.setValue(clienteBasic.getPersona().getPaginaWeb()!=null?
										clienteBasic.getPersona().getPaginaWeb():"usuario@dominio.com");
								textoCelular.setValue(clienteBasic.getPersona().getApartadoAereo()!=null?
										clienteBasic.getPersona().getApartadoAereo():"0");
								textoTelefonoOficina.setValue(clienteBasic.getPersona().getEnvioCorrespondencia());
								textoTelefonoResidencia.setValue(clienteBasic.getPersona().getTelefonoResidencia()!=null?
										clienteBasic.getPersona().getTelefonoResidencia():"0");
								textoDireccionResidencia.setValue(clienteBasic.getPersona().getDireccionResidencia()!=null?
										clienteBasic.getPersona().getDireccionResidencia():"0");
								System.out.println("sigui� por ac� .....");
							
								((Groupbox)this.getFellow("idGbxActualizacionDatosUsuario")).setVisible(true);
								this.setHeight("98%");
							}
						} else{
							Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesAutenticacionRegistro");
							rowsMensaje.getChildren().clear();
							rowsMensaje.invalidate();
							Mensaje mensaje = new Mensaje();
							String tipoMensaje = IConstantes.ERROR;
							mensaje.setMensaje(IConstantes.MENSAJE_CONTRASENA_INVALIDA);
							this.onSetMensaje(mensaje, tipoMensaje);

							Messagebox.show(IConstantes.MENSAJE_CONTRASENA_INVALIDA,
								    "Error de Pagos en L�nea",
								    Messagebox.YES, Messagebox.ERROR);
						}
					} else{
						Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesAutenticacionRegistro");
						rowsMensaje.getChildren().clear();
						rowsMensaje.invalidate();
						Mensaje mensaje = new Mensaje();
						String tipoMensaje = IConstantes.ERROR;
						mensaje.setMensaje(IConstantes.MENSAJE_CONTRASENA_INVALIDA);
						this.onSetMensaje(mensaje, tipoMensaje);
						

						Messagebox.show(IConstantes.MENSAJE_CONTRASENA_INVALIDA,
							    "Error de Pagos en L�nea",
							    Messagebox.YES, Messagebox.ERROR);
					}
				}else{
					Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesAutenticacionRegistro");
					rowsMensaje.getChildren().clear();
					rowsMensaje.invalidate();
					Mensaje mensaje = new Mensaje();
					String tipoMensaje = IConstantes.ERROR;
					mensaje.setMensaje(IConstantes.MENSAJE_CONTRASENA_INVALIDA);
					this.onSetMensaje(mensaje, tipoMensaje);
					

					Messagebox.show(IConstantes.MENSAJE_CONTRASENA_INVALIDA,
						    "Error de Pagos en L�nea",
						    Messagebox.YES, Messagebox.ERROR);
				}	
			}else{
				Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesAutenticacionRegistro");
				rowsMensaje.getChildren().clear();
				rowsMensaje.invalidate();
				Mensaje mensaje = new Mensaje();
				String tipoMensaje = IConstantes.ERROR;
				mensaje.setMensaje(IConstantes.MENSAJE_CONTRASENA_INVALIDA);
				this.onSetMensaje(mensaje, tipoMensaje);
				

				Messagebox.show(IConstantes.MENSAJE_CONTRASENA_INVALIDA,
					    "Error de Pagos en L�nea",
					    Messagebox.YES, Messagebox.ERROR);
			}
		}else{
			Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesAutenticacionRegistro");
			rowsMensaje.getChildren().clear();
			rowsMensaje.invalidate();
			Mensaje mensaje = new Mensaje();
			String tipoMensaje = IConstantes.ERROR;
			mensaje.setMensaje(IConstantes.MENSAJE_CONTRASENA_INVALIDA);
			this.onSetMensaje(mensaje, tipoMensaje);
			
			Messagebox.show(IConstantes.MENSAJE_CONTRASENA_INVALIDA,
				    "Error de Pagos en L�nea",
				    Messagebox.YES, Messagebox.ERROR);
		}
		
	}
	
	public void onSalir(){
		this.detach();
	}
	
	public void onSetMensaje(Mensaje mensaje, String tipoMensaje ){
		((Grid)this.getFellow("gridMensajesAutenticacionRegistro")).setVisible(true);
		MensajesAssembler mensajeAs = new MensajesAssembler();
		Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesAutenticacionRegistro");
		Row filaMensaje = mensajeAs.crearRowDesdeDto(mensaje, tipoMensaje);
		rowsMensaje.appendChild(filaMensaje);
}
	public void onImprimirReporte(ReciboConsignacion recibo){
		try{
			ProgramaReporte programaReporte = new ProgramaReporte();
			CentroCosto centroCosto = new CentroCosto();
			centroCosto.setCentroCosto(recibo.getOrden().getCentroCosto().getCentroCosto());
			programaReporte.setCentroCosto(centroCosto);
			programaReporte = (ProgramaReporte)ParametrizacionFac.getFacade().obtenerRegistro("selectProgramaReporte", programaReporte);
			Map<String,Object> mapaParametros = new HashMap<String, Object>();
			mapaParametros.put("ORDEN", recibo.getOrden().getOrden());
			mapaParametros.put("ORGANIZACION", recibo.getOrden().getOrganizacion());
			mapaParametros.put("DOCUMENTO", recibo.getOrden().getDocumento());
			mapaParametros.put("CLIENTE", recibo.getCliente().getCliente());
			ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarImpresionOrden", mapaParametros);
			PrintReportAction printReportAction = (PrintReportAction)Executions.createComponents("pages/printReport.zul", null, null);
			printReportAction.doModal(programaReporte!=null?programaReporte.getReporte():IConstantes.REP_ORDEN, recibo);
			
			this.onSalir();
			
			}catch(Exception e){
				e.printStackTrace();
				Messagebox.show(e.getMessage(),
					    "Error de Pagos en L�nea",
					    Messagebox.YES, Messagebox.ERROR);
			}
		
	}
	
	public void onActualizarInformacion(){
		Textbox textoCorreo  = (Textbox)this.getFellow("idTbxCorreoElectronicoUsuario");
		Textbox textoCelular = (Textbox)this.getFellow("idTbxCelularUsuario");
		Textbox textoTelefonoOficina =(Textbox)this.getFellow("idTbxTelefonoOficinaUsuario");
		Textbox textoTelefonoResidencia =(Textbox)this.getFellow("idTbxTelefonoResidenciaUsuario");
		Textbox textoDireccionResidencia =(Textbox)this.getFellow("idTbxDireccionResidenciaUsuario");
		
		if(
			(textoCorreo.getValue()==null || textoCorreo.getValue().equals("usuario@dominio.com"))|| 
			(textoCelular.getValue()==null || textoCelular.getValue().equals("0")) ||
			(textoTelefonoResidencia.getValue()==null || textoTelefonoResidencia.getValue().equals("0")) ||
			(textoDireccionResidencia.getValue()== null || textoDireccionResidencia.getValue().equals("0")) 
			){
			Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesAutenticacionRegistro");
			rowsMensaje.getChildren().clear();
			rowsMensaje.invalidate();
			Mensaje mensaje = new Mensaje();
			String tipoMensaje = IConstantes.ERROR;
			mensaje.setMensaje(IConstantes.MENSAJE_INFORMACION_INVALIDA);
			this.onSetMensaje(mensaje, tipoMensaje);
			
			Messagebox.show(IConstantes.MENSAJE_INFORMACION_INVALIDA,
				    "Error de Pagos en L�nea",
				    Messagebox.YES, Messagebox.ERROR);
			
		}else{
			Map<String,Object> mapaParametros = new HashMap<String,Object>();
			mapaParametros.put("CLIENTE", this.getReciboConsignacion().getCliente().getCliente());
			mapaParametros.put("DIRECCION_RESIDENCIA", textoDireccionResidencia.getValue());
			mapaParametros.put("TELEFONO_RESIDENCIA", textoTelefonoResidencia.getValue());
			mapaParametros.put("CELULAR", textoCelular.getValue());
			mapaParametros.put("CORREO_ALTERNO", textoCorreo.getValue().toLowerCase());
			mapaParametros.put("TELEFONO_OFICINA", textoTelefonoOficina.getValue());
			
			Map<String,Object> mapaParametrosPersona = new HashMap<String,Object>();
			mapaParametrosPersona.put("CLIENTE", this.getReciboConsignacion().getCliente().getCliente());
			mapaParametrosPersona.put("DIRECCION_RESIDENCIA", textoDireccionResidencia.getValue());
			mapaParametrosPersona.put("TELEFONO_RESIDENCIA", textoTelefonoResidencia.getValue());
			mapaParametrosPersona.put("CELULAR", textoCelular.getValue());
			mapaParametrosPersona.put("CORREO_ALTERNO", textoCorreo.getValue().toLowerCase());
			mapaParametrosPersona.put("TELEFONO_OFICINA", textoTelefonoOficina.getValue());
			
			
			try {
				ParametrizacionFac.getFacade().ejecutarProcedimiento("setDatosBasicosCliente", mapaParametros);
				ParametrizacionFac.getFacade().ejecutarProcedimiento("setDatosBasicosPersona", mapaParametrosPersona);
				ReciboConsignacionHelper.getHelper().setDatosClienteAcademico(mapaParametros);
				
				this.onImprimirReporte(this.getReciboConsignacion());
				
			} catch (Exception e) {
				Messagebox.show(e.getMessage(),
					    "Error de Pagos en L�nea",
					    Messagebox.YES, Messagebox.ERROR);
				e.printStackTrace();
			}
			
		}
		
		
	}

	public ReciboConsignacion getReciboConsignacion() {
		return reciboConsignacion;
	}

	public void setReciboConsignacion(ReciboConsignacion reciboConsignacion) {
		this.reciboConsignacion = reciboConsignacion;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public AdministradorVentanasAction getAdminW() {
		return adminW;
	}

	public void setAdminW(AdministradorVentanasAction adminW) {
		this.adminW = adminW;
	}	
	
	public String getIdZul() {
		return idZul;
	}

	public void setIdZul(String idZul) {
		this.idZul = idZul;
	}


	public String getAutenticado() {
		return autenticado;
	}


	public void setAutenticado(String autenticado) {
		this.autenticado = autenticado;
	}
	
}