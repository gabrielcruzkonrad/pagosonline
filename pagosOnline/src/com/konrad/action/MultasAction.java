package com.konrad.action;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;

import com.konrad.assembler.CarteraAssembler;
import com.konrad.assembler.MensajesAssembler;
import com.konrad.domain.Cliente;
import com.konrad.domain.CuentaReciboConsignacion;
import com.konrad.domain.DetalleReciboConsignacion;
import com.konrad.domain.Mensaje;
import com.konrad.domain.PeriodoFacturacion;
import com.konrad.domain.Persona;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.SaldoCartera;
import com.konrad.domain.TasaRepresentativaMercado;
import com.konrad.domain.TipoIdentificacion;
import com.konrad.domain.TransaccionZonaPagos;
import com.konrad.domain.Usuario;
import com.konrad.domain.VencimientoPeriodo;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.helper.ReciboConsignacionHelper;
import com.konrad.util.IConstantes;
import com.konrad.window.ActionStandardBorder;
import com.zonapagos.www.prod.ZPagosLocator;
import com.zonapagos.www.prod.ZPagosSoapProxy;

public class MultasAction extends ActionStandardBorder implements AfterCompose {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1564575261216360001L;
	private ZPagosLocator zonaPagosLocator;
	private ZPagosSoapProxy zonaPagosProxy;

	protected static Logger log = Logger.getLogger(MultasAction.class);
		
	
		
		public void afterCompose() {		
			try { 
//				ReciboConsignacionHelper.getHelper().onInicializarListaCurrency((Listbox)this.getFellow("idLbxMultas"));
//				ReciboConsignacionHelper.getHelper().onRetirarElementosListaCurrency((Listbox)this.getFellow("idLbxMultas"),
//						((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
//								getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_EURO)).getValor(),
//								((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
//										getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor());
				this.buscarMaestro();
//				this.onSetImageCurrency();
				this.setMensajeConsultaMultas();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
	
		
		public void onIniciarPagoTotal(){

				ReciboConsignacion recibo = new ReciboConsignacion();
				recibo.setCliente(this.getCliente());
				PagoTotalReciboAction pagoAction = (PagoTotalReciboAction)Executions.createComponents("pages/pagoTotalRecibo.zul", null, null);
				pagoAction.doModal(recibo);
		}
		
		
		public void onSelectMoneda(){
			log.info("Ejecutando el metodo [ onSelectMoneda ]... ");
			this.onSetImageCurrency();
			this.onListCurrencyChange();
			
		}
		
		
		
		public Double getInteresCorriente(ReciboConsignacion recibo){
			Double interesCorriente = 0d;
			try{
				interesCorriente =(Double)ParametrizacionFac.getFacade().obtenerRegistro("selectInteresesCorrientesCliente", recibo);
				if(interesCorriente == null)
					interesCorriente =0d;
			}catch(Exception e){
				e.printStackTrace();
				Messagebox.show("No fue posible obtener intereses corrientes","Error", Messagebox.OK, Messagebox.ERROR);
			}
			
			return interesCorriente;
		}
		
		public Double getInteresMora(ReciboConsignacion recibo){
			Double interesMora = 0d;
			try{
				interesMora =(Double)ParametrizacionFac.getFacade().obtenerRegistro("selectInteresesMoraCliente", recibo);
				if (interesMora== null)
					interesMora=0d;
				
			}catch(Exception e){
				e.printStackTrace();
				Messagebox.show("No fue posible obtener intereses de mora","Error", Messagebox.OK, Messagebox.ERROR);
			}
			
			return interesMora;
		}
		
		
		public Double getObligacion(){
			Double obligacion = 0d;
			Listbox listaObligaciones = (Listbox)this.getFellow("idLbxMaestrosQryMultas");
			Set<Listitem> arrayObligaciones = (Set<Listitem>)listaObligaciones.getSelectedItems();
			Iterator<Listitem> iteradorObligaciones = arrayObligaciones.iterator();
			while(iteradorObligaciones.hasNext()){
				Listitem item = iteradorObligaciones.next();
				ReciboConsignacion recibo = (ReciboConsignacion)item.getAttribute("RECIBO_CONSIGNACION");
				log.info("genera Comision: "+recibo.getGeneraComision());
				obligacion = obligacion+this.getCantidadConvertida(recibo.getValorDetalle(), recibo.getGeneraComision(),recibo.getFecha());
			}
			return obligacion;
		}
		
		
		
		public void calcularVencimientosCliente(ReciboConsignacion recibo){
			try{
				ParametrizacionFac.getFacade().ejecutarProcedimiento("calcularVencimientosCliente", recibo);
			}catch(Exception e){
				e.printStackTrace();
				Messagebox.show("No fue posible calcular vencimientos","Error", Messagebox.OK, Messagebox.ERROR);
			}
			
		}
		
		public void setInteresesCorrientes(Double interesCorriente){
			Label labelCorriente = (Label)this.getFellow("idLblIntCorrienteMultas");
			labelCorriente.setValue(new DecimalFormat("'$'###,###,###.##").format(interesCorriente));
		}
		
		public void setInteresesMora(Double interesMora){
			Label labelMora = (Label)this.getFellow("idLblIntMoraMultas");
			labelMora.setValue(new DecimalFormat("'$'###,###,###.##").format(interesMora));
		}
		
		public void setObligacion(Double obligacion){
			Label labelObligacion = (Label)this.getFellow("idLblObligacionMultas");
			labelObligacion.setValue(new DecimalFormat("'$'###,###,###.##").format(obligacion));
		}
		
		
		public void setTotalPago(Double interesCorriente, Double interesMora, Double obligacion){
			Label labelTotal = (Label)this.getFellow("idLblTotalMultas");
			labelTotal.setValue(new DecimalFormat("'$'###,###,###.##").format(interesCorriente+interesMora+obligacion));
		}
		
		public Date getSiguienteFechaHabil(Date fecha){
			Date fechaHabil = new Date();
			try{
				ReciboConsignacion reciboAux = new ReciboConsignacion();
				VencimientoPeriodo periodo = new VencimientoPeriodo();
				periodo.setFechaVencimiento(fecha);
				reciboAux.setPeriodo(periodo);
				fechaHabil = (Date)ParametrizacionFac.getFacade().obtenerRegistro("getSiguienteDiaHabil", reciboAux);
				
			}catch(Exception e){
				e.printStackTrace();
				Messagebox.show("No fue posible calcular la siguiente fecha","Error", Messagebox.OK, Messagebox.ERROR);
			}
			
			return fechaHabil;
		}
		
		public Date getFechaSistema(){
			Date fechaSistema = null;
			try{
				fechaSistema = new SimpleDateFormat(IConstantes.FORMATO_FECHA).parse((String)ParametrizacionFac.getFacade().obtenerRegistro("selectFechaSistema"));
			}catch(Exception e){
				e.printStackTrace();
				Messagebox.show("No se pudo obtener la fecha de sistema","Error", Messagebox.OK, Messagebox.ERROR);
			}

			return fechaSistema;
		}
		
		public Cliente getCliente(){
			Cliente cliente = new Cliente();
			cliente.setCliente(new Long(((Usuario)this.getDesktop().getSession().getAttribute(IConstantes.USUARIO_SESSION)).getUsuario()));
			return cliente;
		}
		
		@SuppressWarnings("rawtypes")
		public BigDecimal getComisionDolares(){
			BigDecimal comisionPesos = new BigDecimal((String)((HashMap)this.getDesktop().getSession().
					getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.COMISION_DOLAR));
			
			return comisionPesos==null?new BigDecimal(0d):comisionPesos;
		}
		
		@SuppressWarnings("rawtypes")
		public BigDecimal getComisionEuros(){
			BigDecimal comisionPesos = new BigDecimal((String)((HashMap)this.getDesktop().getSession().
					getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.COMISION_EURO));
			return comisionPesos==null?new BigDecimal(0d):comisionPesos;
		}
		
		
		public void setMensajeMonedaExtranjera(){
			Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesMultas");
			rowsMensaje.getChildren().clear();
			Mensaje mensaje = new Mensaje();
			mensaje.setMensaje(IConstantes.ADVERTENCIA_MONEDA_EXTRANJERA);
			String tipoMensaje = new String(IConstantes.WARNING);
			this.onSetMensaje(mensaje, tipoMensaje);
			mensaje.setMensaje(IConstantes.MENSAJE_CARTERA);
			this.onSetMensaje(mensaje, tipoMensaje);

			
		}
		
		public void setMensajeMonedaLocal(){
			Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesMultas");
			rowsMensaje.getChildren().clear();
			Mensaje mensaje = new Mensaje();
			mensaje.setMensaje(IConstantes.MENSAJE_CARTERA);
			String tipoMensaje = new String(IConstantes.INFORMATION);
			this.onSetMensaje(mensaje, tipoMensaje);

			
		}
		
		public void setMensajeConsultaMultas(){
			Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesMultas");
			rowsMensaje.getChildren().clear();
			Mensaje mensaje = new Mensaje();
			mensaje.setMensaje(IConstantes.MENSAJE_CONSULTA_MULTAS_PAGO);
			String tipoMensaje = new String(IConstantes.INFORMATION);
			this.onSetMensaje(mensaje, tipoMensaje);
			
		}
		
		
		@SuppressWarnings("rawtypes")
		public Double getCantidadConvertida(Double cantidad, String generaComision, Date fechaConversion){
			Double cantidadConvertida = 0d;
			Listbox listaMonedaIncluir= (Listbox)this.getFellow("idLbxMultas");
	   		Listitem itemSeleccionadoIncluir = listaMonedaIncluir.getSelectedItem();
	   		BigDecimal cantidadRedondear;
	   		BigDecimal comision = new BigDecimal(0d);
	   		Double factor = 0d;
	   		Double factorEuro = 0d;
	   		
			if(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)){
				this.setMensajeMonedaLocal();
				cantidadRedondear = new BigDecimal(cantidad);
				
			} else if(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)){
				this.setMensajeMonedaExtranjera();
				
				if (generaComision.equals(IConstantes.GENERAL_NO)){
		   			factor = this.getFactorConversion(fechaConversion, IConstantes.DOLARES_AMERICANOS, IConstantes.PESOS_COLOMBIANOS_ICEBERG);
		   			comision = new BigDecimal(0d);
		   		}else{
		   			factor = (((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor());
		   			comision = this.getComisionDolares();
		   		}
				
				
				cantidadRedondear = new BigDecimal(cantidad / (factor-comision.doubleValue()));
				
				
			}else {
				this.setMensajeMonedaExtranjera();
				
				if (generaComision.equals(IConstantes.GENERAL_NO)){
					factor = (((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor());
		   			factorEuro = (((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_EURO)).getValor()); 
		   			//factor = this.getFactorConversion(fechaConversion, IConstantes.DOLARES_AMERICANOS, IConstantes.PESOS_COLOMBIANOS_ICEBERG);
		   			//factorEuro = this.getFactorConversion(fechaConversion, IConstantes.EUROS, IConstantes.DOLARES_AMERICANOS);
		   			comision = new BigDecimal(0d);
		   		}else{
		   			comision = this.getComisionEuros();
		   			factor = (((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor());
		   			factorEuro = (((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_EURO)).getValor()); 
		   		}
				
				
				
				// Se debe calcular una TRM entre USD y EUR 
				BigDecimal retorno = new BigDecimal(factorEuro/factor);
				log.info("retorno cartera: "+retorno);
				
				//luego se realiza el c�lculo del monto (valorRecibo / (retorno * (trm -comision)))
				cantidadRedondear = 
						new BigDecimal(
								(
									cantidad / 
										(retorno.doubleValue()* (
													factor - comision.doubleValue()
								)
							)  
						)
					);
				
				log.info("cantidadRedondear Cartera: "+cantidadRedondear);
				
			}
			cantidadConvertida = cantidadRedondear.setScale(0,RoundingMode.CEILING).doubleValue();
			return cantidadConvertida;
		}
		
		
		public void onSetMensaje(Mensaje mensaje, String tipoMensaje ){
			((Grid)this.getFellow("gridMensajesMultas")).setVisible(true);
		       MensajesAssembler mensajeAs = new MensajesAssembler();
		       Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesMultas");
		       Row filaMensaje = mensajeAs.crearRowDesdeDto(mensaje, tipoMensaje);
		       rowsMensaje.appendChild(filaMensaje);
		}
		
		public void onSelectListaCartera(){
			try{
			this.onListCurrencyChange();
			}catch(Exception e){
				e.printStackTrace();
				Messagebox.show("No fue posible calcular vencimientos","Error", Messagebox.OK, Messagebox.ERROR);
			}
		}
		
		public void onListCurrencyChange(){
			Datebox dateFechaPago = (Datebox)this.getFellow("idDtbxFechaPagoMultas");
			dateFechaPago.setValue(this.getSiguienteFechaHabil(dateFechaPago.getValue()));
			ReciboConsignacion reciboEncabezado= this.getReciboEncabezado(dateFechaPago.getValue(), dateFechaPago.getValue(), IConstantes.GENERAL_SI);
			// calcular vencimientos a la fecha
			this.calcularVencimientosCliente(reciboEncabezado);
			Double interesCorriente = this.getCantidadConvertida(this.getInteresCorriente(reciboEncabezado),IConstantes.GENERAL_SI,dateFechaPago.getValue());
			Double interesMora = this.getCantidadConvertida(this.getInteresMora(reciboEncabezado),IConstantes.GENERAL_SI,dateFechaPago.getValue());
			Double obligacion = this.getObligacion();
			this.setInteresesCorrientes(interesCorriente);
			this.setInteresesMora(interesMora);
			this.setObligacion(obligacion);
			this.setTotalPago(interesCorriente, interesMora, obligacion);
			

		}
		
		public void evaluarSeleccionItem(){
			Listbox listaObligaciones = (Listbox)this.getFellow("idLbxMaestrosQryMultas");
			List<Listitem> listaItems = listaObligaciones.getItems();
			Iterator<Listitem> iteradorItems = listaItems.iterator();
			while(iteradorItems.hasNext()){
					Listitem item = iteradorItems.next();
					if(item.getIndex()>0 && item.isSelected()){
						Listitem itemAux = listaObligaciones.getItemAtIndex(item.getIndex()-1);
						if(!itemAux.isSelected()){
							Clients.showNotification(IConstantes.MENSAJE_CARTERA_LISTA, Clients.NOTIFICATION_TYPE_ERROR, item, "after_start", 3000, true);
							item.setSelected(false);
						}
						
					}
			}
				
			
		} 
		
		@SuppressWarnings({ "unchecked" })
		public void buscarMaestro(){
			
			log.info("Ejecutando el m�todo [ buscarMaestro ]... ");
			List<ReciboConsignacion> listaDatos=null;
			
			this.getFellow("idGbxFormConsultaMultas").setVisible(true);
			try {
				
				Cliente cliente = new Cliente();
				cliente.setCliente(new Long(((Usuario)this.getDesktop().getSession().getAttribute(IConstantes.USUARIO_SESSION)).getUsuario()));
				ReciboConsignacion reciboConsignacion = new ReciboConsignacion();
				reciboConsignacion.setCliente(cliente);
				reciboConsignacion.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
				VencimientoPeriodo vencimientoPeriodo = new VencimientoPeriodo();
				reciboConsignacion.setPeriodo(vencimientoPeriodo);
				reciboConsignacion.setBanderaDiaHabil(IConstantes.GENERAL_NO);
				
				//Obtenemos el listado de documentos de cartera que no sean intereses
				listaDatos=(List<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("selectListadoMultas",reciboConsignacion); 
				
				Listbox listaMaestroCartera = (Listbox)this.getFellow("idLbxMaestrosQryMultas");

		   		
				
				ReciboConsignacion reciboAuxiliar = new ReciboConsignacion();
				reciboAuxiliar.setCliente(cliente);
				reciboAuxiliar.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
				reciboAuxiliar.setFecha(new Date());
				
				listaMaestroCartera.getItems().clear();
			
					
				log.info("termino de ejecutar la consulta ");
				CarteraAssembler carteraAs = new CarteraAssembler();
				if (listaDatos!=null){
					if (listaDatos.size()>0 ){
						
							for (Object object : listaDatos) {
								Listitem fila = carteraAs.crearListItemDesdeDto(object, this);
								listaMaestroCartera.appendChild(fila);	
								}
								//this.onListCurrencyChange();
						}
					}
				
				// se configura la tabla....
				listaMaestroCartera.setMold("paging");
				listaMaestroCartera.setPageSize(IConstantes.TAMANO_PAGINACION_BANDBOX);
				listaMaestroCartera.applyProperties();
				listaMaestroCartera.invalidate();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
	
		
		@SuppressWarnings({ "unchecked" })
		public void buscarMaestro(VencimientoPeriodo vencimiento){
			
			log.info("Ejecutando el m�todo [ buscarMaestro ]... ");
			List<ReciboConsignacion> listaDatos=null;
			
			this.getFellow("idGbxFormConsultaMultas").setVisible(true);
			try {
				
				Cliente cliente = new Cliente();
				cliente.setCliente(new Long(((Usuario)this.getDesktop().getSession().getAttribute(IConstantes.USUARIO_SESSION)).getUsuario()));
				ReciboConsignacion reciboConsignacion = new ReciboConsignacion();
				reciboConsignacion.setPeriodo(vencimiento);
				reciboConsignacion.setCliente(cliente);
				reciboConsignacion.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
				reciboConsignacion.setBanderaDiaHabil(IConstantes.GENERAL_NO);
				
				//Obtenemos el listado de documentos de cartera que no sean intereses
				listaDatos=(List<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("selectListadoMultas",reciboConsignacion); 
				
				Listbox listaMaestroCartera = (Listbox)this.getFellow("idLbxMaestrosQryMultas");


				ReciboConsignacion reciboAuxiliar = new ReciboConsignacion();
				reciboAuxiliar.setCliente(cliente);
				reciboAuxiliar.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
				reciboAuxiliar.setFecha(new Date());
				
				
				listaMaestroCartera.getItems().clear();
			
					
				log.info("termino de ejecutar la consulta ");
				CarteraAssembler carteraAs = new CarteraAssembler();
				if (listaDatos!=null){
					if (listaDatos.size()>0){
						
							for (Object object : listaDatos) {
								Listitem fila = carteraAs.crearListItemDesdeDto(object, this);
								listaMaestroCartera.appendChild(fila);	
							}
						}
					}

				// se configura la tabla....
				listaMaestroCartera.setMold("paging");
				listaMaestroCartera.setPageSize(IConstantes.TAMANO_PAGINACION_BANDBOX);
				listaMaestroCartera.applyProperties();
				listaMaestroCartera.invalidate();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public void onImprimirIndice(){
			Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupMultas");
			log.info("indice seleccionado: "+radioGroup.getSelectedIndex());
			log.info("N�mero de �tems: "+radioGroup.getItemCount());
		}
		
	public Long registrarEncabezadoRecibo(ReciboConsignacion reciboConsignacion){
		ReciboConsignacion recibo = reciboConsignacion;
		try{
			ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarEncabezadoRecibo", recibo);
		}catch (Exception e){
			e.printStackTrace();
		}
		
		if(recibo.getReciboConsignacion()==null){
			recibo.setReciboConsignacion(0L);
		}
		return reciboConsignacion.getReciboConsignacion();
	}
		
	
	public ReciboConsignacion getReciboEncabezado(Date fechaPago, Date fechaVencimiento, String banderaDiaHabil){
		ReciboConsignacion reciboEncabezado = new ReciboConsignacion();
		reciboEncabezado.setFechaPago(fechaPago);
		reciboEncabezado.setCliente(this.getCliente());
		reciboEncabezado.setBanderaDiaHabil(banderaDiaHabil);
		reciboEncabezado.setBanderaFecha(IConstantes.GENERAL_NO);
		reciboEncabezado.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
		VencimientoPeriodo vencimientoPeriodo = new VencimientoPeriodo();
		vencimientoPeriodo.setFechaVencimiento(fechaVencimiento);
		reciboEncabezado.setPeriodo(vencimientoPeriodo);
		return reciboEncabezado;
		
	}
	
	
	public void onTestPagoOnline(){
		Listbox listboxObligaciones = (Listbox)this.getFellow("idLbxMaestrosQryMultas");
 
		Integer  resultado = Messagebox.show(
			    "�Confirma este pago? "+IConstantes.MENSAJE_PERMITIR_POPUPS,
			    "Confirmar Inicio Pago",
			    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
		if(resultado == Messagebox.YES){
			// definir las variables de invocaci�n del servicio
		
		
			ReciboConsignacion reciboEncabezado = new ReciboConsignacion();
			Date fechaSistema = this.getFechaSistema();
			// si el pago es en l�nea debe realizarse a la fecha del sistema
			reciboEncabezado.setFechaPago(fechaSistema);
			reciboEncabezado.setFecha(fechaSistema);
			reciboEncabezado.setCliente(this.getCliente());
			reciboEncabezado.setBanderaDiaHabil(IConstantes.GENERAL_NO);
			reciboEncabezado.setBanderaFecha(IConstantes.GENERAL_NO);
			reciboEncabezado.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
			VencimientoPeriodo vencimientoPeriodo = new VencimientoPeriodo();
			vencimientoPeriodo.setFechaVencimiento(fechaSistema);
			reciboEncabezado.setPeriodo(vencimientoPeriodo);
			// calcular vencimientos a la fecha
			this.calcularVencimientosCliente(reciboEncabezado);
			Set<Listitem> listaItemsSeleccionados = listboxObligaciones.getSelectedItems();
			Iterator<Listitem> iteradorItems = listaItemsSeleccionados.iterator();
			
			Double interesCorriente = this.getInteresCorriente(reciboEncabezado);
			Double interesMora = this.getInteresMora(reciboEncabezado);
			Double obligacion = this.getObligacion();
			ReciboConsignacion recibo = new ReciboConsignacion();
			
			
			// si existen detalles o hay interes por pagar se registra un recibo
			if(obligacion+interesMora+interesCorriente> 0){
				//no registramos encabezado por pruebas 
				//this.registrarEncabezadoRecibo(reciboEncabezado);
				// verificar si el recibo si se registr�
				reciboEncabezado.setReciboConsignacion(136090L);
				if(reciboEncabezado.getReciboConsignacion()!=null){
					
					try{
						// no registramos intereses por pruebas registramos los intereses del recibo
						//ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarInteresesRecibo", reciboEncabezado);
						
						while(iteradorItems.hasNext()){
							recibo = (ReciboConsignacion)iteradorItems.next().getAttribute("RECIBO_CONSIGNACION");
							// registramos los detalles seleccionados 
							DetalleReciboConsignacion detalleRecibo = new DetalleReciboConsignacion();
							SaldoCartera saldoCartera = new SaldoCartera();
							saldoCartera.setNumeroCredito(recibo.getSaldoCartera().getNumeroCredito());
							saldoCartera.setDocumento(recibo.getSaldoCartera().getDocumento());
							saldoCartera.setOrganizacion(recibo.getSaldoCartera().getOrganizacion());
							recibo.setReciboConsignacion(reciboEncabezado.getReciboConsignacion());
							detalleRecibo.setReciboConsignacion(recibo);
							detalleRecibo.setSaldoCartera(saldoCartera);
							System.out.println("Detalle del recibo --- " +detalleRecibo.getSaldoCartera().getDocumento()+" - "+detalleRecibo.getSaldoCartera().getNumeroCredito()
									+" Recibo a afectar: "+detalleRecibo.getReciboConsignacion().getReciboConsignacion());
							ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarDetalleRecibo", detalleRecibo);
					
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
		}
	}
		
		@SuppressWarnings({ "unchecked", "rawtypes" })
		public void onPagarEnLinea(){
			
			Listbox listboxObligaciones = (Listbox)this.getFellow("idLbxMaestrosQryMultas");
			Listbox listaMonedaIncluir= (Listbox)this.getFellow("idLbxMultas");
	   		Listitem itemSeleccionadoIncluir = listaMonedaIncluir.getSelectedItem();
			Integer  resultado = Messagebox.show(
				    "�Confirma este pago? "+IConstantes.MENSAJE_PERMITIR_POPUPS,
				    "Confirmar Inicio Pago",
				    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
			if(resultado == Messagebox.YES){
				// definir las variables de invocaci�n del servicio
				String[] listServiciosMulti = null;
				String[] listNitMulti = null;
				double[] listValIvaMulti = null;
				double[] listIvaMulti = null;
				String wsResult;
			
				ReciboConsignacion reciboEncabezado = new ReciboConsignacion();
				Date fechaSistema = this.getFechaSistema();
				// si el pago es en l�nea debe realizarse a la fecha del sistema
				reciboEncabezado.setFechaPago(fechaSistema);
				reciboEncabezado.setFecha(fechaSistema);
				reciboEncabezado.setCliente(this.getCliente());
				reciboEncabezado.setBanderaDiaHabil(IConstantes.GENERAL_NO);
				reciboEncabezado.setBanderaFecha(IConstantes.GENERAL_NO);
				reciboEncabezado.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
				VencimientoPeriodo vencimientoPeriodo = new VencimientoPeriodo();
				vencimientoPeriodo.setFechaVencimiento(fechaSistema);
				reciboEncabezado.setPeriodo(vencimientoPeriodo);
				// calcular vencimientos a la fecha
				this.calcularVencimientosCliente(reciboEncabezado);
				Set<Listitem> listaItemsSeleccionados = listboxObligaciones.getSelectedItems();
				Iterator<Listitem> iteradorItems = listaItemsSeleccionados.iterator();
				
				Double interesCorriente = this.getInteresCorriente(reciboEncabezado);
				Double interesMora = this.getInteresMora(reciboEncabezado);
				Double obligacion = this.getObligacion();
				ReciboConsignacion recibo = new ReciboConsignacion();
				
				
				// si existen detalles o hay interes por pagar se registra un recibo
				if(obligacion+interesMora+interesCorriente> 0){
					this.registrarEncabezadoRecibo(reciboEncabezado);
					// verificar si el recibo si se registr�
					if(reciboEncabezado.getReciboConsignacion()!=null){
						
						try{
							// registramos los intereses del recibo
							ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarInteresesRecibo", reciboEncabezado);
							
							while(iteradorItems.hasNext()){
								recibo = (ReciboConsignacion)iteradorItems.next().getAttribute("RECIBO_CONSIGNACION");
								// registramos los detalles seleccionados 
								DetalleReciboConsignacion detalleRecibo = new DetalleReciboConsignacion();
								SaldoCartera saldoCartera = new SaldoCartera();
								saldoCartera.setNumeroCredito(recibo.getSaldoCartera().getNumeroCredito());
								saldoCartera.setDocumento(recibo.getSaldoCartera().getDocumento());
								saldoCartera.setOrganizacion(recibo.getSaldoCartera().getOrganizacion());
								recibo.setReciboConsignacion(reciboEncabezado.getReciboConsignacion());
								detalleRecibo.setReciboConsignacion(recibo);
								detalleRecibo.setSaldoCartera(saldoCartera);
								ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarDetalleRecibo", detalleRecibo);
						
							}
						
						
						reciboEncabezado.setValorDetalle(interesCorriente+interesMora+obligacion);
						ParametrizacionFac.getFacade().actualizarRegistro("actualizarEncabezadoRecibo", reciboEncabezado);
						
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				
				
				
			
			this.setZonaPagosLocator(new ZPagosLocator());
			this.setZonaPagosProxy(new ZPagosSoapProxy());
			com.zonapagos.www.test.ZPagosSoapProxy zonaPagosProxyPruebas = new com.zonapagos.www.test.ZPagosSoapProxy();
			
			Map<String, String> mapaParametros = new HashMap<String, String>();
			mapaParametros.put("ESTADO_PENDIENTE_INICIAR",IConstantes.ESTADO_PENDIENTE_INICIAR);
			mapaParametros.put("ESTADO_PENDIENTE_FINALIZAR",IConstantes.ESTADO_PENDIENTE_FINALIZAR);
			mapaParametros.put("ID_CLIENTE",reciboEncabezado.getCliente().getCliente().toString());
			List<TransaccionZonaPagos> listaDatos = new ArrayList<TransaccionZonaPagos>();
			listaDatos = null;
			
			try {
				listaDatos = (List<TransaccionZonaPagos>)ParametrizacionFac.getFacade().obtenerListado("selectTransaccionZonaPagosPendientes", mapaParametros);
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			
			ReciboConsignacion nuevoRecibo = new ReciboConsignacion();
			//fijamos el cliente 
			Cliente cliente = new Cliente();
			cliente.setCliente(reciboEncabezado.getCliente().getCliente());
			// se deben fijar los datos de la persona
			Persona persona = new Persona();
			persona.setDireccionElectronica(reciboEncabezado.getCliente().getPersona().getDireccionElectronica());
			persona.setNombres(reciboEncabezado.getCliente().getPersona().getNombreRazonSocial()+" "+reciboEncabezado.getCliente().getPersona().getPrimerApellido()+" "
			+reciboEncabezado.getCliente().getPersona().getSegundoApellido());
			persona.setNombreRazonSocial(reciboEncabezado.getCliente().getPersona().getNombreRazonSocial());
			persona.setPrimerApellido(reciboEncabezado.getCliente().getPersona().getPrimerApellido());
			persona.setSegundoApellido(reciboEncabezado.getCliente().getPersona().getSegundoApellido());
			persona.setTelefonoResidencia(reciboEncabezado.getCliente().getPersona().getTelefonoResidencia());
			persona.setIdentificacion(reciboEncabezado.getCliente().getPersona().getIdentificacion());
			TipoIdentificacion tipoIdentificacion = new TipoIdentificacion();
			tipoIdentificacion.setTipoIdentificacion(reciboEncabezado.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion());
			persona.setTipoIdentificacion(tipoIdentificacion);
			cliente.setPersona(persona);
			nuevoRecibo.setCliente(cliente);
			// fijamos Numero de recibo original
			nuevoRecibo.setReciboConsignacion(reciboEncabezado.getReciboConsignacion());
			// fijamos el periodo
			VencimientoPeriodo periodo = new VencimientoPeriodo();
			periodo.setFechaVencimiento(reciboEncabezado.getPeriodo().getFechaVencimiento()); // se define que siempre es la fecha de vencimiento
			PeriodoFacturacion periodoFact = new PeriodoFacturacion();
			periodoFact.setPeriodo(reciboEncabezado.getPeriodo().getPeriodo().getPeriodo());
			periodo.setPeriodo(periodoFact);
			nuevoRecibo.setPeriodo(periodo);
			// fijamos la cuenta bancaria
			CuentaReciboConsignacion cuenta = new CuentaReciboConsignacion();
			cuenta.setEntidad(reciboEncabezado.getCuentaReciboConsignacion().getEntidad());
			cuenta.setNumeroCuenta(reciboEncabezado.getCuentaReciboConsignacion().getNumeroCuenta());
			cuenta.setTipoEntidad(reciboEncabezado.getCuentaReciboConsignacion().getTipoEntidad());
			cuenta.setOficinaEntidad(reciboEncabezado.getCuentaReciboConsignacion().getOficinaEntidad());
			nuevoRecibo.setCuentaReciboConsignacion(cuenta);
			
			// fijamos fecha, genera comisi�n, numero fila y observaciones
			nuevoRecibo.setFecha(reciboEncabezado.getFecha());
			nuevoRecibo.setGeneraComision(reciboEncabezado.getGeneraComision());
			nuevoRecibo.setNumeroFila(reciboEncabezado.getNumeroFila());
			nuevoRecibo.setObservaciones(reciboEncabezado.getObservaciones());
			
			// fijamos valor detalle, vencido, total, identificador de saldos y banderas
				nuevoRecibo.setValorDetalle(obligacion+interesMora+interesCorriente);
				nuevoRecibo.setValorTotal(obligacion+interesMora+interesCorriente);
				//OJO: tener en cuenta fijar cada detalle en conversi�n porque puede tener comisi�n
				nuevoRecibo.setValorVencido(this.getCantidadConvertida(interesMora+interesCorriente, IConstantes.GENERAL_SI, reciboEncabezado.getFechaPago())+obligacion);
				nuevoRecibo.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
				nuevoRecibo.setBanderaFecha(IConstantes.GENERAL_NO);
			
			
			
			
			if(listaDatos.size()<=0){
			if (nuevoRecibo!= null){
				try {
					if(ReciboConsignacionHelper.getHelper().getServicioPruebasProduccion()){
						log.info("Invocamos el servicio de producci�n");
						wsResult = this.getZonaPagosProxy().inicio_pagoV2(
								((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)?
										(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
		            					&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
										Integer.parseInt(IConstantes.ID_TIENDA_BANCOLOMBIA):
										Integer.parseInt(IConstantes.ID_TIENDA):
											((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)?
													Integer.parseInt(IConstantes.ID_TIENDA_DOLARES):
														Integer.parseInt(IConstantes.ID_TIENDA_EURO), 
														((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)?
												(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
								            	&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
												IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
												IConstantes.CLAVE_SERVICIO:
													((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)?
															IConstantes.CLAVE_SERVICIO_DOLARES:
															IConstantes.CLAVE_SERVICIO_EURO,  
															((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)?
																	nuevoRecibo.getValorDetalle().doubleValue():
																	nuevoRecibo.getValorVencido().doubleValue(), 
								new Double(0).doubleValue(), 
								nuevoRecibo.getReciboConsignacion().toString(), 
								nuevoRecibo.getObservaciones().substring(0, nuevoRecibo.getObservaciones().length()>=70?69:nuevoRecibo.getObservaciones().length()), 
								nuevoRecibo.getCliente().getPersona().getDireccionElectronica(), 
								nuevoRecibo.getCliente().getCliente().toString(), 
								nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("C.C")?
									new String("1"):
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NIT")?
									new String("3"):
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("T.I")?
									new String("5"):
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("CEX")?	
									new String("2"):
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NEX")?			
									new String("10"):
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("RC")?		
									new String("9"):
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("PS")?			
									new String("6"):
									new String("11"), 
									nuevoRecibo.getCliente().getPersona().getNombreRazonSocial().substring(
										0,	nuevoRecibo.getCliente().getPersona().getNombreRazonSocial().length()>=50?49:
											nuevoRecibo.getCliente().getPersona().getNombreRazonSocial().length()), 
									(nuevoRecibo.getCliente().getPersona().getPrimerApellido()+" "+
											nuevoRecibo.getCliente().getPersona().getSegundoApellido()).substring(
													0, (nuevoRecibo.getCliente().getPersona().getPrimerApellido()+" "+
															nuevoRecibo.getCliente().getPersona().getSegundoApellido())
															.length()>=50?49:
																(nuevoRecibo.getCliente().getPersona().getPrimerApellido()+" "+
																		nuevoRecibo.getCliente().getPersona().getSegundoApellido())
																		.length()
															), 
									nuevoRecibo.getCliente().getPersona().getTelefonoResidencia().substring(0, 
											nuevoRecibo.getCliente().getPersona().getTelefonoResidencia().length()>=50?49:	
												nuevoRecibo.getCliente().getPersona().getTelefonoResidencia().length()), 
								new String(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
								getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor().toString()), 
								new String(nuevoRecibo.getValorVencido().toString()+" "+((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue()), 
								new String(nuevoRecibo.getPeriodo()!=null?
										nuevoRecibo.getPeriodo().getPeriodo()!=null?
												nuevoRecibo.getPeriodo().getPeriodo().getPeriodo():"":""), 
												((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)?
										(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
		            					&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
										IConstantes.CODIGO_SERVICIO_BANCOLOMBIA:
										IConstantes.CODIGO_SERVICIO:
											((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)?
													IConstantes.CODIGO_SERVICIO_DOLARES:
													IConstantes.CODIGO_SERVICIO_EURO,  
								listServiciosMulti,
								listNitMulti,
								listValIvaMulti,
								listIvaMulti, 
								Integer.parseInt(new String("0")));
					}else{
						log.info("Invocamos el servicio de pruebas");
						wsResult = zonaPagosProxyPruebas.inicio_pagoV2(
								((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)?
										(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
		            					&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
										Integer.parseInt(IConstantes.ID_TIENDA_BANCOLOMBIA):
										Integer.parseInt(IConstantes.ID_TIENDA):
											((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)?
													Integer.parseInt(IConstantes.ID_TIENDA_DOLARES):
														Integer.parseInt(IConstantes.ID_TIENDA_EURO)	
												, 
												((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)?
														(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
						            					&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
														IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
														IConstantes.CLAVE_SERVICIO:
															((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)?
																	IConstantes.CLAVE_SERVICIO_DOLARES:
																	IConstantes.CLAVE_SERVICIO_EURO, 
																	((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)?
																			nuevoRecibo.getValorDetalle().doubleValue():
																				nuevoRecibo.getValorVencido().doubleValue(), 
								new Double(0).doubleValue(), 
								nuevoRecibo.getReciboConsignacion().toString(), 
								nuevoRecibo.getObservaciones().substring(0, nuevoRecibo.getObservaciones().length()>=70?69:nuevoRecibo.getObservaciones().length()), 
								nuevoRecibo.getCliente().getPersona().getDireccionElectronica(), 
								nuevoRecibo.getCliente().getCliente().toString(), 
								nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("C.C")?
									new String("1"):
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NIT")?
									new String("3"):
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("T.I")?
									new String("5"):
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("CEX")?	
									new String("2"):
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NEX")?			
									new String("10"):
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("RC")?		
									new String("9"):
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("PS")?			
									new String("6"):
									new String("11"), 
									nuevoRecibo.getCliente().getPersona().getNombreRazonSocial().substring(
											0,	nuevoRecibo.getCliente().getPersona().getNombreRazonSocial().length()>=50?49:
												nuevoRecibo.getCliente().getPersona().getNombreRazonSocial().length()), 
										(nuevoRecibo.getCliente().getPersona().getPrimerApellido()+" "+
												nuevoRecibo.getCliente().getPersona().getSegundoApellido()).substring(
														0, (nuevoRecibo.getCliente().getPersona().getPrimerApellido()+" "+
																nuevoRecibo.getCliente().getPersona().getSegundoApellido())
																.length()>=50?49:
																	(nuevoRecibo.getCliente().getPersona().getPrimerApellido()+" "+
																			nuevoRecibo.getCliente().getPersona().getSegundoApellido())
																			.length()
																), 
										nuevoRecibo.getCliente().getPersona().getTelefonoResidencia().substring(0, 
												nuevoRecibo.getCliente().getPersona().getTelefonoResidencia().length()>=50?49:	
													nuevoRecibo.getCliente().getPersona().getTelefonoResidencia().length()), 
								new String(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
								getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor().toString()), 
								new String(nuevoRecibo.getValorVencido().toString()+" "+((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue()), 
								new String(nuevoRecibo.getPeriodo()!=null?
										nuevoRecibo.getPeriodo().getPeriodo()!=null?
												nuevoRecibo.getPeriodo().getPeriodo().getPeriodo():"":""), 
												((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)?
										(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
		            					&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
										IConstantes.CODIGO_SERVICIO_BANCOLOMBIA:
										IConstantes.CODIGO_SERVICIO:
											((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)?
													IConstantes.CODIGO_SERVICIO_DOLARES:
													IConstantes.CODIGO_SERVICIO_EURO, 
								listServiciosMulti,
								listNitMulti,
								listValIvaMulti,
								listIvaMulti, 
								Integer.parseInt(new String("0")));
					}
				log.info(wsResult);
				
				if (!wsResult.isEmpty()){
					if(!wsResult.startsWith("-1")){
						if(Long.parseLong(wsResult)>0){
							
							TransaccionZonaPagos transaccion = new TransaccionZonaPagos();
		            		transaccion.setSecTransaccionZonaPagos(null);
		            		transaccion.setIdPago(nuevoRecibo.getReciboConsignacion());
		            		transaccion.setEstadoPago(new Long(IConstantes.ESTADO_PENDIENTE_INICIAR));
		            		transaccion.setIdFormaPago(null);
		            		transaccion.setValorPagado(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)?
									nuevoRecibo.getValorDetalle().doubleValue():
										nuevoRecibo.getValorVencido().doubleValue());
		            		transaccion.setTicketId(null);
		            		transaccion.setIdClave(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)?
									(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
							            	&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
											IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
											IConstantes.CLAVE_SERVICIO:
												((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)?
														IConstantes.CLAVE_SERVICIO_DOLARES:
														IConstantes.CLAVE_SERVICIO_EURO
														);
		            		transaccion.setIdCliente(nuevoRecibo.getCliente().getCliente().toString());
		            		transaccion.setFranquicia(null);
		            		transaccion.setCodigoServicio(null);
		            		transaccion.setCodigoBanco(null);
		            		transaccion.setNombreBanco(null);
		            		transaccion.setCodigoTransaccion(null);
		            		transaccion.setCicloTransaccion(null);
		            		transaccion.setCampo1(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
									getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor().toString());
		            		transaccion.setCampo2(nuevoRecibo.getValorVencido().toString()+" "+((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue());
		            		transaccion.setCampo3(nuevoRecibo.getPeriodo()!=null?
		            				nuevoRecibo.getPeriodo().getPeriodo()!=null?
		            						nuevoRecibo.getPeriodo().getPeriodo().getPeriodo():"":"");
		            		transaccion.setIdComercio(new Long(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)?
									(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
			            					&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
											Integer.parseInt(IConstantes.ID_TIENDA_BANCOLOMBIA):
											Integer.parseInt(IConstantes.ID_TIENDA):
												((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)?
														Integer.parseInt(IConstantes.ID_TIENDA_DOLARES):
															Integer.parseInt(IConstantes.ID_TIENDA_EURO)));
		            		transaccion.setDatFecha(new Date());
		            		// no hay predecesor para este tipo de recibos
		            			transaccion.setIdPredecesor(null);
		            		
		            		
		            		ParametrizacionFac.getFacade().guardarRegistro("insertTransaccionZonaPagos", transaccion);
		            		log.info("registro guardado: "+transaccion.getSecTransaccionZonaPagos());
		            		log.info("corriendo el redirect");
		            		
		            		if(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)){
		            			if(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
		            					&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA)){
		            				Executions.sendRedirect(IConstantes.RUTA_SERVICIO_BANCOLOMBIA+wsResult);
		            					
		            			} else if (nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_OCCIDENTE) 
		            					&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_OCCIDENTE)){
		            				Executions.sendRedirect(IConstantes.RUTA_SERVICIO+wsResult);	
		            			} else{
		            				Executions.sendRedirect(IConstantes.RUTA_SERVICIO+wsResult);
		            			}
		            			
		            		}else if (((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)){
		            			//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO_DOLARES+wsResult);
		            			//Execution execution = Executions.getCurrent();
		            			Executions.sendRedirect(IConstantes.RUTA_SERVICIO_DOLARES+wsResult);
		            			//execution.setVoided(true);
		            		} else {
		            			Executions.sendRedirect(IConstantes.RUTA_SERVICIO_EURO+wsResult);
		            		}
		            		  Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesMultas"); 
			       			  rowsMensaje.getChildren().clear();
			       			  Mensaje mensaje = new Mensaje();
			       			  
			       			  mensaje.setMensaje(IConstantes.CONFIRMACION_INICIO_PAGO);
			       			  String tipoMensaje = IConstantes.CONFIRM;
			       			  this.onSetMensaje(mensaje, tipoMensaje);
								
							}else{ //else wsresult >0
								Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesMultas"); 
				       			  rowsMensaje.getChildren().clear();
				       			  Mensaje mensaje = new Mensaje();
				       			  
				       			  mensaje.setMensaje(IConstantes.ERROR_PAGO_INICIO+wsResult);
				       			  String tipoMensaje = IConstantes.ERROR;
				       			  this.onSetMensaje(mensaje, tipoMensaje);
							}
						}else{ // wsresult empieza -1
							Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesMultas"); 
			       			  rowsMensaje.getChildren().clear();
			       			  Mensaje mensaje = new Mensaje();
			       			  
			       			  mensaje.setMensaje(IConstantes.ERROR_PAGO_INICIO+wsResult);
			       			  String tipoMensaje = IConstantes.ERROR;
			       			  this.onSetMensaje(mensaje, tipoMensaje);
						}
					} else{// ws result nulo
						  Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesMultas"); 
		       			  rowsMensaje.getChildren().clear();
		       			  Mensaje mensaje = new Mensaje();
		       			  
		       			  mensaje.setMensaje(IConstantes.ERROR_PAGO_INICIO+wsResult);
		       			  String tipoMensaje = IConstantes.ERROR;
		       			  this.onSetMensaje(mensaje, tipoMensaje);
						
					}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				
				e.printStackTrace();
					}
				}
			
			
			} else if(listaDatos.size()>0){
				  Messagebox.show(
						    IConstantes.ERROR_PAGO_PENDIENTE,
						    "Error de Pagos en L�nea",
						    Messagebox.YES, Messagebox.ERROR);
				  Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesMultas"); 
				  rowsMensaje.getChildren().clear();
				  Mensaje mensaje = new Mensaje();
				  mensaje.setMensaje(IConstantes.ERROR_PAGO_PENDIENTE);
				  String tipoMensaje = IConstantes.ERROR;
				  this.onSetMensaje(mensaje, tipoMensaje);
					}
				}// fin si tiene algo para pagar
				else{
					 Messagebox.show(
							    IConstantes.ERROR_PAGO_SIN_VALOR,
							    "Error de Pagos en L�nea",
							    Messagebox.YES, Messagebox.ERROR);
					  Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesMultas"); 
					  rowsMensaje.getChildren().clear();
					  Mensaje mensaje = new Mensaje();
					  mensaje.setMensaje(IConstantes.ERROR_PAGO_SIN_VALOR);
					  String tipoMensaje = IConstantes.ERROR;
					  this.onSetMensaje(mensaje, tipoMensaje);
				}
			} 
			
			}
		
		
		public void onBuscarParametro()
		{
			PeriodoFacturacion periodoFacturacion = new PeriodoFacturacion();
			Textbox textoParametro = (Textbox)this.getFellow("parPeriodoMultas");
			periodoFacturacion.setPeriodo(textoParametro.getValue());
			VencimientoPeriodo vencimientoPeriodo = new VencimientoPeriodo();
			vencimientoPeriodo.setPeriodo(periodoFacturacion);
			
			this.buscarMaestro(vencimientoPeriodo);
		}

		public ZPagosLocator getZonaPagosLocator() {
			return zonaPagosLocator;
		}

		public void setZonaPagosLocator(ZPagosLocator zonaPagosLocator) {
			this.zonaPagosLocator = zonaPagosLocator;
		}

		public ZPagosSoapProxy getZonaPagosProxy() {
			return zonaPagosProxy;
		}

		public void setZonaPagosProxy(ZPagosSoapProxy zonaPagosProxy) {
			this.zonaPagosProxy = zonaPagosProxy;
		}


		
		public void onIniciarAbono(){
			AbonosIcebergAction abonosAction = (AbonosIcebergAction)Executions.createComponents("pages/abonosIceberg.zul", null, null);
			abonosAction.doModal();
		}
		

		
		@SuppressWarnings("rawtypes")
		public Double getFactorConversion(Date fechaConversion, String monedaOrigen, String monedaDestino){
			// Se realiza la consulta para traer la tasa
			Map<String, Object> mapaParametros = new HashMap<String,Object>();
						mapaParametros.put("MONEDA_ORIGEN",monedaOrigen);
						mapaParametros.put("MONEDA_DESTINO", monedaDestino);
						mapaParametros.put("FECHA", new SimpleDateFormat(IConstantes.FORMATO_FECHA).format(fechaConversion));
						BigDecimal factor = new BigDecimal(0.00);
						
						try {
							factor = (BigDecimal)ParametrizacionFac.getFacade().obtenerRegistro("getCurrencyFecha", mapaParametros);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						/* si encuentra factor entonces se coloca en el map*/
						if (factor!=null){
							if(factor.doubleValue()>0.00){
								mapaParametros.put("FACTOR", factor.doubleValue());
								
							} else {
								mapaParametros.put("FACTOR", ((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
										getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor());
							}
						}else{
							mapaParametros.put("FACTOR", ((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
									getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor());
						}
						
			return (Double)mapaParametros.get("FACTOR");
			
		}
		
		public void onSetImageCurrency(){
			Listbox lista = (Listbox)this.getFellow("idLbxMultas"); 
			Image imagen = (Image)this.getFellow("idImgMonedaMultas");
			ReciboConsignacionHelper.getHelper().onSetImageCurrency(lista, imagen);
		}
		
		
		
		public void onImprimirRecibo(){
			
			Datebox dateFechaPago = (Datebox)this.getFellow("idDtbxFechaPagoMultas");
			ReciboConsignacion reciboEncabezado= this.getReciboEncabezado(dateFechaPago.getValue(), dateFechaPago.getValue(), IConstantes.GENERAL_SI);
			
			// calcular vencimientos a la fecha
			this.calcularVencimientosCliente(reciboEncabezado);
			Listbox listboxObligaciones = (Listbox)this.getFellow("idLbxMaestrosQryMultas");
			Set<Listitem> listaItemsSeleccionados = listboxObligaciones.getSelectedItems();
			Iterator<Listitem> iteradorItems = listaItemsSeleccionados.iterator();
			
			Double interesCorriente = this.getInteresCorriente(reciboEncabezado);
			Double interesMora = this.getInteresMora(reciboEncabezado);
			Double obligacion = this.getObligacion();
			ReciboConsignacion recibo = new ReciboConsignacion();
			
			Integer  resultado = Messagebox.show(
				    "�Confirma la impresi�n de este recibo?; esto puede tardar varios segundos"+IConstantes.MENSAJE_PERMITIR_POPUPS,
				    "Confirmar impresi�n",
				    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
			if(resultado == Messagebox.YES){
			// si existen detalles o hay interes por pagar se registra un recibo
			if(obligacion+interesMora+interesCorriente> 0){
				this.registrarEncabezadoRecibo(reciboEncabezado);
				// verificar si el recibo si se registr�
				if(reciboEncabezado.getReciboConsignacion()!=null){
					
					try{
						// registramos los intereses del recibo
						ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarInteresesRecibo", reciboEncabezado);
						
						while(iteradorItems.hasNext()){
							recibo = (ReciboConsignacion)iteradorItems.next().getAttribute("RECIBO_CONSIGNACION");
							// registramos los detalles seleccionados 
							DetalleReciboConsignacion detalleRecibo = new DetalleReciboConsignacion();
							SaldoCartera saldoCartera = new SaldoCartera();
							saldoCartera.setNumeroCredito(recibo.getSaldoCartera().getNumeroCredito());
							saldoCartera.setDocumento(recibo.getSaldoCartera().getDocumento());
							saldoCartera.setOrganizacion(recibo.getSaldoCartera().getOrganizacion());
							recibo.setReciboConsignacion(reciboEncabezado.getReciboConsignacion());
							detalleRecibo.setReciboConsignacion(recibo);
							detalleRecibo.setSaldoCartera(saldoCartera);
							ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarDetalleRecibo", detalleRecibo);
					
						}
						
						reciboEncabezado.setValorDetalle(interesCorriente+interesMora+obligacion);
						ParametrizacionFac.getFacade().actualizarRegistro("actualizarEncabezadoRecibo", reciboEncabezado);
						
					}catch(Exception e){
						e.printStackTrace();
					}
					
					// realizamos impresi�n del recibo creado
					PrintReportAction reportAction = (PrintReportAction)Executions.createComponents("pages/printReport.zul", null, null);
					reportAction.doModal(IConstantes.REP_CUOTA_CREDITO, reciboEncabezado);
				
				} else{
					Messagebox.show(
						    IConstantes.MENSAJE_OPCION_NO_SELECCIONADA,
						    "Error de Pagos en L�nea",
						    Messagebox.YES, Messagebox.ERROR);
				  Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesMultas"); 
				  rowsMensaje.getChildren().clear();
				  Mensaje mensaje = new Mensaje();
				  mensaje.setMensaje(IConstantes.ERROR_GENERACION_RECIBO);
				  String tipoMensaje = IConstantes.ERROR;
				  this.onSetMensaje(mensaje, tipoMensaje);
				}
			} else{
				 Messagebox.show(
						    IConstantes.MENSAJE_OPCION_NO_SELECCIONADA,
						    "Error de Pagos en L�nea",
						    Messagebox.YES, Messagebox.ERROR);
				  Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesMultas"); 
				  rowsMensaje.getChildren().clear();
				  Mensaje mensaje = new Mensaje();
				  mensaje.setMensaje(IConstantes.MENSAJE_OPCION_NO_SELECCIONADA);
				  String tipoMensaje = IConstantes.ERROR;
				  this.onSetMensaje(mensaje, tipoMensaje);
			}
		}
	}
		

		
}