package com.konrad.action;

import java.io.File;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import com.konrad.assembler.ShoppingCartAssembler;
import com.konrad.domain.CCTAtributoProducto;
import com.konrad.domain.CentroCosto;
import com.konrad.domain.Cliente;
import com.konrad.domain.DetalleOrden;
import com.konrad.domain.DetalleReciboConsignacion;
import com.konrad.domain.Orden;
import com.konrad.domain.ProgramaReporte;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.SaldoCartera;
import com.konrad.domain.Usuario;
import com.konrad.domain.VencimientoPeriodo;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.helper.ReciboConsignacionHelper;
import com.konrad.util.IConstantes;
import com.konrad.window.ActionStandardBorder;

public class ShoppingCartAction extends ActionStandardBorder implements AfterCompose {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7244041161560749009L;
	
	private Set<Listitem> listaSeleccionados;
	private Set<Listitem> listaProductos;
	private Set<Listitem> listaFiltrados;
	private Orden orden;
	private Cliente cliente;
	private ReciboConsignacion reciboConsignacion;
	private List<ReciboConsignacion> listaRecibosOrdenesHistorico;
	private String indicadorCPC;

	protected static Logger log = Logger.getLogger(ShoppingCartAction.class);
			
	public void afterCompose() {		
		try { 
			this.buscarMaestro();
			this.setCliente(this.getClienteSesion());
			this.buscarOrdenesHistoricas();
			
			Button botonPago =(Button)this.getFellow("idBtnShoppingCartPagar");
			if(!ReciboConsignacionHelper.getHelper().validatePaymentDate()) {
				botonPago.setDisabled(true);
				Messagebox.show(
						   IConstantes.MENSAJE_BLOQUEO_HORA_PAGO.replace("{hora}", new SimpleDateFormat("HH:mm").format(new Date())),
						    "Advertencia de Pagos en L�nea",
						    Messagebox.YES, Messagebox.EXCLAMATION);
				
			}else {
				botonPago.setDisabled(false);
			}
			
			if(this.getListaRecibosOrdenesHistorico()!=null 
					&& this.getListaRecibosOrdenesHistorico().size()>0) {
				Integer contador =0;
				for(ReciboConsignacion recibo: this.getListaRecibosOrdenesHistorico()) {
					if(recibo.getPeriodo().getFechaVencimiento().compareTo(this.getFechaSistema())>=0) {
						contador++;
					}
				}
				
				
				
				if(contador>0) {
					Integer  resultado = Messagebox.show(
					    "Se han detectado pedidos pendientes de pago; �desea ir al hist�rico para visualizarlos?",
					    "Ver hist�rico",
					    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
					if(resultado == Messagebox.YES){
						
						
						
						this.irHistorico();
					}
				} 
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
	public void irHistorico() {
		this.buscarHistorico();
		this.switchHistoricoListadoCarrito(IConstantes.VER_HISTORICO_CARRITO);
	}
	
	public void switchHistoricoListadoCarrito(String visualizar) {
		if(visualizar.equalsIgnoreCase(IConstantes.VER_HISTORICO_CARRITO)) {
			((Div)this.getFellow("idDivShoppingCartBody")).setVisible(false);
			((Div)this.getFellow("idDivShoppingCartBotonesBody")).setVisible(false);
			((Div)this.getFellow("idDivBuscadorTotalesCarrito")).setVisible(false);
			((Div)this.getFellow("idDivShoppingCartOrdenesBody")).setVisible(true);
			((Div)this.getFellow("idDivShoppingCartBotonesOrdenes")).setVisible(true);
			
		}else {
			((Div)this.getFellow("idDivShoppingCartBody")).setVisible(true);
			((Div)this.getFellow("idDivShoppingCartBotonesBody")).setVisible(true);
			((Div)this.getFellow("idDivBuscadorTotalesCarrito")).setVisible(true);
			((Div)this.getFellow("idDivShoppingCartOrdenesBody")).setVisible(false);
			((Div)this.getFellow("idDivShoppingCartBotonesOrdenes")).setVisible(false);
		}	
	}
	
	public void buscarHistorico() {
		try {
			this.buscarOrdenesHistoricas();
			ShoppingCartAssembler shoppingAssembler = new ShoppingCartAssembler();
			Listbox listaOrdenes = (Listbox)this.getFellow("idListBoxShoppingCartOrdenes");
			listaOrdenes.getItems().clear();
			if(this.getListaRecibosOrdenesHistorico()!=null 
					&& this.getListaRecibosOrdenesHistorico().size()>0) {
				for (ReciboConsignacion recibo : this.getListaRecibosOrdenesHistorico() ) {
					Listitem item = shoppingAssembler.crearListItemOrdenDesdeDto(recibo, this);
					listaOrdenes.appendChild(item);
				}
				
				listaOrdenes.setMold("paging");
				listaOrdenes.setPageSize(IConstantes.TAMANO_PAGINACION_BANDBOX);
				listaOrdenes.applyProperties();
				listaOrdenes.invalidate();	
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void volverListadoCarrito() {
		switchHistoricoListadoCarrito(IConstantes.VER_CARRITO);
	}
	
	
	@SuppressWarnings("unchecked")
	public void buscarOrdenesHistoricas() {
		try {
			List<ReciboConsignacion> recibosResult = new ArrayList<ReciboConsignacion>();
			Cliente cliente = new Cliente();
			cliente = this.getCliente();
			ReciboConsignacion reciboParametro = new ReciboConsignacion();
			reciboParametro.setCliente(cliente);
			recibosResult = (List<ReciboConsignacion>)ParametrizacionFac
					.getFacade().obtenerListado("selectRecibosHistoricoCarrito", reciboParametro);
			this.setListaRecibosOrdenesHistorico(recibosResult);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void onSelectListItem() {
		Listbox listaItemsCarrito = (Listbox)this.getFellow("idListBoxShoppingCartPpal");
		Label cantidadLabel = (Label)this.getFellow("idLabelShoppingCartQty");
		Label totalLabel = (Label)this.getFellow("idLabelShoppingCartTotal");
		Integer cantidad = 0;
		Double valorTotal = 0d;
		Double subTotal = 0d;
		Double subIva = 0d;
		Double subDescuento = 0d;
		Set<Listitem> seleccionados = new HashSet<Listitem>();
		seleccionados.addAll(listaItemsCarrito.getSelectedItems());
		this.setListaSeleccionados(seleccionados);
		Boolean indicadorCPC = false;
		for(Listitem item : listaItemsCarrito.getSelectedItems()) {
			
			DetalleOrden detalleOrden = (DetalleOrden)item.getAttribute("DETALLE_ORDEN");
			
			Map<String,Object> mapaProducto = new HashMap<String,Object>();
			mapaProducto.put("PRODUCTO", detalleOrden.getReferencia());
			String esProductoCPC ="";
			try{
				esProductoCPC = (String) ParametrizacionFac.getFacade().obtenerRegistro("selectIndicadorCPC", mapaProducto);
			}
			catch(Exception e) {
				e.printStackTrace();
				}
			if(esProductoCPC==null) {
				esProductoCPC = "N";
			}
			if(esProductoCPC.equalsIgnoreCase("S")) {
				indicadorCPC= true;
			} else {
				((Intbox)((Listcell)item.getChildren().get(5)).getFirstChild()).setDisabled(false);	
			}
			
			subTotal= detalleOrden.getPrecio()
					*((Intbox)((Listcell)item.getChildren().get(5)).getFirstChild()).getValue();
			subIva = subTotal*(detalleOrden.getIva()/100);
			subDescuento = subTotal*(detalleOrden.getDescuento()/100);
			subTotal = subTotal+subIva-subDescuento;
			((Label)((Listcell)item.getChildren().get(6)).getFirstChild()).setValue(
					new DecimalFormat("'$'###,###,###.##").format(subTotal));
			cantidad+=((Intbox)((Listcell)item.getChildren().get(5)).getFirstChild()).getValue();
			valorTotal+=subTotal;
		}
			
		cantidadLabel.setValue(new DecimalFormat("######").format(cantidad));
		totalLabel.setValue(new DecimalFormat("'$'###,###,###.##").format(valorTotal));
			
		for (Listitem item : listaItemsCarrito.getItems()) {
			if(!item.isSelected()) {
				((Intbox)((Listcell)item.getChildren().get(5)).getFirstChild()).setDisabled(true);
				((Intbox)((Listcell)item.getChildren().get(5)).getFirstChild()).setValue(1);
				DetalleOrden detalleOrden = (DetalleOrden)item.getAttribute("DETALLE_ORDEN");
				subTotal= detalleOrden.getPrecio()
						*((Intbox)((Listcell)item.getChildren().get(5)).getFirstChild()).getValue();
				subIva = subTotal*(detalleOrden.getIva()/100);
				subDescuento = subTotal*(detalleOrden.getDescuento()/100);
				subTotal = subTotal+subIva-subDescuento;
					((Label)((Listcell)item.getChildren().get(6)).getFirstChild()).setValue(
						new DecimalFormat("'$'###,###,###.##").format(subTotal));
			}
		}
		
		this.setDisabledItems(indicadorCPC, listaItemsCarrito);
	}
	
	public void setDisabledItems(Boolean indicadorCPC, Listbox listaItems) {
		try {
			Map<String,Object> mapaProducto = new HashMap<String,Object>();
			String esProductoCPC = "";
			Double subTotal = 0d;
			Double subIva = 0d;
			Double subDescuento = 0d;
			Integer cantidad = 0;
			Double valorTotal = 0d;
			Label cantidadLabel = (Label)this.getFellow("idLabelShoppingCartQty");
			Label totalLabel = (Label)this.getFellow("idLabelShoppingCartTotal");
			for (Listitem item : listaItems.getItems()) {
				if(indicadorCPC) {
					DetalleOrden detalleOrden = (DetalleOrden)item.getAttribute("DETALLE_ORDEN");
					mapaProducto.put("PRODUCTO", detalleOrden.getReferencia());
					esProductoCPC= (String) ParametrizacionFac.getFacade().obtenerRegistro("selectIndicadorCPC", mapaProducto);
					if(esProductoCPC==null) {
						esProductoCPC = "N";
					}
					if(esProductoCPC.equalsIgnoreCase("N")) {
						
							((Intbox)((Listcell)item.getChildren().get(5)).getFirstChild()).setDisabled(true);
							((Intbox)((Listcell)item.getChildren().get(5)).getFirstChild()).setValue(1);
							subTotal= detalleOrden.getPrecio()
								*((Intbox)((Listcell)item.getChildren().get(5)).getFirstChild()).getValue();
							subIva = subTotal*(detalleOrden.getIva()/100);
							subDescuento = subTotal*(detalleOrden.getDescuento()/100);
							subTotal = subTotal+subIva-subDescuento;
							((Label)((Listcell)item.getChildren().get(6)).getFirstChild()).setValue(
								new DecimalFormat("'$'###,###,###.##").format(subTotal));
							item.setDisabled(true);
							item.setSelected(false);
						
					} 
				} else {
					item.setDisabled(false);
				}
			}	
			
			for(Listitem item : listaItems.getSelectedItems()) {
				DetalleOrden detalleOrden = (DetalleOrden)item.getAttribute("DETALLE_ORDEN");
				mapaProducto.put("PRODUCTO", detalleOrden.getReferencia());
				esProductoCPC= (String) ParametrizacionFac.getFacade().obtenerRegistro("selectIndicadorCPC", mapaProducto);
				if(esProductoCPC==null) {
					esProductoCPC = "N";
				}
				if(esProductoCPC.equalsIgnoreCase("N")) {
					((Intbox)((Listcell)item.getChildren().get(5)).getFirstChild()).setDisabled(false);
				}
				subTotal= detalleOrden.getPrecio()
						*((Intbox)((Listcell)item.getChildren().get(5)).getFirstChild()).getValue();
				subIva = subTotal*(detalleOrden.getIva()/100);
				subDescuento = subTotal*(detalleOrden.getDescuento()/100);
				subTotal = subTotal+subIva-subDescuento;
				((Label)((Listcell)item.getChildren().get(6)).getFirstChild()).setValue(
						new DecimalFormat("'$'###,###,###.##").format(subTotal));
				cantidad+=((Intbox)((Listcell)item.getChildren().get(5)).getFirstChild()).getValue();
				valorTotal+=subTotal;
			}
			
			cantidadLabel.setValue(new DecimalFormat("######").format(cantidad));
			totalLabel.setValue(new DecimalFormat("'$'###,###,###.##").format(valorTotal));
			
		} catch(Exception e) {
			e.printStackTrace();
		}	
	}
	
	public void actualizarListado() {
		Integer  resultado = Messagebox.show(
				 "�Confirma actualizaci�n?; perder� la selecci�n de productos realizada."+IConstantes.MENSAJE_PERMITIR_POPUPS,
				 "Confirmar actualizaci�n",
				 Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
		if(resultado == Messagebox.YES){
			this.buscarMaestro();	
			this.onSelectListItem();
			Button botonPago =(Button)this.getFellow("idBtnShoppingCartPagar");
			if(!ReciboConsignacionHelper.getHelper().validatePaymentDate()) {
				botonPago.setDisabled(true);
			}else {
				botonPago.setDisabled(false);
			}
		}
	}
			
	public Date getFechaSistema(){
		Date fechaSistema = null;
		try{
			fechaSistema = new SimpleDateFormat(IConstantes.FORMATO_FECHA).parse((String)ParametrizacionFac.getFacade().obtenerRegistro("selectFechaSistema"));
		}catch(Exception e){
			e.printStackTrace();
			Messagebox.show("No se pudo obtener la fecha de sistema","Error", Messagebox.OK, Messagebox.ERROR);
		}

		return fechaSistema;
	}
		
		public Cliente getClienteSesion(){
			Cliente cliente = new Cliente();
			cliente.setCliente(new Long(((Usuario)this.getDesktop().getSession().getAttribute(IConstantes.USUARIO_SESSION)).getUsuario()));
			return cliente;
		}
		
		
	@SuppressWarnings("unchecked")
	public void assembleProducts() {
		log.info("[ShoppingCart][assembleProducts]");
		try {
		List<DetalleOrden> listaDetallesProductos = new ArrayList<DetalleOrden>();
		listaDetallesProductos = 
				(List<DetalleOrden>)ParametrizacionFac.getFacade().obtenerListado("selectProductosCarrito");
		ShoppingCartAssembler shoppingAssembler = new ShoppingCartAssembler();
		Listbox listaProductos = (Listbox)this.getFellow("idListBoxShoppingCartPpal");
		listaProductos.getItems().clear();
		Map<String,Object> mapaProducto = new HashMap<String,Object>();
		String stringListaEstratoProductos = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectListadoEstratoProductosCPC");
		String[] listaEstratoProductos = stringListaEstratoProductos.split(",");
		
		for(DetalleOrden detalle : listaDetallesProductos) {			
			mapaProducto.put("PRODUCTO", detalle.getReferencia());
			String esProductoCPC = (String) ParametrizacionFac.getFacade().obtenerRegistro("selectIndicadorCPC", mapaProducto);
			if(esProductoCPC==null) {
				esProductoCPC = "N";
			}
			if(esProductoCPC.equalsIgnoreCase("S")) {
				Cliente clienteSesion = this.getClienteSesion();
				Cliente clienteBasico = (Cliente)ParametrizacionFac.getFacade().obtenerRegistro("selectClienteBasico", clienteSesion);
				
				Map<String,String> mapaEstratoProducto = new HashMap<String,String>();
				
				String estrato = "";
				int i= 0;
				for(String elemento: listaEstratoProductos) {
					if(i%2==0) {
					estrato = elemento;
					} else {
						mapaEstratoProducto.put(estrato, elemento);
					}
					i++;
				}
				estrato = clienteBasico.getCanal().getPrioridad().toString();
				
				for(Entry<String,String> entrada : mapaEstratoProducto.entrySet()) {
					if(entrada.getKey().equalsIgnoreCase(estrato) 
							&& entrada.getValue().equalsIgnoreCase(detalle.getReferencia().toString())) {
						
						Listitem fila = shoppingAssembler.crearListItemDesdeDto(detalle, this);
						listaProductos.appendChild(fila);
					}
				}
				
			}else {
				Listitem fila = shoppingAssembler.crearListItemDesdeDto(detalle, this);
				listaProductos.appendChild(fila);
			}
		}
		
		listaProductos.setMold("paging");
		listaProductos.setPageSize(IConstantes.TAMANO_PAGINACION_BANDBOX);
		listaProductos.applyProperties();
		listaProductos.invalidate();
		Set<Listitem> listaProductosSet = new HashSet<Listitem>();
		listaProductosSet.addAll(listaProductos.getItems());
		this.setListaProductos(listaProductosSet);
		
		} catch(Exception e) {
			log.error("[assembleProducts]["+e.getMessage()+"]");
		}
	}
		
	public void onFilterProducts() {
		Listbox listaProductos = (Listbox)this.getFellow("idListBoxShoppingCartPpal");
		Textbox textoBusqueda = (Textbox)this.getFellow("idShoppingCartTextboxBuscar");
		if(textoBusqueda.getValue()== null) {
			textoBusqueda.setValue("");
		}
		
		listaProductos.getItems().clear();
		
		if(this.getListaSeleccionados()!=null && this.getListaSeleccionados().size()>0) {
			for(Listitem item: this.getListaSeleccionados()) {
				item.setSelectable(true);
				item.setSelected(true);
				listaProductos.appendChild(item);
			}
		}
		
		if(!textoBusqueda.getValue().equals("")) {
			
			for(Listitem item: this.getListaProductos()) {
				
				DetalleOrden detalleOrden = new DetalleOrden(); 
				detalleOrden = (DetalleOrden)item.getAttribute("DETALLE_ORDEN");
				
				if(detalleOrden !=null && detalleOrden.getNombreReferencia()!=null) {
					if(detalleOrden.getNombreReferencia().toUpperCase().contains(textoBusqueda.getValue().toUpperCase())
						&& !listaProductos.getItems().contains(item)) {
						listaProductos.appendChild(item);
					}
				}
			}
		}else {
			
			for(Listitem item: this.getListaProductos()) {
				if(!listaProductos.getItems().contains(item)) {
					listaProductos.appendChild(item);
				}
			}
		}
		
		this.onSelectListItem();
	}
	
	
	public void crearOrdenCarrito() {
		try{
			this.onSelectListItem();
			if(this.getListaSeleccionados()!=null && this.getListaSeleccionados().size()>0) {
				String descripcion = "COMPRA DE PRODUCTOS : ";
				Orden orden = new Orden();
				Cliente cliente = new Cliente(); 
						cliente = this.getCliente();
				Map<String,Object> mapaProducto = new HashMap<String,Object>();
				Boolean indicadorCPC = false;
				orden.setCliente(cliente);
				log.info("Cliente en generaci�n de orden: "+orden.getCliente().getCliente());
				for(Listitem item: this.getListaSeleccionados()) {
					DetalleOrden detalleSel = (DetalleOrden)item.getAttribute("DETALLE_ORDEN");
					
					mapaProducto.put("PRODUCTO", detalleSel.getReferencia());
					String esProductoCPC = (String) ParametrizacionFac.getFacade().obtenerRegistro("selectIndicadorCPC", mapaProducto);
					if(esProductoCPC==null) {
						esProductoCPC = "N";
					}
					
					if(esProductoCPC.equalsIgnoreCase("S")) {
						indicadorCPC=true;
					}
					descripcion += detalleSel.getNombreReferencia()+" ("+
							String.valueOf(((Intbox)((Listcell)
									item.getChildren().get(5)).getFirstChild()).getValue())+") ";
				}
				if (indicadorCPC) {
					orden.setIndicadorCPC("S");
				}else {
					orden.setIndicadorCPC("N");
				}
				orden.setDescripcion(descripcion);
				ParametrizacionFac.getFacade().ejecutarProcedimiento("insertOrdenEncabezado", orden);
			
				if(orden.getOrden() != null && orden.getOrden()> 0){
					for(Listitem item: this.getListaSeleccionados()) {
						DetalleOrden detalle = (DetalleOrden)item.getAttribute("DETALLE_ORDEN");
						DetalleOrden detalleGrabar = new DetalleOrden();
						detalleGrabar.setPrecio(detalle.getPrecio());
						detalleGrabar.setCantidad(
							Double.valueOf(((Intbox)((Listcell)
									item.getChildren().get(5)).getFirstChild()).getValue()));
						detalleGrabar.setDescuento(detalle.getDescuento());
						detalleGrabar.setIva(detalle.getIva());
						detalleGrabar.setDescripcion(detalle.getNombreReferencia()+" ("+
								String.valueOf(((Intbox)((Listcell)
										item.getChildren().get(5)).getFirstChild()).getValue())+") ");
						detalleGrabar.setReferencia(detalle.getReferencia());
						detalleGrabar.setNombreReferencia(detalle.getNombreReferencia());
						detalleGrabar.setOrden(orden);
					
						ParametrizacionFac.getFacade().ejecutarProcedimiento("insertDetalleOrdenCarrito",detalleGrabar);
					}
					
					Orden ordenParametro = new Orden();
					ordenParametro.setOrden(orden.getOrden());
					ordenParametro.setOrganizacion(orden.getOrganizacion());
					ordenParametro.setDocumento(orden.getDocumento());
					ordenParametro.setEstado(IConstantes.ESTADO_CONFIRMADO_ORDEN);
					log.info("orden: "+ordenParametro.getOrden()+" documento: "+ordenParametro.getDocumento()+
							" organizacion: "+ordenParametro.getOrganizacion());
					ParametrizacionFac.getFacade().ejecutarProcedimiento("actualizarEstadoOrdenCarrito", ordenParametro);
					ReciboConsignacion recibo = new ReciboConsignacion();
					Map<String, Object> mapaParametros = new HashMap<String,Object>();
					mapaParametros.put("ORDEN", orden.getOrden());
					mapaParametros.put("ORGANIZACION", orden.getOrganizacion());
					mapaParametros.put("DOCUMENTO", orden.getDocumento());
					mapaParametros.put("RECIBO_INICIAL", 0L);
					mapaParametros.put("RECIBO_FINAL", 0L);
					ParametrizacionFac.getFacade().ejecutarProcedimiento("generarReciboOrdenCarrito", mapaParametros);
					recibo.setOrden(orden);
					recibo.setReciboConsignacion(Long.valueOf(mapaParametros.get("RECIBO_INICIAL").toString()));
					this.setOrden(orden);
					this.setReciboConsignacion(recibo);
				}else {
					Messagebox.show("No fue posible generar la orden de pago.");
				}
			
			} else {
				Messagebox.show("No se ha seleccionado producto alguno.");
			}
		}catch(Exception e) {
			e.printStackTrace();
			Messagebox.show("No fue posible generar la orden de pago. "+e.getMessage());
		}
	}
	
	public void buscarMaestro(){
		try {
			log.info("Ejecutando el m�todo [ buscarMaestro - shoppingCart ]... ");
			this.assembleProducts();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
				
		
	public void onTestPagoOnline(){
		Listbox listboxObligaciones = (Listbox)this.getFellow("idLbxMaestrosQryCartera");
 
		Integer  resultado = Messagebox.show(
			    "�Confirma este pago? "+IConstantes.MENSAJE_PERMITIR_POPUPS,
			    "Confirmar Inicio Pago",
			    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
		if(resultado == Messagebox.YES){
			// definir las variables de invocaci�n del servicio	
			ReciboConsignacion reciboEncabezado = new ReciboConsignacion();
			Date fechaSistema = this.getFechaSistema();
			// si el pago es en l�nea debe realizarse a la fecha del sistema
			reciboEncabezado.setFechaPago(fechaSistema);
			reciboEncabezado.setFecha(fechaSistema);
			reciboEncabezado.setCliente(this.getCliente());
			reciboEncabezado.setBanderaDiaHabil(IConstantes.GENERAL_NO);
			reciboEncabezado.setBanderaFecha(IConstantes.GENERAL_NO);
			reciboEncabezado.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
			VencimientoPeriodo vencimientoPeriodo = new VencimientoPeriodo();
			vencimientoPeriodo.setFechaVencimiento(fechaSistema);
			reciboEncabezado.setPeriodo(vencimientoPeriodo);
			// calcular vencimientos a la fecha
			
			Set<Listitem> listaItemsSeleccionados = listboxObligaciones.getSelectedItems();
			Iterator<Listitem> iteradorItems = listaItemsSeleccionados.iterator();
			
			ReciboConsignacion recibo = new ReciboConsignacion();
			
			// si existen detalles o hay interes por pagar se registra un recibo
			
				//no registramos encabezado por pruebas 
				//this.registrarEncabezadoRecibo(reciboEncabezado);
				// verificar si el recibo si se registr�
				reciboEncabezado.setReciboConsignacion(136090L);
				if(reciboEncabezado.getReciboConsignacion()!=null){
					
					try{
						// no registramos intereses por pruebas registramos los intereses del recibo
						//ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarInteresesRecibo", reciboEncabezado);
						
						while(iteradorItems.hasNext()){
							recibo = (ReciboConsignacion)iteradorItems.next().getAttribute("RECIBO_CONSIGNACION");
							// registramos los detalles seleccionados 
							DetalleReciboConsignacion detalleRecibo = new DetalleReciboConsignacion();
							SaldoCartera saldoCartera = new SaldoCartera();
							saldoCartera.setNumeroCredito(recibo.getSaldoCartera().getNumeroCredito());
							saldoCartera.setDocumento(recibo.getSaldoCartera().getDocumento());
							saldoCartera.setOrganizacion(recibo.getSaldoCartera().getOrganizacion());
							recibo.setReciboConsignacion(reciboEncabezado.getReciboConsignacion());
							detalleRecibo.setReciboConsignacion(recibo);
							detalleRecibo.setSaldoCartera(saldoCartera);
							System.out.println("Detalle del recibo --- " +detalleRecibo.getSaldoCartera().getDocumento()+" - "+detalleRecibo.getSaldoCartera().getNumeroCredito()
									+" Recibo a afectar: "+detalleRecibo.getReciboConsignacion().getReciboConsignacion());
							ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarDetalleRecibo", detalleRecibo);
					
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
	}
	
	public void onAnularOrden(ReciboConsignacion reciboConsignacion) {
		try {
			Integer  resultado = Messagebox.show(
				    "�Confirma anulaci�n de este pedido? ",
				    "Confirmar anulaci�n",
				    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
			if(resultado==Messagebox.YES){
				ParametrizacionFac.getFacade().actualizarRegistro("anularOrdenCarrito", reciboConsignacion);
				this.buscarHistorico();
			}
		}catch(Exception e) {
			e.printStackTrace();
			
		}
	}
	
	public void onPagarEnLineaPayUW(ReciboConsignacion reciboConsignacion){
		try{
			ReciboConsignacion reciboPagar = new ReciboConsignacion();
			this.setReciboConsignacion(reciboConsignacion);
		
			Integer  resultado = Messagebox.show(
				    "�Confirma este pago? "+IConstantes.MENSAJE_PERMITIR_POPUPS,
				    "Confirmar Inicio Pago",
				    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
			if(resultado==Messagebox.YES){
				log.info("recibo antes de consultar: "+this.getReciboConsignacion().getReciboConsignacion());
				reciboPagar = reciboConsignacion;
				log.info("recibo despues de consultar: "+this.getReciboConsignacion().getReciboConsignacion());
				log.info("recibo pagar despues de consultar: "+reciboPagar.getReciboConsignacion());
					ReciboConsignacionHelper.getHelper().sendPostPayU(reciboPagar, IConstantes.TIPO_INVOCACION_DERECHOS_ACADEMICOS);
					ReciboConsignacionHelper.getHelper().registrarAuditoriaPayU(reciboPagar, IConstantes.TIPO_INVOCACION_DERECHOS_ACADEMICOS);
			}
		
		
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
			Messagebox.show("Error generaci�n recibo: "+e.getMessage(), "Error de Pagos en L�nea",
				    Messagebox.YES, Messagebox.ERROR);
		}
	}
		
	@SuppressWarnings("unchecked")
	public void onPagarEnLineaPayUW(){
		try{
		ReciboConsignacion reciboPagar = new ReciboConsignacion();
		
		if(this.getListaSeleccionados()!=null && this.getListaSeleccionados().size()>0) {
			Integer  resultado = Messagebox.show(
				    "�Confirma este pago? "+IConstantes.MENSAJE_PERMITIR_POPUPS,
				    "Confirmar Inicio Pago",
				    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
			if(resultado==Messagebox.YES){
				this.crearOrdenCarrito();
				log.info("recibo antes de consultar: "+this.getReciboConsignacion().getReciboConsignacion());
				reciboPagar = ((List<ReciboConsignacion>)ParametrizacionFac.getFacade().
							obtenerListado("selectRecibosConsignacionOrdenesByRecibo", this.getReciboConsignacion())).get(0);
				log.info("recibo despues de consultar: "+this.getReciboConsignacion().getReciboConsignacion());
				log.info("recibo pagar despues de consultar: "+reciboPagar.getReciboConsignacion());
					ReciboConsignacionHelper.getHelper().sendPostPayU(reciboPagar, IConstantes.TIPO_INVOCACION_DERECHOS_ACADEMICOS);
					ReciboConsignacionHelper.getHelper().registrarAuditoriaPayU(reciboPagar, IConstantes.TIPO_INVOCACION_DERECHOS_ACADEMICOS);
			}
		}else {
			Messagebox.show(
				    IConstantes.MENSAJE_OPCION_NO_SELECCIONADA,
				    "Error de Pagos en L�nea",
				    Messagebox.YES, Messagebox.ERROR);
		}
		
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage());
			Messagebox.show("Error generaci�n recibo: "+e.getMessage(), "Error de Pagos en L�nea",
				    Messagebox.YES, Messagebox.ERROR);
		}
	}
		
	
		public void onImprimirRecibo(ReciboConsignacion reciboConsignacion) {
			try {
			Integer  resultado = Messagebox.show(
				    "�Confirma la impresi�n de este recibo?; esto puede tardar varios segundos"+IConstantes.MENSAJE_PERMITIR_POPUPS,
				    "Confirmar impresi�n",
				    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
			
				if(resultado == Messagebox.YES){
					ReciboConsignacion recibo = new ReciboConsignacion();
					recibo =reciboConsignacion;
					ProgramaReporte programaReporte = new ProgramaReporte();
					CentroCosto centroCosto = new CentroCosto();
					centroCosto.setCentroCosto(recibo.getOrden().getCentroCosto().getCentroCosto());
					programaReporte.setCentroCosto(centroCosto);
					ProgramaReporte programaReporteAux = 
							(ProgramaReporte)ParametrizacionFac.getFacade().
							obtenerRegistro("selectProgramaReporte", programaReporte);
					
					Map<String,Object> mapaParametros = new HashMap<String, Object>();
					mapaParametros.put("ORDEN", recibo.getOrden().getOrden());
					mapaParametros.put("ORGANIZACION", recibo.getOrden().getOrganizacion());
					mapaParametros.put("DOCUMENTO", recibo.getOrden().getDocumento());
					mapaParametros.put("CLIENTE", recibo.getOrden().getCliente().getCliente());

					PrintReportAction printReportAction = 
							(PrintReportAction)Executions.createComponents("pages/printReport.zul", null, null);
					printReportAction.doModal(programaReporteAux!=null?
							programaReporteAux.getReporte():IConstantes.REP_ORDEN, recibo);	
				}
			} catch(Exception e){
				e.printStackTrace();
			}	
		}
		
		public void onImprimirProducto(DetalleOrden detalleOrden) {
			PrintReportAction printReportAction = 
					(PrintReportAction)Executions.createComponents("pages/printReport.zul", null, null);
			printReportAction.imprimirProducto(detalleOrden);
		}
		
		public void onImprimirRecibo(){
			try {
			if(this.getListaSeleccionados()!=null && this.getListaSeleccionados().size()>0) {
				
				Integer  resultado = Messagebox.show(
				    "�Confirma la impresi�n de este recibo?; esto puede tardar varios segundos"+IConstantes.MENSAJE_PERMITIR_POPUPS,
				    "Confirmar impresi�n",
				    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
				if(resultado == Messagebox.YES){
					// si existen detalles o hay interes por pagar se registra un recibo
					this.crearOrdenCarrito();
					ReciboConsignacion recibo = new ReciboConsignacion();
					recibo = this.getReciboConsignacion();
					ProgramaReporte programaReporte = new ProgramaReporte();
					CentroCosto centroCosto = new CentroCosto();
					centroCosto.setCentroCosto(recibo.getOrden().getCentroCosto().getCentroCosto());
					programaReporte.setCentroCosto(centroCosto);
					ProgramaReporte programaReporteAux = 
							(ProgramaReporte)ParametrizacionFac.getFacade().
							obtenerRegistro("selectProgramaReporte", programaReporte);
					
					Map<String,Object> mapaParametros = new HashMap<String, Object>();
					mapaParametros.put("ORDEN", recibo.getOrden().getOrden());
					mapaParametros.put("ORGANIZACION", recibo.getOrden().getOrganizacion());
					mapaParametros.put("DOCUMENTO", recibo.getOrden().getDocumento());
					mapaParametros.put("CLIENTE", recibo.getOrden().getCliente().getCliente());

					PrintReportAction printReportAction = 
							(PrintReportAction)Executions.createComponents("pages/printReport.zul", null, null);
					printReportAction.doModal(programaReporteAux!=null?
							programaReporteAux.getReporte():IConstantes.REP_ORDEN, recibo);
					
					// realizamos impresi�n del recibo creado
					
				} else{
					Messagebox.show(
						    IConstantes.MENSAJE_OPCION_NO_SELECCIONADA,
						    "Error de Pagos en L�nea",
						    Messagebox.YES, Messagebox.ERROR);
				}
			} else{
				 Messagebox.show(
						    IConstantes.MENSAJE_OPCION_NO_SELECCIONADA,
						    "Error de Pagos en L�nea",
						    Messagebox.YES, Messagebox.ERROR);
			}
			} catch(Exception e) {
				e.printStackTrace();
				Messagebox.show("Error generaci�n recibo: "+e.getMessage());
			}	
		}
		
		public List<File> listFilesForFolder(final File folder) {
			List<File> listaArchivos = new ArrayList<File>();
		    for (final File fileEntry : folder.listFiles()) {
		        if (fileEntry.isDirectory()) {
		            listFilesForFolder(fileEntry);
		        } else {
		        	log.info("archivo: "+fileEntry.getName());
		            listaArchivos.add(fileEntry);
		        }
		    }
		    return listaArchivos;
		}

		public void insertarRegistrosAtributosProducto() {
			try {
			final File folder = new File("d:/KonradLorenz/archivosProductos/");
			List<File> listaArchivos = listFilesForFolder(folder);
			if(listaArchivos!=null && listaArchivos.size()>0) {
				for(File archivo : listaArchivos) {
					CCTAtributoProducto atributo = new CCTAtributoProducto();
					atributo.setReferencia(Long.valueOf(archivo.getName().replace(".pdf", "").trim()));
					atributo.setDescuento(new BigDecimal("0.00"));
					atributo.setTipoDescuento("P");
					atributo.setImpuesto(new BigDecimal("0.00"));
					atributo.setTipoImpuesto("P");
					atributo.setFotoProducto(FileUtils.readFileToByteArray(archivo));
					ParametrizacionFac.getFacade().guardarRegistro("insertCCTAtributoProducto", atributo);
				}
			}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		

		public Set<Listitem> getListaSeleccionados() {
			return listaSeleccionados;
		}

		public void setListaSeleccionados(Set<Listitem> listaSeleccionados) {
			this.listaSeleccionados = listaSeleccionados;
		}

		public Set<Listitem> getListaFiltrados() {
			return listaFiltrados;
		}

		public void setListaFiltrados(Set<Listitem> listaFiltrados) {
			this.listaFiltrados = listaFiltrados;
		}

		public Set<Listitem> getListaProductos() {
			return listaProductos;
		}

		public void setListaProductos(Set<Listitem> listaProductos) {
			this.listaProductos = listaProductos;
		}

		public Orden getOrden() {
			return orden;
		}

		public void setOrden(Orden orden) {
			this.orden = orden;
		}
		
		public Cliente getCliente() {
			return cliente;
		}

		public void setCliente(Cliente cliente) {
			this.cliente = cliente;
		}

		public ReciboConsignacion getReciboConsignacion() {
			return reciboConsignacion;
		}

		public void setReciboConsignacion(ReciboConsignacion reciboConsignacion) {
			this.reciboConsignacion = reciboConsignacion;
		}

		public List<ReciboConsignacion> getListaRecibosOrdenesHistorico() {
			return listaRecibosOrdenesHistorico;
		}

		public void setListaRecibosOrdenesHistorico(List<ReciboConsignacion> listaRecibosOrdenesHistorico) {
			this.listaRecibosOrdenesHistorico = listaRecibosOrdenesHistorico;
		}

		public String getIndicadorCPC() {
			return indicadorCPC;
		}

		public void setIndicadorCPC(String indicadorCPC) {
			this.indicadorCPC = indicadorCPC;
		}

		
}