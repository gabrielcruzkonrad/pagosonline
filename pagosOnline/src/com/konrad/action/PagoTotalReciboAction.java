package com.konrad.action;


import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import com.konrad.assembler.MensajesAssembler;
import com.konrad.domain.Cliente;
import com.konrad.domain.Mensaje;
import com.konrad.domain.PeriodoFacturacion;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.TasaRepresentativaMercado;
import com.konrad.domain.TransaccionZonaPagos;
import com.konrad.domain.Usuario;
import com.konrad.domain.VencimientoPeriodo;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.helper.ReciboConsignacionHelper;
import com.konrad.util.IConstantes;
import com.konrad.util.Moneda;
import com.konrad.window.ActionStandard;
import com.zonapagos.www.prod.ZPagosLocator;
import com.zonapagos.www.prod.ZPagosSoapProxy;

public class PagoTotalReciboAction extends ActionStandard {
	
	private static final long serialVersionUID = -1840592650435657086L;

	protected static Logger log = Logger.getLogger(PagoTotalReciboAction.class);
	private ReciboConsignacion reciboConsignacion;
	private ZPagosLocator zonaPagosLocator;
	private ZPagosSoapProxy zonaPagosProxy;
	private Double valorTotal =0d;

	@SuppressWarnings("rawtypes")
	public void onSelectImageCurrency(){
		try{
			log.info("[onSelectImageCurrency]");
			Listbox listaMoneda = (Listbox)this.getFellow("idLbxPagoTotalRecibo");
			Listitem itemListaMoneda = (Listitem)listaMoneda.getSelectedItem();
			Listcell celdaListaMoneda = (Listcell)itemListaMoneda.getFirstChild();
			Datebox fechaAbono = (Datebox)this.getFellow("idDbxDatePagoTotalRecibo");
			Label doubleLabelAbono =(Label)this.getFellow("idLblPagoTotalSaldoRecibo");
			Date fechaHabil = new Date();
		
		if(!celdaListaMoneda.getValue().equals(IConstantes.PESOS_COLOMBIANOS)){
			fechaAbono.setDisabled(true);
			fechaAbono.setValue(fechaHabil);
			this.onCalculateTotal();
			
			if(this.getValorTotal().intValue()>0){
				BigDecimal cantidadConvertida = new BigDecimal(this.getValorTotal());
				BigDecimal tasa; 
				if(celdaListaMoneda.getValue().equals(IConstantes.DOLARES_AMERICANOS)){
					
					tasa = new BigDecimal(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor());
				}else{
					
					tasa = new BigDecimal(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_EURO)).getValor());
				}
				cantidadConvertida = cantidadConvertida.divide(tasa,0,RoundingMode.CEILING);
				doubleLabelAbono.setValue(new DecimalFormat("$"+IConstantes.FORMATO_NUMERO).format(cantidadConvertida.doubleValue()));
			}
				
			
		}else{
			fechaAbono.setDisabled(false);
			ReciboConsignacion recibo = new ReciboConsignacion();
			VencimientoPeriodo periodo = new VencimientoPeriodo();
			periodo.setFechaVencimiento(fechaHabil);
			recibo.setPeriodo(periodo);
			fechaHabil = (Date)ParametrizacionFac.getFacade().obtenerRegistro("getSiguienteDiaHabil", recibo);
			fechaAbono.setValue(fechaHabil);
			this.onCalculateTotal();
			if(this.getValorTotal().intValue()>0){
			doubleLabelAbono.setValue(new DecimalFormat("$"+IConstantes.FORMATO_NUMERO).format(this.getValorTotal()));
			}
		}
		
		ReciboConsignacionHelper.getHelper().onSetImageCurrency(listaMoneda, 
				(Image)this.getFellow("idImgPagoTotalReciboMoneda"));
		
		log.info("onSelectImageCurrency: "+(((Datebox)this.getFellow("idDbxDatePagoTotalRecibo")).getValue()==null?"vacio":new SimpleDateFormat("dd/MM/yyyy").format(((Datebox)this.getFellow("idDbxDatePagoTotalRecibo")).getValue())));
		
		} catch(Exception e){
			e.printStackTrace();
			Messagebox.show(
				    e.getMessage(),
				    "Error de Pagos en L�nea",
				    Messagebox.YES, Messagebox.ERROR);
			
		}
	}


	@SuppressWarnings("rawtypes")
	public void doModal(ReciboConsignacion reciboConsignacion){
		try{
		Mensaje mensaje = new Mensaje();
		String tipoMensaje = IConstantes.INFORMATION;
		mensaje.setMensaje(IConstantes.INFORMACION_FECHA_GENERACION);
		this.onSetMensaje(mensaje, tipoMensaje);
		
		mensaje = new Mensaje();
		mensaje.setMensaje(IConstantes.INFORMACION_HORARIO_BANCARIO);
		this.onSetMensaje(mensaje, tipoMensaje);
		
		ReciboConsignacionHelper.getHelper().onInicializarListaCurrency((Listbox)this.getFellow("idLbxPagoTotalRecibo"));
		ReciboConsignacionHelper.getHelper().onRetirarElementosListaCurrency((Listbox)this.getFellow("idLbxPagoTotalRecibo"),
				((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
						getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_EURO)).getValor(),
						((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
								getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor());
		
		ReciboConsignacion reciboAuxFecha = new ReciboConsignacion();
		VencimientoPeriodo periodoAux = new VencimientoPeriodo();
		periodoAux.setFechaVencimiento(new Date());
		reciboAuxFecha.setPeriodo(periodoAux);
		reciboAuxFecha.setCliente(reciboConsignacion.getCliente());
		reciboAuxFecha.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
		Date fechaGeneracion = (Date)ParametrizacionFac.getFacade().obtenerRegistro("getSiguienteDiaHabil", reciboAuxFecha); 
		log.info("fecha asignada: "+(fechaGeneracion==null?"Vac�o": new SimpleDateFormat("dd/MM/yyyy").format(fechaGeneracion)));
		((Datebox)this.getFellow("idDbxDatePagoTotalRecibo")).setValue(fechaGeneracion);
		log.info("fecha asignada doModal: "+(((Datebox)this.getFellow("idDbxDatePagoTotalRecibo")).getValue()==null?"Vac�o": new SimpleDateFormat("dd/MM/yyyy").format(((Datebox)this.getFellow("idDbxDatePagoTotalRecibo")).getValue())));
		
		ParametrizacionFac.getFacade().ejecutarProcedimiento("calcularVencimientosCliente", reciboAuxFecha);
		Double valorSaldoTotal = (Double)ParametrizacionFac.getFacade().obtenerRegistro("selectValorTotal", reciboAuxFecha);
		if(valorSaldoTotal== null){
			valorSaldoTotal = 0d;
		}
		
		if(valorSaldoTotal<=0){
			((Button)this.getFellow("idBtnPagoTotalReciboPagar")).setDisabled(true);
			((Button)this.getFellow("idBtnPagoTotalReciboImprimir")).setDisabled(true);
		}
		
		this.setReciboConsignacion(reciboConsignacion);
		Label labelUsuario = (Label)this.getFellow("idLblUsuarioPagoTotalRecibo");
		Label labelSaldo = (Label)this.getFellow("idLblPagoTotalSaldoRecibo");
		
		labelSaldo.setValue(new DecimalFormat("$"+IConstantes.FORMATO_NUMERO).format(valorSaldoTotal));
		labelUsuario.setValue(
			((Usuario)((HashMap)this.getDesktop().getSession().getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.USUARIO_SESSION)).getUsuario()
			+"-"+
			((Usuario)((HashMap)this.getDesktop().getSession().getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.USUARIO_SESSION)).getNombres()		
				);
		this.onSelectImageCurrency();
		
		Button botonPago =(Button)this.getFellow("idBtnPagoTotalReciboPagar");
		if(!ReciboConsignacionHelper.getHelper().validatePaymentDate()) {
			botonPago.setDisabled(true);
			Messagebox.show(
					   IConstantes.MENSAJE_BLOQUEO_HORA_PAGO.replace("{hora}", new SimpleDateFormat("HH:mm").format(new Date())),
					    "Advertencia de Pagos en L�nea",
					    Messagebox.YES, Messagebox.EXCLAMATION);
			
		}else {
			botonPago.setDisabled(false);
		}
		
		super.doModal();

		} catch(Exception e){
			e.printStackTrace();
			Messagebox.show(
				    e.getMessage(),
				    "Error de Pagos en L�nea",
				    Messagebox.YES, Messagebox.ERROR);
			e.printStackTrace();
			
		}
	}
	
	public void doOverlapped(){
		super.doOverlapped();
	}
	
	
	public void onCalculateTotal(){
		try{
		Date datePagoTotal =((Datebox)this.getFellow("idDbxDatePagoTotalRecibo")).getValue();
		ReciboConsignacion reciboAuxFecha = new ReciboConsignacion();
		VencimientoPeriodo periodoAux = new VencimientoPeriodo();
		periodoAux.setFechaVencimiento(datePagoTotal);
		reciboAuxFecha.setPeriodo(periodoAux);
		reciboAuxFecha.setCliente(reciboConsignacion.getCliente());
		reciboAuxFecha.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
		reciboAuxFecha.setBanderaDiaHabil(IConstantes.GENERAL_SI);
		reciboAuxFecha.setBanderaFecha(IConstantes.GENERAL_NO);
		
		Date fechaGeneracion = (Date)ParametrizacionFac.getFacade().obtenerRegistro("getSiguienteDiaHabil", reciboAuxFecha); 
		
		periodoAux.setFechaVencimiento(fechaGeneracion);
		reciboAuxFecha.setPeriodo(periodoAux);
		
		ParametrizacionFac.getFacade().ejecutarProcedimiento("calcularVencimientosCliente", reciboAuxFecha);
		Double valorSaldoTotal = (Double)ParametrizacionFac.getFacade().obtenerRegistro("selectValorTotal", reciboAuxFecha);
		if(valorSaldoTotal==null){
			valorSaldoTotal = 0d;
		}
		this.setReciboConsignacion(reciboConsignacion);
		((Datebox)this.getFellow("idDbxDatePagoTotalRecibo")).setValue(fechaGeneracion);
		log.info("fecha en calculateTotal: "+(((Datebox)this.getFellow("idDbxDatePagoTotalRecibo")).getValue()==null?" vacio ":new SimpleDateFormat("dd/MM/yyyy").format(((Datebox)this.getFellow("idDbxDatePagoTotalRecibo")).getValue())));
		this.setValorTotal(valorSaldoTotal);
		Label doubleLabelAbono =(Label)this.getFellow("idLblPagoTotalSaldoRecibo");
		Listbox listaMoneda = (Listbox)this.getFellow("idLbxPagoTotalRecibo");
		Listitem itemListaMoneda = (Listitem)listaMoneda.getSelectedItem();
		Listcell celdaListaMoneda = (Listcell)itemListaMoneda.getFirstChild();
		if(celdaListaMoneda.getValue().equals(IConstantes.PESOS_COLOMBIANOS)){
		doubleLabelAbono.setValue(new DecimalFormat("$"+IConstantes.FORMATO_NUMERO).format(this.getValorTotal()));
		}
		} catch(Exception e){
			e.printStackTrace();
			Messagebox.show(
				    IConstantes.ERROR_GENERACION_RECIBO,
				    "Error de Pagos en L�nea",
				    Messagebox.YES, Messagebox.ERROR);
		}
		
	}

	public void onSalir(){
		this.detach();
	}
	
	public void onSetMensaje(Mensaje mensaje, String tipoMensaje ){
		((Grid)this.getFellow("idGrdPagoTotalReciboMensajes")).setVisible(true);
		MensajesAssembler mensajeAs = new MensajesAssembler();
		Rows rowsMensaje = (Rows)this.getFellow("idRowsPagoTotalReciboMensajes");
		Row filaMensaje = mensajeAs.crearRowDesdeDto(mensaje, tipoMensaje);
		rowsMensaje.appendChild(filaMensaje);
}
	
	
	
	public void onImprimirRecibo(){
		
		ReciboConsignacion recibo = this.getReciboConsignacion();
		Integer  resultado = Messagebox.show(
			    "�Confirma la generaci�n de este recibo de pago? (Esto puede tomar varios segundos) "+IConstantes.MENSAJE_PERMITIR_POPUPS,
			    "Confirmar Generaci�n de Recibo de Pago",
			    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
		if(resultado == Messagebox.YES){
			// Se fijan variables para el nuevo recibo
			ReciboConsignacion nuevoRecibo = new ReciboConsignacion();
			nuevoRecibo.setCliente(recibo.getCliente());
			nuevoRecibo.setReciboConsignacion(recibo.getReciboConsignacion());
			VencimientoPeriodo vencimiento = new VencimientoPeriodo();
			PeriodoFacturacion periodo = new PeriodoFacturacion();
			periodo.setPeriodo(recibo.getPeriodo()!=null?recibo.getPeriodo().getPeriodo()!=null?recibo.getPeriodo().getPeriodo().getPeriodo()!=null?recibo.getPeriodo().getPeriodo().getPeriodo():"":"":"");
			vencimiento.setPeriodo(periodo);
			nuevoRecibo.setPeriodo(vencimiento);
			
			log.info("fecha en imprimir Recibo: "+(((Datebox)this.getFellow("idDbxDatePagoTotalRecibo")).getValue()==null?" vacio ":new SimpleDateFormat("dd/MM/yyyy").format(((Datebox)this.getFellow("idDbxDatePagoTotalRecibo")).getValue())));
			Date dateValue = ((Datebox)this.getFellow("idDbxDatePagoTotalRecibo")).getValue(); 
			nuevoRecibo.getPeriodo().setFechaVencimiento(dateValue);
			nuevoRecibo.setCuentaReciboConsignacion(recibo.getCuentaReciboConsignacion());
			nuevoRecibo.setFecha(recibo.getFecha());
			nuevoRecibo.setGeneraComision(recibo.getGeneraComision());
			nuevoRecibo.setNumeroFila(recibo.getNumeroFila());
			nuevoRecibo.setObservaciones("RECIBO AUTOMATICO POR SALDOS A FAVOR");
			nuevoRecibo.setTasaRepresentativa(recibo.getTasaRepresentativa());
			nuevoRecibo.setTasaRepresentativaVencido(recibo.getTasaRepresentativaVencido());
			this.onCalculateTotal();
			nuevoRecibo.setValorDetalle(this.getValorTotal());
			nuevoRecibo.setValorTotal(this.getValorTotal());
			nuevoRecibo.setValorVencido(this.getValorTotal());
			nuevoRecibo.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
			nuevoRecibo.setBanderaFecha(IConstantes.GENERAL_NO);
			nuevoRecibo.setBanderaDiaHabil(IConstantes.GENERAL_SI);
			
			// Calculamos vencimientos a la fecha y luego procedemos con la creaci�n del nuevo recibo
			try {
				Date fechaGeneracion = (Date)ParametrizacionFac.getFacade().obtenerRegistro("getSiguienteDiaHabil", nuevoRecibo);
				nuevoRecibo.getPeriodo().setFechaVencimiento(fechaGeneracion);
				
				ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarReciboAbono", nuevoRecibo);	

				
				// impresi�n del recibo de consignaci�n
				if( nuevoRecibo.getReciboConsignacion()!=null){
					if(nuevoRecibo.getReciboConsignacion().intValue()>0){
						PrintReportAction reportAction = (PrintReportAction)Executions.createComponents("pages/printReport.zul", null, null);
						reportAction.doModal(IConstantes.REP_CUOTA_CREDITO, nuevoRecibo);
						this.detach();
					}else{
						Messagebox.show(
							    IConstantes.ERROR_GENERACION_RECIBO,
							    "Error de Pagos en L�nea",
							    Messagebox.YES, Messagebox.ERROR);
					  Rows rowsMensaje = (Rows)this.getFellow("idRowsPagoTotalReciboMensajes"); 
					  rowsMensaje.getChildren().clear();
					  Mensaje mensaje = new Mensaje();
					  mensaje.setMensaje(IConstantes.ERROR_GENERACION_RECIBO);
					  String tipoMensaje = IConstantes.ERROR;
					  this.onSetMensaje(mensaje, tipoMensaje);
					}
				}else{
					Messagebox.show(
						    IConstantes.ERROR_GENERACION_RECIBO,
						    "Error de Pagos en L�nea",
						    Messagebox.YES, Messagebox.ERROR);
				  Rows rowsMensaje = (Rows)this.getFellow("idRowsPagoTotalReciboMensajes"); 
				  rowsMensaje.getChildren().clear();
				  Mensaje mensaje = new Mensaje();
				  mensaje.setMensaje(IConstantes.ERROR_GENERACION_RECIBO);
				  String tipoMensaje = IConstantes.ERROR;
				  this.onSetMensaje(mensaje, tipoMensaje);
				}
			} catch (Exception e) {
				Messagebox.show(
					    e.getMessage(),
					    "Error de Pagos en L�nea",
					    Messagebox.YES, Messagebox.ERROR);
				e.printStackTrace();
			}
			
			
		}
		
	}

	
	@SuppressWarnings("rawtypes")
	public void onPagarEnLineaPayUW(){
		Integer  resultado = Messagebox.show(
			    "�Confirma este pago? "+IConstantes.MENSAJE_PERMITIR_POPUPS,
			    "Confirmar Inicio Pago",
			    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
		if(resultado == Messagebox.YES){	
		
		ReciboConsignacion nuevoRecibo = new ReciboConsignacion();
		Cliente cliente = new Cliente();
		cliente.setCliente(new Long(((Usuario)((HashMap)this.getDesktop().getSession().getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.USUARIO_SESSION)).getUsuario()));
		nuevoRecibo.setCliente(cliente);
		nuevoRecibo.setFecha(new Date());
		
		nuevoRecibo.setObservaciones("RECIBO AUTOMATICO POR SALDOS A FAVOR");
		this.onCalculateTotal();
		Double valorTotalPagar =this.getValorTotal(); 
		nuevoRecibo.setValorDetalle(valorTotalPagar);
		nuevoRecibo.setValorTotal(valorTotalPagar);
		nuevoRecibo.setValorVencido(valorTotalPagar);
		nuevoRecibo.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
		nuevoRecibo.setBanderaFecha(IConstantes.GENERAL_NO);
	
		
		//  Procedemos con la creaci�n del nuevo recibo
		try {
			nuevoRecibo = this.setAtributosReciboConsignacion(nuevoRecibo);
			ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarReciboAbono", nuevoRecibo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		log.info("Recibo: "+nuevoRecibo.getReciboConsignacion());
		log.info("valor: "+nuevoRecibo.getValorTotal());
		log.info("valor detalle: "+nuevoRecibo.getValorDetalle());
		log.info("valor Vencido: "+nuevoRecibo.getValorVencido());
		log.info("Observaciones: "+nuevoRecibo.getObservaciones());
		log.info("Valor Original Moneda Convertida: "+nuevoRecibo.getTasaRepresentativa().getValor());
		log.info("Valor Vencido Moneda Convertida: "+nuevoRecibo.getTasaRepresentativaVencido().getValor());
		
		
		
		if(nuevoRecibo.getReciboConsignacion()!= null){
			if(nuevoRecibo.getReciboConsignacion().intValue()>0){
						try {
							ReciboConsignacionHelper.getHelper().sendPostPayU(nuevoRecibo, IConstantes.TIPO_INVOCACION_PAGO_TOTAL);
							ReciboConsignacionHelper.getHelper().registrarAuditoriaPayU(nuevoRecibo, IConstantes.TIPO_INVOCACION_PAGO_TOTAL);
						}catch(Exception e){
							}
						}
					}
				}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void onPagarEnLinea() {
		Integer  resultado = Messagebox.show(
			    "�Confirma este pago? "+IConstantes.MENSAJE_PERMITIR_POPUPS,
			    "Confirmar Inicio Pago",
			    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
		if(resultado == Messagebox.YES){

		String[] listServiciosMulti = null;
		String[] listNitMulti = null;
		double[] listValIvaMulti = null;
		double[] listIvaMulti = null;
		String wsResult;

		
		
		ReciboConsignacion nuevoRecibo = new ReciboConsignacion();
		Cliente cliente = new Cliente();
		cliente.setCliente(new Long(((Usuario)((HashMap)this.getDesktop().getSession().getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.USUARIO_SESSION)).getUsuario()));
		nuevoRecibo.setCliente(cliente);
		nuevoRecibo.setFecha(new Date());
		
		nuevoRecibo.setObservaciones("RECIBO AUTOMATICO POR SALDOS A FAVOR");
		this.onCalculateTotal();
		Double valorTotalPagar =this.getValorTotal(); 
		nuevoRecibo.setValorDetalle(valorTotalPagar);
		nuevoRecibo.setValorTotal(valorTotalPagar);
		nuevoRecibo.setValorVencido(valorTotalPagar);
		nuevoRecibo.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
		nuevoRecibo.setBanderaFecha(IConstantes.GENERAL_NO);
		// preparar map para buscar pagos pendientes
		Map<String, String> mapaParametros = new HashMap<String, String>();
		mapaParametros.put("ESTADO_PENDIENTE_INICIAR",IConstantes.ESTADO_PENDIENTE_INICIAR);
		mapaParametros.put("ESTADO_PENDIENTE_FINALIZAR",IConstantes.ESTADO_PENDIENTE_FINALIZAR);
		mapaParametros.put("ID_CLIENTE",nuevoRecibo.getCliente().getCliente().toString());
		List<TransaccionZonaPagos> listaDatos = new ArrayList<TransaccionZonaPagos>();
		listaDatos = null;
		
		// b�squeda de pagos pendientes
		try {
			listaDatos = (List<TransaccionZonaPagos>)ParametrizacionFac.getFacade().obtenerListado("selectTransaccionZonaPagosPendientes", mapaParametros);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// debe asegurarse de que no se desperdicien recibos si la persona tiene pagos pendientes no generar sino mostrar error
		if(listaDatos.size()<=0){
		//  Procedemos con la creaci�n del nuevo recibo
		try {
			nuevoRecibo = this.setAtributosReciboConsignacion(nuevoRecibo);
			ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarReciboAbono", nuevoRecibo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		log.info("Recibo: "+nuevoRecibo.getReciboConsignacion());
		log.info("valor: "+nuevoRecibo.getValorTotal());
		log.info("valor detalle: "+nuevoRecibo.getValorDetalle());
		log.info("valor Vencido: "+nuevoRecibo.getValorVencido());
		log.info("Observaciones: "+nuevoRecibo.getObservaciones());
		log.info("Valor Original Moneda Convertida: "+nuevoRecibo.getTasaRepresentativa().getValor());
		log.info("Valor Vencido Moneda Convertida: "+nuevoRecibo.getTasaRepresentativaVencido().getValor());
		
		}
		
		if(nuevoRecibo.getReciboConsignacion()!= null){
			if(nuevoRecibo.getReciboConsignacion().intValue()>0){

				
				this.setZonaPagosLocator(new ZPagosLocator());
				this.setZonaPagosProxy(new ZPagosSoapProxy());
				com.zonapagos.www.test.ZPagosSoapProxy zonaPagosProxyPruebas = new com.zonapagos.www.test.ZPagosSoapProxy();
		
				
				
				if(listaDatos.size()<=0){
					if (nuevoRecibo!= null){
						try {
							if(ReciboConsignacionHelper.getHelper().getServicioPruebasProduccion()){
								log.info("Invocamos el servicio de producci�n");
								wsResult = this.getZonaPagosProxy().inicio_pagoV2(
										nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
												(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
												&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
												Integer.parseInt(IConstantes.ID_TIENDA_BANCOLOMBIA):
												Integer.parseInt(IConstantes.ID_TIENDA):
												nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
												Integer.parseInt(IConstantes.ID_TIENDA_DOLARES):
												Integer.parseInt(IConstantes.ID_TIENDA_EURO), 
										nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
												(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
												&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
												IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
												IConstantes.CLAVE_SERVICIO:
												nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
												IConstantes.CLAVE_SERVICIO_DOLARES:
												IConstantes.CLAVE_SERVICIO_EURO,  
										nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
												nuevoRecibo.getValorVencido().doubleValue():
												nuevoRecibo.getTasaRepresentativaVencido().getValor().doubleValue(), 
										new Double(0).doubleValue(), 
										nuevoRecibo.getReciboConsignacion().toString(), 
										nuevoRecibo.getObservaciones().substring(0, nuevoRecibo.getObservaciones().length()>=70?69:nuevoRecibo.getObservaciones().length()), 
										nuevoRecibo.getCliente().getPersona().getDireccionElectronica(), 
										nuevoRecibo.getCliente().getCliente().toString(), 
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("C.C")?
												new String("1"):
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NIT")?
												new String("3"):
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("T.I")?
												new String("5"):
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("CEX")?	
												new String("2"):
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NEX")?			
												new String("10"):
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("RC")?		
												new String("9"):
										nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("PS")?			
												new String("6"):
												new String("11"), 
												nuevoRecibo.getCliente().getPersona().getNombreRazonSocial().substring(
														0,	nuevoRecibo.getCliente().getPersona().getNombreRazonSocial().length()>=50?49:
															nuevoRecibo.getCliente().getPersona().getNombreRazonSocial().length()), 
													(nuevoRecibo.getCliente().getPersona().getPrimerApellido()+" "+
															nuevoRecibo.getCliente().getPersona().getSegundoApellido()).substring(
																	0, (nuevoRecibo.getCliente().getPersona().getPrimerApellido()+" "+
																			nuevoRecibo.getCliente().getPersona().getSegundoApellido())
																			.length()>=50?49:
																				(nuevoRecibo.getCliente().getPersona().getPrimerApellido()+" "+
																						nuevoRecibo.getCliente().getPersona().getSegundoApellido())
																						.length()
																			), 
																			nuevoRecibo.getCliente().getPersona().getTelefonoResidencia().substring(0, 
																					nuevoRecibo.getCliente().getPersona().getTelefonoResidencia().length()>=50?49:	
																						nuevoRecibo.getCliente().getPersona().getTelefonoResidencia().length()), 
										new String(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
												getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor().toString()), 
										new String(nuevoRecibo.getTasaRepresentativaVencido().getValor().toString()+" "+nuevoRecibo.getTasaRepresentativa().getMoneda().getCodigo()), 
										new String(nuevoRecibo.getPeriodo()!=null?
												nuevoRecibo.getPeriodo().getPeriodo()!=null?
												nuevoRecibo.getPeriodo().getPeriodo().getPeriodo():"":""), 
										nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
												(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
												&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
										IConstantes.CODIGO_SERVICIO_BANCOLOMBIA:
											IConstantes.CODIGO_SERVICIO:
											nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
											IConstantes.CODIGO_SERVICIO_DOLARES:
											IConstantes.CODIGO_SERVICIO_EURO,  
										listServiciosMulti,
										listNitMulti,
										listValIvaMulti,
										listIvaMulti, 
										Integer.parseInt(new String("0"))
										);
				}else{
					log.info("Invocamos el servicio de pruebas");
					wsResult = zonaPagosProxyPruebas.inicio_pagoV2(
							nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
									(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
	            					&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
									Integer.parseInt(IConstantes.ID_TIENDA_BANCOLOMBIA):
									Integer.parseInt(IConstantes.ID_TIENDA):
										nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
										Integer.parseInt(IConstantes.ID_TIENDA_DOLARES):
										Integer.parseInt(IConstantes.ID_TIENDA_EURO)	
											, 
											nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
											(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
					            			&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
											IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
											IConstantes.CLAVE_SERVICIO:
											nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
											IConstantes.CLAVE_SERVICIO_DOLARES:
											IConstantes.CLAVE_SERVICIO_EURO, 
											nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
													nuevoRecibo.getValorVencido().doubleValue():
													nuevoRecibo.getTasaRepresentativaVencido().getValor().doubleValue(), 
							new Double(0).doubleValue(), 
							nuevoRecibo.getReciboConsignacion().toString(), 
							nuevoRecibo.getObservaciones().substring(0, nuevoRecibo.getObservaciones().length()>=70?69:nuevoRecibo.getObservaciones().length()), 
							nuevoRecibo.getCliente().getPersona().getDireccionElectronica(), 
							nuevoRecibo.getCliente().getCliente().toString(), 
							nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("C.C")?
								new String("1"):
								nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NIT")?
								new String("3"):
								nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("T.I")?
								new String("5"):
								nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("CEX")?	
								new String("2"):
								nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NEX")?			
								new String("10"):
								nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("RC")?		
								new String("9"):
								nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("PS")?			
								new String("6"):
								new String("11"), 
								nuevoRecibo.getCliente().getPersona().getNombreRazonSocial().substring(
										0,	nuevoRecibo.getCliente().getPersona().getNombreRazonSocial().length()>=50?49:
											nuevoRecibo.getCliente().getPersona().getNombreRazonSocial().length()), 
									(nuevoRecibo.getCliente().getPersona().getPrimerApellido()+" "+
											nuevoRecibo.getCliente().getPersona().getSegundoApellido()).substring(
													0, (nuevoRecibo.getCliente().getPersona().getPrimerApellido()+" "+
															nuevoRecibo.getCliente().getPersona().getSegundoApellido())
															.length()>=50?49:
																(nuevoRecibo.getCliente().getPersona().getPrimerApellido()+" "+
																		nuevoRecibo.getCliente().getPersona().getSegundoApellido())
																		.length()
															), 
															nuevoRecibo.getCliente().getPersona().getTelefonoResidencia().substring(0, 
																	nuevoRecibo.getCliente().getPersona().getTelefonoResidencia().length()>=50?49:	
																		nuevoRecibo.getCliente().getPersona().getTelefonoResidencia().length()),
							new String(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor().toString()), 
							new String(nuevoRecibo.getTasaRepresentativaVencido().getValor().toString()+" "+nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo()), 
							new String(nuevoRecibo.getPeriodo()!=null?
    								nuevoRecibo.getPeriodo().getPeriodo()!=null?
    										nuevoRecibo.getPeriodo().getPeriodo().getPeriodo():"":""), 
							nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
									(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
	            					&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
									IConstantes.CODIGO_SERVICIO_BANCOLOMBIA:
									IConstantes.CODIGO_SERVICIO:
										nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
												IConstantes.CODIGO_SERVICIO_DOLARES:
												IConstantes.CODIGO_SERVICIO_EURO, 
							listServiciosMulti,
							listNitMulti,
							listValIvaMulti,
							listIvaMulti, 
							Integer.parseInt(new String("0")));
				}
			log.info(wsResult);
			
			if (!wsResult.isEmpty()){
				if(!wsResult.startsWith("-1")){
					if(Long.parseLong(wsResult)>0){
						
						TransaccionZonaPagos transaccion = new TransaccionZonaPagos();
	            		transaccion.setSecTransaccionZonaPagos(null);
	            		transaccion.setIdPago(nuevoRecibo.getReciboConsignacion());
	            		transaccion.setEstadoPago(new Long(IConstantes.ESTADO_PENDIENTE_INICIAR));
	            		transaccion.setIdFormaPago(null);
	            		transaccion.setValorPagado(nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
													nuevoRecibo.getValorVencido().doubleValue():
														nuevoRecibo.getTasaRepresentativaVencido().getValor().doubleValue());
	            		transaccion.setTicketId(null);
	            		transaccion.setIdClave(nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
								(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
						            	&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
										IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
										IConstantes.CLAVE_SERVICIO:
											nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
													IConstantes.CLAVE_SERVICIO_DOLARES:
													IConstantes.CLAVE_SERVICIO_EURO
													);
	            		transaccion.setIdCliente(nuevoRecibo.getCliente().getCliente().toString());
	            		transaccion.setFranquicia(null);
	            		transaccion.setCodigoServicio(null);
	            		transaccion.setCodigoBanco(null);
	            		transaccion.setNombreBanco(null);
	            		transaccion.setCodigoTransaccion(null);
	            		transaccion.setCicloTransaccion(null);
	            		transaccion.setCampo1(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
								getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor().toString());
	            		transaccion.setCampo2(nuevoRecibo.getTasaRepresentativaVencido().getValor().toString()+" "+nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo());
	            		transaccion.setCampo3(nuevoRecibo.getPeriodo()!=null?
	            								nuevoRecibo.getPeriodo().getPeriodo()!=null?
	            										nuevoRecibo.getPeriodo().getPeriodo().getPeriodo():"":"");
	            		transaccion.setIdComercio(new Long(nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
								(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
		            					&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
										Integer.parseInt(IConstantes.ID_TIENDA_BANCOLOMBIA):
										Integer.parseInt(IConstantes.ID_TIENDA):
											nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
													Integer.parseInt(IConstantes.ID_TIENDA_DOLARES):
														Integer.parseInt(IConstantes.ID_TIENDA_EURO)));
	            		transaccion.setDatFecha(new Date());
	            		transaccion.setIdPredecesor(null);
	            		
	            		ParametrizacionFac.getFacade().guardarRegistro("insertTransaccionZonaPagos", transaccion);
	            		log.info("registro guardado: "+transaccion.getSecTransaccionZonaPagos());
	            		log.info("corriendo el redirect");
	            		//RedirectAction winRedirectPagos = (RedirectAction)Executions.createComponents("pages/redirectPago.zul", null,null);
	            		
	            		if(nuevoRecibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)){
	            			
	            			//Execution execution = Executions.getCurrent();
	            			if(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
	            					&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA)){
	            				Executions.sendRedirect(IConstantes.RUTA_SERVICIO_BANCOLOMBIA+wsResult);
	            				//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO_BANCOLOMBIA+wsResult);	
	            			} else if (nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_OCCIDENTE) 
	            					&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_OCCIDENTE)){
	            				Executions.sendRedirect(IConstantes.RUTA_SERVICIO+wsResult);	
	            				//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO+wsResult);
	            			} else{
	            				//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO+wsResult);
	            				Executions.sendRedirect(IConstantes.RUTA_SERVICIO+wsResult);
	            			}
	            			//execution.setVoided(true);
	            			
	            			
	            		}else if (nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)){
	            			//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO_DOLARES+wsResult);
	            			//Execution execution = Executions.getCurrent();
	            			Executions.sendRedirect(IConstantes.RUTA_SERVICIO_DOLARES+wsResult);
	            			//execution.setVoided(true);
	            		} else {
	            			//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO_EURO+wsResult);
	            			//Execution execution = Executions.getCurrent();
	            			Executions.sendRedirect(IConstantes.RUTA_SERVICIO_EURO+wsResult);
	            			//execution.setVoided(true);
	            		}
	            		
	            		  Rows rowsMensaje = (Rows)this.getFellow("idRowsPagoTotalReciboMensajes"); 
		       			  rowsMensaje.getChildren().clear();
		       			  Mensaje mensaje = new Mensaje();
		       			  
		       			  mensaje.setMensaje(IConstantes.CONFIRMACION_INICIO_PAGO);
		       			  String tipoMensaje = IConstantes.CONFIRM;
		       			  this.onSetMensaje(mensaje, tipoMensaje);
							
						}else{ //else wsresult >0
							Rows rowsMensaje = (Rows)this.getFellow("idRowsPagoTotalReciboMensajes"); 
			       			  rowsMensaje.getChildren().clear();
			       			  Mensaje mensaje = new Mensaje();
			       			  
			       			  mensaje.setMensaje(IConstantes.ERROR_PAGO_INICIO+wsResult);
			       			  String tipoMensaje = IConstantes.ERROR;
			       			  this.onSetMensaje(mensaje, tipoMensaje);
							
						}
					}else{ // wsresult empieza -1
						Rows rowsMensaje = (Rows)this.getFellow("idRowsPagoTotalReciboMensajes"); 
		       			  rowsMensaje.getChildren().clear();
		       			  Mensaje mensaje = new Mensaje();
		       			  
		       			  mensaje.setMensaje(IConstantes.ERROR_PAGO_INICIO+wsResult);
		       			  String tipoMensaje = IConstantes.ERROR;
		       			  this.onSetMensaje(mensaje, tipoMensaje);
						
					}
					
					
				} else{// ws result nulo
					  Rows rowsMensaje = (Rows)this.getFellow("idRowsPagoTotalReciboMensajes"); 
	       			  rowsMensaje.getChildren().clear();
	       			  Mensaje mensaje = new Mensaje();
	       			  
	       			  mensaje.setMensaje(IConstantes.ERROR_PAGO_INICIO+wsResult);
	       			  String tipoMensaje = IConstantes.ERROR;
	       			  this.onSetMensaje(mensaje, tipoMensaje);
					
				}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			
			e.printStackTrace();
				}
			}
		
		
		} else if(listaDatos.size()>0){
			  Messagebox.show(
					    IConstantes.ERROR_PAGO_PENDIENTE,
					    "Error de Pagos en L�nea",
					    Messagebox.YES, Messagebox.ERROR);
			  Rows rowsMensaje = (Rows)this.getFellow("idRowsPagoTotalReciboMensajes"); 
			  rowsMensaje.getChildren().clear();
			  Mensaje mensaje = new Mensaje();
			  mensaje.setMensaje(IConstantes.ERROR_PAGO_PENDIENTE);
			  String tipoMensaje = IConstantes.ERROR;
			  this.onSetMensaje(mensaje, tipoMensaje);
			}
		} else{
			 Messagebox.show(
					    IConstantes.ERROR_GENERACION_RECIBO,
					    "Error de Pagos en L�nea",
					    Messagebox.YES, Messagebox.ERROR);
			  Rows rowsMensaje = (Rows)this.getFellow("idRowsPagoTotalReciboMensajes"); 
			  rowsMensaje.getChildren().clear();
			  Mensaje mensaje = new Mensaje();
			  mensaje.setMensaje(IConstantes.ERROR_GENERACION_RECIBO);
			  String tipoMensaje = IConstantes.ERROR;
			  this.onSetMensaje(mensaje, tipoMensaje);
			  
		}
	} else{
		 Messagebox.show(
				    IConstantes.ERROR_GENERACION_RECIBO,
				    "Error de Pagos en L�nea",
				    Messagebox.YES, Messagebox.ERROR);
		  Rows rowsMensaje = (Rows)this.getFellow("idRowsPagoTotalReciboMensajes"); 
		  rowsMensaje.getChildren().clear();
		  Mensaje mensaje = new Mensaje();
		  mensaje.setMensaje(IConstantes.ERROR_GENERACION_RECIBO);
		  String tipoMensaje = IConstantes.ERROR;
		  this.onSetMensaje(mensaje, tipoMensaje);
		  
		}
	}	
}
	
	@SuppressWarnings("rawtypes")
	public ReciboConsignacion setAtributosReciboConsignacion(ReciboConsignacion recibo){
		ReciboConsignacion reciboConsignacion = recibo;
		try{
		
			Double saldoActual = reciboConsignacion.getValorVencido();
			TasaRepresentativaMercado tasaIncluir = new TasaRepresentativaMercado();
			TasaRepresentativaMercado tasaIncluirVencido = new TasaRepresentativaMercado();
			Moneda monedaIncluir= new Moneda();
			
			Listbox listaMonedaIncluir= (Listbox)this.getFellow("idLbxPagoTotalRecibo");
			Listitem itemSeleccionadoIncluir = listaMonedaIncluir.getSelectedItem();
			BigDecimal cantidadRedondear;
			BigDecimal cantidadRedondearVencido;
			
			if(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)){
				monedaIncluir.setCodigo(IConstantes.PESOS_COLOMBIANOS);
				
				tasaIncluir.setMoneda(monedaIncluir);
				tasaIncluirVencido.setMoneda(monedaIncluir);
				cantidadRedondearVencido = new BigDecimal(saldoActual);
				cantidadRedondear =new BigDecimal(reciboConsignacion.getValorDetalle().doubleValue());
				tasaIncluirVencido.setValor(cantidadRedondearVencido.setScale(0,RoundingMode.CEILING).doubleValue());
				tasaIncluir.setValor(cantidadRedondear.setScale(0,RoundingMode.CEILING).doubleValue());
				
				tasaIncluir.setFecha(new Date());
				tasaIncluirVencido.setFecha(new Date());
				reciboConsignacion.setTasaRepresentativa(tasaIncluir);
				reciboConsignacion.setTasaRepresentativaVencido(tasaIncluirVencido);
				reciboConsignacion.setValorVencido(tasaIncluirVencido.getValor());
			
			}else{ //Entonces se hace el c�lculo con los USD
				if(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)){
					
					monedaIncluir.setCodigo(IConstantes.DOLARES_AMERICANOS);
					tasaIncluir.setMoneda(monedaIncluir);
					tasaIncluirVencido.setMoneda(monedaIncluir);
					//Se encuentra la comisi�n para restar a la TRM
					BigDecimal comisionPesos = new BigDecimal((String)((HashMap)this.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.COMISION_DOLAR)); 
					//Si es en d�lares se realiza el c�lculo asi (valor total del recibo / (trm-comision))
					//Para la parte de recibos vencidos se hace la conversi�n con el recibo calculado a la fecha
					cantidadRedondearVencido = new BigDecimal(saldoActual 
							/ (((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor()-comisionPesos.doubleValue()));
					
					//Valor original del recibo Vencido
					cantidadRedondear = new BigDecimal(reciboConsignacion.getValorDetalle().doubleValue() 
							/ (((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor()-comisionPesos.doubleValue()));
					
					//Se incluye la tasa redondeada
					tasaIncluir.setValor(cantidadRedondear.setScale(0,RoundingMode.CEILING).doubleValue());
					tasaIncluirVencido.setValor(cantidadRedondearVencido.setScale(0,RoundingMode.CEILING).doubleValue());
					tasaIncluir.setFecha(new Date());
					tasaIncluirVencido.setFecha(new Date());
					reciboConsignacion.setTasaRepresentativa(tasaIncluir);
					reciboConsignacion.setTasaRepresentativaVencido(tasaIncluirVencido);
					
					// Se requiere enviar el valor en pesos por lo tanto se hace valor en USD * trm
					
					BigDecimal cantidadConvertida = new BigDecimal(cantidadRedondear.doubleValue());
					cantidadConvertida = cantidadConvertida.multiply(new BigDecimal(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor()));
					
					BigDecimal cantidadConvertidaVencido = new BigDecimal(cantidadRedondearVencido.doubleValue());
					cantidadConvertidaVencido = cantidadConvertidaVencido.multiply(new BigDecimal(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor()));
					
					reciboConsignacion.setValorDetalle(cantidadConvertida.doubleValue());
					reciboConsignacion.setValorVencido(cantidadConvertidaVencido.doubleValue());
					log.info("valor recibo USD - COP: "+reciboConsignacion.getValorDetalle());
					log.info("valor recibo Vencido USD - COP: "+reciboConsignacion.getValorVencido());
				
				
				} else{//Entonces se hace el c�lculo con los Euros
					
					monedaIncluir.setCodigo(IConstantes.EUROS);
					tasaIncluir.setMoneda(monedaIncluir);
					tasaIncluirVencido.setMoneda(monedaIncluir);
					
					// se encuentra la comision para restar a la TRM 
					BigDecimal comisionPesos = new BigDecimal((String)((HashMap)this.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.COMISION_EURO));
					
					// Se debe calcular una TRM entre USD y EUR 
					BigDecimal retorno = new BigDecimal(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_EURO)).getValor()/
							((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
									getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor());
					//luego se realiza el c�lculo del monto (valorRecibo / (retorno * (trm -comision)))
					cantidadRedondear = 
							new BigDecimal(
									(
										reciboConsignacion.getValorDetalle().doubleValue() / 
											(retorno.doubleValue()* (
														((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
									getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor() - comisionPesos.doubleValue()
									)
								)  
							)
						);
					
					
					cantidadRedondearVencido = 
							new BigDecimal(
									(
										saldoActual / 
											(retorno.doubleValue()* (
														((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
									getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor() - comisionPesos.doubleValue()
									)
								)  
							)
						);
					
					// Se incluye la tasa redondeada
					tasaIncluir.setValor(cantidadRedondear.setScale(0,RoundingMode.CEILING).doubleValue());
					tasaIncluirVencido.setValor(cantidadRedondearVencido.setScale(0,RoundingMode.CEILING).doubleValue());
					tasaIncluir.setFecha(new Date());
					tasaIncluirVencido.setFecha(new Date());
					reciboConsignacion.setTasaRepresentativa(tasaIncluir);
					reciboConsignacion.setTasaRepresentativaVencido(tasaIncluirVencido);
					
					// Se requiere enviar el valor en pesos por lo tanto se hace valor en EUR * trm
								BigDecimal cantidadConvertida = new BigDecimal(cantidadRedondear.doubleValue());
								cantidadConvertida = cantidadConvertida.multiply(new BigDecimal(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
										getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_EURO)).getValor()));
								
								BigDecimal cantidadConvertidaVencido = new BigDecimal(cantidadRedondearVencido.doubleValue());
								cantidadConvertidaVencido = cantidadConvertidaVencido.multiply(new BigDecimal(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
										getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_EURO)).getValor()));
								
								reciboConsignacion.setValorDetalle(cantidadConvertida.doubleValue());
								reciboConsignacion.setValorVencido(cantidadConvertidaVencido.doubleValue());
								log.info("valor recibo EUR - COP: "+reciboConsignacion.getValorDetalle());
								log.info("valor recibo Vencido EUR - COP: "+reciboConsignacion.getValorVencido());
			}
		}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return reciboConsignacion;
	}

	public ReciboConsignacion getReciboConsignacion() {
		return reciboConsignacion;
	}

	public void setReciboConsignacion(ReciboConsignacion reciboConsignacion) {
		this.reciboConsignacion = reciboConsignacion;
	}	
	
	
	
public ZPagosLocator getZonaPagosLocator() {
	return zonaPagosLocator;
}

public void setZonaPagosLocator(ZPagosLocator zonaPagosLocator) {
	this.zonaPagosLocator = zonaPagosLocator;
}

public ZPagosSoapProxy getZonaPagosProxy() {
	return zonaPagosProxy;
}

public void setZonaPagosProxy(ZPagosSoapProxy zonaPagosProxy) {
	this.zonaPagosProxy = zonaPagosProxy;
}

public Double getValorTotal() {
	return valorTotal;
}

public void setValorTotal(Double valorTotal) {
	this.valorTotal = valorTotal;
}	
	
}