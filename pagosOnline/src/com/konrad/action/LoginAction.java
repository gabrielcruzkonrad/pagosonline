package com.konrad.action;



import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Captcha;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import com.konrad.domain.Cliente;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.TasaRepresentativaMercado;
import com.konrad.domain.Usuario;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.helper.ReciboConsignacionHelper;
import com.konrad.util.IConstantes;
import com.konrad.util.Moneda;


public class LoginAction  extends Borderlayout implements AfterCompose{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Usuario current =new Usuario();
	Listbox box;
	protected static Logger log = Logger.getLogger(LoginAction.class);

	
	@Override
	public void afterCompose() {
		// TODO Auto-generated method stub
	    log.info("loginAction.AfterCompose()");
		this.getDesktop().getSession().setAttribute(IConstantes.AUTENTICADO,"N");

		
		try {
		Locale prefer_Locale = org.zkoss.util.Locales.getLocale("es");
		log.info(prefer_Locale.toLanguageTag());
		org.zkoss.zk.ui.Session session = Sessions.getCurrent();
		
	    session.setAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE, prefer_Locale);
	    org.zkoss.util.Locales.setThreadLocal(org.zkoss.util.Locales.getLocale("es"));
	    Clients.reloadMessages(prefer_Locale);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Usuario getCurrent() {
		return current;
	}
	public void setCurrent(Usuario current) {
		this.current = current;
	}
		
	public void cargarZul(String menu){
		Component miPage = this.getParent();
		this.detach();
		Executions.createComponents(menu, miPage, null);
	}
	
	public void onRedirectPublicaciones(){
		Executions.sendRedirect("https://www.konradlorenz.edu.co");
		
	}
	
	public void onRedirectOrdenes(){
		Executions.sendRedirect("https://serviciosweb.konradlorenz.edu.co/omweb");
		
	}
	
	public void onRedirectHome(){
		Executions.sendRedirect("https://www.konradlorenz.edu.co");
		
	}
	
	public void onRedirectSolicitudCredito(){
		Executions.sendRedirect("https://serviciosweb.konradlorenz.edu.co/iceberg/loginterceros.zul");
		
	}
	
	/**
	 * M�todo de recuperaci�n de usuarios 
	 **/
	@SuppressWarnings("unchecked")
	public List<Usuario> getUserList(String identificacion) {
		List<Usuario> listaUsuarios = new ArrayList<Usuario>();
		
		try {
			listaUsuarios = (List<Usuario>)ParametrizacionFac.getFacade().obtenerListado("selectUsuariosIdentificacion", identificacion);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return listaUsuarios;
	}

	public void validarUsuarioList() {
		try {
		String usuario=((Textbox)getFellow("logUsuario")).getValue();
		String clave=((Textbox)getFellow("logContrasena")).getValue();
		String rutamenu  =new String();
		 
		 if(((Captcha)this.getFellow("cap1")).getValue().equalsIgnoreCase((clave))){
			 List<Usuario> listaUsuario = this.getUserList(usuario);
			 if(listaUsuario!= null && listaUsuario.size()>0) {
				if(listaUsuario.size()<=1) {
					// listado de usuarios con solo una ocurrencia
					this.validarUsuario();
				}else if (listaUsuario.size()>1) {
					// listado de usuarios con n ocurrencias
					CustomerListAction customerListW = (CustomerListAction)Executions.createComponents("pages/showCustomerList.zul", null, null);
					customerListW.doModal(listaUsuario);
					rutamenu=(new String("pages/menuPagos.zul"));
      				this.cargarZul(rutamenu);
				}
			 }else {
				 Messagebox.show(
						 "No tiene usuario registrado en la Base de datos � su usuario est� bloqueado en biblioteca",
						 "Error", 
						 Messagebox.OK, 
						 Messagebox.ERROR);
			 }
		 }
			 
		}catch(Exception e) {
			e.printStackTrace();
		}  
		
	}
	
	public void validarUsuario() throws SQLException {
	
		String usuario=((Textbox)getFellow("logUsuario")).getValue();
		String clave=((Textbox)getFellow("logContrasena")).getValue();
		Usuario infusuario = new Usuario();
		String rutamenu  =new String();
		
		log.info("Inicia validacion Usuario");
		
		
		Usuario vusuario=new Usuario();
		try {
			  vusuario.setUsuario(usuario);
			  vusuario.setNumeroidentificacion(clave);
			  infusuario=(Usuario)ParametrizacionFac.getFacade().obtenerRegistro("validarUsuario",vusuario);
			  
	        } catch (Exception e) {
			e.printStackTrace();
		 	throw new RuntimeException ("No ha sido posible culminar el proceso de autenticaci�n debido a un tiempo de espera prolongado, por favor intente de nuevo.");
	        }
		
	 	  
		 if (infusuario!= null)
		 {
			 if(infusuario.getUsuario()!=null){
			 if(((Captcha)this.getFellow("cap1")).getValue().equalsIgnoreCase(((Textbox)this.getFellow("logContrasena")).getValue())){
				 
				Session zkSession = Sessions.getCurrent(); 
				HttpSession nativeHttpSession = (HttpSession) zkSession.getNativeSession();
				nativeHttpSession.setAttribute(IConstantes.USUARIO_SESSION, infusuario);
				Map<String, Object> infoSesion = new HashMap<String, Object>();
				infoSesion.put(IConstantes.USUARIO_SESSION, infusuario);
             
           
             	ReciboConsignacion reciboParametro= new ReciboConsignacion();
             	Cliente cliente = new Cliente();
             	cliente.setCliente(new Long(infusuario.getUsuario()));
             	reciboParametro.setCliente(cliente);
            		
             	// Significa que se hace en moneda extranjera se verifica que haya subido la del d�a DOLARES - PESOS
             	Map<String, Object> mapaResultDolar = new HashMap<String,Object>();
             	Map<String, Object> mapaResultEuro = new HashMap<String,Object>();
             	mapaResultDolar.put("FECHA", new SimpleDateFormat(IConstantes.FORMATO_FECHA).format(new Date()));
             	mapaResultDolar.put("MONEDA_ORIGEN", IConstantes.DOLARES_AMERICANOS);
             	mapaResultDolar.put("MONEDA_DESTINO", IConstantes.PESOS_COLOMBIANOS_ICEBERG);
             	mapaResultEuro.put("FECHA",new SimpleDateFormat(IConstantes.FORMATO_FECHA).format(new Date()));
             	mapaResultEuro.put("MONEDA_ORIGEN", IConstantes.EUROS);
             	mapaResultEuro.put("MONEDA_DESTINO", IConstantes.DOLARES_AMERICANOS);
             			
//             			try{
//             			ReciboConsignacionHelper.getHelper().setConversionHistorica(IConstantes.FORMATO_FECHA, 
//             					IConstantes.DOLARES_AMERICANOS,IConstantes.PESOS_COLOMBIANOS_ICEBERG, new SimpleDateFormat(IConstantes.FORMATO_FECHA).parse("27/10/2013"));
//             			
//             			ReciboConsignacionHelper.getHelper().setConversionHistorica(IConstantes.FORMATO_FECHA, 
//             					IConstantes.EUROS,IConstantes.DOLARES_AMERICANOS, new SimpleDateFormat(IConstantes.FORMATO_FECHA).parse("27/10/2013"));
//             			
//             			} catch(Exception e){
//             				e.printStackTrace();
//             			}
             			
             			try {
								mapaResultDolar.put("FACTOR", (BigDecimal)ParametrizacionFac.getFacade().obtenerRegistro("getCurrencyFecha", mapaResultDolar));
								mapaResultEuro.put("FACTOR", (BigDecimal)ParametrizacionFac.getFacade().obtenerRegistro("getCurrencyFecha", mapaResultEuro));
						} catch (Exception e) {
								e.printStackTrace();
						}
             			
             				
             			if(mapaResultDolar.get("FACTOR") == null){
             			mapaResultDolar = (Map<String,Object>)ReciboConsignacionHelper.getHelper().setConversionHistorica(IConstantes.FORMATO_FECHA, 
             					IConstantes.DOLARES_AMERICANOS,IConstantes.PESOS_COLOMBIANOS_ICEBERG, new Date());
             			}
             			if(mapaResultEuro.get("FACTOR")== null){
             			mapaResultEuro = (Map<String,Object>)ReciboConsignacionHelper.getHelper().setConversionHistorica(IConstantes.FORMATO_FECHA, 
             					IConstantes.EUROS,IConstantes.DOLARES_AMERICANOS, new Date());
             			}
     
             			log.info("Factor Dolar login: "+mapaResultDolar.get("FACTOR"));
             			log.info("Factor Euro login: "+mapaResultEuro.get("FACTOR"));
             			
             			if(mapaResultDolar.get("FACTOR")==null){
             				mapaResultDolar.put("FACTOR", new BigDecimal(0.00));
             			}
             			
             			if(mapaResultEuro.get("FACTOR")==null){
             				mapaResultEuro.put("FACTOR", new BigDecimal(0.00));
             			}
             				//Obtener tasas representativas del Mercado
             				Double trm = ((BigDecimal)mapaResultDolar.get("FACTOR")).doubleValue();

             				TasaRepresentativaMercado  tasaRepMercadoDolar = new TasaRepresentativaMercado();
             				tasaRepMercadoDolar.setValor(trm);
             				tasaRepMercadoDolar.setFecha(new Date());
             				Moneda monedaDolar = new Moneda();
             				monedaDolar.setCodigo(IConstantes.DOLARES_AMERICANOS);
             				tasaRepMercadoDolar.setMoneda(monedaDolar);
             				Map<String, String> mapaParametro = new HashMap<String, String>();
             				mapaParametro.put("COMISION_RECUPERAR",IConstantes.PARAMETRO_DOLAR);
             				String comisionEuro = new String("0");
             				String comisionDolar = new String("0");
             		
             				try {
             			
             					comisionDolar = (String)ParametrizacionFac.getFacade().obtenerRegistro("seleccionarComisionNegociada", mapaParametro );
             					mapaParametro.clear();
             					mapaParametro.put("COMISION_RECUPERAR",IConstantes.PARAMETRO_EURO );
             					comisionEuro = (String)ParametrizacionFac.getFacade().obtenerRegistro("seleccionarComisionNegociada", mapaParametro );
             				} catch (Exception e) {
						
             					e.printStackTrace();
             				}
             				log.info("comisi�n D�lar: "+comisionDolar);
             				infoSesion.put(IConstantes.COMISION_DOLAR,comisionDolar);
             				log.info("comisi�n Euro: "+comisionEuro);
             				infoSesion.put(IConstantes.COMISION_EURO,comisionEuro);
             				
             				infoSesion.put(IConstantes.TRM_DOLAR, tasaRepMercadoDolar); 		
             
             				//Double trmEuroDolar =ReciboConsignacionHelper.getHelper().getConversion(Currency.EUR, Currency.USD);
             				Double trmEuroDolar =((BigDecimal)mapaResultEuro.get("FACTOR")).doubleValue();
             		
             				log.info("Euro - DOLAR TRM: "+trmEuroDolar);
             		
             				TasaRepresentativaMercado tasaRepMercadoEuro = new TasaRepresentativaMercado();
             				log.info("Tasa EUR - COP"+(trmEuroDolar*trm));
             				tasaRepMercadoEuro.setValor(((Double)(trmEuroDolar*trm)));
             				tasaRepMercadoEuro.setFecha(new Date());
             				Moneda monedaEuro = new Moneda();
             				monedaEuro.setCodigo(IConstantes.EUROS);
             				tasaRepMercadoEuro.setMoneda(monedaEuro);		
             		
             				infoSesion.put(IConstantes.TRM_EURO, tasaRepMercadoEuro);
             		
             				this.getDesktop().getSession().setAttribute(IConstantes.INFORMACION_SESION,infoSesion);
             				this.getDesktop().getSession().setAttribute(IConstantes.AUTENTICADO,"N");

             			 	try{
             	             	Cliente clienteConsulta = (Cliente)ParametrizacionFac.getFacade().obtenerRegistro("selectClienteBasico", cliente);
             	             	if (clienteConsulta != null){
             	             		if(clienteConsulta.getCliente()!=null){
             	             			if(clienteConsulta.getPersona().getDireccionElectronica()==null || 
             	             					clienteConsulta.getPersona().getTelefonoResidencia()==null){
             	             				
             	             				System.out.println("Encuentra dato vac�o");
             	             				
             	             				
             	             				UpdateCorreoTelefonoAction updateCorreoTel = (UpdateCorreoTelefonoAction)Executions.createComponents("pages/updateCorreoTelefono.zul", null, null);
             	             				updateCorreoTel.doModal(clienteConsulta);
             	             				
             	             				rutamenu=(new String("pages/menuPagos.zul"));
             	             				this.cargarZul(rutamenu);
             	             				
             	             				
             	             			}else{
             	             				System.out.println("Datos llenos: "+clienteConsulta.getPersona().getDireccionElectronica()+" "
             	             			+clienteConsulta.getPersona().getTelefonoResidencia());
             	             				
             	             				log.info("Administrador: "+infusuario.getAdministrador());
             	             				rutamenu=(new String("pages/menuPagos.zul"));
             	             				this.cargarZul(rutamenu);
             	             			}
             	             		}else {
             	             			Messagebox.show("Error validando su identidad","Error", Messagebox.OK, Messagebox.ERROR);
             	             		}
             	             		
             	             	}
             	             	} catch(Exception e){
             	             		e.printStackTrace();
             	             	}
             				
			 }else {
				 Messagebox.show("Error validando su ingreso; puede intentar generando una nueva imagen con el bot�n \"generar\"","Error", Messagebox.OK, Messagebox.ERROR);
			 }
			}
			 
		else{	
				Messagebox.show("Error validando su identidad","Error", Messagebox.OK, Messagebox.ERROR);
	
				}
			 }
		 else{
			 	Messagebox.show("No tiene usuario registrado en la Base de datos � su usuario est� bloqueado en biblioteca","Error", Messagebox.OK, Messagebox.ERROR);
		 }
		}

}
