package com.konrad.action;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import com.konrad.domain.Cliente;
import com.konrad.domain.Persona;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.window.ActionStandard;


public class UpdateCorreoTelefonoAction extends ActionStandard {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6814047463504815160L;
	protected static Logger log = Logger.getLogger(UpdateCorreoTelefonoAction.class);
	private Persona persona;
	private Cliente cliente;
	
	
	public void doModal(Cliente cliente){
		
		this.setCliente(cliente);
		((Label)this.getFellow("idlblUpdateCorreoUsuario")).setValue(this.getCliente().getCliente().toString()+"-"+
				this.getCliente().getPersona().getNombreRazonSocial()+" "+
				this.getCliente().getPersona().getPrimerApellido()+" "+this.getCliente().getPersona().getSegundoApellido()
				);
		super.doModal();
	}
	
	public void doOverlapped(){
		super.doOverlapped();
	}

	
	public void onActualizarCorreoTelefono(){
		
		try{
			
			Persona persona = new Persona();
			//String rutamenu  =new String();
			persona.setTelefonoResidencia(((Textbox)this.getFellow("idtxtUpdateTelefono")).getValue());
			persona.setDireccionElectronica(((Textbox)this.getFellow("idtxtUpdateCorreo")).getValue());
			this.getCliente().setPersona(persona);
			
			Map<String, Object> mapaActualizacion = new HashMap<String,Object>();
			mapaActualizacion.put("CLIENTE", this.getCliente().getCliente());
			mapaActualizacion.put("DIRECCION_ELECTRONICA", this.getCliente().getPersona().getDireccionElectronica());
			mapaActualizacion.put("TELEFONO_RESIDENCIA", this.getCliente().getPersona().getTelefonoResidencia());
			
			ParametrizacionFac.getFacade().actualizarRegistro("setDatosCorreoTelPersona", mapaActualizacion);
			
			//rutamenu=(new String("pages/menuPagos.zul"));
			//Component miPage = this.getParent();
			this.detach();
			//Executions.createComponents(rutamenu, miPage, null);
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public UpdateCorreoTelefonoAction() {
		// TODO Auto-generated constructor stub
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

}
