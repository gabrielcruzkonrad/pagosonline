package com.konrad.action;

import java.util.List;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;

import com.konrad.assembler.DerechosAcademicosAssembler;
import com.konrad.domain.Usuario;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.util.GestionaError;
import com.konrad.util.IConstantes;
import com.konrad.window.ActionStandardBorder;


public class UsuarioAction extends ActionStandardBorder implements AfterCompose {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4679547518397017291L;

	protected static Logger log = Logger.getLogger(UsuarioAction.class);
		
		private GestionaError gestionarError=new GestionaError();
		
		public void afterCompose() {		
			try { 
				
				this.buscarMaestro("T");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		@SuppressWarnings("unchecked")
		public void buscarMaestro(String parametro){
			
			log.info("Ejecutando el método [ buscarMaestro ]... ");
			List<Usuario> listaDatos=null;
			
			this.getFellow("gbxFormConsultaUsuario").setVisible(true);
			this.getFellow("formEdicionUsuarios").setVisible(false);
			try {
				
				if (parametro.equalsIgnoreCase("T"))
				{ listaDatos =(List<Usuario>)ParametrizacionFac.getFacade().obtenerRegistro(new String(""));}
				else 
				{listaDatos=(List<Usuario>)ParametrizacionFac.getFacade().obtenerRegistro("",parametro); }
				
				Rows filas = (Rows)this.getFellow("rowsMaestrosQryUsuarios");
				filas.getChildren().clear();       
				log.info("termino de ejecutar la consulta ");
				DerechosAcademicosAssembler usuarioAs = new DerechosAcademicosAssembler();
				
				for (Object object : listaDatos) {
					Row fila = usuarioAs.crearRowDesdeDto(object, this);
					filas.appendChild(fila);
				}

	            Grid tabla = (Grid) this.getFellow("gridMaestrosQryUsuarios");
				// se configura la tabla....
				tabla.setMold("paging");
				tabla.setPageSize(IConstantes.TAMANO_PAGINACION);
				tabla.applyProperties();
				tabla.invalidate();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public void onSelectRecord(String id){
			this.getFellow("gbxFormConsultaUsuario").setVisible(false);
			this.getFellow("formEdicionUsuarios").setVisible(true);
			try {
				Usuario usuario  = new Usuario();
				usuario=(Usuario)ParametrizacionFac.getFacade().obtenerRegistro("", id);
				
				Textbox tbxUsuario = (Textbox)this.getFellow("usuario");
				tbxUsuario.setText(usuario.getUsuario());
				
				Textbox tbxNombresUsuario = (Textbox)this.getFellow("nombresusuario");
				tbxNombresUsuario.setText(usuario.getNombres());
//				
//				Textbox tbxApellidosUsuario = (Textbox)this.getFellow("apellidosusuario");
//				tbxApellidosUsuario.setText(usuario.getApellidos());
				
				/*Textbox tbxEstadoUsuario = (Textbox)this.getFellow("estadousuario");
				tbxEstadoUsuario.setText(usuario.getEstado());*/
				log.info("administrador?: "+usuario.getAdministrador());
				Listitem item = new Listitem();
				item.setValue(usuario.getAdministrador().equalsIgnoreCase("SI")?"S":"N");
				item.setLabel(usuario.getAdministrador());
				((Listbox)this.getFellow("estadousuario")).setSelectedIndex(usuario.getAdministrador().equalsIgnoreCase("SI")?0:1);
				
				
				
//				Textbox tbxNumeroIdentificacionUsuario = (Textbox)this.getFellow("identificacionusuario");
//				tbxNumeroIdentificacionUsuario.setText(usuario.getNumeroidentificacion());
				
				
					
		} catch (Exception e) {
			log.error(e.getMessage());
			gestionarError.mostrarError(e.getMessage(), "B");
		}
	}
	
		public void onActualizarMaestro(String id){
			try{
				this.onSelectRecord(id);
				((Textbox)this.getFellow("operacionusuario")).setValue("A");
			}catch(Exception e){
				log.error(e.getMessage());
				gestionarError.mostrarError(e.getMessage(), "B");
				
			}
		}

		public void onNewMaestro(String id){
			this.getFellow("gbxFormConsultaUsuario").setVisible(false);
			this.getFellow("formEdicionUsuarios").setVisible(true);
			try {
				//-- limpiar formulario 
				//limpiarFormulario("formEdicionUsuarios");
				this.onSelectRecord(id);
				((Textbox)this.getFellow("operacionusuario")).setValue("N");
				
			} catch (Exception e) {
				gestionarError.mostrarError(e.getMessage(), "B");
			}
		}
		
		public void onCancelMaestro(){
			log.info("Ejecutando el metodo [ onCancelMaestro ]... ");		
			this.buscarMaestro("T");	
		this.getFellow ("formEdicionUsuarios").setVisible( false );
		}
		
		public void onInsertMaestro(){
			String op = ((Textbox)this.getFellow("operacionusuario")).getValue();
			try {
				    Usuario usuario =new Usuario();
				    
				    usuario.setUsuario(((Textbox)this.getFellow("usuario")).getValue());
				    usuario.setNombres(((Textbox)this.getFellow("nombresusuario")).getValue());
				   // usuario.setApellidos(((Textbox)this.getFellow("apellidosusuario")).getValue());
				    
				    Listitem itemSelected = ((Listbox)this.getFellow("estadousuario")).getSelectedItem();
					String valueSelected = itemSelected.getValue()!=null?itemSelected.getValue().toString():null;
					usuario.setAdministrador(valueSelected);
					usuario.setEstado("A");
				    
				    //usuario.setNumeroidentificacion(((Textbox)this.getFellow("identificacionusuario")).getValue());
					if (op.equalsIgnoreCase("N"))
					{
						//usuario.setClave(((Textbox)this.getFellow("identificacionusuario")).getValue());
						ParametrizacionFac.getFacade().guardarRegistro("",usuario);
					}
					else 
					{
						ParametrizacionFac.getFacade().actualizarRegistro("", usuario);
					}
				
				this.getFellow ("gbxFormConsultaUsuario").setVisible( true );
				this.getFellow ("formEdicionUsuarios").setVisible( false );
				this.buscarMaestro("T");
			} catch (WrongValueException e) {
				System.out.println(e.getMessage());
				gestionarError.mostrarError(e.getMessage(), "B");
			} catch (Exception e) {
				gestionarError.mostrarError(e.getMessage(), "B");
				System.out.println(e.getMessage());
			}
		}
		
		public void onDeleteRecord(String id){
			try {
			
				if(super.onConfirmacionPopup("Usuario", id.toString()) == 1){
					ParametrizacionFac.getFacade().borrarRegistro("",id);
					this.buscarMaestro("T");
				}
			}
			catch (Exception e) {
				gestionarError.mostrarError(e.getMessage(), "B");
		    }		
		}
		
		public void onBuscarParametro()
		{
			String parametro=((Textbox)this.getFellow("parNombreUsuario")).getValue();
			if(parametro.equalsIgnoreCase(""))
			{
				parametro="T";
			}
			buscarMaestro(parametro);
		}

		
		
}