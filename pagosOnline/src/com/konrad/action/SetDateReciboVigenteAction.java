package com.konrad.action;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import com.konrad.assembler.MensajesAssembler;
import com.konrad.domain.Cliente;
import com.konrad.domain.CuentaReciboConsignacion;
import com.konrad.domain.Mensaje;
import com.konrad.domain.PeriodoFacturacion;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.TasaRepresentativaMercado;
import com.konrad.domain.Usuario;
import com.konrad.domain.VencimientoPeriodo;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.util.IConstantes;
import com.konrad.util.Moneda;
import com.konrad.window.ActionStandard;

public class SetDateReciboVigenteAction extends ActionStandard {
	
	private static final long serialVersionUID = -1840592650435657086L;

	protected static Logger log = Logger.getLogger(SetDateReciboVigenteAction.class);
	private ReciboConsignacion reciboConsignacion;
	private Date fechaMaximaRecibo;


	@SuppressWarnings("rawtypes")
	public void doModal(ReciboConsignacion reciboConsignacion){
		try{
			
			
		this.setReciboConsignacion(reciboConsignacion);
		Mensaje mensaje = new Mensaje();
		String tipoMensaje = IConstantes.INFORMATION;
		mensaje.setMensaje(IConstantes.INFORMACION_FECHA_GENERACION);
		this.onSetMensaje(mensaje, tipoMensaje);
		
		mensaje = new Mensaje();
		mensaje.setMensaje(IConstantes.INFORMACION_HORARIO_BANCARIO);
		this.onSetMensaje(mensaje, tipoMensaje);
		
		ReciboConsignacion reciboAuxFecha = new ReciboConsignacion();
		VencimientoPeriodo periodoAux = new VencimientoPeriodo();
		periodoAux.setFechaVencimiento(new Date());
		reciboAuxFecha.setPeriodo(periodoAux);
		
		log.info("fecha Original antes de validar fecha : "+new SimpleDateFormat(IConstantes.FORMATO_FECHA).format(reciboConsignacion.getPeriodo().getFechaVencimiento()));
		
		Date fechaSistema= new SimpleDateFormat("dd/MM/yyyy").parse((String)ParametrizacionFac.getFacade().obtenerRegistro("selectFechaSistema")); 
		Date fechaGeneracion = new Date(); 
		
				if(fechaSistema.compareTo(reciboConsignacion.getPeriodo().getFechaVencimiento())<=0)
				fechaGeneracion =onValidarFechaMaxima(((Date)ParametrizacionFac.getFacade().obtenerRegistro("getSiguienteDiaHabil", reciboAuxFecha)), reciboConsignacion.getPeriodo().getFechaVencimiento());
				
		((Datebox)this.getFellow("idDbxDateSetDateReciboVigente")).setValue(fechaGeneracion);
		
		//Si es por vencer entonces pone fecha m�xima
		if(fechaSistema.compareTo(reciboConsignacion.getPeriodo().getFechaVencimiento())<=0)
		((Label)this.getFellow("idLblFechaMaximaSetDateReciboVigente")).setValue(new SimpleDateFormat(IConstantes.FORMATO_FECHA).format(reciboConsignacion.getPeriodo().getFechaVencimiento()));
		
		log.info("fecha Original luego de validar fecha: "+new SimpleDateFormat(IConstantes.FORMATO_FECHA).format(reciboConsignacion.getPeriodo().getFechaVencimiento()));
		
		Label labelUsuario = (Label)this.getFellow("idLblUsuarioSetDateReciboVigente");
		labelUsuario.setValue(
			((Usuario)((HashMap)this.getDesktop().getSession().getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.USUARIO_SESSION)).getUsuario()
			+"-"+
			((Usuario)((HashMap)this.getDesktop().getSession().getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.USUARIO_SESSION)).getNombres()		
				);
		
		super.doModal();
		} catch(Exception e){
			e.printStackTrace();
			Messagebox.show(
				    e.getMessage(),
				    "Error de Pagos en L�nea",
				    Messagebox.YES, Messagebox.ERROR);
			e.printStackTrace();
			
		}
	}
	
	public void doOverlapped(){
		super.doOverlapped();
	}
	
	public Date onValidarFechaMaxima(Date fecha, Date fechaMaxima){
		if(fecha.after(fechaMaxima))
		return fechaMaxima;
		else 
		return	fecha;
	}

	public void onChangeDateBox(){
		try{
		Date fecha= ((Datebox)this.getFellow("idDbxDateSetDateReciboVigente")).getValue();
		Date fechaMaxima = new Date();
		fechaMaxima = this.getReciboConsignacion().getPeriodo().getFechaVencimiento();
		fecha = this.onValidarFechaMaxima(fecha, fechaMaxima);
		// Si est� vigente la cuota
		if(new SimpleDateFormat("dd/MM/yyyy").parse((String)ParametrizacionFac.getFacade().obtenerRegistro("selectFechaSistema")).compareTo(fechaMaxima)<=0)
		((Datebox)this.getFellow("idDbxDateSetDateReciboVigente")).setValue(fecha);
		
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void onSalir(){
		this.detach();
	}
	
	public void onSetMensaje(Mensaje mensaje, String tipoMensaje ){
		((Grid)this.getFellow("idGrdSetDateReciboVigenteMensajes")).setVisible(true);
		MensajesAssembler mensajeAs = new MensajesAssembler();
		Rows rowsMensaje = (Rows)this.getFellow("idRowsSetDateReciboVigenteMensajes");
		Row filaMensaje = mensajeAs.crearRowDesdeDto(mensaje, tipoMensaje);
		rowsMensaje.appendChild(filaMensaje);
}
	
	
	
	@SuppressWarnings("unchecked")
	public void onImprimirRecibo(){

		Integer  resultado = Messagebox.show(
			    "�Confirma la generaci�n de este recibo de pago? (Esto puede tomar varios segundos) "+IConstantes.MENSAJE_PERMITIR_POPUPS,
			    "Confirmar Generaci�n de Recibo de Pago",
			    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
		if(resultado == Messagebox.YES){
			// Se fijan variables para el nuevo recibo
			ReciboConsignacion nuevoRecibo = new ReciboConsignacion();
			log.info("fecha Original antes de asignacion: "+new SimpleDateFormat(IConstantes.FORMATO_FECHA).format(reciboConsignacion.getPeriodo().getFechaVencimiento()));
			Cliente cliente = new Cliente();
			cliente.setCliente(reciboConsignacion.getCliente().getCliente());
			nuevoRecibo.setCliente(cliente);
			nuevoRecibo.setReciboConsignacion(reciboConsignacion.getReciboConsignacion());
			Datebox dateValue = (Datebox)this.getFellow("idDbxDateSetDateReciboVigente");
			
			VencimientoPeriodo periodo = new VencimientoPeriodo();
			periodo.setFechaVencimiento(reciboConsignacion.getPeriodo().getFechaVencimiento());
			PeriodoFacturacion periodoFact = new PeriodoFacturacion();
			periodoFact.setPeriodo(reciboConsignacion.getPeriodo().getPeriodo().getPeriodo());
			periodo.setPeriodo(periodoFact);
			nuevoRecibo.setPeriodo(periodo);
			nuevoRecibo.setFechaPago(dateValue.getValue());
			CuentaReciboConsignacion cuenta = new CuentaReciboConsignacion();
			cuenta.setEntidad(reciboConsignacion.getCuentaReciboConsignacion().getEntidad());
			cuenta.setNumeroCuenta(reciboConsignacion.getCuentaReciboConsignacion().getNumeroCuenta());
			cuenta.setTipoEntidad(reciboConsignacion.getCuentaReciboConsignacion().getTipoEntidad());
			cuenta.setOficinaEntidad(reciboConsignacion.getCuentaReciboConsignacion().getOficinaEntidad());
			nuevoRecibo.setCuentaReciboConsignacion(cuenta);
			nuevoRecibo.setFecha(reciboConsignacion.getFecha());
			nuevoRecibo.setGeneraComision(reciboConsignacion.getGeneraComision());
			nuevoRecibo.setNumeroFila(reciboConsignacion.getNumeroFila());
			nuevoRecibo.setObservaciones(reciboConsignacion.getObservaciones());
			
			TasaRepresentativaMercado tasaRepresentativa = new TasaRepresentativaMercado();
			tasaRepresentativa.setFecha(reciboConsignacion.getTasaRepresentativa().getFecha());
			tasaRepresentativa.setFestivo(reciboConsignacion.getTasaRepresentativa().getFestivo());
			Moneda moneda = new Moneda();
			moneda.setCodigo(reciboConsignacion.getTasaRepresentativa().getMoneda().getCodigo());
			moneda.setDescripcion(reciboConsignacion.getTasaRepresentativa().getMoneda().getDescripcion());
			tasaRepresentativa.setMoneda(moneda);
			tasaRepresentativa.setValor(reciboConsignacion.getTasaRepresentativa().getValor());
			nuevoRecibo.setTasaRepresentativa(tasaRepresentativa);
			
			TasaRepresentativaMercado tasaRepresentativaVencida = new TasaRepresentativaMercado();
			tasaRepresentativaVencida.setFecha(reciboConsignacion.getTasaRepresentativaVencido().getFecha());
			tasaRepresentativaVencida.setFestivo(reciboConsignacion.getTasaRepresentativaVencido().getFestivo());
			Moneda monedaVencida = new Moneda();
			monedaVencida.setCodigo(reciboConsignacion.getTasaRepresentativaVencido().getMoneda().getCodigo());
			monedaVencida.setDescripcion(reciboConsignacion.getTasaRepresentativaVencido().getMoneda().getDescripcion());
			tasaRepresentativaVencida.setMoneda(monedaVencida);
			tasaRepresentativaVencida.setValor(reciboConsignacion.getTasaRepresentativaVencido().getValor());
			nuevoRecibo.setTasaRepresentativaVencido(tasaRepresentativaVencida);
			
			nuevoRecibo.setValorDetalle(reciboConsignacion.getValorDetalle());
			nuevoRecibo.setValorTotal(reciboConsignacion.getValorTotal());
			nuevoRecibo.setValorVencido(reciboConsignacion.getValorVencido());
			nuevoRecibo.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
			nuevoRecibo.setBanderaFecha(IConstantes.GENERAL_NO);
			nuevoRecibo.setBanderaDiaHabil(IConstantes.GENERAL_SI);
			
			log.info("fecha Original antes de vencimientos: "+new SimpleDateFormat(IConstantes.FORMATO_FECHA).format(reciboConsignacion.getPeriodo().getFechaVencimiento()));
			// Calculamos vencimientos a la fecha y luego procedemos con la creaci�n del nuevo recibo
			try {
				ReciboConsignacion reciboAuxFecha = new ReciboConsignacion();
				VencimientoPeriodo periodoAuxFecha = new VencimientoPeriodo();
				periodoAuxFecha.setFechaVencimiento(nuevoRecibo.getFechaPago());
				reciboAuxFecha.setPeriodo(periodoAuxFecha);
				Date fechaGeneracion = (Date)ParametrizacionFac.getFacade().obtenerRegistro("getSiguienteDiaHabil", reciboAuxFecha);
				periodoAuxFecha.setFechaVencimiento(fechaGeneracion);
				reciboAuxFecha.setPeriodo(periodoAuxFecha);
				Cliente clienteAux = new Cliente();
				clienteAux.setCliente(reciboConsignacion.getCliente().getCliente());
				reciboAuxFecha.setCliente(clienteAux);
				reciboAuxFecha.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
				
				ParametrizacionFac.getFacade().ejecutarProcedimiento("calcularVencimientosCliente",reciboAuxFecha);
				log.info("fecha Original: "+new SimpleDateFormat(IConstantes.FORMATO_FECHA).format(reciboConsignacion.getPeriodo().getFechaVencimiento()));
				log.info("fecha Nueva: "+new SimpleDateFormat(IConstantes.FORMATO_FECHA).format(nuevoRecibo.getPeriodo().getFechaVencimiento()));
				log.info("fecha Pago: "+new SimpleDateFormat(IConstantes.FORMATO_FECHA).format(nuevoRecibo.getFechaPago()));
				Double liquidacion = (Double)ParametrizacionFac.getFacade().obtenerRegistro("selectValorReciboVencido", nuevoRecibo);	
				System.out.println("liquidacion: "+liquidacion.intValue()+" valor vencido: "+nuevoRecibo.getValorVencido().intValue());
				if(Math.abs(liquidacion.intValue()-nuevoRecibo.getValorVencido().intValue())>=IConstantes.MONTO_GENERACION_RECIBO ||
						nuevoRecibo.getFechaPago().compareTo(nuevoRecibo.getPeriodo().getFechaVencimiento())!=0 || 
						(nuevoRecibo.getFechaPago().compareTo(nuevoRecibo.getPeriodo().getFechaVencimiento())==0 && 
						Math.abs(Math.round(liquidacion)-Math.round(nuevoRecibo.getValorDetalle()))>=IConstantes.MONTO_GENERACION_RECIBO) ){
						nuevoRecibo.setValorVencido(liquidacion);
						List <ReciboConsignacion> recibosBusqueda = 
						(List<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("buscarReciboCancelacionSaldos", nuevoRecibo);
				
					if(recibosBusqueda!=null){
						if(recibosBusqueda.size()>0){
							nuevoRecibo.setReciboConsignacion(recibosBusqueda.get(0).getReciboConsignacion());
						}else{
						ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarReciboReliquidado", nuevoRecibo);	
						}
					}else{
					ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarReciboReliquidado", nuevoRecibo);	
					}
				}
				// impresi�n del recibo de consignaci�n
				if(reciboConsignacion.getReciboConsignacion()!= null && nuevoRecibo.getReciboConsignacion()!=null){
					
						PrintReportAction reportAction = (PrintReportAction)Executions.createComponents("pages/printReport.zul", null, null);
						reportAction.doModal(IConstantes.REP_CUOTA_CREDITO, nuevoRecibo);
						this.detach();
				}else{
					Messagebox.show(
						    IConstantes.ERROR_GENERACION_RECIBO,
						    "Error de Pagos en L�nea",
						    Messagebox.YES, Messagebox.ERROR);
				  Rows rowsMensaje = (Rows)this.getFellow("idRowsSetDateReciboVigenteMensajes"); 
				  rowsMensaje.getChildren().clear();
				  Mensaje mensaje = new Mensaje();
				  mensaje.setMensaje(IConstantes.ERROR_GENERACION_RECIBO);
				  String tipoMensaje = IConstantes.ERROR;
				  this.onSetMensaje(mensaje, tipoMensaje);
				}
			} catch (Exception e) {
				Messagebox.show(
					    e.getMessage(),
					    "Error de Pagos en L�nea",
					    Messagebox.YES, Messagebox.ERROR);
				e.printStackTrace();
			}
			
			
		}
		
	}
	

	

	public ReciboConsignacion getReciboConsignacion() {
		return reciboConsignacion;
	}

	public void setReciboConsignacion(ReciboConsignacion reciboConsignacion) {
		this.reciboConsignacion = reciboConsignacion;
	}

	public Date getFechaMaximaRecibo() {
		return fechaMaximaRecibo;
	}

	public void setFechaMaximaRecibo(Date fechaMaximaRecibo) {
		this.fechaMaximaRecibo = fechaMaximaRecibo;
	}	
	
}