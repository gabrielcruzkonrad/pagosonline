package com.konrad.action;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;

import com.konrad.assembler.InscripcionEncuentroAssembler;
import com.konrad.domain.CentroCosto;
import com.konrad.domain.Cliente;
import com.konrad.domain.Departamento;
import com.konrad.domain.Pais;
import com.konrad.domain.Persona;
import com.konrad.domain.Poblacion;
import com.konrad.domain.ProgramaAcademico;
import com.konrad.domain.ProgramaReporte;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.TipoIdentificacion;
import com.konrad.domain.TransaccionZonaPagos;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.helper.ReciboConsignacionHelper;
import com.konrad.util.IConstantes;
import com.konrad.window.ActionStandardBorder;
import com.zonapagos.www.prod.ZPagosLocator;
import com.zonapagos.www.prod.ZPagosSoapProxy;


public class RecibirInscripcionEncuentroAction  extends ActionStandardBorder implements AfterCompose {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected static Logger log = Logger.getLogger(RecibirInscripcionEncuentroAction.class);
	private ZPagosLocator zonaPagosLocator;
	private ZPagosSoapProxy zonaPagosProxy;

	
	public void cargarZul(String menu){
		Component miPage = this.getParent();
		this.detach();
		Executions.createComponents(menu, miPage, null);
	}
	
public void onRecibirParametros(){
	try{
		log.info("ejecutando el m�todo [onRecibirParametros] ");
		// se reciben los par�metros desde el GET
	TransaccionZonaPagos transaccion = new TransaccionZonaPagos();
	transaccion.setIdPago(new Long(
			Executions.getCurrent().getParameterMap().get("id_pago")!=null? 
			 !((String[])Executions.getCurrent().getParameterMap().get("id_pago"))[0].equals("") ?
					((String[])Executions.getCurrent().getParameterMap().get("id_pago"))[0].toString():"0":"0"
			));
	log.info("pasa id pago "+transaccion.getIdPago());
	transaccion.setEstadoPago(new Long(
			Executions.getCurrent().getParameterMap().get("estado_pago")!=null?
					 !((String[])Executions.getCurrent().getParameterMap().get("estado_pago"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("estado_pago"))[0].toString():"0":"0"
			));
	log.info("pasa estado pago "+transaccion.getEstadoPago());
	transaccion.setIdFormaPago(new Long(
			Executions.getCurrent().getParameterMap().get("id_forma_pago")!= null?
					!((String[])Executions.getCurrent().getParameterMap().get("id_forma_pago"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("id_forma_pago"))[0].toString():"0":"0"
			));
	
	log.info("pasa forma pago "+transaccion.getIdFormaPago());
	transaccion.setValorPagado(new Double(
			Executions.getCurrent().getParameterMap().get("valor_pagado")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("valor_pagado"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("valor_pagado"))[0].toString():"0.00":"0.00"
			));
	log.info("pasa valor pago "+transaccion.getValorPagado());
	transaccion.setTicketId(new Long(
			Executions.getCurrent().getParameterMap().get("ticketID")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("ticketID"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("ticketID"))[0].toString():"0":"0"
			));
	log.info("pasa tcket id "+transaccion.getTicketId());
	transaccion.setIdClave(
			Executions.getCurrent().getParameterMap().get("id_clave")!=null?
				 !((String[])Executions.getCurrent().getParameterMap().get("id_clave"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("id_clave"))[0].toString():"0":"0"
			);
	log.info("pasa id clave "+transaccion.getIdClave());
	transaccion.setIdCliente(
			Executions.getCurrent().getParameterMap().get("id_cliente")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("id_cliente"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("id_cliente"))[0].toString():"0":"0"
					);
	log.info("pasa id cliente "+transaccion.getIdCliente());
	
	transaccion.setFranquicia(
					Executions.getCurrent().getParameterMap().get("franquicia")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("franquicia"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("franquicia"))[0].toString():"0":"0"
						);
	
	transaccion.setCodigoServicio(new Long(
			Executions.getCurrent().getParameterMap().get("codigo_servicio")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("codigo_servicio"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("codigo_servicio"))[0].toString():"0":"0"
			));
	
	transaccion.setCodigoBanco(new Long(
				Executions.getCurrent().getParameterMap().get("codigo_banco")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("codigo_banco"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("codigo_banco"))[0].toString():"0":"0"
			));
	
	transaccion.setNombreBanco(
					Executions.getCurrent().getParameterMap().get("nombre_banco")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("nombre_banco"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("nombre_banco"))[0].toString():"0":"0"
			);
	
	transaccion.setCodigoTransaccion(new Long(
			Executions.getCurrent().getParameterMap().get("codigo_transaccion")!=null?
					 !((String[])Executions.getCurrent().getParameterMap().get("codigo_transaccion"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("codigo_transaccion"))[0].toString():"0":"0"
			));
	
	transaccion.setCicloTransaccion(new Long(
			Executions.getCurrent().getParameterMap().get("ciclo_transaccion")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("ciclo_transaccion"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("ciclo_transaccion"))[0].toString():"0":"0"
			));
	
	transaccion.setCampo1(
			Executions.getCurrent().getParameterMap().get("campo1")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("campo1"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("campo1"))[0].toString():"0":"0"
			);
	
	transaccion.setCampo2(
			Executions.getCurrent().getParameterMap().get("campo2")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("campo2"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("campo2"))[0].toString():"0":"0"
			);
	
	transaccion.setCampo3(
			Executions.getCurrent().getParameterMap().get("campo3")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("campo3"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("campo3"))[0].toString():"0":"0"
			);
	
	transaccion.setIdComercio(new Long(
			Executions.getCurrent().getParameterMap().get("idcomercio")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("idcomercio"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("idcomercio"))[0].toString():"0":"0"
			));
	
	transaccion.setDatFecha(new Date());
	

	
	}catch(Exception e){
		e.printStackTrace();
	}
}


public void onValidarGenero(){
	
	Listbox listaGenero = (Listbox)this.getFellow("LbxRecibirInscripcionEncuentroSexo");
	Listitem itemSeleccionado = (Listitem)listaGenero.getSelectedItem();
	if(itemSeleccionado.getValue().equals("0")){
		Clients.showNotification("Seleccione por favor un g�nero v�lido", Clients.NOTIFICATION_TYPE_ERROR, listaGenero, "end_before", 4000, true);
		throw new WrongValueException("valor inv�lido; Seleccione por favor un g�nero v�lido");
	}
}


public void onValidarTipoIdentificacion(){
	
	Listbox listaTipoIdentificacion = (Listbox)this.getFellow("LbxRecibirInscripcionEncuentroTipoIdentificacion");
	Listitem itemSeleccionado = (Listitem)listaTipoIdentificacion.getSelectedItem();
	if(itemSeleccionado.getValue().equals("0")){
		Clients.showNotification("Seleccione por favor un tipo de identificaci�n v�lido", Clients.NOTIFICATION_TYPE_ERROR, listaTipoIdentificacion, "end_before", 4000, true);
		throw new WrongValueException("valor inv�lido; Seleccione por favor un tipo de identificaci�n v�lido");
	}
}

public void onValidarTratamientoDatos(){
	log.info("[recibirInscripcion.onValidateTratamientoDatos]");
	Checkbox cajaChequeo = (Checkbox)this.getFellow("chbxRecibirInscripcionRRHHAceptarPersonal");
	if(!cajaChequeo.isChecked()){
		Clients.showNotification("Debe aceptar tratamiento de datos personales", Clients.NOTIFICATION_TYPE_ERROR, cajaChequeo, "end_before", 4000, true);
		throw new WrongValueException("No se ha aceptado tratamiento de datos personales");
	}
}


public void onCheckPersonales(){
    ((Checkbox)this.getFellow("chbxRecibirInscripcionRRHHAceptarPersonal")).setValue(1);
}

public void onImprimir(){
	Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupInscripcionEncuentro");
	if(radioGroup.getItemCount()>0){
		Radio radio = radioGroup.getSelectedItem();
		ReciboConsignacion recibo = (ReciboConsignacion)radio.getAttribute("RECIBO_CONSIGNACION");
		this.onImprimirReporte(recibo);
	}
}


public void onImprimirReporte(ReciboConsignacion recibo){
	try{
		ProgramaReporte programaReporte = new ProgramaReporte();
		CentroCosto centroCosto = new CentroCosto();
		centroCosto.setCentroCosto(recibo.getOrden().getCentroCosto().getCentroCosto());
		programaReporte.setCentroCosto(centroCosto);
		programaReporte = (ProgramaReporte)ParametrizacionFac.getFacade().obtenerRegistro("selectProgramaReporte", programaReporte);
		Map<String,Object> mapaParametros = new HashMap<String, Object>();
		mapaParametros.put("ORDEN", recibo.getOrden().getOrden());
		mapaParametros.put("ORGANIZACION", recibo.getOrden().getOrganizacion());
		mapaParametros.put("DOCUMENTO", recibo.getOrden().getDocumento());
		mapaParametros.put("CLIENTE", recibo.getCliente().getCliente());
		ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarImpresionOrden", mapaParametros);
		PrintReportAction printReportAction = (PrintReportAction)Executions.createComponents("pages/printReport.zul", null, null);
		printReportAction.doModal(programaReporte!=null?programaReporte.getReporte():IConstantes.REP_ORDEN, recibo);
		
		
		}catch(Exception e){
			e.printStackTrace();
			Messagebox.show(e.getMessage(),
				    "Error de Pagos en L�nea",
				    Messagebox.YES, Messagebox.ERROR);
		}
	
}

public void setMensajeVentanasEmergentes(Component componente, String posicion ){
	Clients.showNotification(IConstantes.MENSAJE_PERMITIR_POPUPS_INSCRIPCION, Clients.NOTIFICATION_TYPE_WARNING, componente, posicion, 20000, true);
}

public void setMensajeColegioColombiano(Component componente, String posicion ){
	Clients.showNotification(IConstantes.MENSAJE_COLEGIO_COLOMBIANO_PSICOLOGIA, Clients.NOTIFICATION_TYPE_WARNING, componente, posicion, 20000, true);
}



public void buscarMaestro(List<ReciboConsignacion> listaDatos){
	
	log.info("Ejecutando el metodo [ buscarMaestro ]... ");
	this.getFellow("idRecibirInscripcionEncuentroGrdPrincipal").setVisible(true);
	
	try {
		
		Rows filas = (Rows)this.getFellow("idRecibirInscripcionEncuentroRowsPrincipal");
		filas.getChildren().clear();       
		Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupInscripcionEncuentro");
		radioGroup.detach();
		radioGroup = new Radiogroup();
		radioGroup.setId("idRadioGroupInscripcionEncuentro");
		((Groupbox)this.getFellow("idGbxFormConsultaInscripcionEncuentro")).appendChild(radioGroup);
	
		
		radioGroup.applyProperties();
		radioGroup.invalidate();
		log.info("numero de �tems en el radio Group: "+radioGroup.getItemCount());
		
		log.info("termino de ejecutar la consulta ");
		InscripcionEncuentroAssembler derechosAs = new InscripcionEncuentroAssembler();
		if (listaDatos!=null){
		if (listaDatos.size()>0){
			for (Object object : listaDatos) {
				Row fila = derechosAs.crearRowDesdeDto(object, this);
				filas.appendChild(fila);
				}
			log.info("Seleccionar primer detalle autom�ticamente");
			radioGroup.setSelectedIndex(0);
			((Button)this.getFellow("idBtnRecibirInscripcionEncuentroImprimirOrden")).setDisabled(false);
			((Button)this.getFellow("idBtnRecibirInscripcionEncuentroPagar")).setDisabled(false);  
			}else{
				((Button)this.getFellow("idBtnRecibirInscripcionEncuentroImprimirOrden")).setDisabled(true);
				((Button)this.getFellow("idBtnRecibirInscripcionEncuentroPagar")).setDisabled(true);
			}
		} else{
			((Button)this.getFellow("idBtnRecibirInscripcionEncuentroImprimirOrden")).setDisabled(true);
			((Button)this.getFellow("idBtnRecibirInscripcionEncuentroPagar")).setDisabled(true);
		}

        Grid tabla = (Grid) this.getFellow("idRecibirInscripcionEncuentroGrdPrincipal");
        
		// se configura la tabla....
		tabla.setMold("paging");
		tabla.setPageSize(IConstantes.TAMANO_PAGINACION);
		tabla.applyProperties();
		tabla.invalidate();
	}catch (Exception e) {
		e.printStackTrace();
	}
}

public void setMensajeVentanasEmergentes(){
	Clients.showNotification(IConstantes.MENSAJE_PERMITIR_POPUPS_INSCRIPCION, Clients.NOTIFICATION_TYPE_WARNING, this, "before_center", 20000, true);
}

public void setMensajeColegioColombiano(){
	Clients.showNotification(IConstantes.MENSAJE_COLEGIO_COLOMBIANO_PSICOLOGIA, Clients.NOTIFICATION_TYPE_WARNING, this, "before_center", 20000, true);
}


@SuppressWarnings("unchecked")
public void registrarInscripcionEncuentro(){
	Persona persona = insertInfoEncuentro();
	try{
		if(persona!= null){
			if(persona.getCliente()!=null){
				if(persona.getCliente().getCliente()!=null){
					ReciboConsignacion recibo = new ReciboConsignacion();
					recibo.setCliente(persona.getCliente());
					List<ReciboConsignacion> listaRecibos = (ArrayList<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("selectRecibosConsignacionOrdenesEncuentro", recibo);
					if(listaRecibos != null){
						if(listaRecibos.size()>0){
							this.buscarMaestro(listaRecibos);
							((Groupbox)this.getFellow("idGbxFormConsultaInscripcionEncuentro")).setVisible(true);
							((Groupbox)this.getFellow("idGbxFormConsultaInscripcionEncuentro")).setOpen(true);
							((Groupbox)this.getFellow("idGbxFormGenerarInscripcionEncuentro")).setOpen(false);
							
							if(!IConstantes.MENSAJE_PERMITIR_POPUPS_INSCRIPCION.equalsIgnoreCase("")){
								this.setMensajeVentanasEmergentes();
							}
							
							if(!IConstantes.MENSAJE_COLEGIO_COLOMBIANO_PSICOLOGIA.equalsIgnoreCase("")){
								if(persona.getPerteneceColegioColombianoPsicologos().equalsIgnoreCase("S")){
									this.setMensajeColegioColombiano();
								}
							}
							
						}else{
							ParametrizacionFac.getFacade().ejecutarProcedimiento("generarOrdenEncuentro", persona);
							listaRecibos = (ArrayList<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("selectRecibosConsignacionOrdenesEncuentro", recibo);
							if(listaRecibos!=null){
								if(listaRecibos.size()>0){
									this.buscarMaestro(listaRecibos);
									((Groupbox)this.getFellow("idGbxFormConsultaInscripcionEncuentro")).setVisible(true);
									((Groupbox)this.getFellow("idGbxFormConsultaInscripcionEncuentro")).setOpen(true);
									((Groupbox)this.getFellow("idGbxFormGenerarInscripcionEncuentro")).setOpen(false);
									if(!IConstantes.MENSAJE_PERMITIR_POPUPS_INSCRIPCION.equalsIgnoreCase("")){
										this.setMensajeVentanasEmergentes();
									}
									
									if(!IConstantes.MENSAJE_COLEGIO_COLOMBIANO_PSICOLOGIA.equalsIgnoreCase("")){
										if(persona.getPerteneceColegioColombianoPsicologos().equalsIgnoreCase("S")){
											this.setMensajeColegioColombiano();
										}
									}
								}
							}
						}
					}else{
						ParametrizacionFac.getFacade().ejecutarProcedimiento("generarOrdenEncuentro", persona);
						listaRecibos = (ArrayList<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("selectRecibosConsignacionOrdenesEncuentro", recibo);
						if(listaRecibos!=null){
							if(listaRecibos.size()>0){
								this.buscarMaestro(listaRecibos);
								((Groupbox)this.getFellow("idGbxFormConsultaInscripcionEncuentro")).setVisible(true);
								((Groupbox)this.getFellow("idGbxFormConsultaInscripcionEncuentro")).setOpen(true);
								((Groupbox)this.getFellow("idGbxFormGenerarInscripcionEncuentro")).setOpen(false);
								
								if(!IConstantes.MENSAJE_PERMITIR_POPUPS_INSCRIPCION.equalsIgnoreCase("")){
									this.setMensajeVentanasEmergentes();
								}
								
								if(!IConstantes.MENSAJE_COLEGIO_COLOMBIANO_PSICOLOGIA.equalsIgnoreCase("")){
									if(persona.getPerteneceColegioColombianoPsicologos().equalsIgnoreCase("S")){
										this.setMensajeColegioColombiano();
									}
								}
								
							}
						}
					}
					
					
					
				}
			}
		}
		
	}catch(Exception e){
		log.info(e.getMessage());
		e.printStackTrace();
	}	
}


public void onPagarEnLineaPayUW(){
	
	Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupInscripcionEncuentro");
	Integer  resultado = Messagebox.show(
		    "�Confirma este pago? "+IConstantes.MENSAJE_PERMITIR_POPUPS_INSCRIPCION,
		    "Confirmar Inicio Pago",
		    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
	if(resultado==Messagebox.YES){

	Radio radio = radioGroup.getSelectedItem();
	ReciboConsignacion recibo = (ReciboConsignacion)radio.getAttribute("RECIBO_CONSIGNACION");
	log.info("Recibo "+recibo.getReciboConsignacion());
	log.info("valor "+recibo.getValorTotal());
	log.info("valor detalle "+recibo.getValorDetalle());

	
	if (recibo!= null){
		try {
			ReciboConsignacionHelper.getHelper().sendPostPayU(recibo, IConstantes.TIPO_INVOCACION_DERECHOS_ACADEMICOS);
			ReciboConsignacionHelper.getHelper().registrarAuditoriaPayU(recibo, IConstantes.TIPO_INVOCACION_DERECHOS_ACADEMICOS);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e.getMessage());
			}
		}
	}
}


@SuppressWarnings({ "unchecked" })
public void onPagarEnLinea(){
	
	Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupInscripcionEncuentro");
	Integer  resultado = Messagebox.show(
		    "�Confirma este pago? "+IConstantes.MENSAJE_PERMITIR_POPUPS_INSCRIPCION,
		    "Confirmar Inicio Pago",
		    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
	if(resultado==Messagebox.YES){

	String[] listServiciosMulti = null;
	String[] listNitMulti = null;
	double[] listValIvaMulti = null;
	double[] listIvaMulti = null;
	String wsResult;
	
	String trmDolar ="0"; 
	try{
	 trmDolar = String.valueOf((ReciboConsignacionHelper.getHelper().getConversion(IConstantes.RUTA_CONVERSION_DOLAR,"yyyy-MM-dd" , new Date())));
	}catch(Exception e){
		trmDolar = "0";
	}
	
	
	Radio radio = radioGroup.getSelectedItem();
	ReciboConsignacion recibo = (ReciboConsignacion)radio.getAttribute("RECIBO_CONSIGNACION");
	log.info("Recibo "+recibo.getReciboConsignacion());
	log.info("valor "+recibo.getValorTotal());
	log.info("valor detalle "+recibo.getValorDetalle());
	this.setZonaPagosLocator(new ZPagosLocator());
	this.setZonaPagosProxy(new ZPagosSoapProxy());
	com.zonapagos.www.test.ZPagosSoapProxy zonaPagosProxyPruebas = new com.zonapagos.www.test.ZPagosSoapProxy();
	
	Map<String, String> mapaParametros = new HashMap<String, String>();
	mapaParametros.put("ESTADO_PENDIENTE_INICIAR",IConstantes.ESTADO_PENDIENTE_INICIAR);
	mapaParametros.put("ESTADO_PENDIENTE_FINALIZAR",IConstantes.ESTADO_PENDIENTE_FINALIZAR);
	mapaParametros.put("ID_CLIENTE",recibo.getCliente().getCliente().toString());
	List<TransaccionZonaPagos> listaDatos = new ArrayList<TransaccionZonaPagos>();
	listaDatos = null;

	try {
		listaDatos = (List<TransaccionZonaPagos>)ParametrizacionFac.getFacade().obtenerListado("selectTransaccionZonaPagosPendientes", mapaParametros);
	} catch (Exception e) {
		e.printStackTrace();
	}
	if(listaDatos.size()<=0){
	if (recibo!= null){
		try {
			if(ReciboConsignacionHelper.getHelper().getServicioPruebasProduccion()){
				log.info("Invocamos el servicio de producci�n");
				wsResult = this.getZonaPagosProxy().inicio_pagoV2(
						recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
								(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
								Integer.parseInt(IConstantes.ID_TIENDA_BANCOLOMBIA):
								Integer.parseInt(IConstantes.ID_TIENDA):
									recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
											Integer.parseInt(IConstantes.ID_TIENDA_DOLARES):
												Integer.parseInt(IConstantes.ID_TIENDA_EURO), 
								recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
										(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
						            	&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
										IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
										IConstantes.CLAVE_SERVICIO:
											recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
													IConstantes.CLAVE_SERVICIO_DOLARES:
													IConstantes.CLAVE_SERVICIO_EURO,  
													recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
															recibo.getValorDetalle().doubleValue():
															recibo.getTasaRepresentativa().getValor().doubleValue(), 
						new Double(0).doubleValue(), 
						recibo.getReciboConsignacion().toString(), 
						recibo.getObservaciones().substring(0, recibo.getObservaciones().length()>=70?69:recibo.getObservaciones().length()), 
						recibo.getCliente().getPersona().getDireccionElectronica(), 
						recibo.getCliente().getCliente().toString(), 
						recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("C.C")?
							new String("1"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NIT")?
							new String("3"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("T.I")?
							new String("5"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("CEX")?	
							new String("2"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NEX")?			
							new String("10"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("RC")?		
							new String("9"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("PS")?			
							new String("6"):
							new String("11"), 
							recibo.getCliente().getPersona().getNombreRazonSocial().substring(
									0,	recibo.getCliente().getPersona().getNombreRazonSocial().length()>=50?49:
										recibo.getCliente().getPersona().getNombreRazonSocial().length()), 
								(recibo.getCliente().getPersona().getPrimerApellido()+" "+
										recibo.getCliente().getPersona().getSegundoApellido()).substring(
												0, (recibo.getCliente().getPersona().getPrimerApellido()+" "+
														recibo.getCliente().getPersona().getSegundoApellido())
														.length()>=50?49:
															(recibo.getCliente().getPersona().getPrimerApellido()+" "+
																	recibo.getCliente().getPersona().getSegundoApellido())
																	.length()
														), 
														recibo.getCliente().getPersona().getTelefonoResidencia().substring(0, 
																recibo.getCliente().getPersona().getTelefonoResidencia().length()>=50?49:	
																	recibo.getCliente().getPersona().getTelefonoResidencia().length()),
						trmDolar, 
						new String(recibo.getTasaRepresentativa().getValor().toString()+" "+recibo.getTasaRepresentativa().getMoneda().getCodigo()), 
						new String(recibo.getPeriodo()!=null?
								recibo.getPeriodo().getPeriodo()!=null?
										recibo.getPeriodo().getPeriodo().getPeriodo():"":""), 
						recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
								(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
								IConstantes.CODIGO_SERVICIO_BANCOLOMBIA:
								IConstantes.CODIGO_SERVICIO:
									recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
											IConstantes.CODIGO_SERVICIO_DOLARES:
											IConstantes.CODIGO_SERVICIO_EURO,  
						listServiciosMulti,
						listNitMulti,
						listValIvaMulti,
						listIvaMulti, 
						Integer.parseInt(new String("0")));
			}else{
				log.info("Invocamos el servicio de pruebas");
				wsResult = zonaPagosProxyPruebas.inicio_pagoV2(
						recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
								(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
								Integer.parseInt(IConstantes.ID_TIENDA_BANCOLOMBIA):
								Integer.parseInt(IConstantes.ID_TIENDA):
									recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
											Integer.parseInt(IConstantes.ID_TIENDA_DOLARES):
												Integer.parseInt(IConstantes.ID_TIENDA_EURO)	
										, 
										recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
												(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
				            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
												IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
												IConstantes.CLAVE_SERVICIO:
													recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
															IConstantes.CLAVE_SERVICIO_DOLARES:
															IConstantes.CLAVE_SERVICIO_EURO, 
						recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
															recibo.getValorDetalle().doubleValue():
															recibo.getTasaRepresentativa().getValor().doubleValue(), 
						new Double(0).doubleValue(), 
						recibo.getReciboConsignacion().toString(), 
						recibo.getObservaciones().substring(0, recibo.getObservaciones().length()>=70?69:recibo.getObservaciones().length()), 
						recibo.getCliente().getPersona().getDireccionElectronica(), 
						recibo.getCliente().getCliente().toString(), 
						recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("C.C")?
							new String("1"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NIT")?
							new String("3"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("T.I")?
							new String("5"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("CEX")?	
							new String("2"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NEX")?			
							new String("10"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("RC")?		
							new String("9"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("PS")?			
							new String("6"):
							new String("11"), 
							recibo.getCliente().getPersona().getNombreRazonSocial().substring(
									0,	recibo.getCliente().getPersona().getNombreRazonSocial().length()>=50?49:
										recibo.getCliente().getPersona().getNombreRazonSocial().length()), 
								(recibo.getCliente().getPersona().getPrimerApellido()+" "+
										recibo.getCliente().getPersona().getSegundoApellido()).substring(
												0, (recibo.getCliente().getPersona().getPrimerApellido()+" "+
														recibo.getCliente().getPersona().getSegundoApellido())
														.length()>=50?49:
															(recibo.getCliente().getPersona().getPrimerApellido()+" "+
																	recibo.getCliente().getPersona().getSegundoApellido())
																	.length()
														), 
														recibo.getCliente().getPersona().getTelefonoResidencia().substring(0, 
																recibo.getCliente().getPersona().getTelefonoResidencia().length()>=50?49:	
																	recibo.getCliente().getPersona().getTelefonoResidencia().length()), 
						trmDolar, 
						new String(recibo.getTasaRepresentativa().getValor().toString()+" "+recibo.getTasaRepresentativa().getMoneda().getCodigo()), 
						new String(recibo.getPeriodo()!=null?
								recibo.getPeriodo().getPeriodo()!=null?
										recibo.getPeriodo().getPeriodo().getPeriodo():"":""), 
						recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
								(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
								IConstantes.CODIGO_SERVICIO_BANCOLOMBIA:
								IConstantes.CODIGO_SERVICIO:
									recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
											IConstantes.CODIGO_SERVICIO_DOLARES:
											IConstantes.CODIGO_SERVICIO_EURO, 
						listServiciosMulti,
						listNitMulti,
						listValIvaMulti,
						listIvaMulti, 
						Integer.parseInt(new String("0")));
			}
		log.info(wsResult);
		
		if (!wsResult.isEmpty()){
			if(!wsResult.startsWith("-1")){
				if(Long.parseLong(wsResult)>0){
					
					TransaccionZonaPagos transaccion = new TransaccionZonaPagos();
            		transaccion.setSecTransaccionZonaPagos(null);
            		transaccion.setIdPago(recibo.getReciboConsignacion());
            		transaccion.setEstadoPago(new Long(IConstantes.ESTADO_PENDIENTE_INICIAR));
            		transaccion.setIdFormaPago(null);
            		transaccion.setValorPagado(recibo.getValorDetalle());
            		transaccion.setTicketId(null);
            		transaccion.setIdClave(recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
							(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
					            	&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
									IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
									IConstantes.CLAVE_SERVICIO:
										recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
												IConstantes.CLAVE_SERVICIO_DOLARES:
												IConstantes.CLAVE_SERVICIO_EURO
												);
            		transaccion.setIdCliente(recibo.getCliente().getCliente().toString());
            		transaccion.setFranquicia(null);
            		transaccion.setCodigoServicio(null);
            		transaccion.setCodigoBanco(null);
            		transaccion.setNombreBanco(null);
            		transaccion.setCodigoTransaccion(null);
            		transaccion.setCicloTransaccion(null);
            		transaccion.setCampo1(trmDolar);
            		transaccion.setCampo2(recibo.getTasaRepresentativa().getValor().toString()+" "+recibo.getTasaRepresentativa().getMoneda().getCodigo());
            		transaccion.setCampo3(recibo.getPeriodo()!=null?
							recibo.getPeriodo().getPeriodo()!=null?
									recibo.getPeriodo().getPeriodo().getPeriodo():"":"");
            		transaccion.setIdComercio(new Long(recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
							(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
	            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
									Integer.parseInt(IConstantes.ID_TIENDA_BANCOLOMBIA):
									Integer.parseInt(IConstantes.ID_TIENDA):
										recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
												Integer.parseInt(IConstantes.ID_TIENDA_DOLARES):
													Integer.parseInt(IConstantes.ID_TIENDA_EURO)));
            		transaccion.setDatFecha(new Date());
            		
            		ParametrizacionFac.getFacade().guardarRegistro("insertTransaccionZonaPagos", transaccion);
            		log.info("registro guardado: "+transaccion.getSecTransaccionZonaPagos());
            		log.info("corriendo el redirect");
            		
            		//RedirectAction winRedirectPagos = (RedirectAction)Executions.createComponents("pages/redirectPago.zul", null,null);
            		if(recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)){
            			
            			Execution execution = Executions.getCurrent();
            			if(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA)){
            				//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO_BANCOLOMBIA+wsResult);
            				execution.sendRedirect(IConstantes.RUTA_SERVICIO_BANCOLOMBIA+wsResult, "_blank");
            				//Executions.sendRedirect(IConstantes.RUTA_SERVICIO_BANCOLOMBIA+wsResult);
            			} else if (recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_OCCIDENTE) 
            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_OCCIDENTE)){
            				execution.sendRedirect(IConstantes.RUTA_SERVICIO+wsResult, "_blank");
            				//Executions.sendRedirect(IConstantes.RUTA_SERVICIO+wsResult);	
            				//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO+wsResult);
            			} else{
            				execution.sendRedirect(IConstantes.RUTA_SERVICIO+wsResult, "_blank");
            				//Executions.sendRedirect(IConstantes.RUTA_SERVICIO+wsResult);
            			}
            			execution.setVoided(true);
            			
            			
            		}else if (recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)){
            			//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO_DOLARES+wsResult);
            			Execution execution = Executions.getCurrent();
            			execution.sendRedirect(IConstantes.RUTA_SERVICIO_DOLARES+wsResult, "_blank");
            			//Executions.sendRedirect(IConstantes.RUTA_SERVICIO_DOLARES+wsResult );
            			execution.setVoided(true);
            		} else {
            			//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO_EURO+wsResult);
            			Execution execution = Executions.getCurrent();
            			execution.sendRedirect(IConstantes.RUTA_SERVICIO_EURO+wsResult, "_blank");
            			//Executions.sendRedirect(IConstantes.RUTA_SERVICIO_EURO+wsResult);
            			execution.setVoided(true);
            		}
            		
            	
       			  Clients.showNotification(IConstantes.CONFIRMACION_INICIO_PAGO, Clients.NOTIFICATION_TYPE_INFO, this, "middle_center", 4000);
					
				}else{ //else wsresult >0
					  Messagebox.show(
							   IConstantes.ERROR_PAGO_INICIO+wsResult,
							    "Error de Pagos en L�nea",
							    Messagebox.YES, Messagebox.ERROR);
					
				}
			}else{ // wsresult empieza -1
				  Messagebox.show(
						   IConstantes.ERROR_PAGO_INICIO+wsResult,
						    "Error de Pagos en L�nea",
						    Messagebox.YES, Messagebox.ERROR);

			}
			
			
		} else{// ws result nulo
			
			  Messagebox.show(
					   IConstantes.ERROR_PAGO_INICIO+wsResult,
					    "Error de Pagos en L�nea",
					    Messagebox.YES, Messagebox.ERROR);

		}
	} catch (NumberFormatException e) {
		e.printStackTrace();
	} catch (RemoteException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} catch (Exception e) {

		e.printStackTrace();
			}
		}
	
	} else if(listaDatos.size()>0){
		  Messagebox.show(
				   IConstantes.ERROR_PAGO_PENDIENTE,
				    "Error de Pagos en L�nea",
				    Messagebox.YES, Messagebox.ERROR);
		}
	} 
}


public void onValidatePersona(){
	try{
		log.info("M�todo [onValidatePersona]");
		Persona personaParametro = new Persona();	
		personaParametro.setIdentificacion(((Intbox)this.getFellow("TxtRecibirInscripcionEncuentroIdentificacion")).getValue().toString());
		log.info("identificaci�n par�metro: "+personaParametro.getIdentificacion());
		Persona persona = (Persona)ParametrizacionFac.getFacade().obtenerRegistro("selectPersonaBasic", personaParametro);
		log.info("hay persona para validar");
		if(persona!= null){
			log.info("hay persona para validar NO nula");
			if(persona.getNombreRazonSocial()!=null && !persona.getNombreRazonSocial().equalsIgnoreCase("")){
				log.info("hay persona para validar: "+persona.getIdentificacion()+" - "+persona.getNombreRazonSocial()+" "+persona.getPrimerApellido());
				if(persona.getPoblacionResidencia()!=null){ 
					if(persona.getPoblacionResidencia().getDepartamento()!=null){
						if(persona.getPoblacionResidencia().getDepartamento().getPais()!=null){
							
							String poblacionValue = String.valueOf(persona.getPoblacionResidencia().getDepartamento().getPais().getPais())+
							String.valueOf(persona.getPoblacionResidencia().getDepartamento().getDepartamento())+
							String.valueOf(persona.getPoblacionResidencia().getPoblacion());
							
							log.info("poblacion recuperada:" +poblacionValue);

							this.filterListPoblacion(persona.getPoblacionResidencia().getNombrePoblacion());
							((Combobox)this.getFellow("CbxRecibirInscripcionEncuentroUbicacion")).setSelectedIndex(0);
							
//							List<Comboitem> listaPoblacion = ((Combobox)this.getFellow("CbxRecibirInscripcionEncuentroUbicacion")).getItems();
//							Iterator<Comboitem> iteradorPoblacion = listaPoblacion.iterator();
//							while(iteradorPoblacion.hasNext()){
//								Comboitem itemComboUbicacion = iteradorPoblacion.next();
//								if(itemComboUbicacion.getValue().toString().equalsIgnoreCase(poblacionValue)){
//									((Combobox)this.getFellow("CbxRecibirInscripcionEncuentroUbicacion")).setSelectedItem(itemComboUbicacion);
//									break;
//								}
//							}
						}
					}
				}
				
				if(persona.getTipoIdentificacion()!=null){
					List<Listitem> listaTipoId =((Listbox)this.getFellow("LbxRecibirInscripcionEncuentroTipoIdentificacion")).getItems();
					Iterator<Listitem> iteradorTipoId = listaTipoId.iterator();
					while(iteradorTipoId.hasNext()){
						Listitem itemListaTipoId = iteradorTipoId.next();
						if(itemListaTipoId.getValue().toString().equalsIgnoreCase(persona.getTipoIdentificacion().getTipoIdentificacion())){
							((Listbox)this.getFellow("LbxRecibirInscripcionEncuentroTipoIdentificacion")).setSelectedItem(itemListaTipoId);
						}
					}
				}
				
				((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroNombres")).setValue(persona.getNombreRazonSocial());
				((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroPrimerApellido")).setValue(persona.getPrimerApellido());
				((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroSegundoApellido")).setValue(persona.getSegundoApellido());
				((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroTelefono")).setValue(persona.getTelefonoResidencia());
				((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroDireccionElectronica")).setValue(persona.getDireccionElectronica());
				((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroDireccionContacto")).setValue(persona.getDireccionResidencia());
				((Listbox)this.getFellow("LbxRecibirInscripcionEncuentroSexo")).setSelectedIndex(persona.getSexo().equalsIgnoreCase("F")?1:2);
				((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroEmpresa")).setValue(persona.getEmpresa());
				((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroCargo")).setValue(persona.getCargo());
				
			}
		}
	}catch(Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
	}
}

public Persona insertInfoEncuentro(){
	Persona persona = new Persona();
	try{
		this.onValidarGenero();
		this.onValidarTipoIdentificacion();
		this.onValidarTratamientoDatos();
		
		Cliente cliente = new Cliente();
		persona.setNombreRazonSocial(((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroNombres")).getValue());
		persona.setPrimerApellido(((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroPrimerApellido")).getValue());
		persona.setSegundoApellido(((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroSegundoApellido")).getValue());
		persona.setTelefonoResidencia(((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroTelefono")).getValue());
		persona.setDireccionElectronica(((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroDireccionElectronica")).getValue());
		persona.setDireccionResidencia(((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroDireccionContacto")).getValue());
		persona.setIdentificacion(((Intbox)this.getFellow("TxtRecibirInscripcionEncuentroIdentificacion")).getValue().toString());
		persona.setSexo(((Listbox)this.getFellow("LbxRecibirInscripcionEncuentroSexo")).getSelectedItem().getValue().toString());
		persona.setEmpresa(((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroEmpresa")).getValue());
		persona.setCargo(((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroCargo")).getValue());
		persona.setAceptarPersonales(((Checkbox)this.getFellow("chbxRecibirInscripcionRRHHAceptarPersonal")).isChecked()?1:0);
		persona.setAceptarInscripcionPsicoWeb(((Checkbox)this.getFellow("chbxRecibirInscripcionRRHHPsicoWeb")).isChecked()?"S":"N");
		persona.setPerteneceColegioColombianoPsicologos(((Checkbox)this.getFellow("TxtRecibirInscripcionEncuentroCheckColegio")).isChecked()?"S":"N");
		String[] listaPob= (((Combobox)this.getFellow("CbxRecibirInscripcionEncuentroUbicacion")).getSelectedItem().getValue().toString()).split("-");
		Pais pais = new Pais();
		Departamento depto = new Departamento();
		Poblacion poblacion = new Poblacion();
		pais.setPais(new Long(listaPob[0]));
		depto.setDepartamento(new Long(listaPob[1]));
		poblacion.setPoblacion(new Long(listaPob[2]));
		depto.setPais(pais);
		poblacion.setDepartamento(depto);
		persona.setPoblacionResidencia(poblacion);
		TipoIdentificacion tipoIdentificacion = new TipoIdentificacion();
		tipoIdentificacion.setTipoIdentificacion(((Listbox)this.getFellow("LbxRecibirInscripcionEncuentroTipoIdentificacion")).getSelectedItem().getValue().toString());
		persona.setTipoIdentificacion(tipoIdentificacion);
		cliente.setCliente(new Long(((Intbox)this.getFellow("TxtRecibirInscripcionEncuentroIdentificacion")).getValue().toString()));
		cliente.setEgresado("N");
		cliente.setEstudianteActivo("N");
		persona.setCliente(cliente);
		
		Persona personaEstudiante = ReciboConsignacionHelper.getHelper().getEstudianteAcademicoAspirante(((Intbox)this.getFellow("TxtRecibirInscripcionEncuentroIdentificacion")).getValue().toString());
		if(personaEstudiante!=null){
			if(personaEstudiante.getCliente()!=null){
				if(personaEstudiante.getCliente().getCliente()!=null){
					log.info("Existe Cliente estudiante: "+personaEstudiante.getCliente().getCliente());
					Cliente clienteEstudiante = personaEstudiante.getCliente();
					persona.getCliente().setEstudianteActivo(clienteEstudiante.getEstudianteActivo());
					persona.getCliente().setEgresado(clienteEstudiante.getEgresado());
				}
			}
		}
		
		Persona personaEgresado = ReciboConsignacionHelper.getHelper().getEgresadoAcademicoAspirante(((Intbox)this.getFellow("TxtRecibirInscripcionEncuentroIdentificacion")).getValue().toString());
			if(personaEgresado !=null){
				if(personaEgresado.getCliente()!= null){
					if(personaEgresado.getCliente().getCliente()!=null){
						log.info("Existe Cliente egresado: "+personaEgresado.getCliente().getCliente());
						Cliente clienteEgresado = personaEgresado.getCliente();
						persona.getCliente().setEstudianteActivo(clienteEgresado.getEstudianteActivo());
						persona.getCliente().setEgresado(clienteEgresado.getEgresado());
					}
				}
			}
			
		
		
	ParametrizacionFac.getFacade().ejecutarProcedimiento("insertViewEscolarisAlumnosEncuentro", persona);
	log.info("Resulado inserci�n: Cli:"+persona.getCliente().getCliente()+" id: "+persona.getIdentificacion());
	ParametrizacionFac.getFacade().ejecutarProcedimiento("insertViewEscolarisLiquidacionesEncuentro", persona);
	log.info("Resulado inserci�n: Cli:"+persona.getCliente().getCliente()+" id: "+persona.getIdentificacion());	
		
	}catch(Exception e){
		log.info(e.getMessage());
		e.printStackTrace();
	}
	
	return persona;
	
}

public String getCodigoEstudiante(){
	String codigoEstudiante = "";
	Listbox selectProgramas = (Listbox)this.getFellow("LbxRecibirInscripcionPrograma");
	Listitem item = selectProgramas.getSelectedItem();
	ProgramaAcademico programa = ReciboConsignacionHelper.getHelper().getDatosProgramaAcademico(item.getValue().toString());
	codigoEstudiante = programa.getFacultad().getIdFacultad().toString()+programa.getIdPrograma();
	codigoEstudiante= codigoEstudiante+ReciboConsignacionHelper.getHelper().getPeriodoEducacionContinuada();
	
	
	return codigoEstudiante;
	
}


public ZPagosLocator getZonaPagosLocator() {
	return zonaPagosLocator;
}

public void setZonaPagosLocator(ZPagosLocator zonaPagosLocator) {
	this.zonaPagosLocator = zonaPagosLocator;
}

public ZPagosSoapProxy getZonaPagosProxy() {
	return zonaPagosProxy;
}

public void setZonaPagosProxy(ZPagosSoapProxy zonaPagosProxy) {
	this.zonaPagosProxy = zonaPagosProxy;
}


public void verificarActivacionInscripcion(){
	try{
		String activacion = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectActivacionInscripcionEncuentro");
		if(!activacion.equalsIgnoreCase("S")){
			((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroNombres")).setDisabled(true);
			((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroPrimerApellido")).setDisabled(true);
			((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroSegundoApellido")).setDisabled(true);
			((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroTelefono")).setDisabled(true);
			((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroDireccionElectronica")).setDisabled(true);
			((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroDireccionContacto")).setDisabled(true);
			((Intbox)this.getFellow("TxtRecibirInscripcionEncuentroIdentificacion")).setDisabled(true);
			((Listbox)this.getFellow("LbxRecibirInscripcionEncuentroSexo")).setDisabled(true);
			((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroEmpresa")).setDisabled(true);
			((Textbox)this.getFellow("TxtRecibirInscripcionEncuentroCargo")).setDisabled(true);
			((Listbox)this.getFellow("LbxRecibirInscripcionEncuentroTipoIdentificacion")).setDisabled(true);
			((Combobox)this.getFellow("CbxRecibirInscripcionEncuentroUbicacion")).setDisabled(true);
			((Checkbox)this.getFellow("chbxRecibirInscripcionRRHHAceptarPersonal")).setDisabled(true);
			((Button)this.getFellow("idBtnRecibirInscripcionEncuentroInscribir")).setDisabled(true);
			((Checkbox)this.getFellow("chbxRecibirInscripcionRRHHPsicoWeb")).setDisabled(true);
		}
		
		
	}catch(Exception e){
		log.info(e.getMessage());
		e.printStackTrace();
	}
	
}

public void filterListPoblacion(String value){
	
	List<Poblacion> listaPoblacion =  ReciboConsignacionHelper.getHelper().getListadoPoblacion(value);
	if(listaPoblacion != null){
		if(listaPoblacion.size()>0){
			Combobox selectPoblacion = (Combobox)this.getFellow("CbxRecibirInscripcionEncuentroUbicacion");
			selectPoblacion.getChildren().clear();
			
			 
			for(Poblacion poblacion: listaPoblacion){
				Comboitem item = new Comboitem();
				item.setValue(poblacion.getDepartamento().getPais().getPais().toString()+"-"+poblacion.getDepartamento().getDepartamento().toString()+
						"-"+poblacion.getPoblacion());
				item.setLabel(poblacion.getNombrePoblacion());
				item.setDescription(poblacion.getNombrePoblacion()+","+poblacion.getDepartamento().getNombreDepartamento()+","+
				poblacion.getDepartamento().getPais().getNombrePais());
				
				selectPoblacion.appendChild(item);
			}

			log.info("items: "+selectPoblacion.getItemCount());
		}
	}
	
}

public void afterCompose(){	
	
	

	List<TipoIdentificacion> listaTipoId =  ReciboConsignacionHelper.getHelper().getListadoTipoIdentificacion();
	
		
	if(listaTipoId != null){
		if(listaTipoId.size()>0){
			Listbox selectTipoId = (Listbox)this.getFellow("LbxRecibirInscripcionEncuentroTipoIdentificacion");
			for(TipoIdentificacion tipoId : listaTipoId){
				selectTipoId.appendItem(tipoId.getNombreTipoIdentificacion(), tipoId.getTipoIdentificacion());
			}
		}
	}
	
	this.filterListPoblacion("");
	this.verificarActivacionInscripcion();
	
	
	//this.onRecibirParametros();
}

}

