package com.konrad.action;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.zkoss.io.Files;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.ext.AfterCompose;

import com.konrad.domain.Cliente;
import com.konrad.domain.Orden;
import com.konrad.domain.PeriodoFacturacion;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.facade.ReportFac;
import com.konrad.listener.ContextoAplicacion;
import com.konrad.util.IConstantes;
import com.konrad.util.SendEmail;
import com.konrad.window.ActionStandardBorder;

public class SendOrdenesHistoricoMasivoAction extends ActionStandardBorder implements AfterCompose  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 281161854540374298L;
	protected static Logger log = Logger.getLogger(SendOrdenesHistoricoMasivoAction.class);
	protected static final Logger lLogger = Logger.getLogger("net.sf.jasperreports");
	private File archivoXlsOrdenes;

	public SendOrdenesHistoricoMasivoAction() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void afterCompose() {
		// TODO Auto-generated method stub
		
	}
	
	
	public void setFileXls(Media media){
		File archivoXls = new File("");
		try {
			Files.copy(archivoXls, media.getStreamData());
			this.setArchivoXlsOrdenes(archivoXls);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void enviarOrdenes(List<ReciboConsignacion> recibos){
		log.info("[enviarOrdenes]");
		
		List<String> receptores = new ArrayList<String>();
		List<String> receptoresCopia = new ArrayList<String>();
		List<String> adjuntos = new ArrayList<String>();
		String emisor = new String("cartera@konradlorenz.edu.co");
		receptores.add("aorjuela@DELOITTE.com");
		receptores.add("ncamacho@deloitte.com");
		//receptores.add(new String("gabriel.cruz@konradlorenz.edu.co"));
		receptoresCopia.add(new String("gabriel.cruz@konradlorenz.edu.co"));
		receptoresCopia.add(new String("grace.bateman@konradlorenz.edu.co"));
		receptoresCopia.add(new String("sandra.perilla@konradlorenz.edu.co"));
		receptoresCopia.add(new String("jose.suspes@konradlorenz.edu.co"));
		String asunto = "Ordenes de matricula";
		String mensaje ="Buenas tardes;\n\n"+  
		"Estimado funcionario revisor Fiscal;\n\n Adjuntamos las ordenes de matr�cula que solicit� como soporte de la revisi�n que ha efectuado.";
		Integer cuenta = 0;
		Integer faltantes = 0;
		
		
		for(ReciboConsignacion rec : recibos){
			log.info("c:\\temp\\Revisoria\\201801\\ORDEN_"+String.valueOf(rec.getCliente().getCliente())+"_"+rec.getOrden().getPeriodo().getPeriodo()+".pdf");
			adjuntos.add("c:\\temp\\Revisoria\\201801\\ORDEN_"+String.valueOf(rec.getCliente().getCliente())+"_"+rec.getOrden().getPeriodo().getPeriodo()+".pdf");
			File f = new File(adjuntos.get(cuenta));
			if(!f.exists()){
				faltantes++;
			}
			cuenta++;
		}
		
		String servidor = new String("smtp.office365.com");
		String puerto = "587";
		SendEmail m = new SendEmail();
		String contrasena = "Cc123456";
		
		log.info(receptores.get(0));
		log.info(adjuntos.get(0));
		
		if(faltantes.intValue()<=0){
			log.info("archivo encontrado");
			Boolean mi_variable = m.envia( servidor, puerto, emisor, asunto, receptores, receptoresCopia, mensaje, adjuntos, contrasena,"true","true");
			if (mi_variable){
			log.info("salida exitosa");
			}
		} else {
			log.info("archivo no encontrado");
		}
	}
	
	public void generarReporte(String nombreReporte, ReciboConsignacion recibo, String bandera, String rutaArchivo){
		 JasperPrint jp = null;
		  ByteArrayOutputStream out = null;
		  lLogger.setLevel(Level.OFF);
		  
	        try {
	            //generate report pdf stream
	        	log.info("Ruta: "+ContextoAplicacion.getInstance().getRutaContexto()+
		                IConstantes.RUTA_REPORTES);
	        	
	        	// fijar par�metros del reporte	
	            final Map<String, Object> params = new HashMap<String, Object>();
	            if(nombreReporte.startsWith("ORDEN")){
	            	params.put("P_ORGANIZACION", recibo.getOrden().getOrganizacion());
	            	params.put("P_ORDEN", recibo.getOrden().getOrden());
	            	params.put("P_DOCUMENTO", recibo.getOrden().getDocumento());
	            	params.put("P_CLIENTE", recibo.getCliente().getCliente());
	            	params.put("CCP_PERIODO", recibo.getOrden().getPeriodo().getPeriodo());
	            
	            }else if (nombreReporte.startsWith("CUOTA")){
	            	
	            	params.put("P_INICIAL", recibo.getReciboConsignacion());
		            params.put("P_FINAL", recibo.getReciboConsignacion());
		            
	            } else{
	            	params.put("P_INICIAL", recibo.getReciboConsignacion());
		            params.put("P_FINAL", recibo.getReciboConsignacion());
	            }
	            //fijar par�metros para subreportes
	            params.put("ICEBERGRS_REPORT_DIR",new File(ContextoAplicacion.getInstance().getRutaContexto()+
		                IConstantes.RUTA_REPORTES_EJECUTABLES));
	            params.put("ICEBERGRS_IMAGE_DIR", new File(ContextoAplicacion.getInstance().getRutaContexto()+IConstantes.RUTA_IMAGENES));
	 
	        
	            // compilamos el jrxml con un jasper
	            log.info("cargar reporte");
	            JasperReport report = (JasperReport)JRLoader.loadObject(new File(ContextoAplicacion.getInstance().getRutaContexto()+
			                IConstantes.RUTA_REPORTES_EJECUTABLES+nombreReporte+".jasper"));
	            
//	             JasperReport report =  JasperCompileManager.compileReport(ContextoAplicacion.getInstance().getRutaContexto()+
//			                IConstantes.RUTA_REPORTES+nombreReporte+".jrxml");
	            log.info("Recuperar Conexion");
	            Connection conexionReporte =  ReportFac.getFacade().retornarConexion();
	            log.info("llenar reporte");	
	            // ejecuci�n del reporte con par�metros 
	    		jp = JasperFillManager.fillReport(report, params, conexionReporte);
	    		// si no hay stream entonces abrirlo
	    		if(out==null){
	    			out = new ByteArrayOutputStream();
	    		}
	    		// se prepara la exportaci�n del reporte
	    		JRPdfExporter exporter = new JRPdfExporter();
	    		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
	            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
	            // Se exporta el reporte
	            log.info("exportar reporte");	
	            exporter.exportReport();
	            // Se pone el reporte en el Amedia
	            log.info("poner reporte Amedia");
	            final AMedia amedia = 
	                new AMedia(nombreReporte+".pdf", "pdf", "application/pdf", out.toByteArray());
	                 
	            byte[] data = amedia.getByteData();

	            FileOutputStream outStream = new FileOutputStream(rutaArchivo+"ORDEN_"+String.valueOf(recibo.getCliente().getCliente())+"_"+
	            recibo.getOrden().getPeriodo().getPeriodo()+".pdf");
	            outStream.write(data);
	            outStream.flush();
	            outStream.close();
	            out.close();
	            
	        } catch (Exception e) {
	            throw new RuntimeException(e);
	        }
	}
	
	
	public List<ReciboConsignacion> setListadoOrdenes(){
		List<ReciboConsignacion> recibos = new ArrayList<ReciboConsignacion>();
		Workbook workbook = null;
		try {
			
			
			workbook = Workbook.getWorkbook(new File("c:\\temp\\Revisoria\\201801\\ordenesConvertido.xls"));
			
			for (int x=0; x<workbook.getNumberOfSheets(); x++){
			
			 	
			 Sheet sheet = workbook.getSheet(x);
			 Integer  numeroFilas =sheet.getRows();
			 Integer numeroColumnas = sheet.getColumns();
			 
	
			for(int i=0; i<numeroFilas; i++ ){
				if(i>0){
					ReciboConsignacion recibo = new ReciboConsignacion();
					Orden orden = new Orden();
					PeriodoFacturacion periodo = new PeriodoFacturacion();
					orden.setOrganizacion(IConstantes.ORGANIZACION_DEFECTO);
					
					Cliente cliente = new Cliente();
				for(int j=0; j<numeroColumnas; j++){
					Cell celda = sheet.getCell(j,i);
					switch (j){
						case 0: 
					    		log.info("documento en excel: "+celda.getContents());
					    		if(celda.getContents()!=null && !celda.getContents().equals(""))
					    		    orden.setDocumento(celda.getContents());
					    		break;
						case 1: 
						    	log.info("orden en excel: "+celda.getContents());
					    		if(celda.getContents()!=null && !celda.getContents().equals(""))
					    		    orden.setOrden(new Long(celda.getContents()));
					    		break;
						case 2:
						    	log.info("periodo en excel: "+celda.getContents());
						    	if(celda.getContents()!=null && !celda.getContents().equals(""))
						    	    periodo.setPeriodo(celda.getContents());
						    	break;
						case 12:
						    	log.info("cliente en excel:"+celda.getContents());
						    	if(celda.getContents()!=null && !celda.getContents().equals(""))
						    	    cliente.setCliente(Long.valueOf(celda.getContents()));
						    	break;
						}	
					}
					orden.setPeriodo(periodo);
					orden.setCliente(cliente);
					recibo.setCliente(cliente);
					recibo.setOrden(orden);
					if(periodo.getPeriodo()!=null &&!periodo.getPeriodo().equals("")){
						if(cliente.getCliente()!= null && cliente.getCliente()>0L){
							recibos.add(recibo);	
						}
					}
					
				}
				}
		
		} //for de hojas de c�lculo
			
			workbook.close();
			
			for(ReciboConsignacion re: recibos){
				String rutaArchivo= "c:\\temp\\Revisoria\\201801\\";
				this.generarReporte("ORDEN_WEB_HISTORICO", re, "true", rutaArchivo);
			}
						
		} catch (BiffException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			workbook.close();
		}
		
		return recibos;
	}

	public File getArchivoXlsOrdenes() {
		return archivoXlsOrdenes;
	}

	
	public void enviarOrdenesSinInteraccion(){
		List<ReciboConsignacion> listado = this.setListadoOrdenes();
		this.enviarOrdenes(listado);
	}
	
	
	public void setArchivoXlsOrdenes(File archivoXlsOrdenes) {
		this.archivoXlsOrdenes = archivoXlsOrdenes;
	}	
	
}
