package com.konrad.action;


import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import com.konrad.assembler.MensajesAssembler;
import com.konrad.domain.Cliente;
import com.konrad.domain.CuentaReciboConsignacion;
import com.konrad.domain.Mensaje;
import com.konrad.domain.PeriodoFacturacion;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.TasaRepresentativaMercado;
import com.konrad.domain.Usuario;
import com.konrad.domain.VencimientoPeriodo;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.util.IConstantes;
import com.konrad.util.Moneda;
import com.konrad.window.ActionStandard;

public class SetDateChequeDevueltoAction extends ActionStandard {
	
	private static final long serialVersionUID = -1840592650435657086L;

	protected static Logger log = Logger.getLogger(SetDateChequeDevueltoAction.class);
	private ReciboConsignacion reciboConsignacion;




	@SuppressWarnings("rawtypes")
	public void doModal(ReciboConsignacion reciboConsignacion){
		try{
		Mensaje mensaje = new Mensaje();
		String tipoMensaje = IConstantes.INFORMATION;
		mensaje.setMensaje(IConstantes.INFORMACION_FECHA_GENERACION);
		this.onSetMensaje(mensaje, tipoMensaje);
		
		mensaje = new Mensaje();
		mensaje.setMensaje(IConstantes.INFORMACION_HORARIO_BANCARIO);
		this.onSetMensaje(mensaje, tipoMensaje);
		
		ReciboConsignacion reciboAuxFecha = new ReciboConsignacion();
		VencimientoPeriodo periodoAux = new VencimientoPeriodo();
		periodoAux.setFechaVencimiento(new Date());
		reciboAuxFecha.setPeriodo(periodoAux);
		
		Date fechaGeneracion = (Date)ParametrizacionFac.getFacade().obtenerRegistro("getSiguienteDiaHabil", reciboAuxFecha); 
		((Datebox)this.getFellow("idDbxDateSetDateChequeDevuelto")).setValue(fechaGeneracion);
		
		this.setReciboConsignacion(reciboConsignacion);
		Label labelUsuario = (Label)this.getFellow("idLblUsuarioSetDateChequeDevuelto");
		labelUsuario.setValue(
			((Usuario)((HashMap)this.getDesktop().getSession().getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.USUARIO_SESSION)).getUsuario()
			+"-"+
			((Usuario)((HashMap)this.getDesktop().getSession().getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.USUARIO_SESSION)).getNombres()		
				);
		
		super.doModal();
		} catch(Exception e){
			e.printStackTrace();
			Messagebox.show(
				    e.getMessage(),
				    "Error de Pagos en L�nea",
				    Messagebox.YES, Messagebox.ERROR);
			e.printStackTrace();
			
		}
	}
	
	public void doOverlapped(){
		super.doOverlapped();
	}
	
	

	public void onSalir(){
		this.detach();
	}
	
	public void onSetMensaje(Mensaje mensaje, String tipoMensaje ){
		((Grid)this.getFellow("idGrdSetDateChequeDevueltoMensajes")).setVisible(true);
		MensajesAssembler mensajeAs = new MensajesAssembler();
		Rows rowsMensaje = (Rows)this.getFellow("idRowsSetDateChequeDevueltoMensajes");
		Row filaMensaje = mensajeAs.crearRowDesdeDto(mensaje, tipoMensaje);
		rowsMensaje.appendChild(filaMensaje);
}
	
	
	
	@SuppressWarnings("unchecked")
	public void onImprimirRecibo(){

		ReciboConsignacion recibo = this.getReciboConsignacion();
		Integer  resultado = Messagebox.show(
			    "�Confirma la generaci�n de este recibo de pago? (Esto puede tomar varios segundos) "+IConstantes.MENSAJE_PERMITIR_POPUPS,
			    "Confirmar Generaci�n de Recibo de Pago",
			    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
		if(resultado == Messagebox.YES){
		
			ReciboConsignacion nuevoRecibo = new ReciboConsignacion();
			//se fija el cliente
			Cliente cliente = new Cliente();
			cliente.setCliente(recibo.getCliente().getCliente());
			nuevoRecibo.setCliente(cliente);
			nuevoRecibo.setReciboConsignacion(recibo.getReciboConsignacion());
			// se fijan las fechas de vencimiento
			VencimientoPeriodo vencimientoPeriodo= new VencimientoPeriodo();
			PeriodoFacturacion periodoFacturacion = new PeriodoFacturacion();
			periodoFacturacion.setPeriodo(recibo.getPeriodo().getPeriodo().getPeriodo());
			vencimientoPeriodo.setPeriodo(periodoFacturacion);
			vencimientoPeriodo.setFechaVencimiento(recibo.getPeriodo().getFechaVencimiento());
			nuevoRecibo.setPeriodo(vencimientoPeriodo);
			// se fija la cuenta bancaria
			CuentaReciboConsignacion cuentaReciboConsignacion = new CuentaReciboConsignacion();
			cuentaReciboConsignacion.setTipoEntidad(recibo.getCuentaReciboConsignacion().getTipoEntidad());
			cuentaReciboConsignacion.setEntidad(recibo.getCuentaReciboConsignacion().getEntidad());
			cuentaReciboConsignacion.setOficinaEntidad(recibo.getCuentaReciboConsignacion().getOficinaEntidad());
			cuentaReciboConsignacion.setNumeroCuenta(recibo.getCuentaReciboConsignacion().getNumeroCuenta());
			nuevoRecibo.setCuentaReciboConsignacion(cuentaReciboConsignacion);
			
			// se fija fecha, genera comisi�n, numero de fila, observaciones
			nuevoRecibo.setFecha(recibo.getFecha());
			nuevoRecibo.setGeneraComision(recibo.getGeneraComision());
			nuevoRecibo.setNumeroFila(recibo.getNumeroFila());
			nuevoRecibo.setObservaciones(recibo.getObservaciones());
			// Tasa representativa original
			TasaRepresentativaMercado tasaRepresentativaMercado = new TasaRepresentativaMercado();
			Moneda moneda = new Moneda();
			moneda.setCodigo(recibo.getTasaRepresentativa().getMoneda().getCodigo());
			moneda.setDescripcion(recibo.getTasaRepresentativa().getMoneda().getDescripcion());
			tasaRepresentativaMercado.setMoneda(moneda);
			tasaRepresentativaMercado.setValor(recibo.getTasaRepresentativa().getValor());
			tasaRepresentativaMercado.setFecha(recibo.getTasaRepresentativa().getFecha());
			nuevoRecibo.setTasaRepresentativa(tasaRepresentativaMercado);
			
			// tasa representativa vencida
			TasaRepresentativaMercado tasaRepresentativaMercadoVencida = new TasaRepresentativaMercado();
			Moneda monedaVencida = new Moneda();
			monedaVencida.setCodigo(recibo.getTasaRepresentativaVencido().getMoneda().getCodigo());
			monedaVencida.setDescripcion(recibo.getTasaRepresentativaVencido().getMoneda().getDescripcion());
			tasaRepresentativaMercadoVencida.setMoneda(monedaVencida);
			tasaRepresentativaMercadoVencida.setValor(recibo.getTasaRepresentativaVencido().getValor());
			tasaRepresentativaMercadoVencida.setFecha(recibo.getTasaRepresentativaVencido().getFecha());
			nuevoRecibo.setTasaRepresentativaVencido(tasaRepresentativaMercadoVencida);
			
			// valores, identificador y banderas
			nuevoRecibo.setValorDetalle(recibo.getValorDetalle());
			nuevoRecibo.setValorTotal(recibo.getValorTotal());
			nuevoRecibo.setValorVencido(recibo.getValorVencido());
			nuevoRecibo.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
			nuevoRecibo.setBanderaFecha(IConstantes.GENERAL_NO);
			
			Datebox dateValue = (Datebox)this.getFellow("idDbxDateSetDateChequeDevuelto");
			
			// Calculamos vencimientos a la fecha y luego procedemos con la creaci�n del nuevo recibo
			try {
				ReciboConsignacion reciboAux = new ReciboConsignacion();
				VencimientoPeriodo periodoAux = new VencimientoPeriodo();
				Cliente clienteAux = new Cliente();
				clienteAux.setCliente(recibo.getCliente().getCliente());
				periodoAux.setFechaVencimiento(dateValue.getValue());
				reciboAux.setPeriodo(periodoAux);
				reciboAux.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
				reciboAux.setCliente(clienteAux);
				
				Date fechaGeneracion = (Date)ParametrizacionFac.getFacade().obtenerRegistro("getSiguienteDiaHabil", reciboAux);
				periodoAux.setFechaVencimiento(fechaGeneracion);
				reciboAux.setPeriodo(periodoAux);
				nuevoRecibo.setFechaPago(fechaGeneracion);
				ParametrizacionFac.getFacade().ejecutarProcedimiento("calcularVencimientosCliente",reciboAux);
				Double liquidacion = (Double)ParametrizacionFac.getFacade().obtenerRegistro("selectValorTotalChequeDevuelto", nuevoRecibo);
				nuevoRecibo.setValorVencido(liquidacion);
				
				List <ReciboConsignacion> recibosBusqueda = 
						(List<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("buscarReciboCancelacionChequeDevuelto", nuevoRecibo);
				
				if(recibosBusqueda!=null){
					if(recibosBusqueda.size()>0){
						nuevoRecibo.setReciboConsignacion(recibosBusqueda.get(0).getReciboConsignacion());
					}else{
						ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarReciboReliquidadoChequeDevuelto", nuevoRecibo);	
					}
				}else{
					ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarReciboReliquidadoChequeDevuelto", nuevoRecibo);	
				}
				
				// impresi�n del recibo de consignaci�n
				if(recibo.getReciboConsignacion()!= null && nuevoRecibo.getReciboConsignacion()!=null){
					if(recibo.getReciboConsignacion().intValue() != nuevoRecibo.getReciboConsignacion().intValue()){
						PrintReportAction reportAction = (PrintReportAction)Executions.createComponents("pages/printReport.zul", null, null);
						reportAction.doModal(IConstantes.REP_CUOTA_CREDITO, nuevoRecibo);
						this.detach();
					}else{
						Messagebox.show(
							    IConstantes.ERROR_GENERACION_RECIBO,
							    "Error de Pagos en L�nea",
							    Messagebox.YES, Messagebox.ERROR);
					  Rows rowsMensaje = (Rows)this.getFellow("idRowsSetDateChequeDevueltoMensajes"); 
					  rowsMensaje.getChildren().clear();
					  Mensaje mensaje = new Mensaje();
					  mensaje.setMensaje(IConstantes.ERROR_GENERACION_RECIBO);
					  String tipoMensaje = IConstantes.ERROR;
					  this.onSetMensaje(mensaje, tipoMensaje);
					}
				}else{
					Messagebox.show(
						    IConstantes.ERROR_GENERACION_RECIBO,
						    "Error de Pagos en L�nea",
						    Messagebox.YES, Messagebox.ERROR);
				  Rows rowsMensaje = (Rows)this.getFellow("idRowsSetDateChequeDevueltoMensajes"); 
				  rowsMensaje.getChildren().clear();
				  Mensaje mensaje = new Mensaje();
				  mensaje.setMensaje(IConstantes.ERROR_GENERACION_RECIBO);
				  String tipoMensaje = IConstantes.ERROR;
				  this.onSetMensaje(mensaje, tipoMensaje);
				}
			} catch (Exception e) {
				Messagebox.show(
					    e.getMessage(),
					    "Error de Pagos en L�nea",
					    Messagebox.YES, Messagebox.ERROR);
				e.printStackTrace();
			}
			
			
		}
		
	}
	


	public ReciboConsignacion getReciboConsignacion() {
		return reciboConsignacion;
	}

	public void setReciboConsignacion(ReciboConsignacion reciboConsignacion) {
		this.reciboConsignacion = reciboConsignacion;
	}	
	
}