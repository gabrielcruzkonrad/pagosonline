package com.konrad.action;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.rpc.holders.IntHolder;
import javax.xml.rpc.holders.StringHolder;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zk.ui.util.Clients;

import com.konrad.domain.AuditoriaPago;
import com.konrad.domain.Cliente;
import com.konrad.domain.Moneda;
import com.konrad.domain.PagosOnline;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.TransaccionZonaPagos;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.helper.ReciboConsignacionHelper;
import com.konrad.util.IConstantes;
import com.konrad.util.SendEmail;
import com.konrad.window.ActionStandardBorder;
import com.zonapagos.www.ws_verificar_pagos.holders.prod.ArrayOfPagos_v3Holder;
import com.zonapagos.www.ws_verificar_pagos.prod.ServiceSoapProxy;


public class RecibirParametrosAction  extends ActionStandardBorder implements AfterCompose {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected static Logger log = Logger.getLogger(RecibirParametrosAction.class);

	
	public void cargarZul(String menu){
		Component miPage = this.getParent();
		this.detach();
		Executions.createComponents(menu, miPage, null);
	}
	
@SuppressWarnings("unchecked")
public void onRecibirParametrosZonaPagos(){
	try{
		log.info("ejecutando el m�todo [onRecibirParametros] ");
		// se reciben los par�metros desde el GET
	TransaccionZonaPagos transaccion = new TransaccionZonaPagos();
	transaccion.setIdPago(new Long(
			Executions.getCurrent().getParameterMap().get("id_pago")!=null? 
			 !((String[])Executions.getCurrent().getParameterMap().get("id_pago"))[0].equals("") ?
					((String[])Executions.getCurrent().getParameterMap().get("id_pago"))[0].toString():"0":"0"
			));
	log.info("pasa id pago "+transaccion.getIdPago());
	transaccion.setEstadoPago(new Long(
			Executions.getCurrent().getParameterMap().get("estado_pago")!=null?
					 !((String[])Executions.getCurrent().getParameterMap().get("estado_pago"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("estado_pago"))[0].toString():"0":"0"
			));
	log.info("pasa estado pago "+transaccion.getEstadoPago());
	transaccion.setIdFormaPago(new Long(
			Executions.getCurrent().getParameterMap().get("id_forma_pago")!= null?
					!((String[])Executions.getCurrent().getParameterMap().get("id_forma_pago"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("id_forma_pago"))[0].toString():"0":"0"
			));
	
	log.info("pasa forma pago "+transaccion.getIdFormaPago());
	transaccion.setValorPagado(new Double(
			Executions.getCurrent().getParameterMap().get("valor_pagado")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("valor_pagado"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("valor_pagado"))[0].toString():"0.00":"0.00"
			));
	log.info("pasa valor pago "+transaccion.getValorPagado());
	transaccion.setTicketId(new Long(
			Executions.getCurrent().getParameterMap().get("ticketID")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("ticketID"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("ticketID"))[0].toString():"0":"0"
			));
	log.info("pasa tcket id "+transaccion.getTicketId());
	transaccion.setIdClave(
			Executions.getCurrent().getParameterMap().get("id_clave")!=null?
				 !((String[])Executions.getCurrent().getParameterMap().get("id_clave"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("id_clave"))[0].toString():"0":"0"
			);
	log.info("pasa id clave "+transaccion.getIdClave());
	transaccion.setIdCliente(
			Executions.getCurrent().getParameterMap().get("id_cliente")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("id_cliente"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("id_cliente"))[0].toString():"0":"0"
					);
	log.info("pasa id cliente "+transaccion.getIdCliente());
	
	transaccion.setFranquicia(
					Executions.getCurrent().getParameterMap().get("franquicia")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("franquicia"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("franquicia"))[0].toString():"0":"0"
						);
	
	transaccion.setCodigoServicio(new Long(
			Executions.getCurrent().getParameterMap().get("codigo_servicio")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("codigo_servicio"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("codigo_servicio"))[0].toString():"0":"0"
			));
	
	transaccion.setCodigoBanco(new Long(
				Executions.getCurrent().getParameterMap().get("codigo_banco")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("codigo_banco"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("codigo_banco"))[0].toString():"0":"0"
			));
	
	transaccion.setNombreBanco(
					Executions.getCurrent().getParameterMap().get("nombre_banco")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("nombre_banco"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("nombre_banco"))[0].toString():"0":"0"
			);
	
	transaccion.setCodigoTransaccion(new Long(
			Executions.getCurrent().getParameterMap().get("codigo_transaccion")!=null?
					 !((String[])Executions.getCurrent().getParameterMap().get("codigo_transaccion"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("codigo_transaccion"))[0].toString():"0":"0"
			));
	
	transaccion.setCicloTransaccion(new Long(
			Executions.getCurrent().getParameterMap().get("ciclo_transaccion")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("ciclo_transaccion"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("ciclo_transaccion"))[0].toString():"0":"0"
			));
	
	transaccion.setCampo1(
			Executions.getCurrent().getParameterMap().get("campo1")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("campo1"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("campo1"))[0].toString():"0":"0"
			);
	
	transaccion.setCampo2(
			Executions.getCurrent().getParameterMap().get("campo2")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("campo2"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("campo2"))[0].toString():"0":"0"
			);
	
	transaccion.setCampo3(
			Executions.getCurrent().getParameterMap().get("campo3")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("campo3"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("campo3"))[0].toString():"0":"0"
			);
	
	transaccion.setIdComercio(new Long(
			Executions.getCurrent().getParameterMap().get("idcomercio")!=null?
					!((String[])Executions.getCurrent().getParameterMap().get("idcomercio"))[0].equals("")?
					((String[])Executions.getCurrent().getParameterMap().get("idcomercio"))[0].toString():"0":"0"
			));
	
	transaccion.setDatFecha(new Date());
	
	log.info("ID PAGO PASADO POR GET: "+transaccion.getIdPago());
	log.info("ESTADO PAGO PASADO POR GET: "+transaccion.getEstadoPago());
	
	// Se consume el Web Service de verificaci�n de pagos
	if(transaccion!= null){
		if(transaccion.getIdPago()>0L){
			if(ReciboConsignacionHelper.getHelper().getServicioPruebasProduccion()){
				if(transaccion.getEstadoPago().intValue()== 1){
					try{
						Map<String,String> mapaServidor = new HashMap<String,String>();
						ParametrizacionFac.getFacade().ejecutarProcedimiento("seleccionarDatosServidorCorreo", mapaServidor);
						if(mapaServidor.get("SERVIDOR_SMTP")!=null &&
								mapaServidor.get("PUERTO_SMTP")!=null &&
								mapaServidor.get("REQUIERE_TLS_SMTP")!=null &&
								mapaServidor.get("REQUIERE_AUTENTICACION_SMTP")!=null &&
								mapaServidor.get("USUARIO_AUTENTICACION_SMTP")!=null &&
								mapaServidor.get("CLAVE_AUTENTICACION_SMTP")!=null &&
								mapaServidor.get("USUARIO_REMITENTE_SMTP")!=null ){
								
								ReciboConsignacion reciboConsignacionVerificacion = new ReciboConsignacion();
								Cliente clienteVerificacion = new Cliente();
								clienteVerificacion.setCliente(new Long(transaccion.getIdCliente()));
								reciboConsignacionVerificacion.setCliente(clienteVerificacion);
								String confirmacionEnvio = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectRastroProvisionCastigo", reciboConsignacionVerificacion);
								if(confirmacionEnvio==null){
									confirmacionEnvio = "N";
								}
								if(confirmacionEnvio.equals("S")){
										clienteVerificacion = (Cliente)ParametrizacionFac.getFacade().obtenerRegistro("selectClienteBasico", clienteVerificacion);
										
										String servidorCorreo = mapaServidor.get("SERVIDOR_SMTP");
										String puertoCorreo = mapaServidor.get("PUERTO_SMTP");
										String emisor = mapaServidor.get("USUARIO_REMITENTE_SMTP");
										String asunto = IConstantes.ASUNTO_CORREO_PAGO_PROVISION;
										String contrasena = mapaServidor.get("CLAVE_AUTENTICACION_SMTP");
										String mensaje = "El estudiante con c�digo: "+clienteVerificacion.getCliente()+" y nombres: "+
												clienteVerificacion.getPersona().getNombreRazonSocial()+" "+clienteVerificacion.getPersona().getPrimerApellido()+" "+clienteVerificacion.getPersona().getSegundoApellido()+
												" ha realizado un pago con el recibo de consignaci�n "+transaccion.getIdPago()+" y presenta saldos por cartera Provisionada/Castigada."+
												"\n Por favor gestione el proceso de recuperaci�n de cartera."+
												"\n Gracias;";
										List<String> receptores = new ArrayList<String>();
										List<String> receptoresCopia = new ArrayList<String>();
										List<String> adjuntos = new ArrayList<String>();
										String correoReporteProvision = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectCorreoReportePagoProvision");
										receptores.add(correoReporteProvision);
										SendEmail enviarCorreo = new SendEmail();
										enviarCorreo.envia(servidorCorreo, puertoCorreo, emisor, asunto, receptores, receptoresCopia, mensaje, adjuntos, contrasena, "true", "true");
								}
						} else{
							log.info("No se encontraron datos del servidor");
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					}
				
				log.info("ejecutando servicio de producci�n [onRecibirParametros]");
				ServiceSoapProxy servicioVerificacion = new ServiceSoapProxy();
				ArrayOfPagos_v3Holder res_pagos_v3 = new ArrayOfPagos_v3Holder();
				IntHolder int_error = new IntHolder();
				int int_id_tienda= new Integer(transaccion.getIdComercio().toString());
				StringHolder str_error = new StringHolder();
				String str_id_pago = new String(transaccion.getIdPago().toString());
				String str_id_clave = new String(transaccion.getIdComercio().toString().equals(IConstantes.ID_TIENDA)?
						IConstantes.CLAVE_SERVICIO:
					transaccion.getIdComercio().toString().equals(IConstantes.ID_TIENDA_BANCOLOMBIA)?
						IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
					transaccion.getIdComercio().toString().equals(IConstantes.ID_TIENDA_DOLARES)?
							IConstantes.CLAVE_SERVICIO_DOLARES:
							IConstantes.CLAVE_SERVICIO_EURO);
				Integer estadoArrayPago;
				int estadoPago = servicioVerificacion.verificar_pago_v3(str_id_pago, int_id_tienda, str_id_clave, res_pagos_v3, int_error, str_error); 
				if (estadoPago>0){ //Significa que encontr� un pago
					estadoArrayPago = (res_pagos_v3.value[0]!=null?
										(((Integer)res_pagos_v3.value[0].getInt_estado_pago())!=null?
										res_pagos_v3.value[0].getInt_estado_pago()
										:0)
										:0);
					if (res_pagos_v3.value[0]!=null){
						// fijar id forma pago 
						transaccion.setIdFormaPago((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()!=null?
								((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()).longValue():new Long(0L));
						// fijar valor pagado
						transaccion.setValorPagado((Double)res_pagos_v3.value[0].getDbl_valor_pagado()!=null?
								((Double)res_pagos_v3.value[0].getDbl_valor_pagado()):new Double(0.00));
						// fijar ticketid
						transaccion.setTicketId(res_pagos_v3.value[0].getStr_ticketID()!= null?
								!res_pagos_v3.value[0].getStr_ticketID().equals("")?
								new Long(res_pagos_v3.value[0].getStr_ticketID()):new Long(0L):new Long(0L));
						// fijar id Clave
						transaccion.setIdClave(res_pagos_v3.value[0].getStr_id_clave());
						//fijar id Cliente
						transaccion.setIdCliente(res_pagos_v3.value[0].getStr_id_cliente());
						// fijar franquicia
						transaccion.setFranquicia(res_pagos_v3.value[0].getStr_franquicia()!= null?!res_pagos_v3.value[0].getStr_franquicia().equals("")?
							res_pagos_v3.value[0].getStr_franquicia():new String("0"):new String("0"));
						// fijar codigo de aprobaci�n
						transaccion.setCodigoAprobacion((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()!=null?
								((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()).longValue():new Long(0L));
						// fijar codigo Servicio
						transaccion.setCodigoServicio((Integer)res_pagos_v3.value[0].getInt_codigo_servico()!=null?
								((Integer)res_pagos_v3.value[0].getInt_codigo_servico()).longValue():new Long(0L));
						// fijar codigo Banco
						transaccion.setCodigoBanco((Integer)res_pagos_v3.value[0].getInt_codigo_banco()!=null?
								((Integer)res_pagos_v3.value[0].getInt_codigo_banco()).longValue():new Long(0L));
						// fijar Nombre banco
						transaccion.setNombreBanco(res_pagos_v3.value[0].getStr_nombre_banco()!=null?!res_pagos_v3.value[0].getStr_nombre_banco().equals("")?
								res_pagos_v3.value[0].getStr_nombre_banco():new String("0"):new String("0"));
						// fijar codigo transaccion
						transaccion.setCodigoTransaccion(res_pagos_v3.value[0].getStr_codigo_transaccion()!=null?
								!res_pagos_v3.value[0].getStr_codigo_transaccion().equals("")?
								new Long(res_pagos_v3.value[0].getStr_codigo_transaccion()):new Long(0L):new Long(0L));
						// fijar ciclo transaccion
						transaccion.setCicloTransaccion((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()!=null?
								((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()).longValue():new Long(0L));
						// fijar campo1
						transaccion.setCampo1(res_pagos_v3.value[0].getStr_campo1());
						// fijar campo2
						transaccion.setCampo2(res_pagos_v3.value[0].getStr_campo2());
						// fijar campo3
						transaccion.setCampo3(res_pagos_v3.value[0].getStr_campo3());
						//fijar datFecha
						transaccion.setDatFecha(new Date());
						}
					
					if(estadoArrayPago.intValue()>0 && (!estadoArrayPago.toString().equals(IConstantes.ESTADO_PENDIENTE_INICIAR))  && int_error.value == 0){
						Map<String,Object> transaccionTemp = new HashMap<String, Object>();
						transaccionTemp.put("ESTADO_PENDIENTE_FINALIZAR",IConstantes.ESTADO_PENDIENTE_FINALIZAR);
						transaccionTemp.put("ESTADO_PENDIENTE_INICIAR",IConstantes.ESTADO_PENDIENTE_INICIAR);
						transaccionTemp.put("ESTADO_PENDIENTE_ESTUDIO",IConstantes.ESTADO_PENDIENTE_ESTUDIO);
						transaccionTemp.put("ID_PAGO",transaccion.getIdPago());
						
						List<TransaccionZonaPagos> listaTransacciones = new ArrayList <TransaccionZonaPagos>();
						listaTransacciones = ((List<TransaccionZonaPagos>)ParametrizacionFac.getFacade().obtenerListado("selectTransaccionZonaPagosPendientes",transaccionTemp));
						for(TransaccionZonaPagos objeto: listaTransacciones){
							objeto.setIdPago(transaccion.getIdPago());
							objeto.setEstadoPago(transaccion.getEstadoPago());
							objeto.setIdFormaPago(transaccion.getIdFormaPago());
							objeto.setValorPagado(transaccion.getValorPagado());
							objeto.setTicketId(transaccion.getTicketId());
							objeto.setIdClave(transaccion.getIdClave());
							objeto.setIdCliente(transaccion.getIdCliente());
							objeto.setFranquicia(transaccion.getFranquicia());
							objeto.setCodigoServicio(transaccion.getCodigoServicio());
							objeto.setCodigoBanco(transaccion.getCodigoBanco());
							objeto.setNombreBanco(transaccion.getNombreBanco());
							objeto.setCodigoTransaccion(transaccion.getCodigoTransaccion());
							objeto.setCicloTransaccion(transaccion.getCicloTransaccion());
							objeto.setCampo1(transaccion.getCampo1());
							objeto.setCampo2(transaccion.getCampo2());
							objeto.setCampo3(transaccion.getCampo3());
							objeto.setIdComercio(transaccion.getIdComercio());
							
							if (estadoArrayPago.longValue() == objeto.getEstadoPago()) {
								ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
							} else{
								objeto.setEstadoPago(estadoArrayPago.longValue());
								ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
							}
							
						}
					}
				}
			}else{
				log.info(" ejecutando servicio de pruebas [onRecibirParametros]");
				com.zonapagos.www.ws_verificar_pagos.test.ServiceSoapProxy servicioVerificacion = new com.zonapagos.www.ws_verificar_pagos.test.ServiceSoapProxy();
				com.zonapagos.www.ws_verificar_pagos.holders.test.ArrayOfPagos_v3Holder res_pagos_v3 = new com.zonapagos.www.ws_verificar_pagos.holders.test.ArrayOfPagos_v3Holder();
				IntHolder int_error = new IntHolder();
				int int_id_tienda= new Integer(transaccion.getIdComercio().toString());
				StringHolder str_error = new StringHolder();
				String str_id_pago = new String(transaccion.getIdPago().toString());
				String str_id_clave = new String(IConstantes.CLAVE_SERVICIO_PRUEBAS);
				Integer estadoArrayPago;
				int estadoPago = servicioVerificacion.verificar_pago_v3(str_id_pago, int_id_tienda, str_id_clave, res_pagos_v3, int_error, str_error); 
				if (estadoPago>0){ //Significa que encontr� un pago
					
						estadoArrayPago = (res_pagos_v3.value[0]!=null?
										(((Integer)res_pagos_v3.value[0].getInt_estado_pago())!=null?
										res_pagos_v3.value[0].getInt_estado_pago()
										:0)
										:0);
					
						if (res_pagos_v3.value[0]!=null){
						
							//fijar id forma pago 
							transaccion.setIdFormaPago((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()!=null?
								((Integer)res_pagos_v3.value[0].getInt_id_forma_pago()).longValue():new Long(0L));
							//fijar valor pagado
							transaccion.setValorPagado((Double)res_pagos_v3.value[0].getDbl_valor_pagado()!=null?
								((Double)res_pagos_v3.value[0].getDbl_valor_pagado()):new Double(0.00));
							// fijar ticketid
							transaccion.setTicketId(res_pagos_v3.value[0].getStr_ticketID()!= null?
								!res_pagos_v3.value[0].getStr_ticketID().equals("")?
								new Long(res_pagos_v3.value[0].getStr_ticketID()):new Long(0L):new Long(0L));
							// fijar id Clave
							transaccion.setIdClave(res_pagos_v3.value[0].getStr_id_clave());
							//	fijar id Cliente
							transaccion.setIdCliente(res_pagos_v3.value[0].getStr_id_cliente());
							// fijar franquicia
							transaccion.setFranquicia(res_pagos_v3.value[0].getStr_franquicia()!= null?!res_pagos_v3.value[0].getStr_franquicia().equals("")?
									res_pagos_v3.value[0].getStr_franquicia():new String("0"):new String("0"));
							// fijar codigo de aprobaci�n
							transaccion.setCodigoAprobacion((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()!=null?
								((Integer)res_pagos_v3.value[0].getInt_cod_aprobacion()).longValue():new Long(0L));
							// fijar codigo Servicio
							transaccion.setCodigoServicio((Integer)res_pagos_v3.value[0].getInt_codigo_servico()!=null?
								((Integer)res_pagos_v3.value[0].getInt_codigo_servico()).longValue():new Long(0L));
							// fijar codigo Banco
							transaccion.setCodigoBanco((Integer)res_pagos_v3.value[0].getInt_codigo_banco()!=null?
								((Integer)res_pagos_v3.value[0].getInt_codigo_banco()).longValue():new Long(0L));
							// fijar Nombre banco
							transaccion.setNombreBanco(res_pagos_v3.value[0].getStr_nombre_banco()!=null?!res_pagos_v3.value[0].getStr_nombre_banco().equals("")?
								res_pagos_v3.value[0].getStr_nombre_banco():new String("0"):new String("0"));
							// fijar codigo transaccion
							transaccion.setCodigoTransaccion(res_pagos_v3.value[0].getStr_codigo_transaccion()!=null?
								!res_pagos_v3.value[0].getStr_codigo_transaccion().equals("")?
								new Long(res_pagos_v3.value[0].getStr_codigo_transaccion()):new Long(0L):new Long(0L));
							// fijar ciclo transaccion
							transaccion.setCicloTransaccion((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()!=null?
								((Integer)res_pagos_v3.value[0].getInt_ciclo_transaccion()).longValue():new Long(0L));
							// fijar campo1
							transaccion.setCampo1(res_pagos_v3.value[0].getStr_campo1());
							// fijar campo2
							transaccion.setCampo2(res_pagos_v3.value[0].getStr_campo2());
							// fijar campo3
							transaccion.setCampo3(res_pagos_v3.value[0].getStr_campo3());
							// fijar datFecha
							transaccion.setDatFecha(new Date());
						
							}
					
					if(estadoArrayPago.intValue()>0 && (!estadoArrayPago.toString().equals(IConstantes.ESTADO_PENDIENTE_INICIAR)) && int_error.value == 0){
						Map<String,Object> transaccionTemp = new HashMap<String, Object>();
						transaccionTemp.put("ESTADO_PENDIENTE_FINALIZAR",IConstantes.ESTADO_PENDIENTE_FINALIZAR);
						transaccionTemp.put("ESTADO_PENDIENTE_INICIAR",IConstantes.ESTADO_PENDIENTE_INICIAR);
						transaccionTemp.put("ESTADO_PENDIENTE_ESTUDIO",IConstantes.ESTADO_PENDIENTE_ESTUDIO);
						transaccionTemp.put("ID_PAGO",transaccion.getIdPago());
						
						List<TransaccionZonaPagos> listaTransacciones = new ArrayList <TransaccionZonaPagos>();
						listaTransacciones = ((List<TransaccionZonaPagos>)ParametrizacionFac.getFacade().obtenerListado("selectTransaccionZonaPagosPendientes",transaccionTemp));
						for(TransaccionZonaPagos objeto: listaTransacciones){
							objeto.setIdPago(transaccion.getIdPago());
							objeto.setEstadoPago(transaccion.getEstadoPago());
							objeto.setIdFormaPago(transaccion.getIdFormaPago());
							objeto.setValorPagado(transaccion.getValorPagado());
							objeto.setTicketId(transaccion.getTicketId());
							objeto.setIdClave(transaccion.getIdClave());
							objeto.setIdCliente(transaccion.getIdCliente());
							objeto.setFranquicia(transaccion.getFranquicia());
							objeto.setCodigoServicio(transaccion.getCodigoServicio());
							objeto.setCodigoBanco(transaccion.getCodigoBanco());
							objeto.setNombreBanco(transaccion.getNombreBanco());
							objeto.setCodigoTransaccion(transaccion.getCodigoTransaccion());
							objeto.setCicloTransaccion(transaccion.getCicloTransaccion());
							objeto.setCampo1(transaccion.getCampo1());
							objeto.setCampo2(transaccion.getCampo2());
							objeto.setCampo3(transaccion.getCampo3());
							objeto.setIdComercio(transaccion.getIdComercio());
							
							if (estadoArrayPago.longValue() == objeto.getEstadoPago()) {
								ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
							} else{
								objeto.setEstadoPago(estadoArrayPago.longValue());
								ParametrizacionFac.getFacade().actualizarRegistro("updateTransaccionZonaPagos", objeto);
							}
							
						}
					}
				}
			}
		}
	}
	
	}catch(Exception e){
		e.printStackTrace();
	}
}


@SuppressWarnings("unchecked")
public void onRecibirParametros(){
	try{
		log.info("ejecutando el metodo [onRecibirParametros] ");
		// se reciben los par�metros desde el GET/POST
		
		  HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
	      if(request!=null){
	    	  log.info("Obtener valores por http Request native --- ");
	    	  log.info(request.getParameter("merchant_id"));
	    	  log.info(request.getParameter("reference_sale"));
	    	  log.info(request.getParameter("value"));
	    	  log.info(request.getParameter("currency"));
	    	  log.info(request.getParameter("state_pol"));
	    	  log.info(request.getParameter("sign"));
	    	  log.info(request.getParameter("extra2"));
	    	  log.info(request.getParameter("payment_method"));
	    	  log.info(request.getParameter("payment_method_type"));
	    	  log.info(request.getParameter("pse_bank"));
	    	  log.info(request.getParameter("commision_pol"));
	    	  log.info(request.getParameter("commision_pol_currency"));
	      }

	      log.info("ver si llegan por el Execution en parametros principales");
	      log.info(Executions.getCurrent().getParameter("merchant_id"));
	      log.info(Executions.getCurrent().getParameter("reference_sale"));
	      log.info(Executions.getCurrent().getParameter("value"));
	      log.info(Executions.getCurrent().getParameter("currency"));
	      log.info(Executions.getCurrent().getParameter("state_pol"));
	      log.info(Executions.getCurrent().getParameter("sign"));
	      log.info(Executions.getCurrent().getParameter("extra2"));
	      log.info(Executions.getCurrent().getParameter("payment_method"));
	      log.info(Executions.getCurrent().getParameter("payment_method_type"));
	      log.info(Executions.getCurrent().getParameter("pse_bank"));
	      log.info(Executions.getCurrent().getParameter("commision_pol"));
    	  log.info(Executions.getCurrent().getParameter("commision_pol_currency"));
	      
		Map<String, Object>  transaccion = new HashMap<String,Object>();
			transaccion.put("merchant_id",new Long(
					Executions.getCurrent().getParameterMap().get("merchant_id")!=null? 
							!((String[])Executions.getCurrent().getParameterMap().get("merchant_id"))[0].equals("") ?
									((String[])Executions.getCurrent().getParameterMap().get("merchant_id"))[0].toString():"0":"0"
					));
	
			transaccion.put("reference_sale",new String(
					Executions.getCurrent().getParameterMap().get("reference_sale")!=null? 
							!((String[])Executions.getCurrent().getParameterMap().get("reference_sale"))[0].equals("") ?
									((String[])Executions.getCurrent().getParameterMap().get("reference_sale"))[0].toString():"0":"0"
					));

			transaccion.put("value",new String(
					Executions.getCurrent().getParameterMap().get("value")!=null? 
							!((String[])Executions.getCurrent().getParameterMap().get("value"))[0].equals("") ?
									((String[])Executions.getCurrent().getParameterMap().get("value"))[0].toString():"0":"0"
					));

			transaccion.put("currency",new String(
					Executions.getCurrent().getParameterMap().get("currency")!=null? 
							!((String[])Executions.getCurrent().getParameterMap().get("currency"))[0].equals("") ?
									((String[])Executions.getCurrent().getParameterMap().get("currency"))[0].toString():"0":"0"
					));

			transaccion.put("state_pol",new String(
					Executions.getCurrent().getParameterMap().get("state_pol")!=null? 
							!((String[])Executions.getCurrent().getParameterMap().get("state_pol"))[0].equals("") ?
									((String[])Executions.getCurrent().getParameterMap().get("state_pol"))[0].toString():"0":"0"
					));
			
			
			transaccion.put("sign",new String(
					Executions.getCurrent().getParameterMap().get("sign")!=null? 
							!((String[])Executions.getCurrent().getParameterMap().get("sign"))[0].equals("") ?
									((String[])Executions.getCurrent().getParameterMap().get("sign"))[0].toString():"0":"0"
					));

			
			transaccion.put("extra2",new String(
					Executions.getCurrent().getParameterMap().get("extra2")!=null? 
							!((String[])Executions.getCurrent().getParameterMap().get("extra2"))[0].equals("") ?
									((String[])Executions.getCurrent().getParameterMap().get("extra2"))[0].toString():"0":"0"
					));

			

			transaccion.put("payment_method",new String(
					Executions.getCurrent().getParameterMap().get("payment_method")!=null? 
							!((String[])Executions.getCurrent().getParameterMap().get("payment_method"))[0].equals("") ?
									((String[])Executions.getCurrent().getParameterMap().get("payment_method"))[0].toString():"0":"0"
					));
			

			transaccion.put("payment_method_type",new String(
					Executions.getCurrent().getParameterMap().get("payment_method_type")!=null? 
							!((String[])Executions.getCurrent().getParameterMap().get("payment_method_type"))[0].equals("") ?
									((String[])Executions.getCurrent().getParameterMap().get("payment_method_type"))[0].toString():"0":"0"
					));
			
			transaccion.put("pse_bank",new String(
					Executions.getCurrent().getParameterMap().get("pse_bank")!=null? 
							!((String[])Executions.getCurrent().getParameterMap().get("pse_bank"))[0].equals("") ?
									((String[])Executions.getCurrent().getParameterMap().get("pse_bank"))[0].toString():"0":"0"
					));
			
			transaccion.put("commision_pol",new String(
					Executions.getCurrent().getParameterMap().get("commision_pol")!=null? 
							!((String[])Executions.getCurrent().getParameterMap().get("commision_pol"))[0].equals("") ?
									((String[])Executions.getCurrent().getParameterMap().get("commision_pol"))[0].toString():"0":"0"
					));
			
			transaccion.put("commision_pol_currency",new String(
					Executions.getCurrent().getParameterMap().get("commision_pol_currency")!=null? 
							!((String[])Executions.getCurrent().getParameterMap().get("commision_pol_currency"))[0].equals("") ?
									((String[])Executions.getCurrent().getParameterMap().get("commision_pol_currency"))[0].toString():"0":"0"
					));
			
			if(transaccion.get("state_pol") == null || 
					transaccion.get("state_pol").toString().equals("0")){
				
				  transaccion.put("merchant_id", Executions.getCurrent().getParameter("merchant_id"));
				  transaccion.put("reference_sale", Executions.getCurrent().getParameter("reference_sale"));
				  transaccion.put("value", Executions.getCurrent().getParameter("value"));
				  transaccion.put("currency", Executions.getCurrent().getParameter("currency"));
				  transaccion.put("state_pol", Executions.getCurrent().getParameter("state_pol"));
				  transaccion.put("sign", Executions.getCurrent().getParameter("sign"));
				  transaccion.put("extra2", Executions.getCurrent().getParameter("extra2"));
				  transaccion.put("payment_method", Executions.getCurrent().getParameter("payment_method"));
				  transaccion.put("payment_method_type", Executions.getCurrent().getParameter("payment_method_type"));
				  transaccion.put("pse_bank", Executions.getCurrent().getParameter("pse_bank"));
				  transaccion.put("commision_pol", Executions.getCurrent().getParameter("commision_pol"));
				  transaccion.put("commision_pol_currency", Executions.getCurrent().getParameter("commision_pol_currency"));
				  
				
			}
			
			if(transaccion.get("state_pol") == null || 
					transaccion.get("state_pol").toString().equals("0")){
				
				transaccion.put("merchant_id", request.getParameter("merchant_id"));
				transaccion.put("reference_pol",request.getParameter("reference_sale"));
				transaccion.put("value", request.getParameter("value"));
				transaccion.put("currency",request.getParameter("currency"));
				transaccion.put("state_pol",request.getParameter("state_pol"));
				transaccion.put("sign",request.getParameter("sign"));
				transaccion.put("extra2",request.getParameter("extra2"));
				transaccion.put("payment_method",request.getParameter("payment_method"));
				transaccion.put("payment_method_type",request.getParameter("payment_method_type"));
				transaccion.put("pse_bank",request.getParameter("pse_bank"));
				transaccion.put("commision_pol",request.getParameter("commision_pol"));
				transaccion.put("commision_pol_currency",request.getParameter("commision_pol_currency"));
				
			}
			
			log.info("ID REF VENTA PASADO POR GET: "+transaccion.get("reference_sale"));
			System.out.println("ID ESTADO PASADO POR GET: "+transaccion.get("state_pol"));
			
	
			if(!transaccion.get("reference_sale").equals("0")){
				AuditoriaPago auditoria = new AuditoriaPago();
				AuditoriaPago auditoriaVerifica = new AuditoriaPago();
				ReciboConsignacion reciboParametro = new ReciboConsignacion();
				reciboParametro.setReciboConsignacion(Long.valueOf(String.valueOf(transaccion.get("extra2"))));
				/* verificacion de auditoria */
				auditoriaVerifica.setxId(String.valueOf(transaccion.get("reference_sale")));
				auditoriaVerifica.setCodTienda(String.valueOf(transaccion.get("merchant_id")));
				auditoriaVerifica.setNumPedido(String.valueOf(transaccion.get("reference_sale")));
				auditoriaVerifica.setReciboConsignacion(reciboParametro);
				/* auditoria de confirmacion*/
				auditoria.setxId(String.valueOf(transaccion.get("reference_sale")));
				auditoria.setCodTienda(String.valueOf(transaccion.get("merchant_id")));
				auditoria.setNumPedido(String.valueOf(transaccion.get("reference_sale")));
				auditoria.setReciboConsignacion(reciboParametro);
				
				try{
					boolean intentoDisponible = false;
					List<AuditoriaPago> auditoriaResponseVerification = (List<AuditoriaPago>)ParametrizacionFac.getFacade().obtenerListado("selectAuditoriaPagoReferencia", auditoriaVerifica);
					if(auditoriaResponseVerification!=null){
						if(auditoriaResponseVerification.size()>0){
							for(AuditoriaPago auditoriaPago : auditoriaResponseVerification){
								if(auditoriaPago!=null){
									if(auditoriaPago.getTipoAccion()==null){
										intentoDisponible = true;
									}
									
									if(!intentoDisponible){
										auditoriaVerifica.setxId(auditoriaPago.getxId());
										auditoriaVerifica.setCodTienda(auditoriaPago.getCodTienda());
										auditoriaVerifica.setCodAlumno(auditoriaPago.getCodAlumno());
										auditoriaVerifica.setNombreAlumno(auditoriaPago.getNombreAlumno());
										auditoriaVerifica.setFechaOperacion(auditoriaVerifica.getFechaOperacion());
										auditoriaVerifica.setCuota(auditoriaVerifica.getCuota());
										auditoriaVerifica.setNumDocumento(auditoriaPago.getNumDocumento());
										auditoriaVerifica.setMoneda(auditoriaPago.getMoneda());
										auditoriaVerifica.setSaldo(auditoriaPago.getSaldo());
										auditoriaVerifica.setMora(auditoriaPago.getMora());
										auditoriaVerifica.setImporteLocal(auditoriaPago.getImporteLocal());
										auditoriaVerifica.setImporteExtr(auditoriaPago.getImporteExtr());
										auditoriaVerifica.setNumPedido(auditoriaPago.getNumPedido());
										auditoriaVerifica.setGlosa(auditoriaPago.getGlosa());
										auditoriaVerifica.setFechaCreacion(auditoriaPago.getFechaCreacion());
										auditoriaVerifica.setUsuarioCreacion(auditoriaPago.getUsuarioCreacion());
										auditoriaVerifica.setFechaModificacion(auditoriaPago.getFechaModificacion());
										auditoriaVerifica.setUsuarioModificacion(auditoriaPago.getUsuarioModificacion());
										auditoriaVerifica.setReciboConsignacion(auditoriaPago.getReciboConsignacion());
										auditoriaVerifica.setEticket(auditoriaPago.getEticket());
										auditoriaVerifica.setTipoAccion(null);
										auditoriaVerifica.setCodLineaNegocio(null);
									}
								}
							}
						}
					}
					if(!intentoDisponible){
						ParametrizacionFac.getFacade().ejecutarProcedimiento("insertAuditoriaPago", auditoriaVerifica);
					}
					
				}catch(Exception e){
					e.printStackTrace();
					log.error(e.getMessage());
					
				}
				
				
				try{
					AuditoriaPago auditoriaResponse = 
							(AuditoriaPago)ParametrizacionFac.getFacade().obtenerRegistro("selectAuditoriaPagoPedido", auditoria);
		
					if(auditoriaResponse.getSecAuditoriaPago()>0L){
		
						// Si encuentra registro de auditoría entonces podemos consultar por e-ticket
						if(auditoriaResponse.getEticket()!=null && !auditoriaResponse.getEticket().equals("")){
							
							Map<String,String> mapaParametros = new HashMap<String,String>();
					    	mapaParametros.put("parametro", IConstantes.API_KEY_PAYULATAM);
					    	mapaParametros.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
					    	String apiKey = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaParametros);
							
							String firma_cadena= apiKey+"~"+(String.valueOf(transaccion.get("merchant_id")))+
							"~"+(String.valueOf(transaccion.get("reference_sale")))+"~"+
								( String.valueOf(new BigDecimal(String.valueOf(transaccion.get("value"))).setScale(1, RoundingMode.CEILING)))
							+"~"+(String.valueOf(transaccion.get("currency")))+"~"
							+(String.valueOf(transaccion.get("state_pol")));
							
							MessageDigest alg = MessageDigest.getInstance("MD5");
							alg.reset();
							alg.update(firma_cadena.getBytes());
							
							byte[] digest = alg.digest();
							StringBuffer hashedpasswd = new StringBuffer();
							String hx;
							
							for (int i=0;i<digest.length;i++){
								hx = Integer.toHexString(0xFF & digest[i]);
								if(hx.length() == 1){
									hx = "0" + hx;
								}
									hashedpasswd.append(hx);
							}
							
							String firmacreada = hashedpasswd.toString();
							System.out.println("firma obtenida "+transaccion.get("sign"));
							System.out.println("firma para registrar pago "+firmacreada);
							
							if((String.valueOf(transaccion.get("sign"))).toUpperCase().equals( firmacreada.toUpperCase()) ){
								if (String.valueOf(transaccion.get("state_pol")).equals("4")){
									auditoriaResponse.setGlosa(String.valueOf(transaccion.get("sign")));
									auditoriaResponse.setTipoAccion(String.valueOf(transaccion.get("state_pol")));
									auditoriaResponse.setCodLineaNegocio("U");
									auditoriaResponse.setNumTarjeta("");
									
									ParametrizacionFac.getFacade().actualizarRegistro("updateAuditoriaPago", auditoriaResponse);
					
									PagosOnline registroPago = new PagosOnline();
									registroPago.setReferenciaVenta(auditoriaResponse.getxId());
									registroPago.setReciboConsignacion(auditoriaResponse.getReciboConsignacion());
									registroPago.setFechaTransaccion(auditoriaResponse.getFechaCreacion());
									registroPago.setFechaPago(new SimpleDateFormat(IConstantes.FORMATO_FECHA_RESPONSE_VISA).
											parse((new SimpleDateFormat(IConstantes.FORMATO_FECHA_RESPONSE_VISA)).format(auditoriaResponse.getFechaCreacion())) );
									registroPago.setHora(
												new SimpleDateFormat("HH:mm").format(new Date()));
									registroPago.setEstadoTransaccion(String.valueOf(transaccion.get("state_pol")));
									registroPago.setMedioPago(String.valueOf(transaccion.get("payment_method")));
									registroPago.setTipoMedioPago(String.valueOf(transaccion.get("payment_method_type")));
									registroPago.setBancoPse(String.valueOf(transaccion.get("pse_bank")));
									registroPago.setValor(Double.parseDouble(String.valueOf(transaccion.get("value"))));
									Moneda moneda = new Moneda();
									if(auditoriaResponse.getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)){
										if(auditoriaResponse.getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS_ICEBERG)){
											moneda = auditoriaResponse.getMoneda();
										} else{
											moneda.setCodigo(IConstantes.PESOS_COLOMBIANOS_ICEBERG);
										}
									}else {
										moneda = auditoriaResponse.getMoneda();
									}
									registroPago.setMoneda(moneda);
									registroPago.setCus(String.valueOf(transaccion.get("reference_sale")));
									registroPago.setReferenciaExtra1(String.valueOf(transaccion.get("commision_pol_currency")));
									registroPago.setReferenciaExtra2(String.valueOf(transaccion.get("commision_pol")));
									registroPago.setProcesado(IConstantes.GENERAL_NO);
									registroPago.setOrganizacion(IConstantes.ORGANIZACION_DEFECTO);
									
									ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarPagoOnlineCondicional", registroPago);
									
									if(registroPago!= null){
										if(registroPago.getSecuencia()!=null){
											if(registroPago.getSecuencia()> 0L){
													ParametrizacionFac.getFacade().ejecutarProcedimiento("procesarPagoOnline", registroPago);
												}
											}
										}
									
									Clients.showNotification("Su pago para la referencia "+
											auditoriaResponse.getReciboConsignacion().getReciboConsignacion()+
											" fue procesado de manera exitosa. ", 
											Clients.NOTIFICATION_TYPE_INFO, null, "middle_center", 6000);
								} // por acá si la transaccion existe pero no fue exitosa
									else {
									
										auditoria = new AuditoriaPago();
										auditoria.setxId(String.valueOf(transaccion.get("reference_sale")));
										auditoria.setCodTienda(String.valueOf(transaccion.get("merchant_id")));
										auditoria.setNumPedido(String.valueOf(transaccion.get("reference_sale")));
										
										try{
											 auditoriaResponse = 
													(AuditoriaPago)ParametrizacionFac.getFacade().obtenerRegistro("selectAuditoriaPagoPedido", auditoria);
								
											if(auditoriaResponse.getSecAuditoriaPago()>0L){
								
												// Si encuentra registro de auditoría entonces podemos consultar por e-ticket
												if(auditoriaResponse.getEticket()!=null && !auditoriaResponse.getEticket().equals("")){
									
															auditoriaResponse.setGlosa(String.valueOf(transaccion.get("sign")));
															auditoriaResponse.setTipoAccion(String.valueOf(transaccion.get("state_pol")));
															auditoriaResponse.setCodLineaNegocio("U");
															
															ParametrizacionFac.getFacade().actualizarRegistro("updateAuditoriaPago", auditoriaResponse);
															Clients.showNotification("Su pago para la referencia "+
																	auditoriaResponse.getReciboConsignacion().getReciboConsignacion()+
																	" no pudo ser procesado, motivo: "+
																	"fallo, cancelacion o rechazo de la transaccion", 
																	Clients.NOTIFICATION_TYPE_ERROR, null, "middle_center", 6000);
												}
											} 	
										}
										catch(Exception e){
											e.printStackTrace();
										}
										
								}
							} // por acá si la firma no coincide
							else{
								auditoria = new AuditoriaPago();
								auditoria.setxId(String.valueOf(transaccion.get("reference_sale")));
								auditoria.setCodTienda(String.valueOf(transaccion.get("merchant_id")));
								auditoria.setNumPedido(String.valueOf(transaccion.get("reference_sale")));
								
								try{
									 auditoriaResponse = 
											(AuditoriaPago)ParametrizacionFac.getFacade().obtenerRegistro("selectAuditoriaPagoPedido", auditoria);
						
									if(auditoriaResponse.getSecAuditoriaPago()>0L){
						
										// Si encuentra registro de auditoría entonces podemos consultar por e-ticket
										if(auditoriaResponse.getEticket()!=null && !auditoriaResponse.getEticket().equals("")){
							
													auditoriaResponse.setGlosa(String.valueOf(transaccion.get("sign")));
													auditoriaResponse.setTipoAccion(String.valueOf(transaccion.get("state_pol")));
													auditoriaResponse.setCodLineaNegocio("U");
													
													ParametrizacionFac.getFacade().actualizarRegistro("updateAuditoriaPago", auditoriaResponse);
													Clients.showNotification("Su pago para la referencia "+
															auditoriaResponse.getReciboConsignacion().getReciboConsignacion()+
															" no pudo ser procesado, motivo: "+
															"Las firmas no coinciden", 
															Clients.NOTIFICATION_TYPE_ERROR, null, "middle_center", 6000);
										}
									} 	
								}
								catch(Exception e){
									e.printStackTrace();
								}
							}
						}	
					}		
				}
				catch(Exception e){
					e.printStackTrace();
				}		
			} else {
				AuditoriaPago auditoria = new AuditoriaPago();
				auditoria.setxId((String)transaccion.get("reference_sale"));
				auditoria.setCodTienda((String)transaccion.get("merchant_id"));
				auditoria.setNumPedido((String)transaccion.get("reference_sale"));
				
				try{
					AuditoriaPago auditoriaResponse = 
							(AuditoriaPago)ParametrizacionFac.getFacade().obtenerRegistro("selectAuditoriaPagoPedido", auditoria);
		
					if(auditoriaResponse.getSecAuditoriaPago()>0L){
		
						// Si encuentra registro de auditoría entonces podemos consultar por e-ticket
						if(auditoriaResponse.getEticket()!=null && !auditoriaResponse.getEticket().equals("")){
			
									auditoriaResponse.setGlosa("Transaccion Rechazada");
									auditoriaResponse.setTipoAccion((String)transaccion.get("state_pol"));
									auditoriaResponse.setCodLineaNegocio("U");
									
									ParametrizacionFac.getFacade().actualizarRegistro("updateAuditoriaPago", auditoriaResponse);
									Clients.showNotification("Su pago para la referencia "+
											auditoriaResponse.getReciboConsignacion().getReciboConsignacion()+
											" no pudo ser procesado, motivo: "+
											"transacción Rechazada", 
											Clients.NOTIFICATION_TYPE_ERROR, null, "middle_center", 6000);
						}
					} 	
				}
				catch(Exception e){
					e.printStackTrace();
				}
				
			}
		} catch(Exception e){
			e.printStackTrace();
		}
	}

public void afterCompose(){	
	this.onRecibirParametros();
}

}
