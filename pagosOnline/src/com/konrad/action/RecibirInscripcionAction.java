package com.konrad.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Column;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import com.konrad.assembler.InscripcionAssembler;
import com.konrad.domain.CategoriaMatricula;
import com.konrad.domain.CentroCosto;
import com.konrad.domain.Cliente;
import com.konrad.domain.ConceptoAcademico;
import com.konrad.domain.Departamento;
import com.konrad.domain.DetalleOrden;
import com.konrad.domain.Estudiante;
import com.konrad.domain.EstudianteSolicitud;
import com.konrad.domain.Facultad;
import com.konrad.domain.OrdenAcademico;
import com.konrad.domain.Pais;
import com.konrad.domain.Patrocinado;
import com.konrad.domain.Patrocinador;
import com.konrad.domain.PeriodoAcademico;
import com.konrad.domain.PeriodoFacturacion;
import com.konrad.domain.Persona;
import com.konrad.domain.PlanEstudios;
import com.konrad.domain.Poblacion;
import com.konrad.domain.ProgramaAcademico;
import com.konrad.domain.ProgramaReporte;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.TasaRepresentativaMercado;
import com.konrad.domain.TipoIdentificacion;
import com.konrad.domain.TransaccionZonaPagos;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.helper.AppContext;
import com.konrad.helper.ReciboConsignacionHelper;
import com.konrad.listener.ContextoAplicacion;
import com.konrad.util.EmailThread;
import com.konrad.util.IConstantes;
import com.konrad.util.Moneda;
import com.konrad.window.ActionStandardBorder;
import com.zonapagos.www.prod.ZPagosLocator;
import com.zonapagos.www.prod.ZPagosSoapProxy;
public class RecibirInscripcionAction  extends ActionStandardBorder implements AfterCompose {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected static Logger log = Logger.getLogger(RecibirInscripcionAction.class);
	private ZPagosLocator zonaPagosLocator;
	private ZPagosSoapProxy zonaPagosProxy;
	private Intbox txtRecibirInscripcionIdentificacion;
	private Listbox lbxRecibirInscripcionTipoIdentificacion;
	private Listbox lbxRecibirInscripcionSexo;
	private Textbox txtRecibirInscripcionNombres;
	private Textbox txtRecibirInscripcionPrimerApellido;
	private Textbox txtRecibirInscripcionSegundoApellido;
	private Textbox txtRecibirInscripcionTelefono;
	private Textbox txtRecibirInscripcionDireccionElectronica;
	private Textbox txtRecibirInscripcionDireccionContacto;
	private Combobox cbxRecibirInscripcionUbicacion;
	private Listbox lbxRecibirInscripcionEscolaridad;
	private Listbox lbxRecibirInscripcionOcupacion;
	private Listbox lbxRecibirInscripcionPrograma;
	private Checkbox chbxRecibirInscripcionAceptarCondiciones;
	private Checkbox chbxRecibirInscripcionAceptarPersonal;
	private Button idBtnRecibirInscripcionInscribir;
	private Grid idRecibirInscripcionEduContGrdPrincipal;
	private Rows idRecibirInscripcionEduContRowsPrincipal;
	private Groupbox idGbxFormGenerarInscripcionEduCont;
	private Groupbox idGbxFormPagarInscripcionEduCont;
	private Button idBtnRecibirInscripcionImprimirOrden;
	private Button idBtnRecibirInscripcionPagar;
	private Radiogroup idRadioGroupInscripcionEduCont;
	private Listbox idLbxInscripcionEduContMoneda;
	private Image idImgMonedaInscripcionEducacionContinuada;
	private Listbox idLbxRecibirInscripcionTipoIdPatrocinador;
	private Listbox lbxRecibirInscripcionModalidadAsistencia;
	private Intbox idIbxRecibirInscripcionNitPatrocinador;
	private Textbox idTxtRecibirInscripcionNombrePatrocinador;
	private Textbox idTxtRecibirInscripcionPrimerApellidoPatrocinador;
	private Textbox idTxtRecibirInscripcionSegundoApellidoPatrocinador;
	private Textbox idTxtRecibirInscripcionEmailPatrocinador;
	private Textbox idTxtRecibirInscripcionTelefonoPatrocinador;
	private Textbox idTxtRecibirInscripcionNombreContactoPatrocinador;
	private Textbox idTxtRecibirInscripcionArchivoRutPatrocinador;
	private Combobox idCbxRecibirInscripcionPatrocinador;
	private Checkbox chbxRecibirInscripcionPatrocinado;
	private Textbox idTxtRecibirInscripcionCargo;
	private File fileRutPatrocinador;
	private Persona persona;
	private Patrocinador patrocinador;
	private Patrocinado patrocinado;
	private ProgramaAcademico programaAcademico;
	private Label idLblRecibirInscripcionTituloPrograma;
	private Div idDivRecibirInscripcionNewPatrocinador;
	private Checkbox chbxRecibirInscripcionAceptarCondicionesNewPatrocinador;
	private Checkbox chbxRecibirInscripcionAceptarPersonalNewPatrocinador;
	private Button idBtnRecibirInscripcionNuevoPatrocinador;
	
	// modificacion cesar chacon 19 de febrero 2020
	private org.zkoss.zhtml.Label idLblRecibirInscripcionTipoId;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionIdenfiticacion;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionGenero;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionNombres;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionPrimer_Apellido;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionSegundo_Apellido;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionTelefono_movil;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionDireccion_Electronica;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionDireccion_Contacto;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionCiudad;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionGrado_escolaridad;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionOcupacion;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionPrograma;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionCargo;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionEmpresaRazonSocial;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionFacturaEmpresa;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionAceptoTratamientoDatos;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionModalidadAsistencia;
	private org.zkoss.zhtml.H2 idLblRecibirInscripcionEducacionContinuada;
	private org.zkoss.zhtml.H3 idLblRecibirInscripcionFormularioInscripcion;
	private org.zkoss.zhtml.Img idLblRecibirInscripcionimg;
	private org.zkoss.zhtml.Label idBtnRecibirInscripcionCondiciones;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionDatosPersonales;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionDatosPago;	
	private org.zkoss.zhtml.Label idLblFormPagosImprimirOrden;
	private org.zkoss.zhtml.Label idLblFormPagosLinea;
	private org.zkoss.zhtml.Label idLblFormPagosListadoPrincipal;
	private org.zkoss.zhtml.Label idLblFormPagosModenaPago;	
	private org.zkoss.zhtml.Label idLbxInscripcionEduContMonedaMoneda;
	private Button idBtnRecibirInscripcionVolver;
	private Button idBtnRecibirInscripcionGuardarPatrocinadorExistente;
	private Button idBtnRecibirInscripcionCancelarPatrocinadorExistente;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionDatosEmpresa;
	private org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorTipoIdentificacion;
	private org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorNumeroIdentificacion;
	private org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorNombreRazonSocial;
	private org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorPrimerApellido;
	private org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorSegundoApellido;
	private org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorDireccionContacto;
	private org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorTelefonoContacto;
	private org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorNombreContacto;
	private org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorArchivoRut;
	private Button idBtnRecibirInscripcionBuscarRut;
	private org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorCondiciones;
	private org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorAceptoTratamientoDatos;
	private Button idBtnRecibirInscripcionGuardarPatrocinador;
	private Button idBtnRecibirInscripcionCancelarPatrocinador;
	private org.zkoss.zhtml.H3 idDivRecibirInscripcionMensajeCulminarInscripcionInscripcionParticipante;
	private org.zkoss.zhtml.H3 idDivRecibirInscripcionMensajeCulminarInscripcionFinalTexto;
	private Button idBtnRecibirInscripcionNuevoInscrito;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionCasillaEstudiante;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionCasillaCena;
	private org.zkoss.zhtml.Label idLblRecibirInscripcionUniversidadProcedencia;
	private org.zkoss.zhtml.Div idDivRecibirInscripcionCasillaCenaVisible;
	private org.zkoss.zhtml.Div idDivRecibirInscripcionCasillaEstudiante;
	private org.zkoss.zhtml.Div idDivRecibirInscripcionUniversidadProcedencia;
	private org.zkoss.zhtml.Div idDivRecibirInscripcionModalidadAsistencia;
	private Radio chbxRecibirInscripcionCasillaEstudianteRadio1;
	private Radio chbxRecibirInscripcionCasillaEstudianteRadio2;
	private Radio chbxRecibirInscripcionCasillaCenaRadio1;
	private Radio chbxRecibirInscripcionCasillaCenaRadio2;
	private Button idBtnRecibirInscripcionInscribirCena;
	private Textbox txtRecibirInscripcionUniversidadProcedencia;
	private Combobox idCbxRecibirInscripcionIdioma;
	private org.zkoss.zhtml.Div idDivRecibirInscripcionMensajeCulminarInscripcionCena; 
	private org.zkoss.zhtml.Div idLblRecibirInscripcionSegundo_ApellidoEstado;
	
	private Properties appProperties;
	private String contextPath;
	private List<ReciboConsignacion> listaRecibosEduCont;
	private List<ReciboConsignacion> listaRecibosEduContPagos;
	private List<ReciboConsignacion> listaRecibosEduContAnt;
	private String language;
	private org.zkoss.zhtml.H4 idDivRecibirInscripcionMensajeCulminarInscripcionInfoCena;
	private Boolean encontroPersona;
	private String mensajeTipoIdentificacion;
	
	@SuppressWarnings("rawtypes")
	public void afterCompose(){	
		log.info("[afterCompose]");
		this.setTxtRecibirInscripcionIdentificacion((Intbox)this.getFellow("txtRecibirInscripcionIdentificacion"));
		this.setLbxRecibirInscripcionTipoIdentificacion((Listbox)this.getFellow("lbxRecibirInscripcionTipoIdentificacion"));
		this.setLbxRecibirInscripcionSexo((Listbox)this.getFellow("lbxRecibirInscripcionSexo"));
		this.setTxtRecibirInscripcionNombres((Textbox)this.getFellow("txtRecibirInscripcionNombres"));
		this.setTxtRecibirInscripcionPrimerApellido((Textbox)this.getFellow("txtRecibirInscripcionPrimerApellido"));
		this.setTxtRecibirInscripcionSegundoApellido((Textbox)this.getFellow("txtRecibirInscripcionSegundoApellido"));
		this.setTxtRecibirInscripcionTelefono((Textbox)this.getFellow("txtRecibirInscripcionTelefono"));
		this.setTxtRecibirInscripcionDireccionElectronica((Textbox)this.getFellow("txtRecibirInscripcionDireccionElectronica"));
		this.setTxtRecibirInscripcionDireccionContacto((Textbox)this.getFellow("txtRecibirInscripcionDireccionContacto"));
		this.setCbxRecibirInscripcionUbicacion((Combobox)this.getFellow("cbxRecibirInscripcionUbicacion"));
		this.setLbxRecibirInscripcionEscolaridad((Listbox)this.getFellow("lbxRecibirInscripcionEscolaridad"));
		this.setLbxRecibirInscripcionOcupacion((Listbox)this.getFellow("lbxRecibirInscripcionOcupacion"));
		this.setLbxRecibirInscripcionPrograma((Listbox)this.getFellow("lbxRecibirInscripcionPrograma"));
		this.setLbxRecibirInscripcionModalidadAsistencia((Listbox)this.getFellow("lbxRecibirInscripcionModalidadAsistencia"));
		this.setChbxRecibirInscripcionAceptarCondiciones((Checkbox)this.getFellow("chbxRecibirInscripcionAceptarCondiciones"));
		this.setChbxRecibirInscripcionAceptarPersonal((Checkbox)this.getFellow("chbxRecibirInscripcionAceptarPersonal")) ;
		this.setIdBtnRecibirInscripcionInscribir((Button)this.getFellow("idBtnRecibirInscripcionInscribir"));
		this.setIdRecibirInscripcionEduContGrdPrincipal((Grid)this.getFellow("idRecibirInscripcionEduContGrdPrincipal"));
		this.setIdRecibirInscripcionEduContRowsPrincipal((Rows)this.getFellow("idRecibirInscripcionEduContRowsPrincipal"));
		this.setIdGbxFormGenerarInscripcionEduCont((Groupbox)this.getFellow("idGbxFormGenerarInscripcionEduCont"));
		this.setIdGbxFormPagarInscripcionEduCont((Groupbox)this.getFellow("idGbxFormPagarInscripcionEduCont"));
		this.setIdBtnRecibirInscripcionImprimirOrden((Button)this.getFellow("idBtnRecibirInscripcionImprimirOrden"));
		this.setIdBtnRecibirInscripcionPagar((Button)this.getFellow("idBtnRecibirInscripcionPagar"));
		this.setIdRadioGroupInscripcionEduCont((Radiogroup)this.getFellow("idRadioGroupInscripcionEduCont") );
		this.setIdLbxInscripcionEduContMoneda((Listbox)this.getFellow("idLbxInscripcionEduContMoneda"));
		this.setIdImgMonedaInscripcionEducacionContinuada((Image)this.getFellow("idImgMonedaInscripcionEducacionContinuada"));
		this.setIdTxtRecibirInscripcionCargo((Textbox)this.getFellow("idTxtRecibirInscripcionCargo"));
		this.setIdLbxRecibirInscripcionTipoIdPatrocinador((Listbox)this.getFellow("idLbxRecibirInscripcionTipoIdPatrocinador"));  
		this.setIdIbxRecibirInscripcionNitPatrocinador((Intbox)this.getFellow("idIbxRecibirInscripcionNitPatrocinador"));  
		this.setIdTxtRecibirInscripcionNombrePatrocinador((Textbox)this.getFellow("idTxtRecibirInscripcionNombrePatrocinador"));
		this.setIdTxtRecibirInscripcionPrimerApellidoPatrocinador((Textbox)this.getFellow("idTxtRecibirInscripcionPrimerApellidoPatrocinador"));
		this.setIdTxtRecibirInscripcionSegundoApellidoPatrocinador((Textbox)this.getFellow("idTxtRecibirInscripcionSegundoApellidoPatrocinador")); 
		this.setIdTxtRecibirInscripcionEmailPatrocinador((Textbox)this.getFellow("idTxtRecibirInscripcionEmailPatrocinador"));
		this.setIdTxtRecibirInscripcionTelefonoPatrocinador((Textbox)this.getFellow("idTxtRecibirInscripcionTelefonoPatrocinador" ));
		this.setIdTxtRecibirInscripcionNombreContactoPatrocinador((Textbox)this.getFellow("idTxtRecibirInscripcionNombreContactoPatrocinador" ));
		this.setIdTxtRecibirInscripcionArchivoRutPatrocinador((Textbox)this.getFellow("idTxtRecibirInscripcionArchivoRutPatrocinador")); 
		this.setIdCbxRecibirInscripcionPatrocinador((Combobox)this.getFellow("idCbxRecibirInscripcionPatrocinador"));	
		this.setChbxRecibirInscripcionPatrocinado((Checkbox)this.getFellow("chbxRecibirInscripcionPatrocinado"));
		this.setIdLblRecibirInscripcionTituloPrograma((Label)this.getFellow("idLblRecibirInscripcionTituloPrograma"));
		this.setIdDivRecibirInscripcionNewPatrocinador((Div)this.getFellow("idDivRecibirInscripcionNewPatrocinador"));
		this.setChbxRecibirInscripcionAceptarCondicionesNewPatrocinador((Checkbox)this.getFellow("chbxRecibirInscripcionAceptarCondicionesNewPatrocinador"));
		this.setChbxRecibirInscripcionAceptarPersonalNewPatrocinador((Checkbox)this.getFellow("chbxRecibirInscripcionAceptarPersonalNewPatrocinador"));
		this.setIdBtnRecibirInscripcionNuevoPatrocinador((Button)this.getFellow("idBtnRecibirInscripcionNuevoPatrocinador"));
		// cesar modificacion
		this.setIdLblRecibirInscripcionTipoId((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionTipoId"));
		this.setIdLblRecibirInscripcionIdenfiticacion((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionIdenfiticacion"));
		this.setIdLblRecibirInscripcionGenero((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionGenero"));
		this.setIdLblRecibirInscripcionNombres((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionNombres"));
		this.setIdLblRecibirInscripcionSegundo_Apellido((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionSegundo_Apellido"));
		this.setIdLblRecibirInscripcionTelefono_movil((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionTelefono_movil"));
		this.setIdLblRecibirInscripcionDireccion_Electronica((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionDireccion_Electronica"));
		this.setIdLblRecibirInscripcionModalidadAsistencia((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionModalidadAsistencia"));
		this.setIdLblRecibirInscripcionDireccion_Contacto((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionDireccion_Contacto"));
		this.setIdLblRecibirInscripcionCiudad((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionCiudad"));
		this.setIdLblRecibirInscripcionPrimer_Apellido((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionPrimer_Apellido"));
		this.setIdLblRecibirInscripcionGrado_escolaridad((org.zkoss.zhtml.Label) this.getFellow("idLblRecibirInscripcionGrado_escolaridad"));
		this.setIdLblRecibirInscripcionOcupacion((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionOcupacion"));
		this.setIdLblRecibirInscripcionPrograma((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionPrograma"));
		this.setIdLblRecibirInscripcionCargo((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionCargo"));
		this.setIdLblRecibirInscripcionEmpresaRazonSocial((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionEmpresaRazonSocial"));
		this.setIdLblRecibirInscripcionFacturaEmpresa((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionFacturaEmpresa"));
		this.setIdLblRecibirInscripcionAceptoTratamientoDatos((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionAceptoTratamientoDatos"));
		this.setIdLblRecibirInscripcionEducacionContinuada((org.zkoss.zhtml.H2) this.getFellow("idLblRecibirInscripcionEducacionContinuada"));
		this.setIdLblRecibirInscripcionFormularioInscripcion((org.zkoss.zhtml.H3) this.getFellow("idLblRecibirInscripcionFormularioInscripcion"));
		this.setIdLblRecibirInscripcionimg((org.zkoss.zhtml.Img) this.getFellow("idLblRecibirInscripcionimg"));
		this.setIdBtnRecibirInscripcionCondiciones((org.zkoss.zhtml.Label) this.getFellow("idBtnRecibirInscripcionCondiciones"));
		this.setIdLblRecibirInscripcionDatosPersonales((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionDatosPersonales"));
		this.setIdLblRecibirInscripcionDatosPago((org.zkoss.zhtml.Label) this.getFellow("idLblRecibirInscripcionDatosPago"));	
		this.setIdLblFormPagosImprimirOrden((org.zkoss.zhtml.Label)this.getFellow("idLblFormPagosImprimirOrden"));
		this.setIdLblFormPagosLinea((org.zkoss.zhtml.Label)this.getFellow("idLblFormPagosLinea"));
		this.setIdLblFormPagosListadoPrincipal((org.zkoss.zhtml.Label)this.getFellow("idLblFormPagosListadoPrincipal"));
		this.setIdLblFormPagosModenaPago((org.zkoss.zhtml.Label)this.getFellow("idLblFormPagosModenaPago"));	
		this.setIdLbxInscripcionEduContMonedaMoneda((org.zkoss.zhtml.Label) this.getFellow("idLbxInscripcionEduContMonedaMoneda"));
		this.setIdBtnRecibirInscripcionVolver((Button)this.getFellow("idBtnRecibirInscripcionVolver"));
		this.setIdBtnRecibirInscripcionGuardarPatrocinadorExistente((Button)this.getFellow("idBtnRecibirInscripcionGuardarPatrocinadorExistente"));
		this.setIdBtnRecibirInscripcionCancelarPatrocinadorExistente((Button) this.getFellow("idBtnRecibirInscripcionCancelarPatrocinadorExistente"));
		this.setIdLblRecibirInscripcionDatosEmpresa((org.zkoss.zhtml.Label) this.getFellow("idLblRecibirInscripcionDatosEmpresa"));
		this.setIdDivRecibirInscripcionNewPatrocinadorTipoIdentificacion((org.zkoss.zhtml.Label) this.getFellow("idDivRecibirInscripcionNewPatrocinadorTipoIdentificacion"));
		this.setIdDivRecibirInscripcionNewPatrocinadorNumeroIdentificacion((org.zkoss.zhtml.Label)this.getFellow("idDivRecibirInscripcionNewPatrocinadorNumeroIdentificacion"));
		this.setIdDivRecibirInscripcionNewPatrocinadorNombreRazonSocial((org.zkoss.zhtml.Label) this.getFellow("idDivRecibirInscripcionNewPatrocinadorNombreRazonSocial"));
		this.setIdDivRecibirInscripcionNewPatrocinadorPrimerApellido((org.zkoss.zhtml.Label) this.getFellow("idDivRecibirInscripcionNewPatrocinadorPrimerApellido"));
		this.setIdDivRecibirInscripcionNewPatrocinadorSegundoApellido((org.zkoss.zhtml.Label) this.getFellow("idDivRecibirInscripcionNewPatrocinadorSegundoApellido"));
		this.setIdDivRecibirInscripcionNewPatrocinadorDireccionContacto((org.zkoss.zhtml.Label) this.getFellow("idDivRecibirInscripcionNewPatrocinadorDireccionContacto"));
		this.setIdDivRecibirInscripcionNewPatrocinadorTelefonoContacto((org.zkoss.zhtml.Label) this.getFellow("idDivRecibirInscripcionNewPatrocinadorTelefonoContacto"));
		this.setIdDivRecibirInscripcionNewPatrocinadorNombreContacto((org.zkoss.zhtml.Label) this.getFellow("idDivRecibirInscripcionNewPatrocinadorNombreContacto"));
		this.setIdDivRecibirInscripcionNewPatrocinadorArchivoRut((org.zkoss.zhtml.Label) this.getFellow("idDivRecibirInscripcionNewPatrocinadorArchivoRut"));
		this.setIdBtnRecibirInscripcionBuscarRut((Button) this.getFellow("idBtnRecibirInscripcionBuscarRut"));
		this.setIdDivRecibirInscripcionNewPatrocinadorCondiciones((org.zkoss.zhtml.Label) this.getFellow("idDivRecibirInscripcionNewPatrocinadorCondiciones"));
		this.setIdDivRecibirInscripcionNewPatrocinadorAceptoTratamientoDatos((org.zkoss.zhtml.Label) this.getFellow("idDivRecibirInscripcionNewPatrocinadorAceptoTratamientoDatos"));
		this.setIdBtnRecibirInscripcionGuardarPatrocinador((Button) this.getFellow("idBtnRecibirInscripcionGuardarPatrocinador"));
		this.setIdBtnRecibirInscripcionCancelarPatrocinador((Button)this.getFellow("idBtnRecibirInscripcionCancelarPatrocinador"));
		this.setIdDivRecibirInscripcionMensajeCulminarInscripcionInscripcionParticipante((org.zkoss.zhtml.H3) this.getFellow("idDivRecibirInscripcionMensajeCulminarInscripcionInscripcionParticipante"));
		this.setIdDivRecibirInscripcionMensajeCulminarInscripcionFinalTexto((org.zkoss.zhtml.H3)this.getFellow("idDivRecibirInscripcionMensajeCulminarInscripcionFinalTexto"));
		this.setIdBtnRecibirInscripcionNuevoInscrito((Button)this.getFellow("idBtnRecibirInscripcionNuevoInscrito"));
		this.setIdLblRecibirInscripcionCasillaEstudiante((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionCasillaEstudiante"));
		this.setIdLblRecibirInscripcionCasillaCena((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionCasillaCena"));
		this.setIdLblRecibirInscripcionUniversidadProcedencia((org.zkoss.zhtml.Label)this.getFellow("idLblRecibirInscripcionUniversidadProcedencia"));
		this.setIdDivRecibirInscripcionCasillaCenaVisible((org.zkoss.zhtml.Div)this.getFellow("idDivRecibirInscripcionCasillaCenaVisible"));
		this.setIdDivRecibirInscripcionCasillaEstudiante((org.zkoss.zhtml.Div)this.getFellow("idDivRecibirInscripcionCasillaEstudiante"));
		this.setIdDivRecibirInscripcionUniversidadProcedencia((org.zkoss.zhtml.Div)this.getFellow("idDivRecibirInscripcionUniversidadProcedencia"));
		this.setChbxRecibirInscripcionCasillaEstudianteRadio1((Radio)this.getFellow("chbxRecibirInscripcionCasillaEstudianteRadio1"));
		this.setChbxRecibirInscripcionCasillaEstudianteRadio2((Radio)this.getFellow("chbxRecibirInscripcionCasillaEstudianteRadio2"));
		this.setChbxRecibirInscripcionCasillaCenaRadio1((Radio)this.getFellow("chbxRecibirInscripcionCasillaCenaRadio1"));
		this.setChbxRecibirInscripcionCasillaCenaRadio2((Radio)this.getFellow("chbxRecibirInscripcionCasillaCenaRadio2"));
		this.setIdBtnRecibirInscripcionInscribirCena((Button)this.getFellow("idBtnRecibirInscripcionInscribirCena"));
		this.setTxtRecibirInscripcionUniversidadProcedencia((Textbox) this.getFellow("txtRecibirInscripcionUniversidadProcedencia"));
		this.setIdCbxRecibirInscripcionIdioma((Combobox) this.getFellow("idCbxRecibirInscripcionIdioma"));
		this.setIdDivRecibirInscripcionMensajeCulminarInscripcionInfoCena((org.zkoss.zhtml.H4) this.getFellow("idDivRecibirInscripcionMensajeCulminarInscripcionInfoCena"));
		this.setIdDivRecibirInscripcionMensajeCulminarInscripcionCena((org.zkoss.zhtml.Div) this.getFellow("idDivRecibirInscripcionMensajeCulminarInscripcionCena"));
		this.setIdDivRecibirInscripcionModalidadAsistencia((org.zkoss.zhtml.Div) this.getFellow("idDivRecibirInscripcionModalidadAsistencia"));
		this.setIdLblRecibirInscripcionSegundo_ApellidoEstado((org.zkoss.zhtml.Div) this.getFellow("idLblRecibirInscripcionSegundo_ApellidoEstado"));
		this.encontroPersona = false;
		this.addListenerUploadArchivoRut();
		this.setListaRecibosEduCont(new ArrayList<ReciboConsignacion>());
		this.setListaRecibosEduContAnt(new ArrayList<ReciboConsignacion>());
		this.setListaRecibosEduContPagos(new ArrayList<ReciboConsignacion>());
		this.setListValuesPrograma();
		this.setListValuesTipoIdentificacion();
		this.setListValuesTipoIdentificacionPatrocinador();
		log.info("cargar listado de poblaciones");
		this.setListValuesUbicacion();
		this.setListValuesPersonasPatrocinador();
		this.fijarTrmSistema();
		ReciboConsignacionHelper.getHelper().onInicializarListaCurrency((Listbox)this.getIdLbxInscripcionEduContMoneda());
		ReciboConsignacionHelper.getHelper().onRetirarElementosListaCurrency((Listbox)this.getIdLbxInscripcionEduContMoneda(),
				((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
						getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_EURO)).getValor(),
						((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
								getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor());
		this.onSetImageCurrency();
		this.verificarActivacionInscripcion();
		this.onRecibirParametros();
		this.contextPath = ContextoAplicacion.getInstance().getRutaContexto();
		this.setLanguage(Executions.getCurrent().getDesktop().getSession().getAttribute("language").toString());
		for(Comboitem item : this.getIdCbxRecibirInscripcionIdioma().getItems()){
			if(item.getValue().toString().equalsIgnoreCase(this.getLanguage())){
				this.getIdCbxRecibirInscripcionIdioma().setSelectedItem(item);
			}
		}
		this.cambiarLabelsIdioma();		
		this.ocultarCamposSegunTipo();
		this.onSetProgramaAcademicoItem();
		this.onSetTituloPrograma();
		this.onValidateModalidadAsistencia();
	}
	

	public void onSetTituloPrograma() {
		
		if(this.getLbxRecibirInscripcionPrograma()!=null) {
			if(this.getLbxRecibirInscripcionPrograma().getSelectedItem()!=null) {
				Listitem item = this.getLbxRecibirInscripcionPrograma().getSelectedItem();
				if(Integer.valueOf(item.getValue().toString())>0) {
					this.getIdLblRecibirInscripcionTituloPrograma().setValue(item.getLabel());
				}
			}
		}
		
	}
	
	public void onValidateModalidadAsistencia() {
		try {
			
			this.getIdDivRecibirInscripcionModalidadAsistencia().setVisible(false);
			Map<String,Object> mapaParam = new HashMap<String, Object>();
			mapaParam.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
			mapaParam.put("parametro", IConstantes.PROGRAMAS_VALIDA_MODALIDAD);
			String programasModalidad = (String)ParametrizacionFac
					.getFacade().obtenerRegistro("getValorParametro", mapaParam);
			String[] arrayProg = programasModalidad.split(",");
			ProgramaAcademico prog =
					(ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA");
			if(prog!=null && prog.getCentroCosto()!=null && prog.getCentroCosto().getCentroCosto()!=null) {
				for(String item : arrayProg) {
					if(item.equalsIgnoreCase(prog.getCentroCosto().getCentroCosto())) {
						this.getIdDivRecibirInscripcionModalidadAsistencia().setVisible(true);
					}
				}
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void onValidateCupoPrograma() {
		try {
			log.info("[onValidateCupoPrograma]");
			if(this.getLbxRecibirInscripcionModalidadAsistencia().getSelectedIndex()>0
					&& this.getIdDivRecibirInscripcionModalidadAsistencia().isVisible()) {
				
				this.getIdBtnRecibirInscripcionInscribir().setDisabled(false);
				
				String modalidad = 
						(String)this.getLbxRecibirInscripcionModalidadAsistencia().getSelectedItem().getValue();
				ProgramaAcademico prog =
						(ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA");
		
				Map<String,Object> mapaParam = new HashMap<String, Object>();
				mapaParam.put("PERIODO", prog.getPeriodo());
				mapaParam.put("CENTRO_COSTO", prog.getCentroCosto().getCentroCosto());
			
				Integer inscritosPresencial = (Integer)ParametrizacionFac.getFacade()
					.obtenerRegistro("selectInscritosModalidadPresencialEduCont", mapaParam); 
		
				log.info("inscritos: "+inscritosPresencial);
				log.info("cupo: "+prog.getCupoPrograma());
				
				if( modalidad.equalsIgnoreCase("P") && 
						prog.getCupoPrograma()<= inscritosPresencial &&
						this.getIdDivRecibirInscripcionModalidadAsistencia().isVisible()) {
						Clients.showNotification("Se ha superado el cupo máximo en modalidad presencial para este programa.",
						Clients.NOTIFICATION_TYPE_ERROR,		
						this.getLbxRecibirInscripcionModalidadAsistencia(), 
						"end_before",
						2000,
						true);
						this.getIdBtnRecibirInscripcionInscribir().setDisabled(true);
						this.getLbxRecibirInscripcionModalidadAsistencia().invalidate();
						
					} else {
						this.getIdBtnRecibirInscripcionInscribir().setDisabled(false);
						this.getLbxRecibirInscripcionModalidadAsistencia().invalidate();
						
					}
				} else {
					if(this.getIdDivRecibirInscripcionModalidadAsistencia().isVisible()) {
					Clients.showNotification("Seleccione una modalidad de asistencia.", 
							Clients.NOTIFICATION_TYPE_ERROR,		
							this.getLbxRecibirInscripcionModalidadAsistencia(), 
							"end_before",
							2000,
							true);
					this.getIdBtnRecibirInscripcionInscribir().setDisabled(true);
					this.getLbxRecibirInscripcionModalidadAsistencia().invalidate();
					}
				}
		} catch(Exception e) {
			e.printStackTrace();
			Messagebox.show("Error validación cupos: "+e.getLocalizedMessage());
		}
	}
	
	public void verificarActivacionInscripcion(){
		try{
			String activacion = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectActivacionInscripcionEduCont");
			if(!activacion.equalsIgnoreCase("S")){
				((Textbox)this.getFellow("txtRecibirInscripcionNombres")).setDisabled(true);
				((Textbox)this.getFellow("txtRecibirInscripcionPrimerApellido")).setDisabled(true);
				((Textbox)this.getFellow("txtRecibirInscripcionSegundoApellido")).setDisabled(true);
				((Textbox)this.getFellow("txtRecibirInscripcionTelefono")).setDisabled(true);
				((Textbox)this.getFellow("txtRecibirInscripcionDireccionElectronica")).setDisabled(true);
				((Textbox)this.getFellow("txtRecibirInscripcionDireccionContacto")).setDisabled(true);
				((Intbox)this.getFellow("txtRecibirInscripcionIdentificacion")).setDisabled(true);
				((Listbox)this.getFellow("lbxRecibirInscripcionSexo")).setDisabled(true);
				((Listbox)this.getFellow("lbxRecibirInscripcionTipoIdentificacion")).setDisabled(true);
				((Combobox)this.getFellow("cbxRecibirInscripcionUbicacion")).setDisabled(true);
				((Listbox)this.getFellow("lbxRecibirInscripcionPrograma")).setDisabled(true);
				((Listbox)this.getFellow("lbxRecibirInscripcionEscolaridad")).setDisabled(true);
				((Listbox)this.getFellow("lbxRecibirInscripcionOcupacion")).setDisabled(true);
				((Checkbox)this.getFellow("chbxRecibirInscripcionAceptarCondiciones")).setDisabled(true);
				((Checkbox)this.getFellow("chbxRecibirInscripcionAceptarPersonal")).setDisabled(true);
				((Button)this.getFellow("idBtnRecibirInscripcionInscribir")).setDisabled(true);
			}
			
			
		}catch(Exception e){
			log.info(e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	public void onSetImageCurrency(){
		ReciboConsignacionHelper.getHelper().onSetImageCurrency(this.getIdLbxInscripcionEduContMoneda(), 
				this.getIdImgMonedaInscripcionEducacionContinuada());
	}
	
	public void setMensajeVentanasEmergentes(Component componente, String posicion ){
		Clients.showNotification(IConstantes.MENSAJE_PERMITIR_POPUPS_INSCRIPCION, Clients.NOTIFICATION_TYPE_WARNING, componente, posicion, 20000, true);
	}
	
	public void setMensajeVentanasEmergentes(){
		Clients.showNotification(IConstantes.MENSAJE_PERMITIR_POPUPS_INSCRIPCION, Clients.NOTIFICATION_TYPE_WARNING, this, "before_center", 20000, true);
	}
	
	public void cargarZul(String menu){
		Component miPage = this.getParent();
		this.detach();
		Executions.createComponents(menu, miPage, null);
	}
	
public void onRecibirParametros(){
	try{
	log.info("ejecutando el m�todo [recibirInscripcion.onRecibirParametros] ");
		// se reciben los par�metros desde el GET
	String tipo = "";
	ProgramaAcademico programaAcademico = new ProgramaAcademico();
	
	programaAcademico.setIdPrograma(
			Executions.getCurrent().getParameterMap().get("programa")!=null? 
			 !((String[])Executions.getCurrent().getParameterMap().get("programa"))[0].equals("") ?
					((String[])Executions.getCurrent().getParameterMap().get("programa"))[0].toString():"-1":"-1");
	log.info("pasa id programa "+programaAcademico.getIdPrograma());
	this.setLanguage(Executions.getCurrent().getParameterMap().get("idioma")!=null? 
			 !((String[])Executions.getCurrent().getParameterMap().get("idioma"))[0].equals("") ?
						((String[])Executions.getCurrent().getParameterMap().get("idioma"))[0].toString():"es":"es");						
	log.info(this.getLanguage());
	
	tipo = Executions.getCurrent().getParameterMap().get("tipo")!=null? 
			 !((String[])Executions.getCurrent().getParameterMap().get("tipo"))[0].equals("") ?
						((String[])Executions.getCurrent().getParameterMap().get("tipo"))[0].toString():"C":"C";
	log.info("Tipo Evento " +tipo);
	Executions.getCurrent().getDesktop().getSession().setAttribute("tipo", tipo);
	Executions.getCurrent().getDesktop().getSession().setAttribute("language", this.getLanguage());
	
	this.setLanguage(Executions.getCurrent().getDesktop().getSession().getAttribute("language").toString());
	org.zkoss.zk.ui.Session session = Sessions.getCurrent();
	Locale prefer_Locale = org.zkoss.util.Locales.getLocale(this.getLanguage());
	log.info(prefer_Locale.toLanguageTag());

	Clients.reloadMessages(prefer_Locale);
    session.setAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE, prefer_Locale);
    org.zkoss.util.Locales.setThreadLocal(org.zkoss.util.Locales.getLocale(this.getLanguage()));
		
	this.setProgramaAcademico(programaAcademico);

	}catch(Exception e){
		e.printStackTrace();
	}
}

public void onRecibirParametroComboBoxIdioma() {
	
	try {
	this.setLanguage(this.getIdCbxRecibirInscripcionIdioma().getSelectedItem().getValue().toString());
	Executions.getCurrent().getDesktop().getSession().setAttribute("language", this.getLanguage());
	org.zkoss.zk.ui.Session session = Sessions.getCurrent();
	Locale prefer_Locale = org.zkoss.util.Locales.getLocale(this.getLanguage());
	log.info(prefer_Locale.toLanguageTag());

	
	Clients.reloadMessages(prefer_Locale);
	session.setAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE, prefer_Locale);
	org.zkoss.util.Locales.setThreadLocal(org.zkoss.util.Locales.getLocale(this.getLanguage()));
	this.setPersona(new Persona());
	this.limpiarCamposFormularioPersona();
	this.cambiarLabelsIdioma();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}

@SuppressWarnings("unused")
public void cambiarLabelsIdioma() {
	String tipo = Executions.getCurrent().getDesktop().getSession().getAttribute("tipo").toString();
	if(this.getLanguage().equalsIgnoreCase("en")) {
		this.appProperties = this.getProperties(this.contextPath + IConstantes.RUTA_I18N_EN);
	}else if(this.getLanguage().equalsIgnoreCase("es")) {
		this.appProperties = this.getProperties(this.contextPath + IConstantes.RUTA_I18N_ES);
	}
		
	AppContext appContext = AppContext.getInstance(); 
	appContext.setContextPath(contextPath);
	appContext.setAppProperties(appProperties);
	this.appProperties = AppContext.getInstance().getAppProperties();

	org.zkoss.zhtml.Text textoAsterisco = new org.zkoss.zhtml.Text(" *");
	
	this.getIdLblRecibirInscripcionTipoId().getChildren().clear();
	org.zkoss.zhtml.Text textoCuestion = new org.zkoss.zhtml.Text(this.appProperties.getProperty("Tipo_Documento"));
	org.zkoss.zhtml.Span spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");	
	textoAsterisco.setParent(spanCuestion);	
	textoCuestion.setParent(this.getIdLblRecibirInscripcionTipoId());
	spanCuestion.setParent(this.getIdLblRecibirInscripcionTipoId());
	
	this.setMensajeTipoIdentificacion(appProperties.getProperty("identificacion_placeholder"));
		
	this.getIdLblRecibirInscripcionIdenfiticacion().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("identificacion"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdLblRecibirInscripcionIdenfiticacion());
	spanCuestion.setParent(this.getIdLblRecibirInscripcionIdenfiticacion());
	
	// Agregar placeholder para identificación
	this.getTxtRecibirInscripcionIdentificacion().setPlaceholder(appProperties.getProperty("identificacion_placeholder"));
	
	this.getIdLblRecibirInscripcionGenero().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Genero"));	
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdLblRecibirInscripcionGenero());
	spanCuestion.setParent(this.getIdLblRecibirInscripcionGenero());
		
	this.getIdLblRecibirInscripcionNombres().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Nombres"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdLblRecibirInscripcionNombres());
	spanCuestion.setParent(this.getIdLblRecibirInscripcionNombres());
	
	this.getIdLblRecibirInscripcionPrimer_Apellido().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Primer_Apellido"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdLblRecibirInscripcionPrimer_Apellido());
	spanCuestion.setParent(this.getIdLblRecibirInscripcionPrimer_Apellido());
	
	this.getIdLblRecibirInscripcionSegundo_Apellido().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text("Segundo Apellido");
	if(this.getLanguage().equalsIgnoreCase("es")) {
		textoCuestion.setParent(this.getIdLblRecibirInscripcionSegundo_Apellido());
		this.getIdLblRecibirInscripcionSegundo_ApellidoEstado().setVisible(true);
	}else if(this.getLanguage().equalsIgnoreCase("en")) {
		this.getIdLblRecibirInscripcionSegundo_ApellidoEstado().setVisible(false);
	}


	this.getIdLblRecibirInscripcionTelefono_movil().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Telefono_movil"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdLblRecibirInscripcionTelefono_movil());
	spanCuestion.setParent(this.getIdLblRecibirInscripcionTelefono_movil());
	
	this.getIdLblRecibirInscripcionDireccion_Electronica().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Direccion_Electronica"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdLblRecibirInscripcionDireccion_Electronica());
	spanCuestion.setParent(this.getIdLblRecibirInscripcionDireccion_Electronica());
	
	this.getIdLblRecibirInscripcionDireccion_Contacto().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Direccion_Contacto"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdLblRecibirInscripcionDireccion_Contacto());
	spanCuestion.setParent(this.getIdLblRecibirInscripcionDireccion_Contacto());
	
	this.getIdLblRecibirInscripcionCiudad().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Ciudad"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdLblRecibirInscripcionCiudad());
	spanCuestion.setParent(this.getIdLblRecibirInscripcionCiudad());
	
	this.getLbxRecibirInscripcionSexo().getChildren().clear();
	Listitem item = new Listitem();
	item.setValue("0");item.setLabel(this.getLanguage().equalsIgnoreCase("en")? "Select": "Seleccione"); item.setSelected(true);
	this.getLbxRecibirInscripcionSexo().appendChild(item);
	item = new Listitem();
	item.setValue("F");item.setLabel(appProperties.getProperty("Femenino"));
	this.getLbxRecibirInscripcionSexo().appendChild(item);
	item = new Listitem();
	item.setValue("M");item.setLabel(appProperties.getProperty("Masculino"));
	this.getLbxRecibirInscripcionSexo().appendChild(item);
	
	
	this.getIdLblRecibirInscripcionGrado_escolaridad().getChildren().clear();
	this.getLbxRecibirInscripcionEscolaridad().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Grado_escolaridad"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdLblRecibirInscripcionGrado_escolaridad());
	spanCuestion.setParent(this.getIdLblRecibirInscripcionGrado_escolaridad());
	item = new Listitem();
	item.setValue("0");item.setLabel(this.getLanguage().equalsIgnoreCase("en")? "Select": "Seleccione"); item.setSelected(true);
	this.getLbxRecibirInscripcionEscolaridad().appendChild(item);
	item = new Listitem();
	item.setValue("PRI");item.setLabel(appProperties.getProperty("Grado_Primaria"));
	this.getLbxRecibirInscripcionEscolaridad().appendChild(item);
	item = new Listitem();
	item.setValue("SEC");item.setLabel(appProperties.getProperty("Grado_Secundaria"));
	this.getLbxRecibirInscripcionEscolaridad().appendChild(item);
	item = new Listitem();
	item.setValue("TEC");item.setLabel(appProperties.getProperty("Grado_Tecnica"));
	this.getLbxRecibirInscripcionEscolaridad().appendChild(item);
	item = new Listitem();
	item.setValue("PRO");item.setLabel(appProperties.getProperty("Grado_Profesional"));
	this.getLbxRecibirInscripcionEscolaridad().appendChild(item);
	item = new Listitem();
	item.setValue("ESP");item.setLabel(appProperties.getProperty("Grado_Especializacion"));
	this.getLbxRecibirInscripcionEscolaridad().appendChild(item);
	item = new Listitem();
	item.setValue("MAE");item.setLabel(appProperties.getProperty("Grado_Maestria"));
	this.getLbxRecibirInscripcionEscolaridad().appendChild(item);
	item = new Listitem();
	item.setValue("DOC");item.setLabel(appProperties.getProperty("Grado_Doctorado"));
	this.getLbxRecibirInscripcionEscolaridad().appendChild(item);
	item = new Listitem();
	item.setValue("POS");item.setLabel(appProperties.getProperty("Grado_Postdoctorado"));
	this.getLbxRecibirInscripcionEscolaridad().appendChild(item);
	
	this.getIdLblRecibirInscripcionOcupacion().getChildren().clear();
	this.getLbxRecibirInscripcionOcupacion().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Ocupacion"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdLblRecibirInscripcionOcupacion());
	spanCuestion.setParent(this.getIdLblRecibirInscripcionOcupacion());
	item = new Listitem();
	item.setValue("0");item.setLabel(this.getLanguage().equalsIgnoreCase("en")? "Select": "Seleccione"); item.setSelected(true);
	this.getLbxRecibirInscripcionOcupacion().appendChild(item);
	item = new Listitem();
	item.setValue("EST");item.setLabel(appProperties.getProperty("Ocupacion_Estudiante"));
	this.getLbxRecibirInscripcionOcupacion().appendChild(item);
	item = new Listitem();
	item.setValue("EMP");item.setLabel(appProperties.getProperty("Ocupacion_Empleado"));
	this.getLbxRecibirInscripcionOcupacion().appendChild(item);
	item = new Listitem();
	item.setValue("IND");item.setLabel(appProperties.getProperty("Ocupacion_Independiente"));
	this.getLbxRecibirInscripcionOcupacion().appendChild(item);
	item = new Listitem();
	item.setValue("DES");item.setLabel(appProperties.getProperty("Ocupacion_Desempleado"));
	this.getLbxRecibirInscripcionOcupacion().appendChild(item);
	
	this.getIdLblRecibirInscripcionPrograma().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Programa"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdLblRecibirInscripcionPrograma());
	spanCuestion.setParent(this.getIdLblRecibirInscripcionPrograma());
	
	this.getIdLblRecibirInscripcionModalidadAsistencia().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Modalidad_asistencia"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdLblRecibirInscripcionModalidadAsistencia());
	spanCuestion.setParent(this.getIdLblRecibirInscripcionModalidadAsistencia());
	
	
	this.getIdLblRecibirInscripcionCargo().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Cargo"));
	textoCuestion.setParent(this.getIdLblRecibirInscripcionCargo());

	this.getIdLblRecibirInscripcionEmpresaRazonSocial().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("EmpresaRazonSocial"));
	textoCuestion.setParent(this.getIdLblRecibirInscripcionEmpresaRazonSocial());
	
	this.getIdLblRecibirInscripcionFacturaEmpresa().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("FacturaEmpresa"));
	textoCuestion.setParent(this.getIdLblRecibirInscripcionFacturaEmpresa());
	
	this.getIdLblRecibirInscripcionAceptoTratamientoDatos().getChildren().clear();
	org.zkoss.zhtml.A ahref = new org.zkoss.zhtml.A();
	ahref.setHref(IConstantes.URL_POLITICAS_TRATAMIENTO_PERSONALES);
	ahref.appendChild(new org.zkoss.zhtml.Text(this.getLanguage().equalsIgnoreCase("en")? "here" : " aquí"));
	ahref.setStyle("color:#4B0082");
	ahref.setTarget("_blank");
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("AceptoTratamientoDatos"));	
	textoCuestion.setParent(this.getIdLblRecibirInscripcionAceptoTratamientoDatos());
	ahref.setParent(this.getIdLblRecibirInscripcionAceptoTratamientoDatos());
	textoCuestion = new org.zkoss.zhtml.Text(")");	
	textoCuestion.setParent(this.getIdLblRecibirInscripcionAceptoTratamientoDatos());

	this.getIdLblRecibirInscripcionEducacionContinuada().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Educacion_Continua"));
	textoCuestion.setParent(this.getIdLblRecibirInscripcionEducacionContinuada());
	
	this.getIdLblRecibirInscripcionFormularioInscripcion().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Formulario_inscripcion"));
	textoCuestion.setParent(this.getIdLblRecibirInscripcionFormularioInscripcion());
	
	this.getIdLblRecibirInscripcionimg().getChildren().clear();
	this.getIdLblRecibirInscripcionimg().setAlt(this.getLanguage().equalsIgnoreCase("en")? "Personal Data" : "Datos Personales");
		
	this.getIdBtnRecibirInscripcionInscribir().getChildren().clear();
	this.getIdBtnRecibirInscripcionInscribirCena().getChildren().clear();
	this.getIdBtnRecibirInscripcionInscribir().setLabel(appProperties.getProperty("inscribirme"));
	this.getIdBtnRecibirInscripcionInscribirCena().setLabel(appProperties.getProperty("inscribirme"));
	
	this.getIdBtnRecibirInscripcionCondiciones().getChildren().clear();
	ahref = new org.zkoss.zhtml.A();
	ahref.setHref("http://continua.konradlorenz.edu.co/condiciones-de-los-programas-de-formacion-continua.html");
	ahref.appendChild(new org.zkoss.zhtml.Text(this.getLanguage().equalsIgnoreCase("en")? " here" : " aquí"));
	ahref.setStyle("color:#4B0082");
	ahref.setTarget("_blank");
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("condiciones"));	
	textoCuestion.setParent(this.getIdBtnRecibirInscripcionCondiciones());
	ahref.setParent(this.getIdBtnRecibirInscripcionCondiciones());
	textoCuestion = new org.zkoss.zhtml.Text(")");	
	textoCuestion.setParent(this.getIdBtnRecibirInscripcionCondiciones());
	
	//this.getCbxRecibirInscripcionUbicacion().getChildren().clear();
	this.getCbxRecibirInscripcionUbicacion().setPlaceholder(this.getLanguage().equalsIgnoreCase("en")? "select or type" : "seleccione o digite");
	
	//this.getIdCbxRecibirInscripcionPatrocinador().getChildren().clear();
	this.getIdCbxRecibirInscripcionPatrocinador().setPlaceholder(this.getLanguage().equalsIgnoreCase("en")? "Type business name (Does not appear? Press 'new')": "Digite razón social (¿No aparece? presione 'nuevo')");
	
	this.getIdBtnRecibirInscripcionNuevoPatrocinador().getChildren().clear();
	this.getIdBtnRecibirInscripcionNuevoPatrocinador().setLabel(this.getLanguage().equalsIgnoreCase("en")? "New" : "Nuevo");
	 
	this.getIdLblRecibirInscripcionDatosPersonales().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(this.getLanguage().equalsIgnoreCase("en")? "Personal Data" : "Datos Personales");
	textoCuestion.setParent(this.getIdLblRecibirInscripcionDatosPersonales());
	
	this.getIdLblFormPagosImprimirOrden().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("FormPagosImprimirOrden"));
	textoCuestion.setParent(this.getIdLblFormPagosImprimirOrden());
	
	this.getIdLblFormPagosLinea().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("FormPagosLinea"));
	textoCuestion.setParent(this.getIdLblFormPagosLinea());
	
	this.getIdLblFormPagosListadoPrincipal().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("FormPagosListadoPrincipal"));
	textoCuestion.setParent(this.getIdLblFormPagosListadoPrincipal());
	
	this.getIdLblFormPagosModenaPago().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("FormPagosModenaPago"));
	textoCuestion.setParent(this.getIdLblFormPagosModenaPago());
	
	this.getIdLbxInscripcionEduContMonedaMoneda().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Moneda"));
	textoCuestion.setParent(this.getIdLbxInscripcionEduContMonedaMoneda());
	
	this.getIdLblRecibirInscripcionDatosPago().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(this.getLanguage().equalsIgnoreCase("en")? "Payments" : "Pagos");
	textoCuestion.setParent(this.getIdLblRecibirInscripcionDatosPago());
	
	this.getIdBtnRecibirInscripcionImprimirOrden().getChildren().clear();
	this.getIdBtnRecibirInscripcionImprimirOrden().setLabel(this.getLanguage().equalsIgnoreCase("en")? "Print Order" :"Imprimir Orden");
	
	this.getIdBtnRecibirInscripcionPagar().getChildren().clear();
	this.getIdBtnRecibirInscripcionPagar().setLabel(this.getLanguage().equalsIgnoreCase("en")? "Pay online" : "Pagar en línea");
	
	this.getIdBtnRecibirInscripcionVolver().getChildren().clear();
	this.getIdBtnRecibirInscripcionVolver().setLabel(this.getLanguage().equalsIgnoreCase("en")? "Go back to start" : "Volver a inicio");
	
	this.getIdRecibirInscripcionEduContGrdPrincipal().getColumns().getChildren().clear();
	Column column = new Column();
	column.setLabel(appProperties.getProperty("Item")); column.setStyle("max-width:80px; width:70px; min-width:60px; color:white; font-family:lato,arial,verdana;");
	column.setAlign("center");
	this.getIdRecibirInscripcionEduContGrdPrincipal().getColumns().appendChild(column);
	column = new Column();
	column.setLabel(appProperties.getProperty("Recibo")); column.setStyle("max-width:80px; width:100px; padding: 0px !important; padding-top: 7px !important; color:white; font-family:lato,arial,verdana;");
	this.getIdRecibirInscripcionEduContGrdPrincipal().getColumns().appendChild(column);
	column = new Column();
	column.setLabel(appProperties.getProperty("Periodo")); column.setStyle("max-width:80px; color:white; font-family:lato,arial,verdana;");
	this.getIdRecibirInscripcionEduContGrdPrincipal().getColumns().appendChild(column);
	column = new Column();
	column.setLabel(appProperties.getProperty("Descripcion")); column.setStyle("color:white; font-family:lato,arial,verdana; width: 250px;");
	this.getIdRecibirInscripcionEduContGrdPrincipal().getColumns().appendChild(column);
	column = new Column();
	column.setLabel(appProperties.getProperty("Fecha_Limite")); column.setStyle("max-width:120px; padding: 0px !important; padding-top: 7px !important; color:white; font-family:lato,arial,verdana;");
	column.setAlign("center");
	this.getIdRecibirInscripcionEduContGrdPrincipal().getColumns().appendChild(column);
	column = new Column();
	column.setLabel(appProperties.getProperty("Valor")); column.setStyle("width: 80px; color:white; font-family:lato,arial,verdana;");
	column.setAlign("center");
	this.getIdRecibirInscripcionEduContGrdPrincipal().getColumns().appendChild(column);
	
	this.getIdBtnRecibirInscripcionGuardarPatrocinadorExistente().getChildren().clear();
	this.getIdBtnRecibirInscripcionGuardarPatrocinadorExistente().setLabel(this.getLanguage().equalsIgnoreCase("en")?  "Finish Registration" :"Finalizar inscripción");
	this.getIdBtnRecibirInscripcionCancelarPatrocinadorExistente().getChildren().clear();
	this.getIdBtnRecibirInscripcionCancelarPatrocinadorExistente().setLabel(this.getLanguage().equalsIgnoreCase("en")? "Add another assistant" :"Agregar otro asistente");
	
	this.getIdLblRecibirInscripcionDatosEmpresa().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(this.getLanguage().equalsIgnoreCase("en")? "Company data" : "Datos Empresa");
	textoCuestion.setParent(this.getIdLblRecibirInscripcionDatosEmpresa());
	
	this.getIdDivRecibirInscripcionNewPatrocinadorTipoIdentificacion().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Tipo_Identificacion"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorTipoIdentificacion());
	spanCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorTipoIdentificacion());

	this.getIdDivRecibirInscripcionNewPatrocinadorNumeroIdentificacion().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("PatrocinadorNumeroIdentificacion"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorNumeroIdentificacion());
	spanCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorNumeroIdentificacion());
	
	this.getIdDivRecibirInscripcionNewPatrocinadorNombreRazonSocial().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Nombre_Razon_social"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorNombreRazonSocial());
	spanCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorNombreRazonSocial());
	
	this.getIdDivRecibirInscripcionNewPatrocinadorPrimerApellido().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Primer_Apellido"));
	textoCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorPrimerApellido());
	
	this.getIdDivRecibirInscripcionNewPatrocinadorSegundoApellido().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Segundo_Apellido"));
	textoCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorSegundoApellido());
	
	this.getIdDivRecibirInscripcionNewPatrocinadorDireccionContacto().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Direccion_electronica_contacto"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorDireccionContacto());
	spanCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorDireccionContacto());
	
	this.getIdDivRecibirInscripcionNewPatrocinadorTelefonoContacto().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Telefono_contacto"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorTelefonoContacto());
	spanCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorTelefonoContacto());
	
	this.getIdDivRecibirInscripcionNewPatrocinadorNombreContacto().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Nombre_contacto"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorNombreContacto());
	spanCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorNombreContacto());
	
	this.getIdDivRecibirInscripcionNewPatrocinadorArchivoRut().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("Archivo_Rut"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorArchivoRut());
	spanCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorArchivoRut());
	
	this.getIdBtnRecibirInscripcionBuscarRut().getChildren().clear();
	this.getIdBtnRecibirInscripcionBuscarRut().setLabel(appProperties.getProperty("Buscar"));
	
	this.getIdDivRecibirInscripcionNewPatrocinadorCondiciones().getChildren().clear();
	ahref = new org.zkoss.zhtml.A();
	ahref.setHref("http://continua.konradlorenz.edu.co/condiciones-de-los-programas-de-formacion-continua.html");
	ahref.appendChild(new org.zkoss.zhtml.Text(this.getLanguage().equalsIgnoreCase("en")? " here" : " aquí"));
	ahref.setStyle("color:#4B0082");
	ahref.setTarget("_blank");
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("condiciones"));	
	textoCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorCondiciones());
	ahref.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorCondiciones());
	textoCuestion = new org.zkoss.zhtml.Text(")");	
	textoCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorCondiciones());
	
	this.getIdDivRecibirInscripcionNewPatrocinadorAceptoTratamientoDatos().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("AceptoTratamientoDatos"));
	textoCuestion.setParent(this.getIdDivRecibirInscripcionNewPatrocinadorAceptoTratamientoDatos());
	
	this.getIdBtnRecibirInscripcionGuardarPatrocinador().getChildren().clear();
	this.getIdBtnRecibirInscripcionGuardarPatrocinador().setLabel(this.getLanguage().equalsIgnoreCase("en")?  "Finish Registration" :"Finalizar inscripción");
	this.getIdBtnRecibirInscripcionCancelarPatrocinador().getChildren().clear();
	this.getIdBtnRecibirInscripcionCancelarPatrocinador().setLabel(this.getLanguage().equalsIgnoreCase("en")?  "Return" :"Regresar");
	
	this.getIdDivRecibirInscripcionMensajeCulminarInscripcionInscripcionParticipante().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("InscripcionParticipante"));
	textoCuestion.setParent(this.getIdDivRecibirInscripcionMensajeCulminarInscripcionInscripcionParticipante());

	this.getIdDivRecibirInscripcionMensajeCulminarInscripcionFinalTexto().getChildren().clear();
	ahref = new org.zkoss.zhtml.A();
	ahref.setHref("mailto: educacion_continua@konradlorenz.edu.co");
	ahref.appendChild(new org.zkoss.zhtml.Text("educacion_continua@konradlorenz.edu.co."));
	ahref.setStyle("color:#4B0082");
	ahref.setTarget("_blank");
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("FinalTexto"));
	textoCuestion.setParent(this.getIdDivRecibirInscripcionMensajeCulminarInscripcionFinalTexto());
	ahref.setParent(this.getIdDivRecibirInscripcionMensajeCulminarInscripcionFinalTexto());
	
	this.getIdDivRecibirInscripcionMensajeCulminarInscripcionInfoCena().getChildren().clear();
	ahref = new org.zkoss.zhtml.A();
	ahref.setHref("mailto: educacion_continua@konradlorenz.edu.co");
	ahref.appendChild(new org.zkoss.zhtml.Text("educacion_continua@konradlorenz.edu.co."));
	ahref.setStyle("color:#4B0082");
	ahref.setTarget("_blank");
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("mensajeCena"));
	textoCuestion.setParent(this.getIdDivRecibirInscripcionMensajeCulminarInscripcionInfoCena());
	ahref.setParent(this.getIdDivRecibirInscripcionMensajeCulminarInscripcionInfoCena());
	
	this.getIdBtnRecibirInscripcionNuevoInscrito().getChildren().clear();
	this.getIdBtnRecibirInscripcionNuevoInscrito().setLabel(appProperties.getProperty("Agregar_otro_asistente"));
	
	this.getIdLblRecibirInscripcionCasillaEstudiante().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("LabelCeldaEstudiante"));
	textoCuestion.setParent(this.getIdLblRecibirInscripcionCasillaEstudiante());
	
	this.getIdLblRecibirInscripcionCasillaCena().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("LabelInscripcionEvento"));
	textoAsterisco = new org.zkoss.zhtml.Text(" *");
	spanCuestion = new org.zkoss.zhtml.Span();
	spanCuestion.setStyle("color:white; font-size:17px; font-weight: bold;");
	textoAsterisco.setParent(spanCuestion);
	textoCuestion.setParent(this.getIdLblRecibirInscripcionCasillaCena());
	spanCuestion.setParent(this.getIdLblRecibirInscripcionCasillaCena());
	
	this.getIdLblRecibirInscripcionUniversidadProcedencia().getChildren().clear();
	textoCuestion = new org.zkoss.zhtml.Text(appProperties.getProperty("UniversidadEvento"));
	textoCuestion.setParent(this.getIdLblRecibirInscripcionUniversidadProcedencia());
	
	this.getChbxRecibirInscripcionCasillaEstudianteRadio1().getChildren().clear();
	this.getChbxRecibirInscripcionCasillaEstudianteRadio1().setLabel(appProperties.getProperty("Estudiante"));
	this.getChbxRecibirInscripcionCasillaEstudianteRadio2().getChildren().clear();
	this.getChbxRecibirInscripcionCasillaEstudianteRadio2().setLabel(appProperties.getProperty("Particular"));
	this.getChbxRecibirInscripcionCasillaCenaRadio1().getChildren().clear();
	this.getChbxRecibirInscripcionCasillaCenaRadio1().setLabel(appProperties.getProperty("Si"));
	this.getChbxRecibirInscripcionCasillaCenaRadio2().getChildren().clear();
	this.getChbxRecibirInscripcionCasillaCenaRadio2().setLabel(appProperties.getProperty("No"));
}

public void ocultarCamposSegunTipo() {
	
	String tipo = Executions.getCurrent().getDesktop().getSession().getAttribute("tipo").toString();
	log.info("Funcion campos idioma "+ tipo);
	if(tipo.equalsIgnoreCase("e")) {
		this.getIdLblRecibirInscripcionDireccion_Contacto().setVisible(false);
		this.getTxtRecibirInscripcionDireccionContacto().setVisible(false);
		this.getIdLblRecibirInscripcionCiudad().setVisible(false);
		this.getCbxRecibirInscripcionUbicacion().setVisible(false);
		this.getIdLblRecibirInscripcionGrado_escolaridad().setVisible(false);
		this.getLbxRecibirInscripcionEscolaridad().setVisible(false);
		this.getIdLblRecibirInscripcionOcupacion().setVisible(false);
		this.getLbxRecibirInscripcionOcupacion().setVisible(false);
		this.getIdLblRecibirInscripcionCargo().setVisible(false);
		this.getIdTxtRecibirInscripcionCargo().setVisible(false);
		this.getIdLblRecibirInscripcionEmpresaRazonSocial().setVisible(false);
		this.getIdCbxRecibirInscripcionPatrocinador().setVisible(false);
		this.getIdBtnRecibirInscripcionNuevoPatrocinador().setVisible(false);
		this.getIdLblRecibirInscripcionFacturaEmpresa().setVisible(false);
		this.getChbxRecibirInscripcionPatrocinado().setVisible(false);
		this.getIdBtnRecibirInscripcionInscribir().setVisible(false);
		this.getIdDivRecibirInscripcionMensajeCulminarInscripcionInfoCena().setVisible(true);
		this.getIdDivRecibirInscripcionMensajeCulminarInscripcionCena().setVisible(true);
	}else if(tipo.equalsIgnoreCase("c")) {
		this.getIdDivRecibirInscripcionCasillaCenaVisible().setVisible(false);
		this.getIdDivRecibirInscripcionUniversidadProcedencia().setVisible(false);
		this.getIdDivRecibirInscripcionCasillaEstudiante().setVisible(false);
		this.getIdBtnRecibirInscripcionInscribirCena().setVisible(false);
		this.getIdDivRecibirInscripcionMensajeCulminarInscripcionInfoCena().setVisible(false);
		this.getIdDivRecibirInscripcionMensajeCulminarInscripcionCena().setVisible(false);
	}
	
}

public Properties getProperties(String pathProperties){
	Properties properties;
    try {     
    	FileInputStream file = new FileInputStream(pathProperties);
        properties = new Properties();
        properties.load(file);
        file.close();
        
        return properties;
    } catch (FileNotFoundException e) {
    	e.printStackTrace();
    	log.error(e);
    	return null;
    } catch (IOException e) {
    	e.printStackTrace();
    	log.error(e);
    	return null;
    }
} 

public void onSetProgramaAcademicoItem(){
	
	if(this.getProgramaAcademico()!=null){
		if(this.getProgramaAcademico().getIdPrograma()!=null ){
			if(!this.getProgramaAcademico().getIdPrograma().equalsIgnoreCase("-1") ){
				Listbox listaProgramas = this.getLbxRecibirInscripcionPrograma();
				boolean encontrado = false;
				for (Listitem item : listaProgramas.getItems()){
					if(item.getValue().toString().equalsIgnoreCase(this.getProgramaAcademico().getIdPrograma())){
						listaProgramas.setSelectedItem(item);
						listaProgramas.setDisabled(true);
						encontrado = true;
					}
				}
				
				if(!encontrado) {
					this.deshabilitarCamposFormularioPersona();
				}
			} else {
				this.deshabilitarCamposFormularioPersona();
			}
		}
	}
}



public Map<String, Object> getConceptoDescuentoFuncionario(){
	Map<String, Object> mapaConceptoCausa = new HashMap<String, Object>();
	try{
		mapaConceptoCausa.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
		mapaConceptoCausa.put("parametro",IConstantes.DESCUENTO_FUNCIONARIO_EDUCACION_CONTINUA);
		String valor = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaConceptoCausa);
		String concepto = valor.substring(0, valor.indexOf("-"));
		String causa = valor.substring(valor.indexOf("-")+1,valor.length());
		
		mapaConceptoCausa.put("CONCEPTO_NOTA", concepto);
		mapaConceptoCausa.put("CAUSA_NOTA", causa);
		
		mapaConceptoCausa.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
		mapaConceptoCausa.put("parametro", IConstantes.PORCENTAJE_DESCUENTO_FUNCIONARIO_EDUCACION_CONTINUA);
		valor = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaConceptoCausa);
		
		mapaConceptoCausa.put("PORCENTAJE", valor);
		
		mapaConceptoCausa.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
		mapaConceptoCausa.put("parametro", IConstantes.INCREMENTO_GRUPO_ORDEN_EDUCACION_CONTINUADA);
		valor = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaConceptoCausa);
		
		mapaConceptoCausa.put("INCREMENTO_GRUPO_ORDEN", valor);
		
		mapaConceptoCausa.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
		mapaConceptoCausa.put("parametro", IConstantes.FUENTE_FUNCION_DESCUENTO_EDUCACION_CONTINUADA);
		valor = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaConceptoCausa);
		
		mapaConceptoCausa.put("FUENTE_FUNCION", valor);
		
	}catch(Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
	}
	
	return mapaConceptoCausa;
}

public Map<String, Object> getConceptoDescuentoEstudiante(){
	Map<String, Object> mapaConceptoCausa = new HashMap<String, Object>();
	try{
		
		mapaConceptoCausa.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
		mapaConceptoCausa.put("parametro",IConstantes.DESCUENTO_ESTUDIANTE_EDUCACION_CONTINUA);
		String valor = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaConceptoCausa);
		String concepto = valor.substring(0, valor.indexOf("-"));
		String causa = valor.substring(valor.indexOf("-")+1,valor.length());
		
		mapaConceptoCausa.put("CONCEPTO_NOTA", concepto);
		mapaConceptoCausa.put("CAUSA_NOTA", causa);
		
		mapaConceptoCausa.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
		mapaConceptoCausa.put("parametro", IConstantes.PORCENTAJE_DESCUENTO_ESTUDIANTE_EDUCACION_CONTINUA);
		valor = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaConceptoCausa);
		
		mapaConceptoCausa.put("PORCENTAJE", valor);
		
		mapaConceptoCausa.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
		mapaConceptoCausa.put("parametro", IConstantes.INCREMENTO_GRUPO_ORDEN_EDUCACION_CONTINUADA);
		valor = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaConceptoCausa);
		
		mapaConceptoCausa.put("INCREMENTO_GRUPO_ORDEN", valor);
		
		
		mapaConceptoCausa.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
		mapaConceptoCausa.put("parametro", IConstantes.FUENTE_FUNCION_DESCUENTO_EDUCACION_CONTINUADA);
		valor = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaConceptoCausa);
		
		mapaConceptoCausa.put("FUENTE_FUNCION", valor);
		
		
		
	}catch(Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
	}
	
	return mapaConceptoCausa;
}

public Map<String, Object> getConceptoDescuentoEgresado(){
	Map<String, Object> mapaConceptoCausa = new HashMap<String, Object>();
	try{
		mapaConceptoCausa.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
		mapaConceptoCausa.put("parametro",IConstantes.DESCUENTO_EGRESADO_EDUCACION_CONTINUA);
		String valor = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaConceptoCausa);
		String concepto = valor.substring(0, valor.indexOf("-"));
		String causa = valor.substring(valor.indexOf("-")+1,valor.length());
		
		mapaConceptoCausa.put("CONCEPTO_NOTA", concepto);
		mapaConceptoCausa.put("CAUSA_NOTA", causa);
		
		mapaConceptoCausa.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
		mapaConceptoCausa.put("parametro", IConstantes.PORCENTAJE_DESCUENTO_EGRESADO_EDUCACION_CONTINUA);
		valor = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaConceptoCausa);
		
		mapaConceptoCausa.put("PORCENTAJE", valor);
		
		mapaConceptoCausa.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
		mapaConceptoCausa.put("parametro", IConstantes.INCREMENTO_GRUPO_ORDEN_EDUCACION_CONTINUADA);
		valor = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaConceptoCausa);
		
		mapaConceptoCausa.put("INCREMENTO_GRUPO_ORDEN", valor);
		
		mapaConceptoCausa.put("componente", IConstantes.ABREVIATURA_COMPONENTE_CXC);
		mapaConceptoCausa.put("parametro", IConstantes.FUENTE_FUNCION_DESCUENTO_EDUCACION_CONTINUADA);
		valor = (String)ParametrizacionFac.getFacade().obtenerRegistro("getValorParametro", mapaConceptoCausa);
		
		mapaConceptoCausa.put("FUENTE_FUNCION", valor);
		
	}catch(Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
	}
	
	return mapaConceptoCausa;
}

public String getPeriodoEducacionContinuada(){
	String periodo = "";
	try{
		List<PeriodoFacturacion> periodos;
		Listbox selectProgramas = this.getLbxRecibirInscripcionPrograma();
		Listitem item = selectProgramas.getSelectedItem();
		ProgramaAcademico programa = ReciboConsignacionHelper.getHelper().getDatosProgramaAcademico(item.getValue().toString());
		
		if (programa!=null) {
			if(programa.getPeriodo()!=null ){
				if(!programa.getPeriodo().equals("")){
					periodo = programa.getPeriodo().trim();
				} else{
					periodos = ReciboConsignacionHelper.getHelper().getPeriodoEducacionContinuada();
					if(periodos!=null){
						if(periodos.size()>0){
							periodo = periodos.get(0).getPeriodo().trim();
						}
					}
				}
			}else{
				periodos = ReciboConsignacionHelper.getHelper().getPeriodoEducacionContinuada();
				if(periodos!=null){
					if(periodos.size()>0){
						periodo = periodos.get(0).getPeriodo().trim();
					}
				}
			}
		} else{
			periodos = ReciboConsignacionHelper.getHelper().getPeriodoEducacionContinuada();
			if(periodos!=null){
				if(periodos.size()>0){
					periodo = periodos.get(0).getPeriodo().trim();
				}
			}
		}
		
		
	}catch(Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
	}
	
	return periodo;
}

public String getCodigoFacultadContinuada(String idPrograma){
	String codigoFacultad ="";
	try{
		List<Facultad> facultades = ReciboConsignacionHelper.getHelper().getFacultad(idPrograma);
		if(facultades!=null){
			if(facultades.size()>0){
				codigoFacultad = facultades.get(0).getCodigoFacultad();
			}
		}
		
	}catch(Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
		
	}
	
	return codigoFacultad;
}


public void onGuardarNuevoPatrocinador() {
	
	try {
	
	Patrocinador personaPatrocinador = new Patrocinador();
	TipoIdentificacion tipoId = new TipoIdentificacion();
	
	if (!this.getIdDivRecibirInscripcionNewPatrocinador().isVisible() 
			&& this.getIdCbxRecibirInscripcionPatrocinador() != null) {
		if(this.getIdCbxRecibirInscripcionPatrocinador().getSelectedItem()!=null) {
			Comboitem comboItem = this.getIdCbxRecibirInscripcionPatrocinador().getSelectedItem();
			
			if(!comboItem.getValue().equals("0")) {
				Persona persona = (Persona)comboItem.getAttribute("PERSONA");
				tipoId.setTipoIdentificacion(persona.getTipoIdentificacion().getTipoIdentificacion());
				personaPatrocinador.setPersona(persona);
				personaPatrocinador.setTipoIdentificacion(tipoId);
				personaPatrocinador.setIdentificacion(persona.getIdentificacion());
				personaPatrocinador.setNombreRazonSocial(persona.getNombreRazonSocial());
				personaPatrocinador.setPrimerApellido(persona.getPrimerApellido());
				personaPatrocinador.setSegundoApellido(persona.getSegundoApellido());
				personaPatrocinador.setDireccionElectronica(persona.getDireccionElectronica()!=null?persona.getDireccionElectronica():"noregistra@dominio.com");
				personaPatrocinador.setTelefonoResidencia((persona.getTelefonoResidencia()!=null?persona.getTelefonoResidencia():"NO REGISTRA"));
				personaPatrocinador.setContactoPatrocinador(persona.getNombreRazonSocial());
				personaPatrocinador.setArchivoRut("000000000");
				personaPatrocinador.setEstado(IConstantes.ACTIVO);
				
				ParametrizacionFac.getFacade().guardarRegistro("insertPatrocinador", personaPatrocinador);	
			}
		} 		
	} else {
		log.info("Es nuevo patrocinador ...");
		this.onValidarTipoIdentificacionPatrocinador();
		Persona persona = new Persona();
		tipoId.setTipoIdentificacion(this.getIdLbxRecibirInscripcionTipoIdPatrocinador().getSelectedItem().getValue());
		personaPatrocinador.setTipoIdentificacion(tipoId);
		personaPatrocinador.setPersona(persona);
		personaPatrocinador.setIdentificacion(this.getIdIbxRecibirInscripcionNitPatrocinador().getValue().toString());
		personaPatrocinador.setNombreRazonSocial(this.getIdTxtRecibirInscripcionNombrePatrocinador().getValue());
		personaPatrocinador.setPrimerApellido(this.getIdTxtRecibirInscripcionPrimerApellidoPatrocinador().getValue());
		personaPatrocinador.setSegundoApellido(this.getIdTxtRecibirInscripcionSegundoApellidoPatrocinador().getValue());
		personaPatrocinador.setDireccionElectronica(this.getIdTxtRecibirInscripcionEmailPatrocinador().getValue());
		personaPatrocinador.setTelefonoResidencia(this.getIdTxtRecibirInscripcionTelefonoPatrocinador().getValue());
		personaPatrocinador.setContactoPatrocinador(this.getIdTxtRecibirInscripcionNombreContactoPatrocinador().getValue());
		personaPatrocinador.setEstado(IConstantes.ACTIVO);
		
		String rutaServicio = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectRutaServicioArchivos");
		File rutaTotal = this.getFileRutPatrocinador();
		File initialFile =new File(rutaTotal.toString());
		final javax.ws.rs.client.Client client = javax.ws.rs.client.ClientBuilder.newBuilder().register(MultiPartFeature.class).build();
		javax.ws.rs.client.WebTarget t = client.target(rutaServicio).path("files").path("sendFileInput");
		FileDataBodyPart filePart = new FileDataBodyPart("file",initialFile);
		filePart.setContentDisposition(FormDataContentDisposition.name("file").fileName(initialFile.getName()).build());
		FormDataMultiPart formDataMultiPart = new FormDataMultiPart();
		MultiPart multipartEntity = formDataMultiPart.field("name",initialFile.getName(),javax.ws.rs.core.MediaType.TEXT_PLAIN_TYPE).bodyPart(filePart);
		javax.ws.rs.core.Response response = t.request().post(javax.ws.rs.client.Entity.entity(multipartEntity, multipartEntity.getMediaType()));
	 
		log.info("Respuesta desde Servicio de documentos: "+response.getStatus());
		String archivoCod=response.readEntity(String.class);
		personaPatrocinador.setArchivoRut(archivoCod);
		 
		if (Integer.valueOf(this.getChbxRecibirInscripcionPatrocinado().getValue().toString())>0) {	
		Map<String,String> mapaServidor = new HashMap<String,String>();
		ParametrizacionFac.getFacade().ejecutarProcedimiento("seleccionarDatosServidorCorreo", mapaServidor);
		String correoReceptor = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectCorreoReportePatrocinador");
			
		if(mapaServidor.get("SERVIDOR_SMTP")!=null &&
			mapaServidor.get("PUERTO_SMTP")!=null &&
			mapaServidor.get("REQUIERE_TLS_SMTP")!=null &&
			mapaServidor.get("REQUIERE_AUTENTICACION_SMTP")!=null &&
			mapaServidor.get("USUARIO_AUTENTICACION_SMTP")!=null &&
			mapaServidor.get("CLAVE_AUTENTICACION_SMTP")!=null &&
			mapaServidor.get("USUARIO_REMITENTE_SMTP")!=null &&
			correoReceptor !=null){
			
				List<String> listaReceptores = new ArrayList<String>();
				listaReceptores.add(correoReceptor);
				EmailThread emailThread = new EmailThread();
				emailThread.setServidorCorreo(mapaServidor.get("SERVIDOR_SMTP"));
				emailThread.setPuertoCorreo(mapaServidor.get("PUERTO_SMTP"));
				emailThread.setEmisor(mapaServidor.get("USUARIO_REMITENTE_SMTP"));
				emailThread.setContrasena(mapaServidor.get("CLAVE_AUTENTICACION_SMTP"));
				emailThread.setAsunto(IConstantes.ASUNTO_NUEVO_PATROCINADOR);
				emailThread.setReceptores(listaReceptores);
			
				List<String> listaReceptoresCopia = new ArrayList<String>();
				emailThread.setReceptoresCopia(listaReceptoresCopia);
				emailThread.setMensaje(IConstantes.MENSAJE_NUEVO_PATROCINADOR+
					"\n"+"Tipo Identificación: "+personaPatrocinador.getTipoIdentificacion().getTipoIdentificacion()+
					"\n"+"Identificación: "+personaPatrocinador.getIdentificacion()+
					"\n"+"Nombre / Razón Social: "+personaPatrocinador.getNombreRazonSocial()+
					"\n"+"Apellidos: "+personaPatrocinador.getPrimerApellido()+
					"\n"+"Dirección electrónica: "+personaPatrocinador.getDireccionElectronica()+
					"\n"+"Teléfono contacto: "+personaPatrocinador.getTelefonoResidencia()+
					"\n"+"Contacto: "+personaPatrocinador.getContactoPatrocinador()+
					"\n"+IConstantes.MENSAJE_FINAL_PATROCINADOR);
			
				List<String> adjuntos = new ArrayList<String>();
				adjuntos.add(rutaTotal.getPath());
				emailThread.setAdjuntos(adjuntos);
				Thread thread = new Thread(emailThread);
				thread.setDaemon(true);
				thread.start();
			}
		}
		response.close();
		//initialFile.delete();
		ParametrizacionFac.getFacade().guardarRegistro("insertPatrocinador", personaPatrocinador);
		formDataMultiPart.close();
		
	}

	this.setPatrocinador(personaPatrocinador);
	
	} catch(Exception e) {
		e.printStackTrace();
		log.info(e.getMessage());
	}
}

public void onGuardarNuevoPatrocinado() {
	try {
		if (Integer.valueOf(this.getChbxRecibirInscripcionPatrocinado().getValue().toString())>0) {		
			Patrocinado patrocinado = new Patrocinado();
			patrocinado.setPersona(this.getPersona());
			PeriodoFacturacion periodo = new PeriodoFacturacion();
			periodo.setPeriodo(((ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA")).getPeriodo());
			patrocinado.setPeriodo(periodo);
			patrocinado.setPatrocinador(this.getPatrocinador());
			patrocinado.setEstado(IConstantes.ACTIVO);
			ParametrizacionFac.getFacade().guardarRegistro("insertPatrocinado", patrocinado);
		}	
	} catch(Exception e) {
		e.printStackTrace();
		log.info(e.getMessage());
	}
}

public Long getSecuenciaEstudiante(String idPrograma){
	Long secuencia = 0L;
	secuencia = ReciboConsignacionHelper.getHelper().getSecuenciaObjeto(idPrograma);
	return secuencia;
}

public Long getIdEstudiante(){
	Long idEstudiante = 0L;
	idEstudiante = ReciboConsignacionHelper.getHelper().getSecuenciaObjeto(IConstantes.NOMBRE_SECUENCIA_ESTUDIANTE);
	return idEstudiante;
}

public Long getIdOrdenes(){
	Long idOrdenes = 0L;
	idOrdenes = ReciboConsignacionHelper.getHelper().getSecuenciaObjeto(IConstantes.NOMBRE_SECUENCIA_ORDENES);
	return idOrdenes;
}

public Long getIdEstSolic(){
	Long idEstSolic = 0L;
	idEstSolic = ReciboConsignacionHelper.getHelper().getSecuenciaObjeto(IConstantes.NOMBRE_SECUENCIA_EST_SOLIC);
	return idEstSolic;
}


public String getCodigoEstudiante(){
	
	String codigoEstudiante = "";
	try{
	Listbox selectProgramas = this.getLbxRecibirInscripcionPrograma();
	Listitem item = selectProgramas.getSelectedItem();
	ProgramaAcademico programa = ReciboConsignacionHelper.getHelper().getDatosProgramaAcademico(item.getValue().toString());
	String periodo = this.getPeriodoEducacionContinuada();
	Long secuencia = this.getSecuenciaEstudiante(programa.getIdPrograma());
	DecimalFormat formatoDecimal = new DecimalFormat("000");
	String codigoFacultad = programa.getFacultad().getCodigoFacultad();	
	codigoEstudiante = codigoFacultad+programa.getIdPrograma()+periodo.substring(2,4).trim()+periodo.substring(4).trim()+ formatoDecimal.format(secuencia);
	
	log.info("codigo Fac: "+codigoFacultad);
	log.info("periodo: "+periodo);
	log.info("año: "+periodo.substring(2,4));
	log.info("sem: "+periodo.substring(4));
	log.info("id Prog: "+programa.getIdPrograma());
	}catch (Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
		
	}
	
	return codigoEstudiante;
	
}


public void onValidateForm(){
	log.info("[recibirInscripcion.onValidateForm]");
	this.onValidarTipoIdentificacion();
	this.onValidarGenero();
	this.onValidarOcupacion();
	this.onValidarEscolaridad();
	this.onValidarPrograma();
	this.onValidarModalidadAsistencia();
	if(!this.getIdDivRecibirInscripcionNewPatrocinador().isVisible()) {
		this.onValidarTerminosCondiciones();
		this.onValidarTratamientoDatos();
		this.onValidarCheckApoyoEmpresa();
	}
}

public void onValidateEventoForm() {
	this.onValidarTipoIdentificacion();
	this.onValidarTerminosCondiciones();
	this.onValidarTratamientoDatos();
	this.onValidarRadioAceptarCena();
}


public CentroCosto getCentroCostoPrograma(String idPrograma){
	CentroCosto centro = new CentroCosto();
	try{
		ProgramaAcademico programa = ReciboConsignacionHelper.getHelper().getDatosProgramaAcademico(idPrograma);
		
		CentroCosto centroParametro = programa.getCentroCosto();
		centro =(CentroCosto)ParametrizacionFac.getFacade().obtenerRegistro("selectCentroCostoBasic", centroParametro);
		
	}catch(Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
	}
	
	return centro;
}

public void generarOrdenEducacionContinuada(Persona persona){
	try{
		log.info("datos persona: "+persona.getCliente().getCliente()+" - periodo: "+persona.getCliente().getPeriodo().getPeriodo());
		ParametrizacionFac.getFacade().actualizarRegistro("generarOrdenEducacionContinuada", persona);
	}catch(Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
	}
}


public boolean validateExcepcionSaldo(Map<String, Object> mapaParametros) {
	
	try {
		String result = (String)ParametrizacionFac.getFacade().obtenerRegistro("validarExcepcionSaldoPrograma", mapaParametros);

		if (result ==null || result.equalsIgnoreCase("")) {
			return false;
		}
		
		if(result.equalsIgnoreCase("S")) {
			return true;
		}
		
	}catch(Exception e) {
		Messagebox.show(e.getMessage());
		e.printStackTrace();
	}
	
	return false;
}

public boolean onValidateSaldoPersona(Persona persona){
	boolean validacion = false;
	try{
		
		Map<String, Object> mapaParametros = new HashMap<String, Object>();
		mapaParametros.put("CENTRO_COSTO", this.getPersona().getCliente().getCentroCosto().getCentroCosto());
		mapaParametros.put("PERIODO", this.getPersona().getCliente().getPeriodo().getPeriodo());
		
		boolean excepcion = this.validateExcepcionSaldo(mapaParametros);
		
		
		BigDecimal saldo = (BigDecimal)ParametrizacionFac.getFacade().obtenerRegistro("selectValidarSaldoPersonaEduCont", persona);
		
		if(saldo== null){
			saldo = new BigDecimal(0);
		}
		
			if(saldo.intValue()>0 && !excepcion){
				validacion = false;
				}
			else{ validacion= true;}
		
	}catch(Exception e){
		Messagebox.show(e.getMessage());
		e.printStackTrace();
	}	
	return validacion;
}

public boolean onValidateConfiguracionFechasInscripcion(Map<String, Object> mapaParametros){
	boolean validacion = false;
	try{
		String confirmacion = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectValidarCCFechasInscr", mapaParametros);
			
			if(confirmacion == null){
				confirmacion = "N";
			}
		
			if(confirmacion.equalsIgnoreCase("S")){
				validacion = true;
			}
		
	}catch(Exception e){
		Messagebox.show(e.getMessage());
		e.printStackTrace();
	}
	return validacion;
}


public boolean onValidateConfiguracionEquivalencia(Map<String, Object> mapaParametros){
	boolean validacion = false;
	try{
		String confirmacion = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectValidarCCEquivalencia", mapaParametros);
			
			if(confirmacion == null){
				confirmacion = "N";
			}
		
			if(confirmacion.equalsIgnoreCase("S")){
				validacion = true;
			}
		
	}catch(Exception e){
		Messagebox.show(e.getMessage());
		e.printStackTrace();
	}
	return validacion;
}

public boolean onValidateConfiguracionBanco(Map<String, Object> mapaParametros){
	boolean validacion = false;
	try{
		String confirmacion = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectValidarCCBanco", mapaParametros);
			
			if(confirmacion == null){
				confirmacion = "N";
			}
		
			if(confirmacion.equalsIgnoreCase("S")){
				validacion = true;
			}
		
	}catch(Exception e){
		Messagebox.show(e.getMessage());
		e.printStackTrace();
	}
	return validacion;
}


public boolean onValidateGruposVencimiento(Map<String, Object> mapaParametros){
	boolean validacion = false;
	try{
		String confirmacion = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectValidarGruposVencimiento", mapaParametros);
			
			if(confirmacion == null){
				confirmacion = "N";
			}
		
			if(confirmacion.equalsIgnoreCase("S")){
				validacion = true;
			}
		
	}catch(Exception e){
		Messagebox.show(e.getMessage());
		e.printStackTrace();
	}
	return validacion;
}

public String onObtenerValorCena(){
	String valor = null;
	try{
		 String confirmacion = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectValorCena");
			
			if(confirmacion == null){
				confirmacion = "0";
			}else {
				valor = confirmacion;
			}
		
	}catch(Exception e){
		Messagebox.show(e.getMessage());
		e.printStackTrace();
	}
	return valor;
}

public Long onObtenerReferenciaCena(){
	Long valor = null;
	try{
		 String confirmacion = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectReferenciaCena");
			
			if(confirmacion == null){
				valor = 0L;
			}else {
				valor = Long.parseLong(confirmacion);
			}
		
	}catch(Exception e){
		Messagebox.show(e.getMessage());
		e.printStackTrace();
	}
	return valor;
}

public void onValidateTipoIdentificacion() {
	Listbox listaTipoIdentificacion = this.getLbxRecibirInscripcionTipoIdentificacion();
	if(	listaTipoIdentificacion.getSelectedItem().getLabel().toLowerCase().contains("extranje") || 
		listaTipoIdentificacion.getSelectedItem().getLabel().toLowerCase().contains("pasapor") ||
		listaTipoIdentificacion.getSelectedItem().getLabel().toLowerCase().contains("permanen")) {
		
		Messagebox.show(this.getMensajeTipoIdentificacion(), "Pagos Online", Messagebox.OK, Messagebox.INFORMATION);
	}
}


public void onValidatePersona(){
	log.info("[recibirInscripcion.onValidatePersona]");
	Persona personaParametro = new Persona();
	Persona personaResult = new Persona();
	Cliente cliente = new Cliente();
	ProgramaAcademico programa = (ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA");
	CentroCosto centroCosto = this.getCentroCostoPrograma(programa.getIdPrograma());
	personaParametro.setCliente(cliente);
	personaParametro.getCliente().setCentroCosto(centroCosto);
	personaParametro.setIdentificacion(this.getTxtRecibirInscripcionIdentificacion().getValue().toString());
	String tipo = Executions.getCurrent().getDesktop().getSession().getAttribute("tipo").toString();
	try{
	personaResult = (Persona)ParametrizacionFac.getFacade().obtenerRegistro("selectPersonaBasicEducacionContinuada", personaParametro);
	}catch(Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
	}
	
	if(personaResult !=null){
	if(personaResult.getNombreRazonSocial()!=null && personaResult.getSecuenciaPersona() != null){
		log.info("[personaResult id="+personaResult.getIdentificacion()+"]");
		this.setEncontroPersona(true);
		this.getTxtRecibirInscripcionNombres().setValue(personaResult.getNombreRazonSocial());
		this.getTxtRecibirInscripcionPrimerApellido().setValue(personaResult.getPrimerApellido());
		this.getTxtRecibirInscripcionSegundoApellido().setValue(personaResult.getSegundoApellido());
		this.getTxtRecibirInscripcionNombres().setDisabled(true);
		if(this.getLanguage().equalsIgnoreCase("es")) {
			this.getTxtRecibirInscripcionPrimerApellido().setDisabled(true);
			this.getTxtRecibirInscripcionSegundoApellido().setDisabled(true);
		}else if(this.getLanguage().equalsIgnoreCase("en")){
			this.getTxtRecibirInscripcionPrimerApellido().setDisabled(true);
			this.getTxtRecibirInscripcionSegundoApellido().setVisible(false);
		}

		for(Listitem item : this.getLbxRecibirInscripcionTipoIdentificacion().getItems()){
			if(item.getValue().toString().equalsIgnoreCase(personaResult.getTipoIdentificacion().getTipoIdentificacion())){
				this.getLbxRecibirInscripcionTipoIdentificacion().setSelectedItem(item);
			}
		}
		this.getLbxRecibirInscripcionTipoIdentificacion().setDisabled(true);
		
		for(Listitem item : this.getLbxRecibirInscripcionSexo().getItems()){
			if(item.getValue().toString().equalsIgnoreCase(personaResult.getSexo())){
				this.getLbxRecibirInscripcionSexo().setSelectedItem(item);
			}
		}
		this.getLbxRecibirInscripcionSexo().setDisabled(true);
		
		for(Comboitem item : this.getCbxRecibirInscripcionUbicacion().getItems()){
			if(item.getValue().toString().equalsIgnoreCase(personaResult.getPoblacionResidencia().getDepartamento().getPais().getPais().toString()+"-"
				+personaResult.getPoblacionResidencia().getDepartamento().getDepartamento().toString()+"-"
				+personaResult.getPoblacionResidencia().getPoblacion().toString())){
				this.getCbxRecibirInscripcionUbicacion().setSelectedItem(item);
			}
		}
		
		for(Listitem item : this.getLbxRecibirInscripcionEscolaridad().getItems()){
			if(item.getValue().toString().equalsIgnoreCase(personaResult.getGradoEscolaridad())){
				this.getLbxRecibirInscripcionEscolaridad().setSelectedItem(item);
			}
		}
		
		for(Listitem item : this.getLbxRecibirInscripcionOcupacion().getItems()){
			if(item.getValue().toString().equalsIgnoreCase(personaResult.getOcupacion())){
				this.getLbxRecibirInscripcionOcupacion().setSelectedItem(item);
				}
			}
		
		this.getTxtRecibirInscripcionDireccionContacto().setValue(personaResult.getDireccionResidencia());
		this.getTxtRecibirInscripcionDireccionElectronica().setValue(personaResult.getDireccionElectronica());
		this.getTxtRecibirInscripcionTelefono().setValue(personaResult.getTelefonoResidencia());
		this.getIdTxtRecibirInscripcionCargo().setValue(personaResult.getCargo());


		if(tipo.equalsIgnoreCase("e")) {
			Listbox lbxEscolaridad = this.getLbxRecibirInscripcionEscolaridad();
			Listbox lbxOcupacion = this.getLbxRecibirInscripcionOcupacion();
			lbxEscolaridad.setSelectedIndex(4);
			lbxOcupacion.setSelectedIndex(3);
		}
		
		if(tipo.equalsIgnoreCase("e")) {
			this.getTxtRecibirInscripcionUniversidadProcedencia().setValue(personaResult.getUniversidadProcedencia());
			if(personaResult.getTipoParticipante().equalsIgnoreCase("E")) {
				this.getChbxRecibirInscripcionCasillaEstudianteRadio2().setChecked(false);
				this.getChbxRecibirInscripcionCasillaEstudianteRadio1().setChecked(true);
			}else {
				this.getChbxRecibirInscripcionCasillaEstudianteRadio2().setChecked(true);
				
			}
			if(personaResult.getAceptaCena().equalsIgnoreCase("S")) {
				this.getChbxRecibirInscripcionCasillaCenaRadio2().setChecked(false);
				this.getChbxRecibirInscripcionCasillaCenaRadio1().setChecked(true);
				this.getChbxRecibirInscripcionCasillaCenaRadio1().setDisabled(true);
				this.getChbxRecibirInscripcionCasillaCenaRadio2().setDisabled(true);
			}else {
				this.getChbxRecibirInscripcionCasillaCenaRadio2().setChecked(true);
				this.getChbxRecibirInscripcionCasillaCenaRadio1().setChecked(false);
				this.getChbxRecibirInscripcionCasillaCenaRadio1().setDisabled(true);
				this.getChbxRecibirInscripcionCasillaCenaRadio2().setDisabled(true);
			}
		}
		
		this.setPersona(personaResult);
		
		}
	}else {
		this.setEncontroPersona(false);
		this.limpiarCamposFormularioPersona();
	}
}

public void onValidatePersona(String valor){
	log.info("[recibirInscripcion.onValidatePersona]");
	Persona personaParametro = new Persona();
	Persona personaResult = new Persona();
	Cliente cliente = new Cliente();
	ProgramaAcademico programa = (ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA");
	CentroCosto centroCosto = this.getCentroCostoPrograma(programa.getIdPrograma());
	personaParametro.setCliente(cliente);
	personaParametro.getCliente().setCentroCosto(centroCosto);
	personaParametro.setIdentificacion(valor);
	try{
	personaResult = (Persona)ParametrizacionFac.getFacade().obtenerRegistro("selectPersonaBasicEducacionContinuada", personaParametro);
	}catch(Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
	}
	
	if(personaResult !=null){
	if(personaResult.getNombreRazonSocial()!=null && personaResult.getSecuenciaPersona() != null){
		log.info("[personaResult id="+personaResult.getIdentificacion()+"]");
		this.getTxtRecibirInscripcionNombres().setValue(personaResult.getNombreRazonSocial());
		this.getTxtRecibirInscripcionNombres().setDisabled(true);
		this.getTxtRecibirInscripcionPrimerApellido().setValue(personaResult.getPrimerApellido());
		this.getTxtRecibirInscripcionPrimerApellido().setDisabled(true);
		this.getTxtRecibirInscripcionSegundoApellido().setValue(personaResult.getSegundoApellido());
		this.getTxtRecibirInscripcionSegundoApellido().setDisabled(true);
		for(Listitem item : this.getLbxRecibirInscripcionTipoIdentificacion().getItems()){
			if(item.getValue().toString().equalsIgnoreCase(personaResult.getTipoIdentificacion().getTipoIdentificacion())){
				this.getLbxRecibirInscripcionTipoIdentificacion().setSelectedItem(item);
			}
		}
		this.getLbxRecibirInscripcionTipoIdentificacion().setDisabled(true);
		
		for(Listitem item : this.getLbxRecibirInscripcionSexo().getItems()){
			if(item.getValue().toString().equalsIgnoreCase(personaResult.getSexo())){
				this.getLbxRecibirInscripcionSexo().setSelectedItem(item);
			}
		}
		this.getLbxRecibirInscripcionSexo().setDisabled(true);
		
		for(Comboitem item : this.getCbxRecibirInscripcionUbicacion().getItems()){
			if(item.getValue().toString().equalsIgnoreCase(personaResult.getPoblacionResidencia().getDepartamento().getPais().getPais().toString()+"-"
				+personaResult.getPoblacionResidencia().getDepartamento().getDepartamento().toString()+"-"
				+personaResult.getPoblacionResidencia().getPoblacion().toString())){
				this.getCbxRecibirInscripcionUbicacion().setSelectedItem(item);
			}
		}
		
		for(Listitem item : this.getLbxRecibirInscripcionEscolaridad().getItems()){
			if(item.getValue().toString().equalsIgnoreCase(personaResult.getGradoEscolaridad())){
				this.getLbxRecibirInscripcionEscolaridad().setSelectedItem(item);
			}
		}
		
		for(Listitem item : this.getLbxRecibirInscripcionOcupacion().getItems()){
			if(item.getValue().toString().equalsIgnoreCase(personaResult.getOcupacion())){
				this.getLbxRecibirInscripcionOcupacion().setSelectedItem(item);
				}
			}
		
		this.getTxtRecibirInscripcionDireccionContacto().setValue(personaResult.getDireccionResidencia());
		this.getTxtRecibirInscripcionDireccionElectronica().setValue(personaResult.getDireccionElectronica());
		this.getTxtRecibirInscripcionTelefono().setValue(personaResult.getTelefonoResidencia());
		this.getIdTxtRecibirInscripcionCargo().setValue(personaResult.getCargo());

		String tipo = Executions.getCurrent().getDesktop().getSession().getAttribute("tipo").toString();
		if(tipo.equalsIgnoreCase("e")) {
			this.getTxtRecibirInscripcionUniversidadProcedencia().setValue(personaResult.getUniversidadProcedencia());
			if(personaResult.getTipoParticipante().equalsIgnoreCase("E")) {
				this.getChbxRecibirInscripcionCasillaEstudianteRadio2().setChecked(false);
				this.getChbxRecibirInscripcionCasillaEstudianteRadio1().setChecked(true);
			}else {
				this.getChbxRecibirInscripcionCasillaEstudianteRadio2().setChecked(true);
				
			}
			if(personaResult.getAceptaCena().equalsIgnoreCase("S")) {
				this.getChbxRecibirInscripcionCasillaCenaRadio2().setChecked(false);
				this.getChbxRecibirInscripcionCasillaCenaRadio1().setChecked(true);
				this.getChbxRecibirInscripcionCasillaCenaRadio1().setDisabled(true);
				this.getChbxRecibirInscripcionCasillaCenaRadio2().setDisabled(true);
			}else {
				this.getChbxRecibirInscripcionCasillaCenaRadio2().setChecked(true);
			}
		}
		
		this.setPersona(personaResult);
		
		}
	}else {
		this.limpiarCamposFormularioPersona();
	}
}

public Persona getDatosFormularioPersona(){
	log.info("[recibirInscripcion.getDatosFormularioPersona]");
	String tipo = Executions.getCurrent().getDesktop().getSession().getAttribute("tipo").toString();
	TipoIdentificacion tipoIdentificacion = (TipoIdentificacion)this.getLbxRecibirInscripcionTipoIdentificacion().getSelectedItem().getAttribute("TIPO_IDENTIFICACION");
	PeriodoFacturacion periodo = new PeriodoFacturacion();
	periodo.setPeriodo(this.getPeriodoEducacionContinuada());
	String nombre = this.getTxtRecibirInscripcionNombres().getValue();
	String primerApellido = this.getTxtRecibirInscripcionPrimerApellido().getValue();
	String segundoApellido = tipo.equalsIgnoreCase("c")? this.getTxtRecibirInscripcionSegundoApellido().getValue() : "";
	String apellidos = primerApellido+((segundoApellido!=null && !segundoApellido.equalsIgnoreCase(""))?" "+segundoApellido:"");
	String telefono = this.getTxtRecibirInscripcionTelefono().getValue();
	String identificacion = this.getTxtRecibirInscripcionIdentificacion().getValue().toString();
	String sexo = this.getLbxRecibirInscripcionSexo().getSelectedItem().getValue().toString();
	String direccionElectronica = this.getTxtRecibirInscripcionDireccionElectronica().getValue();
	String direccionContacto = tipo.equalsIgnoreCase("c")?this.getTxtRecibirInscripcionDireccionContacto().getValue() : IConstantes.DIRECCION_DEFECTO;
	String gradoEscolaridad = this.getLbxRecibirInscripcionEscolaridad().getSelectedItem().getValue().toString();
	String ocupacion = this.getLbxRecibirInscripcionOcupacion().getSelectedItem().getValue().toString();
	Poblacion poblacion = tipo.equalsIgnoreCase("c")?(Poblacion)this.getCbxRecibirInscripcionUbicacion().getSelectedItem().getAttribute("POBLACION") : 
		new Poblacion(new Departamento(new Pais(Long.valueOf(IConstantes.PAIS_DEFECTO), "","",""), Long.valueOf(IConstantes.DEPARTAMENTO_DEFECTO), "",""), Long.valueOf(IConstantes.POBLACION_DEFECTO), "", "","");
	ProgramaAcademico programa = (ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA");
	CentroCosto centroCosto = this.getCentroCostoPrograma(programa.getIdPrograma());
	Integer aceptarCondiciones = Integer.valueOf(this.getChbxRecibirInscripcionAceptarCondiciones().getValue().toString());
	Integer aceptarPersonales = Integer.valueOf(this.getChbxRecibirInscripcionAceptarPersonal().getValue().toString());
	String aceptaCena = tipo.equalsIgnoreCase("c")? "N" : this.getChbxRecibirInscripcionCasillaCenaRadio2().getRadiogroup().getSelectedItem().getValue().toString();
	String tipoParticipante = tipo.equalsIgnoreCase("c")?  "P" : this.getChbxRecibirInscripcionCasillaEstudianteRadio1().getRadiogroup().getSelectedItem().getValue().toString();
	String universidadProcedencia = null;
	Textbox Uprocedencia = this.getTxtRecibirInscripcionUniversidadProcedencia();
	Uprocedencia.setConstraint("");
	if(tipo.equalsIgnoreCase("c")) {
		universidadProcedencia = "NO APLICA"; this.getTxtRecibirInscripcionUniversidadProcedencia().getValue();
	}else if(tipo.equalsIgnoreCase("e")) {
		if(Uprocedencia.getValue() == null || Uprocedencia.getValue().equalsIgnoreCase("")) {
			universidadProcedencia = "NO APLICA";		
		}else {
			universidadProcedencia = this.getTxtRecibirInscripcionUniversidadProcedencia().getValue();
		}
	}
	Cliente clienteNuevo = new Cliente();
	clienteNuevo.setPrograma(programa);
	clienteNuevo.setNombreNegocio(nombre+" "+apellidos);
	clienteNuevo.setCentroCosto(centroCosto);
	clienteNuevo.setPeriodo(periodo);
	Persona personaNuevo = new Persona();
	personaNuevo.setTipoIdentificacion(tipoIdentificacion);
	personaNuevo.setIdentificacion(identificacion);
	personaNuevo.setNombreRazonSocial(nombre);
	personaNuevo.setPrimerApellido(primerApellido);
	personaNuevo.setSegundoApellido(segundoApellido);
	personaNuevo.setTelefonoResidencia(telefono);
	personaNuevo.setDireccionResidencia(direccionContacto);
	personaNuevo.setDireccionElectronica(direccionElectronica);
	personaNuevo.setPoblacionResidencia(poblacion);
	personaNuevo.setGradoEscolaridad(gradoEscolaridad);
	personaNuevo.setSexo(sexo);
	personaNuevo.setOcupacion(ocupacion);
	personaNuevo.setAceptarCondiciones(aceptarCondiciones);
	personaNuevo.setAceptarPersonales(aceptarPersonales);
	personaNuevo.setUniversidadProcedencia(universidadProcedencia);
	personaNuevo.setAceptaCena(aceptaCena);
	personaNuevo.setTipoParticipante(tipoParticipante);
	personaNuevo.setCliente(clienteNuevo);
	
	return personaNuevo;
	
}

@SuppressWarnings("unchecked")
public void onValidatePersonaProgramaInscrito(){
	log.info("[recibirInscripcion.onValidatePersonaProgramaInscrito]");
	Persona personaNuevo = new Persona();
	personaNuevo = this.getDatosFormularioPersona();
	List<ReciboConsignacion> listaRecibosEduCont = new ArrayList<ReciboConsignacion>();

	ReciboConsignacion reciboConsignacion = new ReciboConsignacion();
	reciboConsignacion.setPersona(personaNuevo);
	
	try{
		 listaRecibosEduCont= 
				(List<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("selectRecibosConsignacionOrdenesEduCont", reciboConsignacion);
		if(listaRecibosEduCont!= null){
			if(listaRecibosEduCont.size()>0){
				this.setListaRecibosEduCont(listaRecibosEduCont);
			}
		}
		
	}catch(Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
	}
	
}

@SuppressWarnings("unchecked")
public void onValidatePersonaProgramaAnterior(){
	log.info("[recibirInscripcion.onValidatePersonaProgramaAnterior]");
	Persona personaValidada = new Persona();
	personaValidada = this.getDatosFormularioPersona();
	ReciboConsignacion reciboConsignacion = new ReciboConsignacion();
	reciboConsignacion.setPersona(personaValidada);
	try{
		List<ReciboConsignacion> listaRecibosEduContAnt= 
				(List<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("selectRecibosConsignacionOrdenesEduContAnt", reciboConsignacion);
		if(listaRecibosEduContAnt!= null){
			if(listaRecibosEduContAnt.size()>0){
				this.setListaRecibosEduContAnt(listaRecibosEduContAnt);
			}
		}
		
	}catch(Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
	}
	
}


@SuppressWarnings("unchecked")
public void onValidatePersonaProgramaPago(){
	log.info("[recibirInscripcion.onValidatePersonaProgramaPago]");
	Persona personaValidada = new Persona();
	personaValidada = this.getDatosFormularioPersona();
	ReciboConsignacion reciboConsignacion = new ReciboConsignacion();
	reciboConsignacion.setPersona(personaValidada);
	try{
		List<ReciboConsignacion> listaRecibosEduContPagas= 
				(List<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("selectRecibosConsignacionOrdenesEduContPagas", reciboConsignacion);
		if(listaRecibosEduContPagas!= null){
			if(listaRecibosEduContPagas.size()>0){
				this.setListaRecibosEduContPagos(listaRecibosEduContPagas);
			}
		}
		
	}catch(Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
	}
	
}

public void setPersonaMain(){
	log.info("[recibirInscripcion.setPersonaMain]");
	String tipo = Executions.getCurrent().getDesktop().getSession().getAttribute("tipo").toString();
	TipoIdentificacion tipoIdentificacion = (TipoIdentificacion)this.getLbxRecibirInscripcionTipoIdentificacion().getSelectedItem().getAttribute("TIPO_IDENTIFICACION");
	PeriodoFacturacion periodo = new PeriodoFacturacion();
	periodo.setPeriodo(this.getPeriodoEducacionContinuada());
	String nombre = this.getTxtRecibirInscripcionNombres().getValue();
	String primerApellido = this.getTxtRecibirInscripcionPrimerApellido().getValue();
	String segundoApellido =  this.getTxtRecibirInscripcionSegundoApellido().getValue();
	String apellidos = primerApellido+((segundoApellido!=null && !segundoApellido.equalsIgnoreCase(""))?" "+segundoApellido:"");
	if(this.getLanguage().equalsIgnoreCase("en") && !this.getEncontroPersona()) {
		 String tmp[] = primerApellido.split(" ");
		 apellidos = primerApellido;
		 primerApellido = tmp[0];
		 segundoApellido =  tmp[1]; 	
	}
	String telefono = this.getTxtRecibirInscripcionTelefono().getValue();
	String identificacion = this.getTxtRecibirInscripcionIdentificacion().getValue().toString();
	String sexo = this.getLbxRecibirInscripcionSexo().getSelectedItem().getValue().toString();
	String direccionElectronica = this.getTxtRecibirInscripcionDireccionElectronica().getValue();
	String direccionContacto = tipo.equalsIgnoreCase("c")? this.getTxtRecibirInscripcionDireccionContacto().getValue() : IConstantes.DIRECCION_DEFECTO;
	String gradoEscolaridad = tipo.equalsIgnoreCase("c")? "PRO" :this.getLbxRecibirInscripcionEscolaridad().getSelectedItem().getValue().toString();
	String ocupacion = tipo.equalsIgnoreCase("c")? "EMP" : this.getLbxRecibirInscripcionOcupacion().getSelectedItem().getValue().toString() ;
	Integer aceptarCondiciones = null;
	Integer aceptarPersonales = null;
	String empresa ="";
	String universidadProcedencia = null;
	Textbox Uprocedencia = this.getTxtRecibirInscripcionUniversidadProcedencia();
	Uprocedencia.setConstraint("");
	if(tipo.equalsIgnoreCase("c")) {
		universidadProcedencia = "NO APLICA"; this.getTxtRecibirInscripcionUniversidadProcedencia().getValue();
	}else if(tipo.equalsIgnoreCase("e")) {
		if(Uprocedencia.getValue() == null || Uprocedencia.getValue().equalsIgnoreCase("")) {
			universidadProcedencia = "NO APLICA";		
		}else {
			universidadProcedencia = this.getTxtRecibirInscripcionUniversidadProcedencia().getValue();
		}
	}
	String aceptaCena = tipo.equalsIgnoreCase("c")? "N" : this.getChbxRecibirInscripcionCasillaCenaRadio2().getRadiogroup().getSelectedItem().getValue().toString();
	String tipoParticipante = tipo.equalsIgnoreCase("c")?  "P" : this.getChbxRecibirInscripcionCasillaEstudianteRadio1().getRadiogroup().getSelectedItem().getValue().toString();
	String cargo = this.getIdTxtRecibirInscripcionCargo().getValue();
	String modalidadAsistencia = 
			(String)(this.getLbxRecibirInscripcionModalidadAsistencia().getSelectedItem()).getValue();
	if(modalidadAsistencia==null || modalidadAsistencia.equalsIgnoreCase("0")|| modalidadAsistencia.equalsIgnoreCase("")) {
		modalidadAsistencia = "V";
	}
	
	Poblacion poblacion = tipo.equalsIgnoreCase("c")?(Poblacion)this.getCbxRecibirInscripcionUbicacion().getSelectedItem().getAttribute("POBLACION") : 
		new Poblacion(new Departamento(new Pais(Long.valueOf(IConstantes.PAIS_DEFECTO), "","",""), Long.valueOf(IConstantes.DEPARTAMENTO_DEFECTO), "",""), Long.valueOf(IConstantes.POBLACION_DEFECTO), "", "","");
	ProgramaAcademico programa = (ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA");
	CentroCosto centroCosto = this.getCentroCostoPrograma(programa.getIdPrograma());
	
	if(tipo.equalsIgnoreCase("c")) {
		if(this.getIdDivRecibirInscripcionNewPatrocinador().isVisible()) {
			aceptarCondiciones = Integer.valueOf(this.getChbxRecibirInscripcionAceptarCondicionesNewPatrocinador().getValue().toString());
			aceptarPersonales = Integer.valueOf(this.getChbxRecibirInscripcionAceptarPersonalNewPatrocinador().getValue().toString());
			empresa = this.getIdTxtRecibirInscripcionNombrePatrocinador().getValue();
		}else {
			aceptarCondiciones = Integer.valueOf(this.getChbxRecibirInscripcionAceptarCondiciones().getValue().toString());
			aceptarPersonales = Integer.valueOf(this.getChbxRecibirInscripcionAceptarPersonal().getValue().toString());
			if(this.getIdCbxRecibirInscripcionPatrocinador()!=null) {
				if(this.getIdCbxRecibirInscripcionPatrocinador().getSelectedItem()!=null) {
					empresa = ((Persona)this.getIdCbxRecibirInscripcionPatrocinador().getSelectedItem().getAttribute("PERSONA")).getNombreRazonSocial();
				}
			}
		}
	}else if(tipo.equalsIgnoreCase("e")) {
		aceptarCondiciones = Integer.valueOf(this.getChbxRecibirInscripcionAceptarCondiciones().getValue().toString());
		aceptarPersonales = Integer.valueOf(this.getChbxRecibirInscripcionAceptarPersonal().getValue().toString());
	}

	Cliente clienteNuevo = new Cliente();
	clienteNuevo.setPrograma(programa);
	if(this.getListaRecibosEduContAnt()!=  null && this.getListaRecibosEduContAnt().size()>0) {
		Estudiante estudianteAntiguoEscolaris = ReciboConsignacionHelper.getHelper().getEstudianteBasic("", 
				String.valueOf(this.getListaRecibosEduContAnt().get(0).getCliente().getCliente()), 
				identificacion, programa.getIdPrograma());
		if(estudianteAntiguoEscolaris.getIdEstudiante()!=null && estudianteAntiguoEscolaris.getIdEstudiante()>0L) {
			clienteNuevo.setCliente(this.getListaRecibosEduContAnt().get(0).getCliente().getCliente());
		} else {
			clienteNuevo.setCliente(Long.valueOf(this.getCodigoEstudiante()));
		}
		
	}else {
		clienteNuevo.setCliente(Long.valueOf(this.getCodigoEstudiante()));	
	}
	clienteNuevo.setNombreNegocio(nombre+" "+apellidos);
	clienteNuevo.setCentroCosto(centroCosto);
	clienteNuevo.setDireccion(direccionContacto);
	clienteNuevo.setTelefono(telefono);
	clienteNuevo.setDireccionElectronica(direccionElectronica);
	clienteNuevo.setEgresado("N");
	clienteNuevo.setEstudianteActivo("N");
	clienteNuevo.setPeriodo(periodo);
	Persona personaNuevo = new Persona();
	personaNuevo.setTipoIdentificacion(tipoIdentificacion);
	personaNuevo.setIdentificacion(identificacion);
	personaNuevo.setNombreRazonSocial(nombre);
	personaNuevo.setPrimerApellido(primerApellido);
	personaNuevo.setSegundoApellido(segundoApellido);
	personaNuevo.setTelefonoResidencia(telefono);
	personaNuevo.setDireccionResidencia(direccionContacto);
	personaNuevo.setDireccionElectronica(direccionElectronica);
	personaNuevo.setPoblacionResidencia(poblacion);
	personaNuevo.setGradoEscolaridad(gradoEscolaridad);
	personaNuevo.setEmpresa(empresa);
	personaNuevo.setCargo(cargo);
	personaNuevo.setSexo(sexo);
	personaNuevo.setOcupacion(ocupacion);
	personaNuevo.setAceptarCondiciones(aceptarCondiciones);
	personaNuevo.setAceptarPersonales(aceptarPersonales);
	personaNuevo.setCliente(clienteNuevo);
	personaNuevo.setUniversidadProcedencia(universidadProcedencia);
	personaNuevo.setAceptaCena(aceptaCena);
	personaNuevo.setTipoParticipante(tipoParticipante);
	personaNuevo.setModalidadAsistencia(modalidadAsistencia);
	
	this.setPersona(personaNuevo);
}

public void onCancelarSeleccionPatrocinador() {
	log.info("[recibirInscripcion.onCancelarSeleccionPatrocinador]");
	((Div)this.getFellow("idDivRecibirInscripcionPatrocinador")).setVisible(false);
	((Div)this.getFellow("idDivRecibirInscripcionSelectPatrocinador")).setVisible(false);
	((Div)this.getFellow("idDivRecibirInscripcionNewPatrocinador")).setVisible(false);
	((Div)this.getFellow("idDivRecibirInscripcionMensajeCulminarInscripcion")).setVisible(false);
	((Groupbox)this.getFellow("idGbxFormGenerarInscripcionEduCont")).setVisible(true);
	((Groupbox)this.getFellow("idGbxFormPagarInscripcionEduCont")).setVisible(false);
	this.limpiarCamposFormularioPersona();
}


public void onVolverInicio() {
	log.info("[recibirInscripcion.onVolverInicio]");
	((Div)this.getFellow("idDivRecibirInscripcionPatrocinador")).setVisible(false);
	((Div)this.getFellow("idDivRecibirInscripcionSelectPatrocinador")).setVisible(false);
	((Div)this.getFellow("idDivRecibirInscripcionNewPatrocinador")).setVisible(false);
	((Groupbox)this.getFellow("idGbxFormGenerarInscripcionEduCont")).setVisible(true);
	((Groupbox)this.getFellow("idGbxFormPagarInscripcionEduCont")).setVisible(false);
	this.limpiarCamposFormularioPersona();
	Intbox ibxIdentificacion = this.getTxtRecibirInscripcionIdentificacion();
	ibxIdentificacion.setConstraint("");
	ibxIdentificacion.setValue(null);
	ibxIdentificacion.setConstraint("no empty");
}

public void onRegistrarNuevoPatrocinador() {
	Map<String,Object> mapaValidacion = this.onValidarFormularioDatosPersonalesNuevoPatrocinador();
	boolean resultado = (Boolean)mapaValidacion.get("VALIDACION");
	String error = (String)mapaValidacion.get("ERROR");
	if(resultado) {
	((Div)this.getFellow("idDivRecibirInscripcionPatrocinador")).setVisible(true);
	((Div)this.getFellow("idDivRecibirInscripcionSelectPatrocinador")).setVisible(false);
	((Div)this.getFellow("idDivRecibirInscripcionNewPatrocinador")).setVisible(true);
	((Groupbox)this.getFellow("idGbxFormGenerarInscripcionEduCont")).setVisible(false);
	((Groupbox)this.getFellow("idGbxFormPagarInscripcionEduCont")).setVisible(false);
	} else {
		Clients.showNotification(error, Clients.NOTIFICATION_TYPE_ERROR, this.getIdBtnRecibirInscripcionNuevoPatrocinador(), "end_before", 4000, true);
		
	}
}

public void onCancelarNuevoPatrocinador() {
	((Div)this.getFellow("idDivRecibirInscripcionPatrocinador")).setVisible(false);
	((Div)this.getFellow("idDivRecibirInscripcionSelectPatrocinador")).setVisible(false);
	((Div)this.getFellow("idDivRecibirInscripcionNewPatrocinador")).setVisible(false);
	((Groupbox)this.getFellow("idGbxFormGenerarInscripcionEduCont")).setVisible(true);
	((Groupbox)this.getFellow("idGbxFormPagarInscripcionEduCont")).setVisible(false);	
}

public void onMostrarSeccionesInscripcion() {
	if(Integer.valueOf(this.getChbxRecibirInscripcionPatrocinado().getValue().toString())>0) {
		((Div)this.getFellow("idDivRecibirInscripcionPatrocinador")).setVisible(true);
		((Div)this.getFellow("idDivRecibirInscripcionSelectPatrocinador")).setVisible(true);
		((Div)this.getFellow("idDivRecibirInscripcionNewPatrocinador")).setVisible(false);
		((Groupbox)this.getFellow("idGbxFormGenerarInscripcionEduCont")).setVisible(false);
		((Groupbox)this.getFellow("idGbxFormPagarInscripcionEduCont")).setVisible(false);
	} else {
		((Div)this.getFellow("idDivRecibirInscripcionPatrocinador")).setVisible(false);
		((Div)this.getFellow("idDivRecibirInscripcionSelectPatrocinador")).setVisible(false);
		((Div)this.getFellow("idDivRecibirInscripcionNewPatrocinador")).setVisible(false);
		((Groupbox)this.getFellow("idGbxFormGenerarInscripcionEduCont")).setVisible(false);
		((Groupbox)this.getFellow("idGbxFormPagarInscripcionEduCont")).setVisible(true);
	} // Fin si no chequea patrocinio
	
}

public void handleUploadEvent(Event event) {

	try {
		UploadEvent eventUpload = (UploadEvent) event;
		Media media = eventUpload.getMedia();
		File archivo = new File(media.getName());
		FileOutputStream salida = new FileOutputStream(archivo);
		if (media.isBinary()) {
			IOUtils.copy(media.getStreamData(), salida);
		} else {
			IOUtils.copy(media.getReaderData(), salida);
		}
		((Textbox) this.getFellow("idTxtRecibirInscripcionArchivoRutPatrocinador")).setValue(media.getName());
		this.setFileRutPatrocinador(archivo);

	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}

@SuppressWarnings({ "rawtypes", "unchecked" })
public void addListenerUploadArchivoRut() {
	Button botonUploadSoporte = (Button) this.getFellow("idBtnRecibirInscripcionBuscarRut");

	botonUploadSoporte.addEventListener(Events.ON_UPLOAD, new EventListener() {
		public void onEvent(final Event arg0) throws Exception {
			log.info("[idBtnRecibirInscripcionBuscarRut.eventListener]");
			handleUploadEvent(arg0);
		}
	});

}


public void generarInscripcionCondicional() {
	log.info("[recibirInscripcion.generarInscripcionCondicional]");
	try {
		this.onValidateForm();
		this.onValidatePersonaProgramaInscrito();
		this.onValidatePersonaProgramaAnterior();
		this.onValidatePersonaProgramaPago();
			if(!(this.getListaRecibosEduContPagos().size()>0)){	
				if(!(this.getListaRecibosEduContAnt().size()>0)){	
					if(!(this.getListaRecibosEduCont().size()>0)){
						if(Integer.valueOf(this.getChbxRecibirInscripcionPatrocinado().getValue().toString())>0) {
							log.info("[recibirInscripcion.generarInscripcionCondicional  check]");
							this.onValidarCheckEmpresaPatrocinadora();
							this.generarInscripcion();
						} else {
							log.info("[recibirInscripcion.generarInscripcionCondicional NO check]");
							this.generarInscripcion();
							
						}
					} else {
						if(Integer.valueOf(this.getChbxRecibirInscripcionPatrocinado().getValue().toString())>0) {
							this.onValidarCheckEmpresaPatrocinadora();
							this.generarInscripcion();
						}else {
						this.getIdGbxFormGenerarInscripcionEduCont().setVisible(false);
					    this.getIdGbxFormPagarInscripcionEduCont().setVisible(true);
					    this.buscarMaestro();
					    //this.setMensajeVentanasEmergentes();
						}
					} // Fin si/no se ha generado recibo en el periodo
				} else {
					this.generarInscripcion();
					//this.onMostrarSeccionesInscripcion();
				} // Fin si/no se ha generado  recibos anteriores		
			} else {
				Long cliente = this.getListaRecibosEduContPagos().get(0).getCliente().getCliente();
				String periodo = this.getListaRecibosEduContPagos().get(0).getPeriodo().getPeriodo().getPeriodo();
				Clients.showNotification(IConstantes.MENSAJE_RECIBO_PAGADO_EDU_CONT+
						" C�digo estudiante: "+cliente.toString()+" Periodo: "+periodo,
						Clients.NOTIFICATION_TYPE_WARNING, this, "middle_center", 4000);
			}	// Fin si la persona no ha cursado ese programa
		
	}catch(Exception e) {
		e.printStackTrace();
		log.info(e.getMessage());
		Messagebox.show(e.getMessage(),"Error de inscripción", Messagebox.YES, Messagebox.ERROR);
	}
}


public void generarInscripcion(){
    log.info("[recibirInscripcion.generarInscripcion]");
    String tipo = Executions.getCurrent().getDesktop().getSession().getAttribute("tipo").toString();
    try{
    	boolean validacion = false;
    	String error = "";
    	if(this.getIdDivRecibirInscripcionNewPatrocinador().isVisible()) {
    		Map<String, Object> mapaValidacion = this.onValidarFormularioDatosEmpresaNuevoPatrocinador();
    		validacion = (Boolean)mapaValidacion.get("VALIDACION");
    		error = (String)mapaValidacion.get("ERROR");
    	}else {
    		validacion = true;
    	}
    if (validacion) {	
	this.onValidateForm();
	this.onValidatePersonaProgramaInscrito();
	this.onValidatePersonaProgramaAnterior();
	this.onValidatePersonaProgramaPago();
	this.onValidarCheckApoyoEmpresa();
	if(this.getListaRecibosEduContPagos()!= null){
	    if(!(this.getListaRecibosEduContPagos().size()>0)){	
		if(this.getListaRecibosEduContAnt()!=null){	
		    if(!(this.getListaRecibosEduContAnt().size()>0)){	
			if(this.getListaRecibosEduCont()!= null){
			    if(!(this.getListaRecibosEduCont().size()>0)){
				this.setPersonaMain();	
				if(this.getPersona()!= null){
					log.info("generarInscripcion[pers-1]: Sec: "+this.getPersona().getSecuenciaPersona()+" Id: "+
							this.getPersona().getIdentificacion()+" Cli: "+this.getPersona().getCliente()!=null?this.getPersona().getCliente().getCliente():"Vacío");
				    if(this.getPersona().getCliente()!=null){
					if(this.getPersona().getCliente().getCliente()!=null){
					    if(this.getPersona().getCliente().getCliente()>0){
						try{
						    if(this.onValidateSaldoPersona(this.getPersona())){
														
							Map<String, Object> mapaParametros = new HashMap<String, Object>();
							mapaParametros.put("CENTRO_COSTO", this.getPersona().getCliente().getCentroCosto().getCentroCosto());
							mapaParametros.put("PERIODO", this.getPersona().getCliente().getPeriodo().getPeriodo().trim());
								

								if(this.onValidateConfiguracionEquivalencia(mapaParametros)
									&& this.onValidateConfiguracionBanco(mapaParametros)
									&& this.onValidateConfiguracionFechasInscripcion(mapaParametros)
									&& this.onValidateGruposVencimiento(mapaParametros)
									&& this.onValidateIdPlan()
								){
									this.getIdBtnRecibirInscripcionInscribir().setDisabled(true);
									
									log.info("generarInscripcion(no existen recibos anteriores 0)");
									
									
									Integer  resultado = null;
									if(this.getLanguage().equalsIgnoreCase("es")) {
										resultado = Messagebox.show(
												"¿Confirma esta inscripción?, esto puede tardar algunos minutos. ",
												"Confirmar Inicio Inscripción",
												Messagebox.YES | Messagebox.NO, Messagebox.QUESTION 
												);
									}else if(this.getLanguage().equalsIgnoreCase("en")) {
										resultado = Messagebox.show(
												"Confirm this registration ?, this may take a few minutes.",
												"Confirm Start Registration",
												Messagebox.YES | Messagebox.NO, Messagebox.QUESTION 
												);
									}


									if(resultado==Messagebox.YES){
									    this.insertRegistroPersonaCliente();
									    this.insertRegistroEstudiante(this.getPersona());
									    this.generarOrdenEducacionContinuada(this.getPersona());
									    
									    if(Integer.valueOf(this.getChbxRecibirInscripcionPatrocinado().getValue().toString())>0) {
											    	if(this.getIdDivRecibirInscripcionNewPatrocinador().isVisible()) {
											    		this.getIdCbxRecibirInscripcionPatrocinador().setSelectedItem(null);
											    		this.getIdCbxRecibirInscripcionPatrocinador().setText(null);
											    	}
											    	
											    	this.onGuardarNuevoPatrocinador();
											    	this.onGuardarNuevoPatrocinado();
											    	this.setListValuesPersonasPatrocinador();
											    	((Div)this.getFellow("idDivRecibirInscripcionPatrocinador")).setVisible(false);
													((Div)this.getFellow("idDivRecibirInscripcionSelectPatrocinador")).setVisible(false);
													((Div)this.getFellow("idDivRecibirInscripcionNewPatrocinador")).setVisible(false);
													this.getIdGbxFormGenerarInscripcionEduCont().setVisible(false);
													((Div)this.getFellow("idDivRecibirInscripcionMensajeCulminarInscripcion")).setVisible(true);
													
												} else {
													if(this.getIdDivRecibirInscripcionNewPatrocinador().isVisible()) {
														this.onGuardarNuevoPatrocinador();
												    	this.onGuardarNuevoPatrocinado();
												    	this.setListValuesPersonasPatrocinador();
													}
													this.getIdGbxFormGenerarInscripcionEduCont().setVisible(false);
												    this.getIdGbxFormPagarInscripcionEduCont().setVisible(true);
												    ((Div)this.getFellow("idDivRecibirInscripcionPatrocinador")).setVisible(false);
													((Div)this.getFellow("idDivRecibirInscripcionSelectPatrocinador")).setVisible(false);
													((Div)this.getFellow("idDivRecibirInscripcionNewPatrocinador")).setVisible(false);
													this.buscarMaestro();
												}
											    
											    Map<String, Object> mapaConfDescuento = new HashMap<String,Object>();
											    Map<String, String> mapaAplicaDescuentos= this.onBuscarAspirante(this.getPersona().getIdentificacion());
											    if(mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_SI) && 
												    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_SI) &&
												    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_SI)){
													// se toma como prioridad el estudiante
													mapaConfDescuento = this.getConceptoDescuentoEstudiante();

											    }else if (mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_SI) && 
												    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_SI) &&
												    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_NO)){
													// se toma como prioridad el estudiante
													mapaConfDescuento = this.getConceptoDescuentoEstudiante();
												
											    }else if (mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_SI) && 
												    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_NO) &&
												    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_NO)){
													// se toma como prioridad empleado
													mapaConfDescuento = this.getConceptoDescuentoFuncionario();
												
											    }else if (mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_SI) && 
												    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_NO) &&
												    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_SI)
												    ){
													// se toma como prioridad el estudiante
													mapaConfDescuento = this.getConceptoDescuentoFuncionario();
												
											    } else if (mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_NO) && 
												    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_SI) &&
												    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_SI)){
													// se toma como prioridad el egresado
													mapaConfDescuento = this.getConceptoDescuentoEgresado();
												
											    } else if (mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_NO) && 
												    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_NO) &&
												    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_SI)){
													mapaConfDescuento = this.getConceptoDescuentoEgresado();
												
											    } else if (mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_NO) && 
												    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_SI) &&
												    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_NO)){
													mapaConfDescuento = this.getConceptoDescuentoEstudiante();
											    }
											    
											    //this.setMensajeVentanasEmergentes();
											    this.getIdBtnRecibirInscripcionInscribir().setDisabled(false);
											    // si vienen valores en el mapa de configuraci�n de descuentos
											    if(mapaConfDescuento.get("CONCEPTO_NOTA")!= null && mapaConfDescuento.get("CAUSA_NOTA")!=null 
												    && mapaConfDescuento.get("PORCENTAJE")!=null && mapaConfDescuento.get("INCREMENTO_GRUPO_ORDEN")!=null){
												// tampoco debe ser vac�o
												if(!mapaConfDescuento.get("CONCEPTO_NOTA").equals("") && !mapaConfDescuento.get("CAUSA_NOTA").equals("") 
													    && !mapaConfDescuento.get("PORCENTAJE").equals("") && !mapaConfDescuento.get("INCREMENTO_GRUPO_ORDEN").equals("")
													
													){
												    		log.info("Encuentra cofiguración de conceptos");
												    if(this.getListaRecibosEduCont()!=null){
													if(this.getListaRecibosEduCont().size()>0){
													    
													    ReciboConsignacion reciboAplicar = this.getListaRecibosEduCont().get(0);
													    Map<String, Object> mapaSolicitudNota = new HashMap<String, Object>();
													    mapaSolicitudNota.put("CONCEPTO_NOTA", mapaConfDescuento.get("CONCEPTO_NOTA").toString());
													    mapaSolicitudNota.put("CAUSA_NOTA", mapaConfDescuento.get("CAUSA_NOTA").toString());
													    mapaSolicitudNota.put("PORCENTAJE", mapaConfDescuento.get("PORCENTAJE").toString());
													    
													    if(this.getProgramaAcademico()!=null){
															if(this.getProgramaAcademico().getCodigoPrograma()!=null){
															    mapaSolicitudNota.put("PORCENTAJE", this.getProgramaAcademico().getPorcentaje());
															}else{
															    if(this.getLbxRecibirInscripcionPrograma().getSelectedItem()!=null){
																mapaSolicitudNota.put("PORCENTAJE", 
																    ((ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA")).getPorcentaje());
															    }
															}
														    }else{
															if(this.getLbxRecibirInscripcionPrograma().getSelectedItem()!=null){
															    mapaSolicitudNota.put("PORCENTAJE", 
																    ((ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA")).getPorcentaje());
															}   
														    }
													    
													    mapaSolicitudNota.put("INCREMENTO", mapaConfDescuento.get("INCREMENTO_GRUPO_ORDEN").toString());
													    mapaSolicitudNota.put("ORDEN", reciboAplicar.getOrden().getOrden());
													    mapaSolicitudNota.put("ORGANIZACION_ORDEN", reciboAplicar.getOrden().getOrganizacion());
													    mapaSolicitudNota.put("DOCUMENTO", reciboAplicar.getOrden().getDocumento());
													    mapaSolicitudNota.put("VALOR", new Double(0));
													    mapaSolicitudNota.put("NUMERO_SOLICITUD", new Long(0));
													    mapaSolicitudNota.put("FUENTE_FUNCION", mapaConfDescuento.get("FUENTE_FUNCION").toString());
													    mapaSolicitudNota.put("ACEPTO_CENA", this.getChbxRecibirInscripcionCasillaCenaRadio2().getRadiogroup().getSelectedItem().getValue().toString());
													    if(((ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA")).getDescuento().equalsIgnoreCase("S")){
													    	ParametrizacionFac.getFacade().ejecutarProcedimiento("insertSolicitudNota", mapaSolicitudNota);
													    }
													    
													    if(tipo.equalsIgnoreCase("e") && this.getChbxRecibirInscripcionCasillaCenaRadio2().getRadiogroup().getSelectedItem().getValue().toString().equalsIgnoreCase("S")) {
													    	this.onValidatePersonaProgramaInscrito();
													    	this.generarInscripcionCena();
													    }
													    
													    if(this.getChbxRecibirInscripcionCasillaCenaRadio2().getRadiogroup().getSelectedItem().getValue().toString().equalsIgnoreCase("S")
													    		|| ((ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA")).getDescuento().equalsIgnoreCase("S")
													    		) {
													    	this.regenerarReciboConsignacion();
													    }
													    
													    this.buscarMaestro();
													    //this.setMensajeVentanasEmergentes();
													}
												    }
												}
											    }  
									}else{
									    this.getIdBtnRecibirInscripcionInscribir().setDisabled(false);
									}
								} else{
								    Messagebox.show("No es posible realizar la inscripción por problemas de configuración por favor realice una llamada al número: 57(1) 3472311 Ext. 129,156,186","Error de inscripción",
										Messagebox.YES,Messagebox.ERROR);
								}			
							} else{
							    Messagebox.show("No es posible realizar la inscripción por que el cliente presenta saldo en su estado de cuenta","Error de inscripción",
										Messagebox.YES,Messagebox.ERROR);
							}		
						} catch(Exception e){
						    e.printStackTrace();
						    log.info(e.getMessage());
						    Messagebox.show("No es posible realizar la inscripción por falta de información","Error de inscripción",
								Messagebox.YES,Messagebox.ERROR);
						}
								
					} else{
					    Messagebox.show("No es posible realizar la inscripción por falta de información","Error de Inscripción",
								Messagebox.YES,Messagebox.ERROR);
					    throw new Exception("No es posible realizar la inscripción por falta de información");
				}
			}
		}
	}
	}
			    else {
	    // Si no es 0 
		log.info("generarInscripcion(existen recibos anteriores)");
		 if(Integer.valueOf(this.getChbxRecibirInscripcionPatrocinado().getValue().toString())>0) {
		    	if(this.getIdDivRecibirInscripcionNewPatrocinador().isVisible()) {
		    		this.getIdCbxRecibirInscripcionPatrocinador().setSelectedItem(null);
		    		this.getIdCbxRecibirInscripcionPatrocinador().setText(null);
		    	}
		    	
		    	this.onGuardarNuevoPatrocinador();
		    	this.onGuardarNuevoPatrocinado();
		    	this.setListValuesPersonasPatrocinador();
		    	((Div)this.getFellow("idDivRecibirInscripcionPatrocinador")).setVisible(false);
				((Div)this.getFellow("idDivRecibirInscripcionSelectPatrocinador")).setVisible(false);
				((Div)this.getFellow("idDivRecibirInscripcionNewPatrocinador")).setVisible(false);
				this.getIdGbxFormGenerarInscripcionEduCont().setVisible(false);
				((Div)this.getFellow("idDivRecibirInscripcionMensajeCulminarInscripcion")).setVisible(true);
		    
			} else {
				if(this.getIdDivRecibirInscripcionNewPatrocinador().isVisible()) {
					this.onGuardarNuevoPatrocinador();
			    	this.onGuardarNuevoPatrocinado();
			    	this.setListValuesPersonasPatrocinador();
				}
			    if(tipo.equalsIgnoreCase("e") && this.getChbxRecibirInscripcionCasillaCenaRadio2().getRadiogroup().getSelectedItem().getValue().toString().equalsIgnoreCase("S")) {
			    	this.onValidatePersonaProgramaInscrito();
			    	this.generarInscripcionCena();
			    }
				    Map<String, Object> mapaConfDescuento = new HashMap<String,Object>();
				    Map<String, String> mapaAplicaDescuentos= this.onBuscarAspirante(this.getPersona().getIdentificacion());
				    if(mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_SI) && 
					    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_SI) &&
					    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_SI)){
						// se toma como prioridad el estudiante
						mapaConfDescuento = this.getConceptoDescuentoEstudiante();
	
				    }else if (mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_SI) && 
					    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_SI) &&
					    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_NO)){
						// se toma como prioridad el estudiante
						mapaConfDescuento = this.getConceptoDescuentoEstudiante();
					
				    }else if (mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_SI) && 
					    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_NO) &&
					    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_NO)){
						// se toma como prioridad empleado
						mapaConfDescuento = this.getConceptoDescuentoFuncionario();
					
				    }else if (mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_SI) && 
					    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_NO) &&
					    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_SI)
					    ){
						// se toma como prioridad el estudiante
						mapaConfDescuento = this.getConceptoDescuentoFuncionario();
					
				    } else if (mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_NO) && 
					    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_SI) &&
					    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_SI)){
						// se toma como prioridad el egresado
						mapaConfDescuento = this.getConceptoDescuentoEgresado();
					
				    } else if (mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_NO) && 
					    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_NO) &&
					    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_SI)){
						mapaConfDescuento = this.getConceptoDescuentoEgresado();
					
				    } else if (mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_NO) && 
					    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_SI) &&
					    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_NO)){
						mapaConfDescuento = this.getConceptoDescuentoEstudiante();
				    }
				    
				    //this.setMensajeVentanasEmergentes();
				    this.getIdBtnRecibirInscripcionInscribir().setDisabled(false);
				    // si vienen valores en el mapa de configuraci�n de descuentos
				    if(mapaConfDescuento.get("CONCEPTO_NOTA")!= null && mapaConfDescuento.get("CAUSA_NOTA")!=null 
					    && mapaConfDescuento.get("PORCENTAJE")!=null && mapaConfDescuento.get("INCREMENTO_GRUPO_ORDEN")!=null){
					// tampoco debe ser vac�o
					if(!mapaConfDescuento.get("CONCEPTO_NOTA").equals("") && !mapaConfDescuento.get("CAUSA_NOTA").equals("") 
						    && !mapaConfDescuento.get("PORCENTAJE").equals("") && !mapaConfDescuento.get("INCREMENTO_GRUPO_ORDEN").equals("")
						
						){
					    		log.info("Encuentra cofiguraci�n de conceptos");
					    if(this.getListaRecibosEduCont()!=null){
						if(this.getListaRecibosEduCont().size()>0){
						    
						    ReciboConsignacion reciboAplicar = this.getListaRecibosEduCont().get(0);
						    Map<String, Object> mapaSolicitudNota = new HashMap<String, Object>();
						    mapaSolicitudNota.put("CONCEPTO_NOTA", mapaConfDescuento.get("CONCEPTO_NOTA").toString());
						    mapaSolicitudNota.put("CAUSA_NOTA", mapaConfDescuento.get("CAUSA_NOTA").toString());
						    mapaSolicitudNota.put("PORCENTAJE", mapaConfDescuento.get("PORCENTAJE").toString());
						    
						    if(this.getProgramaAcademico()!=null){
								if(this.getProgramaAcademico().getCodigoPrograma()!=null){
								    mapaSolicitudNota.put("PORCENTAJE", this.getProgramaAcademico().getPorcentaje());
								}else{
								    if(this.getLbxRecibirInscripcionPrograma().getSelectedItem()!=null){
									mapaSolicitudNota.put("PORCENTAJE", 
									    ((ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA")).getPorcentaje());
								    }
								}
							    }else{
								if(this.getLbxRecibirInscripcionPrograma().getSelectedItem()!=null){
								    mapaSolicitudNota.put("PORCENTAJE", 
									    ((ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA")).getPorcentaje());
								}   
							    }
						    
						    mapaSolicitudNota.put("INCREMENTO", mapaConfDescuento.get("INCREMENTO_GRUPO_ORDEN").toString());
						    mapaSolicitudNota.put("ORDEN", reciboAplicar.getOrden().getOrden());
						    mapaSolicitudNota.put("ORGANIZACION_ORDEN", reciboAplicar.getOrden().getOrganizacion());
						    mapaSolicitudNota.put("DOCUMENTO", reciboAplicar.getOrden().getDocumento());
						    mapaSolicitudNota.put("VALOR", new Double(0));
						    mapaSolicitudNota.put("NUMERO_SOLICITUD", new Long(0));
						    mapaSolicitudNota.put("FUENTE_FUNCION", mapaConfDescuento.get("FUENTE_FUNCION").toString());
						    mapaSolicitudNota.put("ACEPTO_CENA", this.getChbxRecibirInscripcionCasillaCenaRadio2().getRadiogroup().getSelectedItem().getValue().toString());
						    if(((ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA")).getDescuento().equalsIgnoreCase("S")){
						    	ParametrizacionFac.getFacade().ejecutarProcedimiento("insertSolicitudNota", mapaSolicitudNota);
						    }
						    if(tipo.equalsIgnoreCase("e") && this.getChbxRecibirInscripcionCasillaCenaRadio2().getRadiogroup().getSelectedItem().getValue().toString().equalsIgnoreCase("S")) {
						    	this.onValidatePersonaProgramaInscrito();
						    	this.generarInscripcionCena();
						    }
						    
						    if(this.getChbxRecibirInscripcionCasillaCenaRadio2().getRadiogroup().getSelectedItem().getValue().toString().equalsIgnoreCase("S")
						    		|| ((ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA")).getDescuento().equalsIgnoreCase("S")
						    		) {
						    	this.regenerarReciboConsignacion();
						    }
				this.getIdGbxFormGenerarInscripcionEduCont().setVisible(false);
			    this.getIdGbxFormPagarInscripcionEduCont().setVisible(true);
			    ((Div)this.getFellow("idDivRecibirInscripcionPatrocinador")).setVisible(false);
				((Div)this.getFellow("idDivRecibirInscripcionSelectPatrocinador")).setVisible(false);
				((Div)this.getFellow("idDivRecibirInscripcionNewPatrocinador")).setVisible(false);
				this.buscarMaestro();
			}
					}
					}
				    }
					}
	
	}	
}
} // fin si NO > 0 lista de recibos anteriores
else {
	if(this.getListaRecibosEduCont()!=null){
	    if(!(this.getListaRecibosEduCont().size()>0)){
	    	log.info("generarInscripcion(no existen recibos anteriores)");
	    	Integer  resultado = 0;	
	    	if(this.getLanguage().equalsIgnoreCase("es")) {
	    		resultado = Messagebox.show(
	    				"¿Confirma esta inscripción?, esto puede tardar algunos minutos. ",
	    				"Confirmar Inicio Inscripción",
	    				Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
	    	}else if(this.getLanguage().equalsIgnoreCase("en")){
	    		resultado = Messagebox.show(
	    				"Do you confirm this registration? This may take a few minutes.",
	    				"Confirm Start Registration",
	    				Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
	    	}

		if(resultado==Messagebox.YES){
			
		    this.setPersonaMain();	
		    this.insertRegistroPersonaCliente();
		    Estudiante estudianteAntiguoEscolaris = ReciboConsignacionHelper.getHelper().getEstudianteBasic("", 
					String.valueOf(this.getListaRecibosEduContAnt().get(0).getCliente().getCliente()), 
					this.getPersona().getIdentificacion(), this.getProgramaAcademico().getIdPrograma());
		    if(estudianteAntiguoEscolaris.getIdEstudiante()!= null && estudianteAntiguoEscolaris.getIdEstudiante()>0L) {
		    	this.updateRegistroEstudiante(this.getPersona(),this.getListaRecibosEduContAnt());
		    }else {
		    	this.insertRegistroEstudiante(this.getPersona());
		    }
		    
		    this.generarOrdenEducacionContinuada(this.getPersona());
		    if(Integer.valueOf(this.getChbxRecibirInscripcionPatrocinado().getValue().toString())>0) {
		    	this.onGuardarNuevoPatrocinador();
		    	this.onGuardarNuevoPatrocinado();
		    	this.setListValuesPersonasPatrocinador();
		    	((Div)this.getFellow("idDivRecibirInscripcionPatrocinador")).setVisible(false);
				((Div)this.getFellow("idDivRecibirInscripcionSelectPatrocinador")).setVisible(false);
				((Div)this.getFellow("idDivRecibirInscripcionNewPatrocinador")).setVisible(false);
				this.getIdGbxFormGenerarInscripcionEduCont().setVisible(false);
				((Div)this.getFellow("idDivRecibirInscripcionMensajeCulminarInscripcion")).setVisible(true);
			} else {
				if(this.getIdDivRecibirInscripcionNewPatrocinador().isVisible()) {
					this.onGuardarNuevoPatrocinador();
			    	this.onGuardarNuevoPatrocinado();
			    	this.setListValuesPersonasPatrocinador();
				}
				this.getIdGbxFormGenerarInscripcionEduCont().setVisible(false);
			    this.getIdGbxFormPagarInscripcionEduCont().setVisible(true);
			    ((Div)this.getFellow("idDivRecibirInscripcionPatrocinador")).setVisible(false);
				((Div)this.getFellow("idDivRecibirInscripcionSelectPatrocinador")).setVisible(false);
				((Div)this.getFellow("idDivRecibirInscripcionNewPatrocinador")).setVisible(false);
				this.buscarMaestro();
			    this.setMensajeVentanasEmergentes();	
			}
		    
		    Map<String, Object> mapaConfDescuento = new HashMap<String,Object>();
		    Map<String, String> mapaAplicaDescuentos= this.onBuscarAspirante(this.getPersona().getIdentificacion());
		    if(mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_SI) && 
			    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_SI) &&
			    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_SI)){
				// se toma como prioridad el estudiante
				mapaConfDescuento = this.getConceptoDescuentoEstudiante();

		    }else if (mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_SI) && 
			    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_SI) &&
			    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_NO)){
				// se toma como prioridad el estudiante
				mapaConfDescuento = this.getConceptoDescuentoEstudiante();
			
		    }else if (mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_SI) && 
			    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_NO) &&
			    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_NO)){
				// se toma como prioridad empleado
				mapaConfDescuento = this.getConceptoDescuentoFuncionario();
			
		    }else if (mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_SI) && 
			    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_NO) &&
			    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_SI)
			    ){
				// se toma como prioridad el estudiante
				mapaConfDescuento = this.getConceptoDescuentoFuncionario();
			
		    } else if (mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_NO) && 
			    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_SI) &&
			    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_SI)){
			
				// se toma como prioridad el egresado
				mapaConfDescuento = this.getConceptoDescuentoEgresado();
			
		    } else if (mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_NO) && 
			    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_NO) &&
			    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_SI)){
			
				mapaConfDescuento = this.getConceptoDescuentoEgresado();
				
		    } else if (mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_NO) && 
			    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_SI) &&
			    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_NO)){
				mapaConfDescuento = this.getConceptoDescuentoEstudiante();
		    } else if (
				    mapaAplicaDescuentos.get("FUNCIONARIO").equalsIgnoreCase(IConstantes.GENERAL_NO) && 
				    mapaAplicaDescuentos.get("ESTUDIANTE").equalsIgnoreCase(IConstantes.GENERAL_NO) &&
				    mapaAplicaDescuentos.get("EGRESADO").equalsIgnoreCase(IConstantes.GENERAL_SI)
				    ){
				mapaConfDescuento = this.getConceptoDescuentoEgresado();
		    }
		    this.setMensajeVentanasEmergentes();
		    this.getIdBtnRecibirInscripcionInscribir().setDisabled(false);
		    // si vienen valores en el mapa de configuraci�n de descuentos
		    if(mapaConfDescuento.get("CONCEPTO_NOTA")!= null && mapaConfDescuento.get("CAUSA_NOTA")!=null 
			    && mapaConfDescuento.get("PORCENTAJE")!=null && mapaConfDescuento.get("INCREMENTO_GRUPO_ORDEN")!=null){
			// tampoco debe ser vac�o
			if(!mapaConfDescuento.get("CONCEPTO_NOTA").equals("") && !mapaConfDescuento.get("CAUSA_NOTA").equals("") 
				    && !mapaConfDescuento.get("PORCENTAJE").equals("") && !mapaConfDescuento.get("INCREMENTO_GRUPO_ORDEN").equals("")
				
				){
			    		log.info("Encuentra cofiguraci�n de conceptos");
			    		if(this.getListaRecibosEduCont()!=null){
			    		    if(this.getListaRecibosEduCont().size()>0){
				    
				    ReciboConsignacion reciboAplicar = this.getListaRecibosEduCont().get(0);
				    Map<String, Object> mapaSolicitudNota = new HashMap<String, Object>();
				    mapaSolicitudNota.put("CONCEPTO_NOTA", mapaConfDescuento.get("CONCEPTO_NOTA").toString());
				    mapaSolicitudNota.put("CAUSA_NOTA", mapaConfDescuento.get("CAUSA_NOTA").toString());
				    mapaSolicitudNota.put("PORCENTAJE", mapaConfDescuento.get("PORCENTAJE").toString());
				    if(this.getProgramaAcademico()!=null){
						if(this.getProgramaAcademico().getCodigoPrograma()!=null){
						    mapaSolicitudNota.put("PORCENTAJE", this.getProgramaAcademico().getPorcentaje());
						}else{
						    if(this.getLbxRecibirInscripcionPrograma().getSelectedItem()!=null){
							mapaSolicitudNota.put("PORCENTAJE", 
							    ((ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA")).getPorcentaje());
						    }
						}
					    }else{
						if(this.getLbxRecibirInscripcionPrograma().getSelectedItem()!=null){
						    mapaSolicitudNota.put("PORCENTAJE", 
							    ((ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA")).getPorcentaje());
						}   
					    }
				    
				    mapaSolicitudNota.put("INCREMENTO", mapaConfDescuento.get("INCREMENTO_GRUPO_ORDEN").toString());
				    mapaSolicitudNota.put("ORDEN", reciboAplicar.getOrden().getOrden());
				    mapaSolicitudNota.put("ORGANIZACION_ORDEN", reciboAplicar.getOrden().getOrganizacion());
				    mapaSolicitudNota.put("DOCUMENTO", reciboAplicar.getOrden().getDocumento());
				    mapaSolicitudNota.put("VALOR", new Double(0));
				    mapaSolicitudNota.put("NUMERO_SOLICITUD", new Double(0));
				    mapaSolicitudNota.put("FUENTE_FUNCION", mapaConfDescuento.get("FUENTE_FUNCION").toString());
				    mapaSolicitudNota.put("ACEPTO_CENA", this.getChbxRecibirInscripcionCasillaCenaRadio2().getRadiogroup().getSelectedItem().getValue().toString());
				    if(((ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA")).getDescuento().equalsIgnoreCase("S")){
				    	ParametrizacionFac.getFacade().ejecutarProcedimiento("insertSolicitudNota", mapaSolicitudNota);
				    }
				    if(tipo.equalsIgnoreCase("e") && this.getChbxRecibirInscripcionCasillaCenaRadio2().getRadiogroup().getSelectedItem().getValue().toString().equalsIgnoreCase("S")) {
				    	this.onValidatePersonaProgramaInscrito();
				    	this.generarInscripcionCena();
				    }
				    
				    if(this.getChbxRecibirInscripcionCasillaCenaRadio2().getRadiogroup().getSelectedItem().getValue().toString().equalsIgnoreCase("S")
				    		|| ((ProgramaAcademico)this.getLbxRecibirInscripcionPrograma().getSelectedItem().getAttribute("PROGRAMA")).getDescuento().equalsIgnoreCase("S")
				    		) {
				    	this.regenerarReciboConsignacion();
				    }
				    this.buscarMaestro();
				}
			    }
			}
		    }
		}	
	} else{
		
		log.info("generarInscripcion(no existen recibos anteriores 2)");
		
		if(Integer.valueOf(this.getChbxRecibirInscripcionPatrocinado().getValue().toString())>0) {
			this.onGuardarNuevoPatrocinador();
	    	this.onGuardarNuevoPatrocinado();
	    	this.setListValuesPersonasPatrocinador();
	    	((Div)this.getFellow("idDivRecibirInscripcionPatrocinador")).setVisible(false);
			((Div)this.getFellow("idDivRecibirInscripcionSelectPatrocinador")).setVisible(false);
			((Div)this.getFellow("idDivRecibirInscripcionNewPatrocinador")).setVisible(false);
			this.getIdGbxFormGenerarInscripcionEduCont().setVisible(false);
			((Div)this.getFellow("idDivRecibirInscripcionMensajeCulminarInscripcion")).setVisible(true);
		} else {
			if(this.getIdDivRecibirInscripcionNewPatrocinador().isVisible()) {
				this.onGuardarNuevoPatrocinador();
		    	this.onGuardarNuevoPatrocinado();
		    	this.setListValuesPersonasPatrocinador();
			}
			this.getIdGbxFormGenerarInscripcionEduCont().setVisible(false);
		    this.getIdGbxFormPagarInscripcionEduCont().setVisible(true);
		    ((Div)this.getFellow("idDivRecibirInscripcionPatrocinador")).setVisible(false);
			((Div)this.getFellow("idDivRecibirInscripcionSelectPatrocinador")).setVisible(false);
			((Div)this.getFellow("idDivRecibirInscripcionNewPatrocinador")).setVisible(false);
			this.buscarMaestro();
		    this.setMensajeVentanasEmergentes();	
		}
	    
	    
			}
		}	
	}
} // Si no es nula lista de recibos anteriores
}// fin si NO > 0 recibos Pagados Curso
else {
	Long cliente = this.getListaRecibosEduContPagos().get(0).getCliente().getCliente();
	String periodo = this.getListaRecibosEduContPagos().get(0).getPeriodo().getPeriodo().getPeriodo();
	Clients.showNotification(IConstantes.MENSAJE_RECIBO_PAGADO_EDU_CONT+
			" C�digo estudiante: "+cliente.toString()+" Periodo: "+periodo,
			Clients.NOTIFICATION_TYPE_WARNING, this, "middle_center", 4000);
	}	
} // Fin si no es Nula la lista de recibos pagados	
}  // fin si validaci�n
    else {
    	Clients.showNotification(error, Clients.NOTIFICATION_TYPE_ERROR, this.getFellow("idBtnRecibirInscripcionGuardarPatrocinador"), "end_before", 4000, true);
    }    
	
}catch(Exception e){
    e.printStackTrace();
    log.info(e.getMessage());
}
}

private boolean onValidateIdPlan() {
	ProgramaAcademico programa = this.getPersona().getCliente().getPrograma();
	PlanEstudios planEstudios = ReciboConsignacionHelper.getHelper().getPlanEstudiosAcademico(programa.getIdPrograma());
	if(planEstudios != null && planEstudios.getIdPlan() != null) {
		return true;
	}
	return false;
}


public void generarInscripcionCenaCondicional() {
	this.onValidateEventoForm();
	this.generarInscripcion();
}

public void generarInscripcionCena() {
    if(this.getListaRecibosEduCont()!=null){
		if(this.getListaRecibosEduCont().size()>0){
			try {
		    ReciboConsignacion reciboAplicar = this.getListaRecibosEduCont().get(0);
		    DetalleOrden detalleOrden = new DetalleOrden();
		    
		    CentroCosto centroCosto = new CentroCosto();
		    centroCosto.setOrganizacion(reciboAplicar.getOrden().getOrganizacion());
		    centroCosto.setCentroCosto(reciboAplicar.getOrden().getCentroCosto().getCentroCosto());
		    detalleOrden.setOrden(reciboAplicar.getOrden());
		    detalleOrden.setCantidad((new BigDecimal(1)).doubleValue());
		    detalleOrden.setPrecio((new BigDecimal(onObtenerValorCena().equals("0")? null : onObtenerValorCena())).doubleValue());
		    detalleOrden.setDescuento((new BigDecimal(0)).doubleValue());
		    detalleOrden.setIva((new BigDecimal(0)).doubleValue());
		    detalleOrden.setSubBruto(calcularSubBruto(new BigDecimal(detalleOrden.getCantidad()),new BigDecimal(detalleOrden.getPrecio())));
		    detalleOrden.setSubDescuento((new BigDecimal(0)).doubleValue());
		    detalleOrden.setSubIva((new BigDecimal(0)).doubleValue());
		    detalleOrden.setSubTotal(calcularSubTotal(new BigDecimal(detalleOrden.getSubBruto()),new BigDecimal(detalleOrden.getIva()),new BigDecimal(detalleOrden.getDescuento())));
		    detalleOrden.setLiquidado("N");
		    detalleOrden.setReferencia(onObtenerReferenciaCena().equals(0L) ? null : onObtenerReferenciaCena());
		    detalleOrden.setEstado("I");
		    detalleOrden.setCentroCosto(centroCosto);
		    
		    
			ParametrizacionFac.getFacade().ejecutarProcedimiento("insertDetalleOrden", detalleOrden);
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    }
}

public void regenerarReciboConsignacion (){
	 if(this.getListaRecibosEduCont()!=null){
			if(this.getListaRecibosEduCont().size()>0){
				try {
					ReciboConsignacion reciboAplicar = this.getListaRecibosEduCont().get(0);
					DetalleOrden detalleOrden = new DetalleOrden();
					detalleOrden.setOrden(reciboAplicar.getOrden());
					ParametrizacionFac.getFacade().ejecutarProcedimiento("regenerarReciboConsignacion", detalleOrden);
				
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
	    }
	
}

private double calcularSubTotal(BigDecimal subBruto, BigDecimal iva, BigDecimal descuento) {
	BigDecimal calculo = subBruto.add(iva);
	double resultado = (calculo.subtract(descuento)).doubleValue();
	return resultado;
}


private double calcularSubBruto(BigDecimal cantidad, BigDecimal precio) {
	return (cantidad.multiply(precio)).doubleValue();
}


public void insertRegistroPersonaCliente(){
	log.info("[recibirInscripcion.insertRegistroPersonaCliente]");
	try{
		
		Persona personaNuevo = new Persona();
		personaNuevo = this.getPersona();
		
		if(personaNuevo!=null){
		if(personaNuevo.getCliente()!=null ){
			if(personaNuevo.getCliente().getCliente()>0){
				ParametrizacionFac.getFacade().ejecutarProcedimiento("insertViewEscolarisAlumnosEducacionContinuada", personaNuevo);
				if(personaNuevo.getSecuenciaPersona()!=null) {
					log.info("Si llega pesonaNuevo:"+personaNuevo.getSecuenciaPersona());
						this.setPersona(personaNuevo);
					}
				}
			}
		}		
		
	}catch(Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
	}
	
}

public TipoIdentificacion onConvertirTipoIdentificacionAcademico(TipoIdentificacion tipoIdentificacion){
	log.info("[recibirInscripcion.onConvertirTipoIdentificacionAcademico]");
	TipoIdentificacion tipoIdConvertido = new TipoIdentificacion();
	if(tipoIdentificacion.getTipoIdentificacion().equalsIgnoreCase("C.C") 
			|| tipoIdentificacion.getTipoIdentificacion().equalsIgnoreCase("NIT") 
			|| tipoIdentificacion.getTipoIdentificacion().equalsIgnoreCase("NIF")
			|| tipoIdentificacion.getTipoIdentificacion().equalsIgnoreCase("NEX") ){
				tipoIdConvertido.setTipoIdentificacion("C");
				tipoIdConvertido.setNombreTipoIdentificacion("C�dula de Ciudadan�a");
	}else if(tipoIdentificacion.getTipoIdentificacion().equalsIgnoreCase("T.I")){
		tipoIdConvertido.setTipoIdentificacion("T");
		tipoIdConvertido.setNombreTipoIdentificacion("Tarjeta Identidad");
	}else if(tipoIdentificacion.getTipoIdentificacion().equalsIgnoreCase("PS")){
		tipoIdConvertido.setTipoIdentificacion("6");
		tipoIdConvertido.setNombreTipoIdentificacion("Pasaporte");
	}else if(tipoIdentificacion.getTipoIdentificacion().equalsIgnoreCase("RC")){
		tipoIdConvertido.setTipoIdentificacion("5");
		tipoIdConvertido.setNombreTipoIdentificacion("Registro Civil");
	} else if (tipoIdentificacion.getTipoIdentificacion().equalsIgnoreCase("VIS")){
		tipoIdConvertido.setTipoIdentificacion("V");
		tipoIdConvertido.setNombreTipoIdentificacion("Visa");
	} else if (tipoIdentificacion.getTipoIdentificacion().equalsIgnoreCase("CEX")){
		tipoIdConvertido.setTipoIdentificacion("E");
		tipoIdConvertido.setNombreTipoIdentificacion("C�dula Extranjer�a");
	}
	return tipoIdConvertido;
}


public void updateRegistroEstudiante(Persona persona, List<ReciboConsignacion> listaRecibosAnteriores){
	
	String codigoClienteAnterior = String.valueOf(listaRecibosAnteriores.get(0).getCliente().getCliente());
	ProgramaAcademico programa = persona.getCliente().getPrograma();
	Estudiante estudianteAntiguo = ReciboConsignacionHelper.getHelper().getEstudianteBasic(new String(""), codigoClienteAnterior, persona.getIdentificacion(), programa.getIdPrograma());
	log.info("[recibirInscripcion.updateRegistroEstudiante][programa: "+programa.getIdPrograma()+"-"+programa.getCodigoPrograma()+"-"+programa.getNombrePrograma()+"]"+
	"[Codigo Anterior: "+codigoClienteAnterior+"]"+"[identificacion: "+persona.getIdentificacion()+"]");
	Estudiante estudianteNuevo = new Estudiante();
	TipoIdentificacion tipoIdConvertido = this.onConvertirTipoIdentificacionAcademico(persona.getTipoIdentificacion());
	PlanEstudios planEstudios = ReciboConsignacionHelper.getHelper().getPlanEstudiosAcademico(programa.getIdPrograma());
	PeriodoAcademico periodoAcademico = new PeriodoAcademico();
	periodoAcademico.setPeriodoAcademico(this.getPeriodoEducacionContinuada());
	CategoriaMatricula categoriaMatricula = ReciboConsignacionHelper.getHelper().getCategoriaMatricula(programa.getIdPrograma());
	ConceptoAcademico conceptoAcademico = ReciboConsignacionHelper.getHelper().getConceptoAcademico(categoriaMatricula.getIdCategoria().toString());
	estudianteNuevo.setIdEstudiante(estudianteAntiguo.getIdEstudiante());
	estudianteNuevo.setNumeroCarne(persona.getCliente().getCliente());
	estudianteNuevo.setDocumentoEstudiante(persona.getIdentificacion());
	estudianteNuevo.setNombreEstudiante(persona.getNombreRazonSocial());
	estudianteNuevo.setApellidosEstudiante((persona.getPrimerApellido()+" "+persona.getSegundoApellido()).trim());
	estudianteNuevo.setEmail(persona.getDireccionElectronica());
	estudianteNuevo.setEmailOtro(persona.getDireccionElectronica());
	estudianteNuevo.setCelularEstudiante(persona.getTelefonoResidencia());
	estudianteNuevo.setSexo(persona.getSexo());
	estudianteNuevo.setTipoDocumento(tipoIdConvertido);
	estudianteNuevo.setDireccionCorrespondencia(persona.getDireccionResidencia());
	estudianteNuevo.setTelefonoCorrespondencia(persona.getTelefonoResidencia());
	estudianteNuevo.setFechaNacimiento(new Date());
	estudianteNuevo.setProgramaAcademico(programa);
	estudianteNuevo.setPeriodoIngreso(periodoAcademico);
	estudianteNuevo.setPlanEstudios(planEstudios);
	estudianteNuevo.setCategoriaMatricula(categoriaMatricula); // falta averiguarla, preguntar mauricio como se almacena
	estudianteNuevo.setEstado(IConstantes.ESTADO_EDUCACION_CONTINUADA);
	estudianteNuevo.setPazSalvo(IConstantes.PAZ_SALVO_EDUCACION_CONTINUADA); //preguntar esta parte a Mauricio
	estudianteNuevo.setClaveNumero(persona.getIdentificacion());
	estudianteNuevo.setSemestreNivel(1); // preguntar este dato a Mauricio
	estudianteNuevo.setConceptoAcademico(conceptoAcademico);
	
	log.info("datos: id: "+estudianteNuevo.getIdEstudiante()+" cod: "+estudianteNuevo.getNumeroCarne()+" doc: "+estudianteNuevo.getDocumentoEstudiante());
	
	// armamos creaci�n de la orden en escolaris
	OrdenAcademico orden = new OrdenAcademico();
	orden.setEstudiante(estudianteNuevo);
	orden.setPeriodoAcademico(periodoAcademico);
	orden.setValorTotal(categoriaMatricula.getValor());
	orden.setIdOrden(this.getIdOrdenes());
	
	//Armamos listado de registros estudiante Solicitud
	List<EstudianteSolicitud> listaEstSolic = new ArrayList<EstudianteSolicitud>();
	//registro inscrito
	EstudianteSolicitud estSolic = new EstudianteSolicitud();
	estSolic.setIdEstudianteSolicitud(this.getIdEstSolic());
	estSolic.setEstudiante(estudianteNuevo);
	estSolic.setProgramaAcademico(programa);
	estSolic.setPeriodoAcademico(periodoAcademico);
	estSolic.setEstado(IConstantes.ESTADO_INSCRITO_EDUCACION_CONTINUADA);
	listaEstSolic.add(estSolic);
	// registro present� documentos
	estSolic = new EstudianteSolicitud();
	estSolic.setIdEstudianteSolicitud(this.getIdEstSolic());
	estSolic.setEstudiante(estudianteNuevo);
	estSolic.setProgramaAcademico(programa);
	estSolic.setPeriodoAcademico(periodoAcademico);
	estSolic.setEstado(IConstantes.ESTADO_DOCUMENTOS_EDUCACION_CONTINUADA);
	listaEstSolic.add(estSolic);
	// registro admitido
	estSolic = new EstudianteSolicitud();
	estSolic.setIdEstudianteSolicitud(this.getIdEstSolic());
	estSolic.setEstudiante(estudianteNuevo);
	estSolic.setProgramaAcademico(programa);
	estSolic.setPeriodoAcademico(periodoAcademico);
	estSolic.setEstado(IConstantes.ESTADO_ADMITIDO_EDUCACION_CONTINUADA);
	listaEstSolic.add(estSolic);
	
	try{
	ReciboConsignacionHelper.getHelper().actualizarEstudianteEducacionContinuadaId(estudianteNuevo);
	ReciboConsignacionHelper.getHelper().crearOrdenAcademico(orden);
	for(EstudianteSolicitud es : listaEstSolic){
		ReciboConsignacionHelper.getHelper().crearEstadoSolicitudAcademico(es);
	}
	}catch(Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
	}
	
	
}

public void deshabilitarCamposFormularioPersona() {
	Button botonNuevo = this.getIdBtnRecibirInscripcionNuevoPatrocinador();
	Button botonInscribir = this.getIdBtnRecibirInscripcionInscribir();
	Intbox ibxIdentificacion = this.getTxtRecibirInscripcionIdentificacion();
	Combobox cbxUbicacion = this.getCbxRecibirInscripcionUbicacion();
	Combobox cbxEmpresa = this.getIdCbxRecibirInscripcionPatrocinador();
	Listbox lbxTipoIdentificacion = this.getLbxRecibirInscripcionTipoIdentificacion();
	Listbox lbxGenero = this.getLbxRecibirInscripcionSexo();
	Listbox lbxEscolaridad = this.getLbxRecibirInscripcionEscolaridad();
	Listbox lbxOcupacion = this.getLbxRecibirInscripcionOcupacion();
	Listbox lbxPrograma = this.getLbxRecibirInscripcionPrograma();
	Textbox txtNombre = this.getTxtRecibirInscripcionNombres();
	Textbox txtPrimerApellido = this.getTxtRecibirInscripcionPrimerApellido();
	Textbox txtSegundoApellido = this.getTxtRecibirInscripcionSegundoApellido();
	Textbox txtTelefono = this.getTxtRecibirInscripcionTelefono();
	Textbox txtCorreo = this.getTxtRecibirInscripcionDireccionElectronica();
	Textbox txtDireccion = this.getTxtRecibirInscripcionDireccionContacto();
	Textbox txtCargo = this.getIdTxtRecibirInscripcionCargo();
	Checkbox chbxCondiciones = this.getChbxRecibirInscripcionAceptarCondiciones();
	Checkbox chbxPersonal = this.getChbxRecibirInscripcionAceptarPersonal();
	Checkbox chbxEmpresa = this.getChbxRecibirInscripcionPatrocinado();
	
	botonNuevo.setDisabled(true);
	botonInscribir.setDisabled(true);
	ibxIdentificacion.setDisabled(true);
	cbxUbicacion.setDisabled(true);
	cbxEmpresa.setDisabled(true);
	lbxTipoIdentificacion.setDisabled(true);
	lbxGenero.setDisabled(true);
	lbxEscolaridad.setDisabled(true);
	lbxOcupacion.setDisabled(true);
	lbxPrograma.setDisabled(true);
	txtNombre.setDisabled(true);
	txtPrimerApellido.setDisabled(true);
	txtSegundoApellido.setDisabled(true);
	txtTelefono.setDisabled(true);
	txtCorreo.setDisabled(true);
	txtDireccion.setDisabled(true);
	txtCargo.setDisabled(true);
	chbxCondiciones.setDisabled(true);
	chbxPersonal.setDisabled(true);
	chbxEmpresa.setDisabled(true);
}

@SuppressWarnings("unused")
public void limpiarCamposFormularioPersona() {
	
	Intbox ibxIdentificacion = this.getTxtRecibirInscripcionIdentificacion();
	Listbox lbxTipoIdentificacion = this.getLbxRecibirInscripcionTipoIdentificacion();
	Listbox lbxGenero = this.getLbxRecibirInscripcionSexo();
	Listbox lbxEscolaridad = this.getLbxRecibirInscripcionEscolaridad();
	Listbox lbxOcupacion = this.getLbxRecibirInscripcionOcupacion();
	Textbox txtNombre = this.getTxtRecibirInscripcionNombres();
	Textbox txtPrimerApellido = this.getTxtRecibirInscripcionPrimerApellido();
	Textbox txtSegundoApellido = this.getTxtRecibirInscripcionSegundoApellido();
	Textbox txtTelefono = this.getTxtRecibirInscripcionTelefono();
	Textbox txtCorreo = this.getTxtRecibirInscripcionDireccionElectronica();
	Textbox txtDireccion = this.getTxtRecibirInscripcionDireccionContacto();
	Textbox txtCargo = this.getIdTxtRecibirInscripcionCargo();
	Textbox txtUniversidad = this.getTxtRecibirInscripcionUniversidadProcedencia();
	Checkbox chbxCondiciones = this.getChbxRecibirInscripcionAceptarCondiciones();
	Checkbox chbxPersonal = this.getChbxRecibirInscripcionAceptarPersonal();
	Checkbox chbxEmpresa = this.getChbxRecibirInscripcionPatrocinado();
	Radio radioEst = this.getChbxRecibirInscripcionCasillaEstudianteRadio1();
	Radio radioPar = this.getChbxRecibirInscripcionCasillaEstudianteRadio2();
	Radio radioS = this.getChbxRecibirInscripcionCasillaCenaRadio1();
	Radio radioN = this.getChbxRecibirInscripcionCasillaCenaRadio2();
	
	//ibxIdentificacion.setConstraint("");
	//ibxIdentificacion.setValue(null);
	//ibxIdentificacion.setConstraint("no empty");
	
	txtUniversidad.setConstraint("");
	txtUniversidad.setValue(null);
	txtUniversidad.setDisabled(false);
	txtUniversidad.setConstraint("no empty");
	
	
	lbxTipoIdentificacion.setSelectedIndex(0);
	lbxTipoIdentificacion.setDisabled(false);
	lbxGenero.setSelectedIndex(0);
	lbxGenero.setDisabled(false);
	lbxEscolaridad.setSelectedIndex(0);
	lbxOcupacion.setSelectedIndex(0);
	
	txtNombre.setConstraint("");
	txtNombre.setValue(null);
	txtNombre.setDisabled(false);
	txtNombre.setConstraint("no empty");
	
	txtPrimerApellido.setConstraint("");
	txtPrimerApellido.setValue(null);
	txtPrimerApellido.setDisabled(false);
	txtPrimerApellido.setConstraint("no empty");
	
	txtSegundoApellido.setValue(null);
	txtSegundoApellido.setDisabled(false);
	
	txtTelefono.setConstraint("");
	txtTelefono.setValue(null);
	txtTelefono.setConstraint("no empty");
	
	txtCorreo.setConstraint("");
	txtCorreo.setValue(null);
	txtCorreo.setConstraint("no empty");
	
	txtDireccion.setConstraint("");
	txtDireccion.setValue(null);
	txtDireccion.setConstraint("no empty");
	
	txtCargo.setValue(null);
	
	chbxCondiciones.setChecked(false);
	chbxCondiciones.setValue(0);
	
	chbxPersonal.setChecked(false);
	chbxPersonal.setValue(0);
	
	chbxEmpresa.setChecked(false);
	chbxEmpresa.setValue(0);
	
	radioEst.setDisabled(false);
	radioEst.setChecked(false);
	
	radioPar.setDisabled(false);
	radioPar.setChecked(true);
	
	radioS.setDisabled(false);
	radioS.setChecked(false);
	
	radioN.setDisabled(false);
	radioN.setChecked(true);
	
	this.setPersona(null);
	this.setPatrocinador(null);
	this.setPatrocinado(null);
	this.setListaRecibosEduCont(new ArrayList<ReciboConsignacion>());
	this.setListaRecibosEduContAnt(new ArrayList<ReciboConsignacion>());
	this.setListaRecibosEduContPagos(new ArrayList<ReciboConsignacion>());
}

public void insertRegistroEstudiante(Persona persona){
	log.info("[recibirInscripcion.insertRegistroEstudiante]");
	Estudiante estudianteNuevo = new Estudiante();
	TipoIdentificacion tipoIdConvertido = this.onConvertirTipoIdentificacionAcademico(persona.getTipoIdentificacion());
	ProgramaAcademico programa = persona.getCliente().getPrograma();
	PlanEstudios planEstudios = ReciboConsignacionHelper.getHelper().getPlanEstudiosAcademico(programa.getIdPrograma());
	PeriodoAcademico periodoAcademico = new PeriodoAcademico();
	periodoAcademico.setPeriodoAcademico(this.getPeriodoEducacionContinuada());
	CategoriaMatricula categoriaMatricula = ReciboConsignacionHelper.getHelper().getCategoriaMatricula(programa.getIdPrograma());
	ConceptoAcademico conceptoAcademico = ReciboConsignacionHelper.getHelper().getConceptoAcademico(categoriaMatricula.getIdCategoria().toString());
	estudianteNuevo.setIdEstudiante(this.getIdEstudiante());
	estudianteNuevo.setNumeroCarne(persona.getCliente().getCliente());
	estudianteNuevo.setDocumentoEstudiante(persona.getIdentificacion());
	estudianteNuevo.setNombreEstudiante(persona.getNombreRazonSocial());
	if(persona.getSegundoApellido()!=null){
		if(!persona.getSegundoApellido().equalsIgnoreCase("")){
			estudianteNuevo.setApellidosEstudiante((persona.getPrimerApellido()+" "+persona.getSegundoApellido().trim()));
		} else{
			estudianteNuevo.setApellidosEstudiante(persona.getPrimerApellido());
		}
	} else {
		estudianteNuevo.setApellidosEstudiante(persona.getPrimerApellido());
	}
	
	estudianteNuevo.setEmail(persona.getDireccionElectronica());
	estudianteNuevo.setEmailOtro(persona.getDireccionElectronica());
	estudianteNuevo.setCelularEstudiante(persona.getTelefonoResidencia());
	estudianteNuevo.setSexo(persona.getSexo());
	estudianteNuevo.setTipoDocumento(tipoIdConvertido);
	estudianteNuevo.setDireccionCorrespondencia(persona.getDireccionResidencia());
	estudianteNuevo.setTelefonoCorrespondencia(persona.getTelefonoResidencia());
	estudianteNuevo.setFechaNacimiento(new Date());
	estudianteNuevo.setProgramaAcademico(programa);
	estudianteNuevo.setPeriodoIngreso(periodoAcademico);
	estudianteNuevo.setPlanEstudios(planEstudios);
	estudianteNuevo.setCategoriaMatricula(categoriaMatricula); // falta averiguarla, preguntar mauricio como se almacena
	estudianteNuevo.setEstado(IConstantes.ESTADO_EDUCACION_CONTINUADA);
	estudianteNuevo.setPazSalvo(IConstantes.PAZ_SALVO_EDUCACION_CONTINUADA); //preguntar esta parte a Mauricio
	estudianteNuevo.setClaveNumero(persona.getIdentificacion());
	estudianteNuevo.setSemestreNivel(1); // preguntar este dato a Mauricio
	estudianteNuevo.setConceptoAcademico(conceptoAcademico);
	
	// armamos creaci�n de la orden en escolaris
	OrdenAcademico orden = new OrdenAcademico();
	orden.setEstudiante(estudianteNuevo);
	orden.setPeriodoAcademico(periodoAcademico);
	orden.setValorTotal(categoriaMatricula.getValor());
	orden.setIdOrden(this.getIdOrdenes());
	
	//Armamos listado de registros estudiante Solicitud
	List<EstudianteSolicitud> listaEstSolic = new ArrayList<EstudianteSolicitud>();
	//registro inscrito
	EstudianteSolicitud estSolic = new EstudianteSolicitud();
	estSolic.setIdEstudianteSolicitud(this.getIdEstSolic());
	estSolic.setEstudiante(estudianteNuevo);
	estSolic.setProgramaAcademico(programa);
	estSolic.setPeriodoAcademico(periodoAcademico);
	estSolic.setEstado(IConstantes.ESTADO_INSCRITO_EDUCACION_CONTINUADA);
	listaEstSolic.add(estSolic);
	// registro present� documentos
	estSolic = new EstudianteSolicitud();
	estSolic.setIdEstudianteSolicitud(this.getIdEstSolic());
	estSolic.setEstudiante(estudianteNuevo);
	estSolic.setProgramaAcademico(programa);
	estSolic.setPeriodoAcademico(periodoAcademico);
	estSolic.setEstado(IConstantes.ESTADO_DOCUMENTOS_EDUCACION_CONTINUADA);
	listaEstSolic.add(estSolic);
	// registro admitido
	estSolic = new EstudianteSolicitud();
	estSolic.setIdEstudianteSolicitud(this.getIdEstSolic());
	estSolic.setEstudiante(estudianteNuevo);
	estSolic.setProgramaAcademico(programa);
	estSolic.setPeriodoAcademico(periodoAcademico);
	estSolic.setEstado(IConstantes.ESTADO_ADMITIDO_EDUCACION_CONTINUADA);
	listaEstSolic.add(estSolic);
	
	try{
	ReciboConsignacionHelper.getHelper().crearEstudianteAcademico(estudianteNuevo);
	ReciboConsignacionHelper.getHelper().crearOrdenAcademico(orden);
	for(EstudianteSolicitud es : listaEstSolic){
		ReciboConsignacionHelper.getHelper().crearEstadoSolicitudAcademico(es);
	}
	}catch(Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
		
	}
	
}



public void testCodigoEstudiante(){
	log.info("C�digo: "+this.getCodigoEstudiante());
}


public Map<String,String> onBuscarAspirante(String identificacion){
    Map<String,String> mapaResult = new HashMap<String,String> ();
try{	
    String estudiante = "N";
    String egresado = "N";
    String funcionario = "N";
    
    egresado = ReciboConsignacionHelper.getHelper().evaluarEgresadoAcademicoAspirante(identificacion);
    estudiante = ReciboConsignacionHelper.getHelper().evaluarEstudianteAcademicoAspirante(identificacion);
    funcionario = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectValidarEmpleadoPersona", identificacion);
    
    if(estudiante==null || estudiante.equalsIgnoreCase("N")){
    	estudiante = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectValidarPersonaEstudiante", identificacion);
    }
    
    if(estudiante==null) {
    	estudiante ="N";
    }
    
    if(egresado ==null || egresado.equalsIgnoreCase("N")){
    	egresado = (String)ParametrizacionFac.getFacade().obtenerRegistro("selectValidarPersonaEgresado", identificacion);
    }
    
    if(egresado==null){
    	egresado ="N";	
    }
	
    if(funcionario==null){
    	funcionario ="N";
    }
    
    
    mapaResult.put("ESTUDIANTE", estudiante);
    mapaResult.put("EGRESADO", egresado);
    mapaResult.put("FUNCIONARIO", funcionario);
    
    log.info("Estudiante: "+estudiante+" - Egresado: "+egresado+" - Empleado: "+funcionario);

    
} catch(Exception e){
    e.printStackTrace();
    log.info(e.getMessage());
}
	return mapaResult;    

}


public void setListValuesPrograma(){
	log.info("[recibirInscripcion.setListValuesPrograma]");
	List<ProgramaAcademico> listaProgramas =  ReciboConsignacionHelper.getHelper().getListadoProgramasAcademicos();
	if(listaProgramas!=null){
		if(listaProgramas.size()>0){
			Listbox selectProgramas = this.getLbxRecibirInscripcionPrograma();
			for(ProgramaAcademico programa : listaProgramas){
				Listitem item = new Listitem();
				item.setLabel(programa.getNombrePrograma());
				item.setValue(programa.getIdPrograma());
				item.setAttribute("PROGRAMA", programa);
				selectProgramas.appendChild(item);
			}
		}
	}
}

@SuppressWarnings("unchecked")
public void setListValuesTipoIdentificacion(){
	log.info("[recibirInscripcion.setListValuesTipoIdentificacion]");
	try {
	List<TipoIdentificacion> listaTipoId = (List<TipoIdentificacion>)ParametrizacionFac.getFacade().obtenerListado("selectTipoIdentificacionNatural");
	if(listaTipoId != null){
		if(listaTipoId.size()>0){
			Listbox selectTipoId = this.getLbxRecibirInscripcionTipoIdentificacion();
			for(TipoIdentificacion tipoId : listaTipoId){
				Listitem item = new Listitem();
				item.setLabel(tipoId.getNombreTipoIdentificacion());
				item.setValue(tipoId.getTipoIdentificacion());
				item.setAttribute("TIPO_IDENTIFICACION",tipoId);
				selectTipoId.appendChild(item);
				}
			}
		}
	} catch (Exception e) {
		e.printStackTrace();
		log.error(e.getMessage());
	}
}

@SuppressWarnings("unchecked")
public void setListValuesTipoIdentificacionPatrocinador(){
	log.info("[recibirInscripcion.setListValuesTipoIdentificacionPatrocinador]");
	try {
		List<TipoIdentificacion> listaTipoId =  (List<TipoIdentificacion>)ParametrizacionFac.getFacade().obtenerListado("selectTipoIdentificacionJuridica");
		if(listaTipoId != null){
			if(listaTipoId.size()>0){
				Listbox selectTipoId = this.getIdLbxRecibirInscripcionTipoIdPatrocinador();
				for(TipoIdentificacion tipoId : listaTipoId){
					Listitem item = new Listitem();
					item.setLabel(tipoId.getNombreTipoIdentificacion());
					item.setValue(tipoId.getTipoIdentificacion());
					item.setAttribute("TIPO_IDENTIFICACION",tipoId);
					selectTipoId.appendChild(item);
				}
			}
		}
	} catch(Exception e) {
		e.printStackTrace();
		log.error(e.getMessage());
	}	
}



public void setListValuesPersonasPatrocinador(){
	log.info("[recibirInscripcion.setListValuesPersonasPatrocinador]");
	this.filterListPatrocinador("");	
}

@SuppressWarnings("unchecked")
public void filterListPatrocinador(String value){
	log.info("[recibirInscripcion.onFilterListPoblacion] [Valor pasado:"+value+"]");
	
	try {
	List<Persona> listaPersona = (List<Persona>)ParametrizacionFac.getFacade().obtenerListado("selectPersonaListBasicPatrocinador", value);
	Combobox selectPatrocinador = this.getIdCbxRecibirInscripcionPatrocinador();
	
	if(listaPersona != null){
		if(listaPersona.size()>0){
			selectPatrocinador.getChildren().clear();
			for(Persona persona: listaPersona){
				Comboitem item = new Comboitem();
				item.setLabel(persona.getNombreRazonSocial()+(persona.getPrimerApellido()!=null?" "+persona.getPrimerApellido():"")+
						(persona.getSegundoApellido()!=null?" "+persona.getSegundoApellido():"")+" - "+persona.getIdentificacion());
				item.setValue(persona.getIdentificacion());
				item.setAttribute("PERSONA",persona);
				selectPatrocinador.appendChild(item);
			}
			selectPatrocinador.invalidate();			
			log.info("items: "+selectPatrocinador.getItemCount());
		}
	}
	}catch(Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
		
	}
}

@SuppressWarnings("unchecked")
public void filterListPatrocinadorNew(String value){
	log.info("[recibirInscripcion.onFilterListPoblacion] [Valor pasado:"+value+"]");
	
	try {
	List<Persona> listaPersona = (List<Persona>)ParametrizacionFac.getFacade().obtenerListado("selectPersonaListBasicPatrocinador", value);
	Combobox selectPatrocinador = this.getIdCbxRecibirInscripcionPatrocinador();
	
	if(listaPersona != null){
		if(listaPersona.size()>0){
			ListModel<Persona> dictModel= new SimpleListModel<Persona>(listaPersona);
			selectPatrocinador.setModel(dictModel);			
			
		}
	}
	}catch(Exception e){
		e.printStackTrace();
		log.info(e.getMessage());
		
	}
}


public void setListValuesUbicacion(){
	log.info("[recibirInscripcion.setListValuesUbicacion]");
	this.filterListPoblacion("");	
}

public void filterListPoblacion(String value){
	log.info("[recibirInscripcion.onFilterListPoblacion]");
	List<Poblacion> listaPoblacion =  ReciboConsignacionHelper.getHelper().getListadoPoblacion(value);
	if(listaPoblacion != null){
		if(listaPoblacion.size()>0){
			Combobox selectPoblacion = this.getCbxRecibirInscripcionUbicacion();
			selectPoblacion.getChildren().clear();
			
			 
			for(Poblacion poblacion: listaPoblacion){
				Comboitem item = new Comboitem();
				item.setValue(poblacion.getDepartamento().getPais().getPais().toString()+"-"+poblacion.getDepartamento().getDepartamento().toString()+
						"-"+poblacion.getPoblacion());
				item.setLabel(poblacion.getNombrePoblacion());
				item.setDescription(poblacion.getNombrePoblacion()+","+poblacion.getDepartamento().getNombreDepartamento()+","+
				poblacion.getDepartamento().getPais().getNombrePais());
				item.setAttribute("POBLACION", poblacion);
				selectPoblacion.appendChild(item);
			}

			log.info("items: "+selectPoblacion.getItemCount());
		}
	}
	
}

public void onValidarRadioAceptarCena() {
	Checkbox checkRadio = this.getChbxRecibirInscripcionCasillaCenaRadio1();
	log.info("Radio :" + checkRadio.isChecked());
//	if(!checkRadio.isChecked()) {
//		Clients.showNotification("Debe aceptar ir a la cena", Clients.NOTIFICATION_TYPE_ERROR, checkRadio, "end_before", 4000, true);
//		throw new WrongValueException("No se ha aceptado la Evento");
//	}if else {
		String tipoParticipante = this.getChbxRecibirInscripcionCasillaEstudianteRadio1().getRadiogroup().getSelectedItem().getValue().toString();
		if(tipoParticipante.equalsIgnoreCase("E")) {
			Textbox Uprocedencia = this.getTxtRecibirInscripcionUniversidadProcedencia();
			Uprocedencia.setConstraint("");
			if(Uprocedencia.getValue() == null || Uprocedencia.getValue().equalsIgnoreCase("")) {
				Clients.showNotification(this.appProperties.getProperty("mensajeUniversidad"), Clients.NOTIFICATION_TYPE_ERROR, checkRadio, "end_before", 4000, true);
				throw new WrongValueException(this.appProperties.getProperty("mensajeUniversidad"));
			}
			Uprocedencia.setConstraint("no empty");
		}
}

public void onValidarTerminosCondiciones(){
	log.info("[recibirInscripcion.onValidateTerminosCondiciones]");
	Checkbox cajaChequeo = this.getChbxRecibirInscripcionAceptarCondiciones();
	if(!cajaChequeo.isChecked()){
		Clients.showNotification(this.appProperties.getProperty("mensajeTerminosCondiciones"), Clients.NOTIFICATION_TYPE_ERROR, cajaChequeo, "end_before", 4000, true);
		throw new WrongValueException(this.appProperties.getProperty("mensajeTerminosCondiciones"));
	}
}

public void onValidarTratamientoDatos(){
	log.info("[recibirInscripcion.onValidateTratamientoDatos]");
	Checkbox cajaChequeo = this.getChbxRecibirInscripcionAceptarPersonal();
	if(!cajaChequeo.isChecked()){
		Clients.showNotification(this.appProperties.getProperty("mensajeTratamientoDatos"), Clients.NOTIFICATION_TYPE_ERROR, cajaChequeo, "end_before", 4000, true);
		throw new WrongValueException(this.appProperties.getProperty("mensajeTratamientoDatos"));
	}
}


public void onValidarCheckApoyoEmpresa(){
	log.info("[recibirInscripcion.onValidarCheckApoyoEmpresa]");
	Checkbox cajaChequeo = this.getChbxRecibirInscripcionPatrocinado();
	if(!cajaChequeo.isChecked() && this.getIdCbxRecibirInscripcionPatrocinador().getSelectedItem() != null){
		Clients.showNotification(this.appProperties.getProperty("mensajeCheckEmpresa"), Clients.NOTIFICATION_TYPE_ERROR, cajaChequeo, "end_before", 4000, true);
		throw new WrongValueException(this.appProperties.getProperty("mensajeCheckEmpresa"));
	}
}

public void onValidarTerminosCondicionesNewPatrocinador(){
	log.info("[recibirInscripcion.onValidateTerminosCondicionesNewPatrocinador]");
	Checkbox cajaChequeo = this.getChbxRecibirInscripcionAceptarCondicionesNewPatrocinador();
	if(!cajaChequeo.isChecked()){
		Clients.showNotification(this.appProperties.getProperty("mensajeTerminosCondiconesPatrocinador"), Clients.NOTIFICATION_TYPE_ERROR, cajaChequeo, "end_before", 4000, true);
		throw new WrongValueException(this.appProperties.getProperty("mensajeTerminosCondiconesPatrocinador"));
	}
}

public void onValidarTratamientoDatosNewPatrocinador(){
	log.info("[recibirInscripcion.onValidateTratamientoDatosNewPatrocinador]");
	Checkbox cajaChequeo = this.getChbxRecibirInscripcionAceptarPersonalNewPatrocinador();
	if(!cajaChequeo.isChecked()){
		Clients.showNotification(this.appProperties.getProperty("mensajeTratamientoDatosPatrocinador"), Clients.NOTIFICATION_TYPE_ERROR, cajaChequeo, "end_before", 4000, true);
		throw new WrongValueException(this.appProperties.getProperty("mensajeTratamientoDatosPatrocinador"));
	}
}


public void onValidarGenero(){
	log.info("[recibirInscripcion.onValidateGenero]");
	Listbox listaGenero = this.getLbxRecibirInscripcionSexo();
	Listitem itemSeleccionado = (Listitem)listaGenero.getSelectedItem();
	if(itemSeleccionado.getValue().equals("0")){
		Clients.showNotification(this.appProperties.getProperty("mensajeGenero"), Clients.NOTIFICATION_TYPE_ERROR, listaGenero, "end_before", 4000, true);
		throw new WrongValueException(this.appProperties.getProperty("mensajeGenero"));
	}
} 

public void onValidarTipoIdentificacion(){
	log.info("[recibirInscripcion.onValidateTipoIdentificacion]");
	Listbox listaTipoIdentificacion = this.getLbxRecibirInscripcionTipoIdentificacion();
	Listitem itemSeleccionado = (Listitem)listaTipoIdentificacion.getSelectedItem();
	if(itemSeleccionado.getValue().equals("0")){
		Clients.showNotification(this.appProperties.getProperty("mensajeTipoIdentificacion"), Clients.NOTIFICATION_TYPE_ERROR, listaTipoIdentificacion, "end_before", 4000, true);
		throw new WrongValueException(this.appProperties.getProperty("mensajeTipoIdentificacion"));
	}
}

public void onValidarModalidadAsistencia(){
	log.info("[recibirInscripcion.onValidarModalidadAsistencia]");
	Listbox listaModalidad = this.getLbxRecibirInscripcionModalidadAsistencia();
	Listitem itemSeleccionado = (Listitem)listaModalidad.getSelectedItem();
	if(itemSeleccionado.getValue().equals("0") 
			&& this.getIdDivRecibirInscripcionModalidadAsistencia().isVisible()){
		Clients.showNotification(this.appProperties.getProperty("mensajeModalidad"), Clients.NOTIFICATION_TYPE_ERROR, listaModalidad, "end_before", 4000, true);
		throw new WrongValueException(this.appProperties.getProperty("mensajeModalidad"));
	}
}


public void onValidarTipoIdentificacionPatrocinador(){
	log.info("[recibirInscripcion.onValidarTipoIdentificacionPatrocinador]");
	Listbox listaTipoIdentificacion = this.getIdLbxRecibirInscripcionTipoIdPatrocinador();
	Listitem itemSeleccionado = (Listitem)listaTipoIdentificacion.getSelectedItem();
	if(itemSeleccionado.getValue().equals("0")){
		Clients.showNotification(this.appProperties.getProperty("mensajeTipoIdentificacionPatrocinador"), Clients.NOTIFICATION_TYPE_ERROR, listaTipoIdentificacion, "end_before", 4000, true);
		throw new WrongValueException(this.appProperties.getProperty("mensajeTipoIdentificacionPatrocinador"));
	}
}

public void onValidarCheckEmpresaPatrocinadora() {
	log.info("[recibirInscripcion.onValidarCheckEmpresaPatrocinadora]");
	if(Integer.valueOf(this.getChbxRecibirInscripcionPatrocinado().getValue().toString())>0) {
		if(this.getIdCbxRecibirInscripcionPatrocinador().getSelectedItem()==null) {
			Clients.showNotification(this.appProperties.getProperty("mensajeCheckEmpresaPatrocinadora"), Clients.NOTIFICATION_TYPE_ERROR, 
					this.getIdCbxRecibirInscripcionPatrocinador(), "end_before", 4000, true);
			throw new WrongValueException(this.appProperties.getProperty("mensajeCheckEmpresaPatrocinadora"));
			
		}
	}
	
	
}


public void onValidarPrograma(){
	log.info("[recibirInscripcion.onValidatePrograma]");
	Listbox listaTipoIdentificacion = this.getLbxRecibirInscripcionPrograma();
	Listitem itemSeleccionado = (Listitem)listaTipoIdentificacion.getSelectedItem();
	if(itemSeleccionado.getValue().equals("0")){
		Clients.showNotification(this.appProperties.getProperty("mensajePrograma"), Clients.NOTIFICATION_TYPE_ERROR, listaTipoIdentificacion, "end_before", 4000, true);
		throw new WrongValueException(this.appProperties.getProperty("mensajePrograma"));
	}
}


public void onValidarOcupacion(){
	String tipo = Executions.getCurrent().getDesktop().getSession().getAttribute("tipo").toString();
	log.info("[recibirInscripcion.onValidateOcupacion]");
	Listbox listaOcupacion = this.getLbxRecibirInscripcionOcupacion();
	Listitem itemSeleccionado = (Listitem)listaOcupacion.getSelectedItem();
	if(itemSeleccionado.getValue().equals("0") && !tipo.equalsIgnoreCase("e")){
		Clients.showNotification(this.appProperties.getProperty("mensajeOcupacion"), Clients.NOTIFICATION_TYPE_ERROR, listaOcupacion, "end_before", 4000, true);
		throw new WrongValueException(this.appProperties.getProperty("mensajeOcupacion"));
	}
}


public void onValidarEscolaridad(){
	String tipo = Executions.getCurrent().getDesktop().getSession().getAttribute("tipo").toString();
	log.info("[recibirInscripcion.onValidarEscolaridad]");
	Listbox listaEscolaridad = this.getLbxRecibirInscripcionEscolaridad();
	Listitem itemSeleccionado = (Listitem)listaEscolaridad.getSelectedItem();
	if(itemSeleccionado.getValue().equals("0") && !tipo.equalsIgnoreCase("e")){
		Clients.showNotification(this.appProperties.getProperty("mensajeEscolaridad"), Clients.NOTIFICATION_TYPE_ERROR, listaEscolaridad, "end_before", 4000, true);
		throw new WrongValueException(this.appProperties.getProperty("mensajeEscolaridad"));
	}
}


public Map<String, Object> onValidarFormularioDatosEmpresaNuevoPatrocinador() {
	Map<String, Object> mapaValidar = new HashMap<String,Object>(); 
	boolean validar = true;
	String error = "Por favor diligencie los campos: ";
	
	Listbox listaTipoIdentificacion = this.getIdLbxRecibirInscripcionTipoIdPatrocinador();
	Listitem itemSeleccionado = (Listitem)listaTipoIdentificacion.getSelectedItem();
	if(itemSeleccionado.getValue().equals("0")){
		validar = false;
		error = error+"<br> Tipo identificaci�n.";
	}
	
	
	Intbox ibxIdentificacion = this.getIdIbxRecibirInscripcionNitPatrocinador();
	if(ibxIdentificacion.getValue()==null || ibxIdentificacion.getValue()<=0) {
		validar = false;
		error = error+"<br> Identificaci�n.";
	}
	
	Textbox txtNombre = this.getIdTxtRecibirInscripcionNombrePatrocinador();
	txtNombre.setConstraint("");
	if(txtNombre.getValue()==null || txtNombre.getValue().equals("")) {
		validar = false;
		error = error+"<br> Nombre / Raz�n Social.";
	}
	
	Textbox txtTelefono = this.getIdTxtRecibirInscripcionTelefonoPatrocinador();
	txtTelefono.setConstraint("");
	if(txtTelefono.getValue()==null || txtTelefono.getValue().equals("")) {
		validar = false;
		error = error+"<br> Tel�fono contacto.";
	}
	
	
	Textbox txtNombreContacto = this.getIdTxtRecibirInscripcionNombreContactoPatrocinador();
	txtNombreContacto.setConstraint("");
	if(txtNombreContacto.getValue()==null || txtNombreContacto.getValue().equals("")) {
		validar = false;
		error = error+"<br> Nombre Contacto.";
	}
	
	Textbox txtArchivo = this.getIdTxtRecibirInscripcionArchivoRutPatrocinador();
	txtArchivo.setConstraint("");
	if(txtArchivo.getValue()==null || txtArchivo.getValue().equals("")) {
		validar = false;
		error = error+"<br> Archivo Rut.";
	}
	
	Textbox txtCorreo = this.getIdTxtRecibirInscripcionEmailPatrocinador();
	txtCorreo.setConstraint("");
	if(txtCorreo.getValue()==null ||  txtCorreo.getValue().equals("")) {
		validar = false;
		error = error+"<br> Direcci�n electr�nica contacto.";
	}
	
	
	if (!this.getChbxRecibirInscripcionAceptarCondicionesNewPatrocinador().isChecked()) {
		validar = false;
		error = error+"<br> Aceptar condiciones del programa.";	
	}
	
	if (!this.getChbxRecibirInscripcionAceptarPersonalNewPatrocinador().isChecked()) {
		validar = false;
		error = error+"<br> Aceptar tratamiento de datos personales.";
	}
	
	if(validar) {
		error = "OK";
	}
	
	txtNombre.setConstraint("no empty");
	txtTelefono.setConstraint("no empty");
	txtArchivo.setConstraint("no empty");
	txtNombreContacto.setConstraint("no empty");
	txtCorreo.setConstraint("no empty, /.+@.+\\.[a-z,A-Z]+/: Verifique su e-mail");
	mapaValidar.put("VALIDACION", validar);
	mapaValidar.put("ERROR", error);
	return mapaValidar;
}

public Map<String, Object> onValidarFormularioDatosPersonalesNuevoPatrocinador() {
	Map<String, Object> mapaValidar = new HashMap<String,Object>(); 
	boolean validar = true;
	String error = "Por favor diligencie los campos: ";
	if (this.getTxtRecibirInscripcionIdentificacion().getValue()==null || this.getTxtRecibirInscripcionIdentificacion().getValue()<=0) {
		validar = false;
		error = error+"<br> Identificaci�n.";
	}
	
	Listbox listaTipoIdentificacion = this.getLbxRecibirInscripcionTipoIdentificacion();
	Listitem itemSeleccionado = (Listitem)listaTipoIdentificacion.getSelectedItem();
	if(itemSeleccionado.getValue().equals("0")){
		validar = false;
		error = error+"<br> Tipo identificaci�n.";
	}
	
	Listbox listaGenero = this.getLbxRecibirInscripcionSexo();
	Listitem itemSeleccionadoGenero = (Listitem)listaGenero.getSelectedItem();
	if(itemSeleccionadoGenero.getValue().equals("0")){
		validar = false;
		error = error+"<br> G�nero";
	}
	
	if(this.getTxtRecibirInscripcionNombres().getValue()==null ||this.getTxtRecibirInscripcionNombres().getValue().equals("")) {
		validar = false;
		error = error+"<br> Nombres.";
	}
	
	if(this.getTxtRecibirInscripcionPrimerApellido().getValue()==null ||this.getTxtRecibirInscripcionPrimerApellido().getValue().equals("")) {
		validar = false;
		error = error+"<br> Primer apellido.";
	}
	
	if(this.getTxtRecibirInscripcionTelefono().getValue()==null ||this.getTxtRecibirInscripcionTelefono().getValue().equals("")) {
		validar = false;
		error = error+"<br> Tel�fono m�vil.";
		
	}
	
	if(this.getTxtRecibirInscripcionDireccionElectronica().getValue()==null ||this.getTxtRecibirInscripcionDireccionElectronica().getValue().equals("")) {
		validar = false;
		error = error+"<br> Direcci�n electr�nica.";
	}
	
	if(this.getTxtRecibirInscripcionDireccionContacto().getValue()==null ||this.getTxtRecibirInscripcionDireccionContacto().getValue().equals("")) {
		validar = false;
		error = error+"<br> Direcci�n de contacto.";
	}
	
	Combobox comboPoblacion = this.getCbxRecibirInscripcionUbicacion();
	Comboitem itemPoblacion = comboPoblacion.getSelectedItem();
	if(itemPoblacion ==null ) {
		validar = false;
		error = error+"<br> Ciudad.";	
	}
	
	Listbox listaEscolaridad = this.getLbxRecibirInscripcionEscolaridad();
	Listitem itemSeleccionadoEscolaridad = (Listitem)listaEscolaridad.getSelectedItem();
	if(itemSeleccionadoEscolaridad.getValue().equals("0")){
		validar = false;
		error = error+"<br> Grado escolaridad.";
	}
	
	Listbox listaOcupacion = this.getLbxRecibirInscripcionOcupacion();
	Listitem itemSeleccionadoOcupacion = (Listitem)listaOcupacion.getSelectedItem();
	if(itemSeleccionadoOcupacion.getValue().equals("0")){
		validar = false;
		error = error+"<br> Ocupaci�n.";
	}
	
	Listbox listaPrograma = this.getLbxRecibirInscripcionPrograma();
	Listitem itemSeleccionadoPrograma = (Listitem)listaPrograma.getSelectedItem();
	if(itemSeleccionadoPrograma.getValue().equals("0")){
		validar = false;
		error = error+"<br> Programa.";
	}
	
	Checkbox checkFacturaPatrocinador = this.getChbxRecibirInscripcionPatrocinado();
	if(!checkFacturaPatrocinador.isChecked()) {
		validar = false;
		error =error+"<br> Cuadro de chequeo con etiqueta: \"requiere factura para la empresa que representa\"";
	}
	
	if(validar) {
		error = "OK";
	}
	mapaValidar.put("VALIDACION", validar);
	mapaValidar.put("ERROR", error);
	return mapaValidar;
}

public void onImprimir(){
	Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupInscripcionEduCont");
	if(radioGroup.getItemCount()>0){
		Radio radio = radioGroup.getSelectedItem();
		ReciboConsignacion recibo = (ReciboConsignacion)radio.getAttribute("RECIBO_CONSIGNACION");
		this.onImprimirReporte(recibo);
	}
}


public void onSelectMoneda(){
	log.info("Ejecutando el metodo [ onSelectMoneda ]... ");
	this.onSetImageCurrency();
	this.buscarMaestro();
	
}



public void fijarTrmSistema(){
	Map<String, Object> infoSesion = new HashMap<String, Object>();
	
	// Significa que se hace en moneda extranjera se verifica que haya subido la del d�a DOLARES - PESOS
		Map<String, Object> mapaResultDolar = new HashMap<String,Object>();
		Map<String, Object> mapaResultEuro = new HashMap<String,Object>();
		mapaResultDolar.put("FECHA", new SimpleDateFormat(IConstantes.FORMATO_FECHA).format(new Date()));
		mapaResultDolar.put("MONEDA_ORIGEN", IConstantes.DOLARES_AMERICANOS);
		mapaResultDolar.put("MONEDA_DESTINO", IConstantes.PESOS_COLOMBIANOS_ICEBERG);
		mapaResultEuro.put("FECHA",new SimpleDateFormat(IConstantes.FORMATO_FECHA).format(new Date()));
		mapaResultEuro.put("MONEDA_ORIGEN", IConstantes.EUROS);
		mapaResultEuro.put("MONEDA_DESTINO", IConstantes.DOLARES_AMERICANOS);
		
		try {
			mapaResultDolar.put("FACTOR", (BigDecimal)ParametrizacionFac.getFacade().obtenerRegistro("getCurrencyFecha", mapaResultDolar));
			mapaResultEuro.put("FACTOR", (BigDecimal)ParametrizacionFac.getFacade().obtenerRegistro("getCurrencyFecha", mapaResultEuro));
	} catch (Exception e) {
			e.printStackTrace();
	}
		
			
		if(mapaResultDolar.get("FACTOR") == null){
		mapaResultDolar = (Map<String,Object>)ReciboConsignacionHelper.getHelper().setConversionHistorica(IConstantes.FORMATO_FECHA, 
				IConstantes.DOLARES_AMERICANOS,IConstantes.PESOS_COLOMBIANOS_ICEBERG, new Date());
		}
		if(mapaResultEuro.get("FACTOR")== null){
		mapaResultEuro = (Map<String,Object>)ReciboConsignacionHelper.getHelper().setConversionHistorica(IConstantes.FORMATO_FECHA, 
				IConstantes.EUROS,IConstantes.DOLARES_AMERICANOS, new Date());
		}

		log.info("Factor Dolar login: "+mapaResultDolar.get("FACTOR"));
		log.info("Factor Euro login: "+mapaResultEuro.get("FACTOR"));
		
		if(mapaResultDolar.get("FACTOR")==null){
			mapaResultDolar.put("FACTOR", new BigDecimal(0.00));
		}
		
		if(mapaResultEuro.get("FACTOR")==null){
			mapaResultEuro.put("FACTOR", new BigDecimal(0.00));
		}
			//Obtener tasas representativas del Mercado
			Double trm = ((BigDecimal)mapaResultDolar.get("FACTOR")).doubleValue();

			TasaRepresentativaMercado  tasaRepMercadoDolar = new TasaRepresentativaMercado();
			tasaRepMercadoDolar.setValor(trm);
			tasaRepMercadoDolar.setFecha(new Date());
			Moneda monedaDolar = new Moneda();
			monedaDolar.setCodigo(IConstantes.DOLARES_AMERICANOS);
			tasaRepMercadoDolar.setMoneda(monedaDolar);
			Map<String, String> mapaParametro = new HashMap<String, String>();
			mapaParametro.put("COMISION_RECUPERAR",IConstantes.PARAMETRO_DOLAR);
			String comisionEuro = new String("0");
			String comisionDolar = new String("0");
	
			try {
		
				comisionDolar = (String)ParametrizacionFac.getFacade().obtenerRegistro("seleccionarComisionNegociada", mapaParametro );
				mapaParametro.clear();
				mapaParametro.put("COMISION_RECUPERAR",IConstantes.PARAMETRO_EURO );
				comisionEuro = (String)ParametrizacionFac.getFacade().obtenerRegistro("seleccionarComisionNegociada", mapaParametro );
			} catch (Exception e) {
	
				e.printStackTrace();
			}
			log.info("comisi�n D�lar: "+comisionDolar);
			infoSesion.put(IConstantes.COMISION_DOLAR,comisionDolar);
			log.info("comisi�n Euro: "+comisionEuro);
			infoSesion.put(IConstantes.COMISION_EURO,comisionEuro);
			
			infoSesion.put(IConstantes.TRM_DOLAR, tasaRepMercadoDolar); 		

			//Double trmEuroDolar =ReciboConsignacionHelper.getHelper().getConversion(Currency.EUR, Currency.USD);
			Double trmEuroDolar =((BigDecimal)mapaResultEuro.get("FACTOR")).doubleValue();
	
			log.info("Euro - DOLAR TRM: "+trmEuroDolar);
	
			TasaRepresentativaMercado tasaRepMercadoEuro = new TasaRepresentativaMercado();
			log.info("Tasa EUR - COP"+(trmEuroDolar*trm));
			tasaRepMercadoEuro.setValor(((Double)(trmEuroDolar*trm)));
			tasaRepMercadoEuro.setFecha(new Date());
			Moneda monedaEuro = new Moneda();
			monedaEuro.setCodigo(IConstantes.EUROS);
			tasaRepMercadoEuro.setMoneda(monedaEuro);		
	
			infoSesion.put(IConstantes.TRM_EURO, tasaRepMercadoEuro);
	
			this.getDesktop().getSession().setAttribute(IConstantes.INFORMACION_SESION,infoSesion);
}


@SuppressWarnings({ "unchecked"})
public void buscarMaestro(){
	log.info("Ejecutando el metodo [ buscarMaestro ]... ");
	List<ReciboConsignacion> listaDatos=null;
	
	 //String tipo = Executions.getCurrent().getDesktop().getSession().getAttribute("tipo").toString();
	try {
		
		Persona personaParametro = new Persona();
		personaParametro = this.getDatosFormularioPersona();
		ReciboConsignacion reciboConsignacion = new ReciboConsignacion();
		reciboConsignacion.setPersona(personaParametro);
		 
		listaDatos=(List<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("selectRecibosConsignacionOrdenesEduCont",reciboConsignacion); 
		
		this.setListaRecibosEduCont(listaDatos);
//	    if(tipo.equalsIgnoreCase("e") && this.getChbxRecibirInscripcionCasillaCenaRadio2().getRadiogroup().getSelectedItem().getValue().toString().equalsIgnoreCase("S")) {
//	    	this.generarInscripcionCena();
//	    	this.regenerarReciboConsignacion();
//			listaDatos=(List<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("selectRecibosConsignacionOrdenesEduCont",reciboConsignacion); 		
//			this.setListaRecibosEduCont(listaDatos);
//	    }
		
		Rows filas = (Rows)this.getFellow("idRecibirInscripcionEduContRowsPrincipal");
		filas.getChildren().clear();       
		Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupInscripcionEduCont");
		radioGroup.detach();
		radioGroup = new Radiogroup();
		radioGroup.setId("idRadioGroupInscripcionEduCont");
		((Groupbox)this.getFellow("idGbxFormPagarInscripcionEduCont")).appendChild(radioGroup);

		
		radioGroup.applyProperties();
		radioGroup.invalidate();
		log.info("numero de �tems en el radio Group: "+radioGroup.getItemCount());
		
		log.info("termino de ejecutar la consulta ");
		InscripcionAssembler inscripcionEduContAs = new InscripcionAssembler();
		if (listaDatos!=null){
		if (listaDatos.size()>0){
			for (Object object : listaDatos) {
				Row fila = inscripcionEduContAs.crearRowDesdeDto(object, this);
				filas.appendChild(fila);
				}
			log.info("Seleccionar primer detalle autom�ticamente");
			radioGroup.setSelectedIndex(0);
			}
		} 

        Grid tabla = (Grid) this.getFellow("idRecibirInscripcionEduContGrdPrincipal");
        
		// se configura la tabla....
		tabla.setMold("paging");
		tabla.setPageSize(IConstantes.TAMANO_PAGINACION);
		tabla.applyProperties();
		tabla.invalidate();
	}catch (Exception e) {
		e.printStackTrace();
	}
}

public void onImprimirReporte(ReciboConsignacion recibo){
	try{
		ProgramaReporte programaReporte = new ProgramaReporte();
		CentroCosto centroCosto = new CentroCosto();
		centroCosto.setCentroCosto(recibo.getOrden().getCentroCosto().getCentroCosto());
		programaReporte.setCentroCosto(centroCosto);
		programaReporte = (ProgramaReporte)ParametrizacionFac.getFacade().obtenerRegistro("selectProgramaReporte", programaReporte);
		Map<String,Object> mapaParametros = new HashMap<String, Object>();
		mapaParametros.put("ORDEN", recibo.getOrden().getOrden());
		mapaParametros.put("ORGANIZACION", recibo.getOrden().getOrganizacion());
		mapaParametros.put("DOCUMENTO", recibo.getOrden().getDocumento());
		mapaParametros.put("CLIENTE", recibo.getCliente().getCliente());
		ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarImpresionOrden", mapaParametros);
		PrintReportAction printReportAction = (PrintReportAction)Executions.createComponents("pages/printReport.zul", null, null);
		printReportAction.doModal(programaReporte!=null?programaReporte.getReporte():IConstantes.REP_ORDEN, recibo);
		
		
		}catch(Exception e){
			e.printStackTrace();
			Messagebox.show(e.getMessage(),
				    "Error de Pagos en L�nea",
				    Messagebox.YES, Messagebox.ERROR);
		}
	
}


public void onPagarEnLineaPayUW(){
	Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupInscripcionEduCont");
	if(radioGroup.getItemCount()>0){
		Integer  resultado = 0;
	 if(this.getLanguage().equalsIgnoreCase("es")) {
		 resultado = Messagebox.show(
				    "�Confirma este pago? "+IConstantes.MENSAJE_PERMITIR_POPUPS,
				    "Confirmar Inicio Pago",
				    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
	 }else if(this.getLanguage().equalsIgnoreCase("en")) {
		 resultado = Messagebox.show(
				    "Confirm this payment? "+IConstantes.MENSAJE_PERMITIR_POPUPS,
				    "Confirm this payment",
				    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
	 }

	if(resultado==Messagebox.YES){

		Radio radio = radioGroup.getSelectedItem();
		ReciboConsignacion recibo = (ReciboConsignacion)radio.getAttribute("RECIBO_CONSIGNACION");
		log.info("Recibo "+recibo.getReciboConsignacion());
		log.info("valor "+recibo.getValorTotal());
		log.info("valor detalle "+recibo.getValorDetalle());

	if (recibo!= null){
		try {
				ReciboConsignacionHelper.getHelper().sendPostPayU(recibo, IConstantes.TIPO_INVOCACION_DERECHOS_ACADEMICOS);
				ReciboConsignacionHelper.getHelper().registrarAuditoriaPayU(recibo, IConstantes.TIPO_INVOCACION_DERECHOS_ACADEMICOS);
			}catch(Exception e){
				
			}
				}
			}
		}
	
}

@SuppressWarnings({ "unchecked", "rawtypes" })
public void onPagarEnLinea(){
	
	Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupInscripcionEduCont");
	if(radioGroup.getItemCount()>0){
		Integer  resultado = 0;
	 if(this.getLanguage().equalsIgnoreCase("es")) {
		 resultado = Messagebox.show(
				    "�Confirma este pago? "+IConstantes.MENSAJE_PERMITIR_POPUPS,
				    "Confirmar Inicio Pago",
				    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
	 }else if(this.getLanguage().equalsIgnoreCase("en")) {
		 resultado = Messagebox.show(
				    "Confirm this payment? "+IConstantes.MENSAJE_PERMITIR_POPUPS,
				    "Confirm this payment",
				    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
	 }
	if(resultado==Messagebox.YES){

	String[] listServiciosMulti = null;
	String[] listNitMulti = null;
	double[] listValIvaMulti = null;
	double[] listIvaMulti = null;
	String wsResult;
	
	Radio radio = radioGroup.getSelectedItem();
	ReciboConsignacion recibo = (ReciboConsignacion)radio.getAttribute("RECIBO_CONSIGNACION");
	log.info("Recibo "+recibo.getReciboConsignacion());
	log.info("valor "+recibo.getValorTotal());
	log.info("valor detalle "+recibo.getValorDetalle());
	this.setZonaPagosLocator(new ZPagosLocator());
	this.setZonaPagosProxy(new ZPagosSoapProxy());
	com.zonapagos.www.test.ZPagosSoapProxy zonaPagosProxyPruebas = new com.zonapagos.www.test.ZPagosSoapProxy();
	
	Map<String, String> mapaParametros = new HashMap<String, String>();
	mapaParametros.put("ESTADO_PENDIENTE_INICIAR",IConstantes.ESTADO_PENDIENTE_INICIAR);
	mapaParametros.put("ESTADO_PENDIENTE_FINALIZAR",IConstantes.ESTADO_PENDIENTE_FINALIZAR);
	mapaParametros.put("ID_CLIENTE",recibo.getCliente().getCliente().toString());
	List<TransaccionZonaPagos> listaDatos = new ArrayList<TransaccionZonaPagos>();
	listaDatos = null;

	try {
		listaDatos = (List<TransaccionZonaPagos>)ParametrizacionFac.getFacade().obtenerListado("selectTransaccionZonaPagosPendientes", mapaParametros);
	} catch (Exception e) {
		e.printStackTrace();
	}
	if(listaDatos.size()<=0){
	if (recibo!= null){
		try {
			if(ReciboConsignacionHelper.getHelper().getServicioPruebasProduccion()){
				log.info("Invocamos el servicio de producci�n");
				wsResult = this.getZonaPagosProxy().inicio_pagoV2(
						recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
								(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
								Integer.parseInt(IConstantes.ID_TIENDA_BANCOLOMBIA):
								Integer.parseInt(IConstantes.ID_TIENDA):
									recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
											Integer.parseInt(IConstantes.ID_TIENDA_DOLARES):
												Integer.parseInt(IConstantes.ID_TIENDA_EURO), 
								recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
										(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
						            	&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
										IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
										IConstantes.CLAVE_SERVICIO:
											recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
													IConstantes.CLAVE_SERVICIO_DOLARES:
													IConstantes.CLAVE_SERVICIO_EURO,  
													recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
															recibo.getValorDetalle().doubleValue():
															recibo.getTasaRepresentativa().getValor().doubleValue(), 
						new Double(0).doubleValue(), 
						recibo.getReciboConsignacion().toString(), 
						recibo.getObservaciones().substring(0, recibo.getObservaciones().length()>=70?69:recibo.getObservaciones().length()), 
						recibo.getCliente().getPersona().getDireccionElectronica(), 
						recibo.getCliente().getCliente().toString(), 
						recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("C.C")?
							new String("1"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NIT")?
							new String("3"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("T.I")?
							new String("5"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("CEX")?	
							new String("2"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NEX")?			
							new String("10"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("RC")?		
							new String("9"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("PS")?			
							new String("6"):
							new String("11"), 
							recibo.getCliente().getPersona().getNombreRazonSocial().substring(
									0,	recibo.getCliente().getPersona().getNombreRazonSocial().length()>=50?49:
										recibo.getCliente().getPersona().getNombreRazonSocial().length()), 
								(recibo.getCliente().getPersona().getPrimerApellido()+" "+
										recibo.getCliente().getPersona().getSegundoApellido()).substring(
												0, (recibo.getCliente().getPersona().getPrimerApellido()+" "+
														recibo.getCliente().getPersona().getSegundoApellido())
														.length()>=50?49:
															(recibo.getCliente().getPersona().getPrimerApellido()+" "+
																	recibo.getCliente().getPersona().getSegundoApellido())
																	.length()
														), 
														recibo.getCliente().getPersona().getTelefonoResidencia().substring(0, 
																recibo.getCliente().getPersona().getTelefonoResidencia().length()>=50?49:	
																	recibo.getCliente().getPersona().getTelefonoResidencia().length()),
						new String(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
						getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor().toString()), 
						new String(recibo.getTasaRepresentativa().getValor().toString()+" "+recibo.getTasaRepresentativa().getMoneda().getCodigo()), 
						new String(recibo.getPeriodo()!=null?
								recibo.getPeriodo().getPeriodo()!=null?
										recibo.getPeriodo().getPeriodo().getPeriodo():"":""), 
						recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
								(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
								IConstantes.CODIGO_SERVICIO_BANCOLOMBIA:
								IConstantes.CODIGO_SERVICIO:
									recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
											IConstantes.CODIGO_SERVICIO_DOLARES:
											IConstantes.CODIGO_SERVICIO_EURO,  
						listServiciosMulti,
						listNitMulti,
						listValIvaMulti,
						listIvaMulti, 
						Integer.parseInt(new String("0")));
			}else{
				log.info("Invocamos el servicio de pruebas");
				wsResult = zonaPagosProxyPruebas.inicio_pagoV2(
						recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
								(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
								Integer.parseInt(IConstantes.ID_TIENDA_BANCOLOMBIA):
								Integer.parseInt(IConstantes.ID_TIENDA):
									recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
											Integer.parseInt(IConstantes.ID_TIENDA_DOLARES):
												Integer.parseInt(IConstantes.ID_TIENDA_EURO)	
										, 
										recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
												(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
				            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
												IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
												IConstantes.CLAVE_SERVICIO:
													recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
															IConstantes.CLAVE_SERVICIO_DOLARES:
															IConstantes.CLAVE_SERVICIO_EURO, 
						recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
															recibo.getValorDetalle().doubleValue():
															recibo.getTasaRepresentativa().getValor().doubleValue(), 
						new Double(0).doubleValue(), 
						recibo.getReciboConsignacion().toString(), 
						recibo.getObservaciones().substring(0, recibo.getObservaciones().length()>=70?69:recibo.getObservaciones().length()), 
						recibo.getCliente().getPersona().getDireccionElectronica(), 
						recibo.getCliente().getCliente().toString(), 
						recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("C.C")?
							new String("1"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NIT")?
							new String("3"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("T.I")?
							new String("5"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("CEX")?	
							new String("2"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NEX")?			
							new String("10"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("RC")?		
							new String("9"):
							recibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("PS")?			
							new String("6"):
							new String("11"), 
							recibo.getCliente().getPersona().getNombreRazonSocial().substring(
									0,	recibo.getCliente().getPersona().getNombreRazonSocial().length()>=50?49:
										recibo.getCliente().getPersona().getNombreRazonSocial().length()), 
								(recibo.getCliente().getPersona().getPrimerApellido()+" "+
										recibo.getCliente().getPersona().getSegundoApellido()).substring(
												0, (recibo.getCliente().getPersona().getPrimerApellido()+" "+
														recibo.getCliente().getPersona().getSegundoApellido())
														.length()>=50?49:
															(recibo.getCliente().getPersona().getPrimerApellido()+" "+
																	recibo.getCliente().getPersona().getSegundoApellido())
																	.length()
														), 
														recibo.getCliente().getPersona().getTelefonoResidencia().substring(0, 
																recibo.getCliente().getPersona().getTelefonoResidencia().length()>=50?49:	
																	recibo.getCliente().getPersona().getTelefonoResidencia().length()), 
						new String(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
						getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor().toString()), 
						new String(recibo.getTasaRepresentativa().getValor().toString()+" "+recibo.getTasaRepresentativa().getMoneda().getCodigo()), 
						new String(recibo.getPeriodo()!=null?
								recibo.getPeriodo().getPeriodo()!=null?
										recibo.getPeriodo().getPeriodo().getPeriodo():"":""), 
						recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
								(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
								IConstantes.CODIGO_SERVICIO_BANCOLOMBIA:
								IConstantes.CODIGO_SERVICIO:
									recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
											IConstantes.CODIGO_SERVICIO_DOLARES:
											IConstantes.CODIGO_SERVICIO_EURO, 
						listServiciosMulti,
						listNitMulti,
						listValIvaMulti,
						listIvaMulti, 
						Integer.parseInt(new String("0")));
			}
		log.info(wsResult);
		
		if (!wsResult.isEmpty()){
			if(!wsResult.startsWith("-1")){
				if(Long.parseLong(wsResult)>0){
					
					TransaccionZonaPagos transaccion = new TransaccionZonaPagos();
            		transaccion.setSecTransaccionZonaPagos(null);
            		transaccion.setIdPago(recibo.getReciboConsignacion());
            		transaccion.setEstadoPago(new Long(IConstantes.ESTADO_PENDIENTE_INICIAR));
            		transaccion.setIdFormaPago(null);
            		transaccion.setValorPagado(recibo.getValorDetalle());
            		transaccion.setTicketId(null);
            		transaccion.setIdClave(recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
							(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
					            	&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
									IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
									IConstantes.CLAVE_SERVICIO:
										recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
												IConstantes.CLAVE_SERVICIO_DOLARES:
												IConstantes.CLAVE_SERVICIO_EURO
												);
            		transaccion.setIdCliente(recibo.getCliente().getCliente().toString());
            		transaccion.setFranquicia(null);
            		transaccion.setCodigoServicio(null);
            		transaccion.setCodigoBanco(null);
            		transaccion.setNombreBanco(null);
            		transaccion.setCodigoTransaccion(null);
            		transaccion.setCicloTransaccion(null);
            		transaccion.setCampo1(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
							getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor().toString());
            		transaccion.setCampo2(recibo.getTasaRepresentativa().getValor().toString()+" "+recibo.getTasaRepresentativa().getMoneda().getCodigo());
            		transaccion.setCampo3(recibo.getPeriodo()!=null?
							recibo.getPeriodo().getPeriodo()!=null?
									recibo.getPeriodo().getPeriodo().getPeriodo():"":"");
            		transaccion.setIdComercio(new Long(recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
							(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
	            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
									Integer.parseInt(IConstantes.ID_TIENDA_BANCOLOMBIA):
									Integer.parseInt(IConstantes.ID_TIENDA):
										recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
												Integer.parseInt(IConstantes.ID_TIENDA_DOLARES):
													Integer.parseInt(IConstantes.ID_TIENDA_EURO)));
            		transaccion.setDatFecha(new Date());
            		
            		ParametrizacionFac.getFacade().guardarRegistro("insertTransaccionZonaPagos", transaccion);
            		log.info("registro guardado: "+transaccion.getSecTransaccionZonaPagos());
            		log.info("corriendo el redirect");
            		
            		//RedirectAction winRedirectPagos = (RedirectAction)Executions.createComponents("pages/redirectPago.zul", null,null);
            		if(recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)){
            			
            			Execution execution = Executions.getCurrent();
            			if(recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA)){
            				//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO_BANCOLOMBIA+wsResult);	
            				//Executions.sendRedirect(IConstantes.RUTA_SERVICIO_BANCOLOMBIA+wsResult);
            				execution.sendRedirect(IConstantes.RUTA_SERVICIO_BANCOLOMBIA+wsResult, "_blank");
            			} else if (recibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_OCCIDENTE) 
            					&& recibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_OCCIDENTE)){
            				execution.sendRedirect(IConstantes.RUTA_SERVICIO+wsResult, "_blank");
            				//Executions.sendRedirect(IConstantes.RUTA_SERVICIO+wsResult);	
            				//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO+wsResult);
            			} else{
            				execution.sendRedirect(IConstantes.RUTA_SERVICIO+wsResult, "_blank");
            				//Executions.sendRedirect(IConstantes.RUTA_SERVICIO+wsResult);
            			}
            			execution.setVoided(true);
            			
            			
            		}else if (recibo.getTasaRepresentativa().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)){
            			//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO_DOLARES+wsResult);
            			Execution execution = Executions.getCurrent();
            			execution.sendRedirect(IConstantes.RUTA_SERVICIO_DOLARES+wsResult, "_blank");
            			//Executions.sendRedirect(IConstantes.RUTA_SERVICIO_DOLARES+wsResult);
            			execution.setVoided(true);
            		} else {
            			//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO_EURO+wsResult);
            			Execution execution = Executions.getCurrent();
            			execution.sendRedirect(IConstantes.RUTA_SERVICIO_EURO+wsResult, "_blank");
            			//Executions.sendRedirect(IConstantes.RUTA_SERVICIO_EURO+wsResult);
            			execution.setVoided(true);
            		}
            		
            	   	
         			  Clients.showNotification(IConstantes.CONFIRMACION_INICIO_PAGO, Clients.NOTIFICATION_TYPE_INFO, this, "middle_center", 4000);
					
				}else{ //else wsresult >0
					  Messagebox.show(
							   IConstantes.ERROR_PAGO_INICIO+wsResult,
							    "Error de Pagos en L�nea",
							    Messagebox.YES, Messagebox.ERROR);
					
				}
			}else{ // wsresult empieza -1
				  Messagebox.show(
						   IConstantes.ERROR_PAGO_INICIO+wsResult,
						    "Error de Pagos en L�nea",
						    Messagebox.YES, Messagebox.ERROR);
				
			}
			
			
		} else{// ws result nulo
			  Messagebox.show(
					   IConstantes.ERROR_PAGO_INICIO+wsResult,
					    "Error de Pagos en L�nea",
					    Messagebox.YES, Messagebox.ERROR);
			
		}
	} catch (NumberFormatException e) {
		e.printStackTrace();
	} catch (RemoteException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} catch (Exception e) {

		e.printStackTrace();
			}
		}
	
	} else if(listaDatos.size()>0){
		  Messagebox.show(
				   IConstantes.ERROR_PAGO_PENDIENTE,
				    "Error de Pagos en L�nea",
				    Messagebox.YES, Messagebox.ERROR);
		  
			}
		} 
	} // Si tiene seleccionado un radio 
	else{
		Messagebox.show(
				   IConstantes.ERROR_GENERACION_RECIBO,
				    "Error de Pagos en L�nea",
				    Messagebox.YES, Messagebox.ERROR);
	}
}


public void onCheckPersonales(){
	this.getChbxRecibirInscripcionAceptarPersonal().setValue(1);
}

public void onCheckPersonalesNewPatrocinador() {
		this.getChbxRecibirInscripcionAceptarPersonalNewPatrocinador().setValue(1);
}

public void onCheckPatrocinio(){
	if(this.getChbxRecibirInscripcionPatrocinado().isChecked()) {
		this.getChbxRecibirInscripcionPatrocinado().setValue(1);
	} else {
		this.getChbxRecibirInscripcionPatrocinado().setValue(0);
	}
}

public void onCheckCondiciones(){
	this.getChbxRecibirInscripcionAceptarCondiciones().setValue(1);
}


public void onCheckCondicionesNewPatrocinador(){
	this.getChbxRecibirInscripcionAceptarCondicionesNewPatrocinador().setValue(1);
}

public Intbox getTxtRecibirInscripcionIdentificacion() {
	return txtRecibirInscripcionIdentificacion;
}

public void setTxtRecibirInscripcionIdentificacion(
		Intbox txtRecibirInscripcionIdentificacion) {
	this.txtRecibirInscripcionIdentificacion = txtRecibirInscripcionIdentificacion;
}

public Listbox getLbxRecibirInscripcionTipoIdentificacion() {
	return lbxRecibirInscripcionTipoIdentificacion;
}

public void setLbxRecibirInscripcionTipoIdentificacion(
		Listbox lbxRecibirInscripcionTipoIdentificacion) {
	this.lbxRecibirInscripcionTipoIdentificacion = lbxRecibirInscripcionTipoIdentificacion;
}

public Listbox getLbxRecibirInscripcionSexo() {
	return lbxRecibirInscripcionSexo;
}

public void setLbxRecibirInscripcionSexo(Listbox lbxRecibirInscripcionSexo) {
	this.lbxRecibirInscripcionSexo = lbxRecibirInscripcionSexo;
}

public Textbox getTxtRecibirInscripcionNombres() {
	return txtRecibirInscripcionNombres;
}

public void setTxtRecibirInscripcionNombres(Textbox txtRecibirInscripcionNombres) {
	this.txtRecibirInscripcionNombres = txtRecibirInscripcionNombres;
}

public Textbox getTxtRecibirInscripcionPrimerApellido() {
	return txtRecibirInscripcionPrimerApellido;
}

public void setTxtRecibirInscripcionPrimerApellido(
		Textbox txtRecibirInscripcionPrimerApellido) {
	this.txtRecibirInscripcionPrimerApellido = txtRecibirInscripcionPrimerApellido;
}

public Textbox getTxtRecibirInscripcionSegundoApellido() {
	return txtRecibirInscripcionSegundoApellido;
}

public void setTxtRecibirInscripcionSegundoApellido(
		Textbox txtRecibirInscripcionSegundoApellido) {
	this.txtRecibirInscripcionSegundoApellido = txtRecibirInscripcionSegundoApellido;
}

public Textbox getTxtRecibirInscripcionTelefono() {
	return txtRecibirInscripcionTelefono;
}

public void setTxtRecibirInscripcionTelefono(
		Textbox txtRecibirInscripcionTelefono) {
	this.txtRecibirInscripcionTelefono = txtRecibirInscripcionTelefono;
}

public Textbox getTxtRecibirInscripcionDireccionElectronica() {
	return txtRecibirInscripcionDireccionElectronica;
}

public void setTxtRecibirInscripcionDireccionElectronica(
		Textbox txtRecibirInscripcionDireccionElectronica) {
	this.txtRecibirInscripcionDireccionElectronica = txtRecibirInscripcionDireccionElectronica;
}

public Textbox getTxtRecibirInscripcionDireccionContacto() {
	return txtRecibirInscripcionDireccionContacto;
}

public void setTxtRecibirInscripcionDireccionContacto(
		Textbox txtRecibirInscripcionDireccionContacto) {
	this.txtRecibirInscripcionDireccionContacto = txtRecibirInscripcionDireccionContacto;
}

public Combobox getCbxRecibirInscripcionUbicacion() {
	return cbxRecibirInscripcionUbicacion;
}

public void setCbxRecibirInscripcionUbicacion(
		Combobox cbxRecibirInscripcionUbicacion) {
	this.cbxRecibirInscripcionUbicacion = cbxRecibirInscripcionUbicacion;
}

public Listbox getLbxRecibirInscripcionEscolaridad() {
	return lbxRecibirInscripcionEscolaridad;
}

public void setLbxRecibirInscripcionEscolaridad(
		Listbox lbxRecibirInscripcionEscolaridad) {
	this.lbxRecibirInscripcionEscolaridad = lbxRecibirInscripcionEscolaridad;
}

public Listbox getLbxRecibirInscripcionOcupacion() {
	return lbxRecibirInscripcionOcupacion;
}

public void setLbxRecibirInscripcionOcupacion(
		Listbox lbxRecibirInscripcionOcupacion) {
	this.lbxRecibirInscripcionOcupacion = lbxRecibirInscripcionOcupacion;
}

public Listbox getLbxRecibirInscripcionPrograma() {
	return lbxRecibirInscripcionPrograma;
}

public void setLbxRecibirInscripcionPrograma(
		Listbox lbxRecibirInscripcionPrograma) {
	this.lbxRecibirInscripcionPrograma = lbxRecibirInscripcionPrograma;
}

public Checkbox getChbxRecibirInscripcionAceptarCondiciones() {
	return chbxRecibirInscripcionAceptarCondiciones;
}

public void setChbxRecibirInscripcionAceptarCondiciones(
		Checkbox chbxRecibirInscripcionAceptarCondiciones) {
	this.chbxRecibirInscripcionAceptarCondiciones = chbxRecibirInscripcionAceptarCondiciones;
}

public Checkbox getChbxRecibirInscripcionAceptarPersonal() {
	return chbxRecibirInscripcionAceptarPersonal;
}

public void setChbxRecibirInscripcionAceptarPersonal(
		Checkbox chbxRecibirInscripcionAceptarPersonal) {
	this.chbxRecibirInscripcionAceptarPersonal = chbxRecibirInscripcionAceptarPersonal;
}

public Button getIdBtnRecibirInscripcionInscribir() {
	return idBtnRecibirInscripcionInscribir;
}

public void setIdBtnRecibirInscripcionInscribir(
		Button idBtnRecibirInscripcionInscribir) {
	this.idBtnRecibirInscripcionInscribir = idBtnRecibirInscripcionInscribir;
}

public Grid getIdRecibirInscripcionEduContGrdPrincipal() {
	return idRecibirInscripcionEduContGrdPrincipal;
}

public void setIdRecibirInscripcionEduContGrdPrincipal(
		Grid idRecibirInscripcionEduContGrdPrincipal) {
	this.idRecibirInscripcionEduContGrdPrincipal = idRecibirInscripcionEduContGrdPrincipal;
}

public Rows getIdRecibirInscripcionEduContRowsPrincipal() {
	return idRecibirInscripcionEduContRowsPrincipal;
}

public void setIdRecibirInscripcionEduContRowsPrincipal(
		Rows idRecibirInscripcionEduContRowsPrincipal) {
	this.idRecibirInscripcionEduContRowsPrincipal = idRecibirInscripcionEduContRowsPrincipal;
}

public Groupbox getIdGbxFormGenerarInscripcionEduCont() {
	return idGbxFormGenerarInscripcionEduCont;
}

public void setIdGbxFormGenerarInscripcionEduCont(Groupbox idGbxFormGenerarInscripcionEduCont) {
	this.idGbxFormGenerarInscripcionEduCont = idGbxFormGenerarInscripcionEduCont;
}

public Groupbox getIdGbxFormPagarInscripcionEduCont() {
	return idGbxFormPagarInscripcionEduCont;
}

public void setIdGbxFormPagarInscripcionEduCont(Groupbox idGbxFormPagarInscripcionEduCont) {
	this.idGbxFormPagarInscripcionEduCont = idGbxFormPagarInscripcionEduCont;
}

public Button getIdBtnRecibirInscripcionImprimirOrden() {
	return idBtnRecibirInscripcionImprimirOrden;
}

public void setIdBtnRecibirInscripcionImprimirOrden(
		Button idBtnRecibirInscripcionImprimirOrden) {
	this.idBtnRecibirInscripcionImprimirOrden = idBtnRecibirInscripcionImprimirOrden;
}

public Button getIdBtnRecibirInscripcionPagar() {
	return idBtnRecibirInscripcionPagar;
}

public void setIdBtnRecibirInscripcionPagar(Button idBtnRecibirInscripcionPagar) {
	this.idBtnRecibirInscripcionPagar = idBtnRecibirInscripcionPagar;
}

public ZPagosLocator getZonaPagosLocator() {
	return zonaPagosLocator;
}

public void setZonaPagosLocator(ZPagosLocator zonaPagosLocator) {
	this.zonaPagosLocator = zonaPagosLocator;
}

public ZPagosSoapProxy getZonaPagosProxy() {
	return zonaPagosProxy;
}

public void setZonaPagosProxy(ZPagosSoapProxy zonaPagosProxy) {
	this.zonaPagosProxy = zonaPagosProxy;
}

public Radiogroup getIdRadioGroupInscripcionEduCont() {
	return idRadioGroupInscripcionEduCont;
}

public void setIdRadioGroupInscripcionEduCont(
		Radiogroup idRadioGroupInscripcionEduCont) {
	this.idRadioGroupInscripcionEduCont = idRadioGroupInscripcionEduCont;
}

public Listbox getIdLbxInscripcionEduContMoneda() {
	return idLbxInscripcionEduContMoneda;
}

public void setIdLbxInscripcionEduContMoneda(
		Listbox idLbxInscripcionEduContMoneda) {
	this.idLbxInscripcionEduContMoneda = idLbxInscripcionEduContMoneda;
}

public Image getIdImgMonedaInscripcionEducacionContinuada() {
	return idImgMonedaInscripcionEducacionContinuada;
}

public void setIdImgMonedaInscripcionEducacionContinuada(
		Image idImgMonedaInscripcionEducacionContinuada) {
	this.idImgMonedaInscripcionEducacionContinuada = idImgMonedaInscripcionEducacionContinuada;
}

public Persona getPersona() {
	return persona;
}

public void setPersona(Persona persona) {
	this.persona = persona;
}

public List<ReciboConsignacion> getListaRecibosEduCont() {
	return listaRecibosEduCont;
}

public void setListaRecibosEduCont(List<ReciboConsignacion> listaRecibosEduCont) {
	this.listaRecibosEduCont = listaRecibosEduCont;
}

public List<ReciboConsignacion> getListaRecibosEduContPagos() {
	return listaRecibosEduContPagos;
}

public void setListaRecibosEduContPagos(List<ReciboConsignacion> listaRecibosEduContPagos) {
	this.listaRecibosEduContPagos = listaRecibosEduContPagos;
}

public List<ReciboConsignacion> getListaRecibosEduContAnt() {
	return listaRecibosEduContAnt;
}

public void setListaRecibosEduContAnt(List<ReciboConsignacion> listaRecibosEduContAnt) {
	this.listaRecibosEduContAnt = listaRecibosEduContAnt;
}

public ProgramaAcademico getProgramaAcademico() {
	return programaAcademico;
}

public void setProgramaAcademico(ProgramaAcademico programaAcademico) {
	this.programaAcademico = programaAcademico;
}

public Listbox getIdLbxRecibirInscripcionTipoIdPatrocinador() {
	return idLbxRecibirInscripcionTipoIdPatrocinador;
}

public void setIdLbxRecibirInscripcionTipoIdPatrocinador(Listbox idLbxRecibirInscripcionTipoIdPatrocinador) {
	this.idLbxRecibirInscripcionTipoIdPatrocinador = idLbxRecibirInscripcionTipoIdPatrocinador;
}

public Intbox getIdIbxRecibirInscripcionNitPatrocinador() {
	return idIbxRecibirInscripcionNitPatrocinador;
}

public void setIdIbxRecibirInscripcionNitPatrocinador(Intbox idIbxRecibirInscripcionNitPatrocinador) {
	this.idIbxRecibirInscripcionNitPatrocinador = idIbxRecibirInscripcionNitPatrocinador;
}

public Textbox getIdTxtRecibirInscripcionNombrePatrocinador() {
	return idTxtRecibirInscripcionNombrePatrocinador;
}

public void setIdTxtRecibirInscripcionNombrePatrocinador(Textbox idTxtRecibirInscripcionNombrePatrocinador) {
	this.idTxtRecibirInscripcionNombrePatrocinador = idTxtRecibirInscripcionNombrePatrocinador;
}

public Textbox getIdTxtRecibirInscripcionPrimerApellidoPatrocinador() {
	return idTxtRecibirInscripcionPrimerApellidoPatrocinador;
}

public void setIdTxtRecibirInscripcionPrimerApellidoPatrocinador(
		Textbox idTxtRecibirInscripcionPrimerApellidoPatrocinador) {
	this.idTxtRecibirInscripcionPrimerApellidoPatrocinador = idTxtRecibirInscripcionPrimerApellidoPatrocinador;
}

public Textbox getIdTxtRecibirInscripcionSegundoApellidoPatrocinador() {
	return idTxtRecibirInscripcionSegundoApellidoPatrocinador;
}

public void setIdTxtRecibirInscripcionSegundoApellidoPatrocinador(
		Textbox idTxtRecibirInscripcionSegundoApellidoPatrocinador) {
	this.idTxtRecibirInscripcionSegundoApellidoPatrocinador = idTxtRecibirInscripcionSegundoApellidoPatrocinador;
}

public Textbox getIdTxtRecibirInscripcionEmailPatrocinador() {
	return idTxtRecibirInscripcionEmailPatrocinador;
}

public void setIdTxtRecibirInscripcionEmailPatrocinador(Textbox idTxtRecibirInscripcionEmailPatrocinador) {
	this.idTxtRecibirInscripcionEmailPatrocinador = idTxtRecibirInscripcionEmailPatrocinador;
}

public Textbox getIdTxtRecibirInscripcionTelefonoPatrocinador() {
	return idTxtRecibirInscripcionTelefonoPatrocinador;
}

public void setIdTxtRecibirInscripcionTelefonoPatrocinador(Textbox idTxtRecibirInscripcionTelefonoPatrocinador) {
	this.idTxtRecibirInscripcionTelefonoPatrocinador = idTxtRecibirInscripcionTelefonoPatrocinador;
}

public Textbox getIdTxtRecibirInscripcionNombreContactoPatrocinador() {
	return idTxtRecibirInscripcionNombreContactoPatrocinador;
}

public void setIdTxtRecibirInscripcionNombreContactoPatrocinador(
		Textbox idTxtRecibirInscripcionNombreContactoPatrocinador) {
	this.idTxtRecibirInscripcionNombreContactoPatrocinador = idTxtRecibirInscripcionNombreContactoPatrocinador;
}

public Textbox getIdTxtRecibirInscripcionArchivoRutPatrocinador() {
	return idTxtRecibirInscripcionArchivoRutPatrocinador;
}

public void setIdTxtRecibirInscripcionArchivoRutPatrocinador(Textbox idTxtRecibirInscripcionArchivoRutPatrocinador) {
	this.idTxtRecibirInscripcionArchivoRutPatrocinador = idTxtRecibirInscripcionArchivoRutPatrocinador;
}

public Combobox getIdCbxRecibirInscripcionPatrocinador() {
	return idCbxRecibirInscripcionPatrocinador;
}

public void setIdCbxRecibirInscripcionPatrocinador(Combobox idCbxRecibirInscripcionPatrocinador) {
	this.idCbxRecibirInscripcionPatrocinador = idCbxRecibirInscripcionPatrocinador;
}

public Checkbox getChbxRecibirInscripcionPatrocinado() {
	return chbxRecibirInscripcionPatrocinado;
}

public void setChbxRecibirInscripcionPatrocinado(Checkbox chbxRecibirInscripcionPatrocinado) {
	this.chbxRecibirInscripcionPatrocinado = chbxRecibirInscripcionPatrocinado;
}

public File getFileRutPatrocinador() {
	return fileRutPatrocinador;
}

public void setFileRutPatrocinador(File fileRutPatrocinador) {
	this.fileRutPatrocinador = fileRutPatrocinador;
}


public Textbox getIdTxtRecibirInscripcionCargo() {
	return idTxtRecibirInscripcionCargo;
}

public void setIdTxtRecibirInscripcionCargo(Textbox idTxtRecibirInscripcionCargo) {
	this.idTxtRecibirInscripcionCargo = idTxtRecibirInscripcionCargo;
}

public Label getIdLblRecibirInscripcionTituloPrograma() {
	return idLblRecibirInscripcionTituloPrograma;
}

public void setIdLblRecibirInscripcionTituloPrograma(Label idLblRecibirInscripcionTituloPrograma) {
	this.idLblRecibirInscripcionTituloPrograma = idLblRecibirInscripcionTituloPrograma;
}


public Div getIdDivRecibirInscripcionNewPatrocinador() {
	return idDivRecibirInscripcionNewPatrocinador;
}


public void setIdDivRecibirInscripcionNewPatrocinador(Div idDivRecibirInscripcionNewPatrocinador) {
	this.idDivRecibirInscripcionNewPatrocinador = idDivRecibirInscripcionNewPatrocinador;
}


public Checkbox getChbxRecibirInscripcionAceptarCondicionesNewPatrocinador() {
	return chbxRecibirInscripcionAceptarCondicionesNewPatrocinador;
}


public void setChbxRecibirInscripcionAceptarCondicionesNewPatrocinador(
		Checkbox chbxRecibirInscripcionAceptarCondicionesNewPatrocinador) {
	this.chbxRecibirInscripcionAceptarCondicionesNewPatrocinador = chbxRecibirInscripcionAceptarCondicionesNewPatrocinador;
}


public Checkbox getChbxRecibirInscripcionAceptarPersonalNewPatrocinador() {
	return chbxRecibirInscripcionAceptarPersonalNewPatrocinador;
}


public void setChbxRecibirInscripcionAceptarPersonalNewPatrocinador(
		Checkbox chbxRecibirInscripcionAceptarPersonalNewPatrocinador) {
	this.chbxRecibirInscripcionAceptarPersonalNewPatrocinador = chbxRecibirInscripcionAceptarPersonalNewPatrocinador;
}


public Button getIdBtnRecibirInscripcionNuevoPatrocinador() {
	return idBtnRecibirInscripcionNuevoPatrocinador;
}


public void setIdBtnRecibirInscripcionNuevoPatrocinador(Button idBtnRecibirInscripcionNuevoPatrocinador) {
	this.idBtnRecibirInscripcionNuevoPatrocinador = idBtnRecibirInscripcionNuevoPatrocinador;
}


public Patrocinador getPatrocinador() {
	return patrocinador;
}


public void setPatrocinador(Patrocinador patrocinador) {
	this.patrocinador = patrocinador;
}


public Patrocinado getPatrocinado() {
	return patrocinado;
}


public void setPatrocinado(Patrocinado patrocinado) {
	this.patrocinado = patrocinado;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionTipoId() {
	return idLblRecibirInscripcionTipoId;
}


public void setIdLblRecibirInscripcionTipoId(org.zkoss.zhtml.Label idLblRecibirInscripcionTipoId) {
	this.idLblRecibirInscripcionTipoId = idLblRecibirInscripcionTipoId;
}

public org.zkoss.zhtml.Label getIdLblRecibirInscripcionIdenfiticacion() {
	return idLblRecibirInscripcionIdenfiticacion;
}


public void setIdLblRecibirInscripcionIdenfiticacion(org.zkoss.zhtml.Label idLblRecibirInscripcionIdenfiticacion) {
	this.idLblRecibirInscripcionIdenfiticacion = idLblRecibirInscripcionIdenfiticacion;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionGenero() {
	return idLblRecibirInscripcionGenero;
}


public void setIdLblRecibirInscripcionGenero(org.zkoss.zhtml.Label idLblRecibirInscripcionGenero) {
	this.idLblRecibirInscripcionGenero = idLblRecibirInscripcionGenero;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionNombres() {
	return idLblRecibirInscripcionNombres;
}


public void setIdLblRecibirInscripcionNombres(org.zkoss.zhtml.Label idLblRecibirInscripcionNombres) {
	this.idLblRecibirInscripcionNombres = idLblRecibirInscripcionNombres;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionSegundo_Apellido() {
	return idLblRecibirInscripcionSegundo_Apellido;
}


public void setIdLblRecibirInscripcionSegundo_Apellido(org.zkoss.zhtml.Label idLblRecibirInscripcionSegundo_Apellido) {
	this.idLblRecibirInscripcionSegundo_Apellido = idLblRecibirInscripcionSegundo_Apellido;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionTelefono_movil() {
	return idLblRecibirInscripcionTelefono_movil;
}


public void setIdLblRecibirInscripcionTelefono_movil(org.zkoss.zhtml.Label idLblRecibirInscripcionTelefono_movil) {
	this.idLblRecibirInscripcionTelefono_movil = idLblRecibirInscripcionTelefono_movil;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionDireccion_Electronica() {
	return idLblRecibirInscripcionDireccion_Electronica;
}


public void setIdLblRecibirInscripcionDireccion_Electronica(
		org.zkoss.zhtml.Label idLblRecibirInscripcionDireccion_Electronica) {
	this.idLblRecibirInscripcionDireccion_Electronica = idLblRecibirInscripcionDireccion_Electronica;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionDireccion_Contacto() {
	return idLblRecibirInscripcionDireccion_Contacto;
}


public void setIdLblRecibirInscripcionDireccion_Contacto(
		org.zkoss.zhtml.Label idLblRecibirInscripcionDireccion_Contacto) {
	this.idLblRecibirInscripcionDireccion_Contacto = idLblRecibirInscripcionDireccion_Contacto;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionCiudad() {
	return idLblRecibirInscripcionCiudad;
}


public void setIdLblRecibirInscripcionCiudad(org.zkoss.zhtml.Label idLblRecibirInscripcionCiudad) {
	this.idLblRecibirInscripcionCiudad = idLblRecibirInscripcionCiudad;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionPrimer_Apellido() {
	return idLblRecibirInscripcionPrimer_Apellido;
}


public void setIdLblRecibirInscripcionPrimer_Apellido(org.zkoss.zhtml.Label idLblRecibirInscripcionPrimer_Apellido) {
	this.idLblRecibirInscripcionPrimer_Apellido = idLblRecibirInscripcionPrimer_Apellido;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionGrado_escolaridad() {
	return idLblRecibirInscripcionGrado_escolaridad;
}


public void setIdLblRecibirInscripcionGrado_escolaridad(
		org.zkoss.zhtml.Label idLblRecibirInscripcionGrado_escolaridad) {
	this.idLblRecibirInscripcionGrado_escolaridad = idLblRecibirInscripcionGrado_escolaridad;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionOcupacion() {
	return idLblRecibirInscripcionOcupacion;
}


public void setIdLblRecibirInscripcionOcupacion(org.zkoss.zhtml.Label idLblRecibirInscripcionOcupacion) {
	this.idLblRecibirInscripcionOcupacion = idLblRecibirInscripcionOcupacion;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionPrograma() {
	return idLblRecibirInscripcionPrograma;
}


public void setIdLblRecibirInscripcionPrograma(org.zkoss.zhtml.Label idLblRecibirInscripcionPrograma) {
	this.idLblRecibirInscripcionPrograma = idLblRecibirInscripcionPrograma;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionCargo() {
	return idLblRecibirInscripcionCargo;
}


public void setIdLblRecibirInscripcionCargo(org.zkoss.zhtml.Label idLblRecibirInscripcionCargo) {
	this.idLblRecibirInscripcionCargo = idLblRecibirInscripcionCargo;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionEmpresaRazonSocial() {
	return idLblRecibirInscripcionEmpresaRazonSocial;
}


public void setIdLblRecibirInscripcionEmpresaRazonSocial(
		org.zkoss.zhtml.Label idLblRecibirInscripcionEmpresaRazonSocial) {
	this.idLblRecibirInscripcionEmpresaRazonSocial = idLblRecibirInscripcionEmpresaRazonSocial;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionFacturaEmpresa() {
	return idLblRecibirInscripcionFacturaEmpresa;
}


public void setIdLblRecibirInscripcionFacturaEmpresa(org.zkoss.zhtml.Label idLblRecibirInscripcionFacturaEmpresa) {
	this.idLblRecibirInscripcionFacturaEmpresa = idLblRecibirInscripcionFacturaEmpresa;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionAceptoTratamientoDatos() {
	return idLblRecibirInscripcionAceptoTratamientoDatos;
}


public void setIdLblRecibirInscripcionAceptoTratamientoDatos(
		org.zkoss.zhtml.Label idLblRecibirInscripcionAceptoTratamientoDatos) {
	this.idLblRecibirInscripcionAceptoTratamientoDatos = idLblRecibirInscripcionAceptoTratamientoDatos;
}


public org.zkoss.zhtml.H2 getIdLblRecibirInscripcionEducacionContinuada() {
	return idLblRecibirInscripcionEducacionContinuada;
}


public void setIdLblRecibirInscripcionEducacionContinuada(
		org.zkoss.zhtml.H2 idLblRecibirInscripcionEducacionContinuada) {
	this.idLblRecibirInscripcionEducacionContinuada = idLblRecibirInscripcionEducacionContinuada;
}


public org.zkoss.zhtml.H3 getIdLblRecibirInscripcionFormularioInscripcion() {
	return idLblRecibirInscripcionFormularioInscripcion;
}


public void setIdLblRecibirInscripcionFormularioInscripcion(
		org.zkoss.zhtml.H3 idLblRecibirInscripcionFormularioInscripcion) {
	this.idLblRecibirInscripcionFormularioInscripcion = idLblRecibirInscripcionFormularioInscripcion;
}


public org.zkoss.zhtml.Img getIdLblRecibirInscripcionimg() {
	return idLblRecibirInscripcionimg;
}


public void setIdLblRecibirInscripcionimg(org.zkoss.zhtml.Img idLblRecibirInscripcionimg) {
	this.idLblRecibirInscripcionimg = idLblRecibirInscripcionimg;
}


public org.zkoss.zhtml.Label getIdBtnRecibirInscripcionCondiciones() {
	return idBtnRecibirInscripcionCondiciones;
}


public void setIdBtnRecibirInscripcionCondiciones(org.zkoss.zhtml.Label idBtnRecibirInscripcionCondiciones) {
	this.idBtnRecibirInscripcionCondiciones = idBtnRecibirInscripcionCondiciones;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionDatosPersonales() {
	return idLblRecibirInscripcionDatosPersonales;
}


public void setIdLblRecibirInscripcionDatosPersonales(org.zkoss.zhtml.Label idLblRecibirInscripcionDatosPersonales) {
	this.idLblRecibirInscripcionDatosPersonales = idLblRecibirInscripcionDatosPersonales;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionDatosPago() {
	return idLblRecibirInscripcionDatosPago;
}


public void setIdLblRecibirInscripcionDatosPago(org.zkoss.zhtml.Label idLblRecibirInscripcionDatosPago) {
	this.idLblRecibirInscripcionDatosPago = idLblRecibirInscripcionDatosPago;
}


public org.zkoss.zhtml.Label getIdLblFormPagosImprimirOrden() {
	return idLblFormPagosImprimirOrden;
}


public void setIdLblFormPagosImprimirOrden(org.zkoss.zhtml.Label idLblFormPagosImprimirOrden) {
	this.idLblFormPagosImprimirOrden = idLblFormPagosImprimirOrden;
}


public org.zkoss.zhtml.Label getIdLblFormPagosLinea() {
	return idLblFormPagosLinea;
}


public void setIdLblFormPagosLinea(org.zkoss.zhtml.Label idLblFormPagosLinea) {
	this.idLblFormPagosLinea = idLblFormPagosLinea;
}


public org.zkoss.zhtml.Label getIdLblFormPagosListadoPrincipal() {
	return idLblFormPagosListadoPrincipal;
}


public void setIdLblFormPagosListadoPrincipal(org.zkoss.zhtml.Label idLblFormPagosListadoPrincipal) {
	this.idLblFormPagosListadoPrincipal = idLblFormPagosListadoPrincipal;
}


public org.zkoss.zhtml.Label getIdLblFormPagosModenaPago() {
	return idLblFormPagosModenaPago;
}


public void setIdLblFormPagosModenaPago(org.zkoss.zhtml.Label idLblFormPagosModenaPago) {
	this.idLblFormPagosModenaPago = idLblFormPagosModenaPago;
}


public org.zkoss.zhtml.Label getIdLbxInscripcionEduContMonedaMoneda() {
	return idLbxInscripcionEduContMonedaMoneda;
}


public void setIdLbxInscripcionEduContMonedaMoneda(org.zkoss.zhtml.Label idLbxInscripcionEduContMonedaMoneda) {
	this.idLbxInscripcionEduContMonedaMoneda = idLbxInscripcionEduContMonedaMoneda;
}


public Button getIdBtnRecibirInscripcionVolver() {
	return idBtnRecibirInscripcionVolver;
}


public void setIdBtnRecibirInscripcionVolver(Button idBtnRecibirInscripcionVolver) {
	this.idBtnRecibirInscripcionVolver = idBtnRecibirInscripcionVolver;
}


public Button getIdBtnRecibirInscripcionGuardarPatrocinadorExistente() {
	return idBtnRecibirInscripcionGuardarPatrocinadorExistente;
}


public void setIdBtnRecibirInscripcionGuardarPatrocinadorExistente(
		Button idBtnRecibirInscripcionGuardarPatrocinadorExistente) {
	this.idBtnRecibirInscripcionGuardarPatrocinadorExistente = idBtnRecibirInscripcionGuardarPatrocinadorExistente;
}


public Button getIdBtnRecibirInscripcionCancelarPatrocinadorExistente() {
	return idBtnRecibirInscripcionCancelarPatrocinadorExistente;
}


public void setIdBtnRecibirInscripcionCancelarPatrocinadorExistente(
		Button idBtnRecibirInscripcionCancelarPatrocinadorExistente) {
	this.idBtnRecibirInscripcionCancelarPatrocinadorExistente = idBtnRecibirInscripcionCancelarPatrocinadorExistente;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionDatosEmpresa() {
	return idLblRecibirInscripcionDatosEmpresa;
}


public void setIdLblRecibirInscripcionDatosEmpresa(org.zkoss.zhtml.Label idLblRecibirInscripcionDatosEmpresa) {
	this.idLblRecibirInscripcionDatosEmpresa = idLblRecibirInscripcionDatosEmpresa;
}


public org.zkoss.zhtml.Label getIdDivRecibirInscripcionNewPatrocinadorTipoIdentificacion() {
	return idDivRecibirInscripcionNewPatrocinadorTipoIdentificacion;
}


public void setIdDivRecibirInscripcionNewPatrocinadorTipoIdentificacion(
		org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorTipoIdentificacion) {
	this.idDivRecibirInscripcionNewPatrocinadorTipoIdentificacion = idDivRecibirInscripcionNewPatrocinadorTipoIdentificacion;
}


public org.zkoss.zhtml.Label getIdDivRecibirInscripcionNewPatrocinadorNumeroIdentificacion() {
	return idDivRecibirInscripcionNewPatrocinadorNumeroIdentificacion;
}


public void setIdDivRecibirInscripcionNewPatrocinadorNumeroIdentificacion(
		org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorNumeroIdentificacion) {
	this.idDivRecibirInscripcionNewPatrocinadorNumeroIdentificacion = idDivRecibirInscripcionNewPatrocinadorNumeroIdentificacion;
}


public org.zkoss.zhtml.Label getIdDivRecibirInscripcionNewPatrocinadorNombreRazonSocial() {
	return idDivRecibirInscripcionNewPatrocinadorNombreRazonSocial;
}


public void setIdDivRecibirInscripcionNewPatrocinadorNombreRazonSocial(
		org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorNombreRazonSocial) {
	this.idDivRecibirInscripcionNewPatrocinadorNombreRazonSocial = idDivRecibirInscripcionNewPatrocinadorNombreRazonSocial;
}


public org.zkoss.zhtml.Label getIdDivRecibirInscripcionNewPatrocinadorPrimerApellido() {
	return idDivRecibirInscripcionNewPatrocinadorPrimerApellido;
}


public void setIdDivRecibirInscripcionNewPatrocinadorPrimerApellido(
		org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorPrimerApellido) {
	this.idDivRecibirInscripcionNewPatrocinadorPrimerApellido = idDivRecibirInscripcionNewPatrocinadorPrimerApellido;
}


public org.zkoss.zhtml.Label getIdDivRecibirInscripcionNewPatrocinadorSegundoApellido() {
	return idDivRecibirInscripcionNewPatrocinadorSegundoApellido;
}


public void setIdDivRecibirInscripcionNewPatrocinadorSegundoApellido(
		org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorSegundoApellido) {
	this.idDivRecibirInscripcionNewPatrocinadorSegundoApellido = idDivRecibirInscripcionNewPatrocinadorSegundoApellido;
}


public org.zkoss.zhtml.Label getIdDivRecibirInscripcionNewPatrocinadorDireccionContacto() {
	return idDivRecibirInscripcionNewPatrocinadorDireccionContacto;
}


public void setIdDivRecibirInscripcionNewPatrocinadorDireccionContacto(
		org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorDireccionContacto) {
	this.idDivRecibirInscripcionNewPatrocinadorDireccionContacto = idDivRecibirInscripcionNewPatrocinadorDireccionContacto;
}


public org.zkoss.zhtml.Label getIdDivRecibirInscripcionNewPatrocinadorTelefonoContacto() {
	return idDivRecibirInscripcionNewPatrocinadorTelefonoContacto;
}


public void setIdDivRecibirInscripcionNewPatrocinadorTelefonoContacto(
		org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorTelefonoContacto) {
	this.idDivRecibirInscripcionNewPatrocinadorTelefonoContacto = idDivRecibirInscripcionNewPatrocinadorTelefonoContacto;
}


public org.zkoss.zhtml.Label getIdDivRecibirInscripcionNewPatrocinadorNombreContacto() {
	return idDivRecibirInscripcionNewPatrocinadorNombreContacto;
}


public void setIdDivRecibirInscripcionNewPatrocinadorNombreContacto(
		org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorNombreContacto) {
	this.idDivRecibirInscripcionNewPatrocinadorNombreContacto = idDivRecibirInscripcionNewPatrocinadorNombreContacto;
}


public org.zkoss.zhtml.Label getIdDivRecibirInscripcionNewPatrocinadorArchivoRut() {
	return idDivRecibirInscripcionNewPatrocinadorArchivoRut;
}


public void setIdDivRecibirInscripcionNewPatrocinadorArchivoRut(
		org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorArchivoRut) {
	this.idDivRecibirInscripcionNewPatrocinadorArchivoRut = idDivRecibirInscripcionNewPatrocinadorArchivoRut;
}


public Button getIdBtnRecibirInscripcionBuscarRut() {
	return idBtnRecibirInscripcionBuscarRut;
}


public void setIdBtnRecibirInscripcionBuscarRut(Button idBtnRecibirInscripcionBuscarRut) {
	this.idBtnRecibirInscripcionBuscarRut = idBtnRecibirInscripcionBuscarRut;
}


public org.zkoss.zhtml.Label getIdDivRecibirInscripcionNewPatrocinadorCondiciones() {
	return idDivRecibirInscripcionNewPatrocinadorCondiciones;
}


public void setIdDivRecibirInscripcionNewPatrocinadorCondiciones(
		org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorCondiciones) {
	this.idDivRecibirInscripcionNewPatrocinadorCondiciones = idDivRecibirInscripcionNewPatrocinadorCondiciones;
}


public org.zkoss.zhtml.Label getIdDivRecibirInscripcionNewPatrocinadorAceptoTratamientoDatos() {
	return idDivRecibirInscripcionNewPatrocinadorAceptoTratamientoDatos;
}


public void setIdDivRecibirInscripcionNewPatrocinadorAceptoTratamientoDatos(
		org.zkoss.zhtml.Label idDivRecibirInscripcionNewPatrocinadorAceptoTratamientoDatos) {
	this.idDivRecibirInscripcionNewPatrocinadorAceptoTratamientoDatos = idDivRecibirInscripcionNewPatrocinadorAceptoTratamientoDatos;
}


public Button getIdBtnRecibirInscripcionGuardarPatrocinador() {
	return idBtnRecibirInscripcionGuardarPatrocinador;
}


public void setIdBtnRecibirInscripcionGuardarPatrocinador(Button idBtnRecibirInscripcionGuardarPatrocinador) {
	this.idBtnRecibirInscripcionGuardarPatrocinador = idBtnRecibirInscripcionGuardarPatrocinador;
}


public Button getIdBtnRecibirInscripcionCancelarPatrocinador() {
	return idBtnRecibirInscripcionCancelarPatrocinador;
}


public void setIdBtnRecibirInscripcionCancelarPatrocinador(Button idBtnRecibirInscripcionCancelarPatrocinador) {
	this.idBtnRecibirInscripcionCancelarPatrocinador = idBtnRecibirInscripcionCancelarPatrocinador;
}


public org.zkoss.zhtml.H3 getIdDivRecibirInscripcionMensajeCulminarInscripcionInscripcionParticipante() {
	return idDivRecibirInscripcionMensajeCulminarInscripcionInscripcionParticipante;
}


public void setIdDivRecibirInscripcionMensajeCulminarInscripcionInscripcionParticipante(
		org.zkoss.zhtml.H3 idDivRecibirInscripcionMensajeCulminarInscripcionInscripcionParticipante) {
	this.idDivRecibirInscripcionMensajeCulminarInscripcionInscripcionParticipante = idDivRecibirInscripcionMensajeCulminarInscripcionInscripcionParticipante;
}


public org.zkoss.zhtml.H3 getIdDivRecibirInscripcionMensajeCulminarInscripcionFinalTexto() {
	return idDivRecibirInscripcionMensajeCulminarInscripcionFinalTexto;
}


public void setIdDivRecibirInscripcionMensajeCulminarInscripcionFinalTexto(
		org.zkoss.zhtml.H3 idDivRecibirInscripcionMensajeCulminarInscripcionFinalTexto) {
	this.idDivRecibirInscripcionMensajeCulminarInscripcionFinalTexto = idDivRecibirInscripcionMensajeCulminarInscripcionFinalTexto;
}


public Button getIdBtnRecibirInscripcionNuevoInscrito() {
	return idBtnRecibirInscripcionNuevoInscrito;
}


public void setIdBtnRecibirInscripcionNuevoInscrito(Button idBtnRecibirInscripcionNuevoInscrito) {
	this.idBtnRecibirInscripcionNuevoInscrito = idBtnRecibirInscripcionNuevoInscrito;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionCasillaEstudiante() {
	return idLblRecibirInscripcionCasillaEstudiante;
}


public void setIdLblRecibirInscripcionCasillaEstudiante(
		org.zkoss.zhtml.Label idLblRecibirInscripcionCasillaEstudiante) {
	this.idLblRecibirInscripcionCasillaEstudiante = idLblRecibirInscripcionCasillaEstudiante;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionCasillaCena() {
	return idLblRecibirInscripcionCasillaCena;
}


public void setIdLblRecibirInscripcionCasillaCena(org.zkoss.zhtml.Label idLblRecibirInscripcionCasillaCena) {
	this.idLblRecibirInscripcionCasillaCena = idLblRecibirInscripcionCasillaCena;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionUniversidadProcedencia() {
	return idLblRecibirInscripcionUniversidadProcedencia;
}


public void setIdLblRecibirInscripcionUniversidadProcedencia(
		org.zkoss.zhtml.Label idLblRecibirInscripcionUniversidadProcedencia) {
	this.idLblRecibirInscripcionUniversidadProcedencia = idLblRecibirInscripcionUniversidadProcedencia;
}


public org.zkoss.zhtml.Div getIdDivRecibirInscripcionCasillaCenaVisible() {
	return idDivRecibirInscripcionCasillaCenaVisible;
}


public void setIdDivRecibirInscripcionCasillaCenaVisible(
		org.zkoss.zhtml.Div idDivRecibirInscripcionCasillaCenaVisible) {
	this.idDivRecibirInscripcionCasillaCenaVisible = idDivRecibirInscripcionCasillaCenaVisible;
}


public org.zkoss.zhtml.Div getIdDivRecibirInscripcionCasillaEstudiante() {
	return idDivRecibirInscripcionCasillaEstudiante;
}


public void setIdDivRecibirInscripcionCasillaEstudiante(org.zkoss.zhtml.Div idDivRecibirInscripcionCasillaEstudiante) {
	this.idDivRecibirInscripcionCasillaEstudiante = idDivRecibirInscripcionCasillaEstudiante;
}


public org.zkoss.zhtml.Div getIdDivRecibirInscripcionUniversidadProcedencia() {
	return idDivRecibirInscripcionUniversidadProcedencia;
}


public void setIdDivRecibirInscripcionUniversidadProcedencia(
		org.zkoss.zhtml.Div idDivRecibirInscripcionUniversidadProcedencia) {
	this.idDivRecibirInscripcionUniversidadProcedencia = idDivRecibirInscripcionUniversidadProcedencia;
}


public Radio getChbxRecibirInscripcionCasillaEstudianteRadio1() {
	return chbxRecibirInscripcionCasillaEstudianteRadio1;
}


public void setChbxRecibirInscripcionCasillaEstudianteRadio1(Radio chbxRecibirInscripcionCasillaEstudianteRadio1) {
	this.chbxRecibirInscripcionCasillaEstudianteRadio1 = chbxRecibirInscripcionCasillaEstudianteRadio1;
}


public Radio getChbxRecibirInscripcionCasillaEstudianteRadio2() {
	return chbxRecibirInscripcionCasillaEstudianteRadio2;
}


public void setChbxRecibirInscripcionCasillaEstudianteRadio2(Radio chbxRecibirInscripcionCasillaEstudianteRadio2) {
	this.chbxRecibirInscripcionCasillaEstudianteRadio2 = chbxRecibirInscripcionCasillaEstudianteRadio2;
}


public Radio getChbxRecibirInscripcionCasillaCenaRadio1() {
	return chbxRecibirInscripcionCasillaCenaRadio1;
}


public void setChbxRecibirInscripcionCasillaCenaRadio1(Radio chbxRecibirInscripcionCasillaCenaRadio1) {
	this.chbxRecibirInscripcionCasillaCenaRadio1 = chbxRecibirInscripcionCasillaCenaRadio1;
}


public Radio getChbxRecibirInscripcionCasillaCenaRadio2() {
	return chbxRecibirInscripcionCasillaCenaRadio2;
}


public void setChbxRecibirInscripcionCasillaCenaRadio2(Radio chbxRecibirInscripcionCasillaCenaRadio2) {
	this.chbxRecibirInscripcionCasillaCenaRadio2 = chbxRecibirInscripcionCasillaCenaRadio2;
}


public Button getIdBtnRecibirInscripcionInscribirCena() {
	return idBtnRecibirInscripcionInscribirCena;
}


public void setIdBtnRecibirInscripcionInscribirCena(Button idBtnRecibirInscripcionInscribirCena) {
	this.idBtnRecibirInscripcionInscribirCena = idBtnRecibirInscripcionInscribirCena;
}


public Textbox getTxtRecibirInscripcionUniversidadProcedencia() {
	return txtRecibirInscripcionUniversidadProcedencia;
}


public void setTxtRecibirInscripcionUniversidadProcedencia(Textbox txtRecibirInscripcionUniversidadProcedencia) {
	this.txtRecibirInscripcionUniversidadProcedencia = txtRecibirInscripcionUniversidadProcedencia;
}


public Combobox getIdCbxRecibirInscripcionIdioma() {
	return idCbxRecibirInscripcionIdioma;
}


public void setIdCbxRecibirInscripcionIdioma(Combobox idCbxRecibirInscripcionIdioma) {
	this.idCbxRecibirInscripcionIdioma = idCbxRecibirInscripcionIdioma;
}


public String getLanguage() {
	return language;
}


public void setLanguage(String language) {
	this.language = language;
}


public org.zkoss.zhtml.H4 getIdDivRecibirInscripcionMensajeCulminarInscripcionInfoCena() {
	return idDivRecibirInscripcionMensajeCulminarInscripcionInfoCena;
}


public void setIdDivRecibirInscripcionMensajeCulminarInscripcionInfoCena(
		org.zkoss.zhtml.H4 idDivRecibirInscripcionMensajeCulminarInscripcionInfoCena) {
	this.idDivRecibirInscripcionMensajeCulminarInscripcionInfoCena = idDivRecibirInscripcionMensajeCulminarInscripcionInfoCena;
}


public org.zkoss.zhtml.Div getIdDivRecibirInscripcionMensajeCulminarInscripcionCena() {
	return idDivRecibirInscripcionMensajeCulminarInscripcionCena;
}


public void setIdDivRecibirInscripcionMensajeCulminarInscripcionCena(
		org.zkoss.zhtml.Div idDivRecibirInscripcionMensajeCulminarInscripcionCena) {
	this.idDivRecibirInscripcionMensajeCulminarInscripcionCena = idDivRecibirInscripcionMensajeCulminarInscripcionCena;
}


public org.zkoss.zhtml.Div getIdLblRecibirInscripcionSegundo_ApellidoEstado() {
	return idLblRecibirInscripcionSegundo_ApellidoEstado;
}


public void setIdLblRecibirInscripcionSegundo_ApellidoEstado(
		org.zkoss.zhtml.Div idLblRecibirInscripcionSegundo_ApellidoEstado) {
	this.idLblRecibirInscripcionSegundo_ApellidoEstado = idLblRecibirInscripcionSegundo_ApellidoEstado;
}


public Boolean getEncontroPersona() {
	return encontroPersona;
}


public void setEncontroPersona(Boolean encontroPersona) {
	this.encontroPersona = encontroPersona;
}


public Listbox getLbxRecibirInscripcionModalidadAsistencia() {
	return lbxRecibirInscripcionModalidadAsistencia;
}


public void setLbxRecibirInscripcionModalidadAsistencia(Listbox lbxRecibirInscripcionModalidadAsistencia) {
	this.lbxRecibirInscripcionModalidadAsistencia = lbxRecibirInscripcionModalidadAsistencia;
}


public org.zkoss.zhtml.Label getIdLblRecibirInscripcionModalidadAsistencia() {
	return idLblRecibirInscripcionModalidadAsistencia;
}


public void setIdLblRecibirInscripcionModalidadAsistencia(
		org.zkoss.zhtml.Label idLblRecibirInscripcionModalidadAsistencia) {
	this.idLblRecibirInscripcionModalidadAsistencia = idLblRecibirInscripcionModalidadAsistencia;
}


public org.zkoss.zhtml.Div getIdDivRecibirInscripcionModalidadAsistencia() {
	return idDivRecibirInscripcionModalidadAsistencia;
}


public void setIdDivRecibirInscripcionModalidadAsistencia(
		org.zkoss.zhtml.Div idDivRecibirInscripcionModalidadAsistencia) {
	this.idDivRecibirInscripcionModalidadAsistencia = idDivRecibirInscripcionModalidadAsistencia;
}


public String getMensajeTipoIdentificacion() {
	return mensajeTipoIdentificacion;
}


public void setMensajeTipoIdentificacion(String mensajeTipoIdentificacion) {
	this.mensajeTipoIdentificacion = mensajeTipoIdentificacion;
}


}
