package com.konrad.action;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;

import com.konrad.domain.Usuario;
import com.konrad.helper.ReciboConsignacionHelper;
import com.konrad.util.IConstantes;
import com.konrad.window.ActionStandardBorder;

public class NotasPeriodoActualAction extends ActionStandardBorder implements AfterCompose{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8038925469796721739L;

	
	public NotasPeriodoActualAction() {
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unchecked")
	@Override
	public void afterCompose() {
		// TODO Auto-generated method stub
		
		Map<String, Object> mapaRequest = new HashMap<String, Object>();
		mapaRequest.put("CLIENTE", 
				((Usuario)((HashMap<String, Object>)this.getDesktop().getSession().getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.USUARIO_SESSION)).getUsuario());
		
		Map<String,Object> mapaResult = ReciboConsignacionHelper.getHelper().getDatosAcademicosEstudiante(mapaRequest);
		
		((Label)this.getFellow("idNotasPeriodoActualCodigo")).setValue(mapaResult.get("CLIENTE").toString());
		((Label)this.getFellow("idNotasPeriodoActualNombres")).setValue(mapaResult.get("NOMBRES").toString());
		((Label)this.getFellow("idNotasPeriodoActualSemestre")).setValue(mapaResult.get("SEMESTRE").toString());
		((Label)this.getFellow("idNotasPeriodoActualEstado")).setValue(mapaResult.get("ESTADO_ACADEMICO").toString());
		((Label)this.getFellow("idNotasPeriodoActualPrograma")).setValue(mapaResult.get("PROGRAMA").toString());
		((Label)this.getFellow("idNotasPeriodoActualCargaMaxima")).setValue(mapaResult.get("CARGA_MAXIMA").toString());

	}

}
