package com.konrad.action;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.zkoss.zul.Include;

import com.konrad.helper.Tester;
import com.konrad.util.IConstantes;
import com.konrad.window.ActionStandard;

public class RedirectAction extends ActionStandard {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1564575261216360001L;


	protected static Logger log = Logger.getLogger(RedirectAction.class);
		
	public void doModal(String url){
		Include includeRedirect= (Include)this.getFellow("includePagos");
		includeRedirect.setSrc(null);
		includeRedirect.setSrc(url);
		super.doModal();
	}
	
	public void probarMetodo(){
		Tester tester = new Tester();
		Date fecha;
		Map<String, Object> mapaPruebas = new HashMap <String,Object>();
		try {
		//fecha = new SimpleDateFormat(IConstantes.FORMATO_FECHA).parse("13/06/2013");
			fecha = new Date();
		mapaPruebas = tester.pruebasSetCurrencyFecha(IConstantes.FORMATO_FECHA,IConstantes.DOLARES_AMERICANOS,IConstantes.PESOS_COLOMBIANOS_ICEBERG,fecha);
		System.out.println("Moneda Origen Pruebas: "+mapaPruebas.get("MONEDA_ORIGEN"));
		System.out.println("Moneda Destino Pruebas: "+mapaPruebas.get("MONEDA_DESTINO"));
		System.out.println("Fecha Pruebas: "+mapaPruebas.get("FECHA"));
		System.out.println("Factor Pruebas: "+mapaPruebas.get("FACTOR"));
		
		
		mapaPruebas = tester.pruebasSetCurrencyFecha(IConstantes.FORMATO_FECHA,IConstantes.EUROS,IConstantes.DOLARES_AMERICANOS,fecha);
		System.out.println("Moneda Origen Pruebas 2: "+mapaPruebas.get("MONEDA_ORIGEN"));
		System.out.println("Moneda Destino Pruebas 2: "+mapaPruebas.get("MONEDA_DESTINO"));
		System.out.println("Fecha Pruebas 2: "+mapaPruebas.get("FECHA"));
		System.out.println("Factor Pruebas 2: "+mapaPruebas.get("FACTOR"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}