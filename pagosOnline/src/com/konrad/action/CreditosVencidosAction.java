package com.konrad.action;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;

import com.konrad.assembler.CreditosVencidosAssembler;
import com.konrad.assembler.MensajesAssembler;
import com.konrad.domain.Cliente;
import com.konrad.domain.Mensaje;
import com.konrad.domain.PeriodoFacturacion;
import com.konrad.domain.ReciboConsignacion;
import com.konrad.domain.TasaRepresentativaMercado;
import com.konrad.domain.TransaccionZonaPagos;
import com.konrad.domain.Usuario;
import com.konrad.domain.VencimientoPeriodo;
import com.konrad.facade.ParametrizacionFac;
import com.konrad.helper.ReciboConsignacionHelper;
import com.konrad.util.IConstantes;
import com.konrad.util.Moneda;
import com.konrad.window.ActionStandardBorder;
import com.zonapagos.www.prod.ZPagosLocator;
import com.zonapagos.www.prod.ZPagosSoapProxy;

public class CreditosVencidosAction extends ActionStandardBorder implements AfterCompose {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1564575261216360001L;
	private ZPagosLocator zonaPagosLocator;
	private ZPagosSoapProxy zonaPagosProxy;

	protected static Logger log = Logger.getLogger(CreditosVencidosAction.class);
		
	
		
		@SuppressWarnings("rawtypes")
		public void afterCompose() {		
			try { 
				ReciboConsignacionHelper.getHelper().onInicializarListaCurrency((Listbox)this.getFellow("idLbxCreditosVencidos"));
				ReciboConsignacionHelper.getHelper().onRetirarElementosListaCurrency((Listbox)this.getFellow("idLbxCreditosVencidos"),
						((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
								getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_EURO)).getValor(),
								((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
										getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor());
				this.onSetImageCurrency();
				((Label)this.getFellow("idLblMensajeImpresionPagosCreditosVencidos")).setValue(IConstantes.INFORMACION_PAGO_VENCIDO_BANCO);
				((Label)this.getFellow("idLblMensajePagosCreditosVencidos")).setValue(IConstantes.INFORMACION_PAGO_VENCIDO);
				this.buscarMaestro();
				
				Button botonPago =(Button)this.getFellow("idBtnCreditosVencidosPagar");
				if(!ReciboConsignacionHelper.getHelper().validatePaymentDate()) {
					botonPago.setDisabled(true);
					Messagebox.show(
							   IConstantes.MENSAJE_BLOQUEO_HORA_PAGO.replace("{hora}", new SimpleDateFormat("HH:mm").format(new Date())),
							    "Advertencia de Pagos en L�nea",
							    Messagebox.YES, Messagebox.EXCLAMATION);
					
				}else {
					botonPago.setDisabled(false);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		public void onSelectMoneda(){
			log.info("Ejecutando el metodo [ onSelectMoneda ]... ");
			this.onSetImageCurrency();
			this.buscarMaestro();
			
		}
		
		@SuppressWarnings({ "unchecked" })
		public void buscarMaestro(){
			
			log.info("Ejecutando el m�todo [ buscarMaestro ]... ");
			List<ReciboConsignacion> listaDatos=null;
			
			this.getFellow("idGbxFormConsultaCreditosVencidos").setVisible(true);
			try {
				
				Cliente cliente = new Cliente();
				cliente.setCliente(new Long(((Usuario)this.getDesktop().getSession().getAttribute(IConstantes.USUARIO_SESSION)).getUsuario()));
				ReciboConsignacion reciboConsignacion = new ReciboConsignacion();
				reciboConsignacion.setCliente(cliente);
				 
				listaDatos=(List<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("selectRecibosConsignacionCreditosVencidos",reciboConsignacion); 
				
				Rows filas = (Rows)this.getFellow("idRowsMaestrosQryCreditosVencidos");
				filas.getChildren().clear();   
				Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupCreditosVencidos"); 
				radioGroup.detach();
				radioGroup = new Radiogroup();
				radioGroup.setId("idRadioGroupCreditosVencidos");
				((Groupbox)this.getFellow("idGbxFormConsultaCreditosVencidos")).appendChild(radioGroup);
				Listbox listaMonedaIncluir= (Listbox)this.getFellow("idLbxCreditosVencidos");
		   		Listitem itemSeleccionadoIncluir = listaMonedaIncluir.getSelectedItem();
		   		Label labelFechaConsulta = (Label)this.getFellow("idLblFechaCreditosVencidos");
			
					radioGroup.applyProperties();
					radioGroup.invalidate();
				log.info("termino de ejecutar la consulta ");
				CreditosVencidosAssembler derechosAs = new CreditosVencidosAssembler();
				if (listaDatos!=null){
				if (listaDatos.size()>0){
					
					ReciboConsignacion reciboAuxiliar = new ReciboConsignacion();
					reciboAuxiliar.setCliente(cliente);
					reciboAuxiliar.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
					reciboAuxiliar.setFecha(new Date());
					labelFechaConsulta.setValue("Fecha de Consulta: "+new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
					ParametrizacionFac.getFacade().ejecutarProcedimiento("calcularVencimientosCliente",reciboAuxiliar);
					
					for (Object object : listaDatos) {
						Row fila = derechosAs.crearRowDesdeDto(object, this);
						filas.appendChild(fila);
						}
					((Button)this.getFellow("idBtnCreditosVencidosRealizarAbono")).setDisabled(false);
					((Button)this.getFellow("idBtnCreditosVencidosPagoTotal")).setDisabled(false);
					((Button)this.getFellow("idBtnCreditosVencidosPagar")).setDisabled(false);
					((Button)this.getFellow("idBtnCreditosVencidosImprimirRecibo")).setDisabled(false);
					((Button)this.getFellow("idBtnCreditosVencidosRefresh")).setDisabled(false);
					
					radioGroup.setSelectedIndex(0);
					
					Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesCreditosVencidos");
					rowsMensaje.getChildren().clear();
					Mensaje mensaje = new Mensaje();
			       	String tipoMensaje = new String();
			        if(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)){
			        	
			        	tipoMensaje = IConstantes.ERROR_VENCIDO;
						mensaje.setMensaje(IConstantes.INFORMACION_GENERACION_RECIBO);
						this.onSetMensaje(mensaje,tipoMensaje);
					
						
			        }else if(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)) {
			        	
			        	tipoMensaje = IConstantes.WARNING;
						mensaje.setMensaje(IConstantes.ADVERTENCIA_MONEDA_EXTRANJERA);
						this.onSetMensaje(mensaje, tipoMensaje);
						mensaje.setMensaje(IConstantes.INFORMACION_GENERACION_RECIBO);
						this.onSetMensaje(mensaje,tipoMensaje);
					
						
					}else{
						
						tipoMensaje = IConstantes.WARNING;
						mensaje.setMensaje(IConstantes.ADVERTENCIA_MONEDA_EXTRANJERA);
						this.onSetMensaje(mensaje, tipoMensaje);
						mensaje.setMensaje(IConstantes.INFORMACION_GENERACION_RECIBO);
						this.onSetMensaje(mensaje,tipoMensaje);
					
					}
				}else{
					((Button)this.getFellow("idBtnCreditosVencidosRealizarAbono")).setDisabled(true);
					((Button)this.getFellow("idBtnCreditosVencidosPagar")).setDisabled(true);
					((Button)this.getFellow("idBtnCreditosVencidosImprimirRecibo")).setDisabled(true);
					((Button)this.getFellow("idBtnCreditosVencidosRefresh")).setDisabled(true);
					((Button)this.getFellow("idBtnCreditosVencidosPagoTotal")).setDisabled(true);
				}
			}else{
				((Button)this.getFellow("idBtnCreditosVencidosRealizarAbono")).setDisabled(true);
				((Button)this.getFellow("idBtnCreditosVencidosPagar")).setDisabled(true);
				((Button)this.getFellow("idBtnCreditosVencidosImprimirRecibo")).setDisabled(true);
				((Button)this.getFellow("idBtnCreditosVencidosRefresh")).setDisabled(true);
				((Button)this.getFellow("idBtnCreditosVencidosPagoTotal")).setDisabled(true);
			}

	            Grid tabla = (Grid) this.getFellow("idGridMaestrosQryCreditosVencidos");
				// se configura la tabla....
				tabla.setMold("paging");
				tabla.setPageSize(IConstantes.TAMANO_PAGINACION);
				tabla.applyProperties();
				tabla.invalidate();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		public void onSetMensaje(Mensaje mensaje, String tipoMensaje ){
				((Grid)this.getFellow("gridMensajesCreditosVencidos")).setVisible(true);
				MensajesAssembler mensajeAs = new MensajesAssembler();
				Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesCreditosVencidos");
				Row filaMensaje = mensajeAs.crearRowDesdeDto(mensaje, tipoMensaje);
				rowsMensaje.appendChild(filaMensaje);
		}	
		
		@SuppressWarnings({ "unchecked" })
		public void buscarMaestro(VencimientoPeriodo vencimiento){
			
			log.info("Ejecutando el m�todo [ buscarMaestro ]... ");
			List<ReciboConsignacion> listaDatos=null;
			
			this.getFellow("idGbxFormConsultaCreditosVencidos").setVisible(true);
			try {
				
				Cliente cliente = new Cliente();
				cliente.setCliente(new Long(((Usuario)this.getDesktop().getSession().getAttribute(IConstantes.USUARIO_SESSION)).getUsuario()));
				ReciboConsignacion reciboConsignacion = new ReciboConsignacion();
				reciboConsignacion.setPeriodo(vencimiento);
				reciboConsignacion.setCliente(cliente);
				 
				listaDatos=(List<ReciboConsignacion>)ParametrizacionFac.getFacade().obtenerListado("selectRecibosConsignacionCreditosVencidos",reciboConsignacion);
				Label labelFechaConsulta = (Label)this.getFellow("idLblFechaCreditosVencidos");
				labelFechaConsulta.setValue("Fecha de Consulta: "+new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
				ReciboConsignacion reciboAuxiliar = new ReciboConsignacion();
				reciboAuxiliar.setCliente(cliente);
				reciboAuxiliar.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
				reciboAuxiliar.setFecha(new Date());
				ParametrizacionFac.getFacade().ejecutarProcedimiento("calcularVencimientosCliente",reciboAuxiliar);
				Rows filas = (Rows)this.getFellow("idRowsMaestrosQryCreditosVencidos");
				filas.getChildren().clear();   
				Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupCreditosVencidos"); 
				radioGroup.detach();
				radioGroup = new Radiogroup();
				radioGroup.setId("idRadioGroupCreditosVencidos");
				((Groupbox)this.getFellow("idGbxFormConsultaCreditosVencidos")).appendChild(radioGroup);
				Listbox listaMonedaIncluir= (Listbox)this.getFellow("idLbxCreditosVencidos");
		   		Listitem itemSeleccionadoIncluir = listaMonedaIncluir.getSelectedItem();
				
			
					radioGroup.applyProperties();
					radioGroup.invalidate();
				log.info("termino de ejecutar la consulta ");
				CreditosVencidosAssembler derechosAs = new CreditosVencidosAssembler();
				if (listaDatos!=null){
				if (listaDatos.size()>0){
					for (Object object : listaDatos) {
						Row fila = derechosAs.crearRowDesdeDto(object, this);
						filas.appendChild(fila);
						}
					((Button)this.getFellow("idBtnCreditosVencidosRealizarAbono")).setDisabled(false);
					((Button)this.getFellow("idBtnCreditosVencidosPagar")).setDisabled(false);
					((Button)this.getFellow("idBtnCreditosVencidosImprimirRecibo")).setDisabled(false);
					((Button)this.getFellow("idBtnCreditosVencidosRefresh")).setDisabled(false);
					((Button)this.getFellow("idBtnCreditosVencidosPagoTotal")).setDisabled(false);
					radioGroup.setSelectedIndex(0);
					Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesCreditosVencidos");
					rowsMensaje.getChildren().clear();
					Mensaje mensaje = new Mensaje();
			       	String tipoMensaje = new String();
			        if(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)){
			        	
			        	tipoMensaje = IConstantes.ERROR_VENCIDO;
						mensaje.setMensaje(IConstantes.INFORMACION_GENERACION_RECIBO);
						this.onSetMensaje(mensaje, tipoMensaje);
						
			        }else if(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)) {
			        	
			        	tipoMensaje = IConstantes.WARNING;
						mensaje.setMensaje(IConstantes.ADVERTENCIA_MONEDA_EXTRANJERA);
						this.onSetMensaje(mensaje, tipoMensaje);
						mensaje.setMensaje(IConstantes.INFORMACION_GENERACION_RECIBO);
						this.onSetMensaje(mensaje, tipoMensaje);
						
					}else{
						
						tipoMensaje = IConstantes.WARNING;
						mensaje.setMensaje(IConstantes.ADVERTENCIA_MONEDA_EXTRANJERA);
						this.onSetMensaje(mensaje, tipoMensaje);
						mensaje.setMensaje(IConstantes.INFORMACION_GENERACION_RECIBO);
						this.onSetMensaje(mensaje, tipoMensaje);
					}

					
					}else{
						((Button)this.getFellow("idBtnCreditosVencidosRealizarAbono")).setDisabled(true);
						((Button)this.getFellow("idBtnCreditosVencidosPagar")).setDisabled(true);
						((Button)this.getFellow("idBtnCreditosVencidosImprimirRecibo")).setDisabled(true);
						((Button)this.getFellow("idBtnCreditosVencidosRefresh")).setDisabled(true);
						((Button)this.getFellow("idBtnCreditosVencidosPagoTotal")).setDisabled(true);
					}
				}else{
					((Button)this.getFellow("idBtnCreditosVencidosRealizarAbono")).setDisabled(true);
					((Button)this.getFellow("idBtnCreditosVencidosPagar")).setDisabled(true);
					((Button)this.getFellow("idBtnCreditosVencidosImprimirRecibo")).setDisabled(true);
					((Button)this.getFellow("idBtnCreditosVencidosRefresh")).setDisabled(true);
					((Button)this.getFellow("idBtnCreditosVencidosPagoTotal")).setDisabled(true);
				}

	            Grid tabla = (Grid) this.getFellow("idGridMaestrosQryCreditosVencidos");
				// se configura la tabla....
				tabla.setMold("paging");
				tabla.setPageSize(IConstantes.TAMANO_PAGINACION);
				tabla.applyProperties();
				tabla.invalidate();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public void onImprimirIndice(){
			Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupCreditosVencidos");
			log.info("indice seleccionado: "+radioGroup.getSelectedIndex());
			log.info("N�mero de �tems: "+radioGroup.getItemCount());
		}
		
		@SuppressWarnings("rawtypes")
		public ReciboConsignacion setAtributosReciboConsignacion(ReciboConsignacion recibo){
			ReciboConsignacion reciboConsignacion = recibo;
			try{
			
				Double saldoActual = reciboConsignacion.getValorVencido();
				TasaRepresentativaMercado tasaIncluir = new TasaRepresentativaMercado();
				TasaRepresentativaMercado tasaIncluirVencido = new TasaRepresentativaMercado();
				Moneda monedaIncluir= new Moneda();
				
				Listbox listaMonedaIncluir= (Listbox)this.getFellow("idLbxCreditosVencidos");
				Listitem itemSeleccionadoIncluir = listaMonedaIncluir.getSelectedItem();
				BigDecimal cantidadRedondear;
				BigDecimal cantidadRedondearVencido;
				
				if(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.PESOS_COLOMBIANOS)){
					monedaIncluir.setCodigo(IConstantes.PESOS_COLOMBIANOS);
					
					tasaIncluir.setMoneda(monedaIncluir);
					tasaIncluirVencido.setMoneda(monedaIncluir);
					cantidadRedondearVencido = new BigDecimal(saldoActual);
					cantidadRedondear =new BigDecimal(reciboConsignacion.getValorDetalle().doubleValue());
					tasaIncluirVencido.setValor(cantidadRedondearVencido.setScale(0,RoundingMode.CEILING).doubleValue());
					tasaIncluir.setValor(cantidadRedondear.setScale(0,RoundingMode.CEILING).doubleValue());
					
					tasaIncluir.setFecha(new Date());
					tasaIncluirVencido.setFecha(new Date());
					reciboConsignacion.setTasaRepresentativa(tasaIncluir);
					reciboConsignacion.setTasaRepresentativaVencido(tasaIncluirVencido);
					reciboConsignacion.setValorVencido(tasaIncluirVencido.getValor());
				
				}else{ //Entonces se hace el c�lculo con los USD
					if(((Listcell)itemSeleccionadoIncluir.getFirstChild()).getValue().equals(IConstantes.DOLARES_AMERICANOS)){
						
						monedaIncluir.setCodigo(IConstantes.DOLARES_AMERICANOS);
						tasaIncluir.setMoneda(monedaIncluir);
						tasaIncluirVencido.setMoneda(monedaIncluir);
						//Se encuentra la comisi�n para restar a la TRM
						BigDecimal comisionPesos = new BigDecimal((String)((HashMap)this.getDesktop().getSession().
								getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.COMISION_DOLAR)); 
						//Si es en d�lares se realiza el c�lculo asi (valor total del recibo / (trm-comision))
						//Para la parte de recibos vencidos se hace la conversi�n con el recibo calculado a la fecha
						cantidadRedondearVencido = new BigDecimal(saldoActual 
								/ (((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
								getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor()-comisionPesos.doubleValue()));
						
						//Valor original del recibo Vencido
						cantidadRedondear = new BigDecimal(reciboConsignacion.getValorDetalle().doubleValue() 
								/ (((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
								getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor()-comisionPesos.doubleValue()));
						
						//Se incluye la tasa redondeada
						tasaIncluir.setValor(cantidadRedondear.setScale(0,RoundingMode.CEILING).doubleValue());
						tasaIncluirVencido.setValor(cantidadRedondearVencido.setScale(0,RoundingMode.CEILING).doubleValue());
						tasaIncluir.setFecha(new Date());
						tasaIncluirVencido.setFecha(new Date());
						reciboConsignacion.setTasaRepresentativa(tasaIncluir);
						reciboConsignacion.setTasaRepresentativaVencido(tasaIncluirVencido);
						
						// Se requiere enviar el valor en pesos por lo tanto se hace valor en USD * trm
						
						BigDecimal cantidadConvertida = new BigDecimal(cantidadRedondear.doubleValue());
						cantidadConvertida = cantidadConvertida.multiply(new BigDecimal(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
								getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor()));
						
						BigDecimal cantidadConvertidaVencido = new BigDecimal(cantidadRedondearVencido.doubleValue());
						cantidadConvertidaVencido = cantidadConvertidaVencido.multiply(new BigDecimal(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
								getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor()));
						
						reciboConsignacion.setValorDetalle(cantidadConvertida.doubleValue());
						reciboConsignacion.setValorVencido(cantidadConvertidaVencido.doubleValue());
						log.info("valor recibo USD - COP: "+reciboConsignacion.getValorDetalle());
						log.info("valor recibo Vencido USD - COP: "+reciboConsignacion.getValorVencido());
					
					
					} else{//Entonces se hace el c�lculo con los Euros
						
						monedaIncluir.setCodigo(IConstantes.EUROS);
						tasaIncluir.setMoneda(monedaIncluir);
						tasaIncluirVencido.setMoneda(monedaIncluir);
						
						// se encuentra la comision para restar a la TRM 
						BigDecimal comisionPesos = new BigDecimal((String)((HashMap)this.getDesktop().getSession().
								getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.COMISION_EURO));
						
						// Se debe calcular una TRM entre USD y EUR 
						BigDecimal retorno = new BigDecimal(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
								getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_EURO)).getValor()/
								((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
										getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor());
						//luego se realiza el c�lculo del monto (valorRecibo / (retorno * (trm -comision)))
						cantidadRedondear = 
								new BigDecimal(
										(
											reciboConsignacion.getValorDetalle().doubleValue() / 
												(retorno.doubleValue()* (
															((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
										getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor() - comisionPesos.doubleValue()
										)
									)  
								)
							);
						
						
						cantidadRedondearVencido = 
								new BigDecimal(
										(
											saldoActual / 
												(retorno.doubleValue()* (
															((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
										getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor() - comisionPesos.doubleValue()
										)
									)  
								)
							);
						
						// Se incluye la tasa redondeada
						tasaIncluir.setValor(cantidadRedondear.setScale(0,RoundingMode.CEILING).doubleValue());
						tasaIncluirVencido.setValor(cantidadRedondearVencido.setScale(0,RoundingMode.CEILING).doubleValue());
						tasaIncluir.setFecha(new Date());
						tasaIncluirVencido.setFecha(new Date());
						reciboConsignacion.setTasaRepresentativa(tasaIncluir);
						reciboConsignacion.setTasaRepresentativaVencido(tasaIncluirVencido);
						
						// Se requiere enviar el valor en pesos por lo tanto se hace valor en EUR * trm
									BigDecimal cantidadConvertida = new BigDecimal(cantidadRedondear.doubleValue());
									cantidadConvertida = cantidadConvertida.multiply(new BigDecimal(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
											getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_EURO)).getValor()));
									
									BigDecimal cantidadConvertidaVencido = new BigDecimal(cantidadRedondearVencido.doubleValue());
									cantidadConvertidaVencido = cantidadConvertidaVencido.multiply(new BigDecimal(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
											getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_EURO)).getValor()));
									
									reciboConsignacion.setValorDetalle(cantidadConvertida.doubleValue());
									reciboConsignacion.setValorVencido(cantidadConvertidaVencido.doubleValue());
									log.info("valor recibo EUR - COP: "+reciboConsignacion.getValorDetalle());
									log.info("valor recibo Vencido EUR - COP: "+reciboConsignacion.getValorVencido());
				}
			}
				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			return reciboConsignacion;
		}
		
		
		public void onPagarEnLineaPayUW(){
			
			Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupCreditosVencidos");
			Integer  resultado = Messagebox.show(
				    "�Confirma este pago? "+IConstantes.MENSAJE_PERMITIR_POPUPS,
				    "Confirmar Inicio Pago",
				    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
			if(resultado == Messagebox.YES){

			
			Radio radio = radioGroup.getSelectedItem();
			ReciboConsignacion recibo = (ReciboConsignacion)radio.getAttribute("RECIBO_CONSIGNACION");
			ReciboConsignacion nuevoRecibo = new ReciboConsignacion();
			nuevoRecibo.setCliente(recibo.getCliente());
			nuevoRecibo.setReciboConsignacion(recibo.getReciboConsignacion());
			nuevoRecibo.setPeriodo(recibo.getPeriodo());
			nuevoRecibo.getPeriodo().setFechaVencimiento(new Date());
			nuevoRecibo.setCuentaReciboConsignacion(recibo.getCuentaReciboConsignacion());
			nuevoRecibo.setFecha(recibo.getFecha());
			nuevoRecibo.setGeneraComision(recibo.getGeneraComision());
			nuevoRecibo.setNumeroFila(recibo.getNumeroFila());
			nuevoRecibo.setObservaciones(recibo.getObservaciones());
			nuevoRecibo.setTasaRepresentativa(recibo.getTasaRepresentativa());
			nuevoRecibo.setTasaRepresentativaVencido(recibo.getTasaRepresentativaVencido());
			nuevoRecibo.setValorDetalle(recibo.getValorDetalle());
			nuevoRecibo.setValorTotal(recibo.getValorTotal());
			nuevoRecibo.setValorVencido(recibo.getValorVencido());
			nuevoRecibo.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
			nuevoRecibo.setBanderaFecha(IConstantes.GENERAL_NO);
			nuevoRecibo.setBanderaDiaHabil(IConstantes.GENERAL_NO);
			// Calculamos vencimientos a la fecha y luego procedemos con la creaci�n del nuevo recibo
			try {
				ParametrizacionFac.getFacade().ejecutarProcedimiento("calcularVencimientosCliente",nuevoRecibo);
				ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarReciboReliquidado", nuevoRecibo);
				nuevoRecibo = this.setAtributosReciboConsignacion(nuevoRecibo);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			log.info("Recibo: "+nuevoRecibo.getReciboConsignacion());
			log.info("Recibo Predecesor: "+recibo.getReciboConsignacion());
			log.info("valor: "+nuevoRecibo.getValorTotal());
			log.info("valor detalle: "+nuevoRecibo.getValorDetalle());
			log.info("valor Vencido: "+nuevoRecibo.getValorVencido());
			log.info("Observaciones: "+nuevoRecibo.getObservaciones());
			log.info("Observaciones Predecesor: "+recibo.getObservaciones());
			log.info("Valor Original Moneda Convertida: "+nuevoRecibo.getTasaRepresentativa().getValor());
			log.info("Valor Vencido Moneda Convertida: "+nuevoRecibo.getTasaRepresentativaVencido().getValor());
			
			
			if(recibo.getReciboConsignacion()!= null && nuevoRecibo.getReciboConsignacion()!= null){
				if(recibo.getReciboConsignacion().intValue()!=nuevoRecibo.getReciboConsignacion().intValue()){
						
					
						ReciboConsignacionHelper.getHelper().sendPostPayU(nuevoRecibo, IConstantes.TIPO_INVOCACION_CREDITOS_VENCIDOS);
						ReciboConsignacionHelper.getHelper().registrarAuditoriaPayU(nuevoRecibo, IConstantes.TIPO_INVOCACION_CREDITOS_VENCIDOS);
						
						  Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesCreditosVencidos"); 
		       			  rowsMensaje.getChildren().clear();
		       			  Mensaje mensaje = new Mensaje();
		       			  
		       			  mensaje.setMensaje(IConstantes.CONFIRMACION_INICIO_PAGO);
		       			  String tipoMensaje = IConstantes.CONFIRM;
		       			  this.onSetMensaje(mensaje, tipoMensaje);
						 
						
					}
				}
			}
		}	
		
		@SuppressWarnings({ "unchecked", "rawtypes" })
		public void onPagarEnLinea(){
			Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupCreditosVencidos");
			Integer  resultado = Messagebox.show(
				    "�Confirma este pago? "+IConstantes.MENSAJE_PERMITIR_POPUPS,
				    "Confirmar Inicio Pago",
				    Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
			if(resultado == Messagebox.YES){

			String[] listServiciosMulti = null;
			String[] listNitMulti = null;
			double[] listValIvaMulti = null;
			double[] listIvaMulti = null;
			String wsResult;
			
			Radio radio = radioGroup.getSelectedItem();
			ReciboConsignacion recibo = (ReciboConsignacion)radio.getAttribute("RECIBO_CONSIGNACION");
			ReciboConsignacion nuevoRecibo = new ReciboConsignacion();
			nuevoRecibo.setCliente(recibo.getCliente());
			nuevoRecibo.setReciboConsignacion(recibo.getReciboConsignacion());
			nuevoRecibo.setPeriodo(recibo.getPeriodo());
			nuevoRecibo.getPeriodo().setFechaVencimiento(new Date());
			nuevoRecibo.setCuentaReciboConsignacion(recibo.getCuentaReciboConsignacion());
			nuevoRecibo.setFecha(recibo.getFecha());
			nuevoRecibo.setGeneraComision(recibo.getGeneraComision());
			nuevoRecibo.setNumeroFila(recibo.getNumeroFila());
			nuevoRecibo.setObservaciones(recibo.getObservaciones());
			nuevoRecibo.setTasaRepresentativa(recibo.getTasaRepresentativa());
			nuevoRecibo.setTasaRepresentativaVencido(recibo.getTasaRepresentativaVencido());
			nuevoRecibo.setValorDetalle(recibo.getValorDetalle());
			nuevoRecibo.setValorTotal(recibo.getValorTotal());
			nuevoRecibo.setValorVencido(recibo.getValorVencido());
			nuevoRecibo.setIdentificador(Long.valueOf(this.getDesktop().getSession().hashCode()));
			nuevoRecibo.setBanderaFecha(IConstantes.GENERAL_NO);
			nuevoRecibo.setBanderaDiaHabil(IConstantes.GENERAL_NO);
			// Calculamos vencimientos a la fecha y luego procedemos con la creaci�n del nuevo recibo
			try {
				ParametrizacionFac.getFacade().ejecutarProcedimiento("calcularVencimientosCliente",nuevoRecibo);
				ParametrizacionFac.getFacade().ejecutarProcedimiento("registrarReciboReliquidado", nuevoRecibo);
				nuevoRecibo = this.setAtributosReciboConsignacion(nuevoRecibo);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			log.info("Recibo: "+nuevoRecibo.getReciboConsignacion());
			log.info("Recibo Predecesor: "+recibo.getReciboConsignacion());
			log.info("valor: "+nuevoRecibo.getValorTotal());
			log.info("valor detalle: "+nuevoRecibo.getValorDetalle());
			log.info("valor Vencido: "+nuevoRecibo.getValorVencido());
			log.info("Observaciones: "+nuevoRecibo.getObservaciones());
			log.info("Observaciones Predecesor: "+recibo.getObservaciones());
			log.info("Valor Original Moneda Convertida: "+nuevoRecibo.getTasaRepresentativa().getValor());
			log.info("Valor Vencido Moneda Convertida: "+nuevoRecibo.getTasaRepresentativaVencido().getValor());
			
			
			if(recibo.getReciboConsignacion()!= null && nuevoRecibo.getReciboConsignacion()!= null){
				if(recibo.getReciboConsignacion().intValue()!=nuevoRecibo.getReciboConsignacion().intValue()){
	
					
					this.setZonaPagosLocator(new ZPagosLocator());
					this.setZonaPagosProxy(new ZPagosSoapProxy());
					com.zonapagos.www.test.ZPagosSoapProxy zonaPagosProxyPruebas = new com.zonapagos.www.test.ZPagosSoapProxy();
			
					Map<String, String> mapaParametros = new HashMap<String, String>();
					mapaParametros.put("ESTADO_PENDIENTE_INICIAR",IConstantes.ESTADO_PENDIENTE_INICIAR);
					mapaParametros.put("ESTADO_PENDIENTE_FINALIZAR",IConstantes.ESTADO_PENDIENTE_FINALIZAR);
					mapaParametros.put("ID_CLIENTE",recibo.getCliente().getCliente().toString());
					List<TransaccionZonaPagos> listaDatos = new ArrayList<TransaccionZonaPagos>();
					listaDatos = null;
		
					try {
						listaDatos = (List<TransaccionZonaPagos>)ParametrizacionFac.getFacade().obtenerListado("selectTransaccionZonaPagosPendientes", mapaParametros);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(listaDatos.size()<=0){
						if (nuevoRecibo!= null){
							try {
								if(ReciboConsignacionHelper.getHelper().getServicioPruebasProduccion()){
									log.info("Invocamos el servicio de producci�n");
									wsResult = this.getZonaPagosProxy().inicio_pagoV2(
											nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
													(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
													&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
													Integer.parseInt(IConstantes.ID_TIENDA_BANCOLOMBIA):
													Integer.parseInt(IConstantes.ID_TIENDA):
													nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
													Integer.parseInt(IConstantes.ID_TIENDA_DOLARES):
													Integer.parseInt(IConstantes.ID_TIENDA_EURO), 
											nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
													(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
													&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
													IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
													IConstantes.CLAVE_SERVICIO:
													nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
													IConstantes.CLAVE_SERVICIO_DOLARES:
													IConstantes.CLAVE_SERVICIO_EURO,  
											nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
													nuevoRecibo.getValorVencido().doubleValue():
													nuevoRecibo.getTasaRepresentativaVencido().getValor().doubleValue(), 
											new Double(0).doubleValue(), 
											nuevoRecibo.getReciboConsignacion().toString(), 
											nuevoRecibo.getObservaciones().substring(0, nuevoRecibo.getObservaciones().length()>=70?69:nuevoRecibo.getObservaciones().length()), 
											nuevoRecibo.getCliente().getPersona().getDireccionElectronica(), 
											nuevoRecibo.getCliente().getCliente().toString(), 
											nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("C.C")?
													new String("1"):
											nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NIT")?
													new String("3"):
											nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("T.I")?
													new String("5"):
											nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("CEX")?	
													new String("2"):
											nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NEX")?			
													new String("10"):
											nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("RC")?		
													new String("9"):
											nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("PS")?			
													new String("6"):
													new String("11"), 
													nuevoRecibo.getCliente().getPersona().getNombreRazonSocial().substring(
															0,	nuevoRecibo.getCliente().getPersona().getNombreRazonSocial().length()>=50?49:
																nuevoRecibo.getCliente().getPersona().getNombreRazonSocial().length()), 
														(nuevoRecibo.getCliente().getPersona().getPrimerApellido()+" "+
																nuevoRecibo.getCliente().getPersona().getSegundoApellido()).substring(
																		0, (nuevoRecibo.getCliente().getPersona().getPrimerApellido()+" "+
																				nuevoRecibo.getCliente().getPersona().getSegundoApellido())
																				.length()>=50?49:
																					(nuevoRecibo.getCliente().getPersona().getPrimerApellido()+" "+
																							nuevoRecibo.getCliente().getPersona().getSegundoApellido())
																							.length()
																				), 
														nuevoRecibo.getCliente().getPersona().getTelefonoResidencia().substring(0, 
																nuevoRecibo.getCliente().getPersona().getTelefonoResidencia().length()>=50?49:	
																	nuevoRecibo.getCliente().getPersona().getTelefonoResidencia().length()),
											new String(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
													getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor().toString()), 
											new String(nuevoRecibo.getTasaRepresentativaVencido().getValor().toString()+" "+nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo()), 
											new String(nuevoRecibo.getPeriodo()!=null?
													nuevoRecibo.getPeriodo().getPeriodo()!=null?
													nuevoRecibo.getPeriodo().getPeriodo().getPeriodo():"":""), 
											nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
													(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
													&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
											IConstantes.CODIGO_SERVICIO_BANCOLOMBIA:
												IConstantes.CODIGO_SERVICIO:
												nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
												IConstantes.CODIGO_SERVICIO_DOLARES:
												IConstantes.CODIGO_SERVICIO_EURO,  
											listServiciosMulti,
											listNitMulti,
											listValIvaMulti,
											listIvaMulti, 
											Integer.parseInt(new String("0"))
											);
					}else{
						log.info("Invocamos el servicio de pruebas");
						wsResult = zonaPagosProxyPruebas.inicio_pagoV2(
								nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
										(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
		            					&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
										Integer.parseInt(IConstantes.ID_TIENDA_BANCOLOMBIA):
										Integer.parseInt(IConstantes.ID_TIENDA):
											nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
											Integer.parseInt(IConstantes.ID_TIENDA_DOLARES):
											Integer.parseInt(IConstantes.ID_TIENDA_EURO)	
												, 
												nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
												(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
						            			&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
												IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
												IConstantes.CLAVE_SERVICIO:
												nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
												IConstantes.CLAVE_SERVICIO_DOLARES:
												IConstantes.CLAVE_SERVICIO_EURO, 
												nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
														nuevoRecibo.getValorVencido().doubleValue():
														nuevoRecibo.getTasaRepresentativaVencido().getValor().doubleValue(), 
								new Double(0).doubleValue(), 
								nuevoRecibo.getReciboConsignacion().toString(), 
								nuevoRecibo.getObservaciones().substring(0, nuevoRecibo.getObservaciones().length()>=70?69:nuevoRecibo.getObservaciones().length()), 
								nuevoRecibo.getCliente().getPersona().getDireccionElectronica(), 
								nuevoRecibo.getCliente().getCliente().toString(), 
								nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("C.C")?
									new String("1"):
									nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NIT")?
									new String("3"):
									nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("T.I")?
									new String("5"):
									nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("CEX")?	
									new String("2"):
									nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("NEX")?			
									new String("10"):
									nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("RC")?		
									new String("9"):
									nuevoRecibo.getCliente().getPersona().getTipoIdentificacion().getTipoIdentificacion().equals("PS")?			
									new String("6"):
									new String("11"), 
									nuevoRecibo.getCliente().getPersona().getNombreRazonSocial().substring(
											0,	nuevoRecibo.getCliente().getPersona().getNombreRazonSocial().length()>=50?49:
												nuevoRecibo.getCliente().getPersona().getNombreRazonSocial().length()), 
										(nuevoRecibo.getCliente().getPersona().getPrimerApellido()+" "+
												nuevoRecibo.getCliente().getPersona().getSegundoApellido()).substring(
														0, (nuevoRecibo.getCliente().getPersona().getPrimerApellido()+" "+
																nuevoRecibo.getCliente().getPersona().getSegundoApellido())
																.length()>=50?49:
																	(nuevoRecibo.getCliente().getPersona().getPrimerApellido()+" "+
																			nuevoRecibo.getCliente().getPersona().getSegundoApellido())
																			.length()
																), 
										nuevoRecibo.getCliente().getPersona().getTelefonoResidencia().substring(0, 
												nuevoRecibo.getCliente().getPersona().getTelefonoResidencia().length()>=50?49:	
													nuevoRecibo.getCliente().getPersona().getTelefonoResidencia().length()),
								new String(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
								getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor().toString()), 
								new String(nuevoRecibo.getTasaRepresentativaVencido().getValor().toString()+" "+nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo()), 
								new String(nuevoRecibo.getPeriodo()!=null?
        								nuevoRecibo.getPeriodo().getPeriodo()!=null?
        										nuevoRecibo.getPeriodo().getPeriodo().getPeriodo():"":""), 
								nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
										(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
		            					&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
										IConstantes.CODIGO_SERVICIO_BANCOLOMBIA:
										IConstantes.CODIGO_SERVICIO:
											nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
													IConstantes.CODIGO_SERVICIO_DOLARES:
													IConstantes.CODIGO_SERVICIO_EURO, 
								listServiciosMulti,
								listNitMulti,
								listValIvaMulti,
								listIvaMulti, 
								Integer.parseInt(new String("0")));
					}
				log.info(wsResult);
				
				if (!wsResult.isEmpty()){
					if(!wsResult.startsWith("-1")){
						if(Long.parseLong(wsResult)>0){
							
							TransaccionZonaPagos transaccion = new TransaccionZonaPagos();
		            		transaccion.setSecTransaccionZonaPagos(null);
		            		transaccion.setIdPago(nuevoRecibo.getReciboConsignacion());
		            		transaccion.setEstadoPago(new Long(IConstantes.ESTADO_PENDIENTE_INICIAR));
		            		transaccion.setIdFormaPago(null);
		            		transaccion.setValorPagado(nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
														nuevoRecibo.getValorVencido().doubleValue():
															nuevoRecibo.getTasaRepresentativaVencido().getValor().doubleValue());
		            		transaccion.setTicketId(null);
		            		transaccion.setIdClave(nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
									(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
							            	&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
											IConstantes.CLAVE_SERVICIO_BANCOLOMBIA:
											IConstantes.CLAVE_SERVICIO:
												nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
														IConstantes.CLAVE_SERVICIO_DOLARES:
														IConstantes.CLAVE_SERVICIO_EURO
														);
		            		transaccion.setIdCliente(nuevoRecibo.getCliente().getCliente().toString());
		            		transaccion.setFranquicia(null);
		            		transaccion.setCodigoServicio(null);
		            		transaccion.setCodigoBanco(null);
		            		transaccion.setNombreBanco(null);
		            		transaccion.setCodigoTransaccion(null);
		            		transaccion.setCicloTransaccion(null);
		            		transaccion.setCampo1(((TasaRepresentativaMercado)((HashMap)this.getDesktop().getSession().
									getAttribute(IConstantes.INFORMACION_SESION)).get(IConstantes.TRM_DOLAR)).getValor().toString());
		            		transaccion.setCampo2(nuevoRecibo.getTasaRepresentativaVencido().getValor().toString()+" "+nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo());
		            		transaccion.setCampo3(nuevoRecibo.getPeriodo()!=null?
		            								nuevoRecibo.getPeriodo().getPeriodo()!=null?
		            										nuevoRecibo.getPeriodo().getPeriodo().getPeriodo():"":"");
		            		transaccion.setIdComercio(new Long(nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)?
									(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
			            					&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA))?	
											Integer.parseInt(IConstantes.ID_TIENDA_BANCOLOMBIA):
											Integer.parseInt(IConstantes.ID_TIENDA):
												nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)?
														Integer.parseInt(IConstantes.ID_TIENDA_DOLARES):
															Integer.parseInt(IConstantes.ID_TIENDA_EURO)));
		            		transaccion.setDatFecha(new Date());
		            		transaccion.setIdPredecesor(recibo.getReciboConsignacion());
		            		
		            		ParametrizacionFac.getFacade().guardarRegistro("insertTransaccionZonaPagos", transaccion);
		            		log.info("registro guardado: "+transaccion.getSecTransaccionZonaPagos());
		            		log.info("corriendo el redirect");
		            		//RedirectAction winRedirectPagos = (RedirectAction)Executions.createComponents("pages/redirectPago.zul", null,null);
		            		
		            		if(nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.PESOS_COLOMBIANOS)){
		            			
		            			//Execution execution = Executions.getCurrent();
		            			if(nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_BANCOLOMBIA) 
		            					&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_BANCOLOMBIA)){
		            				Executions.sendRedirect(IConstantes.RUTA_SERVICIO_BANCOLOMBIA+wsResult);
		            				//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO_BANCOLOMBIA+wsResult);	
		            			} else if (nuevoRecibo.getCuentaReciboConsignacion().getTipoEntidad().equals(IConstantes.TIPO_ENTIDAD_OCCIDENTE) 
		            					&& nuevoRecibo.getCuentaReciboConsignacion().getEntidad().equals(IConstantes.ENTIDAD_OCCIDENTE)){
		            				Executions.sendRedirect(IConstantes.RUTA_SERVICIO+wsResult);	
		            				//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO+wsResult);
		            			} else{
		            				//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO+wsResult);
		            				Executions.sendRedirect(IConstantes.RUTA_SERVICIO+wsResult);
		            			}
		            			//execution.setVoided(true);
		            			
		            			
		            		}else if (nuevoRecibo.getTasaRepresentativaVencido().getMoneda().getCodigo().equals(IConstantes.DOLARES_AMERICANOS)){
		            			//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO_DOLARES+wsResult);
		            			//Execution execution = Executions.getCurrent();
		            			Executions.sendRedirect(IConstantes.RUTA_SERVICIO_DOLARES+wsResult);
		            			//execution.setVoided(true);
		            		} else {
		            			//winRedirectPagos.doModal(IConstantes.RUTA_SERVICIO_EURO+wsResult);
		            			//Execution execution = Executions.getCurrent();
		            			Executions.sendRedirect(IConstantes.RUTA_SERVICIO_EURO+wsResult);
		            			//execution.setVoided(true);
		            		}
		            		
		            		  Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesCreditosVencidos"); 
			       			  rowsMensaje.getChildren().clear();
			       			  Mensaje mensaje = new Mensaje();
			       			  
			       			  mensaje.setMensaje(IConstantes.CONFIRMACION_INICIO_PAGO);
			       			  String tipoMensaje = IConstantes.CONFIRM;
			       			  this.onSetMensaje(mensaje, tipoMensaje);
								
							}else{ //else wsresult >0
								Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesCreditosVencidos"); 
				       			  rowsMensaje.getChildren().clear();
				       			  Mensaje mensaje = new Mensaje();
				       			  
				       			  mensaje.setMensaje(IConstantes.ERROR_PAGO_INICIO+wsResult);
				       			  String tipoMensaje = IConstantes.ERROR;
				       			  this.onSetMensaje(mensaje, tipoMensaje);
								
							}
						}else{ // wsresult empieza -1
							Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesCreditosVencidos"); 
			       			  rowsMensaje.getChildren().clear();
			       			  Mensaje mensaje = new Mensaje();
			       			  
			       			  mensaje.setMensaje(IConstantes.ERROR_PAGO_INICIO+wsResult);
			       			  String tipoMensaje = IConstantes.ERROR;
			       			  this.onSetMensaje(mensaje, tipoMensaje);
							
						}
						
						
					} else{// ws result nulo
						  Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesCreditosVencidos"); 
		       			  rowsMensaje.getChildren().clear();
		       			  Mensaje mensaje = new Mensaje();
		       			  
		       			  mensaje.setMensaje(IConstantes.ERROR_PAGO_INICIO+wsResult);
		       			  String tipoMensaje = IConstantes.ERROR;
		       			  this.onSetMensaje(mensaje, tipoMensaje);
						
					}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				
				e.printStackTrace();
					}
				}
			
			
			} else if(listaDatos.size()>0){
				  Messagebox.show(
						    IConstantes.ERROR_PAGO_PENDIENTE,
						    "Error de Pagos en L�nea",
						    Messagebox.YES, Messagebox.ERROR);
				  Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesCreditosVencidos"); 
				  rowsMensaje.getChildren().clear();
				  Mensaje mensaje = new Mensaje();
				  mensaje.setMensaje(IConstantes.ERROR_PAGO_PENDIENTE);
				  String tipoMensaje = IConstantes.ERROR;
				  this.onSetMensaje(mensaje, tipoMensaje);
				}
			} else{
				 Messagebox.show(
						    IConstantes.ERROR_GENERACION_RECIBO,
						    "Error de Pagos en L�nea",
						    Messagebox.YES, Messagebox.ERROR);
				  Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesCreditosVencidos"); 
				  rowsMensaje.getChildren().clear();
				  Mensaje mensaje = new Mensaje();
				  mensaje.setMensaje(IConstantes.ERROR_GENERACION_RECIBO);
				  String tipoMensaje = IConstantes.ERROR;
				  this.onSetMensaje(mensaje, tipoMensaje);
			}
		} else{
			 Messagebox.show(
					    IConstantes.ERROR_GENERACION_RECIBO,
					    "Error de Pagos en L�nea",
					    Messagebox.YES, Messagebox.ERROR);
			  Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesCreditosVencidos"); 
			  rowsMensaje.getChildren().clear();
			  Mensaje mensaje = new Mensaje();
			  mensaje.setMensaje(IConstantes.ERROR_GENERACION_RECIBO);
			  String tipoMensaje = IConstantes.ERROR;
			  this.onSetMensaje(mensaje, tipoMensaje);
			}
		}	
	}

		public void onBuscarParametro()
		{
			PeriodoFacturacion periodoFacturacion = new PeriodoFacturacion();
			Textbox textoParametro = (Textbox)this.getFellow("parPeriodoCreditosVencidos");
			periodoFacturacion.setPeriodo(textoParametro.getValue());
			VencimientoPeriodo vencimientoPeriodo = new VencimientoPeriodo();
			vencimientoPeriodo.setPeriodo(periodoFacturacion);
			
			this.buscarMaestro(vencimientoPeriodo);
			
			Button botonPago =(Button)this.getFellow("idBtnCreditosVencidosPagar");
			if(!ReciboConsignacionHelper.getHelper().validatePaymentDate()) {
				botonPago.setDisabled(true);
			}else {
				botonPago.setDisabled(false);
			}
		}

		public ZPagosLocator getZonaPagosLocator() {
			return zonaPagosLocator;
		}

		public void setZonaPagosLocator(ZPagosLocator zonaPagosLocator) {
			this.zonaPagosLocator = zonaPagosLocator;
		}

		public ZPagosSoapProxy getZonaPagosProxy() {
			return zonaPagosProxy;
		}

		public void setZonaPagosProxy(ZPagosSoapProxy zonaPagosProxy) {
			this.zonaPagosProxy = zonaPagosProxy;
		}

		public void onImprimirRecibo(){
			Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupCreditosVencidos");
			if (radioGroup.getItemCount()>0){
			Radio radio = radioGroup.getSelectedItem();
			ReciboConsignacion recibo = (ReciboConsignacion)radio.getAttribute("RECIBO_CONSIGNACION");
			SetDateReciboAction fechaAction = (SetDateReciboAction)Executions.createComponents("pages/setDateRecibo.zul", null, null);
			fechaAction.doModal(recibo);
			
			} else{
				 Messagebox.show(
						    IConstantes.MENSAJE_OPCION_NO_SELECCIONADA,
						    "Error de Pagos en L�nea",
						    Messagebox.YES, Messagebox.ERROR);
				  Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesCreditosVencidos"); 
				  rowsMensaje.getChildren().clear();
				  Mensaje mensaje = new Mensaje();
				  mensaje.setMensaje(IConstantes.MENSAJE_OPCION_NO_SELECCIONADA);
				  String tipoMensaje = IConstantes.ERROR;
				  this.onSetMensaje(mensaje, tipoMensaje);
			}
			
		}
	public void onIniciarAbono(){
		AbonosIcebergAction abonosAction = (AbonosIcebergAction)Executions.createComponents("pages/abonosIceberg.zul", null, null);
		abonosAction.doModal();
	}
		
	public void onSetImageCurrency(){
		Listbox lista = (Listbox)this.getFellow("idLbxCreditosVencidos"); 
		Image imagen = (Image)this.getFellow("idImgMonedaCreditosVencidos");
		ReciboConsignacionHelper.getHelper().onSetImageCurrency(lista, imagen);
	}
	
	public void onIniciarPagoTotal(){
		Radiogroup radioGroup = (Radiogroup)this.getFellow("idRadioGroupCreditosVencidos");
		if(radioGroup.getItemCount()>0){
			Radio radio = radioGroup.getSelectedItem();
			ReciboConsignacion recibo = (ReciboConsignacion)radio.getAttribute("RECIBO_CONSIGNACION");
			PagoTotalReciboAction pagoAction = (PagoTotalReciboAction)Executions.createComponents("pages/pagoTotalRecibo.zul", null, null);
			pagoAction.doModal(recibo);
			
		}else{
			 Messagebox.show(
					    IConstantes.MENSAJE_OPCION_NO_SELECCIONADA,
					    "Error de Pagos en L�nea",
					    Messagebox.YES, Messagebox.ERROR);
			  Rows rowsMensaje = (Rows)this.getFellow("idRowsMensajesCreditosVencidos"); 
			  rowsMensaje.getChildren().clear();
			  Mensaje mensaje = new Mensaje();
			  mensaje.setMensaje(IConstantes.MENSAJE_OPCION_NO_SELECCIONADA);
			  String tipoMensaje = IConstantes.ERROR;
			  this.onSetMensaje(mensaje, tipoMensaje);
		}
	}
	
}