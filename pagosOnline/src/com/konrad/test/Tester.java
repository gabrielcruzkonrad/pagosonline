package com.konrad.test;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.konrad.domain.Details;
import com.konrad.domain.Merchant;
import com.konrad.domain.PingRequest;
import com.konrad.util.IConstantes;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class Tester {

	/**
	 * @param args
	 */
	
	public void verificarPagoPorReferencia(String referenceCode){
		try{
			
			System.out.println("Inicio de invocación \n");
			
		Client client = Client.create();
    	String urlRestApi = "https://api.payulatam.com/reports-api/4.0/service.cgi";	
		
    	WebResource webResource = client.resource(urlRestApi);
		
    	
		
    	String apiKey = "JxjXKtzo7BqE049nRIYRgjh8xf";
    	

    	String apiLogin = "NY4ypIzKTAFIYE6";
		
		PingRequest request = new PingRequest();
    	Details details = new Details();
    	Merchant merchant = new Merchant();
    	merchant.setApiKey(apiKey);
    	merchant.setApiLogin(apiLogin);
    	details.setReferenceCode(referenceCode);
    	request.setDetails(details);
    	request.setMerchant(merchant);
    	request.setCommand(IConstantes.COMMAND_API_REFERENCE_CODE_PAYU);
    	request.setTest("false");
    	request.setLanguage("en");
    	
    	GsonBuilder builder = new GsonBuilder(); 
        builder.setPrettyPrinting(); 
        Gson gson = builder.create();
        
        String input = gson.toJson(request);
    	ClientResponse response = webResource.type("application/json")
				   .post(ClientResponse.class, input);
    	
    	System.out.println("Output from Server .... \n");
		String output = response.getEntity(String.class);
		System.out.println(output);
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
	}
	
	public void testSubstring(String st1, String st2){
		String resultado = 
		st1+st2.substring(0, st2.length()<=15-st1.length()?st2.length():15-st1.length());
		
		System.out.println(resultado);
		
	}
	
	public static Boolean printDate() {
		String horaActual = new SimpleDateFormat("HH:mm").format(new Date());
		String[] horaMinutoActual = horaActual.split(":");
		
		String horaParametro = IConstantes.HORA_BLOQUEO_PAGO;
		String[] horaMinutoParametro = horaParametro.split(":");
		
		if(	Integer.valueOf(horaMinutoActual[0])==Integer.valueOf(horaMinutoParametro[0])
				&& Integer.valueOf(horaMinutoActual[1])>= Integer.valueOf(horaMinutoParametro[1]))
				{
			
					System.out.println("Es hora aplicable a bloqueo..."+horaMinutoActual[0]+" - "+horaMinutoActual[1]);
					return false;
				}
		
		
		return true;
		
	}
	
	public void testStringConcepto(String valor){
		String concepto = valor.substring(0, valor.indexOf("-"));
		String causa = valor.substring(valor.indexOf("-")+1,valor.length());
		
		System.out.println("concepto: "+concepto);
		System.out.println("causa: "+causa);
		
	}
	
	public void testFormatString(String reference, Integer longitud){
		if(reference == null){
			reference="0";
		}
		String formato = String.format("%1$"+longitud+"s", reference).replace(" ", "0");
		System.out.println(formato);
		
	}
	
	public static void main(String[] args) {
		Tester tester = new Tester();
		
		tester.testStringConcepto("201-320");
		tester.testFormatString("", 10);
		
		String[] array = {"0","1","2"};
		for(int i=0; i<array.length; i++) {
			System.out.println("elementos: "+array[i]);
		}
	
		printDate();
		
	}

}
