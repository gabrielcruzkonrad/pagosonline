<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="ORDEN_WEBFECHAS"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="552"
		 pageHeight="802"
		 columnWidth="552"
		 columnSpacing="0"
		 leftMargin="0"
		 rightMargin="0"
		 topMargin="0"
		 bottomMargin="0"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="P_REFERENCIA" isForPrompting="false" class="java.math.BigDecimal">
		<defaultValueExpression ><![CDATA[new BigDecimal(0)]]></defaultValueExpression>
	</parameter>
	<parameter name="P_ORGANIZACION" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA[new String("")]]></defaultValueExpression>
	</parameter>
	<parameter name="P_DOCUMENTO" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA[new String("")]]></defaultValueExpression>
	</parameter>
	<parameter name="P_LIQUIDACION" isForPrompting="false" class="java.math.BigDecimal">
		<defaultValueExpression ><![CDATA[new BigDecimal(0)]]></defaultValueExpression>
	</parameter>
	<queryString><![CDATA[SELECT mostrar_descrip_fecha(orden.grupo, vencimiento.fecha_vencimiento)  texto_fecha,
        vencimiento.fecha_vencimiento                      VENCIMIENTO1,
	DECODE ( 
		SIGN ( 
			RECIBO.valor
		), 1,
		RECIBO.valor 
		* ( 1 + 
			sd_vencimiento_periodo.porcentaje (
				orden.grupo,
				vencimiento.fecha_vencimiento,
				orden.periodo,
				orden.condicion_comercial,
				vencimiento.fila
			) /100 
		) ,
		RECIBO.valor
	) + NVL(RECIBO_SIN.valor, 0)				SUMA1,
	RECIBO.recibo
FROM
        orden        ORDEN,
	(
		select 
			   rownum fila,
			vencimiento.fecha_vencimiento fecha_vencimiento	
		from   orden
			   , vencimiento_periodo vencimiento 
		WHERE
			ORDEN.documento = $P{P_DOCUMENTO}
		AND	ORDEN.organizacion = $P{P_ORGANIZACION}
		AND	ORDEN.orden = $P{P_LIQUIDACION}
		and 	vencimiento.periodo = orden.periodo
		and 	vencimiento.grupo = orden.grupo
                AND trunc(vencimiento.fecha_vencimiento)>= trunc(sysdate)
		ORDER BY 1
	) VENCIMIENTO,
	( 
		-- Solo suma los detalles que se incluyen en el calculo de la extraordinaria como base de calculo
		SELECT
		   SUM     ( DRC.valor )   valor,
			   RC.recibo_consignacion recibo
		   FROM
			   detalle_recibo_consignacion DRC,
			   recibo_consignacion RC
			   , tipo_solicitud_nota ts
		   WHERE   DRC.recibo_consignacion = $P{P_REFERENCIA}
		   AND     RC.estado IN  ('I','C')
		   AND     RC.recibo_consignacion = DRC.recibo_consignacion
		   AND	   ts.concepto_nota (+) = drc.concepto_nota
		   AND	   ts.causa_nota (+) = drc.causa_nota
		   AND	   (
		   		NVL(ts.incluye_liquidacion_extra, 'N') = 'S'
				OR drc.orden > 0
			   )
	   AND NOT EXISTS (SELECT 'X' FROM concepto_ingreso_credito WHERE 
		concepto IN ('EX','CX') AND concepto_nota = drc.concepto_nota 
		AND causa_nota = drc.causa_nota
		)
		   GROUP BY
			   RC.recibo_consignacion
	)  recibo
	, ( 
		-- Muestra los valores que NO se incluyen en la extraordinaria
		SELECT
		   SUM     ( DRC.valor )   valor,
			   RC.recibo_consignacion recibo
		   FROM
			   detalle_recibo_consignacion DRC,
			   recibo_consignacion RC
			   , tipo_solicitud_nota ts
		   WHERE   DRC.recibo_consignacion = $P{P_REFERENCIA}
		   AND     RC.estado IN  ('I','C')
		   AND     RC.recibo_consignacion = DRC.recibo_consignacion
		   AND	   ts.concepto_nota (+) = drc.concepto_nota
		   AND	   ts.causa_nota (+) = drc.causa_nota
		   AND	   NVL(ts.incluye_liquidacion_extra, 'N') = 'N'
	           AND	   drc.orden IS NULL
	   AND NOT EXISTS (SELECT 'X' FROM concepto_ingreso_credito WHERE 
		concepto IN ( 'EX','CX') AND concepto_nota = drc.concepto_nota 
		AND causa_nota = drc.causa_nota
		)
		   GROUP BY
			   RC.recibo_consignacion
	)  recibo_sin
WHERE
	ORDEN.documento = $P{P_DOCUMENTO}
AND	ORDEN.organizacion = $P{P_ORGANIZACION}
AND	ORDEN.orden = $P{P_LIQUIDACION}
AND	ORDEN.grupo IS NOT NULL
AND     VENCIMIENTO.FECHA_VENCIMIENTO IS NOT NULL
AND	VENCIMIENTO.fila BETWEEN 1 and 4  /* Solo CUATRO fechas */
AND	recibo_sin.recibo (+) = recibo.recibo
ORDER BY
        2]]></queryString>

	<field name="TEXTO_FECHA" class="java.lang.String"/>
	<field name="VENCIMIENTO1" class="java.sql.Timestamp"/>
	<field name="SUMA1" class="java.math.BigDecimal"/>
	<field name="RECIBO" class="java.math.BigDecimal"/>

	<variable name="CCV_CUENTA_FECHAS" class="java.math.BigDecimal" resetType="Report" calculation="Count">
		<variableExpression><![CDATA[$F{VENCIMIENTO1}]]></variableExpression>
		<initialValueExpression><![CDATA[new BigDecimal(0)]]></initialValueExpression>
	</variable>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="16"  isSplitAllowed="true" >
				<line direction="TopDown">
					<reportElement
						x="274"
						y="0"
						width="0"
						height="15"
						key="line-1"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<line direction="TopDown">
					<reportElement
						x="400"
						y="0"
						width="0"
						height="15"
						key="line-2"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="49"
						y="1"
						width="195"
						height="12"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{TEXTO_FECHA}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd/MM/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="301"
						y="1"
						width="86"
						height="12"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center">
						<font size="9"/>
					</textElement>
				<textFieldExpression   class="java.sql.Timestamp"><![CDATA[$F{VENCIMIENTO1}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="419"
						y="1"
						width="122"
						height="12"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font size="9"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{SUMA1}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="14"
						width="551"
						height="0"
						key="line-3"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="16"  isSplitAllowed="true" >
				<printWhenExpression><![CDATA[new Boolean ($V{CCV_CUENTA_FECHAS}.intValue()==1)]]></printWhenExpression>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="552"
						height="16"
						key="textField-1"
						positionType="Float">
							<printWhenExpression><![CDATA[new Boolean ($V{CCV_CUENTA_FECHAS}.intValue()==1)]]></printWhenExpression>
						</reportElement>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[new String("PARA PAGO UNICAMENTE EN BANCOS Y EN HORARIO BANCARIO")]]></textFieldExpression>
				</textField>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
