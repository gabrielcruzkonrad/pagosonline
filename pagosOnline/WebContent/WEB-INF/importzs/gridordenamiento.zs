import java.text.DecimalFormat;
import java.text.ParsePosition;
import java.math.BigDecimal;

/*
* Clase encargada de ordenar la columan de una Grilla que contenga datos numericos
*
*/

class CompNumeros implements Comparator {
	
   private boolean _asc;
   private int _col;
		
  public CompNumeros(boolean asc,int columna){
		
			_asc = asc;
			_col = columna;
		}

  public int compare(Object o1, Object o2){
	
		String  s1 = o1.getChildren().get(_col).getValue();
		String 	s2 = o2.getChildren().get(_col).getValue();
			
		BigDecimal val1 = cadenaANumero(s1); 	
		BigDecimal val2 = cadenaANumero(s2); 	
 			
		int v = val1.compareTo(val2);

		return _asc ? v: -v;
	}
		
	public static BigDecimal cadenaANumero(String cadena ){
	
		DecimalFormat formateador= new DecimalFormat();
		BigDecimal numero=null;
	
		formateador.setParseBigDecimal(true);
		numero = (BigDecimal) formateador.parseObject( cadena,new ParsePosition (0));
			
		return numero;
	}
}


/*
 * Esta clase permite el ordenamiento de datos de una columna en un Grilla
 *que contenga datos String
 *
*/


	class CompString implements Comparator {

		private boolean _asc;
		private int _col;
		
		public CompString(boolean asc,int columna) {
		
			_asc = asc;
			_col = columna;
		}
		
		public int compare(Object o1, Object o2) {
		
			String s1 = o1.getChildren().get(_col).getValue();
			String s2 = o2.getChildren().get(_col).getValue();
			
			int v = s1.compareTo(s2);
			return _asc ? v: -v;
		}
	}


/*
*
*------------  PARA MEJORAR ------------
*/


/*
* Se realiza un new de objetos Comparator de tipo de dato num�rico por cada columna que se dese ordenar
* 
*/	
	Comparator  ascNum0 = new CompNumeros(true,0);
    Comparator	dscNum0= new CompNumeros(false,0);
    
    Comparator  ascNum1 = new CompNumeros(true,1);
    Comparator	dscNum1 = new CompNumeros(false,1);
    
    Comparator  ascNum2 = new CompNumeros(true,2);
    Comparator	dscNum2 = new CompNumeros(false,2);
    
    
    Comparator  ascNum3 = new CompNumeros(true,3);
    Comparator	dscNum3 = new CompNumeros(false,3);
    
    Comparator  ascNum4 = new CompNumeros(true,4);
    Comparator	dscNum4 = new CompNumeros(false,4);
    
    Comparator  ascNum5 = new CompNumeros(true,5);
    Comparator	dscNum5 = new CompNumeros(false,5);
    
    Comparator  ascNum6 = new CompNumeros(true,6);
    Comparator	dscNum6 = new CompNumeros(false,6);
    
    Comparator  ascNum7 = new CompNumeros(true,7);
    Comparator	dscNum7 = new CompNumeros(false,7);
    

/*
* Se realiza un new de objetos Comparator de tipo de dato String por cada columna que se dese ordenar
* 
*/	    
    Comparator  ascStr0 = new CompString(true,0);
    Comparator	dscStr0= new CompString(false,0);
    
    Comparator  ascStr1 = new CompString(true,1);
    Comparator	dscStr1 = new CompString(false,1);
    
    Comparator  ascStr2 = new CompString(true,2);
    Comparator	dscStr2 = new CompString(false,2);
    
    Comparator  ascStr3 = new CompString(true,3);
    Comparator	dscStr3 = new CompString(false,3);
    
    Comparator  ascStr4 = new CompString(true,4);
    Comparator	dscStr4 = new CompString(false,4);
    
    Comparator  ascStr5 = new CompString(true,5);
    Comparator	dscStr5 = new CompString(false,5);
    
    Comparator  ascStr6 = new CompString(true,6);
    Comparator	dscStr6 = new CompString(false,6);
    
     Comparator  ascStr7 = new CompString(true,7);
    Comparator	dscStr7 = new CompString(false,7);