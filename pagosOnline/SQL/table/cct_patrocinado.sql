create table cct_patrocinado (
    sec_patrocinado number(16) primary key,
    secuencia_persona number(38) not null,
    sec_patrocinador number(16) not null,
    fecha_creacion date default sysdate not null,
    periodo varchar2(6) not null,
    estado varchar2(1) default 'A' not null,
    constraint patrocinado_c1 check (estado in ('A','I')),
    constraint patrocinado_u1 unique(sec_patrocinador,secuencia_persona,periodo),
    constraint patrocinado_f1 foreign key (secuencia_persona) references persona(secuencia_persona) deferrable initially deferred,
    constraint patrocinado_f2 foreign key (sec_patrocinador) references cct_patrocinador(sec_patrocinador) deferrable initially deferred,
    constraint patrocinado_f3 foreign key (periodo) references periodo_facturacion(periodo) deferrable initially deferred
);