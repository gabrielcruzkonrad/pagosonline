create or replace
TRIGGER TRANSACCION_ZONA_PAGOS_A1  
AFTER UPDATE ON CCT_TRANSACCION_ZONA_PAGOS
FOR EACH ROW
DECLARE 
  mi_recibo_consignacion recibo_consignacion%ROWTYPE;
  mi_error VARCHAR2(4000);
  mi_recibo_caja NUMBER;
  mi_modalidad modalidad_pago.modalidad%TYPE;
  mi_caja caja.caja%TYPE;
  mi_moneda moneda.moneda%TYPE;
  mi_numero_cuenta cuenta_bancaria.numero_cuenta%TYPE;
  mi_tipo_entidad cuenta_bancaria.tipo_entidad%TYPE;
  mi_entidad cuenta_bancaria.entidad%TYPE;
  mi_oficina_entidad cuenta_bancaria.oficina_entidad%TYPE;
  
BEGIN 
  mi_recibo_consignacion := sd_recibo_consignacion.datos(un_recibo_consignacion =>:new.id_pago);
  
  IF UPDATING THEN
    IF mi_recibo_consignacion.estado = 'I'
    AND :new.estado_pago  = 1
    AND INSTR(:new.campo2, 'COP') >0 
    THEN
    
     BEGIN
        SELECT valor 
        INTO mi_caja
        FROM valor_defecto_archivo 
        WHERE tipo_archivo = '01' 
        AND tipo_entidad = mi_recibo_consignacion.tipo_entidad 
        AND entidad= mi_recibo_consignacion.entidad 
        AND columna= 'CAJA';
     END;    
     
     BEGIN                        
        IF :new.franquicia IS NOT NULL AND :new.franquicia != '0' THEN
            SELECT modalidad 
            INTO mi_modalidad
            FROM modalidad_pago 
            WHERE upper(nombre_modalidad) LIKE '%CREDITO%' 
            AND upper(nombre_modalidad) LIKE upper('%'||:new.franquicia||'%');
        ELSE 
            SELECT valor 
            INTO mi_modalidad
            FROM valor_defecto_archivo 
            WHERE tipo_archivo = '01' 
            AND tipo_entidad = mi_recibo_consignacion.tipo_entidad 
            AND entidad= mi_recibo_consignacion.entidad 
            AND columna= 'MODALIDAD';
        END IF;
      END; 
      
      BEGIN
        SELECT valor 
        INTO mi_moneda
        FROM valor_defecto_archivo 
        WHERE tipo_archivo = '01' 
        AND tipo_entidad = mi_recibo_consignacion.tipo_entidad 
        AND entidad= mi_recibo_consignacion.entidad 
        AND columna= 'MONEDA';
      END;
      
      BEGIN
        IF  :new.franquicia IS NOT NULL AND :new.franquicia != '0' THEN
          SELECT valor 
          INTO mi_numero_cuenta
          FROM valor_defecto_archivo 
          WHERE tipo_archivo = '01' 
          AND tipo_entidad = 'FIN' 
          AND entidad= '7' 
          AND columna= 'NUMERO_CUENTA';
       ELSE 
          SELECT valor 
          INTO mi_numero_cuenta
          FROM valor_defecto_archivo 
          WHERE tipo_archivo = '01' 
          AND tipo_entidad = mi_recibo_consignacion.tipo_entidad 
          AND entidad= mi_recibo_consignacion.entidad 
          AND columna= 'NUMERO_CUENTA';
        END IF;
      END;
      
      BEGIN 
        IF  :new.franquicia IS NOT NULL and :new.franquicia != '0' THEN
            SELECT valor 
            INTO mi_tipo_entidad
            FROM valor_defecto_archivo 
            WHERE tipo_archivo = '01' 
            AND tipo_entidad = 'FIN' 
            AND entidad= '7' 
            AND columna= 'TIPO_ENTIDAD';
        ELSE      
          SELECT valor 
          INTO mi_tipo_entidad
          FROM valor_defecto_archivo 
          WHERE tipo_archivo = '01' 
          AND tipo_entidad = mi_recibo_consignacion.tipo_entidad 
          AND entidad= mi_recibo_consignacion.entidad 
          AND columna= 'TIPO_ENTIDAD';
        END IF;
      END;
      
      BEGIN
        IF :new.franquicia IS NOT NULL  AND :new.franquicia != '0' THEN
          SELECT valor 
          INTO mi_oficina_entidad
          FROM valor_defecto_archivo 
          WHERE tipo_archivo = '01' 
          AND tipo_entidad = 'FIN' 
          AND entidad= '7' 
          AND columna= 'OFICINA_ENTIDAD';
        ELSE 
          SELECT valor 
          INTO mi_oficina_entidad
          FROM valor_defecto_archivo 
          WHERE tipo_archivo = '01' 
          AND tipo_entidad = mi_recibo_consignacion.tipo_entidad 
          AND entidad= mi_recibo_consignacion.entidad 
          and columna= 'OFICINA_ENTIDAD';
        END IF;
      END;
      
      BEGIN
        IF   :new.franquicia IS NOT NULL and :new.franquicia != '0' THEN     
            SELECT valor 
            INTO mi_entidad
            FROM valor_defecto_archivo 
            WHERE tipo_archivo = '01' 
            AND tipo_entidad = 'FIN' 
            AND entidad= '7' 
            AND columna= 'ENTIDAD';
        ELSE 
            SELECT valor 
            INTO mi_entidad
            FROM valor_defecto_archivo 
            WHERE tipo_archivo = '01' 
            AND tipo_entidad = mi_recibo_consignacion.tipo_entidad 
            AND entidad= mi_recibo_consignacion.entidad 
          AND columna= 'ENTIDAD'; 
        END IF;
      END;
                       
                 mi_recibo_caja :=  sd_recibo_consignacion.crear (     
                                   un_organizacion        => mi_recibo_consignacion.organizacion,
                                   un_recibo_consignacion => mi_recibo_consignacion.recibo_consignacion,
                                   un_valor               => :new.valor_pagado,
                                   un_cliente             => mi_recibo_consignacion.cliente,
                                   un_fecha               => :new.dat_fecha,
                                   un_fecha_pago          => :new.dat_fecha,
                                   un_caja                =>mi_caja,
                                   un_modalidad           =>mi_modalidad,
                                   un_moneda              =>mi_moneda,
                                   un_numero_cuenta       =>mi_numero_cuenta,
                                   un_tipo_entidad        =>mi_tipo_entidad,
                                   un_oficina_entidad     =>mi_oficina_entidad,
                                   un_entidad             =>mi_entidad,
                                   un_periodo             =>mi_recibo_consignacion.periodo,
                                   un_tipo_cliente        => NULL,
                                   un_documento           => NULL,
                                   un_orden               => NULL,
                                   un_numero_documento    => NULL
                       );       
                        
  
    END IF;
  END IF;
END;
/