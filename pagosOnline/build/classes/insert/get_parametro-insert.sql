declare
    mi_parametro gep_parametro.DTO;
begin 

    -- INSERTAR Parametro NEGOCIACION_DOLAR
    mi_parametro.componente := 'PO';
    mi_parametro.parametro :='NEGOCIACION_DOLAR';
    mi_parametro.tipo := 'S';
    mi_parametro.permite_nulo := 'N';
    mi_parametro.valor := '15';
    mi_parametro.descripcion := to_clob('COMISIÓN PARA NEGOCIACIÓN DÓLAR');
    mi_parametro.organizacion :=sd_constantes.organizacion;
    gep_parametro.insertar(mi_parametro);
    
END;
/

declare
    mi_parametro gep_parametro.DTO;
begin 

    
    -- INSERTAR Parametro NEGOCIACION_EURO
    mi_parametro.componente := 'PO';
    mi_parametro.parametro :='NEGOCIACION_EURO';
    mi_parametro.tipo := 'S';
    mi_parametro.permite_nulo := 'N';
    mi_parametro.valor := '15';
    mi_parametro.descripcion := to_clob('COMISIÓN PARA NEGOCIACIÓN EURO');
    mi_parametro.organizacion :=sd_constantes.organizacion;
    gep_parametro.insertar(mi_parametro);
    
END;