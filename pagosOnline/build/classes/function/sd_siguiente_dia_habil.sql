CREATE OR REPLACE FUNCTION sd_siguiente_dia_habil (
        un_fecha     IN DATE
    ) RETURN DATE IS
        mi_fecha    DATE := un_fecha;
        mi_retorno VARCHAR2(1) := NULL;
        mi_bandera BOOLEAN := TRUE;
    BEGIN
       
        WHILE (mi_bandera)  
        LOOP
            BEGIN
                SELECT    'S'
                INTO mi_retorno
                FROM    festivo
                WHERE    trunc(festivo) = trunc(mi_fecha)
                AND    rownum = 1;
            EXCEPTION WHEN OTHERS THEN
                mi_retorno := 'N';
            END;
        
        IF( TRIM(TO_CHAR(mi_fecha,'DAY')) IN (
                'SABADO',
                'S�BADO',
                'DOMINGO',
                'SATURDAY',
                'SUNDAY'
                ) OR mi_retorno = 'S') 
        THEN
              mi_fecha := mi_fecha+1;          
        ELSE
                mi_bandera := FALSE;
        END IF;    
        
        END LOOP;
        RETURN mi_fecha;
    END sd_siguiente_dia_habil;
/
