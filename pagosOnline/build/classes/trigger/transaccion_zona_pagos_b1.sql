
CREATE OR REPLACE TRIGGER transaccion_zona_pagos_b1 before delete ON CCT_TRANSACCION_ZONA_PAGOS          for each row
begin 
         if deleting then 
            pk_excepcion.error_aplicacion(0, 'No puede borrar registros de esta tabla... ');
         end if;
         end;
/