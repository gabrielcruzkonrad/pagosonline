create table cct_patrocinador (
    sec_patrocinador number(16) primary key,
    tipo_identificacion  varchar2(3) not null,
    identificacion varchar2(30) not null,
    nombre_razon_social varchar2(255) not null, 
    primer_apellido varchar2(255) null,
    segundo_apellido varchar2(255) null,
    fecha_creacion date  default sysdate not null,
    fecha_constitucion date null,
    archivo_rut varchar2(255) not null,
    secuencia_persona number(38) null,
    direccion_electronica  varchar2(80)  not null,
    telefono_residencia    varchar2(80)   not null,
    contacto_patrocinador  varchar2(255)     not null,
    estado                varchar2(1)       default 'A' not null,
    constraint patrocinador_c1 check(estado in ('A','I')),
    constraint patrocinador_u1 unique(tipo_identificacion, identificacion),
    constraint patrocinador_f1 foreign key  (tipo_identificacion) references tipo_identificacion(tipo_identificacion)  deferrable initially deferred,
    constraint patrocinador_f2 foreign key (secuencia_persona) references persona(secuencia_persona) deferrable initially deferred
);

