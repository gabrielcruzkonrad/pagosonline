CREATE OR REPLACE procedure ccp_registra_transaccion_pesos 
is
    mi_recibo_caja NUMBER := 0;
    mi_error VARCHAR2(4000);
begin 

for pagos in (select rc.organizacion organizacion,
                            tzp.id_pago recibo_consignacion,
                            tzp.valor_pagado valor,
                            rc.cliente cliente,
                            tzp.dat_fecha fecha,
                            tzp.dat_fecha fecha_pago,
                            (select valor from valor_defecto_archivo where tipo_archivo = '01' and tipo_entidad = rc.tipo_entidad and entidad= rc.entidad and columna= 'CAJA') caja,
                            case when tzp.franquicia IS NOT NULL and tzp.franquicia != '0' THEN
                            (select modalidad from modalidad_pago where upper(nombre_modalidad) like '%CREDITO%' and upper(nombre_modalidad) like upper('%'||tzp.franquicia||'%'))
                            ELSE (select valor from valor_defecto_archivo where tipo_archivo = '01' and tipo_entidad = rc.tipo_entidad and entidad= rc.entidad and columna= 'MODALIDAD')
                            END modalidad,
                            (select valor from valor_defecto_archivo where tipo_archivo = '01' and tipo_entidad = rc.tipo_entidad and entidad= rc.entidad and columna= 'MONEDA') moneda,
                            case when  tzp.franquicia IS NOT NULL and tzp.franquicia != '0' THEN
                            (select valor from valor_defecto_archivo where tipo_archivo = '01' and tipo_entidad = 'FIN' and entidad= '7' and columna= 'NUMERO_CUENTA')
                            ELSE (select valor from valor_defecto_archivo where tipo_archivo = '01' and tipo_entidad = rc.tipo_entidad and entidad= rc.entidad and columna= 'NUMERO_CUENTA') 
                            END numero_cuenta,
                            case when  tzp.franquicia IS NOT NULL and tzp.franquicia != '0' THEN
                            (select valor from valor_defecto_archivo where tipo_archivo = '01' and tipo_entidad = 'FIN' and entidad= '7' and columna= 'TIPO_ENTIDAD')
                            ELSE
                            (select valor from valor_defecto_archivo where tipo_archivo = '01' and tipo_entidad = rc.tipo_entidad and entidad= rc.entidad and columna= 'TIPO_ENTIDAD')
                            END tipo_entidad,
                            case when  tzp.franquicia IS NOT NULL  and tzp.franquicia != '0' THEN
                            (select valor from valor_defecto_archivo where tipo_archivo = '01' and tipo_entidad = 'FIN' and entidad= '7' and columna= 'OFICINA_ENTIDAD')
                            ELSE (select valor from valor_defecto_archivo where tipo_archivo = '01' and tipo_entidad = rc.tipo_entidad and entidad= rc.entidad and columna= 'OFICINA_ENTIDAD') 
                            END oficina_entidad,
                            case when  tzp.franquicia IS NOT NULL and tzp.franquicia != '0' THEN
                            (select valor from valor_defecto_archivo where tipo_archivo = '01' and tipo_entidad = 'FIN' and entidad= '7' and columna= 'ENTIDAD')
                            ELSE (select valor from valor_defecto_archivo where tipo_archivo = '01' and tipo_entidad = rc.tipo_entidad and entidad= rc.entidad and columna= 'ENTIDAD') 
                            END entidad, 
                            rc.periodo periodo       
                    from cct_transaccion_zona_pagos tzp
                    inner join recibo_consignacion rc
                    on tzp.id_pago = rc.recibo_consignacion
                    where  
                    tzp.estado_pago = 1
                    and rc.estado = 'I'
                    and tzp.campo2 like '%COP%'
                    ) 
                    loop
                    begin
                      mi_recibo_caja :=  sd_recibo_consignacion.crear (     
                                   un_organizacion =>pagos.organizacion,
                                   un_recibo_consignacion => pagos.recibo_consignacion,
                                   un_valor                 =>pagos.valor,
                                   un_cliente          =>pagos.cliente,
                                   un_fecha            =>pagos.fecha,
                                   un_fecha_pago    =>pagos.fecha_pago,
                                   un_caja                  =>pagos.caja,
                                   un_modalidad             =>pagos.modalidad,
                                   un_moneda                =>pagos.moneda,
                                   un_numero_cuenta     =>pagos.numero_cuenta,
                                   un_tipo_entidad          =>pagos.tipo_entidad,
                                   un_oficina_entidad       =>pagos.oficina_entidad,
                                   un_entidad               =>pagos.entidad,
                                   un_periodo               =>pagos.periodo,
                                   un_tipo_cliente => NULL,
                                   un_documento=> NULL,
                                   un_orden  => NULL,
                                   un_numero_documento => NULL
                       );
                exception when others then    
                mi_error := sqlerrm;
                insert into borrar values ('Error: id_pago'||pagos.recibo_consignacion||mi_error);      
                end;
end loop;
end;
/
